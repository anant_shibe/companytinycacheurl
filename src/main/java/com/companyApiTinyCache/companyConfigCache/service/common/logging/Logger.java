package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.LogLevel;
import org.apache.commons.logging.impl.Log4JLogger;

public class Logger extends Log4JLogger {

    private static final long serialVersionUID = -5485949562916628093L;

    private String name;

    private boolean logToStats;

    public Logger(String pname) {
        super(pname);
        name = pname;
        if (name != null && name.indexOf("cleartrip") > 0) {
            logToStats = true;
        }
    }

    private void log(LogLevel level, Object message, Throwable t) {
        BaseStats baseStats = BaseStats.threadLocal();
        if (baseStats != null) {
            baseStats.log(level, name, message == null ? "null" : message.toString(), t);
        }
    }

    private boolean isEnabled(LogLevel level) {
        boolean isEnabled = false;
        BaseStats baseStats = BaseStats.threadLocal();
        if (baseStats != null && level.getRank() >= baseStats.getLogLevel().getRank()) {
            isEnabled = true;
        }

        return isEnabled;
    }

    @Override
    public boolean isFatalEnabled() {
        boolean isFatalEnabled = super.isFatalEnabled();
        if (!isFatalEnabled) {
            isFatalEnabled = isEnabled(LogLevel.FATAL);
        }
        return isFatalEnabled;
    }

    @Override
    public void fatal(Object message, Throwable t) {
        super.fatal(withPrefixes(message), t);
        log(LogLevel.FATAL, message, t);
    }

    @Override
    public void fatal(Object message) {
        super.fatal(withPrefixes(message));
        log(LogLevel.FATAL, message, null);
    }

    @Override
    public boolean isErrorEnabled() {
        boolean isErrorEnabled = super.isErrorEnabled();
        if (!isErrorEnabled) {
            isErrorEnabled = isEnabled(LogLevel.ERROR);
        }
        return isErrorEnabled;
    }

    @Override
    public void error(Object message, Throwable t) {
        super.error(withPrefixes(message), t);
        log(LogLevel.ERROR, message, t);
    }

    @Override
    public void error(Object message) {
        super.error(withPrefixes(message));
        log(LogLevel.ERROR, message, null);
    }

    @Override
    public boolean isWarnEnabled() {
        boolean isWarnEnabled = super.isWarnEnabled();
        if (!isWarnEnabled) {
            isWarnEnabled = isEnabled(LogLevel.WARNING);
        }
        return isWarnEnabled;
    }

    @Override
    public void warn(Object message, Throwable t) {
        super.warn(withPrefixes(message), t);
        log(LogLevel.WARNING, message, t);
    }

    @Override
    public void warn(Object message) {
        super.warn(withPrefixes(message));
        log(LogLevel.WARNING, message, null);
    }

    @Override
    public boolean isInfoEnabled() {
        boolean isInfoEnabled = super.isInfoEnabled();
        if (!isInfoEnabled && logToStats) {
            isInfoEnabled = isEnabled(LogLevel.INFO);
        }
        return isInfoEnabled;
    }

    @Override
    public void info(Object message, Throwable t) {
        super.info(withPrefixes(message), t);
        if (logToStats) {
            log(LogLevel.INFO, message, t);
        }
    }

    @Override
    public void info(Object message) {
        super.info(withPrefixes(message));
        if (logToStats) {
            log(LogLevel.INFO, message, null);
        }
    }

    @Override
    public boolean isTraceEnabled() {
        boolean isTraceEnabled = super.isTraceEnabled();
        if (!isTraceEnabled && logToStats) {
            isTraceEnabled = isEnabled(LogLevel.INFO);
        }
        return isTraceEnabled;
    }

    @Override
    public void trace(Object message, Throwable t) {
        super.trace(withPrefixes(message), t);
        if (logToStats) {
            log(LogLevel.INFO, message, t);
        }
    }

    @Override
    public void trace(Object message) {
        super.trace(withPrefixes(message));
        if (logToStats) {
            log(LogLevel.INFO, message, null);
        }
    }

    @Override
    public boolean isDebugEnabled() {
        boolean isDebugEnabled = super.isDebugEnabled();
        if (!isDebugEnabled && logToStats) {
            isDebugEnabled = isEnabled(LogLevel.DEBUG);
        }
        return isDebugEnabled;
    }

    @Override
    public void debug(Object message, Throwable t) {
        super.debug(withPrefixes(message), t);
        if (logToStats) {
            log(LogLevel.DEBUG, message, t);
        }
    }

    @Override
    public void debug(Object message) {
        super.debug(withPrefixes(message));
        if (logToStats) {
            log(LogLevel.DEBUG, message, null);
        }
    }

    private Object withPrefixes(Object msg) {
        LogData logData = LogThreadLocal.getLogData();
        if (logData == null)
            return msg;
        return logData.asString() + "'" + msg + "'";
    }
}
