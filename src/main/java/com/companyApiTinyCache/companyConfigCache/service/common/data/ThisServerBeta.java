package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by kcrob.in on 02/04/18.
 */
public class ThisServerBeta extends ThisServer{

    private static final Log logger = LogFactory.getLog(ThisServerBeta.class);

    public ThisServerBeta(CacheFactory cacheFactory) {
        super(cacheFactory);
    }

    @Override
    protected String getRelease() {
        String release = super.getRelease();
        switch (release) {
            case "release1": {
                release = "release3";
                break;
            }
            case "release3": {
                release = "release1";
                break;
            }
        }

        return release;
    }
}