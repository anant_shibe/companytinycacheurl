package com.companyApiTinyCache.companyConfigCache.service.common.util.rest;

import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestClient.CharEncoding;

import javax.servlet.http.Cookie;
import java.util.List;
import java.util.Map;

/**
 * Holds data for a HTTP request, executed by RestClient.
 */
public class RestRequest {

    public static final String CONTENT_TYPE_FORM_URL_ENCODED = "application/x-www-form-urlencoded";

    public static final String CONTENT_TYPE_JSON = "application/json";

    public static final int DEFAULT_TIMEOUT = 120000; // 2 minutes

    private String targetUrl;

    private String queryString;

    private Map<String, String> headers;

    private List<Cookie> cookies;

    private String contentType;

    private CharEncoding charEncoding;

    private String payload;

    private String userName;

    private String password;

    private int timeout;

    private boolean ignoreHostVerification;

    private String paramPrefix;

    private boolean gunzipResponse;

    private boolean isByteResponse;

    public RestRequest(String targetUrl) {
        this.targetUrl = targetUrl;
        this.timeout = DEFAULT_TIMEOUT;
        this.contentType = "text/xml";
        this.payload = "";
        this.gunzipResponse = true;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public RestRequest setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
        return this;
    }

    public String getQueryString() {
        return queryString;
    }

    public RestRequest setQueryString(String queryString) {
        this.queryString = queryString;
        return this;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public RestRequest setHeaders(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    public List<Cookie> getCookies() {
        return cookies;
    }

    public RestRequest setCookies(List<Cookie> cookies) {
        this.cookies = cookies;
        return this;
    }

    public String getContentType() {
        return contentType;
    }

    public RestRequest setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public CharEncoding getCharEncoding() {
        return charEncoding;
    }

    public RestRequest setCharEncoding(CharEncoding charEncoding) {
        this.charEncoding = charEncoding;
        return this;
    }

    public String getPayload() {
        return payload;
    }

    public RestRequest setPayload(String payload) {
        this.payload = payload;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public RestRequest setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public RestRequest setPassword(String password) {
        this.password = password;
        return this;
    }

    public int getTimeout() {
        return timeout;
    }

    public RestRequest setTimeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    public boolean isIgnoreHostVerification() {
        return ignoreHostVerification;
    }

    public RestRequest setIgnoreHostVerification(boolean ignoreHostVerification) {
        this.ignoreHostVerification = ignoreHostVerification;
        return this;
    }

    public boolean isGunzipResponse() {
        return gunzipResponse;
    }

    public void setGunzipResponse(boolean gunzipResponse) {
        this.gunzipResponse = gunzipResponse;
    }

    public boolean isByteResponse() {
        return isByteResponse;
    }

    public void setByteResponse(boolean isByteResponse) {
        this.isByteResponse = isByteResponse;
    }

    public String getParamPrefix() {
        return paramPrefix;
    }

    public RestRequest setParamPrefix(String paramPrefix) {
        this.paramPrefix = paramPrefix;
        return this;
    }

    @Override
    public String toString() {
        return String.format("RestResquest {%n URL: %s %n Query string: %s %n Headers: %s %n Payload: %s %n}", this.targetUrl, this.queryString, this.headers, this.payload);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof RestRequest)) {
            return false;
        }
        RestRequest otherReq = (RestRequest) other;
        return compare(this.targetUrl, otherReq.targetUrl) && compare(this.queryString, otherReq.queryString) && compare(this.payload, otherReq.payload);
    }

    @Override
    public int hashCode() {
        return this.targetUrl.hashCode() ^ this.queryString.hashCode() ^ this.payload.hashCode();
    }

    private boolean compare(String s1, String s2) {
        if (s1 != null && s2 != null) {
            return s1.equals(s2);
        } else if ((s1 != null && s2 == null) || (s1 == null && s2 != null)) {
            return false;
        }
        return true;
    }
}
