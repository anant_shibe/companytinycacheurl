/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.data.*;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.EmailType;
import com.csvreader.CsvReader;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.EmailValidator;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Cleartrip
 * 
 */
public class CSVParser extends ImportParser {

    public List<List<String>> parseFile(String fileName) throws Exception {
        CsvReader reader = new CsvReader(new InputStreamReader(new FileInputStream(fileName), Charset.forName("UTF-8")));
        List<List<String>> parsedRecords = parse(reader);
        reader.close();
        return parsedRecords;
    }

    public List<List<String>> parseStream(byte[] stream) throws Exception {
        CsvReader reader = new CsvReader(new InputStreamReader(new ByteArrayInputStream(stream)));
        List<List<String>> parsedRecords = parse(reader);
        reader.close();
        return parsedRecords;
    }

    private List<List<String>> parse(CsvReader reader) throws Exception {
        List<List<String>> userRecords = new ArrayList<List<String>>();

        try {
            reader.readHeaders();

            int count = 0;
            while (reader.readRecord()) {
                count++;
                List<String> userRecord = new ArrayList<String>();
                String title = reader.get(0);
                String firstName = reader.get(1);
                String lastName = reader.get(2);
                String employeeId = reader.get(3);
                String email = reader.get(4);
                String designation = reader.get(5);
                String mobile = reader.get(6);
                String officePhone = reader.get(7);
                String homeEmail = reader.get(8);
                String workAddress = reader.get(9);
                String city = reader.get(10);
                String state = reader.get(11);
                String pinCode = reader.get(12);
                String country = reader.get(13);

                boolean hasPrefix = false;
                for (String honoraryPrefix : honoraryPrefixes) {
                    if (title.equalsIgnoreCase(honoraryPrefix)) {
                        if (honoraryPrefix.contains(".")) {
                            honoraryPrefix = honoraryPrefix.substring(0, ((honoraryPrefix.length()) - 1));
                        }
                        userRecord.add(honoraryPrefix);
                        hasPrefix = true;
                        break;
                    }
                }
                if (!hasPrefix)
                    userRecord.add(""); // add an empty title
                userRecord.add(firstName);
                userRecord.add(lastName);
                userRecord.add(employeeId);
                userRecord.add(email);
                userRecord.add(designation);
                userRecord.add(mobile);
                userRecord.add(officePhone);
                userRecord.add(homeEmail);
                userRecord.add(workAddress);
                userRecord.add(city);
                userRecord.add(state);
                userRecord.add(pinCode);
                userRecord.add(country);
                userRecords.add(userRecord);
                // System.out.println(count + ". " + title + ". " + firstName + " " + lastName + " " + mobile + " " + officePhone);
            }
        } catch (IOException iox) {
            logger.error(" ");
        }
        return userRecords;
    }

    public List<UserProfile> getImportedUserList(List<List<String>> importedUserRecords) {
        ArrayList<UserProfile> importedUserList = new ArrayList<UserProfile>();
        for (List<String> importedUserRecord : importedUserRecords) {
            UserProfile userProfile = new UserProfile();
            CompanyDetail companyDetail = new CompanyDetail();
            userProfile.getCompanyDetails().put(companyDetail.getCompanyId(), companyDetail);

            userProfile.getPersonalDetail().setTitle(importedUserRecord.get(0));
            userProfile.getPersonalDetail().setFirstName(importedUserRecord.get(1));
            userProfile.getPersonalDetail().setLastName(importedUserRecord.get(2));

            PhoneNumbers phoneNumbers = new PhoneNumbers();
            PhoneNumber mobileNumber = new PhoneNumber();
            PhoneNumber officeNumber = new PhoneNumber();
            mobileNumber.setNumber(importedUserRecord.get(6));
            mobileNumber.setType(TELE_TYPE_MOBILE);
            officeNumber.setNumber(importedUserRecord.get(7));
            officeNumber.setType(TYPE_WORK);
            phoneNumbers.add(mobileNumber);
            phoneNumbers.add(officeNumber);

            ContactDetail contactDetail = userProfile.getContactDetail();
            if (contactDetail == null) {
                contactDetail = new ContactDetail();
            }
            contactDetail.setPhoneNumbers(phoneNumbers);

            Address workAddress = new Address();
            workAddress.setStreetAddress(importedUserRecord.get(9));
            workAddress.setCity(importedUserRecord.get(10));
            workAddress.setState(importedUserRecord.get(11));
            workAddress.setPincode(importedUserRecord.get(12));
            workAddress.setCountry(importedUserRecord.get(13));
            workAddress.setAddressType(TYPE_WORK);
            contactDetail.setWorkAddress(workAddress);

            ArrayList<Email> mailList = new ArrayList<Email>();
            setEmailIdsInContactDetail(mailList, importedUserRecord.get(4), TYPE_WORK);
            setEmailIdsInContactDetail(mailList, importedUserRecord.get(8), TYPE_HOME);
            userProfile.getContactDetail().setEmails(mailList);
            String primaryEmail = userProfile.getContactDetail().getEmail(CommonEnumConstants.EmailType.WORK);
            userProfile.getPersonalDetail().setPrimaryEmail(primaryEmail);

            Map<Integer, CompanyDetail> companyDetails = userProfile.getCompanyDetails();
            CompanyDetail comDetail = null;
            if (companyDetails != null && !companyDetails.isEmpty()) {
                comDetail = companyDetails.get(0);
                if (comDetail == null) {
                    comDetail = new CompanyDetail();
                }
            }

            comDetail.setEmployeeId(importedUserRecord.get(3));
            comDetail.setCompanyDesignation(importedUserRecord.get(5));
            companyDetails.put(0, comDetail);
            userProfile.setCompanyDetails(companyDetails);
            importedUserList.add(userProfile);
        }
        return importedUserList;
    }

    private void setEmailIdsInContactDetail(ArrayList<Email> mailList, String email, String emailType) {
        if (StringUtils.isNotEmpty(email)) {
            if (EmailValidator.getInstance().isValid(email)) {
                Email e_mail = new Email();
                e_mail.setEmailId(email);
                e_mail.setType(emailType);
                mailList.add(e_mail);
            }
        }
    }
}
