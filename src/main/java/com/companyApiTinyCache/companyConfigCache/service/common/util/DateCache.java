package com.companyApiTinyCache.companyConfigCache.service.common.util;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A utility class to cache Date and XMLGregorianCalendar values for the next 400 truncated days (A little more than a year).
 * This is done for performance improvement to avoid the repeated cost of creating these values again and
 * again for the same days. It will be useful to include Formatted dates in one or two formats if required.
 * @author suresh
 *
 */
public final class DateCache {

    private static final class DateCacheEntry {
        private Date date;

        private XMLGregorianCalendar xmlGreg;

        private DateCacheEntry(Date d, XMLGregorianCalendar c) {
            date = d;
            xmlGreg = c;
        }
    }

    public static final int MAX_DAYS = 400;

    private int baseDate;

    private long nextDayMillis;

    private DateCacheEntry [] entries;

    private static volatile DateCache instance = new DateCache(System.currentTimeMillis(), null);

    private static Lock lock = new ReentrantLock();

    private static long mockTimeInMillis;

    private DateCache(long now, DateCache prevValue) {
        Calendar c = Calendar.getInstance();
        entries = new DateCacheEntry[MAX_DAYS];
        long todayMillis = GenUtil.getTruncatedDateMillis(now, c);
        c.setTimeInMillis(todayMillis);
        baseDate = ShortDate.toShortDate(todayMillis);
        nextDayMillis = ShortDate.toMillis((short) (baseDate + 1));
        int numEntries = 0;
        if (prevValue != null) {
            int prevBaseDate = prevValue.baseDate;
            if (baseDate >= prevBaseDate) {
                int ofs = baseDate - prevBaseDate;
                DateCacheEntry [] prevEntries = prevValue.entries;
                if (ofs < prevEntries.length) {
                    numEntries = prevEntries.length - ofs;
                    System.arraycopy(prevEntries, ofs, entries, 0, numEntries);
                }
            }
        }
        c.add(Calendar.DAY_OF_YEAR, numEntries);
        GregorianCalendar tmpGreg = new GregorianCalendar();
        while (numEntries < entries.length) {
            Date date = c.getTime();
            XMLGregorianCalendar xmlGreg = GenUtil.dateToXmlGreg(date, tmpGreg);
            entries[numEntries] = new DateCacheEntry(date, xmlGreg);
            c.add(Calendar.DAY_OF_YEAR, 1);
            numEntries++;
        }
    }

    private DateCacheEntry getEntry(int shortDate) {
        DateCacheEntry entry = null;
        int ofs = shortDate - baseDate;
        DateCacheEntry [] cacheEntries = entries;
        if (ofs >= 0 && ofs < cacheEntries.length) {
            entry = cacheEntries[ofs];
        }

        return entry;
    }

    private static DateCacheEntry checkEntry(int shortDate) {
        DateCache dateCache = instance;
        DateCacheEntry entry = dateCache.getEntry(shortDate);
        if (entry == null) {
            long now = System.currentTimeMillis();
            if (mockTimeInMillis > 0) {
                now = mockTimeInMillis;
            }
            if (now >= dateCache.nextDayMillis) {
                if (lock.tryLock()) {
                    try {
                        dateCache = instance;
                        if (now >= dateCache.nextDayMillis) {
                            dateCache = new DateCache(now, dateCache);
                            instance = dateCache;
                        }
                    } finally {
                        lock.unlock();
                    }
                }
                entry = dateCache.getEntry(shortDate);
            }
        }

        return entry;
    }

    public static Date getDate(int shortDate) {
        Date d = null;
        DateCacheEntry entry = checkEntry(shortDate);
        if (entry != null) {
            d = entry.date;
        }
        if (d == null) {
            d = new Date(ShortDate.toMillis((short) shortDate));
        }

        return d;
    }

    public static XMLGregorianCalendar getXmlGreg(int shortDate) {
        XMLGregorianCalendar xmlGreg = null;
        DateCacheEntry entry = checkEntry(shortDate);
        if (entry != null) {
            xmlGreg = entry.xmlGreg;
        }
        if (xmlGreg == null) {
            Date d = new Date(ShortDate.toMillis((short) shortDate));
            xmlGreg = GenUtil.dateToXmlGreg(d);
        }

        return xmlGreg;
    }

    // For Testing next day
    static void setMockTimeInMillis(long pmockTimeInMillis) {
        mockTimeInMillis = pmockTimeInMillis;
    }
}
