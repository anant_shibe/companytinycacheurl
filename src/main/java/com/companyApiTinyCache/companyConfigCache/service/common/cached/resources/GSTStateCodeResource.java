/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.cached.resources;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedUrlResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.StreamResourceLoader;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Map;

/**
 * @author sumanth
 *
 */
@ClassExcludeCoverage
public class GSTStateCodeResource extends CachedUrlResource<Map<String, Map<String, Object>>> {

	private static final Log logger = LogFactory.getLog(GSTStateCodeResource.class);

	public GSTStateCodeResource(CachedProperties pcommonCachedProperties)
			throws MalformedURLException {
		super(pcommonCachedProperties, "ct.common.gst.state.code.resource.url.ctconfig", null, false);
		setLoader(new GSTStateCodeLoader());
		refreshResource();
	}

	private class GSTStateCodeLoader implements StreamResourceLoader<Map<String, Map<String, Object>>> {

		@SuppressWarnings("unchecked")
		@Override
		public Map<String, Map<String, Object>> loadStreamResource(
				InputStream is) throws Exception {
			logger.info("loading gst state code resource");

			Map<String, Map<String, Object>> gstStateCodeResourceMap = null;

			String gstStateCodeResourceJson = null;

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

			String line = null;

			StringBuilder sb = new StringBuilder();

			while ((line = bufferedReader.readLine()) != null) {

				sb.append(line);
			}

			bufferedReader.close();

			gstStateCodeResourceJson = sb.toString();

			ObjectMapper mapper = JsonUtil.getObjectMapper();

			gstStateCodeResourceMap = (Map<String, Map<String, Object>>) mapper.readValue(gstStateCodeResourceJson, Map.class);

			logger.info("done with loading gst state code resource");

			return gstStateCodeResourceMap;
		}
	}

}
