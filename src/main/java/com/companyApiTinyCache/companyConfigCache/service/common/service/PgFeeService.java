package com.companyApiTinyCache.companyConfigCache.service.common.service;

import java.util.Map;

public interface PgFeeService {

    Map<String, Map<String, Double>> getPgFee(String prod);

    double getPgFeeTaxPercent();

}
