package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class TimeKeeper implements Serializable {

    private static final long serialVersionUID = -8780990991522826640L;

    private static final Log logger = LogFactory.getLog(TaskNode.class);

    private List<TaskNode> rootTasks;

    private List<TaskNode> openTasks;

    public static class TaskNode implements MutableTreeNode, Serializable {

        private static final long serialVersionUID = -7750674776561956540L;

        private TaskNode parent;

        private List<TaskNode> children;

        private String name;

        private long start;

        private long total;

        private long count;

        public TaskNode(String taskName) {
            name = taskName;
            children = new ArrayList<TaskNode>(4);
        }

        public void markStart(long time) {
            start = time;
        }

        public void markEnd(long time) {
            if (start > 0) {
                total += (time - start);
                start = 0;
                count++;
            }
        }

        /**
         * {@inheritDoc}
         * 
         * @see MutableTreeNode#insert(MutableTreeNode, int)
         */
        @Override
        public void insert(MutableTreeNode child, int index) {
            TaskNode childNode = (TaskNode) child;
            children.add(index, childNode);
        }

        public void add(MutableTreeNode child) {
            TaskNode childNode = (TaskNode) child;
            children.add(childNode);
        }

        /**
         * {@inheritDoc}
         *
         * @see MutableTreeNode#remove(int)
         */
        @Override
        public void remove(int index) {
            children.remove(index);
        }

        /**
         * {@inheritDoc}
         *
         * @see MutableTreeNode#remove(MutableTreeNode)
         */
        @Override
        public void remove(MutableTreeNode node) {
            if (!children.remove(node)) {
                throw new IllegalArgumentException("Child TaskNode Not Found");
            }
        }

        /**
         * {@inheritDoc}
         *
         * @see MutableTreeNode#removeFromParent()
         */
        @Override
        public void removeFromParent() {
            parent.remove(this);
        }

        /**
         * {@inheritDoc}
         *
         * @see MutableTreeNode#setParent(MutableTreeNode)
         */
        @Override
        public void setParent(MutableTreeNode newParent) {
            parent.remove(this);
            TaskNode parentNode = (TaskNode) newParent;
            parentNode.add(this);
        }

        /**
         * {@inheritDoc}
         *
         * @see MutableTreeNode#setUserObject(Object)
         */
        @Override
        public void setUserObject(Object object) {
            throw new UnsupportedOperationException("setUserObject not supported");
        }

        /**
         * {@inheritDoc}
         *
         * @see TreeNode#children()
         */
        @Override
        public Enumeration<TaskNode> children() {
            return Collections.enumeration(children);
        }

        /**
         * {@inheritDoc}
         *
         * @see TreeNode#getAllowsChildren()
         */
        @Override
        public boolean getAllowsChildren() {
            return true;
        }

        /**
         * {@inheritDoc}
         *
         * @see TreeNode#getChildAt(int)
         */
        @Override
        public TreeNode getChildAt(int childIndex) {
            return children.get(childIndex);
        }

        /**
         * {@inheritDoc}
         *
         * @see TreeNode#getChildCount()
         */
        @Override
        public int getChildCount() {
            return children.size();
        }

        /**
         * @see TreeNode#getIndex(TreeNode)
         */
        @Override
        public int getIndex(TreeNode node) {
            return children.indexOf(node);
        }

        /**
         * {@inheritDoc}
         *
         * @see TreeNode#getParent()
         */
        @Override
        public TreeNode getParent() {
            return parent;
        }

        /**
         * {@inheritDoc}
         *
         * @see TreeNode#isLeaf()
         */
        @Override
        public boolean isLeaf() {
            return children.size() == 0;
        }

        /**
         * @see Object#equals(Object)
         */
        @Override
        public boolean equals(Object obj) {
            boolean isEqual = false;
            if (obj != null && obj instanceof TaskNode) {
                TaskNode otherNode = (TaskNode) obj;
                if (name != null) {
                    isEqual = name.equals(otherNode.name);
                } else {
                    isEqual = otherNode.name == null;
                }
            }
            return isEqual;
        }

        /**
         * Getter for start.
         * 
         * @return the start
         */
        public long getStart() {
            return start;
        }

        /**
         * Setter for start.
         * 
         * @param pstart
         *            the start to set
         */
        public void setStart(long pstart) {
            start = pstart;
        }

        /**
         * Getter for name.
         * 
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Setter for name.
         * 
         * @param pname
         *            the name to set
         */
        public void setName(String pname) {
            name = pname;
        }

        /**
         * Getter for total.
         * 
         * @return the total
         */
        public long getTotal() {
            return total;
        }

        /**
         * Setter for total.
         * 
         * @param ptotal
         *            the total to set
         */
        public void setTotal(long ptotal) {
            total = ptotal;
        }
    }

    // Main Class

    public TimeKeeper() {
        rootTasks = new ArrayList<TaskNode>();
        openTasks = new ArrayList<TaskNode>();
    }

    /**
     * Logs Task Start after creating one. Will be attached as the child of the most recent unfinished task if found. Otherwise it is put as a root Task.
     * 
     * @param taskName
     *            Task name
     * @param time
     *            milliseconds time or 0 to use System.currentTimeMillis()
     * @return milliseconds time
     */
    public long taskStarted(String taskName, long time) {
        try {
            if (time <= 0) {
                time = System.currentTimeMillis();
            }
            TaskNode newTask = null;
            int len = openTasks.size();
            TaskNode parentTask = null;
            if (len > 0) {
                parentTask = openTasks.get(len - 1);
            }
            List<TaskNode> peerList = null;
            if (parentTask != null) {
                peerList = parentTask.children;
            } else {
                peerList = rootTasks;
            }
            for (TaskNode testTask : peerList) {
                if (testTask.name.equals(taskName) && testTask.start <= 0) {
                    newTask = testTask;
                    break;
                }
            }
            if (newTask == null) {
                newTask = new TaskNode(taskName);
                peerList.add(newTask);
            }
            newTask.markStart(time);
            openTasks.add(newTask);
        } catch (Exception e) {
            try {
                logger.error("Error in taskStarted: ", e);
            } catch (Exception e1) {
            }
        }
        return time;
    }

    private void closeTaskTree(TaskNode task, long time) {
        for (TaskNode child : task.children) {
            closeTaskTree(child, time);
        }
        task.markEnd(time);
    }

    /**
     * Logs Task End for the most recent open task with the given taskName. Logs error if not found. Automatically calls taskFinished on all the child tasks.
     * 
     * @param taskName
     *            Task name
     * @param time
     *            milliseconds time or 0 to use System.currentTimeMillis()
     * @return milliseconds time
     */
    public long taskFinished(String taskName, long time) {
        try {
            if (time <= 0) {
                time = System.currentTimeMillis();
            }
            TaskNode openTask = null;
            int len = openTasks.size();
            int i = len;
            while (i > 0) {
                i--;
                TaskNode testTask = openTasks.get(i);
                if (taskName.equals(testTask.name)) {
                    openTask = testTask;
                    break;
                }
            }
            if (openTask != null && openTask.start > 0) {
                closeTaskTree(openTask, time);
                // Ideally all open tasks after closed task should be descendants.
                // Delete them from openTasks
                while (len > i) {
                    openTasks.remove(i);
                    len--;
                }
            } else {
                logger.info("No Open Task with name " + taskName + " found");
            }
        } catch (Exception e) {
            try {
                logger.error("Error in taskFinished: ", e);
            } catch (Exception e1) {
            }
        }
        return time;
    }

    /**
     * Does nothing if there is no open root Task. Otherwise closes the open root Task after logging an error.
     */
    public long allTasksOver(long time) {
        try {
            if (time <= 0) {
                time = System.currentTimeMillis();
            }
            for (TaskNode rootTask : rootTasks) {
                if (rootTask.start > 0) {
                    logger.info("Root Task " + rootTask.name + " Not marked as Finished. Closing it.");
                    closeTaskTree(rootTask, time);
                }
            }
        } catch (Exception e) {
            try {
                logger.error("Error in taskFinished: ", e);
            } catch (Exception e1) {
            }
        }
        return time;
    }

    private void printIndent(PrintWriter writer, int indent) {
        for (int i = 0; i < indent; i++) {
            writer.print(' ');
        }
    }

    /**
     * Prints Task Summary for a task and children
     * 
     * @param task
     *            Task
     * @param writer
     *            Writer
     * @param indent
     *            Indentation
     * @return total time
     */
    private long print(TaskNode task, PrintWriter writer, int indent) {
        long rolledUpTime = 0;
        printIndent(writer, indent);
        writer.println("Task-" + task.name + ":: Time: " + task.total + ", count: " + task.count);
        if (task.children.size() > 0) {
            for (TaskNode childTask : task.children) {
                rolledUpTime += print(childTask, writer, indent + 4);
            }
            printIndent(writer, indent + 4);
            writer.println("Unaccounted Time::" + (task.total - rolledUpTime));
        }
        return task.total;
    }

    /**
     * Prints Task Summary for a task and children
     * 
     * @param task
     *            Task
     * @param writer
     *            Writer
     * @param indent
     *            Indentation
     * @return total time
     */
    private long printXml(TaskNode task, PrintWriter writer, int indent) {
        long rolledUpTime = 0;
        printIndent(writer, indent);
        writer.print("<task name=\"" + task.name + "\" time=\"" + task.total + '\"');
        if (task.count > 1) {
            writer.print(" count=\"" + task.count + '"');
        }
        writer.println('>');
        if (task.children.size() > 0) {
            for (TaskNode childTask : task.children) {
                rolledUpTime += printXml(childTask, writer, indent + 4);
            }
            if (task.total != rolledUpTime) {
                printIndent(writer, indent + 4);
                writer.println("<unccounted-time>" + (task.total - rolledUpTime) + "</unccounted-time>");
            }
        }
        printIndent(writer, indent);
        writer.println("</task>");
        return task.total;
    }

    /**
     * Prints Task Summary
     * 
     * @param writer
     *            Writer
     * @param indent
     *            Indentation
     * @return total time
     */
    public void print(PrintWriter writer, int indent) {
        for (TaskNode task : rootTasks) {
            print(task, writer, indent);
            writer.println();
        }
    }

    /**
     * Prints Task Summary
     * 
     * @param writer
     *            Writer
     * @param indent
     *            Indentation
     * @return total time
     */
    public void printXml(PrintWriter writer, int indent) {
        for (TaskNode task : rootTasks) {
            printXml(task, writer, indent);
            writer.println();
        }
    }

    public List<TaskNode> getRootTasks() {
        return rootTasks;
    }
}
