package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.commons.lang.StringUtils;

import java.io.File;

public abstract class CachedFileResource<R> extends CachedResource<R> {

    private String fileName;

    private String dir;

    private String file;

    private long fileModifiedTime;

    private void setFile() {
        file = null;
        if (!StringUtils.isEmpty(dir) && !StringUtils.isEmpty(fileName)) {
            file = dir + fileName;
        }
    }

    protected abstract R loadFileResource(File f) throws Exception;

    /**
     * Loads File Resource by checking timestamp and then calling loadFileResource. {@inheritDoc}
     * 
     * @see com.cleartrip.common.util.CachedResource#loadResource()
     */
    @Override
    protected R loadResource(long cacheMillis, boolean force) throws Exception {
        R resource = null;
        boolean requiresRefresh = force;
        File f = new File(file);
        long currentFileModifiedTime = f.lastModified();
        if (!force) {
            requiresRefresh = currentFileModifiedTime > fileModifiedTime;
        }
        if (requiresRefresh) {
            fileModifiedTime = currentFileModifiedTime; // Changed First itself to avoid repeating errors
            isErrorOnLastLoad = false;
            resource = loadFileResource(f);
        }

        return resource;
    }

    /**
     * Getter for fileName.
     * 
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Setter for fileName.
     * 
     * @param pfileName
     *            the fileName to set
     */
    public void setFileName(String pfileName) {
        fileName = pfileName;
        setFile();
    }

    /**
     * Getter for dir.
     * 
     * @return the dir
     */
    public String getDir() {
        return dir;
    }

    /**
     * Setter for dir.
     * 
     * @param pdir
     *            the dir to set
     */
    public void setDir(String pdir) {
        dir = pdir;
        setFile();
    }

    /**
     * Getter for file.
     * 
     * @return the file
     */
    public String getFile() {
        return file;
    }
}
