package com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.LogData;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.LogThreadLocal;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.MDC;

/**
 * User: Naveen Date: 7/31/13
 */

@ClassExcludeCoverage
public class LogEnabledRunnable implements Runnable {
    private LogData logData;
    private Runnable taskToBeRun;
    String traceID = null;
    String callerID = null;

    public LogEnabledRunnable(Runnable taskToBeRun) {
        traceID = (String) MDC.get("traceID");
        this.taskToBeRun = taskToBeRun;
        logData = LogThreadLocal.getLogData();
    }

    @Override
    public void run() {
        if(StringUtils.isNotEmpty(traceID)) {
            MDC.put("traceID", traceID);
            MDC.put("callerID",callerID);
        }
        LogThreadLocal.setLogData(logData);
        try {
            taskToBeRun.run();
        } finally {
            MDC.remove("traceID");
            MDC.remove("callerID");
            LogThreadLocal.getThreadLocal().remove();
        }
    }
}
