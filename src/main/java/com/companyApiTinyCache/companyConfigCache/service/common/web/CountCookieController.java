package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.FilterConfig;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author cleartrip
 *
 */
public class CountCookieController extends AbstractController {
    private static final Log logger = LogFactory.getLog(CountCookieController.class);
    protected FilterConfig filterConfig;
    protected CachedProperties cachedProperties;
    protected static final String CT_COUNT_COOKIE = "ct.services.count-cookiename";
    protected static final String CT_COUNT_COOKIE_BOOKING = "ct.services.count-cookiename.booking.detail";
    private static final String CT_COUNT_COOKIE_JSON = "ctjson";
    private static final String CT_COUNT_COOKIE_VALUE = "ctvalue";
    private static final Integer oneYear = 365 * 24 * 60 * 60;
    private static final byte[] defaultAuthKey = {65, (byte) 128, 127, (byte) 226, 30, (byte) 252, (byte) 136, (byte) 224, (byte) 135, (byte) 178, (byte) 142, (byte) 208, 104, (byte) 183, (byte) 183,
            23, (byte) 182, 11, 104, 102, 86, 107, 21, (byte) 240, (byte) 182, (byte) 242, (byte) 146, 107, 73, 11, 11, 3};

    public CountCookieController(CachedProperties pcachedUiProperties) {
        cachedProperties = pcachedUiProperties;
        setCacheSeconds(0);
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Cookie countCookie = getCountCookie(request, cachedProperties.getPropertyValue(CT_COUNT_COOKIE, "ct_ct"));
        String countCookieValue = countCookie.getValue();
        String apacheCookieValue = getApacheCookie(request).getValue();
        String prod = request.getParameter("p");
        String pageName = request.getParameter("n");
        String json = request.getParameter("bd");
        String sendResponse = request.getParameter("sr");

        long hrsSinceFirstVisit = 0;
        Cookie utmaCookie = GenUtil.getCookieFromRequest(request, "__utma");
        if (utmaCookie != null) {
            String[] utmaCookieSplit = utmaCookie.getValue().split("\\.");
            String firstVisitTimeStr = utmaCookieSplit[2];
            long firstVisitTimeMillis = Long.parseLong(firstVisitTimeStr) * 1000;
            long currentTimeMillis = Calendar.getInstance().getTimeInMillis();
            hrsSinceFirstVisit = (currentTimeMillis - firstVisitTimeMillis) / (3600000);
        }

        boolean isBookingDetailCookieRequired = cachedProperties.getBooleanPropertyValue("ct.common.booking.detail.cookie.required", true);
        if (StringUtils.isNotEmpty(pageName) && StringUtils.isNotEmpty(prod) && pageName.equals("c") && StringUtils.isNotEmpty(json) && isBookingDetailCookieRequired) {
            Cookie bookingDetailCookie = getCountCookie(request, cachedProperties.getPropertyValue(CT_COUNT_COOKIE_BOOKING, "bd_bd"));
            String bookingDetailCookieValue = bookingDetailCookie.getValue();
            String newBookingCookieValue;
            if (StringUtils.isEmpty(bookingDetailCookieValue)) {
                newBookingCookieValue = createBookingCookie(prod, "bd", json, apacheCookieValue, hrsSinceFirstVisit);
            } else {
                newBookingCookieValue = modifyBookingCookie(bookingDetailCookieValue, prod, "bd", json, apacheCookieValue, hrsSinceFirstVisit);
            }
            if (StringUtils.isNotEmpty(newBookingCookieValue)) {
                bookingDetailCookie.setValue(newBookingCookieValue);
                response.addCookie(bookingDetailCookie);
            }

        }

        Map<String, String> cookieDetails = null;
        if (StringUtils.isEmpty(countCookieValue)) {
            cookieDetails = createCountCookie(prod, pageName, apacheCookieValue, hrsSinceFirstVisit);
        } else {
            cookieDetails = modifyCountCookie(countCookieValue, prod, pageName, apacheCookieValue, hrsSinceFirstVisit);
        }

        if (cookieDetails != null && !cookieDetails.isEmpty()) {
            if (cookieDetails.get(CT_COUNT_COOKIE_VALUE) != null) {
                countCookie.setValue(cookieDetails.get(CT_COUNT_COOKIE_VALUE));
                response.addCookie(countCookie);
            }
        }

        if (StringUtils.isNotEmpty(sendResponse) && sendResponse.equals("y")) {
            String cookieJson = cookieDetails.get(CT_COUNT_COOKIE_JSON);
            if (StringUtils.isNotEmpty(cookieJson)) {
                Map<String, Map<String, Number>> countCookieMap = new HashMap<String, Map<String, Number>>();
                countCookieMap = APIUtil.deserializeJsonToObject(cookieJson, HashMap.class);
                if (countCookieMap != null) {
                    Map<String, Number> cookieDetail = countCookieMap.get(prod);
                    cookieDetail.put("ct", getCurrentTime());
                }
                cookieJson = APIUtil.serializeObjectToJson(countCookieMap);

                response.setContentType("application/json");
                PrintWriter responseWriter = new PrintWriter(response.getWriter());
                responseWriter.println(cookieJson);
                responseWriter.flush();
                responseWriter.close();
            }
        }

        return null;
    }

    private String modifyBookingCookie(String cookieValue, String productName, String pageName, String json, String apacheCookieValue, long hrsSinceFirstVisit) {

        try {
            String decryptedCookie = SecurityUtil.aesDecrypt(URLDecoder.decode(cookieValue, "UTF-8"), getAcubeKey());
            logger.debug("DecryptedCountCookie from modifyCountCookie(): " + decryptedCookie);
            if (StringUtils.isNotEmpty(decryptedCookie)) {
                String[] splitCookie = decryptedCookie.split("\\|", -1);
                ObjectMapper mapper = JsonUtil.getObjectMapper();
                Map<String, Map<String, Object>> countCookieMap = (Map<String, Map<String, Object>>) mapper.readValue(splitCookie[0], Map.class);
                modifyBookingDetails(productName, pageName, json, countCookieMap);
                cookieValue = APIUtil.serializeObjectToJson(countCookieMap);
                // we are doing one more split on "{" because of a bug that was introduced
                // which resulted in adding the count cookie json right after the createdDate string in the cookie
                long createdDate = Long.parseLong(splitCookie[1].split("\\{", -1)[0]);
                return generateCountCookie(apacheCookieValue, cookieValue, createdDate, getCurrentTime(), hrsSinceFirstVisit);
            } else {
                logger.info("Decrypted cookie is empty/null. Creating a new booking cookie instead.");
                return createBookingCookie(productName, "bd", json, apacheCookieValue, hrsSinceFirstVisit);
            }
        } catch (Exception e) {
            logger.error("Error while modifying countCookie", e);
            return createBookingCookie(productName, "bd", json, apacheCookieValue, hrsSinceFirstVisit);
        }

    }

    private void modifyBookingDetails(String productName, String pageName, String json, Map<String, Map<String, Object>> bookingDetailCookieMap) {

        Map<String, Object> pageCountsMap = bookingDetailCookieMap.get(productName);
        if (pageCountsMap == null) {
            createEntryForProductWithPageNameForBookingDetails(productName, pageName, json, bookingDetailCookieMap);
        } else {
            if (pageCountsMap.containsKey(pageName)) {
                if (StringUtils.isNotEmpty(json)) {
                    modifyJsonCookie(pageCountsMap, json, pageName);
                }
            } else {
                Map<String, Object> bookingDetailMap = APIUtil.deserializeJsonToObject(json, HashMap.class);
                List<Object> listBookingDetailMap = new ArrayList<Object>();
                listBookingDetailMap.add(bookingDetailMap);
                pageCountsMap.put(pageName, listBookingDetailMap);

            }
            bookingDetailCookieMap.put(productName, pageCountsMap);
        }
    }

    private Cookie getApacheCookie(HttpServletRequest request) {
        Cookie apache = GenUtil.getCookieFromRequest(request, "Apache");
        return apache == null ? new Cookie("Apache", "") : apache;
    }

    /**
     * Allows test cases to override where application context obtained from.
     *
     * @param filterConfig
     *            which can be used to find the <code>ServletContext</code>
     * @return the Spring application context
     */
    protected ApplicationContext getContext(FilterConfig filterConfig) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
    }

    /**
     * Creates the count cookie.
     * <p/>
     * cookie structure: cookievalue|create-time|modified-time|checksum cookie value json: {prod1:{si:count, sr:count, br:count, ....}, prod2{},etc} si-search index page, sr-search results page,
     * br-book review page, etc
     */
    private Map<String, String> createCountCookie(String prod, String pageName, String apacheCookieValue, long hrsSinceFirstVisit) {
        String encryptedCookieValue = "";
        Map<String, String> cookieDetails = new HashMap<String, String>();
        String countCookieValue = null;
        if (StringUtils.isNotEmpty(prod) && StringUtils.isNotEmpty(pageName)) {
            try {
                Map<String, Map<String, Number>> countCookieMap = new HashMap<String, Map<String, Number>>();
                createEntryForProductWithPageName(prod, pageName, countCookieMap);
                countCookieValue = APIUtil.serializeObjectToJson(countCookieMap);
                encryptedCookieValue = generateCountCookie(apacheCookieValue, countCookieValue, getCurrentTime(), getCurrentTime(), hrsSinceFirstVisit);
            } catch (Exception e) {
                logger.error("Error while creating countCookie", e);
            }
        }
        cookieDetails.put(CT_COUNT_COOKIE_JSON, countCookieValue);
        cookieDetails.put(CT_COUNT_COOKIE_VALUE, encryptedCookieValue);
        return cookieDetails;
    }

    private String createBookingCookie(String prod, String pageName, String json, String apacheCookieValue, long hrsSinceFirstVisit) {
        String encryptedCookieValue = "";
        if (StringUtils.isNotEmpty(prod) && StringUtils.isNotEmpty(pageName)) {
            try {
                Map<String, Map<String, Object>> countCookieMap = new HashMap<String, Map<String, Object>>();
                createEntryForProductWithPageNameForBookingDetails(prod, pageName, json, countCookieMap);
                String bookingDetailCookieValue = APIUtil.serializeObjectToJson(countCookieMap);
                encryptedCookieValue = generateCountCookie(apacheCookieValue, bookingDetailCookieValue, getCurrentTime(), getCurrentTime(), hrsSinceFirstVisit);
            } catch (Exception e) {
                logger.error("Error while creating countCookie", e);
            }
        }
        return encryptedCookieValue;
    }

    public Map<String, String> modifyCountCookie(String cookieValue, String productName, String pageName, String apacheCookieValue, long hrsSinceFirstVisit) {
        try {
            String decryptedCookie = SecurityUtil.aesDecrypt(URLDecoder.decode(cookieValue, "UTF-8"), getAcubeKey());
            logger.debug("DecryptedCountCookie from modifyCountCookie(): " + decryptedCookie);
            if (StringUtils.isNotEmpty(decryptedCookie)) {
                Map<String, String> cookieDetails = new HashMap<String, String>();
                String encryptedCookie = "";
                String[] splitCookie = decryptedCookie.split("\\|", -1);
                ObjectMapper mapper = JsonUtil.getObjectMapper();
                Map<String, Map<String, Number>> countCookieMap = (Map<String, Map<String, Number>>) mapper.readValue(splitCookie[0], Map.class);
                modifyPageCount(productName, pageName, countCookieMap);
                cookieValue = APIUtil.serializeObjectToJson(countCookieMap);
                // we are doing one more split on "{" because of a bug that was introduced
                // which resulted in adding the count cookie json right after the createdDate string in the cookie
                long createdDate = Long.parseLong(splitCookie[1].split("\\{", -1)[0]);
                encryptedCookie = generateCountCookie(apacheCookieValue, cookieValue, createdDate, getCurrentTime(), hrsSinceFirstVisit);
                cookieDetails.put(CT_COUNT_COOKIE_JSON, cookieValue);
                cookieDetails.put(CT_COUNT_COOKIE_VALUE, encryptedCookie);
                return cookieDetails;
            } else {
                logger.info("Decrypted cookie is empty/null. Creating a new count cookie instead.");
                return createCountCookie(productName, pageName, apacheCookieValue, hrsSinceFirstVisit);
            }
        } catch (Exception e) {
            logger.error("Error while modifying countCookie", e);
            return createCountCookie(productName, pageName, apacheCookieValue, hrsSinceFirstVisit);
        }
    }

    private long getCurrentTime() {
        return new Date().getTime() / 1000;
    }

    private void modifyPageCount(String productName, String pageName, Map<String, Map<String, Number>> countCookieMap) {
        Map<String, Number> pageCountsMap = countCookieMap.get(productName);
        if (pageCountsMap == null) {
            createEntryForProductWithPageName(productName, pageName, countCookieMap);
        } else {
            if (pageCountsMap.containsKey(pageName)) {
                pageCountsMap.put(pageName, pageCountsMap.get(pageName).intValue() + 1);
            } else {
                pageCountsMap.put(pageName, 1);
            }
        }
        // src (searchResults{SinceLast}Confirmation) is updated for each search and is reset at confirmation page
        if ("sr".equals(pageName)) {
            modifyPageCount(productName, "src", countCookieMap);
            updateLastSearchDate(productName, countCookieMap);
        } else if ("c".equals(pageName)) {
            resetSRC(productName, countCookieMap);
            updateLastConfirmationDate(productName, countCookieMap);
        }
    }

    private void modifyJsonCookie(Map<String, Object> pageCountsMap, String json, String pageName) {

        List<Object> bookingDetailJsonObjectFromCookie = (List<Object>) pageCountsMap.get("bd");
        int maxValuesToBeStored = cachedProperties.getIntPropertyValue("ct.common.booking.detail.cookie.max.sector.values", 3);

        Map<String, Object> jsonMapToBeInserted = APIUtil.deserializeJsonToObject(json, HashMap.class);
        String jsonKey = null;
        for (String key : jsonMapToBeInserted.keySet()) {
            jsonKey = key;
        }

        for (Object listValue : bookingDetailJsonObjectFromCookie) {
            Map<String, Object> bookingDetailMap = (Map<String, Object>) listValue;
            for (String key : bookingDetailMap.keySet()) {
                if (key.equals(jsonKey)) {
                    return;
                }
            }
        }

        for (String key : jsonMapToBeInserted.keySet()) {
            String[] searchCriteriaParams = key.split("_");
            StringBuilder sb = new StringBuilder();
            String keyToBeRemoved = (sb.append(searchCriteriaParams[1]).append("_").append(searchCriteriaParams[0])).toString();
            for (int i = bookingDetailJsonObjectFromCookie.size() - 1; i >= 0; i--) {
                Map<String, Object> bookingDetailMap = (Map<String, Object>) bookingDetailJsonObjectFromCookie.get(i);
                for (String bookingDetailMapKey : bookingDetailMap.keySet()) {
                    if (bookingDetailMapKey.contains(keyToBeRemoved)) {
                        bookingDetailJsonObjectFromCookie.remove(i);
                    }
                }
            }

        }

        Collections.sort(bookingDetailJsonObjectFromCookie, new Comparator<Object>() {

            @Override
            public int compare(Object o1, Object o2) {
                Map<String, Object> bookingJson1 = (Map<String, Object>) o1;
                Map<String, Object> bookingJson2 = (Map<String, Object>) o2;
                String date1 = null;
                String date2 = null;
                SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

                for (String key : bookingJson1.keySet()) {
                    Map<String, Object> bookingJsonDetails = (Map<String, Object>) bookingJson1.get(key);
                    date1 = (String) bookingJsonDetails.get("bd");
                }

                for (String key : bookingJson2.keySet()) {
                    Map<String, Object> bookingJsonDetails = (Map<String, Object>) bookingJson2.get(key);
                    date2 = (String) bookingJsonDetails.get("bd");
                }

                Date bookingDate1 = null;
                Date bookingDate2 = null;
                try {
                    bookingDate1 = sdf.parse(date1);
                    bookingDate2 = sdf.parse(date2);
                } catch (Exception e) {
                    logger.warn("Error parsing Date");
                }
                if (bookingDate1 != null && bookingDate2 != null) {
                    if (bookingDate1.before(bookingDate2)) {
                        return -1;
                    }

                    if (bookingDate1.after(bookingDate2)) {
                        return 1;
                    }
                }
                return 0;

            }

        });

        if (maxValuesToBeStored < bookingDetailJsonObjectFromCookie.size() && maxValuesToBeStored >= 1) {
            for (int i = bookingDetailJsonObjectFromCookie.size() - maxValuesToBeStored; i >= 0; i--) {
                bookingDetailJsonObjectFromCookie.remove(i);
            }
        } else if (bookingDetailJsonObjectFromCookie.size() == maxValuesToBeStored && maxValuesToBeStored >= 1) {
            bookingDetailJsonObjectFromCookie.remove(0);
        }

        bookingDetailJsonObjectFromCookie.add(jsonMapToBeInserted);

        pageCountsMap.put("bd", bookingDetailJsonObjectFromCookie);

    }

    private void updateLastConfirmationDate(String productName, Map<String, Map<String, Number>> countCookieMap) {
        countCookieMap.get(productName).put("cd", getCurrentTime());
    }

    private void resetSRC(String productName, Map<String, Map<String, Number>> countCookieMap) {
        countCookieMap.get(productName).put("src", 0);
    }

    private void updateLastSearchDate(String productName, Map<String, Map<String, Number>> countCookieMap) {
        countCookieMap.get(productName).put("sd", getCurrentTime());
    }

    private void createEntryForProductWithPageName(String productName, String pageName, Map<String, Map<String, Number>> countCookieMap) {
        Map<String, Number> pageCountsMap = new HashMap<String, Number>();
        pageCountsMap.put(pageName, 1);
        countCookieMap.put(productName, pageCountsMap);
        if (StringUtils.isNotEmpty(pageName) && pageName.equals("sr")) {
            updateLastSearchDate(productName, countCookieMap);
        }
    }

    private void createEntryForProductWithPageNameForBookingDetails(String productName, String pageName, String json, Map<String, Map<String, Object>> countCookieMap) {
        Map<String, Object> pageCountsMap = new HashMap<String, Object>();
        Map<String, Object> bookingDetailMap = APIUtil.deserializeJsonToObject(json, HashMap.class);
        if (StringUtils.isNotEmpty(pageName) && pageName.equals("bd")) {
            List<Object> listBookingDetailMap = new ArrayList<Object>();
            listBookingDetailMap.add(bookingDetailMap);
            pageCountsMap.put(pageName, listBookingDetailMap);
        }
        countCookieMap.put(productName, pageCountsMap);
    }

    private String generateCountCookie(String apacheCookieValue, String cookieValue, long createdDate, long modifiedDate, long hoursSinceFirstVisit) throws Exception {
        StringBuilder newCountCookieValue = new StringBuilder();
        newCountCookieValue.append(cookieValue).append("|").append(createdDate).append("|").append(modifiedDate).append("|").append(hoursSinceFirstVisit);

        String checksum = getChecksum(newCountCookieValue.toString());
        newCountCookieValue.append("|").append(checksum);

        return URLEncoder.encode(SecurityUtil.aesEncrypt(newCountCookieValue.toString(), getAcubeKey()), "UTF-8");
    }

    private Cookie getCountCookie(HttpServletRequest request, String cookieName) {
        Cookie countCookie = GenUtil.getCookieFromRequest(request, cookieName);
        if (countCookie == null) {
            countCookie = new Cookie(cookieName, "");
        }
        countCookie.setPath("/");
        countCookie.setMaxAge(oneYear);
        return countCookie;
    }

    public byte[] getAcubeKey() {
        long start = System.currentTimeMillis();
        byte[] authKeyArr = new byte[32];
        System.arraycopy(defaultAuthKey, 0, authKeyArr, 0, defaultAuthKey.length);
        if (logger.isDebugEnabled()) {
            logger.debug("Get Auth Key Time = " + (System.currentTimeMillis() - start));
        }
        return authKeyArr;
    }

    public static String removeChar(String s, char c) {
        String r = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c) {
                r += s.charAt(i);
            }
        }
        return r;
    }

    /**
     * Creates the checksum from the given decrypted cookie by using SHA-1 hashing technique and returns the hex format.
     *
     * @param decryptedCookie
     *            String
     * @return String
     * @throws NoSuchAlgorithmException
     *             - NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     *             - UnsupportedEncodingException
     */
    public String getChecksum(String decryptedCookie) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String tempStr;
        String hashStr = "";
        String[] splitCookie = decryptedCookie.split("\\|", -1);
        logger.debug("DecryptedCookie from getChecksum" + decryptedCookie);

        for (int i = 0; i < splitCookie.length; i++) {
            tempStr = splitCookie[i];
            if (i < splitCookie.length - 2) {
                hashStr += tempStr + "|";
            }
            if (i == splitCookie.length - 2) {
                hashStr += tempStr;
            }
        }
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(hashStr.getBytes("UTF-8"));

        byte[] hashedArr = md.digest();
        return GenUtil.asHex(hashedArr);
    }
}
