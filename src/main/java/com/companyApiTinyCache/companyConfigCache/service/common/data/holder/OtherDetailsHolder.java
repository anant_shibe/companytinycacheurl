package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.OtherDetail;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collection;

public class OtherDetailsHolder {
    @JsonProperty("other_details")
    private Collection<OtherDetail> otherDetails;

    public OtherDetailsHolder() {
        otherDetails = new ArrayList<OtherDetail>();
    }

    /**
     * @param otherDetail
     *            the otherDetail to set
     */
    @JsonProperty("other_details")
    public void setOtherDetail(Collection<OtherDetail> otherDetail) {
        this.otherDetails = otherDetail;
    }

    /**
     * @return the otherDetail
     */
    @JsonProperty("other_details")
    public Collection<OtherDetail> getOtherDetail() {
        return otherDetails;
    }

}
