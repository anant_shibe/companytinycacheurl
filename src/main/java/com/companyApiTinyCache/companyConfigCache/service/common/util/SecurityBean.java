package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.data.DepositSplitAmount;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Role;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class SecurityBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean isLoggenIn;
    private int peopleId;
    private String name;
    private String userScreenName;
    private String emailAddress;
    private String accessRoles = null;
    private boolean isUnconfirmedUser;

    private Map<String, String> restrictedArtifacts = null;

    private Role currentUserRole;

    private int userId;
    private int affiliateId;
    private int unreadMsgCount;

    private String subDomain;
    private String companyWebsite;
    private String companyName;
    private String companyImageUrl;
    private int companyId;

    private String apiKey;
    private int depositAccountId;
    private double depositAccountBalance;
    private String depositAccountBalanceType;
    private double creditLimit;
    private HashMap<Integer, DepositSplitAmount> depositAccountSplitMap;

    private Map<String, String> allArtifacts;

    private String allRoles;

    public HashMap<Integer, DepositSplitAmount> getDepositAccountSplitMap() {
        return depositAccountSplitMap;
    }

    public void setDepositAccountSplitMap(HashMap<Integer, DepositSplitAmount> depositAccountSplitMap) {
        this.depositAccountSplitMap = depositAccountSplitMap;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Map<String, String> getRestrictedArtifacts() {
        return restrictedArtifacts;
    }

    public void setRestrictedArtifacts(Map<String, String> restrictedArtifacts) {
        this.restrictedArtifacts = restrictedArtifacts;
    }

    /**
     * use getPeopleId instead
     * 
     * @return
     */
    @Deprecated
    public int getUserId() {
        return userId;
    }

    @Deprecated
    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(int userId) {
        this.peopleId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isUserSignedIn() {
        return isLoggenIn;
    }

    public void setUserLoggenIn(boolean isLoggenIn) {
        this.isLoggenIn = isLoggenIn;
    }

    public String getUserScreenName() {
        return userScreenName;
    }

    public void setScreenName(String userScreenName) {
        this.userScreenName = userScreenName;
    }

    /**
     * @return the affiliateId
     */
    public int getAffiliateId() {
        return affiliateId;
    }

    /**
     * @param affiliateId
     *            the affiliateId to set
     */
    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    /**
     * @return the subDomain
     */
    public String getSubDomain() {
        return subDomain;
    }

    /**
     * @param subDomain
     *            the subDomain to set
     */
    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;
    }

    public boolean isUnconfirmedUser() {
        return isUnconfirmedUser;
    }

    public boolean isConfirmedUser() {
        return isLoggenIn && !isUnconfirmedUser;
    }

    public void setUnconfirmedUser(boolean isUnconfirmedUser) {
        this.isUnconfirmedUser = isUnconfirmedUser;
    }

    public String getAccessRoles() {
        return accessRoles;
    }

    public void setAccessRoles(String accessRoles) {
        this.accessRoles = accessRoles;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        if (StringUtils.isEmpty(companyWebsite))
            return;
        if (companyWebsite.startsWith("http://"))
            this.companyWebsite = companyWebsite;
        else
            this.companyWebsite = "http://" + companyWebsite;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public boolean isViewPermissible(String viewName, boolean isInclusive) {
        if (viewName != null) {
            if (restrictedArtifacts == null)
                return false;
            return (isInclusive) ? restrictedArtifacts.containsKey(viewName) : !restrictedArtifacts.containsKey(viewName);
        }
        return true;
    }

    public boolean isActionPermissible(String actionName, boolean isInclusive) {
        if (actionName != null) {
            return (isInclusive) ? restrictedArtifacts.containsKey(actionName) : !restrictedArtifacts.containsKey(actionName);
        }
        return true;
    }

    public boolean isViewPermissible(String viewName) {
        return isViewPermissible(viewName, true);
    }

    public boolean isActionPermissible(String actionName) {
        return isActionPermissible(actionName, true);
    }

    public int getCompanyId() {
        // TODO Auto-generated method stub
        return this.companyId;
    }

    public void setDepositAccountId(int depositAccountId) {
        this.depositAccountId = depositAccountId;
    }

    public int getDepositAccountId() {
        return this.depositAccountId;
    }

    public String getDepositAccountBalanceType() {
        return depositAccountBalanceType;
    }

    public void setDepositAccountBalanceType(String depositAccountBalanceType) {
        this.depositAccountBalanceType = depositAccountBalanceType;
    }

    /**
     * @return the apiKey
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * @param apiKey
     *            the apiKey to set
     */
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * @return the depositAccountBalance
     */
    public double getDepositAccountBalance() {
        return depositAccountBalance;
    }

    /**
     * @param depositAccountBalance
     *            the depositAccountBalance to set
     */
    public void setDepositAccountBalance(double depositAccountBalance) {
        this.depositAccountBalance = depositAccountBalance;
    }

    /**
     * @param companyImageUrl
     *            the companyImageUrl to set
     */
    public void setCompanyImageUrl(String companyImageUrl) {
        this.companyImageUrl = companyImageUrl;
    }

    /**
     * @return the companyImageUrl
     */
    public String getCompanyImageUrl() {
        return companyImageUrl;
    }

    /**
     * @return the currentUserRole
     */
    public Role getCurrentUserRole() {
        return currentUserRole;
    }

    /**
     * @param currentUserRole
     *            the currentUserRole to set
     */
    public void setCurrentUserRole(Role currentUserRole) {
        this.currentUserRole = currentUserRole;
    }

    /**
     * @return the unreadMsgCount
     */
    public int getUnreadMsgCount() {
        return unreadMsgCount;
    }

    /**
     * @param msgCount
     *            the msgCount to set
     */
    public void setUnreadMsgCount(int unreadMsgCount) {
        this.unreadMsgCount = unreadMsgCount;
    }

    public Map<String, String> getAllArtifacts() {
        return allArtifacts;
    }

    public void setAllArtifacts(Map<String, String> allArtifacts) {
        this.allArtifacts = allArtifacts;
    }

    public String getAllRoles() {
        return allRoles;
    }

    public void setAllRoles(String allRoles) {
        this.allRoles = allRoles;
    }
}
