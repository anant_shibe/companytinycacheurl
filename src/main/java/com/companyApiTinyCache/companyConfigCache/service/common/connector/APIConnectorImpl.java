package com.companyApiTinyCache.companyConfigCache.service.common.connector;

import com.companyApiTinyCache.companyConfigCache.service.common.util.RestResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.FileNotFoundException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Map;

/**
 * Base implementation for API connectors.
 * 
 * @author Ramesh Krishnamoorthy
 */
public class APIConnectorImpl implements APIConnector {

    private static final Log logger = LogFactory.getLog(APIConnectorImpl.class);

    private boolean proxyEnable = false; // read from the services.properties property file
    protected boolean authEnable = false; // read from the services.properties property file
    private String proxyAddr;
    private String proxyPort;
    private String proxyAuthUsername;
    private String proxyAuthPassword;

    protected String url;

    /*
     * (non-Javadoc)
     * 
     * @see com.cleartrip.whitelabel.connector.APIConnector#getRestResponse(java.lang.String, java.lang.String)
     */
    @Override
    public RestResponse getRestResponse(String url, String queryString) {
        return getRestResponse(url, queryString, null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cleartrip.whitelabel.connector.APIConnector#getRestResponse(java.lang.String, java.lang.String)
     */
    @Override
    public RestResponse getRestResponse(String url, String queryString, List<String> dontLog) {
        return getRestResponse(url, queryString, dontLog, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cleartrip.whitelabel.connector.APIConnector#getRestResponse(java.lang.String, java.lang.String, boolean)
     */
    @Override
    public RestResponse getRestResponse(String url, String queryString, List<String> dontLog, boolean setProxy) {
        return getRestResponse(url, queryString, dontLog, setProxy, null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cleartrip.common.connector.APIConnector#getRestResponse(java.lang.String, java.lang.String, boolean, java.util.Map)
     */
    @Override
    public RestResponse getRestResponse(String url, String queryString, List<String> dontLog, boolean setProxy, Map<String, String> requestHeaders) {
        RestResponse restResponse = null;
        try {
            if (proxyEnable && setProxy) {
                RestUtil.setProxy(proxyAddr, proxyPort, proxyAuthUsername, proxyAuthPassword);
            } else {
                RestUtil.removeProxy();
            }

            if (authEnable)
                RestUtil.setAuthenticator(proxyAuthUsername, proxyAuthPassword);

            restResponse = RestUtil.get(url, queryString.toString(), requestHeaders, null, dontLog, RestUtil.Char_Encoding_Type_Enum.UTF);
        } catch (SocketTimeoutException sock) {
            logger.error("Socket time out while trying to connect to " + url + " resource.", sock);
        } catch (FileNotFoundException e) {
            logger.error("File not found for " + url + " resource.", e);
            restResponse = new RestResponse();
            restResponse.setCode("404");
            return restResponse;
        } catch (Exception e) {
            logger.error("Unknown exception while trying to connect to " + url + " resource.", e);
        }
        if (restResponse == null) {
            return new RestResponse();
        }
        return restResponse;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the proxyEnable
     */
    public boolean isProxyEnable() {
        return proxyEnable;
    }

    /**
     * @param proxyEnable
     *            the proxyEnable to set
     */
    public void setProxyEnable(boolean proxyEnable) {
        this.proxyEnable = proxyEnable;
    }

    /**
     * @return the proxyAddr
     */
    public String getProxyAddr() {
        return proxyAddr;
    }

    /**
     * @param proxyAddr
     *            the proxyAddr to set
     */
    public void setProxyAddr(String proxyAddr) {
        this.proxyAddr = proxyAddr;
    }

    /**
     * @return the proxyPort
     */
    public String getProxyPort() {
        return proxyPort;
    }

    /**
     * @param proxyPort
     *            the proxyPort to set
     */
    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    /**
     * @return the proxyAuthUsername
     */
    public String getProxyAuthUsername() {
        return proxyAuthUsername;
    }

    /**
     * @param proxyAuthUsername
     *            the proxyAuthUsername to set
     */
    public void setProxyAuthUsername(String proxyAuthUsername) {
        this.proxyAuthUsername = proxyAuthUsername;
    }

    /**
     * @return the proxyAuthPassword
     */
    public String getProxyAuthPassword() {
        return proxyAuthPassword;
    }

    /**
     * @param proxyAuthPassword
     *            the proxyAuthPassword to set
     */
    public void setProxyAuthPassword(String proxyAuthPassword) {
        this.proxyAuthPassword = proxyAuthPassword;
    }
}
