package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.connector.CompanyAPIConnector;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Affiliate;
import com.companyApiTinyCache.companyConfigCache.service.common.data.AffiliateConfig;
import com.companyApiTinyCache.companyConfigCache.service.common.notification.NotificationMediator;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedVms;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class SendEmailController extends AbstractController {

    private NotificationMediator notificationMediator;
    private CompanyAPIConnector companyAPIConnector;
    private CachedVms<CommonConstants.CommonTemplate> cachedVms;
    private CachedProperties commonCachedProperties;

    public SendEmailController(NotificationMediator pnotificationMediator, CachedVms<CommonConstants.CommonTemplate> pcachedVms) {
        notificationMediator = pnotificationMediator;
        cachedVms = pcachedVms;
    }

    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String fromName = request.getParameter("fromName");
        String toEmail = request.getParameter("toEmail");
        String fromEmail = request.getParameter("fromEmail");
        String contextBody = request.getParameter("context");
        String url = request.getParameter("url");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");
        String link = request.getParameter("link_text");
        String contentType = "text/html";
        String companyId = request.getParameter("companyid");
        String productCode = request.getParameter("productCode");
        String copyAuthor = request.getParameter("copyAuthor");
        String authorEmail = fromEmail;
        String logoUrl = "";
        boolean whitelabeled = false;
        if (StringUtils.isNotBlank(companyId) & StringUtils.isNumeric(companyId)) {
            whitelabeled = true;
            Affiliate affiliate = companyAPIConnector.getCompanyByCompanyIdWithoutPeople(Integer.parseInt(companyId));
            Map<CommonEnumConstants.CompanyConfig, AffiliateConfig> configs = affiliate.getAffiliateConfigs();
            logoUrl = configs.get(CommonEnumConstants.CompanyConfig.LOGO_URL).getConfigValue();

            if (StringUtils.isNotBlank(logoUrl) && !logoUrl.contains("http")) {

                String hloc = commonCachedProperties.getPropertyValue("ct.hloc");

                String prefixURL = "http://" + hloc + ".cleartrip.com";

                if (logoUrl.startsWith("/")) {
                    logoUrl = prefixURL + logoUrl;
                } else {
                    logoUrl = prefixURL + "/" + logoUrl;
                }
            }
        }
        EnumMap<CommonConstants.CommonTemplate, Template> templates = cachedVms.getResource();
        Template t = templates.get(CommonConstants.CommonTemplate.EMAIL_CONTENT_VM);
        VelocityContext context = new VelocityContext();
        if (StringUtils.isNotBlank(productCode) && productCode.equalsIgnoreCase("CP")) {
            t = templates.get(CommonConstants.CommonTemplate.EMAIL_SELECTED_FLIGHTS_CONTENT_VM);
            String noReplyEmailJson = commonCachedProperties.getPropertyValue("ct.common.b2b.emails.from.address", "");
            Map<String, String> fromEmailMap = new HashMap<String, String>();
            if (StringUtils.isNotBlank(noReplyEmailJson)) {
                fromEmailMap = APIUtil.deserializeJsonToObject(noReplyEmailJson, Map.class);
                if (null != fromEmailMap && fromEmailMap.size() > 0 && fromEmailMap.containsKey("CORP")) {
                    fromEmail = fromEmailMap.get("CORP");
                    fromName = "";
                    context.put("authorEmail", authorEmail);
                }
            }
        }
        context.put("contextBody", contextBody);
        context.put("url", url);
        context.put("message", message);
        context.put("link", link);
        context.put("fromName", fromName);
        context.put("fromEmail", fromEmail);
        context.put("subject", subject);
        context.put("logoUrl", logoUrl);
        context.put("whitelabeled", whitelabeled);

        String emailBody = GenUtil.mergeTemplateIntoString(t, context);

        notificationMediator.transmitNotification(subject, emailBody, toEmail, fromEmail, null, null, fromName, contentType);
        if (StringUtils.isNotBlank(copyAuthor) && copyAuthor.equalsIgnoreCase("on")) {
            notificationMediator.transmitNotification(subject, emailBody, authorEmail, fromEmail, null, null, fromName, contentType);
        }

        return new ModelAndView("email_response").addObject("toEmail", toEmail);

    }

    public void setCompanyAPIConnector(CompanyAPIConnector companyAPIConnector) {
        this.companyAPIConnector = companyAPIConnector;
    }

    public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }

}
