package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.List;

public class Resource implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 2035860450429519598L;

    private List<AffiliateArtifact> artifacts;

    private int restypeId;

    private String resourceType;

    private String resourceValue;

    @JsonProperty("artifacts")
    public List<AffiliateArtifact> getArtifacts() {
        return artifacts;
    }

    @JsonProperty("artifacts")
    public void setArtifacts(List<AffiliateArtifact> artifacts) {
        this.artifacts = artifacts;
    }

    @JsonProperty("restype_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getRestypeId() {
        return restypeId;
    }

    @JsonProperty("restype_id")
    public void setRestypeId(int restypeId) {
        this.restypeId = restypeId;
    }

    @JsonProperty("resource_value")
    public String getResourceValue() {
        return resourceValue;
    }

    @JsonProperty("resource_value")
    public void setResourceValue(String resorceValue) {
        this.resourceValue = resorceValue;
    }

    @JsonProperty("resource_type")
    public String getResourceType() {
        return resourceType;
    }

    @JsonProperty("resource_type")
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Resource == false)
            return false;
        Resource res = (Resource) obj;
        if ((artifacts != null && res.getArtifacts() != null) && !GenUtil.sameCollections(artifacts, res.getArtifacts(), true)) {
            return false;
        }
        if (restypeId != res.getRestypeId()) {
            return false;
        }
        if ((resourceType != null || res.getResourceType() != null) && !GenUtil.equals(resourceType, res.getResourceType())) {
            return false;
        }
        if ((resourceValue != null || res.getResourceValue() != null) && !GenUtil.equals(resourceValue, res.getResourceValue())) {
            return false;
        }

        return true;
    }
}
