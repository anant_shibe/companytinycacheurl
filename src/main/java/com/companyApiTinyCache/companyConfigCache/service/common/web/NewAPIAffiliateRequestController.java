package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.data.NewAPIAffiliateRequestDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.notification.EmailSender;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedVms;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import com.companyApiTinyCache.companyConfigCache.service.common.org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.EnumMap;

public class NewAPIAffiliateRequestController extends SimpleFormController {

    private static final Log log = LogFactory.getLog(NewAPIAffiliateRequestController.class);

    public static final String DATE_FORMAT = "d/M/yyyy";

    private CachedProperties commonCachedProperties;

    private EmailSender emailSender;

    private CachedVms<CommonConstants.CommonTemplate> cachedVms;

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        String to = commonCachedProperties.getPropertyValue("ct.services.api.newreq.to");
        String from = commonCachedProperties.getPropertyValue("ct.services.api.newreq.from");

        NewAPIAffiliateRequestDetail requestDetail = (NewAPIAffiliateRequestDetail) command;

        EnumMap<CommonConstants.CommonTemplate, Template> templates = cachedVms.getResource();
        Template template = templates.get(CommonConstants.CommonTemplate.MAIL_NEW_API_REQUEST_SUBJECT_VM);

        VelocityContext context = new VelocityContext();
        StringBuilderWriter writer = new StringBuilderWriter(new StringBuilder(256));
        String subjectMessage = GenUtil.mergeTemplateIntoString(template, context, writer);

        template = templates.get(CommonConstants.CommonTemplate.MAIL_NEW_API_REQUEST_BODY_VM);

        context = new VelocityContext();
        context.put("request", requestDetail);
        writer = new StringBuilderWriter(new StringBuilder(256));
        String bodyMessage = GenUtil.mergeTemplateIntoString(template, context, writer);
        writer.clear();
        String[] toEmails = to.split(",");
        for (String toAddress : toEmails) {
            emailSender.sendEmail(to, from, subjectMessage, bodyMessage, null, null);
        }
        PrintWriter responseWriter = response.getWriter();
        responseWriter.println("success");
        responseWriter.close();
        return null;
    }

    /**
     * @param commonCachedProperties
     *            the commonCachedProperties to set
     */
    public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }

    /**
     * @param emailSender
     *            the emailSender to set
     */
    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    /**
     * @param cachedVms
     *            the cachedVms to set
     */
    public void setCachedVms(CachedVms<CommonConstants.CommonTemplate> cachedVms) {
        this.cachedVms = cachedVms;
    }

}
