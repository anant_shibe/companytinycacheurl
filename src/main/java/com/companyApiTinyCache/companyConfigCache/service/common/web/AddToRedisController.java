package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

/** End point to add data to redis. Developed for FlexiSearch automation. Purely no authentication, as of now.
 * post params 
 * {
      "key":"test_key",
      "value":"test_value",
      "cache_property":"properties like this, ct.services.air.memcached.servers",
      "expiry":"expiry_in_millis. If not mentioned, default is 1 day"
}
 * @author pavitra
 *
 */
@ClassExcludeCoverage
public class AddToRedisController extends AbstractController {

    private static final String EXPIRY_IN_MILLIS = "expiry_in_millis";
    private static final String CACHE_PROPERTY = "cache_property";
    private static final String VALUE = "value";
    private static final String KEY = "key";
    private static final int DEFAULT_EXPIRY = 86400;
    private Cache cache;
    private CachedProperties commonCachedProperties;
    private CacheFactory cacheFactory;

    public AddToRedisController(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int statusCode = HttpServletResponse.SC_OK;
        String responseMessage = null;
        String input = GenUtil.readAsStringString(request.getInputStream());
        try {
            if (StringUtils.isNotBlank(input)) {
                Map<String, String> inputRequestMap = APIUtil.deserializeJsonToObject(input, Map.class);
                if (MapUtils.isNotEmpty(inputRequestMap)) {
                    String key = inputRequestMap.get(KEY);
                    String value = inputRequestMap.get(VALUE);
                    String cacheProperty = inputRequestMap.get(CACHE_PROPERTY);
                    int expiryInMillis = (inputRequestMap.get(EXPIRY_IN_MILLIS) != null) ? Integer.valueOf(inputRequestMap.get(EXPIRY_IN_MILLIS)) : DEFAULT_EXPIRY;
                    if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value)) {
                        if (StringUtils.isNotBlank(cacheProperty)) {
                            cache = cacheFactory.getCacheForServer(cacheProperty);
                            if (cache != null) {
                                cache.put(key, value, expiryInMillis);
                                responseMessage = "Added data to Redis !!";
                            } else {
                                responseMessage = "Given Cache Property is wrong. Please check !!" ;
                                statusCode = HttpServletResponse.SC_BAD_REQUEST;
                            }
                        } else {
                            responseMessage = "Mention the cache property !!";
                            statusCode = HttpServletResponse.SC_BAD_REQUEST;
                        } 
                    } else {
                        responseMessage = "Key/Value is blank !!";
                        statusCode = HttpServletResponse.SC_BAD_REQUEST;
                    }
                } else {
                    responseMessage = "Input isn't Proper. Please Check !!";
                    statusCode = HttpServletResponse.SC_BAD_REQUEST;
                }
            } else {
                responseMessage = "Input is blank !!";
                statusCode = HttpServletResponse.SC_BAD_REQUEST;
            } 
        } catch (Exception e) {
            logger.error("Exception in addToRedisController. Exception is ", e);
            responseMessage = "Oops!! System is acting up";
            statusCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        } finally {
            response.setStatus(statusCode);
            PrintWriter writer = response.getWriter();
            writer.println(responseMessage);
        }
        return null;
    }

    public CacheFactory getCacheFactory() {
        return cacheFactory;
    }

    public void setCacheFactory(CacheFactory cacheFactory) {
        this.cacheFactory = cacheFactory;
    }

}
