package com.companyApiTinyCache.companyConfigCache.service.common.cache;

/**
 * Root class for cache exception hierarchy.
 * 
 * @author sdesai
 * 
 *         Date: Jan 24, 2008 Time: 01:27:03 AM
 * 
 */
public class CacheException extends RuntimeException {
    private static final long serialVersionUID = 4331660477005299958L;

    /**
     * Constructs a new exception with <code>null</code> as its detail message. The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
     */
    public CacheException() {
    }

    /**
     * Constructs a new exception with the specified detail message. The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
     * 
     * @param message
     *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
     */
    public CacheException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     * <p>
     * Note that the detail message associated with <code>cause</code> is <i>not</i> automatically incorporated in this exception's detail message.
     * 
     * @param message
     *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     * @param cause
     *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.)
     * @since 1.4
     */
    public CacheException(String message, Throwable cause) {
        super(message, cause);
    }
}
