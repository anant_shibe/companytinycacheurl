package com.companyApiTinyCache.companyConfigCache.service.common.notification;

/**
 * Exception class for message formatter.
 * 
 * @author sanjeev
 * 
 */
public class MessageFormatException extends Exception {

    /**
     * Generated Serial version uid.
     */
    private static final long serialVersionUID = 8261285701484330784L;

    /**
     * Constructor.
     * 
     * @param message
     *            the message
     * @param e
     *            the root cause
     */
    public MessageFormatException(String message, Exception e) {
        super(message, e);
    }

}
