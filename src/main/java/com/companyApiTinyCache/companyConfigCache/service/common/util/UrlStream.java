package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

/**
 * A utility class to Open a Stream from URL. RestClient can be used to directly read the contents, but this is better when the data is large (in mbs).
 * 
 * This wrapper is written mainly so that it can be mocked for Test Cases.
 * 
 * @author suresh
 * 
 */
@ClassExcludeCoverage
public class UrlStream {

    private static final Log logger = LogFactory.getLog(UrlStream.class);

    public InputStream getInputStream(String loc) {
        InputStream ip = null;
        try {
            URL url = new URL(loc);
            ip = new BufferedInputStream(url.openStream());
        } catch (Exception e) {
            logger.info("Error opening URL: " + loc + ". Returning null");
        }
        return ip;
    }

    public Reader getReader(String loc) {
        Reader r = null;
        InputStream ip = getInputStream(loc);
        if (ip != null) {
            r = new InputStreamReader(ip);
        }
        return r;
    }

}
