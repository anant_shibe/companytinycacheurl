package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.ApiType;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.DirectionType;
import com.google.common.collect.Maps;
import jersey.repackaged.com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 *
 */
public class ConnectorStats extends BaseStats {

    private static final long serialVersionUID = 2122599197789909462L;

    private static final Log logger = LogFactory.getLog(ConnectorStats.class);

    private String name;
    private String nameAlias;

    private long callIn;

    private long callOut;

    private boolean success;

    protected List<ApiCallStats> apiCallStats  = Lists.newArrayList();;

    protected Map<String, ApiCallStats> apiCallStatsV1 = Maps.newHashMap();


    protected List<DBStats> dbStats;
    
    protected List<InfoMessage> infoMessages;

    public List<DBStats> getDbStats() {
		return dbStats;
	}

	public void setDbStats(List<DBStats> dbStats) {
		this.dbStats = dbStats;
	}

	private DirectionType currentDirectionType;

    private long fci;

    private long fco;

    public String getConnectorSearchType() {
        return connectorSearchType;
    }

    public void setConnectorSearchType(String connectorSearchType) {
        this.connectorSearchType = connectorSearchType;
    }

    private String connectorSearchType;

	private List<InfoMessage> genericMessages;

    public ConnectorStats() {
        callIn = System.currentTimeMillis();
    }

    /**
     * Returns null if all cached, ONWARD, RETURN, ROUNDTRIP depending on ONWARD only RETURN only or both ONWARD and RETURN api calls.
     *
     * @return DirectionType of Api calls
     */
    public DirectionType evalApiCallStatus() {
        int i, numCallStats = 0;
        DirectionType direction = null;
        if (apiCallStats != null) {
            numCallStats = apiCallStats.size();
        }
        for (i = 0; i < numCallStats && direction != DirectionType.ROUNDTRIP; i++) {
            ApiCallStats callStats = apiCallStats.get(i);
            if ("CACHED".equals(callStats.getUrl())) {
                continue;
            }
            DirectionType callDirection = callStats.getDirectionType();
            if (callDirection == null) {
                callDirection = DirectionType.ROUNDTRIP;
            }
            if (direction == null) {
                direction = callDirection;
            } else if (direction != callDirection) {
                direction = DirectionType.ROUNDTRIP;
            }
        }

        return direction;
    }

    public static List<ApiCallStats> addNewApiRequest(List<ApiCallStats> apiStats, boolean isLogRequestResponse, ApiType apiType, DirectionType directionType, String apiName, String supplier,
            String url, String request) {
       return addNewApiRequest(apiStats, isLogRequestResponse, apiType, directionType, apiName, supplier, url, request, null);
    }

    public void open( String callStatId, ApiType apiType, DirectionType directionType, String apiName, String supplier, String url, String request) {
        ApiCallStats apiStat = new ApiCallStats();
        apiStat.setApiType(apiType);
        apiStat.setApiName(apiName);
        apiStat.setSupplier(supplier);
        apiStat.setDirectionType(directionType);
        apiStat.setUrl(url);
        apiStat.setSuccess(false);
        apiStat.setRequest(request);
        apiStat.setCtStep(getCtStep());
        apiCallStatsV1.put(callStatId, apiStat);
    }

    public void closeSuccess(String callStatId, String response, String itiId, int retryCount, int responseCode) {
        try {
            ApiCallStats apiStat = apiCallStatsV1.get(callStatId);
            apiStat.setSuccess(true);
            apiStat.setCallOut(System.currentTimeMillis());
            apiStat.setResponseCode(responseCode);
            int size = 0;
            if (response != null) {
                size = response.length();
            }
            apiStat.setResponseSize(size);
            apiStat.setResponse(response);
            apiStat.setItineraryId(itiId);
            apiStat.setRetryCount(retryCount);
            apiStat.setSuccess(true);
            apiCallStats.add(apiStat);
            apiCallStatsV1.remove(callStatId);
        } catch (Exception e) {
            logger.error("Error while closing api call stats ", e);
        }
    }

    public void closeSuccess(String callStatId, String response, String itiId, int retryCount) {
        closeSuccess(callStatId, response, itiId, retryCount, 200);
    }

    public void closeFailure(String callStatId, String response, String errorMsg, ErrorCategory errorCategory, ErrorSeverity severity, String itiId, int retryCount) {
        closeFailure(callStatId, response, errorMsg, errorCategory, severity, itiId, retryCount, 500);
    }

    public void closeFailure(String callStatId, String response, String errorMsg, ErrorCategory errorCategory, ErrorSeverity severity, String itiId, int retryCount, int responseCOde) {
       try {
           ApiCallStats apiStat = apiCallStatsV1.get(callStatId);
           apiStat.setCallOut(System.currentTimeMillis());
           apiStat.setResponseCode(responseCOde);
           int size = 0;
           if (response != null) {
               size = response.length();
           }
           apiStat.setResponseSize(size);
           apiStat.setResponse(response);
           apiStat.setErrorMessage(errorMsg);
           apiStat.setErrorCategory(errorCategory);
           apiStat.setSeverity(severity);
           apiStat.setItineraryId(itiId);
           apiStat.setRetryCount(retryCount);
           apiStat.setSuccess(false);
           apiCallStats.add(apiStat);
           apiCallStatsV1.remove(callStatId);
       } catch (Exception e) {
           logger.error("Error while closing api call stats ", e);
       }
    }

    public static List<ApiCallStats> addNewApiRequest(List<ApiCallStats> apiStats, boolean isLogRequestResponse, ApiType apiType, DirectionType directionType, String apiName, String supplier,
            String url, String request, String hostIp) {
        ApiCallStats apiStat = new ApiCallStats();
        apiStat.setApiType(apiType);
        apiStat.setApiName(apiName);
        apiStat.setSupplier(supplier);
        apiStat.setDirectionType(directionType);
        apiStat.setUrl(url);
        apiStat.setSuccess(false);
        apiStat.setHostIp(hostIp);
        if (isLogRequestResponse) {
            apiStat.setRequest(request);
        }

        if (apiStats == null) {
            apiStats = new ArrayList<ApiCallStats>(6);
        }
        apiStats.add(apiStat);

        return apiStats;
    }

    public static void closeApiRequest(List<ApiCallStats> apiStats, boolean isLogRequestResponse, ApiType apiType, String apiName, int responseCode, String response, boolean isSuccess) {
        ApiCallStats apiStat = null;
        int size;
        String msg = null;
        if (apiStats != null && (size = apiStats.size()) > 0) {
            apiStat = apiStats.get(size - 1);
            if (apiStat.getApiType() != apiType || !StringUtils.equals(apiStat.getApiName(), apiName)) {
                msg = "apiType, apiName of closeApiRequest " + apiType + ' ' + apiName + " does not match the recent call to newApiRequest " + apiStat.getApiType() + ' ' + apiStat.getApiName();
            }
        } else {
            msg = "closeApiRequest with apiType, apiName " + apiType + ' ' + apiName + " made without newApiRequest";
        }
        if (msg == null) {
            apiStat.setSuccess(isSuccess);
            apiStat.setCallOut(System.currentTimeMillis());
            apiStat.setApiType(apiType);
            apiStat.setApiName(apiName);
            apiStat.setResponseCode(responseCode);
            size = 0;
            if (response != null) {
                size = response.length();
            }
            apiStat.setResponseSize(size);
            if (isLogRequestResponse) {
                apiStat.setResponse(response);
            }
            apiStat.setSuccess(responseCode == 200 && isSuccess);
        } else {
            logger.error(msg);
        }
    }

    public static List<ApiCallStats> addCachedApiStat(List<ApiCallStats> apiStats, ApiType apiType, DirectionType directionType, String apiName, String supplier, String cacheKey, long callIn) {
        ApiCallStats apiStat = new ApiCallStats(callIn, System.currentTimeMillis());
        apiStat.setApiType(apiType);
        if (apiName != null) {
            apiStat.setApiName(apiName);
        }
        apiStat.setSupplier(supplier);
        apiStat.setCacheKey(cacheKey);
        apiStat.setDirectionType(directionType);
        apiStat.setSuccess(true);
        if (apiStats == null) {
            apiStats = new ArrayList<ApiCallStats>(6);
        }
        apiStats.add(apiStat);

        return apiStats;
    }

    public void addFailureStats(ApiType apiType, CtStep ctStep,  DirectionType directionType, String supplier, String errorMsg, ErrorCategory errorCategory, int retryCount, ErrorSeverity severity, String itiId){
        ApiCallStats apiStat = new ApiCallStats();
        apiStat.setApiType(apiType);
        apiStat.setCtStep(ctStep);
        apiStat.setDirectionType(directionType);
        apiStat.setSupplier(supplier);
        apiStat.setErrorCategory(errorCategory);
        apiStat.setErrorMessage(errorMsg);
        apiStat.setSeverity(severity);
        apiStat.setSuccess(false);
        apiStat.setRetryCount(retryCount);
        apiStat.setItineraryId(itiId);
        apiStat.setResponseCode(500);
        apiCallStats.add(apiStat);
    }


    public void addNewApiRequest(ApiType apiType, DirectionType directionType, String apiName, String supplier, String url, String request, String credentialInfo) {
        apiCallStats = addNewApiRequest(apiCallStats, logRequestResponse, apiType, directionType, apiName, supplier, url, request);
        if (apiCallStats != null && apiCallStats.size() > 0) {
            ApiCallStats apiCallStat = apiCallStats.get(apiCallStats.size() - 1);
            apiCallStat.setCredentialInfo(credentialInfo);
        }
    }

    public void addNewApiRequest(ApiType apiType, DirectionType directionType, String apiName, String supplier, String url, String request) {
        if (directionType == null) {
            directionType = currentDirectionType;
        }

        apiCallStats = addNewApiRequest(apiCallStats, logRequestResponse, apiType, directionType, apiName, supplier, url, request);
        if (apiCallStats != null && apiCallStats.size() > 0 && this.connectorSearchType != null) {
            ApiCallStats apiCallStat = apiCallStats.get(apiCallStats.size() - 1);
            apiCallStat.setConnectorSearchType(this.connectorSearchType);
        }
    }
    
    public void addNewApiRequestWithHostIp(ApiType apiType, DirectionType directionType, String apiName, String supplier, String url, String request) {
        if (directionType == null) {
            directionType = currentDirectionType;
        }
        apiCallStats = addNewApiRequest(apiCallStats, logRequestResponse, apiType, directionType, apiName, supplier, url, request, GenUtil.getLocalIpAddress());
    }
    public void addNewApiRequest(ApiType apiType, DirectionType directionType, String apiName, String url, String request) {
        addNewApiRequest(apiType, directionType, apiName, null, url, request);
    }

    public void closeApiRequest(ApiType apiType, String apiName, int responseCode, String response, boolean isSuccess) {
        closeApiRequest(apiCallStats, logRequestResponse, apiType, apiName, responseCode, response, isSuccess);
    }
    
    public void closeApiRequest(ApiType apiType, String apiName, RestResponse response) {
        closeApiRequest(apiCallStats, logRequestResponse, apiType, apiName, response.getStatus(), response.getContent(), GenUtil.hasSuccessCode(response.getStatus()));
    }
    
    public void closeApiRequestWithError(ApiType apiType, String apiName) {
        closeApiRequest(apiCallStats, logRequestResponse, apiType, apiName, 500, "", false);
    }

    public void addCachedApiStat(ApiType apiType, DirectionType directionType, String apiName, String supplier, String cacheKey, long ctCallIn) {
        apiCallStats = addCachedApiStat(apiCallStats, apiType, directionType, apiName, supplier, cacheKey, ctCallIn);
    }

    /**
     * Getter for name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for name.
     *
     * @param pname
     *            the name to set
     */
    public void setName(String pname) {
        name = pname;
    }

    /**
     * Getter for callIn.
     *
     * @return the callIn
     */
    public long getCallIn() {
        return callIn;
    }

    /**
     * Setter for callIn.
     *
     * @param pcallIn
     *            the callIn to set
     */
    public void setCallIn(long pcallIn) {
        callIn = pcallIn;
    }

    /**
     * Getter for callOut.
     *
     * @return the callOut
     */
    public long getCallOut() {
        return callOut;
    }

    /**
     * Setter for callOut.
     *
     * @param pcallOut
     *            the callOut to set
     */
    public void setCallOut(long pcallOut) {
        callOut = pcallOut;
    }

    /**
     * Getter for apiCallStats.
     *
     * @return the apiCallStats
     */
    public List<ApiCallStats> getApiCallStats() {
        return apiCallStats;
    }

    /**
     * Setter for apiCallStats.
     *
     * @param papiCallStats
     *            the apiCallStats to set
     */
    public void setApiCallStats(List<ApiCallStats> papiCallStats) {
        apiCallStats = papiCallStats;
    }

    /**
     * Setter for currentDirectionType.
     *
     * @param pcurrentDirectionType
     *            the currentDirectionType to set
     */
    public void setCurrentDirectionType(DirectionType pcurrentDirectionType) {
        currentDirectionType = pcurrentDirectionType;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setFirstCallIn(long fci) {
    	if (this.fci != 0) {
    		logger.warn(String.format("Stats discrepancy found in ConnectorStats.setFirstCallIn - Non zero value found for fci -  Old value: %s, New value: %s", this.fci, fci));
    	}
    	this.fci = fci;
    }

    public long getFirstCallIn() {
        return fci;
    }

    public void setFinalCallOut(long fco) {
    	if (this.fco != 0) {
    		logger.warn(String.format("Stats discrepancy found in ConnectorStats.setFirstCallOut - Non zero value found for fco -  Old value: %s, New value: %s", this.fco, fco));
    	}
    	this.fco = fco;
    }

    public long getFinalCallOut() {
        return fco;
    }
    
    public static void closeDBStat(DBStats dbStats) {
		dbStats.setCallOut(System.currentTimeMillis());
	}

	public String getNameAlias() {
		if (nameAlias == null) {
			return name;
		} else {
			return nameAlias;
		}
	}

	public void setNameAlias(String nameAlias) {
		this.nameAlias = nameAlias;
	}
	
	public List<InfoMessage> getGenericMessages() {
		return genericMessages;
	}

	public void addGenericStats(InfoMessage message) {
		if (this.genericMessages == null) {
			this.genericMessages = new ArrayList<InfoMessage>();
		}
		
		this.genericMessages.add(message);
	}
	
	public void addGenericStats(String supplier, String errorCode, String message) {
		this.addGenericStats(new InfoMessage(supplier, errorCode, message));
	}
	
    public ApiCallStats addNewApiRequestAndReturn(ApiType apiType, DirectionType directionType, String apiName, String supplier, String url, String request) {
    	if (directionType == null) {
            directionType = currentDirectionType;
        }
        ApiCallStats apiStat = new ApiCallStats();
        apiStat.setApiType(apiType);
        apiStat.setApiName(apiName);
        apiStat.setSupplier(supplier);
        apiStat.setDirectionType(directionType);
        apiStat.setUrl(url);
        apiStat.setSuccess(false);
        apiStat.setHostIp(GenUtil.getLocalIpAddress());
        
        if (isLogRequestResponse()) {
            apiStat.setRequest(request);
        }
        
        if (apiCallStats == null) {
        	apiCallStats = new ArrayList<ApiCallStats>(6);
        }
        
        apiCallStats.add(apiStat);
        return apiStat;
    }
    
    public void closeApiRequest(ApiCallStats apiCallStat, int responseCode, String response, boolean isSuccess) {
        if (apiCallStat != null ) {
        	apiCallStat.setSuccess(isSuccess);
            apiCallStat.setCallOut(System.currentTimeMillis());
            apiCallStat.setResponseCode(responseCode);
            apiCallStat.setResponseSize(response.length());
            
            if (isLogRequestResponse()) {
            	apiCallStat.setResponse(response);
            }

            apiCallStat.setSuccess(responseCode == 200 && isSuccess);
        }
    }
}
