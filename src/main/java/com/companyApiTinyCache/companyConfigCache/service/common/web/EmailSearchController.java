package com.companyApiTinyCache.companyConfigCache.service.common.web;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public class EmailSearchController extends AbstractController {

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String context = request.getParameter("context");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");
        String url = request.getParameter("url");
        ModelAndView model = new ModelAndView("email").addObject("message", message).addObject("context", context).addObject("subject", subject).addObject("url", url);
        String sender = StringUtils.trimToNull(request.getParameter("sender"));
        String senderEmail = StringUtils.trimToNull(request.getParameter("sender_email"));
        if (sender != null) {
            model.addObject("sender", sender);
        }
        if (senderEmail != null) {
            model.addObject("senderEmail", senderEmail);
        }
        String companyId = request.getParameter("companyid");

        if (StringUtils.isNotBlank(companyId)) {
            model.addObject("companyid", companyId);
        }

        String productCode = request.getParameter("productCode");
        if (StringUtils.isNotBlank(productCode)) {
            model.addObject("productCode", productCode);
        }
        return model;
    }
}
