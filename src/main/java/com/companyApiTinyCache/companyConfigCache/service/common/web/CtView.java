package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.SecurityUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.servlet.View;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public class CtView implements View {

    private String context;

    private String path;

    public CtView(String pcontext, String ppath) {
        context = pcontext;
        path = ppath;
    }

    @Override
    public String getContentType() {
        return "text/html";
    }

    @Override
    public void render(Map map, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (map != null) {
            for (Object keyObject : map.keySet()) {
                Object value = map.get(keyObject);
                request.setAttribute(keyObject.toString(), value);
            }
        }
        // Set the locale for fmt: jstl tag in case Accept-Language is missing
        // from request header
        if (request.getHeader("Accept-Language") == null) {
            request.setAttribute("javax.servlet.jsp.jstl.fmt.locale.request", Locale.getDefault().toString());
        }
        String lang = (String) request.getAttribute("lang");
        if (lang == null) {
            lang = "en";
        }
        RequestDispatcher dispatcher = null;
        String pathWithLang = path.replace("${lang}", lang);
        if (context != null) {
            ServletContext servletContext = (ServletContext) request.getAttribute("servletContext");
            dispatcher = servletContext.getContext(context).getRequestDispatcher(pathWithLang);
        } else {
            dispatcher = request.getRequestDispatcher(pathWithLang);
        }
        dispatcher.include(request, response);

    }

    public Map<String, String> getDomainSpecificUIParameters(HttpServletRequest request) {
        String country = "IN";
        try {
            country = SecurityUtil.getCountryFromDomain(request);
            country = country.toUpperCase();
        } catch (MalformedURLException e) {
            country = "IN";
        }
        Map<String, String> domainParameters = null;
        try {

            // domainParameters = getDomainParameters(country);

            if (country.equalsIgnoreCase("IN")) {
                domainParameters = new LinkedHashMap<String, String>();
                domainParameters.put("flights", "true");
                domainParameters.put("hotels", "true");
                domainParameters.put("trains", "false");
                domainParameters.put("more", "true");
                domainParameters.put("more-airfare-calendar", "true");
                domainParameters.put("more-airfare-graphs", "true");
                domainParameters.put("more-mobile-search", "true");
                domainParameters.put("more-print-etickets", "true");
                domainParameters.put("more-check-flight-status", "true");
                domainParameters.put("more-trains-pnr-status", "false");
                domainParameters.put("more-trains-calendar", "false");
                domainParameters.put("more-hotel-directory", "true");
                domainParameters.put("more-india-hotels", "false");
                domainParameters.put("more-small-world", "true");
            } else {
                domainParameters = new LinkedHashMap<String, String>();
                domainParameters.put("flights", "true");
                domainParameters.put("hotels", "true");
                domainParameters.put("trains", "false");
                domainParameters.put("more", "true");
                domainParameters.put("more-airfare-calendar", "true");
                domainParameters.put("more-airfare-graphs", "true");
                domainParameters.put("more-mobile-search", "true");
                domainParameters.put("more-print-etickets", "true");
                domainParameters.put("more-check-flight-status", "true");
                domainParameters.put("more-trains-pnr-status", "false");
                domainParameters.put("more-trains-calendar", "false");
                domainParameters.put("more-hotel-directory", "true");
                domainParameters.put("more-india-hotels", "false");
                domainParameters.put("more-small-world", "true");
            }

        } catch (Exception e) {

        }

        return domainParameters;
    }

    public Map<String, String> getDomainParameters(String country) throws JsonParseException, JsonMappingException, IOException {
        Map<String, String> domainParameters = new LinkedHashMap<String, String>();
        String json = null;
        // json = commonCachedProperties.getPropertyValue("ct.appdata.domain.ui.parameters");
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        Map<String, Object> map = (Map<String, Object>) mapper.readValue(json, Map.class);
        domainParameters = (Map<String, String>) map.get(country);
        return domainParameters;
    }

}
