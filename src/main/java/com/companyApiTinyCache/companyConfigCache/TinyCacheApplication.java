package com.companyApiTinyCache.companyConfigCache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
class CompanyConfigCacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanyConfigCacheApplication.class, args);
	}

}
