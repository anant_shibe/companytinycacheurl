package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The ApplicationContextFactory presents a factory implementation for representing singleton Spring application context (www.springframework.org). Contexts are loaded by name and stored for reuse
 * until the class leaves context.
 * 
 * @author Amlan Roy
 */
public class ServicesApplicationContext implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext appContext) throws BeansException {
        applicationContext = appContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }

}
