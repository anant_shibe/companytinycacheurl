package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@ClassExcludeCoverage
public class CachedResourceInfoController extends AbstractController {

    private static final Log logger = LogFactory.getLog(CachedResourceInfoController.class);

    public CachedResourceInfoController() {
        setCacheSeconds(0);
    }

    private ApplicationContext getContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.mvc.AbstractController#handleRequestInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String beanName = request.getParameter("beanName");
        ApplicationContext ctx = getContext();
        CachedResource cachedResource = (CachedResource) ctx.getBean(beanName);
        Map<String, Object> responseMap = new HashMap<String, Object>();
        responseMap.put("digest", cachedResource.getDigest());
        responseMap.put("lct", cachedResource.getLastCheckTimestamp());
        responseMap.put("lrt", cachedResource.getLastRefreshTimestamp());
        response.setContentType("text/json");
        PrintWriter writer = response.getWriter();
        try {
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            JsonFactory jsonFactory = new JsonFactory();
            JsonGenerator jgen = jsonFactory.createGenerator(writer);
            jgen.useDefaultPrettyPrinter();
            mapper.writeValue(jgen, responseMap);
        } catch (Exception e) {
            logger.error("Error Converting to Json for Server Registry", e);
        }
        return null;
    }

}
