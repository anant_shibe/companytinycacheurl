package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.service.PromoServiceInvoker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ClassExcludeCoverage
public class KafkaBrokerRefreshController extends AbstractController {
	private PromoServiceInvoker promoServiceInvoker;
	private static final Log LOG = LogFactory.getLog(KafkaBrokerRefreshController.class);


	public void setPromoServiceInvoker(PromoServiceInvoker promoServiceInvoker) {
		this.promoServiceInvoker = promoServiceInvoker;
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setContentType("text/xml");
		try {
			promoServiceInvoker.init();
			response.setStatus(HttpServletResponse.SC_OK);
			ServletOutputStream op = response.getOutputStream();
			op.println("Refresh Successful");
		} catch (Exception exception) {
			LOG.error("Error during refresh ::" + exception.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			ServletOutputStream op = response.getOutputStream();
			op.println("Error during kafka refresh :::" + exception.getMessage());
		}
		return null;
	}

}
