package com.companyApiTinyCache.companyConfigCache.service.common.web.filter.ss2;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ning.http.client.*;
import com.ning.http.client.AsyncHttpClientConfig.Builder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Random;
import java.util.UUID;

/**
 * SS2.
 * @author ss2
 */
@ClassExcludeCoverage
public class SS2 {

    private static final Log logger = LogFactory.getLog(SS2.class);

    private CachedProperties commonCachedProperties;

    private static String allowExp = "-1";
    private static String staticResource = "-2";
    private static String allow = "0";
    private static Builder builder;
    private static AsyncHttpClient asyncHttpClient;

    public SS2(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
        initializeBuilder();
    }

    public JsonNode shieldsquareValidateRequest(HttpServletRequest req, HttpServletResponse resp, String shieldsquareTid, String shieldsquareUsername, int shieldsquareCalltype) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode ssreqJsonObj = mapper.createObjectNode();
        ObjectNode ss2respJson = mapper.createObjectNode();

        /*
         * Reason for change : For D3.2 Release, to filter out the static resource requests to SS2 server based on the customer choice to filter or not
         */

        boolean isFilterReqd = commonCachedProperties.getBooleanPropertyValue(ConnConstants.IS_REQD_STATIC_FILTER, false);
        String reqfilter = commonCachedProperties.getPropertyValue(ConnConstants.FILTER_REQ_TYPE);
        boolean isStatic = false;

        if (isFilterReqd) {
            isStatic = req.getRequestURI().matches(".*\\.(" + reqfilter + ")");
        }

        if (!isStatic) {
            ssreqJsonObj.put("_zpsbd0", false);
            ssreqJsonObj.put("_zpsbd1", "");
            ssreqJsonObj.put("_zpsbd2", "");
            ssreqJsonObj.put("_zpsbd2", "");
            ssreqJsonObj.put("_zpsbd3", "");
            ssreqJsonObj.put("_zpsbd4", "");
            ssreqJsonObj.put("_zpsbd5", "");
            ssreqJsonObj.put("_zpsbd6", "");
            ssreqJsonObj.put("_zpsbd7", "");
            ssreqJsonObj.put("_zpsbd8", 0);
            ssreqJsonObj.put("_zpsbd9", "");
            ssreqJsonObj.put("_zpsbda", 0);
            ssreqJsonObj.put("__uzma", "");
            ssreqJsonObj.put("__uzmb", 0);
            ssreqJsonObj.put("__uzmc", "");
            ssreqJsonObj.put("__uzmd", 0);

            ss2respJson.put("pid", "");
            ss2respJson.put("responsecode", 0);
            ss2respJson.put("reason", "");
            ss2respJson.put("url", commonCachedProperties.getPropertyValue(ConnConstants.JS_DATA_URL));

            int shieldsquarLow = 10000;
            int shieldsquareHigh = 99999;
            int shieldsquareA = 1;
            int shieldsquareB = 3;
            int shieldsquareC = 7;
            int shieldsquareD = 1;
            int shieldsquareE = 5;

            long shieldsquareTime = System.currentTimeMillis() / 1000;

            String shieldsquareLastaccesstime = null;
            String reqIpAddr = null;
            // Default false for Monitor Mode
            Boolean isSync = false;

            try {
                if (commonCachedProperties.getPropertyValue(ConnConstants.IP_ADDRESS).equalsIgnoreCase("REMOTE_ADDR")) {
                    reqIpAddr = req.getRemoteAddr();
                } else {
                    reqIpAddr = req.getHeader(commonCachedProperties.getPropertyValue(ConnConstants.IP_ADDRESS));
                }

                logger.debug("IP Logged: " + reqIpAddr);

                if (shieldsquareTid.isEmpty()) {
                    shieldsquareTid = shieldsquareGenerateTid(reqIpAddr, commonCachedProperties.getPropertyValue(ConnConstants.SID));
                }
                Cookie[] cookies = req.getCookies();
                boolean cookieAA = false;
                boolean cookieBB = false;
                boolean cookieDD = false;
                String name = null;
                String value = null;
                String shieldsquareUzmc = "0";

                if (cookies != null) {
                    for (int i = 0; i < cookies.length; i++) {
                        name = cookies[i].getName();
                        value = cookies[i].getValue();
                        if (name != null) {
                            if (name.equals("__uzmd")) {
                                cookieDD = true;
                                shieldsquareLastaccesstime = value;
                                ssreqJsonObj.put("__uzmd", Long.parseLong(shieldsquareLastaccesstime));
                            } else if (name.equals("__uzma")) {
                                cookieAA = true;
                                ssreqJsonObj.put("__uzma", value);
                            } else if (name.equals("__uzmb")) {
                                cookieBB = true;
                                ssreqJsonObj.put("__uzmb", Long.parseLong(value));
                            } else if (name.equals("__uzmc")) {
                                shieldsquareUzmc = value;
                                ssreqJsonObj.put("__uzmc", shieldsquareUzmc);
                            }
                        }
                    }
                }
                Random random = new Random();
                if (cookieAA && cookieBB && cookieDD) {
                    shieldsquareUzmc = shieldsquareUzmc.substring(shieldsquareE, shieldsquareUzmc.length() - shieldsquareE);
                    shieldsquareA = (Integer.parseInt(shieldsquareUzmc) - shieldsquareC) / shieldsquareB + shieldsquareD;
                    int randomnumLow = random.nextInt(shieldsquareHigh - shieldsquarLow) + shieldsquarLow;
                    int randomnumLow1 = random.nextInt(shieldsquareHigh - shieldsquarLow) + shieldsquarLow;
                    shieldsquareUzmc = randomnumLow + Integer.toString(shieldsquareC + shieldsquareA * shieldsquareB) + randomnumLow1;

                    Cookie cookieC = new Cookie("__uzmc", shieldsquareUzmc);
                    cookieC.setMaxAge(3600 * 24 * 365 * 10);
                    resp.addCookie(cookieC);

                    Cookie cookieD = new Cookie("__uzmd", Long.toString(shieldsquareTime));
                    cookieD.setMaxAge(3600 * 24 * 365 * 10);
                    resp.addCookie(cookieD);

                } else {
                    int randomnum = random.nextInt(9999 - 1000) + 1000;
                    UUID uuid = UUID.randomUUID();
                    String randomUUIDStr = uuid.toString();
                    String shieldsquareUzma = "ma" + randomUUIDStr + randomnum;
                    shieldsquareLastaccesstime = Long.toString(shieldsquareTime);
                    int randomnumLow = random.nextInt(shieldsquareHigh - shieldsquarLow) + shieldsquarLow;
                    int randomnumLow1 = random.nextInt(shieldsquareHigh - shieldsquarLow) + shieldsquarLow;
                    shieldsquareUzmc = randomnumLow + Integer.toString(shieldsquareC + shieldsquareA * shieldsquareB) + randomnumLow1;

                    Cookie cookieC = new Cookie("__uzmc", shieldsquareUzmc);
                    Cookie cookieA = new Cookie("__uzma", shieldsquareUzma);
                    Cookie cookieB = new Cookie("__uzmb", shieldsquareLastaccesstime);
                    Cookie cookieD = new Cookie("__uzmd", shieldsquareLastaccesstime);
                    cookieA.setMaxAge(3600 * 24 * 365 * 10);
                    cookieB.setMaxAge(3600 * 24 * 365 * 10);
                    cookieC.setMaxAge(3600 * 24 * 365 * 10);
                    cookieD.setMaxAge(3600 * 24 * 365 * 10);
                    resp.addCookie(cookieA);
                    resp.addCookie(cookieB);
                    resp.addCookie(cookieC);
                    resp.addCookie(cookieD);

                    ssreqJsonObj.put("__uzma", shieldsquareUzma);
                    ssreqJsonObj.put("__uzmb", shieldsquareTime);
                    ssreqJsonObj.put("__uzmc", shieldsquareUzmc);
                    ssreqJsonObj.put("__uzmd", Long.parseLong(shieldsquareLastaccesstime));
                }
                if (commonCachedProperties.getPropertyValue(ConnConstants.MODE).equalsIgnoreCase("Active") && (shieldsquareCalltype != 4 && shieldsquareCalltype != 5)) {
                    isSync = true;
                }
                ssreqJsonObj.put("_zpsbd0", isSync);
                ssreqJsonObj.put("_zpsbd1", commonCachedProperties.getPropertyValue(ConnConstants.SID));
                ssreqJsonObj.put("_zpsbd2", shieldsquareTid);
                ssreqJsonObj.put("_zpsbd3", req.getHeader("referer") != null ? req.getHeader("referer") : "");

                // using getAttribute allows us to get the orginal url out of
                // the page when a forward has taken place.
                String queryString = "?" + req.getAttribute("javax.servlet.forward.query_string");
                String requestURI = "" + req.getAttribute("javax.servlet.forward.request_uri");
                if (requestURI.equals("null")) {
                    // using getAttribute allows us to get the orginal url out
                    // of the page when a include has taken place.
                    queryString = "?" + req.getAttribute("javax.servlet.include.query_string");
                    requestURI = "" + req.getAttribute("javax.servlet.include.request_uri");
                }
                if (requestURI.equals("null")) {
                    queryString = "?" + req.getQueryString();
                    requestURI = req.getRequestURI();
                }
                if (queryString.equals("?null")) {
                    queryString = "";
                }
                String reqURL = requestURI + queryString;

                ssreqJsonObj.put("_zpsbd4", reqURL != null ? reqURL : "");
                String sessId = "";
                if (commonCachedProperties.getPropertyValue(ConnConstants.SESS_ID).equalsIgnoreCase("JSESSIONID")) {
                    sessId = req.getSession().getId() != null ? req.getSession().getId() : "";
                } else {
                    if (req.getParameter(commonCachedProperties.getPropertyValue(ConnConstants.SESS_ID)) != null) {
                        sessId = req.getParameter(commonCachedProperties.getPropertyValue(ConnConstants.SESS_ID));
                    }
                }
                ssreqJsonObj.put("_zpsbd5", sessId);
                ssreqJsonObj.put("_zpsbd6", reqIpAddr != null ? reqIpAddr : "");
                ssreqJsonObj.put("_zpsbd7", req.getHeader("User-Agent") != null ? req.getHeader("User-Agent") : "");
                ssreqJsonObj.put("_zpsbd8", shieldsquareCalltype);
                ssreqJsonObj.put("_zpsbd9", shieldsquareUsername);
                ssreqJsonObj.put("_zpsbda", System.currentTimeMillis() / 1000);
                ss2respJson.put("pid", shieldsquareTid);

                if (isSync) {
                	long start = System.currentTimeMillis();
                    ObjectNode ssrespJson = syncSendReq2SS(commonCachedProperties.getPropertyValue(ConnConstants.HTTP_API_URL), ssreqJsonObj.toString());
                    String ssrespString = null;
                    if (ssrespJson.get("ssresp") != null) {
                        ssrespString = ssrespJson.get("ssresp").toString();
                    }

                    if (ssrespString != null && !ssrespString.trim().isEmpty()) {
                        ss2respJson.put("responsecode", ssrespString);
                    }

                    String dynamicJS = null;
                    if (ssrespJson.get("dynamic_JS") != null) {
                        dynamicJS = ssrespJson.get("dynamic_JS").asText();
                    }
                    dynamicJS = dynamicJS != null ? dynamicJS : "var __uzdbm_c = 2+2";
                    ss2respJson.put("dynamic_JS", dynamicJS);
                    if (logger.isDebugEnabled()) {
                    	logger.debug("Time Take to get response from SS2: " + (System.currentTimeMillis() - start));
                    }
                } else {
                    /*
                     * Reason for change : For D3.2 Release, to encode every request sent to SS2 server
                     */

                    asyncSendReq2SS(commonCachedProperties.getPropertyValue(ConnConstants.HTTP_API_URL), URLEncoder.encode(ssreqJsonObj.toString(), "UTF-8"));
                    ss2respJson.put("responsecode", allow);
                    ss2respJson.put("dynamic_JS", "var __uzdbm_c = 2+2");
                }
            } catch (Exception e) {
                ss2respJson.put("reason", convertStackTrace(e));
                ss2respJson.put("responsecode", allowExp);
                ss2respJson.put("dynamic_JS", "var __uzdbm_c = 2+2");
            }
        } else {
            ss2respJson.put("pid", "00000000-0000-0000-0000-000000000000");
            ss2respJson.put("responsecode", staticResource);
            ss2respJson.put("reason", "Filtered the request received to serve static content");
        }
        return ss2respJson;
    }

    public synchronized void initializeBuilder() {
        if (asyncHttpClient == null) {
            builder = new AsyncHttpClientConfig.Builder();
            builder.setCompressionEnabled(true).setAllowPoolingConnection(true).setRequestTimeoutInMs(commonCachedProperties.getIntPropertyValue(ConnConstants.REQ_TIME_OUT, 100))
                    .build();
            asyncHttpClient = new AsyncHttpClient(builder.build());
        }
    }

    public void sendJSReq(String jsdata, HttpServletRequest req) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode ss2respJson = mapper.createObjectNode();

        try {
            String reqIpAddr = null;
            if (commonCachedProperties.getPropertyValue(ConnConstants.IP_ADDRESS).equalsIgnoreCase("REMOTE_ADDR")) {
                reqIpAddr = req.getRemoteAddr();
            } else {
                reqIpAddr = req.getHeader(commonCachedProperties.getPropertyValue(ConnConstants.IP_ADDRESS));
                /*
                 * Reason for change : For D3.2 Release, to encode every request sent to SS2 server
                 */
            }

            ObjectNode jsdataNode = mapper.createObjectNode();
            if (jsdata != null && !jsdata.isEmpty()) {
                JsonNode respJsonNode = (ObjectNode) mapper.readTree(jsdata);
                if (respJsonNode.isObject()) {
                    jsdataNode = (ObjectNode) respJsonNode;
                }
            }

            jsdataNode.put("sid", commonCachedProperties.getPropertyValue(ConnConstants.SID));
            jsdataNode.put("host", reqIpAddr);

            String jsencodeData = URLEncoder.encode(jsdataNode.toString(), "UTF-8");
            asyncSendReq2SS(commonCachedProperties.getPropertyValue(ConnConstants.JS_API_URL), jsencodeData);
        } catch (Exception e) {
            ss2respJson.put("reason", convertStackTrace(e));
            ss2respJson.put("responsecode", allowExp);
        }
    }

    public void asyncSendReq2SS(String ss2url, String indata) throws Exception {

        Request req = asyncHttpClient.preparePost(ss2url).setHeader("Content-Type", "application/json").setBody(indata).build();
        final long start = System.currentTimeMillis();

        try {
            asyncHttpClient.prepareRequest(req).execute(new AsyncCompletionHandler<Response>() {
                @Override
                public Response onCompleted(Response response) throws Exception {
                    if (logger.isDebugEnabled()) {
                    	logger.debug("Async Time Take to get response from SS2: " + (System.currentTimeMillis() - start));
                    }
                    return response;
                }

                @Override
                public void onThrowable(Throwable t) {

                }
            });
        } catch (IOException e) {
            throw e;
        }
    }

    public ObjectNode syncSendReq2SS(String ss2url, String indata) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode ss2respJson = mapper.createObjectNode();
        HttpURLConnection connection = null;
        DataOutputStream out = null;
        BufferedInputStream inStream = null;
        BufferedReader respReader = null;
        try {
            URL url = new URL(ss2url);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setReadTimeout(commonCachedProperties.getIntPropertyValue(ConnConstants.REQ_TIME_OUT, 100));
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Content-Encoding", "UTF-8");
            out = new DataOutputStream(connection.getOutputStream());
            out.writeBytes(URLEncoder.encode(indata, "UTF-8"));
            out.flush();
            connection.connect();
            inStream = new BufferedInputStream(connection.getInputStream());

            int statusCode = connection.getResponseCode();
            if (statusCode != 200) {
                inStream = new BufferedInputStream(connection.getErrorStream());
            }

            // read the output from the server
            respReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder respBuilder = new StringBuilder();

            String line = null;
            while ((line = respReader.readLine()) != null) {
                respBuilder.append(line + "\n");
            }

            String ssresp = respBuilder.toString();
            JsonNode respJsonNode = (ObjectNode) mapper.readTree(ssresp);
            if (respJsonNode.isObject()) {
                ss2respJson = (ObjectNode) respJsonNode;
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                logger.info(e);
            }

            try {
                if (inStream != null) {
                    inStream.close();
                }
            } catch (IOException e) {
                logger.info(e);
            }

            try {
                if (respReader != null) {
                    respReader.close();
                }
            } catch (IOException ioe) {
                logger.info(ioe);
            }

        }
        return ss2respJson;
    }

    public String convertStackTrace(Exception t) {
        return t.getMessage();
    }

    private String shieldsquareGenerateTid(String reqIpAddr, String shieldsquareSid) {
        long t = System.currentTimeMillis() / 1000;
        String[] sipParts = shieldsquareSid.split("-");
        String sidMin = sipParts[3];
        Random random = new Random();
        int randomnumLow = random.nextInt(39321 - 4096) + 4096;
        int randomnumLow1 = random.nextInt(39321 - 4096) + 4096;
        int randomnumLow2 = random.nextInt(39321 - 4096) + 4096;
        int randomnumLow3 = random.nextInt(39321 - 4096) + 4096;
        String timeHex = Long.toHexString(t);
        String ip2hexStr = "00000000";
        if (reqIpAddr != null) {
            ip2hexStr = shieldsquareIP2Hex(reqIpAddr);
        }
        String tid = ip2hexStr + "-" + sidMin + "-" + timeHex.charAt(timeHex.length() - 1) + timeHex.charAt(timeHex.length() - 2) + timeHex.charAt(timeHex.length() - 3)
                + timeHex.charAt(timeHex.length() - 4) + "-" + Integer.toHexString(randomnumLow) + "-" + Integer.toHexString(randomnumLow1) + Integer.toHexString(randomnumLow2)
                + Integer.toHexString(randomnumLow3);
        return tid;
    }

    public String shieldsquareIP2Hex(String reqIpAddr) {
        String hex = "";
        String[] part = reqIpAddr.split("[\\.,]");
        if (part.length < 4) {
            return "00000000";
        }
        for (int i = 0; i < 4; i++) {
            int decimal = Integer.parseInt(part[i]);
            if (decimal < 16) // Append a 0 to maintian 2 digits for every number
            {
                hex += "0" + String.format("%01X", decimal);
            } else {
                hex += String.format("%01X", decimal);
            }
        }
        return hex;
    }
}
