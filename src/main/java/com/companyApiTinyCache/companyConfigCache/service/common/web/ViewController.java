/*
 * @(#) ViewController.java
 *
 * Copyright 2010-2011 by Cleartrip Travel Services Pvt. Ltd.
 * Unit No 001, Ground Floor, DTC Bldg,
 * Sitaram Mills Compound, N.M. Joshi Marg,
 * Delisle Road,
 * Lower Parel (E)
 * Mumbai - 400011
 * India
 *
 * This software is the confidential and proprietary information
 * of Cleartrip Travel Services Pvt. Ltd. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of license agreement you
 * entered into with Cleartrip Travel Service Pvt. Ltd.
 */
package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.cached.resources.WhitelabelViewResource;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.CtBaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * ViewController defines the flow:
 * <ol>
 * <li>Resolve company-id and source-type from incoming request</li>
 * <li>Add company-id and source-type as attributes to the request</li>
 * <li>Invoke abstract method processRequest().</li>
 * <li>Retrieve pageElementsMap</li>
 * <li>Inject the pageElementsMap data into the modelAndView</li>
 * <li>Return modelAndView</li>
 * </ol>
 * 
 * 
 * @author Amit Kumar Sharma, Sanjeev Desai
 */
public abstract class ViewController extends BaseController {

    /** Flag indicating if stats logging is enabled */
    protected boolean doStats;

    /**
     * Parameter name for extracting default company-id value in dev-env. not used in production.
     */
    public static final String DEFAULT_COMAPNY_ID_PARAM = "ct.commonui.air.companyid";

    /** Parameter name for source type */
    public final static String SOURCE_TYPE_PARAM = "source";

    public static final String SET_JS_COOKIE_DOMAIN = "SET_JS_COOKIE_DOMAIN";

    /** Logger instance */
    protected static final Log logger = LogFactory.getLog(ViewController.class);

    /**
     * Map of resources. This should be PRIVATE as only this class needs to be aware of whiteLabelViewResource
     */
    private WhitelabelViewResource whiteLabelViewResource;

    /**
     * Map of viewnames. This should be PRIVATE as only this class needs to be aware of commonUIViewnameFactory.
     */
    private CommonUIViewnameFactory commonUIViewnameFactory;

    /** All the cached properties */
    protected CachedProperties cachedProperties;

    /**
     * Defines the flow for all controllers extending ViewController. This method is final and it defines the flow.
     * <p>
     * <u>Workflow :</u>
     * <ol>
     * <li>Resolve company-id and source-type from incoming request</li>
     * <li>Add company-id and source-type as attributes to the request</li>
     * <li>Invoke abstract method processRequest(). This method will be implemented by all subclasses.</li>
     * <li>Retrieve pageElementsMap</li>
     * <li>Inject the pageElementsMap data into the modelAndView</li>
     * <li>Return modelAndView</li>
     * </ol>
     * 
     */
    protected final ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        long startTime = System.currentTimeMillis();
        long endTime = -1;
        long turnaroundTime = -1;

        /** Format for logging all requests (max-resolution: ms) */
        SimpleDateFormat logTimeStampFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SS");

        try {
            // 0. Check if CompanyConfigFilter caught injected and HTTP error number
            // In such a case, just send across the same error-code to client and
            // abort any further processing.
            Object httpErrorCode = request.getAttribute(CompanyConfigurationFilter.HTTP_ERROR_NUMBER);
            if (httpErrorCode != null) {
                response.sendError((Integer) httpErrorCode);
                return null;
            }

            // 1. Extract required parameters
            Integer companyId = retrieveCompanyId(request);
            if (companyId == null) {
                response.sendError(HttpURLConnection.HTTP_BAD_REQUEST);
                return null;
            }

            addCookieDomainSwitch(request);

            // 2. Pre-Process request. Proceed further only if preprocessRequest()
            // return true.
            // (This is done to accomodate the method
            // BookAppController.checkAndRedirect(). The method redirects incoming
            // request to correct view depending on the state of initnerary object).
            boolean proceed = preprocessRequest(request, response);
            if (!proceed) {
                return null;
            }

            // 3. Process the request and obtain ModelAndView object
            // [WARNING!]: ModelAndView can be null at this point. This
            // This is because BookController calls checkAndRedirect()
            // after booking. This flow could not be changed, hence
            // after processing request, null is checked. If modelandview
            // is null, further processing is aborted.
            ModelAndView modelAndView = processRequest(request, response);
            if (modelAndView == null) {
                return null;
            }

            // 4. Perform any post-processing required.
            postprocessRequest(modelAndView, request, response);

            // 5. Add view-level page-element overrides.
            // CompanyConfig (Affiliate) and default-page-elements are already
            // injected by CompanyResolutionFilter as attributes to incoming request.
            addViewLevelPageElementOverrides(companyId, modelAndView, request);

            // 6. Resolve the correct view-name here. Whether only content-area
            // needs to be returned or the whole page with header and footer
            // as well.
            tweakModelAndViewForContentOnly(modelAndView, request);

            // 7. Return the modelAndView.
            return modelAndView;
        } finally {

            // Calculate turnaround time for request.
            endTime = System.currentTimeMillis();
            turnaroundTime = endTime - startTime;

            // Extract the apache cookie
            Cookie apacheCookie = WebUtils.getCookie(request, "Apache");
            String apacheCookieValue = "[N/A]";
            if (apacheCookie != null) {
                apacheCookieValue = apacheCookie.getValue();
            }

            // Generate the logging message :
            // > Controller Name
            // > Start Time
            // > End Time
            // > Turnaround time (in ms)
            // > Apache Cookie
            StringBuffer msg = new StringBuffer().append("\n> Controller: ").append(this.getClass())

            .append("\n> Start time: ").append(logTimeStampFormat.format(new Date(startTime)))

            .append("\n> End time: ").append(logTimeStampFormat.format(new Date(endTime)))

            .append("\n> Turnaround time: ").append(turnaroundTime).append(" ms")

            .append("\n> Apache cookie: ").append(apacheCookieValue);

            logger.warn(msg.toString());
        }
    }

    protected void addViewLevelPageElementOverrides(Integer companyId, ModelAndView modelAndView, HttpServletRequest request) {
        // 1. Add custom page elements to the ModelAndView being returned.
        Map<String, Map<String, String>> viewResource = getPageElementsMap(companyId.toString());

        // 2. View level overrides
        Map<String, String> viewSpecificMap = viewResource.get(modelAndView.getViewName());
        if (viewSpecificMap != null && viewSpecificMap.size() > 0) {
            addAllObjectsToRequest(viewSpecificMap, request);
        }

        return;
    }

    protected void addAllObjectsToRequest(Map<String, String> objectMap, HttpServletRequest request) {

        if (objectMap == null) {
            return;
        }

        String key = null;
        String value = null;
        for (Iterator<String> keyIter = objectMap.keySet().iterator(); keyIter.hasNext();) {
            key = keyIter.next();
            value = objectMap.get(key);
            request.setAttribute(key, value);
        }

        return;
    }

    /**
     * Function to perform any preprocessing on HttpServletRequest and HttpServletResponse before processing the request. This function will be invoked by ViewController before invoking
     * preprocessRequest(). The default implementation is empty and always returns true. Should be overridden by the extending class.
     * 
     * @param request
     *            Incoming request object
     * @param response
     *            Response object
     * @return Boolean value indicating if further processing should be performed. processRequest() and postprocessRequest() will be invoked only if preprocessRequest() returns true.
     */
    protected boolean preprocessRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return true;
    }

    /**
     * Invoked by ViewController.handleRequestInternal(). Defines the following flow:
     * <ol>
     * <li>Initiate stats object</li>
     * <li>Initialize the response code to 500</li>
     * <li>Invoke processRequestInternal() to obtain ModelAndView</li>
     * <li>Set response code to 200</li>
     * <li>In case if an exception, log the exception</li>
     * <li>Finally log the stats</li>
     * </ol>
     * 
     * @param request
     * @param response
     * @return
     */
    protected final ModelAndView processRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        CtBaseStats stats = null;
        ModelAndView modelAndView = null;

        int responseStatusCode = -1;

        try {
            // 1. Init stats object
            stats = initStats(request);

            // 2. Set the request-status = Failure
            responseStatusCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

            // 3. Invoke the core procressing
            // [WARNING!]: ModelAndView can be null at this point. So please
            // do check if any function is added after this call
            // in the flow.
            modelAndView = processRequestInternal(request, response);

            // 4. Set the bookStatus = Success
            responseStatusCode = HttpServletResponse.SC_OK;
        } catch (Exception e) {
            // 5. Log any errors
            logger.error("An error occured while processing request: ", e);
        } finally {
            // 6. Log the status of book-request
            commitStats(stats, request, responseStatusCode);
        }

        return modelAndView;
    }

    /**
     * Function to perform any processing on generated ModelAndView and HttpServletResponse after ModelAndView has been generated. This function will be invoked by ViewController after invoking
     * preprocessRequest(...). <BR/>
     * <strong>WARNING!:</strong> ModelAndView can be null at this poin in the flow, so overriding implementations should perform a null check.
     * 
     * @param modelAndView
     * @param request
     * @param response
     */
    protected void postprocessRequest(ModelAndView modelAndView, HttpServletRequest request, HttpServletResponse response) {
        if (modelAndView == null) {
            return;
        }
    }

    /**
     * Initialize appropriate Stats object (default implementations are provided in SearchAppController and BookAppController for common-search-ui and common-book-ui respectively.
     * 
     * @param request
     * @return
     */
    protected abstract CtBaseStats initStats(HttpServletRequest request);

    /**
     * Log the event using appropriate stats object (default implementations are provided in SearchAppController and BookAppController for common-search-ui and common-book-ui respectively.
     * 
     * @param statsObj
     * @param request
     * @param responseStatusCode
     * @return
     */
    protected abstract CtBaseStats commitStats(CtBaseStats statsObj, HttpServletRequest request, int responseStatusCode);

    /**
     * Method to be implemented by controllers to process the incoming request.
     * 
     * @param request
     * @param response
     * @return
     */
    protected abstract ModelAndView processRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception;

    /**
     * Retrieves pageElementsMap for a given company-id
     * 
     * @param companyId
     * @return
     */
    protected Map<String, Map<String, String>> getPageElementsMap(String companyId) {

        Map<String, Map<String, Map<String, String>>> resource = whiteLabelViewResource.getResourceValue();
        if (resource == null) {
            logger.info("Resource value is null for whiteLabelViewResource");
            throw new IllegalStateException("Resource value is null for whiteLabelViewResource");
        }

        Map<String, Map<String, String>> pageElementsMap = resource.get(companyId);
        if (pageElementsMap == null) {
            logger.error("pageElementsMap is null for companyId=" + companyId);
            throw new IllegalStateException("pageElementsMap is null for companyId=" + companyId);
        }

        return pageElementsMap;
    }

    /**
     * Method to retrieve company-id from incoming request, parse it and set it as an attribute in request.
     * 
     * @param request
     * @return
     */
    protected int retrieveCompanyId(HttpServletRequest request) {

        boolean isDev = cachedProperties.getBooleanPropertyValue("dev_env", false);
        String defaultCompanyId = cachedProperties.getPropertyValue(DEFAULT_COMAPNY_ID_PARAM);
        String companyIdVal = StringUtils.trimToNull(request.getParameter(CompanyConfigurationFilter.COMPANY_ID_PARAM));

        // For testing common-ui, extract a default value.
        if (isDev && companyIdVal == null) {
            companyIdVal = defaultCompanyId;
        }

        Integer companyId = null;
        try {
            companyId = Integer.parseInt(companyIdVal);
            request.setAttribute(CompanyConfigurationFilter.COMPANY_ID_PARAM, companyId);
        } catch (NumberFormatException nex) {
            // do nothing. NumberFormatException will result in
            // null value being returned which is handled by the calling method.
        }

        return companyId;
    }

    /**
     * Method to retrieve company-id from incoming request and set it as an attribute in request.
     * 
     * @param request
     * @return
     */
    protected String retrieveSourceType(HttpServletRequest request) {

        String sourceType = request.getParameter(SOURCE_TYPE_PARAM);
        request.setAttribute(SOURCE_TYPE_PARAM, sourceType);

        return sourceType;
    }

    /**
     * Tweaks ModelAndView for content-only request.
     * 
     * @param modelAndView
     * @param request
     */
    protected void tweakModelAndViewForContentOnly(ModelAndView modelAndView, HttpServletRequest request) {
        boolean contentOnly = CommonUIViewnameFactory.extractContentonlyParam(request);
        String viewName = modelAndView.getViewName();
        viewName = commonUIViewnameFactory.resolveViewName(viewName, contentOnly);
        if (contentOnly) {
            // Only content-JSP needs to be rendered in this case.
            modelAndView.setViewName(viewName);
        } else {
            // Content-JSP needs to be included in the page in this case.
            modelAndView.addObject("CONTENT", viewName);
        }
    }

    /**
     * Extracts current domain (eg, expedia.whitelabel.com) and adds it to current request object as attribute.
     * 
     * @param request
     * @param companyId
     */
    private void addCookieDomainSwitch(HttpServletRequest request) {

        // 1.Check if cookie needs to be set at domain level (for *.cleartrip.*)
        boolean setCookieDomain = GenUtil.isSetCookieDomain(request);

        // 2. Set the decision as boolean attribute in request
        request.setAttribute(SET_JS_COOKIE_DOMAIN, setCookieDomain);
    }

    // /////////////////////////////////////////////////////////////////
    // /////////////////// Setters for members /////////////////////////
    // /////////////////////////////////////////////////////////////////
    public void setCachedProperties(CachedProperties cachedProperties) {
        this.cachedProperties = cachedProperties;
    }

    public void setWhiteLabelViewResource(WhitelabelViewResource whiteLabelViewResource) {
        this.whiteLabelViewResource = whiteLabelViewResource;
    }

    public void setCommonUIViewnameFactory(CommonUIViewnameFactory commonUIViewnameFactory) {
        this.commonUIViewnameFactory = commonUIViewnameFactory;
    }

}
