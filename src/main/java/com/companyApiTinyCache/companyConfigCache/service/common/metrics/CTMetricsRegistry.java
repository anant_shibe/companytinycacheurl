package com.companyApiTinyCache.companyConfigCache.service.common.metrics;

import com.codahale.metrics.MetricRegistry;


public class CTMetricsRegistry {
	public static final MetricRegistry METRIC_REGISTRY = new MetricRegistry();

    public static MetricRegistry getMetricRegistry() {
        return METRIC_REGISTRY;
    }

}
