package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.Department;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
*
* @author amith
*
*/
public class DepartmentsHolder {

    @JsonProperty("departments")
    private List<Department> departments;

    public DepartmentsHolder() {
        departments = new ArrayList<Department>(1);
    }

    @JsonProperty("departments")
    public List<Department> getDepartments() {
        return departments;
    }

    @JsonProperty("departments")
    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

}
