package com.companyApiTinyCache.companyConfigCache.service.common.exceptions;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

/**
 * The Class JWTException.
 */
@ClassExcludeCoverage
public class JWTException extends RuntimeException {

    private static final long serialVersionUID = 8128969971920559352L;

    /**
     * Instantiates a new JWTxception.
     */
    public JWTException() {
    }

    /**
     * Instantiates a new JWTException.
     * 
     * @param message
     *            the message
     */
    public JWTException(String message) {
        super(message);
    }

    /**
     * Instantiates a new JWTException.
     * 
     * @param cause
     *            the cause
     */
    public JWTException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new JWTException.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public JWTException(String message, Throwable cause) {
        super(message, cause);
    }

}
