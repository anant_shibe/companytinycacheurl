package com.companyApiTinyCache.companyConfigCache.service.common.service;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Sudhanshu
 *
 */
public interface SourceIdentifierService {
	public static final String THIRD_PARTY = "THIRD_PARTY";
	public static final String MOBILE_APP = "MOBILE_APP";
	public static final String DESKTOP = "DESKTOP";
	public static final String MOBILE_WEBSITE = "MOBILE_WEBSITE";
	public static final String UNKNOWN = "UNKNOWN";
	
	public String getSource(HttpServletRequest request);
}
