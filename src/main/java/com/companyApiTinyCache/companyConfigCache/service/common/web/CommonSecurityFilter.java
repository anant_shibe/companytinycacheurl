/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.service.AuthenticationService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CommonUrlResources;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonSecurityFilter.Config;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Security Filter that decrypts user cookie and sets it as Session Security Bean. Webapp security filters extend this and call the doFilter method.
 *
 * @author deepa
 */
public class CommonSecurityFilter implements Filter, ConfigLoader<Config> {

    static class Config {
        private boolean cltpStaticEnabled;

        public Config(Map<String, String> cachedPropertiesResource) {
            cltpStaticEnabled = CachedProperties.getBooleanPropertyValue(cachedPropertiesResource, "ct.common.cltpstatic.enabled", true);
        }

        public boolean isCltpStaticEnabled() {
            return cltpStaticEnabled;
        }
    }

    private static final Log logger = LogFactory.getLog(CommonSecurityFilter.class);

    private static final String[] INTERNAL_DOMAIN_PREFIXES = {"qa2", "beta", "staging", "dev"};

    protected FilterConfig filterConfig;
    protected AuthenticationService authenticationService;
    protected CachedProperties cachedProperties;
    protected CachedProperties cachedUiProperties;
    protected CachedUrlResources<CommonUrlResources> cachedUrlResources;
    protected String[] excludeSuffixes;
    protected String[] includes;

    protected CachedPropertiesManager<Config> configManager;

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy() {
    }

    private boolean isExcluded(HttpServletRequest request) {
        boolean excluded = true;
        int i;
        String path = request.getRequestURI().toLowerCase();
        for (i = 0; i < includes.length; i++) {
            if (path.indexOf(includes[i]) >= 0) {
                excluded = false;
                break;
            }
        }
        if (excluded) {
            excluded = false;
            for (i = 0; i < excludeSuffixes.length; i++) {
                if (path.endsWith(excludeSuffixes[i])) {
                    excluded = true;
                    break;
                }
            }
        }

        return excluded;
    }

    /**
     * Creates a preferred cookie for country.
     *
     * @param requestedCountry
     * @return
     */
    private Cookie createCtAuthCookie(String cookieValue, String domain) {

        String ctAuthCookieName = authenticationService.getAuthCookieName();
        Cookie prefCookie = new Cookie(ctAuthCookieName, cookieValue);

        if (domain != null) {
            if ("IN".equalsIgnoreCase(domain)) {
                domain = "com";
            }
            domain = ".cleartrip." + domain.toLowerCase();
            prefCookie.setDomain(domain);
        }

        // need to set the path, otherwise it signout won't be recognized.
        prefCookie.setPath("/");

        return prefCookie;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        doFilter(request, response, chain, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain, boolean delegateToChain) throws IOException, ServletException {

        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("Can only process HttpServletRequest");
        }

        HttpServletRequest httpReq = (HttpServletRequest) request;

        Config c = configManager.getConfig();
        // Set the flag to check if express checkout is enabled for mobile
        boolean isExpressCheckout = isExpressCheckoutEnabled(httpReq);
        request.setAttribute("isExpressCheckout", isExpressCheckout);

        // Let staging / www be available to JSPs
        String ctHloc = SecurityUtil.getHostWithoutPort(httpReq);
        int pos = ctHloc.indexOf(':');
        if (pos > 0) {
            ctHloc = ctHloc.substring(0, pos);
        }
        ctHloc = ctHloc.toLowerCase();
        request.setAttribute("ct_hloc", ctHloc);

        int i;
        String cltpStaticDomain = "";
        if (c.isCltpStaticEnabled()) {
            if ((ctHloc.indexOf(".cleartrip.") > 0) || (ctHloc.indexOf("amex") > 0)) {
                String internalDomainPrefix = null;
                for (i = 0; i < INTERNAL_DOMAIN_PREFIXES.length; i++) {
                    if (ctHloc.startsWith(INTERNAL_DOMAIN_PREFIXES[i])) {
                        internalDomainPrefix = INTERNAL_DOMAIN_PREFIXES[i];
                        break;
                    }
                }
                if (internalDomainPrefix != null) {
                    cltpStaticDomain = "//" + internalDomainPrefix + ".cltpstatic.com";
                } else {
                    cltpStaticDomain = "//ui.cltpstatic.com";
                }
                boolean isLanPrefCookieEnabled = cachedProperties.getBooleanPropertyValue("ct.services.language.pref.for.static.site.cookie.enabled", false);
                if (isLanPrefCookieEnabled) {
                    String lpCookieName = cachedProperties.getPropertyValue("ct.services.language.preference.cookie.name", "lp");
                    Cookie lpCookie = GenUtil.getCookieFromRequest((HttpServletRequest) request, lpCookieName);
                    if (null != lpCookie && StringUtils.isNotBlank(lpCookie.getValue()) && !lpCookie.getValue().equalsIgnoreCase("en")) {
                        // setting cltpStaticDomain to //ui.cltpstatic.com from null for arabic language as well. reference: AIRGROW-190
                        cltpStaticDomain = "//ui.cltpstatic.com";
                    }
                }
            }
        }
        request.setAttribute("cltp_static_site", cltpStaticDomain);

        boolean excluded = isExcluded(httpReq);
        String host = null;
        if (!excluded) {
            // Add ct_properties as request attributes
            Map<String, String> uiProperties = null;
            if (cachedUiProperties != null) {
                uiProperties = cachedUiProperties.getResource();
            }
            if (uiProperties != null) {
                httpReq.setAttribute("uiProperties", uiProperties);
            }
            cachedUrlResources.updateUiAttributes(httpReq);
            // End Add ct_properties as request attributes
            host = SecurityUtil.getHost(httpReq);
            cachedUiProperties.setProperty("ct.common.webhost", host);
        }

        if (!authenticationService.isSecurityEnable() || excluded) {
            chain.doFilter(request, response);
        } else {
            if (!(response instanceof HttpServletResponse)) {
                throw new ServletException("Can only process HttpServletResponse");
            }
            HttpServletResponse httpResp = (HttpServletResponse) response;
            httpResp.setContentType("text/html; charset=UTF-8");

            httpReq.setAttribute("servletContext", filterConfig.getServletContext());
            Cookie authCookie = authenticationService.getACubeAuthCookieFromRequest(httpReq);
            SecurityBean securityBean = null;

            // Set domainName to null for non-cleartrip (which means whitelabel) cookies except few wl sites
            String domainName = GenUtil.isSetCookieDomain(httpReq) ? SecurityUtil.getCookieDomainForRequestDomain(httpReq) : SecurityUtil.setCookieDomainWL(httpReq, cachedProperties);

            // this is used when somebody changes the country dropdown
            if (authCookie == null) {
                String value = (String) request.getParameter("ct_auth");
                if (StringUtils.isNotBlank(value)) {
                    authCookie = createCtAuthCookie(value, domainName);
                }
            }

            if (authCookie != null && authCookie.getValue() != null && !(authCookie.getValue().isEmpty())) {
                try {
                    Cookie cookie = authCookie;
                    HashMap<String, String> user = authenticationService.getPersonFromCookie(authCookie);

                    // modify based on domain
                    Cookie updatedCookie = authenticationService.modifyCookie(cookie, domainName);
                    // updatedCookie.setDomain(".cleartrip.com");
                    httpResp.addCookie(updatedCookie);
                    if (user != null) {
                        int userId;
                        try {
                            userId = (Integer.parseInt(user.get(CommonEnumConstants.UserDetails.USER_ID.toString())));
                        } catch (Exception e) {
                            logger.debug("Exception thrown while trying to convert userId to integer - " + user);
                            throw e;
                        }
                        if (securityBean == null || securityBean.getPeopleId() != userId) {
                            securityBean = createSecurityBean(user);
                        }
                    }
                    request.setAttribute(CommonEnumConstants.UserDataKey.USER_NAME.toString(), authenticationService.getScreenName(user));
                    request.setAttribute(CommonEnumConstants.UserDataKey.USER_DATA_KEY.toString(), user);

                } catch (Exception e) {
                    logger.error("Exception is:" + e.getMessage(), e);
                }
            } else {
                Cookie unconfirmedCookie = authenticationService.getUnconfirmedCookieFromRequest(httpReq);
                String email = null;
                if (unconfirmedCookie != null) {
                    email = unconfirmedCookie.getValue();
                    if (StringUtils.isEmpty(email)) {
                        email = null;
                    } else {
                        email = URLDecoder.decode(email, "UTF-8");
                    }
                }
                if (email != null) {
                    if (securityBean == null || !email.equals(securityBean.getEmailAddress())) {
                        securityBean = createUnconfirmedSecurityBean(email);
                    }
                }
            }
            if (securityBean == null) {
                securityBean = new SecurityBean();
            }
            httpReq.setAttribute(CommonEnumConstants.SESSION_SECURITY_BEAN_ID, securityBean);
            String screenName = securityBean.getUserScreenName();
            if (screenName != null) {
                boolean isLoggedIn = true;
                boolean isConfirmedUser = false;
                if (!securityBean.isUnconfirmedUser()) {
                    isConfirmedUser = true;
                }
                request.setAttribute("logged_in", isLoggedIn);
                request.setAttribute("is_confirmed", isConfirmedUser);
                request.setAttribute("screen_name", screenName);
            }
            String productCode = StringUtils.trimToNull(request.getParameter("product-code"));
            if (productCode != null) {
                productCode = productCode.toUpperCase();
                request.setAttribute("productCode", productCode);
            }
            String path = httpReq.getRequestURI().toLowerCase();
            String app = null;
            if (path.indexOf("/flights") > 0) {
                app = "flights";
            } else if (path.indexOf("/hotels") >= 0) {
                app = "hotels";
            }
            if (app != null) {
                request.setAttribute("ctApp", app);
            }

            if (delegateToChain) {
                chain.doFilter(request, response);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        doInit();
    }

    /**
     * This doInit() gets the bean from the application context.
     *
     * @throws ServletException
     *             If there is an exception
     */
    public void doInit() throws ServletException {

        ApplicationContext ctx = this.getContext(filterConfig);

        String beanName = this.filterConfig.getInitParameter("authBean");
        if (beanName == null) {
            beanName = "authenticationServiceBean";
        }
        Object beanObj = ctx.getBean(beanName);
        if (beanObj instanceof AuthenticationService) {
            authenticationService = (AuthenticationService) beanObj;
        }
        beanObj = ctx.getBean("cachedUiProperties");
        if (beanObj instanceof CachedProperties) {
            cachedUiProperties = (CachedProperties) beanObj;
        }
        beanObj = ctx.getBean("cachedUrlResources");
        if (beanObj instanceof CachedUrlResources) {
            cachedUrlResources = (CachedUrlResources<CommonUrlResources>) beanObj;
        }
        beanObj = ctx.getBean("commonCachedProperties");
        if (beanObj instanceof CachedProperties) {
            cachedProperties = (CachedProperties) beanObj;
            configManager = new CachedPropertiesManager<Config>(cachedProperties, this);
        }
        String suffixList = this.filterConfig.getInitParameter("excludeSuffixes");
        if (suffixList != null) {
            excludeSuffixes = suffixList.split(",");
        } else {
            excludeSuffixes = new String[0];
        }
        String includeList = this.filterConfig.getInitParameter("include");
        if (includeList != null) {
            includes = includeList.split(",");
        } else {
            includes = new String[0];
        }
    }

    /*
     * Allows test cases to override where application context obtained from.
     * 
     * @param filterConfig which can be used to find the <code>ServletContext</code>
     * 
     * @return the Spring application context
     */
    protected ApplicationContext getContext(FilterConfig filterConfig) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
    }

    private SecurityBean createSecurityBean(HashMap<String, String> user) {
        SecurityBean securityBean = new SecurityBean();

        int peopleId = Integer.parseInt(user.get(CommonEnumConstants.UserDetails.USER_ID.toString()));
        if (peopleId > 0) {
            securityBean.setUserLoggenIn(true);
            securityBean.setPeopleId(peopleId);
            securityBean.setEmailAddress(user.get(CommonEnumConstants.UserDetails.USER_NAME.toString()));

            String userName = user.get(CommonEnumConstants.UserDetails.USER_NAME.toString());
            String screenName = user.get(CommonEnumConstants.UserDetails.SCREEN_NAME.toString());
            if (screenName == null || screenName.length() < 1) {
                securityBean.setScreenName(userName);
            } else {
                securityBean.setScreenName(screenName);
            }

            securityBean.setAccessRoles(user.get(CommonEnumConstants.UserDetails.USER_ROLES.toString()));

            securityBean.setAllRoles(user.get(CommonEnumConstants.UserDetails.ACCESS_ROLES.toString()));
        } else {
            securityBean.setUserLoggenIn(false);
        }

        return securityBean;
    }

    private SecurityBean createUnconfirmedSecurityBean(String unconfirmedEmail) {
        SecurityBean securityBean = new SecurityBean();
        if (unconfirmedEmail != null && !unconfirmedEmail.isEmpty()) {
            securityBean.setUserLoggenIn(true);
            securityBean.setUnconfirmedUser(true);
            securityBean.setEmailAddress(unconfirmedEmail);
            securityBean.setScreenName(unconfirmedEmail + "(" + CommonEnumConstants.UNCONFIRMED + ")");
        }

        return securityBean;
    }

    /**
     * Checks if the express checkout cookie is set for mobile.
     *
     * @param request
     * @return
     */
    private boolean isExpressCheckoutEnabled(HttpServletRequest request) {

        boolean result = false;

        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return false;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(CommonEnumConstants.EXPRESSO_COOKIE_NAME)) {
                result = true;
                break;
            }
        }

        return result;
    }

    @Override
    public Config loadConfig(Map<String, String> cachedPropertiesResource) {
        return new Config(cachedPropertiesResource);
    }
}
