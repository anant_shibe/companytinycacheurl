package com.companyApiTinyCache.companyConfigCache.service.common.util.config;

public class CtConfigConsuleProperties {

    private final String server;
    private final String applicationName;
    private final String branchName;
    private final String profileName;

    public CtConfigConsuleProperties(String server, String applicationName, String branchName, String profileName) {
        this.server = server;
        this.applicationName = applicationName;
        this.branchName = branchName;
        this.profileName = profileName;
    }

    public String getServer() {
        return server;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getProfileName() {
        return profileName;
    }

    @Override
    public String toString() {
        return "CtConfigConsuleProperties{" +
                "server='" + server + '\'' +
                ", applicationName='" + applicationName + '\'' +
                ", branchName='" + branchName + '\'' +
                ", profileName='" + profileName + '\'' +
                '}';
    }
}
