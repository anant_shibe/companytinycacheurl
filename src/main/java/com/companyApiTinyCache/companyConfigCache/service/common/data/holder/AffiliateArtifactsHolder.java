package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.AffiliateArtifact;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author amith
 *
 */
public class AffiliateArtifactsHolder implements Serializable {

    @JsonProperty("artifacts")
    private List<AffiliateArtifact> artifacts;

    public AffiliateArtifactsHolder() {
        artifacts = new ArrayList<AffiliateArtifact>(1);
    }

    @JsonProperty("artifacts")
    public List<AffiliateArtifact> getArtifacts() {
        return artifacts;
    }

    @JsonProperty("artifacts")
    public void setArtifacts(List<AffiliateArtifact> artifacts) {
        this.artifacts = artifacts;
    }

}
