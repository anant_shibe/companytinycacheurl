package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.impl;

import com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.config.RabbitMQConnectionConfig;
import com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions.AMQPChannelCreationException;
import com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions.RabbitMQCreateChannelException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.util.Map;

public interface AMQPMessageBrokerService {
	Connection getConnection(Map<String, Object> connectionCredentials) throws Exception;
	Connection getConnection(final String amqp_brokerUri) throws Exception;
	Connection getConnection(RabbitMQConnectionConfig connectionConfigs) throws Exception;
	Connection getConnection(String hostName, String userName, String password, String portNoAsString, String vHost, boolean isRecoverable) throws Exception;
	Channel createChannel(Connection connection, String queueName, boolean isDurable)
			throws RabbitMQCreateChannelException;
	void publishToAMQPQueue(final String queueName, String message, Connection connection) throws Exception;
	void pushToAMQPQueueWithBindingAndRoutingKey(final String queueName,
			final String routingKey, final String exchangeName, final String message, Connection connection) throws Exception;
	/*void consumeMessageFromQueue(Connection connection, String queueName, int prefetchCount, boolean autoAck,
			IMarkerRabitMQConsumer amqpConsumer) throws Exception;*/
	Channel createChannelWithBindingDetails(Connection connection, final String queueName, final String routingKey,
			final String exchangeName ) throws AMQPChannelCreationException;

}

