package com.companyApiTinyCache.companyConfigCache.service.common.template;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.List;
import java.util.Map;

/**
 * Template for executing jedis operations using jedisPool.
 * @param <T> - return type of template method {@link #execute(JedisPool, Object...)}
 */
public abstract class JedisExecutor<T> {

    private static final Log LOGGER = LogFactory.getLog(JedisExecutor.class);

    /**
     * Template method for executing jedis operation using jedisPool,
     * with pool handling as proposed in https://github.com/xetorthio/jedis/wiki/Getting-started.
     *
     * @param jedisPool - jedisPool
     * @param params - parameters which are passed as is to {@link #executeInternal(Jedis, Object...)}
     * @return result of {@link #executeInternal(Jedis, Object...)}
     */
    public T execute(JedisSentinelPool jedisPool, Object... params) {
        T result = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            result = executeInternal(jedis, params);
        } catch (JedisConnectionException e) {
            LOGGER.error(e);
            if (jedis != null) {
                jedisPool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (jedis != null) {
                jedisPool.returnResource(jedis);
            }
        }
        return result;
    }

    /**
     * Template method for executing jedis operation using jedisPool,
     * with pool handling as proposed in https://github.com/xetorthio/jedis/wiki/Getting-started.
     *
     * @param jedisPool - jedisPool
     * @param params - parameters which are passed as is to {@link #executeInternal(Jedis, Object...)}
     * @return result of {@link #executeInternal(Jedis, Object...)}
     */
    public T execute(JedisPool jedisPool, Object... params) {
        T result = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            result = executeInternal(jedis, params);
        } catch (JedisConnectionException e) {
            LOGGER.error(e);
            if (jedis != null) {
                jedisPool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            if (jedis != null) {
                jedisPool.returnResource(jedis);
            }
        }
        return result;
    }

    protected abstract T executeInternal(Jedis jedis, Object... params);

    public static JedisExecutor<String> hget() {
        return new JedisExecutor<String>() {
            @Override
            protected String executeInternal(Jedis jedis, Object... params) {
                return jedis.hget((String) params[0], (String) params[1]);
            }
        };
    }

    public static JedisExecutor<List<String>> hvals() {
        return new JedisExecutor<List<String>>() {
            @Override
            protected List<String> executeInternal(Jedis jedis, Object... params) {
                return jedis.hvals((String) params[0]);
            }
        };
    }

    public static JedisExecutor<Map<String, String>> hgetAll() {
        return new JedisExecutor<Map<String, String>>() {
            @Override
            protected Map<String, String> executeInternal(Jedis jedis, Object... params) {
                return jedis.hgetAll((String) params[0]);
            }
        };
    }
}
