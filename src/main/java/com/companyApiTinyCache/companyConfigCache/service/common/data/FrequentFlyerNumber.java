package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.cached.resources.FreqFlyerCompatibleAirlinesResource;
import com.companyApiTinyCache.companyConfigCache.service.common.cached.resources.FreqFlyerExcludedAirlinesResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.FrequentFlyerUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class FrequentFlyerNumber implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private int sequenceNo;

    private String airline;

    private String number;

    private String airlineCode;

    @JsonIgnore
    private String applicableAirlineCode;

    @JsonProperty("seq_no")
    public int getSequenceNo() {
        return sequenceNo;
    }

    @JsonProperty("seq_no")
    public void setSequenceNo(int sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    @JsonProperty("airline")
    public String getAirline() {
        return airline;
    }

    @JsonProperty("airline")
    public void setAirline(String airline) {
        this.airline = airline;
    }

    @JsonProperty("frequent_flyer_number_value")
    public String getNumber() {
        return number;
    }

    @JsonProperty("frequent_flyer_number_value")
    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("airline_code")
    public String getAirlineCode() {
        if (this.airlineCode != null)
            return this.airlineCode;

        String splitAirlineCode = StringUtils.substringBetween(this.airline, "(", ")");
        return splitAirlineCode;
    }

    @JsonProperty("airline_code")
    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    @JsonIgnore
    public String getApplicableAirlineCode() {
        return applicableAirlineCode;
    }

    @JsonIgnore
    public void setApplicableAirlineCode(String applicableAirlineCode) {
        this.applicableAirlineCode = applicableAirlineCode;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("FF num:").append(number).append(" - ").append("airline code:").append(airlineCode).append(" - ").append("airline:").append(airline).toString();
    }

    /**
     * @param ignoreAirlineName
     *            is used to skip strict empty check; if its true, just FF number & airlineCode need be blank for FFN to be considered empty
     */
    @JsonIgnore
    public boolean isEmpty(boolean ignoreAirlineName) {
        if (StringUtils.isBlank(number) && StringUtils.isBlank(airlineCode) && (ignoreAirlineName || StringUtils.isBlank(airline))) {
            return true;
        }

        return false;
    }

    /**
     * @param ignoreAirlineName
     *            is used to skip strict incompleteness check; if its true, FFN need have just number and airline code, to be considered as not incomplete
     */
    @JsonIgnore
    public boolean isIncomplete(boolean ignoreAirlineName) {
        if (StringUtils.isBlank(number) || StringUtils.isBlank(airlineCode) || (!ignoreAirlineName && StringUtils.isBlank(airline))) {
            return true;
        }

        return false;
    }

    @JsonIgnore
    public boolean isPermitted(boolean intl, boolean ignoreAirlineName) {
        if (this.isIncomplete(ignoreAirlineName)) {
            return false;
        }

        String trimmedThisAirlineCode = StringUtils.upperCase(StringUtils.trimToNull(this.airlineCode));
        String thisMktgAirlineCode = FrequentFlyerUtil.getFreqFlyerMktgAirlineCode(trimmedThisAirlineCode);

        Set<String> excludedAirlineCodes = intl ? FreqFlyerExcludedAirlinesResource.getExcludedIntlAirlineCodes() : FreqFlyerExcludedAirlinesResource.getExcludedDomAirlineCodes();

        return excludedAirlineCodes == null ? true : !excludedAirlineCodes.contains(thisMktgAirlineCode);
    }

    @JsonIgnore
    public boolean isIssuedBy(String airlineCode) {
        boolean ignoreAirlineName = true;

        if (this.isIncomplete(ignoreAirlineName)) { // strict incompleteness not required...
            return false;
        }

        String trimmedThisAirlineCode = StringUtils.upperCase(StringUtils.trimToNull(this.airlineCode));
        String trimmedGivenAirlineCode = StringUtils.upperCase(StringUtils.trimToNull(airlineCode));

        String thisMktgAirlineCode = FrequentFlyerUtil.getFreqFlyerMktgAirlineCode(trimmedThisAirlineCode);
        String givenMktgAirlineCode = FrequentFlyerUtil.getFreqFlyerMktgAirlineCode(trimmedGivenAirlineCode);

        if (thisMktgAirlineCode.equals(givenMktgAirlineCode)) { // null check for thisMktgAirlineCode not required since incompleteness check done before
            return true;
        }

        return false;
    }

    @JsonIgnore
    public boolean isApplicableTo(String airlineCode) {
        boolean ignoreAirlineName = true;

        if (this.isIncomplete(ignoreAirlineName)) { // strict incompleteness not required...
            return false;
        }

        String trimmedThisAirlineCode = StringUtils.upperCase(StringUtils.trimToNull(this.airlineCode));
        String trimmedGivenAirlineCode = StringUtils.upperCase(StringUtils.trimToNull(airlineCode));

        String thisMktgAirlineCode = FrequentFlyerUtil.getFreqFlyerMktgAirlineCode(trimmedThisAirlineCode);
        String givenMktgAirlineCode = FrequentFlyerUtil.getFreqFlyerMktgAirlineCode(trimmedGivenAirlineCode);

        // Give more preference to VFFD compatibility by doing it before doing equals check on airline-codes,
        // since when just airline equality is to be checked, isIssuedBy method can be used.

        Map<String, Set<String>> ffCompatibleAirlineCodesMap = FreqFlyerCompatibleAirlinesResource.getCompatibleAirlineCodesMap();
        Set<String> compatibleCodesForGivenAirlineCode = ffCompatibleAirlineCodesMap.get(givenMktgAirlineCode);

        if (compatibleCodesForGivenAirlineCode != null && compatibleCodesForGivenAirlineCode.contains(thisMktgAirlineCode)) {
            return true;
        }

        if (thisMktgAirlineCode.equals(givenMktgAirlineCode)) { // null check for thisMktgAirlineCode not required since incompleteness check done before
            return true;
        }

        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FrequentFlyerNumber)) {
            return false;
        }

        FrequentFlyerNumber ffn = (FrequentFlyerNumber) obj;
        boolean ignoreAirlineName = true;

        if (this.isIncomplete(ignoreAirlineName) || ffn.isIncomplete(ignoreAirlineName)) {
            return false;
        }

        String trimmedThisNumber = StringUtils.trimToNull(number);
        String trimmedGivenNumber = StringUtils.trimToNull(ffn.getNumber());

        if (!trimmedThisNumber.equals(trimmedGivenNumber)) { // null check for trimmedThisNumber not required since incompleteness check done before
            return false;
        }

        String trimmedThisAirlineCode = StringUtils.upperCase(StringUtils.trimToNull(airlineCode));
        String trimmedGivenAirlineCode = StringUtils.upperCase(StringUtils.trimToNull(ffn.getAirlineCode()));

        String thisMktgAirlineCode = FrequentFlyerUtil.getFreqFlyerMktgAirlineCode(trimmedThisAirlineCode);
        String givenMktgAirlineCode = FrequentFlyerUtil.getFreqFlyerMktgAirlineCode(trimmedGivenAirlineCode);

        if (!thisMktgAirlineCode.equals(givenMktgAirlineCode)) { // null check not required; DON'T put VFFD applicability here
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        String trimmedThisAirlineCode = StringUtils.upperCase(StringUtils.trimToEmpty(airlineCode));
        String thisMktgAirlineCode = FrequentFlyerUtil.getFreqFlyerMktgAirlineCode(trimmedThisAirlineCode);

        String trimmedThisNumber = StringUtils.trimToEmpty(number);

        return thisMktgAirlineCode.hashCode() + trimmedThisNumber.hashCode();
    }

}
