package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;

/**
 * Cached Resource Containing a URL Resource mapped to values of an Enum.
 * @author suresh
 *
 * @param <K> Enum Key Type
 * @param <V> Value Type
 */
public class CachedEnumUrlResources<K extends Enum<K> & EnumResourceName, V> extends CachedResource<UrlResources<K, V>> {

    private static final Log logger = LogFactory.getLog(CachedEnumUrlResources.class);

    protected CachedProperties commonCachedProperties;

    protected StreamResourceLoader<V> loader;

    private String url;

    protected String prevUrl;

    protected Class<K> enumClass;

    // Array of Enum Vals
    protected K[] enumVals;

    protected boolean[] isPrevError;

    protected String[] prevUrls;

    private String urlDigest;

    public CachedEnumUrlResources(Class<K> c, CachedProperties pcommonCachedProperties, boolean loadResource) {
        enumClass = c;
        enumVals = c.getEnumConstants();
        isPrevError = new boolean[enumVals.length];
        prevUrls = new String[enumVals.length];
        commonCachedProperties = pcommonCachedProperties;
        setName(c.getName());
        if (loadResource) {
            refreshResource();
        }
        try{
            List<String> enumPropertyValues = new ArrayList<>();
            for (K enumVal : Arrays.asList(enumVals)) {
                String resourceName = (enumVal.getResourceName().startsWith("$")) ? enumVal.getResourceName().substring(1) : enumVal.getResourceName();
                if (resourceName != null && getProperty(resourceName) != null && getProperty(resourceName).contains(FETCH_RESOURCE)) {
                    String resourceUrl = getProperty(resourceName);
                    if (resourceUrl != null && resourceUrl.indexOf('/') > 0) {
                        String substring = resourceUrl.substring(resourceUrl.lastIndexOf("/") + 1);
                        enumPropertyValues.add(substring);
                    }
                }
            }

            if (enumPropertyValues != null && enumPropertyValues.size() > 0) {
                String[] keysToRefreshForConsulEvent = enumPropertyValues.toArray(new String[enumPropertyValues.size()]);
                pcommonCachedProperties.registerConsulEventListener(keysToRefreshForConsulEvent, vertical, (key, value) -> {
                    logger.error("CT-CONFIG refresh event triggered for Key =" + key);
                    refreshResource(0, false);
                });
            }
        } catch (Exception exception) {
            logger.error("Failed to register consul event" + exception.getMessage());
        }

    }

    public CachedEnumUrlResources(Class<K> c, CachedProperties pcommonCachedProperties) {
        this(c, pcommonCachedProperties, true);
    }

    protected String getProperty(String name) {
        return commonCachedProperties.getPropertyValue(name);
    }

    /**
     * Calls loader.loadStreamResource.
     *
     * @param is
     *            InputStream
     * @return Resource
     * @throws Exception
     */
    protected V loadStreamResource(InputStream is, K resourceKey) throws Exception {
        return loader.loadStreamResource(is);
    }

    /**
     * Fetches Url Data as Strings and reloads if any of them has changed. {@inheritDoc}
     *
     * @see com.cleartrip.common.util.CachedResource#loadResource(boolean)
     */
    @Override
    protected UrlResources<K, V> loadResource(long cacheMillis, boolean force) throws Exception {
        boolean isloadEnabled = commonCachedProperties.getBooleanPropertyValue("ct.common.urlresources.enabled", true);
        boolean isDebug = logger.isDebugEnabled();
        if (!isloadEnabled) {
            logger.debug("LOAD NOT ENABLED");
            return null;
        }
        int i;
        urlDigest = null;
        RestUtil.setResetAuthentication(commonCachedProperties);
        UrlResources<K, V> newResource = null;
        EnumMap<K, V> newUrlData = null;
        StringBuilder urlBuf = new StringBuilder(128);
        String defaultUrl = getUrl();
        EnumMap<K, V> urlData = null;
        if (resource != null) {
            urlData = resource.getUrlData();
        }
        if (isDebug) {
            logger.debug("BASE URL = " + defaultUrl + " resouce = " + resource + " bean = " + this + " urlData = " + urlData);
        }
        if (getName() == null) {
            setName(defaultUrl);
        }
        boolean urlChanged = false;
        if (!defaultUrl.equals(prevUrl)) {
            urlChanged = true;
            if (isDebug) {
                logger.debug("BASE URL CHANGED FROM PREV URL = " + prevUrl);
            }
            if (getName() == prevUrl) {
                setName(defaultUrl);
            }
            prevUrl = defaultUrl;
        }
        urlBuf.append(defaultUrl);
        if (urlChanged || resource == null) {
            newUrlData = resource != null ? new EnumMap<K, V>(resource.getUrlData()) : new EnumMap<K, V>(enumClass);
            newResource = new UrlResources<K, V>(newUrlData);
        }
        int urlBufLen = urlBuf.length();
        StringBuilder rootBuf = new StringBuilder(128);
        i = urlBuf.indexOf("//");
        if (i > 0) {
            i = urlBuf.indexOf("/", i + 2);
            if (i > 0) {
                rootBuf.append(urlBuf, 0, i);
            }
        }
        int rootBufLen = rootBuf.length();
        String url;
        for (i = 0; i < enumVals.length; i++) {
            K resourceKey = enumVals[i];
            V resourceVal = null;
            String path = resourceKey.getResourceName();
            if (isDebug) {
                logger.debug("CHECKING KEY = " + resourceKey + " PATH = " + path);
                if (urlData != null) {
                    logger.debug("current value = '" + urlData.get(resourceKey));
                }
            }
            if (path == null) {
                continue;
            }
            int len = path.length();
            if (len > 0 && path.charAt(0) == '$') {
                url = getProperty(path.substring(1));
                if (prevUrls[i] != null && !prevUrls[i].equals(url)) {
                    isPrevError[i] = false;
                }
                prevUrls[i] = url;
            } else if (len > 0 && path.charAt(0) == '/') {
                url = rootBuf.append(path).toString();
                rootBuf.setLength(rootBufLen);
            } else {
                url = urlBuf.append(path).toString();
                urlBuf.setLength(urlBufLen);
            }
            if (isDebug) {
                logger.debug("using URL: " + url);
            }
            InputStream in = null;
            String digest = null;
            try {
                RestResponse response = RestUtil.get(url, null);
                int responseCode = response.getCode();
                // -1 check added to accommodate files
                if (responseCode != -1 && responseCode != 200) {
                    throw new IOException("Got error code: " + response.getCode() + ", message: " + response.getMessage());
                }
                byte[] b = response.getMessage().getBytes();
                if (enumVals.length == 1) {
                    // Compute Digest based on URL data instead of resource
                    digest = GenUtil.digestBytes(b);
                }
                in = new ByteArrayInputStream(b);
                isErrorOnLastLoad = false;
                resourceVal = loadStreamResource(in, resourceKey);
                isPrevError[i] = false;
            } catch (Exception e) {
                if (!isPrevError[i] || cacheMillis == 0) {
                    isPrevError[i] = true;
                    logger.error("Error accessing URL resource : " + url + " for resource name " + getName(), e);
                }
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (Exception e) {
                        in = null;
                    }
                }
            }
            if (resourceVal != null) {
                if (newResource == null) {
                    newUrlData = new EnumMap<K, V>(resource.getUrlData());
                    newResource = new UrlResources<K, V>(newUrlData);
                }
                newUrlData.put(resourceKey, resourceVal);
                urlDigest = digest;
            }
        }
        if (isDebug) {
            logger.debug(" RETURNING " + newResource);
        }
        return newResource;
    }

    // Considering only urlData for digest since createTime may differ.
    @Override
    protected String computeDigest() {
        String digest = urlDigest;
        if (digest == null) {
            UrlResources<K, V> currentResource = resource;
            if (currentResource != null) {
                EnumMap<K, V> urlData = currentResource.getUrlData();
                String description = getJson(urlData);
                digest = computeDigest(description);
            }
        }
        return digest;
    }

    /**
     * Getter for url. This can be overridden by subclasses.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Setter for url.
     *
     * @param purl
     *            the url to set
     */
    public void setUrl(String purl) {
        url = purl;
    }

    /**
     * Setter for loader.
     *
     * @param ploader
     *            the loader to set
     */
    public void setLoader(StreamResourceLoader<V> ploader) {
        loader = ploader;
    }
}
