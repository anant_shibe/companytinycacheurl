package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.io.ReusableByteArrayInputStream;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Controller for /common/memcached_page URL to check or delete a Cache Entry.
 *
 * @author suresh
 */
public class InspectMemcachedServerController extends AbstractController {

    private CacheFactory cacheFactory;

    private CachedProperties commonCachedProperties;

    // Positions stream at a point ready to read Object or String after discarding ESCAPE_MARKER/Object Marker
    private static int checkHeader(InputStream ip) throws IOException {
        ip.mark(2);
        int firstTwoBytes = ip.read();
        firstTwoBytes = (firstTwoBytes << 8) | ip.read();
        ip.reset();

        return firstTwoBytes;
    }

    private static Object deSerialize(byte [] b) throws IOException, ClassNotFoundException {
        ReusableByteArrayInputStream rip = new ReusableByteArrayInputStream(b);
        InputStream ip = rip;
        Object o = null;
        int header = checkHeader(ip);
        if (header == GenUtil.GZIP_HEADER) {
            ip = new BufferedInputStream(new GZIPInputStream(ip), 1024);
            header = checkHeader(ip);
        } else if (header != GenUtil.OBJECT_STREAM_HEADER) {
            // Treat it as String
            o = new String(b, GenUtil.UTF_8_CHARSET);
        }
        if (o == null) {
            switch(header) {
              case GenUtil.OBJECT_STREAM_HEADER:
                ObjectInputStream oip = new ObjectInputStream(ip);
                o = oip.readObject();
                break;
              default:
                Reader r = new InputStreamReader(ip, GenUtil.UTF_8_CHARSET);
                int charsRead;
                char [] cbuf = new char[128];
                StringBuilder sb = new StringBuilder(b.length * 4); // Estimating 4x compression
                while ((charsRead = r.read(cbuf)) > 0) {
                    sb.append(cbuf, 0, charsRead);
                }
                o = sb.toString();
                break;
            }
        }

        return o;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String property = StringUtils.trimToNull(request.getParameter("property"));
        String server = commonCachedProperties.getPropertyValue(property);
        String message = null;
        ModelAndView modelAndView = null;
        Map<String, Cache> propertyCacheMap = cacheFactory.getCacheMap();
        if (propertyCacheMap.get(property) == null) {
            message = "Property " + property + " is not defined. Please pass in a valid property parameter";
            if (server != null) {
                message = "There is no Memcached Connection on this server for property: " + property + ". Please try another server";
            }
        }
        String infoMessage = null;
        Object value = null;
        String key = null;
        if (message == null) {
            String action = request.getParameter("action");
            key = StringUtils.trimToNull(request.getParameter("key"));
            if (action != null) {
                Cache cache = cacheFactory.getCacheForServer(property);
                if ("del".equals(action)) {
                    Object checkVal = cache.get(key);
                    if (checkVal != null && cache.remove(key)) {
                        message = "Successfully removed key: " + key + " from cache";
                    } else {
                        message = "Key to be deleted: " + key + " not found in cache";
                    }
                } else if ("get".equals(action)) {
                    value = cache.get(key);
                    if (value == null) {
                        message = "Key: " + key + " not found in cache";
                    } else {
                        if (value instanceof byte[]) {
                            try {
                                infoMessage = "Original Type is byte[]";
                                value = (Serializable) deSerialize((byte[]) value);
                            } catch (Exception e) {
                                infoMessage = "Exception when trying to de-sereialize byte array: " + e.getMessage();
                                logger.error("Exception when trying to de-sereialize byte array", e);
                            }
                        }
                        if (value instanceof String) {
                            message = (String) value;
                        }
                    }
                }
            }
        }
        if (message == null && value == null) {
            modelAndView = new ModelAndView("memcachedPage", "property", property);
            modelAndView.addObject("server", server);
        } else {
            ByteArrayOutputStream bop = new ByteArrayOutputStream();
            OutputStream out = bop;
            if (message == null || message.length() > 1000) {
                String encodings = request.getHeader("Accept-Encoding");
                if (encodings != null && encodings.indexOf("gzip") != -1) {
                    out = new GZIPOutputStream(out);
                    response.setHeader("Content-Encoding", "gzip");
                }
            }
            PrintStream ps = new PrintStream(out);
            if (value != null) {
                String type = value.getClass().getName();
                ps.println("<b>Server:</b> " + server + " (Property: " + property + ")<br/>");
                ps.println("<b>Key:</b> " + key + "<br/>");
                if (infoMessage != null) {
                    ps.println("<b>Additional Info:</b> " + infoMessage + "<br/>");
                }
                ps.println("<b>Type:</b> " + type + "<br/>");
                ps.println("<b>Value:</b><br/>");
            }
            ps.println("<pre>");
            ps.flush();
            if (message != null) {
                ps.println(message);
            } else {
                JsonFactory jsonFactory = new JsonFactory();
                JsonGenerator jgen = jsonFactory.createGenerator(out, JsonEncoding.UTF8);
                jgen.useDefaultPrettyPrinter();
                ObjectMapper mapper = JsonUtil.getObjectMapper();
                mapper.writeValue(jgen, value);
            }
            ps.println("</pre>");
            ps.close();
            bop.writeTo(response.getOutputStream());
        }

        return modelAndView;
    }

    public final void setCacheFactory(CacheFactory pcacheFactory) {
        cacheFactory = pcacheFactory;
    }

    public final void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
    }

}
