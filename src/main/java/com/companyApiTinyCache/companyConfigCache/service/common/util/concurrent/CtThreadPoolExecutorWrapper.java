package com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * User: Naveen Date: 8/6/13.
 */
@ClassExcludeCoverage
public class CtThreadPoolExecutorWrapper implements InitializingBean, DisposableBean {
    private int corePoolSize = 1;
    private int maxPoolSize = Integer.MAX_VALUE;
    private int queueCapacity = Integer.MAX_VALUE;
    private ThreadPoolExecutor threadPoolExecutor;
    private boolean allowTimeout = false;

    public ThreadPoolExecutor getThreadPoolExecutor() {
        return threadPoolExecutor;
    }

    @Override
    public void destroy() throws Exception {
        threadPoolExecutor.shutdown();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initialize();
    }

    private void initialize() {
        this.threadPoolExecutor = new CtThreadPoolExecutor(corePoolSize, maxPoolSize, queueCapacity);
        if (allowTimeout) {
            this.threadPoolExecutor.allowCoreThreadTimeOut(true);
        }
    }

    @Required
    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    @Required
    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public void setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    public void setAllowTimeout(boolean allowTimeout) {
        this.allowTimeout = allowTimeout;
    }
}
