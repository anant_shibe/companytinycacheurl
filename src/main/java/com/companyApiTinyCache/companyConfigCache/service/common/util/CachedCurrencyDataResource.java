package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.data.CurrencyData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains Currency Data for each country.
 *
 * @author suresh
 */
public class CachedCurrencyDataResource extends CachedUrlResource<Map<String, CurrencyData>> {

    private static final Log logger = LogFactory.getLog(CachedUrlResource.class);

    private static final String CURRENCY_JS = "currency.js";

    private volatile CurrencyData defaultCurrencyData;

    public CachedCurrencyDataResource(CachedProperties pcommonCachedProperties) throws MalformedURLException {
        super(pcommonCachedProperties, "ct.services.currencyjs.url.ctconfig", null, false);
        refreshResource();
    }

    /**
     * @see com.cleartrip.common.util.CachedUrlResource#loadStreamResource(InputStream)
     */
    @Override
    protected Map<String, CurrencyData> loadStreamResource(InputStream is) throws Exception {
        Map<String, CurrencyData> currencyDataMap = new HashMap<String, CurrencyData>();
        Reader reader = new InputStreamReader(is);
        StringBuilder sb = new StringBuilder(128);
        char[] buf = new char[128];
        int len;
        while ((len = reader.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        reader.close();
        String json = sb.toString();
        int i = json.indexOf('[');
        int pos = json.indexOf(']');
        json = json.substring(i, pos + 1);
        json = json.replace("\\\\u", "\\u");
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        List<Map<String, Object>> currencyConversions = mapper.readValue(json, List.class);
        len = currencyConversions.size();
        for (i = 0; i < len; i++) {
            Map<String, Object> currencyConversionMap = currencyConversions.get(i);
            String toCurrency = ((String) currencyConversionMap.get("code")).trim().toUpperCase();
            sb.setLength(0);
            String currencyCode = toCurrency;
            Object rateObj = currencyConversionMap.get("rate");
            double rate;
            if (rateObj instanceof String) {
                rate = Double.parseDouble((String) rateObj);
            } else {
                rate = (Double) rateObj;
            }
            String symbol = (String) currencyConversionMap.get("symbol");
            String countryName = (String) currencyConversionMap.get("name");

            CurrencyData currencyData = new CurrencyData();
            currencyData.setCode(currencyCode);
            currencyData.setRate(rate);
            currencyData.setSymbol(symbol);
            currencyData.setCountryName(countryName);

            currencyDataMap.put(currencyCode, currencyData);
        }

        return currencyDataMap;
    }

    // Guaranteed to return default of INR
    public CurrencyData getCurrencyData(String currencyCode) {
        CurrencyData currencyData = null;
        currencyCode = StringUtils.trimToNull(currencyCode);
        if (currencyCode == null) {
            currencyCode = "INR";
        }
        Map<String, CurrencyData> currencyDataMap = getResource();
        if (currencyDataMap != null) {
            currencyData = currencyDataMap.get(currencyCode);
        }
        if (currencyData == null) {
            // Standard DCL
            if (defaultCurrencyData == null) {
                synchronized (this.getClass()) {
                    if (defaultCurrencyData == null) {
                        defaultCurrencyData = new CurrencyData();
                        defaultCurrencyData.setCode("INR");
                        defaultCurrencyData.setCountryName("India");
                        defaultCurrencyData.setRate(1);
                        defaultCurrencyData.setSymbol("Rs.");
                    }
                }
            }
            currencyData = defaultCurrencyData;
        }

        return currencyData;
    }


}
