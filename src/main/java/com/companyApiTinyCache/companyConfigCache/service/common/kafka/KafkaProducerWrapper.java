package com.companyApiTinyCache.companyConfigCache.service.common.kafka;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@ClassExcludeCoverage
public class KafkaProducerWrapper {
    private Producer<String, byte[]> producer;

    public KafkaProducerWrapper(String brokerList, boolean isAsync) {
        Properties props = new Properties();
        props.put("metadata.broker.list", brokerList);
        if (isAsync) {
        props.put("producer.type", "async");
        } else {
        	 props.put("producer.type", "sync");
        }
        props.put("compression.codec", "1");

        ProducerConfig config = new ProducerConfig(props);
        producer = new Producer<String, byte[]>(config);
    }

    public void send(byte[] data, String topic) {
        KeyedMessage<String, byte[]> message = new KeyedMessage<String, byte[]>(topic, data);
        producer.send(message);
    }

    public void sendMulti(List<byte[]> dataList, String topic) {
        List<KeyedMessage<String, byte[]>> messages = new ArrayList<KeyedMessage<String, byte[]>>();

        for (byte[] data : dataList) {
            KeyedMessage<String, byte[]> message = new KeyedMessage<String, byte[]>(topic, data);
            messages.add(message);
        }

        producer.send(messages);
    }

    public void close() {
        producer.close();
    }

}