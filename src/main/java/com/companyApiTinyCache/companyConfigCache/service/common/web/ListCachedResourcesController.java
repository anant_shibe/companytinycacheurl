/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Suresh
 * 
 */
public class ListCachedResourcesController extends AbstractController {

    private static final Log log = LogFactory.getLog(ListCachedResourcesController.class);

    public ListCachedResourcesController() {
        setCacheSeconds(0);
    }

    private ApplicationContext getContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    private void getCachedResourceBeanNames(ApplicationContext ctx, Map<String, CachedResource> beanNames) {
        if (ctx != null) {
            Map<String, CachedResource> cachedResourceBeans = ctx.getBeansOfType(CachedResource.class, false, false);
            if (cachedResourceBeans != null && cachedResourceBeans.size() > 0) {
                beanNames.putAll(cachedResourceBeans);
            }
            getCachedResourceBeanNames(ctx.getParent(), beanNames);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.mvc.AbstractFormController#handleRequestInternal(HttpServletRequest, HttpServletResponse)
     */
    @Override
    public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ApplicationContext ctx = getContext();
        Map<String, CachedResource> allBeansMap = new TreeMap<String, CachedResource>();
        getCachedResourceBeanNames(ctx, allBeansMap);
        int i, len = allBeansMap.size();

        long now = System.currentTimeMillis();
        Map<String, Map<String, Map<String, Object>>> packageMap = new TreeMap<String, Map<String, Map<String, Object>>>();
        for (String beanName : allBeansMap.keySet()) {
            String packageName = getPackageName(allBeansMap.get(beanName));
            Map<String, Map<String, Object>> beanMap = packageMap.get(packageName);
            if (beanMap == null) {
                beanMap = new TreeMap<String, Map<String, Object>>();
                packageMap.put(packageName, beanMap);
            }

            final CachedResource cachedResource = allBeansMap.get(beanName);
            Map<String, Object> values = new HashMap<String, Object>();
            values.put("resourceName", cachedResource.getName());
//            values.put("description", cachedResource.getResourceDescription());
            values.put("checked", getDuration(now - cachedResource.getLastCheckTimestamp()));
            values.put("refreshed", getDuration(now - cachedResource.getLastRefreshTimestamp()));
            values.put("inspect", "/common/inspect_resource?beanName="+beanName);
            values.put("refresh", "/common/refresh_resource?beanName="+beanName);
            beanMap.put(beanName, values);

        }

        String json = new ObjectMapper().writeValueAsString(packageMap);

        final PrintWriter writer = response.getWriter();
        writer.println(json);
        return null;
    }

    private String getPackageName(CachedResource cachedResource) {
        String className = cachedResource.getClass().getName();
        className = className.substring(14);// Remove com.cleartrip.
        String packageName = "";
        int index = className.indexOf('.');
        if (index > 0) {
            packageName = className.substring(0, index);
        }
        return packageName;
    }

    private static String getDuration(long time) {
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        long seconds = (time + 500)/1000;
        long mins = seconds/60;
        seconds %= 60;
        long hours = mins/60;
        mins %= 60;
        if(hours > 0) {
            sb.append(hours).append("h ");
        }
        if(mins > 0) {
            sb.append(mins).append("m ");
        }
        sb.append(seconds).append("s ago");
        return sb.toString();
    }
}
