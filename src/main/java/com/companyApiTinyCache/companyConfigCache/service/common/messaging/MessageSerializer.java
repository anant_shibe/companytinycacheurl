package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

public interface MessageSerializer {

    Object serialize(Object o) throws Exception;

    Object deSerialize(Object s) throws Exception;
}
