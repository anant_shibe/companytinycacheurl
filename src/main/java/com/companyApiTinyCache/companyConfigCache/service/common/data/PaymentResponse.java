package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;
import java.util.Map;

/**
 * Response from Payment module. Will contain the status and redirection info if redirection is required.
 * 
 * @author Cleartrip
 */
public class PaymentResponse implements Serializable {

    private static final long serialVersionUID = -5663803819697848569L;

    private Map<Long, PaymentStatus> paymentStatusMap;

    private String xmlResponse;

    private int responseCode;

    /**
     * @return the responseCode
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public Map<Long, PaymentStatus> getPaymentStatusMap() {
        return paymentStatusMap;
    }

    public void setPaymentStatusMap(Map<Long, PaymentStatus> paymentStatusMap) {
        this.paymentStatusMap = paymentStatusMap;
    }

    public String getXmlResponse() {
        return xmlResponse;
    }

    public void setXmlResponse(String xmlResponse) {
        this.xmlResponse = xmlResponse;
    }
}
