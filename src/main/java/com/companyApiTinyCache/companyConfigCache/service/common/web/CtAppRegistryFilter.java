package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.data.ServerInfo;
import com.companyApiTinyCache.companyConfigCache.service.common.data.ServerInfoBasic;
import com.companyApiTinyCache.companyConfigCache.service.common.data.ThisServer;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class CtAppRegistryFilter implements Filter {

    private static final Log log = LogFactory.getLog(CtAppRegistryFilter.class);

    private static ThreadLocal<String> app = new ThreadLocal<String>();

    public static final String CALL_TRACE_HEADER = "ct-caller-info";

    public static final String CALL_TRACE_PROPERTY = "ct.common.appregistry.call-trace.enabled";

    protected ThisServer thisServer;

    protected CachedProperties commonCachedProperties;

    /*
     * Allows test cases to override where application context obtained from.
     * 
     * @param filterConfig which can be used to find the <code>ServletContext</code>
     * 
     * @return the Spring application context
     */
    protected ApplicationContext getContext(FilterConfig filterConfig) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        doInit(filterConfig);
    }

    /**
     * This doInit() gets the bean from the application context.
     * 
     * @throws ServletException
     */
    public void doInit(FilterConfig filterConfig) throws ServletException {
        ApplicationContext ctx = getContext(filterConfig);
        Object beanObj = ctx.getBean("thisServer");
        if (beanObj instanceof ThisServer) {
            thisServer = (ThisServer) beanObj;
            RestUtil.setThisServer(thisServer);
        }
        beanObj = ctx.getBean("commonCachedProperties");
        if (beanObj instanceof CachedProperties) {
            commonCachedProperties = (CachedProperties) beanObj;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        ServerInfo serverInfo = null;
        String currentApp = null;
        if (thisServer != null) {
            serverInfo = thisServer.getServerInfo();
            ServerInfoBasic basicInfo = serverInfo.getBasicInfo();
            if (serverInfo != null) {
                int port = basicInfo.getRuntimePort();
                HttpServletRequest httpReq = (HttpServletRequest) request;
                currentApp = System.getProperty("app.context.path");
                if (port <= 0) {
                    port = httpReq.getLocalPort();
                    basicInfo.setRuntimePort(port);
                }
                boolean callTraceEnabled = commonCachedProperties.getBooleanPropertyValue(CALL_TRACE_PROPERTY, false);
                RestUtil.setCallTraceEnabled(callTraceEnabled);
                if (callTraceEnabled) {
                    String callerInfoHeader = httpReq.getHeader(CALL_TRACE_HEADER);
                    serverInfo.getCallerInfo().addCaller(callerInfoHeader, httpReq.getRequestURI());
                }
            }
        }

        try {
            if (currentApp != null) {
                app.set(currentApp);
            }
            chain.doFilter(request, response);
        } finally {
            if (currentApp != null) {
                app.remove();
            }
        }
    }

    @Override
    public void destroy() {
    }

    public static final String getCurrentApp() {
        return app.get();
    }

}
