package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CurrencyData;
import com.companyApiTinyCache.companyConfigCache.service.common.service.CalendarSearchService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedCurrencyDataResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @author cleartrip
 *
 */
public class CalendarSearchController extends BaseController {

    private CalendarSearchService calendarSearchService;

    private CachedCurrencyDataResource cachedCurrencyDataResource;

    private CachedProperties cachedProperties;

    public CalendarSearchController(CachedProperties pcachedProperties, CachedCurrencyDataResource pcachedCurrencyDataResource) {
        this.cachedProperties = pcachedProperties;
        this.cachedCurrencyDataResource = pcachedCurrencyDataResource;
    }

    public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // This method decides which service is to be used based on a switch.
       // setService();

        String from = request.getParameter("from");
        String to = request.getParameter("to");

        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String country = request.getParameter("country");

        String startDate = request.getParameter("start_date");
        String endDate = request.getParameter("end_date");

        String roundtripReturnAfter = request.getParameter("return_after");
        String cheapestFareOnly = request.getParameter("cheapest_only");
        String sourceType = request.getParameter("source");
        if (sourceType == null) {
            sourceType = "B2C";
        }

        String version = request.getParameter("ver");

        String countryCode = (String) request.getAttribute(CommonConstants.COUNTRY);

        String currency = (String) request.getAttribute(CommonConstants.SELL_CURRENCY);

        CurrencyData currencyData = cachedCurrencyDataResource.getResourceValue().get(currency);
        String symbol = currencyData.getSymbol();

        String jsonData = null;

        boolean cheapestOnly = false;
        if (StringUtils.isNotBlank(cheapestFareOnly) && ("true".equalsIgnoreCase(cheapestFareOnly) || "1".equalsIgnoreCase(cheapestFareOnly))) {
                cheapestOnly = true;
        }

        if (request.getRequestURI().contains("flights")) {
            jsonData = calendarSearchService.generateJsonString(from, to, startDate, endDate, roundtripReturnAfter, countryCode, cheapestOnly, version, sourceType, false);

            // /calendar/stub.json will return the old json & /calendar/calendarstub.json will return the new json (changed for internationalization).
            if (request.getRequestURL().toString().contains("calendarstub.json")) {
                // JSON format changed to support internationalization.
                jsonData = "{" + "\"calendar_json\":" + jsonData + "," + "\"sell_currency_json\":{ \"code\":\"" + countryCode + "\",\"symbol\":\"" + symbol + "\" }" + "}";
            }

        } else if (request.getRequestURI().contains("hotels")) {

            String jsn = calendarSearchService.getHotelCalendarJson(city, state, country, startDate, endDate, currency);
            jsonData = "{\"calendar_json\":" + jsn + ",\"sell_currency_json\":{\"code\":\"" + countryCode + "\",\"symbol\":\"" + symbol + "\"}}";
        }

        response.setContentType("text");
        PrintWriter responseWriter = new PrintWriter(response.getWriter());
        responseWriter.println(jsonData);
        responseWriter.flush();
        responseWriter.close();

        return null;

    }

    public CalendarSearchService getCalendarSearchService() {
        return calendarSearchService;
    }

    public void setCalendarSearchService(CalendarSearchService calendarSearchService) {
        this.calendarSearchService = calendarSearchService;
    }

    /*private void setService() {
        ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        if (cachedProperties.getBooleanPropertyValue("ct.common.calendar.new.service.enable", false)) {
            this.calendarSearchService = (CalendarSearchService) ctx.getBean("newCalendarSearchService");
        } else {
            this.calendarSearchService = (CalendarSearchService) ctx.getBean("calendarSearchService");
        }
    }*/

}
