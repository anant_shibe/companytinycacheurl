package com.companyApiTinyCache.companyConfigCache.service.common.rule;

/**
 * @author vijay
 * @param <T> represents the required type.
 */
public abstract class RedisCommandCache<T> {
    protected T resultCache;

    public abstract T transform(Object redisResult);

    public T getResultCache() {
        return resultCache;
    }
}
