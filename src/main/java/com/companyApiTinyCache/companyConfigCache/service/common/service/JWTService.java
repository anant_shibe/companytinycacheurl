package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.data.UserProfile;
import com.companyApiTinyCache.companyConfigCache.service.common.exceptions.JWTException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Sudhanshu
 *
 */
public interface JWTService {
	public String getToken(UserProfile pUserProfile);
	public void validateToken(HttpServletRequest request) throws JWTException;
	public UserProfile getUserProfile(String token) throws JWTException;
}
