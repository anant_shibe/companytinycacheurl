package com.companyApiTinyCache.companyConfigCache.service.common.analytics.cache;

import java.util.Map;
import java.util.Optional;

public interface AnalyticsCachingService {
    Optional<Map> getAnalyticsAttributeFromCache(String cacheKey);
    void setAnalyticsAttributeInCache(String key, Map analyticsAttributeMap);
}
