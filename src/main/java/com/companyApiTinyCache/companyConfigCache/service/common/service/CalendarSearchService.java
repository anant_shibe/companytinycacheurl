package com.companyApiTinyCache.companyConfigCache.service.common.service;

/**
 * @author cleartrip
 *
 */
public interface CalendarSearchService {

    String generateJsonString(String from, String to, String startDate, String endDate, String roundtripReturnAfter,
            String countryCode, boolean cheapestOnly, String version, String sourceType, boolean sendAirline);

    String getHotelCalendarJson(String city, String state, String country, String startDate, String endDate, String sellingCurrency);

    void insertCalendarData(String key, String value, int expiryTime);

}
