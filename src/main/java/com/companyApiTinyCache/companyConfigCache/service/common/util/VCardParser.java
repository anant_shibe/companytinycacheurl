package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.data.CompanyDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.data.UserProfile;
import net.sf.vcard4j.java.AddressBook;
import net.sf.vcard4j.java.VCard;
import net.sf.vcard4j.parser.DomParser;
import net.sf.vcard4j.parser.VCardParseException;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VCardParser extends ImportParser {

    public List<UserProfile> parseVCard(byte[] cardFile) {
        ArrayList<UserProfile> importedUserList = new ArrayList<UserProfile>();

        DomParser domParser = new net.sf.vcard4j.parser.DomParser();
        Document document = new DocumentImpl();
        try {
            domParser.parse(new ByteArrayInputStream(cardFile), document);
        } catch (VCardParseException e) {
            logger.error("VCardParseException while parsing the vcard.");
        } catch (IOException e) {
            logger.error("IOException while parsing the vcard.");
        }
        AddressBook addressBook = new AddressBook(document);
        Iterator vCards = addressBook.getVCards();
        for (; vCards.hasNext();) {
            VCard vcard = (VCard) vCards.next();
            UserProfile userProfile = new UserProfile();
            CompanyDetail companyDetail = new CompanyDetail();
            userProfile.getCompanyDetailsFromList().add(companyDetail);

            assignFullName(vcard, userProfile);
            assignPhoneNumbers(vcard, userProfile);
            assignEmails(vcard, userProfile);
            assignTitle(vcard, userProfile);
            assignEmployeeId(vcard, userProfile);
            assignAddress(vcard, userProfile);

            importedUserList.add(userProfile);
        }
        return importedUserList;
    }
}
