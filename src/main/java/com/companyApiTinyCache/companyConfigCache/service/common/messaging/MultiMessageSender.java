package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Wrapper to send same message via multiple senders.
 */
public class MultiMessageSender implements MessageSender {
    private static final Log logger = LogFactory.getLog(MultiMessageSender.class);

    private final List<MessageSender> senders;

    /**
     * Initialises new message senders.
     * 
     * @param senders
     *            the senders
     */
    public MultiMessageSender(List<MessageSender> senders) {
        this.senders = senders;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cleartrip.common.messaging.MessageSender#send(java.lang.Object)
     */
    @Override
    public void send(Object message) {
    	if (senders != null) {
	        for (MessageSender sender : senders) {
	            try {
	                sender.send(message);
	            } catch (Exception e) {
	                logger.error("While sending message : ", e);
	            }
	        }
    	}
    }
}
