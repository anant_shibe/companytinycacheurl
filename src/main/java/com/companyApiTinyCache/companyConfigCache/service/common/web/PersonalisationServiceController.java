package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.service.PersonalizationService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.SecurityBean;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;

@ClassExcludeCoverage
public class PersonalisationServiceController extends AbstractController {

    private static final Log logger = LogFactory.getLog(PersonalisationServiceController.class);
    private PersonalizationService personalizationService;

    public PersonalisationServiceController(PersonalizationService pPersonalizationService) {
        personalizationService = pPersonalizationService;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // TODO Auto-generated method stub

        logger.info("Entered Personalization service controller");
        String jsonRetrieved = null;
        String operation = request.getParameter("op");// operation to be performed in personalization service
        String personalisationServiceJsonString = request.getHeader("prs-json");// personalization service json to update
        String userId = request.getParameter("uid");// user id
        String productType = request.getParameter("pt");// product type
        String elementType = request.getParameter("et");// element type
        String source = request.getParameter("source");

        if (!StringUtils.isNotBlank(userId)) {
            logger.info("userId in personalization service is null");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }

        PersonalizationGetOpBean personalizationGetOps = new PersonalizationGetOpBean();
        personalizationGetOps.setElementType(elementType);
        personalizationGetOps.setProductType(productType);
        personalizationGetOps.setUserId(userId);

        if (StringUtils.isNotBlank(operation)) {
            if (operation.equalsIgnoreCase("get")) {
                SecurityBean securityBean = (SecurityBean) request.getAttribute(CommonEnumConstants.SESSION_SECURITY_BEAN_ID);
                boolean isConfirmed = false;
                if (securityBean != null) {
                    isConfirmed = securityBean.isConfirmedUser();
                    if (logger.isInfoEnabled()) {
                        logger.info("isConfirmedUser--->" + isConfirmed);
                    }
                }
                jsonRetrieved = personalizationService.get(personalizationGetOps, isConfirmed);
            } else if (operation.equalsIgnoreCase("put")) {
                if (!StringUtils.isNotBlank(personalisationServiceJsonString)) {
                    logger.info("input json in personalization service is null");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return null;
                }
                jsonRetrieved = personalizationService.put(userId, personalisationServiceJsonString, source);
                // also return a get data
            } else {
                logger.info("We support only get and put in our personalisation service for now.");
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return null;
            }
        } else {
            logger.info("Operation to be performed in personalisation service is not mentioned");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }

        StringWriter sw = new StringWriter();
        sw.append(jsonRetrieved);
        PrintWriter pw = response.getWriter();
        pw.println(sw);
        pw.flush();
        pw.close();

        return null;
    }

}
