package com.companyApiTinyCache.companyConfigCache.service.common.util;

public interface VmResourceInfo extends EnumResourceName {
    enum TemplateType {
        REGULAR, XML, HTML
    };

    TemplateType getTemplateType();
}
