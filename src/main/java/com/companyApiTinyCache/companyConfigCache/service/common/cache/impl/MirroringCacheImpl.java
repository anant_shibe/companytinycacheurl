package com.companyApiTinyCache.companyConfigCache.service.common.cache.impl;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.*;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.lang.StringUtils;

import java.nio.charset.Charset;
import java.util.Map;

/**
 * Class which wraps two caches one of which is a mirror. It is a wrapper over the main cache and in addition
 * duplicates the write calls to the mirror cache.
 *
 * @author suresh
 */
public class MirroringCacheImpl extends CacheBase {

    private static class ValueUpdaterWrapper implements ValueUpdater {

        private ValueUpdater updater;

        private Object newValue;

        public ValueUpdaterWrapper(ValueUpdater pupdater) {
            updater = pupdater;
        }

        @Override
        public Object update(Object value, int tryIndex) {
            newValue = updater.update(value, tryIndex);
            return newValue;
        }

        public Object getNewValue() {
            return newValue;
        }

    }

    private Cache cache;

    private Cache mirrorCache;

    private CachedProperties commonCachedProperties;

    private String mirrorCacheProperty;

    public MirroringCacheImpl(Cache pcache, Cache pmirrorCache, CachedProperties pcommonCachedProperties, String pmirrorCacheProperty) {
        cache = pcache;
        mirrorCache = pmirrorCache;
        commonCachedProperties = pcommonCachedProperties;
        mirrorCacheProperty = pmirrorCacheProperty;
    }

    @Override
    public Object getNativeConnection(String key) {
        return cache.getNativeConnection(key);
    }

    @Override
    public void returnNativeConnection(String key, Object jedis) {
        cache.returnNativeConnection(key, jedis);
    }

    @Override
    public Object get(String key, Charset charset) throws CacheRetrieveException {
        return cache.get(key, charset);
    }

    @Override
    public Map<String, Object> getMulti(String[] ids, Charset charset)
            throws CacheRetrieveException {
        return cache.getMulti(ids, charset);
    }

    @Override
    public boolean put(String key, Object val, int expirySeconds) throws CacheStoreException {
        boolean result = cache.put(key, val, expirySeconds);
        if (!StringUtils.isBlank(commonCachedProperties.getPropertyValue(mirrorCacheProperty))) {
            mirrorCache.put(key, val, expirySeconds);
        }
        return result;
    }

    @Override
    public boolean remove(String key) {
        boolean result = cache.remove(key);
        if (!StringUtils.isBlank(commonCachedProperties.getPropertyValue(mirrorCacheProperty))) {
            mirrorCache.remove(key);
        }
        return result;
    }

    @Override
    public void clear() throws CacheDeleteException {
        cache.clear();
        if (!StringUtils.isBlank(commonCachedProperties.getPropertyValue(mirrorCacheProperty))) {
            mirrorCache.clear();
        }
    }

    @Override
    public boolean add(String key, Object val, int expirySeconds)
            throws CacheStoreException {
        boolean result = cache.add(key, val, expirySeconds);
        if (result && !StringUtils.isBlank(commonCachedProperties.getPropertyValue(mirrorCacheProperty))) {
            mirrorCache.put(key, val, expirySeconds);
        }
        return result;
    }

    @Override
    public LockStatus lock(String key, int expirySeconds, boolean verifyFailedLock) {
        return cache.lock(key, expirySeconds, verifyFailedLock);
    }

    @Override
    public boolean atomicUpdate(String id, int expirySeconds, ValueUpdater updater, int maxTries) {
        if (updater == null) {
            throw new NullPointerException("Null Updater");
        }
        ValueUpdaterWrapper updaterWrapper = new ValueUpdaterWrapper(updater);
        boolean result = cache.atomicUpdate(id, expirySeconds, updaterWrapper, maxTries);
        if (result && !StringUtils.isBlank(commonCachedProperties.getPropertyValue(mirrorCacheProperty))) {
            Object newValue = updaterWrapper.getNewValue();
            if (newValue != ValueUpdater.SKIP_UPDATE) {
                if (newValue != null && expirySeconds >= 0) {
                    mirrorCache.put(id, newValue, expirySeconds);
                } else {
                    mirrorCache.remove(id);
                }
            }
        }
        return result;
    }

    @Override
    public long getServerTime() {
        return cache.getServerTime();
    }

    @Override
    public Long incrBy(String key, long integer) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long incr(String key) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long decr(String key) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long decrBy(String key, long integer) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

}
