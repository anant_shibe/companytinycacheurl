package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

public class AffiliateImport {

    private int id;

    private int importerId;

    @JsonIgnore
    private byte[] importFile;

    private String importFileString;

    @JsonIgnore
    private int affiliateId;

    private int totalVcards;

    private int importedVcards;

    private int rejectedVcards;

    private int statusId;

    private Date createdAt;

    private Date updatedAt;

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("importer_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getImporterId() {
        return importerId;
    }

    @JsonProperty("importer_id")
    public void setImporterId(int importerId) {
        this.importerId = importerId;
    }

    @JsonIgnore
    public byte[] getImportFile() {
        return importFile;
    }

    @JsonIgnore
    public void setImportFile(byte[] importFile) {
        importFileString = new String(importFile);
        this.importFile = importFile;
    }

    @JsonProperty("import_file")
    public String getImportFileString() {
        return importFileString;
    }

    @JsonProperty("import_file")
    public void setImportFileString(String importFileString) {
        importFile = importFileString.getBytes();
        this.importFileString = importFileString;
    }

    @JsonIgnore
    public int getAffiliateId() {
        return affiliateId;
    }

    @JsonIgnore
    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    @JsonProperty("total_vcards")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getTotalVcards() {
        return totalVcards;
    }

    @JsonProperty("total_vcards")
    public void setTotalVcards(int totalVcards) {
        this.totalVcards = totalVcards;
    }

    @JsonProperty("imported_vcards")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getImportedVcards() {
        return importedVcards;
    }

    @JsonProperty("imported_vcards")
    public void setImportedVcards(int importedVcards) {
        this.importedVcards = importedVcards;
    }

    @JsonProperty("rejected_vcards")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getRejectedVcards() {
        return rejectedVcards;
    }

    @JsonProperty("rejected_vcards")
    public void setRejectedVcards(int rejectedVcards) {
        this.rejectedVcards = rejectedVcards;
    }

    @JsonProperty("status_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getStatusId() {
        return statusId;
    }

    @JsonProperty("status_id")
    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    @JsonProperty("created_date")
    public Date getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("craeted_date")
    public void setCreatedAt(Date createDate) {
        this.createdAt = createDate;
    }

    @JsonProperty("updated_date")
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_date")
    public void setUpdatedAt(Date updateDate) {
        this.updatedAt = updateDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AffiliateImport == false)
            return false;
        AffiliateImport affImport = (AffiliateImport) obj;

        if (importerId != affImport.getImporterId()) {
            return false;
        }
        if ((importFile != null && affImport.getImportFile() != null) && !GenUtil.equals(importFile, affImport.getImportFile())) {
            return false;
        }
        if (affiliateId != affImport.getAffiliateId()) {
            return false;
        }
        if (totalVcards != affImport.getTotalVcards()) {
            return false;
        }
        if (importedVcards != affImport.getImportedVcards()) {
            return false;
        }
        if (rejectedVcards != affImport.getRejectedVcards()) {
            return false;
        }
        if (statusId != affImport.getStatusId()) {
            return false;
        }

        return true;
    }

}
