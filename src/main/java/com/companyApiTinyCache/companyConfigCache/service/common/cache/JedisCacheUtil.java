package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

public class JedisCacheUtil {


    public static void validateJedisCache(JedisCacheFactory jedisCacheFactory, String serverProperty){
        JedisCache cache = null;
        try {
            cache = jedisCacheFactory.getCache(serverProperty);
            cache.getJedis().ping();
        }finally {
            jedisCacheFactory.releaseJedisPool(serverProperty, cache);
        }
    }

    public static void validateJedisSentinel(JedisCacheFactory jedisCacheFactory, String serverProperty) throws Exception {
        JedisSentinelPool jedisSentinelPool = null;
        Jedis cache = null;
        try{
            jedisSentinelPool = jedisCacheFactory.getSentinelPool(serverProperty);
            cache = jedisSentinelPool.getResource();
        } finally {
           if(cache != null){
               jedisSentinelPool.returnResource(cache);
           }
        }
    }
}
