package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.service.CommonUtilityService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CompanyResourceUpdateController extends AbstractController {

    private CommonUtilityService commonUtilityService;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object o = request.getParameter("company-id");
        String resourceType = request.getParameter("resource-type");
        if (o == null) {
            throw new Exception("company-id parameter is blank or empty");
        }
        int companyId = Integer.parseInt(o.toString());
        commonUtilityService.updateCompanyResourcesInFileSystem(companyId, resourceType);
        return null;
    }

    public void setCommonUtilityService(CommonUtilityService commonUtilityService) {
        this.commonUtilityService = commonUtilityService;
    }

}
