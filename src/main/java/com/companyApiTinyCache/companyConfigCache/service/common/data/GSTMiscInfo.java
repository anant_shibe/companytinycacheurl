/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;


import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


/**
 * @author sumanth
 *
 */
@ClassExcludeCoverage
public class GSTMiscInfo implements Serializable {
    
    private static final long serialVersionUID = 6740284950717787126L;
    private String gstStates;
    private boolean isGstEnabled;
    private boolean tripHasGst;
    private boolean prePopulateGstDetailsEnabled;
    private boolean gstBlockEnabled;
    private boolean stateEnabled;
    private String gstNumberPattern;
    private String defaultStateCode;
    private boolean gstAddressEnabled;
    
	public boolean isGstAddressEnabled() {
		return gstAddressEnabled;
	}

	public void setGstAddressEnabled(boolean gstAddressEnabled) {
		this.gstAddressEnabled = gstAddressEnabled;
	}

	public String getDefaultStateCode() {
		return defaultStateCode;
	}

	public void setDefaultStateCode(String defaultStateCode) {
		this.defaultStateCode = defaultStateCode;
	}

	@JsonProperty("gst_enabled")
    public boolean isGstEnabled() {
        return isGstEnabled;
    }

    @JsonProperty("gst_enabled")
    public void setGstEnabled(boolean isGstEnabled) {
        this.isGstEnabled = isGstEnabled;
    }

    @JsonProperty("trip_has_gst")
    public boolean tripHasGst() {
        return tripHasGst;
    }

    @JsonProperty("trip_has_gst")
    public void setTripHasGst(boolean tripHasGst) {
        this.tripHasGst = tripHasGst;
    }

    @JsonProperty("pre_populate_gst")
    public boolean prePopulateGstDetailsEnabled() {
        return prePopulateGstDetailsEnabled;
    }

    @JsonProperty("pre_populate_gst")
    public void setPrePopulateGstEnabled(boolean prePopulateGstDetailsEnabled) {
        this.prePopulateGstDetailsEnabled = prePopulateGstDetailsEnabled;
    }

    @JsonProperty("gst_states")
    public String getGstStates() {
        return gstStates;
    }

    @JsonProperty("gst_states")
    public void setGstStates(String gstStates) {
        this.gstStates = gstStates;
    }
    
    @JsonProperty("show_gst")
    public boolean isGstBlockEnabled() {
        return gstBlockEnabled;
    }

    @JsonProperty("show_gst")
    public void setGstBlockEnabled(boolean gstBlockEnabled) {
        this.gstBlockEnabled = gstBlockEnabled;
    }

    @JsonProperty("show_state")
    public boolean isStateEnabled() {
        return stateEnabled;
    }

    @JsonProperty("show_state")
    public void setStateEnabled(boolean stateEnabled) {
        this.stateEnabled = stateEnabled;
    }

    @JsonProperty("gstn_pattern")
	public String getGstNumberPattern() {
		return gstNumberPattern;
	}

    @JsonProperty("gstn_pattern")
	public void setGstNumberPattern(String gstNumberPattern) {
		this.gstNumberPattern = gstNumberPattern;
	}
}
