package com.companyApiTinyCache.companyConfigCache.service.common.web.metrics;

import com.companyApiTinyCache.companyConfigCache.service.common.metrics.CTMetricsRegistry;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.servlets.MetricsServlet;

public class MetricsServletContextListener extends MetricsServlet.ContextListener {

    @Override
    protected MetricRegistry getMetricRegistry() {
        return CTMetricsRegistry.getMetricRegistry();
    }

}
