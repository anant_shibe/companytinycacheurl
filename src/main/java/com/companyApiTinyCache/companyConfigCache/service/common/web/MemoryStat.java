package com.companyApiTinyCache.companyConfigCache.service.common.web;

import java.util.ArrayList;

public class MemoryStat {
    private long minMemory = Long.MAX_VALUE;
    private long maxMemory = 0L;
    private long minUsed = Long.MAX_VALUE;
    private long maxUsed = 0L;
    private long minFree = Long.MAX_VALUE;
    private long maxFree = 0L;
    private long minTime = Long.MAX_VALUE;
    private long maxTime = 0L;

    public static MemoryStat getMemoryStat(ArrayList<Memory> memories) {
        long minUsed = Long.MAX_VALUE;
        long maxUsed = 0L;
        long minFree = Long.MAX_VALUE;
        long maxFree = 0L;
        long minTime = Long.MAX_VALUE;
        long maxTime = 0L;
        for (Memory memory : memories) {
            if (minUsed > memory.getUsedMB())
                minUsed = memory.getUsedMB();
            if (maxUsed < memory.getUsedMB())
                maxUsed = memory.getUsedMB();

            if (minFree > memory.getFreeMB())
                minFree = memory.getFreeMB();
            if (maxFree < memory.getFreeMB())
                maxFree = memory.getFreeMB();

            if (minTime > memory.getTimeMin())
                minTime = memory.getTimeMin();
            if (maxTime < memory.getTimeMin())
                maxTime = memory.getTimeMin();
        }
        MemoryStat memoryStat = new MemoryStat();
        memoryStat.minUsed = minUsed;
        memoryStat.maxUsed = maxUsed;
        memoryStat.minFree = minFree;
        memoryStat.maxFree = maxFree;
        memoryStat.minTime = minTime;
        memoryStat.maxTime = maxTime;

        memoryStat.minMemory = Math.min(minUsed, minFree);
        memoryStat.maxMemory = Math.max(maxUsed, maxFree);
        return memoryStat;
    }

    public long getMinUsed() {
        return minUsed;
    }

    public long getMaxUsed() {
        return maxUsed;
    }

    public long getMinFree() {
        return minFree;
    }

    public long getMaxFree() {
        return maxFree;
    }

    public long getMinTime() {
        return minTime;
    }

    public long getMaxTime() {
        return maxTime;
    }

    public long getMinMemory() {
        return minMemory;
    }

    public long getMaxMemory() {
        return maxMemory;
    }
}
