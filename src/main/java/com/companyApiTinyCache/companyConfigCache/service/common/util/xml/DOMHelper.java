package com.companyApiTinyCache.companyConfigCache.service.common.util.xml;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class DOMHelper {

    private static class NodeIterator implements Iterable<Node>, Iterator<Node> {

        private NodeList nodes;

        private String name;

        private int numNodes;

        private int currentIndex;

        public NodeIterator(NodeList pnodes, String pname) {
            nodes = pnodes;
            if (nodes != null) {
                numNodes = nodes.getLength();
            }
            name = pname;
        }

        private boolean findNext() {
            boolean found = true;
            do {
                currentIndex++;
                if (currentIndex >= numNodes) {
                    found = false;
                    break;
                }
            } while (!name.equalsIgnoreCase(nodes.item(currentIndex).getNodeName()));

            return found;
        }

        @Override
        public Iterator<Node> iterator() {
            currentIndex = -1;
            findNext();
            return this;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < numNodes;
        }

        @Override
        public Node next() {
            if (!hasNext()) {
                throw new NoSuchElementException("No Node matching name " + name + " found");
            }

            Node node = nodes.item(currentIndex);
            findNext();
            return node;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Remove not supported");
        }

    }

    private static final Log logger = LogFactory.getLog(DOMHelper.class);

    private static int findNext(NodeList nodes, String name, int currentIndex) {
        int len = 0;
        if (nodes != null) {
            len = nodes.getLength();
        }
        int index = -1;
        while (currentIndex < len) {
            if (name.equalsIgnoreCase(nodes.item(currentIndex).getNodeName())) {
                index = currentIndex;
                break;
            }
            currentIndex++;
        }
        ;

        return index;
    }

    public static Document createDocument(String xml) throws ParserConfigurationException, SAXException, IOException {
        StringReader reader = new StringReader(xml);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(new InputSource(reader));
    }

    public static Iterable<Node> selectNodes(String path, Node node) {
        NodeList nodes = null;
        String name = null;
        int i;
        if (path != null && path.length() > 0 && node != null) {
            int startIndex = 0, endIndex;
            while ((endIndex = path.indexOf('/', startIndex)) > 0) {
                String childName = path.substring(startIndex, endIndex);
                startIndex = endIndex + 1;
                NodeList childNodes = node.getChildNodes();
                i = findNext(childNodes, childName, 0);
                if (i >= 0) {
                    node = childNodes.item(i);
                } else {
                    node = null;
                    break;
                }
            }
            if (node != null) {
                nodes = node.getChildNodes();
                name = path.substring(startIndex);
            }
        }
        NodeIterator nodeIterator = new NodeIterator(nodes, name);

        return nodeIterator;
    }

    private static Node selectOrCreateNode(Document document, String path, Node node, boolean createForAllLevels) {
        int i, len;
        if (path != null && (len = path.length()) > 0 && node != null) {
            int startIndex = 0, endIndex;
            do {
                endIndex = path.indexOf('/', startIndex);
                if (endIndex < 0) {
                    endIndex = len;
                }
                String childName = path.substring(startIndex, endIndex);
                startIndex = endIndex + 1;
                NodeList childNodes = node.getChildNodes();
                Node childNode = null;
                i = findNext(childNodes, childName, 0);
                if (i >= 0) {
                    childNode = childNodes.item(i);
                } else if (document != null && (endIndex >= len || createForAllLevels)) {
                    childNode = document.createElement(childName);
                    node.appendChild(childNode);
                }
                node = childNode;
            } while (endIndex < len && node != null);
        }

        return node;
    }

    public static Node selectNode(String path, Node node) {
        return selectOrCreateNode(null, path, node, false);
    }

    public static String selectNodeValue(String path, Node node) {
        String res = "";
        node = selectNode(path, node);

        if (node != null) {
            res = StringUtils.trimToEmpty(node.getTextContent());
        }

        return res;
    }

    public static int selectNodeIntValue(String path, Node node) {
        String res = selectNodeValue(path, node);
        if (res.equals(""))
            res = "0";

        return Integer.parseInt(res);
    }

    public static Integer selectNodeIntegerValue(String path, Node node) {
        Integer i = null;
        String res = selectNodeValue(path, node);
        if (res != null && res.length() > 0) {
            i = Integer.parseInt(res);
        }

        return i;
    }

    public static double selectNodeDoubleValue(String path, Node node) {
        String res = selectNodeValue(path, node);

        if (res.equals(""))
            res = "0";

        return Double.parseDouble(res);
    }

    public static <E extends Enum<E>> E selectNodeEnumValue(String path, Node node, Class<E> c) {
        E e = null;

        String res = selectNodeValue(path, node);
        if (res != null && res.length() > 0) {
            try {
                e = Enum.valueOf(c, res);
            } catch (Exception ex) {
                logger.error("Error parsing value: " + res + " for Enum Class: " + c, ex);
            }
        }

        return e;
    }

    public static Node selectOrCreateNode(Document document, String path, Node node) {
        return selectOrCreateNode(document, path, node, false);
    }
}
