package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.logging.StatsLogger;
import com.companyApiTinyCache.companyConfigCache.service.common.service.CommonMiscService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.SecurityBean;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @author cleartrip
 */
public class SubscriptionController extends AbstractController {

    private CommonMiscService commonMiscService;

    private StatsLogger statsLogger;

    public SubscriptionController(CommonMiscService pCommonMiscService) {
        this.commonMiscService = pCommonMiscService;
    }

    public StatsLogger getStatsLogger() {
        return statsLogger;
    }

    public void setStatsLogger(StatsLogger statsLogger) {
        this.statsLogger = statsLogger;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int responseStatusCode = 200;

        String message = "{'status':'success'}";

        SecurityBean securityBean = (SecurityBean) request.getAttribute(CommonEnumConstants.SESSION_SECURITY_BEAN_ID);

        // people id is not available for un-confirmed users, hence use email address instead of people id.
        if (securityBean != null && StringUtils.isNotBlank(securityBean.getEmailAddress())) {

            String emailAddress = securityBean.getEmailAddress();

            try {
                commonMiscService.subscribeToNewsLetters(emailAddress, true, request);
            } catch (Exception e) {
                logger.error("Error: ", e);
                message = "{'status': 'failure', 'message': '" + e.getMessage() + "'}";
            }

            statsLogger.sendCounterStats("newsletter_subscribe");

        } else {
            response.setStatus(201);
            message = "{'status': 'failure', 'message': 'User is not logged in or people id is less than or equal to 0'}";
            message = "";
        }

        response.setStatus(responseStatusCode);
        PrintWriter pw = response.getWriter();
        pw.println(message);
        // logger.info("Seat Sell 2 Response for Multicity:" + tripXML);
        response.setContentType("text/xml");

        return null;
    }

}
