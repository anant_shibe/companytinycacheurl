package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Read Only Map intended to translate map.get to request.getParameter/request.getHeader avoiding a HashMap.
 * However Any call other than get and containsKey creates initializes an internal HashMap (We were trying to avoid in the first place)
 * and proxys the call to that.
 *
 * @author suresh
 */
public class RequestWrappingMap implements Map<String, String> {

    private HttpServletRequest request;

    private Map<String, String> requestMap;

    private int flags;

    public RequestWrappingMap(HttpServletRequest prequest) {
        request = prequest;
        flags = 3;
    }

    /**
     * Initializes Wrapper with request and flags indicating whether to use request.getParameter or
     * request.getHeader or both in which case request.getParameter is first checked and then request.getHeader.
     * @param prequest request
     * @param pflags 1 - check request.getParameter, 2 - Check request.getHeader, 3 - Check both
     */
    public RequestWrappingMap(HttpServletRequest prequest, int pflags) {
        request = prequest;
    }

    public static HashMap<String, String> createRequestMap(HttpServletRequest r, int f) {
        HashMap<String, String> requestMap = new HashMap<String, String>();
        if ((f & 1) != 0) {
            Enumeration<String> paramNames = r.getParameterNames();
            while (paramNames.hasMoreElements()) {
                String paramName = paramNames.nextElement();
                requestMap.put(paramName, r.getParameter(paramName));
            }
        }
        if ((f & 2) != 0) {
            Enumeration<String> headerNames = r.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                requestMap.put(headerName, r.getHeader(headerName));
            }
        }

        return requestMap;
    }

    public void initRequestMap() {
        if (requestMap == null) {
            requestMap = createRequestMap(request, flags);
        }
    }

    @Override
    public int size() {
        initRequestMap();
        return requestMap.size();
    }

    @Override
    public boolean isEmpty() {
        initRequestMap();
        return requestMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        boolean result = key instanceof String && get(key) != null;
        return result;
    }

    @Override
    public boolean containsValue(Object value) {
        initRequestMap();
        return requestMap.containsValue(value);
    }

    @Override
    public String get(Object key) {
        String val = null;
        if (key instanceof String) {
            if ((flags & 1) != 0) {
                val = request.getParameter((String) key);
            }
            if (val == null && (flags & 2) != 0) {
                val = request.getHeader((String) key);
            }
        }
        return val;
    }

    @MethodExcludeCoverage
    @Override
    public String put(String key, String value) {
        throw new UnsupportedOperationException("Map is Read Only");
    }

    @MethodExcludeCoverage
    @Override
    public String remove(Object key) {
        throw new UnsupportedOperationException("Map is Read Only");
    }

    @MethodExcludeCoverage
    @Override
    public void putAll(Map<? extends String, ? extends String> m) {
        throw new UnsupportedOperationException("Map is Read Only");
    }

    @MethodExcludeCoverage
    @Override
    public void clear() {
        throw new UnsupportedOperationException("Map is Read Only");
    }

    @Override
    public Set<String> keySet() {
        initRequestMap();
        return requestMap.keySet();
    }

    @Override
    public Collection<String> values() {
        initRequestMap();
        return requestMap.values();
    }

    @Override
    public Set<Entry<String, String>> entrySet() {
        initRequestMap();
        return requestMap.entrySet();
    }

}
