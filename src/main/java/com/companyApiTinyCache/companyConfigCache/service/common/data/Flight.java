/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Ramesh Krishnamoorthy
 * 
 */
public class Flight implements Serializable {
    private static final long serialVersionUID = 1L;
    private int no_legs;
    private double base_price; // adult_base * total number of passengers, pre-calculated in json
    private double adult_base; // sum of adult_bases of all the FlightSegments, pre-calculated in json
    private double child_base; // not in json, so calculate by adding all segment child_bases
    private double infant_base; // not in json, so calculate by adding all segment infant_bases
    private double taxes; // pre-calculated in json
    private double totalFare; // base_price + taxes
    private double discount; // not in json
    private double bookingFee; // not in json
    private double agencyServiceFee;

    private String from;
    private String fromCityName;
    private String fromAirportName;
    private String to;
    private String toCityName;
    private String toAirportName;

    private String e_ticket;

    private Collection<FlightSegment> segments;

    /**
     * @return the segments
     */
    public Collection<FlightSegment> getSegments() {
        return segments;
    }

    /**
     * @param segments
     *            the segments to set
     */
    public void setSegments(Collection<FlightSegment> segments) {
        this.segments = segments;
    }

    /**
     * @return the no_legs
     */
    public int getNo_legs() {
        return no_legs;
    }

    /**
     * @param no_legs
     *            the no_legs to set
     */
    public void setNo_legs(int no_legs) {
        this.no_legs = no_legs;
    }

    /**
     * @return the base_price
     */
    public double getBase_price() {
        return base_price;
    }

    /**
     * @param base_price
     *            the base_price to set
     */
    public void setBase_price(double base_price) {
        this.base_price = base_price;
    }

    /**
     * @return the taxes
     */
    public double getTaxes() {
        return taxes;
    }

    /**
     * @param taxes
     *            the taxes to set
     */
    public void setTaxes(double taxes) {
        this.taxes = taxes;
    }

    /**
     * @return the e_ticket
     */
    public String getE_ticket() {
        return e_ticket;
    }

    /**
     * @param e_ticket
     *            the e_ticket to set
     */
    public void setE_ticket(String e_ticket) {
        this.e_ticket = e_ticket;
    }

    /**
     * @return the totalFare
     */
    public double getTotalFare() {
        return getBase_price() + getTaxes() + getAgencyServiceFee();
    }

    /**
     * @return the adult_base
     */
    public double getAdult_base() {
        return adult_base;
    }

    /**
     * @param adult_base
     *            the adult_base to set
     */
    public void setAdult_base(double adult_base) {
        this.adult_base = adult_base;
    }

    /**
     * @return the child_base
     */
    public double getChild_base() {
        // double child_base=0;
        // for (Iterator segs = segments.iterator(); segs.hasNext();) {
        // FlightSegment segment = (FlightSegment) segs.next();
        // child_base+=segment.getChildBase();
        // }
        return child_base;
    }

    /**
     * @param child_base
     *            the child_base to set
     */
    public void setChild_base(double child_base) {
        this.child_base = child_base;
    }

    /**
     * @return the infant_base
     */
    public double getInfant_base() {
        // double infant_base=0;
        // for (Iterator segs = segments.iterator(); segs.hasNext();) {
        // FlightSegment segment = (FlightSegment) segs.next();
        // infant_base+=segment.getInfantBase();
        // }
        return infant_base;
    }

    /**
     * @param infant_base
     *            the infant_base to set
     */
    public void setInfant_base(double infant_base) {
        this.infant_base = infant_base;
    }

    /**
     * @return the discount
     */
    public double getDiscount() {
        return discount;
    }

    /**
     * @param discount
     *            the discount to set
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    /**
     * @return the bookingFee
     */
    public double getBookingFee() {
        return bookingFee;
    }

    /**
     * @param bookingFee
     *            the bookingFee to set
     */
    public void setBookingFee(double bookingFee) {
        this.bookingFee = bookingFee;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from
     *            the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the fromCityName
     */
    public String getFromCityName() {
        return fromCityName;
    }

    /**
     * @param fromCityName
     *            the fromCityName to set
     */
    public void setFromCityName(String fromCityName) {
        this.fromCityName = fromCityName;
    }

    /**
     * @return the fromAirportName
     */
    public String getFromAirportName() {
        return fromAirportName;
    }

    /**
     * @param fromAirportName
     *            the fromAirportName to set
     */
    public void setFromAirportName(String fromAirportName) {
        this.fromAirportName = fromAirportName;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to
     *            the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the toCityName
     */
    public String getToCityName() {
        return toCityName;
    }

    /**
     * @param toCityName
     *            the toCityName to set
     */
    public void setToCityName(String toCityName) {
        this.toCityName = toCityName;
    }

    /**
     * @return the toAirportName
     */
    public String getToAirportName() {
        return toAirportName;
    }

    /**
     * @param toAirportName
     *            the toAirportName to set
     */
    public void setToAirportName(String toAirportName) {
        this.toAirportName = toAirportName;
    }

    /**
     * @return the agencyServiceFee
     */
    public double getAgencyServiceFee() {
        return agencyServiceFee;
    }

    /**
     * @param agencyServiceFee
     *            the agencyServiceFee to set
     */
    public void setAgencyServiceFee(double agencyServiceFee) {
        this.agencyServiceFee = agencyServiceFee;
    }

}
