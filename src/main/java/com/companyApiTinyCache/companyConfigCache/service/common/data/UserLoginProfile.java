package com.companyApiTinyCache.companyConfigCache.service.common.data;

public class UserLoginProfile {

    private String userName;
    private int companyId;
    private int roleId;
    private String product;
    private boolean enableEmail;
    private String domainName;
    private String mailerSubscribeAction;
    private boolean travellersNeeded;
    private String device;
    private String host;

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the companyId
     */
    public int getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId
     *            the companyId to set
     */
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    /**
     * @return the roleId
     */
    public int getRoleId() {
        return roleId;
    }

    /**
     * @param roleId
     *            the roleId to set
     */
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the enableEmail
     */
    public boolean isEnableEmail() {
        return enableEmail;
    }

    /**
     * @param enableEmail
     *            the enableEmail to set
     */
    public void setEnableEmail(boolean enableEmail) {
        this.enableEmail = enableEmail;
    }

    /**
     * @return the domainName
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * @param domainName
     *            the domainName to set
     */
    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    /**
     * @return the mailerSubscribeAction
     */
    public String getMailerSubscribeAction() {
        return mailerSubscribeAction;
    }

    /**
     * @param mailerSubscribeAction
     *            the mailerSubscribeAction to set
     */
    public void setMailerSubscribeAction(String mailerSubscribeAction) {
        this.mailerSubscribeAction = mailerSubscribeAction;
    }

    /**
     * @return the travellersNeeded
     */
    public boolean isTravellersNeeded() {
        return travellersNeeded;
    }

    /**
     * @param travellersNeeded
     *            the travellersNeeded to set
     */
    public void setTravellersNeeded(boolean travellersNeeded) {
        this.travellersNeeded = travellersNeeded;
    }

    /**
     * @return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device
     *            the device to set
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @param host
     *            the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

}
