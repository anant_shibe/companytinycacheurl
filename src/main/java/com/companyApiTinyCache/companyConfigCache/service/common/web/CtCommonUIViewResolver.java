package com.companyApiTinyCache.companyConfigCache.service.common.web;

import org.springframework.core.Ordered;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.AbstractCachingViewResolver;

import java.util.Locale;

public class CtCommonUIViewResolver extends AbstractCachingViewResolver implements Ordered {

    private String context;
    private int order;

    @Override
    protected View loadView(String viewName, Locale locale) throws Exception {
        return new CtView(context, viewName);
    }

    public void setContext(String context) {
        this.context = context;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

}
