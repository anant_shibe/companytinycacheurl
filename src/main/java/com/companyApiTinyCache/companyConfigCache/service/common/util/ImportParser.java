/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.data.*;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import net.sf.vcard4j.java.VCard;
import net.sf.vcard4j.java.type.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Cleartrip
 * 
 */
public class ImportParser {

    private static final String FULL_NAME_SPLITTER = " ";
    private static final String VCARD_TYPE_EMAIL = "EMAIL";
    private static final String VCARD_TYPE_TITLE = "TITLE";
    private static final String VCARD_TYPE_ADDRESS = "ADR";
    private static final String VCARD_TYPE_EMPLOYEE_ID = "UID";
    private static final String VCARD_TYPE_TELEPHONE = "TEL";
    private static final String VCARD_TYPE_FULL_NAME = "FN";

    protected static final String TYPE_OTHER = "other";
    protected static final String TYPE_WORK = "work";
    protected static final String TELE_TYPE_MOBILE = "mobile";
    protected static final String TYPE_HOME = "home";

    protected static final String SUCCESSFULLY_IMPORTED_USERS = "successfullyImportedUsers";
    protected static final String FAILED_IMPORTS = "failedImports";

    protected String honoraryPrefixes[] = {"Miss", "Mrs.", "Mrs", "Mr.", "Mr", "Ms.", "Ms", "Mstr", "Mstr."};

    protected static final Log logger = LogFactory.getLog(ImportParser.class);

    protected void assignFullName(VCard vcard, UserProfile userProfile) {
        String fullName = getFullName(vcard);
        parseAndAssignCompleteName(fullName, userProfile);
    }

    protected void parseAndAssignCompleteName(String fullName, UserProfile userProfile) {
        boolean hasPrefix = false;
        String[] splittedName = fullName.split(FULL_NAME_SPLITTER);
        for (String honoraryPrefix : honoraryPrefixes) {
            if (splittedName[0].equalsIgnoreCase(honoraryPrefix)) {
                if (honoraryPrefix.contains(".")) {
                    honoraryPrefix = honoraryPrefix.substring(0, ((honoraryPrefix.length()) - 1));
                }
                userProfile.getPersonalDetail().setTitle(honoraryPrefix);
                hasPrefix = true;
                break;
            }
        }

        if (hasPrefix && splittedName.length > 2) { // CASE: MR. Nilesh Pathak
            userProfile.getPersonalDetail().setFirstName(splittedName[1]);
            userProfile.getPersonalDetail().setLastName(splittedName[2]);
        } else if (hasPrefix && splittedName.length == 2) { // CASE: MR. Nilesh
            userProfile.getPersonalDetail().setFirstName(splittedName[1]);
        } else if (!hasPrefix && splittedName.length == 2) { // CASE: Nilesh Pathak
            userProfile.getPersonalDetail().setFirstName(splittedName[0]);
            userProfile.getPersonalDetail().setLastName(splittedName[1]);
        } else if (!hasPrefix && splittedName.length == 1) { // CASE: Nilesh
            userProfile.getPersonalDetail().setFirstName(splittedName[0]);
        }
    }

    protected void assignPhoneNumbers(VCard vcard, UserProfile userProfile) {
        PhoneNumbers phoneNumbers = new PhoneNumbers();
        Iterator tels = vcard.getTypes(VCARD_TYPE_TELEPHONE);
        for (; tels.hasNext();) {
            TEL tel = (TEL) tels.next();
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setNumber(tel.get());
            if (isOfTypeMobile(tel)) {
                phoneNumber.setType(TELE_TYPE_MOBILE);
            } else {
                phoneNumber.setType(TYPE_WORK);
            }
            phoneNumbers.add(phoneNumber);
        }
        userProfile.getContactDetail().setPhoneNumbers(phoneNumbers);
    }

    protected boolean isOfTypeMobile(TEL tel) {
        return ((TEL.Parameters) tel.getParameters()).containsTYPE(TEL.Parameters.TYPE_CELL);
    }

    protected void assignTitle(VCard vcard, UserProfile userProfile) {
        Iterator titleTypes = vcard.getTypes(VCARD_TYPE_TITLE);
        if (titleTypes.hasNext()) {
            TITLE title = (TITLE) titleTypes.next();
            Map<Integer, CompanyDetail> companyDetails = userProfile.getCompanyDetails();
            CompanyDetail companyDetail = null;
            if (companyDetails != null && !companyDetails.isEmpty()) {
                companyDetail = companyDetails.get(0);
                if (companyDetail == null) {
                    companyDetail = new CompanyDetail();
                }
            }
            companyDetail.setCompanyDesignation(title.get());
            companyDetails.put(0, companyDetail);
            userProfile.setCompanyDetails(companyDetails);
        }
    }

    protected void assignEmails(VCard vcard, UserProfile userProfile) {
        ArrayList<Email> mailList = new ArrayList<Email>();
        Iterator emails = vcard.getTypes(VCARD_TYPE_EMAIL);
        for (; emails.hasNext();) {
            EMAIL email = (EMAIL) emails.next();
            if (!(email.get().isEmpty())) {
                boolean workType = ((EMAIL.Parameters) email.getParameters()).containsTYPE(EMAIL.Parameters.TYPE_WORK);
                Email e_mail = new Email();
                e_mail.setEmailId(email.get());
                if (workType) {
                    e_mail.setType(TYPE_WORK);
                } else {
                    e_mail.setType(TYPE_OTHER);
                }
                mailList.add(e_mail);
            }
        }
        userProfile.getContactDetail().setEmails(mailList);
        String primaryEmail = userProfile.getContactDetail().getEmail(CommonEnumConstants.EmailType.WORK);
        if (!(primaryEmail.isEmpty())) {
            userProfile.getPersonalDetail().setPrimaryEmail(primaryEmail);
        } else {
            primaryEmail = userProfile.getContactDetail().getEmail(CommonEnumConstants.EmailType.OTHER);
            userProfile.getPersonalDetail().setPrimaryEmail(primaryEmail);
        }
    }

    protected String getFullName(VCard vcard) {
        Iterator fnTypes = vcard.getTypes(VCARD_TYPE_FULL_NAME);
        FN fn = null;
        if (fnTypes.hasNext()) {
            fn = (FN) fnTypes.next();
        }
        String fullName = fn.get();
        return StringUtils.isNotEmpty(fullName) ? fullName.trim() : null;
    }

    protected void assignEmployeeId(VCard vcard, UserProfile userProfile) {
        Iterator employeeIdTypes = vcard.getTypes(VCARD_TYPE_EMPLOYEE_ID);
        if (employeeIdTypes.hasNext()) {
            UID employeeId = (UID) employeeIdTypes.next();
            CompanyDetail companyDetail = userProfile.getCompanyDetails().get(0);
            companyDetail.setEmployeeId(employeeId.get());
        }
    }

    protected void assignAddress(VCard vcard, UserProfile userProfile) {
        Iterator addressTypes = vcard.getTypes(VCARD_TYPE_ADDRESS);
        if (addressTypes.hasNext()) {
            {
                ADR workAddress = (ADR) addressTypes.next();
                ContactDetail contactDetail = userProfile.getContactDetail();
                if (contactDetail == null) {
                    contactDetail = new ContactDetail();
                }
                Address address = new Address();
                address.setAddressType(TYPE_WORK);
                address.setStreetAddress(workAddress.getStreet());
                address.setCity(workAddress.getLocality());
                address.setState(workAddress.getRegion());
                address.setCountry(workAddress.getCountry());
                address.setPincode(workAddress.getPcode());
                Collection<Address> addresses = new ArrayList<Address>();
                addresses.add(address);
                contactDetail.setAddresses(addresses);
            }
        }
    }

    // public static void main(String[] args) throws IOException
    // {
    // String file = ("C:\\Documents and Settings\\nilesh\\Desktop\\VCF Cards\\Chaitanya Gupta and 15 others.vcf");
    // FileInputStream fileInputStream = new FileInputStream(file);
    // byte[] read= new byte[fileInputStream.available()];
    // fileInputStream.read(read);
    // VCardParser vcard = new VCardParser();
    // vcard.parseVCard(read);
    // try {
    // DomParser parser = new DomParser();
    // //Document document = DocumentBuilderFactory.newDocumentBuilder().newDocument();
    // Document document = new DocumentImpl();
    // parser.parse(fileInputStream, document);
    //
    // OutputFormat format = new OutputFormat(document);
    // format.setOmitDocumentType(true);
    // format.setOmitXMLDeclaration(true);
    // format.setMethod("xml");
    // format.setIndenting(true);
    // format.setPreserveSpace(false);
    // format.setLineWidth(0);
    // XMLSerializer serializer = new XMLSerializer(System.out, format);
    // System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    // //System.out.println("<!DOCTYPE vcard4j PUBLIC \"Yann Duponchel, vCard 4 Java\" \"http://vcard4j.sf.net/xml/vcard4j.dtd\">");
    // System.out.println("<?xml-stylesheet type=\"text/xsl\" href=\"src/xml/vcard4j-2-vcard30.xsl\"?>");
    // System.out.println("<!--<?xml-stylesheet type=\"text/xsl\" href=\"src/xml/vcard4j-2-xhtml.xsl\"?>-->");
    // serializer.serialize(document);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }
}
