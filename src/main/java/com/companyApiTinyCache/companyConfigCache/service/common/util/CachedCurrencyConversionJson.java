package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Cached Resource containing currency rates.
 *
 * @author suresh
 */
public class CachedCurrencyConversionJson extends CachedUrlResource<Map<String, Double>> {

    private static final Log logger = LogFactory.getLog(CachedUrlResource.class);

    private static final String CURRENCY_JSON = "currency.json";

    public CachedCurrencyConversionJson(CachedProperties pcommonCachedProperties) throws MalformedURLException {
        super(pcommonCachedProperties, "ct.services.currency.json.url.ctconfig", null, false);
        String url = commonCachedProperties.getPropertyValue("ct.services.currency.json.url.ctconfig");
        setLoader(new CachedCurrencyConversionJsonLoader());
        refreshResource();
    }

    private class CachedCurrencyConversionJsonLoader implements StreamResourceLoader<Map<String, Double>> {

        @Override
        public Map<String, Double> loadStreamResource(InputStream is) throws Exception {
            Reader reader = new InputStreamReader(is);
            StringBuilder sb = new StringBuilder();
            char[] buf = new char[1024];
            int len;
            while ((len = reader.read(buf)) > 0) {
                sb.append(buf, 0, len);
            }
            reader.close();
            String json = sb.toString();
            Map<String, Double> currencyConversionRatesFromJson = JsonUtil.parseMap(json, String.class, Double.class);
            Map<String, Double> currencyConversionRates = new HashMap<String, Double>(currencyConversionRatesFromJson);
            return currencyConversionRates;
        }
    }
}
