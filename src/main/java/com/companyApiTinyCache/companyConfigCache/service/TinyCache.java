/* 
 * @(#) TinyCache.java
 * 
 * Copyright 2010-2011 by Cleartrip Travel Services Pvt. Ltd.
 * Unit No 001, Ground Floor, DTC Bldg, 
 * Sitaram Mills Compound, N.M. Joshi Marg, 
 * Delisle Road, 
 * Lower Parel (E) 
 * Mumbai - 400011
 * India
 * 
 * This software is the confidential and proprietary information 
 * of Cleartrip Travel Services Pvt. Ltd. ("Confidential Information"). 
 * You shall not disclose such Confidential Information and shall 
 * use it only in accordance with the terms of license agreement you 
 * entered into with Cleartrip Travel Service Pvt. Ltd.
 */
package com.companyApiTinyCache.companyConfigCache.service;

import java.io.IOException;
import java.io.Writer;

/**
 * Interface for exposing the MiniCache implementation. TinyCache is an in memory cache to quickly cahce objects. It should not be used to store too many or large objects as it uses JVM's memory.
 * 
 * @author Amit Kumar Sharma
 */
public interface TinyCache<T> {

    /**
     * Adds a given value to the cache against the given key. If an object already exists in the cache, return the same
     * 
     * @param key
     * @param value
     */
    public T put(String key, T value);

    /**
     * Extracts value for given key from TinyCache
     * 
     * @param key
     * @return
     */
    public T get(String key);

    /**
     * Empties the entire cache
     */
    public void flush();

    /**
     * Remove a particular object.
     * 
     * @param key
     */
    public void remove(String key);

    /**
     * Returns the number of elements in the map
     */
    public int size();

    public void printKey(String key, Writer writer) throws IOException;

    public void printAllKeys(Writer writer) throws IOException;

    public void refresh();
}
