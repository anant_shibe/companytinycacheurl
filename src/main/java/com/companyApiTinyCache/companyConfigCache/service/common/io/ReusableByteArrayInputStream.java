package com.companyApiTinyCache.companyConfigCache.service.common.io;

import java.io.ByteArrayInputStream;

/**
 * A ByteArrayInputStream which gives access to the current positions to provide more flexibility.
 * @author Suresh
 *
 */
public class ReusableByteArrayInputStream extends ByteArrayInputStream {


    /**
     * Creates a ByteArrayInputStream by calling the super class's constructor.
     * @param   buf   the input buffer.
     */
    public ReusableByteArrayInputStream(byte[] buf) {
        super(buf);
    }

    /**
     * Creates a ByteArrayInputStream by calling the super class's constructor.
     * @param   buf      the input buffer.
     * @param   offset   the offset in the buffer of the first byte to read.
     * @param   length   the maximum number of bytes to read from the buffer.
     */
    public ReusableByteArrayInputStream(byte[] buf, int offset, int length) {
        super(buf, offset, length);
    }

    /**
     * Sets the buffer to use.
     * @param   b      the input buffer.
     * @param   offset   the offset in the buffer of the first byte to read.
     * @param   length   the maximum number of bytes to read from the buffer.
     */
    public void setBuf(byte [] b, int offset, int length) {
        buf = b;
        pos = offset;
        count = Math.min(offset + length, buf.length);
        mark = offset;
    }

    /**
     * Gets the current position (offset) of the Stream.
     * @return Current possition
     */
    public int getPos() {
        return pos;
    }

    /**
     * Gets the current last position of the Stream till which there is data.
     * @return The count which means there is data till count-1
     */
    public int getCount() {
        return count;
    }
}
