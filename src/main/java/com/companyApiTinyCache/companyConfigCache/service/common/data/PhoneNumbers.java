package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.PhoneType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Vector;

public class PhoneNumbers extends Vector<PhoneNumber> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    @JsonIgnore
    public synchronized boolean add(PhoneNumber e) {
        return typeExists(e) ? false : super.add(e);
    }

    @JsonIgnore
    public boolean typeExists(PhoneNumber e) {
        for (PhoneNumber userPhoneNumber : this) {
            if (userPhoneNumber.getType().equalsIgnoreCase(e.getType())) {
                return true;
            }
        }
        return false;
    }

    @JsonIgnore
    public boolean typeExists(PhoneType phoneType) {
        for (PhoneNumber userPhoneNumber : this) {
            if (userPhoneNumber.getType().equals(phoneType.toString())) {
                return true;
            }
        }
        return false;
    }

}
