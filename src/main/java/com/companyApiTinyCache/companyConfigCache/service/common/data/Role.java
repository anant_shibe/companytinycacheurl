package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.List;

public class Role implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private int id;

    private String name;

    private String desc;

    private int parentId;

    private Integer channelType;

    private SourceType channelName;

    @JsonIgnore
    private List<Integer> permissions;

    private int accessLevel;

    @JsonProperty("role_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getId() {
        return id;
    }

    @JsonProperty("role_id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("role_name")
    public String getName() {
        return name;
    }

    @JsonProperty("role_name")
    public void setName(String name) {
        this.name = name;

    }

    /**
     * RoR doesn't expect this description as it maps to COMPANY_PEOPLE_ROLES table and it doesn't have this column.
     * 
     * @return
     */
    @JsonIgnore
    public String getDesc() {
        return desc;
    }

    @JsonProperty("description")
    public void setDesc(String desc) {
        this.desc = desc;

    }

    @JsonProperty("company_people_parent_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getParentId() {
        return parentId;
    }

    @JsonProperty("company_people_parent_id")
    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @JsonProperty("access_level")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getAccessLevel() {
        return accessLevel;
    }

    @JsonProperty("access_level")
    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    /**
     * This parameter should not be updated. That is why its json binding is removed from the getter method
     * 
     * @return SourceType
     */
    @JsonIgnore
    public Integer getChannelType() {
        return channelType;
    }

    @JsonProperty("channel_type")
    public void setChannelType(int channelType) {
        this.channelType = new Integer(channelType);
    }

    /**
     * This parameter should not be updated. That is why its json binding is removed from the getter method
     * 
     * @return SourceType
     */
    @JsonIgnore
    public SourceType getChannelName() {
        return channelName;
    }

    @JsonProperty("channel_type_name")
    public void setChannelName(SourceType channelName) {
        this.channelName = channelName;
    }

    @JsonIgnore
    public List<Integer> getPermissions() {
        return permissions;
    }

    @JsonIgnore
    public void setPermissions(List<Integer> permissions) {
        this.permissions = permissions;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((channelType == null) ? 0 : channelType.hashCode());
        result = prime * result + ((desc == null) ? 0 : desc.hashCode());
        result = prime * result + id;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + parentId;
        result = prime * result + ((permissions == null) ? 0 : permissions.hashCode());
        return result;
    }

    public boolean equals(Object obj) {

        if (obj instanceof Role == false)
            return false;
        Role role = (Role) obj;
        if (id != role.getId()) {
            return false;
        }
        if ((name != null || role.getName() != null) && !GenUtil.equals(name, role.getName())) {
            return false;
        }
        if ((desc != null || role.getDesc() != null) && !GenUtil.equals(desc, role.getDesc())) {
            return false;
        }
        if (parentId != role.getParentId()) {
            return false;
        }
        if ((permissions != null || role.getPermissions() != null) && !GenUtil.equals(permissions, role.getPermissions())) {
            return false;
        }
        if ((channelType != null || role.getChannelType() != null) && !channelType.equals(role.getChannelType())) {
            return false;
        }

        return true;
    }

}
