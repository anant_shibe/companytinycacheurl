package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.web.PersonalizationGetOpBean;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import java.io.IOException;

public interface PersonalizationService {

    public String get(PersonalizationGetOpBean personalizationGetOps, boolean isConfirmed) throws JsonParseException, JsonMappingException, IOException;

    public String put(String userId, String jsonData, String source) throws JsonParseException, JsonMappingException, IOException;

}
