package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.ValueUpdater;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Updates information about current server in AppRegistry in memcache. Also cleans up expired servers. This object holds a ServerInfo object which contains the actual Server information.
 *
 * @author suresh
 *
 */
public class ThisServer implements ApplicationContextAware {

    private static final Log logger = LogFactory.getLog(ThisServer.class);
    public static final String CT_SERVICES_COMMON_APPREGISTRY_MEMCACHED_SERVERS = "ct.services.common.appregistry.memcached.servers";

    private CachedProperties commonCachedProperties;

    private ServerInfo serverInfo;

    private CacheFactory cacheFactory;

    private Cache appRegistryCache;

    public ThisServer(CacheFactory cacheFactory) {
        serverInfo = new ServerInfo();
        appRegistryCache = cacheFactory.getCacheForServer(CT_SERVICES_COMMON_APPREGISTRY_MEMCACHED_SERVERS);
        try {
            Properties properties = new Properties();
            properties.load(ServerInfo.class.getResourceAsStream("/build-info.properties"));
            ServerInfo currentServerInfo = serverInfo;
            ServerInfoBasic basicInfo = currentServerInfo.getBasicInfo();
            basicInfo.setBuildTag(properties.getProperty("build.tag"));
            basicInfo.setSvnRevision(properties.getProperty("svn.revision"));
            basicInfo.setBuildTime(properties.getProperty("build.time"));
        } catch (Exception e) {
            logger.warn("Error Getting Build Info for AppRegistry", e);
        }
    }

    // Gets Memcached registry Key
    public String getRegistryCacheKey() {
        String keyPrefix = CommonEnumConstants.APP_REGISTRY_KEY_PREFIX;
        String key = keyPrefix;
        String env = getRelease();
        if (env != null) {
            key = keyPrefix + '_' + env;
        }

        return key;
    }

    protected String getRelease() {
        return commonCachedProperties.getPropertyValue("ct.common.runtime.env");
    }

    private String getInfoCacheKey(String keyPrefix, String name, String host, int port) {
        StringBuilder sb = new StringBuilder();
        sb.append(keyPrefix).append('_').append(name);
        String env = getRelease();
        if (env != null) {
            sb.append('_').append(env);
        }
        sb.append('_').append(host).append('_').append(port);

        return sb.toString();
    }

    public String getRegistryPingCacheKey(String host, int port) {
        return getInfoCacheKey(CommonEnumConstants.APP_REGISTRY_KEY_PREFIX, "PING", host, port);
    }

    public String getRegistryInfoCacheKey(String name, String host, int port) {
        return getInfoCacheKey(CommonEnumConstants.APP_REGISTRY_INFO_KEY_PREFIX, name, host, port);
    }

    public int getCleanupSeconds() {
        return commonCachedProperties.getIntPropertyValue("ct.common.appregistry.app-cleanup-seconds", 1800);
    }

    public int getValidateSeconds() {
        return commonCachedProperties.getIntPropertyValue("ct.common.appregistry.app-validate-seconds", 1800);
    }

    public int getExpirySeconds() {
        return commonCachedProperties.getIntPropertyValue("ct.common.appregistry.app-expiry-seconds", 86400);
    }

    public void addApp(String appName, String serverType) {
        String configHost = commonCachedProperties.getPropertyValue("ct.common.hostname");
        int configPort = commonCachedProperties.getIntPropertyValue("ct.common.port", 0);
        ServerInfoBasic basicInfo = serverInfo.getBasicInfo();
        basicInfo.setHost(configHost);
        basicInfo.setConfigPort(configPort);
        if (appName != null) {
            basicInfo.addApp(appName);
        }
        if (serverType != null) {
            basicInfo.setType(serverType);
        }
        if (appName != null) {
            pingToAppRegistry();
        }
    }

    public void pingToAppRegistry() {
        logger.info("Pinging to App Registry");
        addApp(null, null);
        ServerInfoBasic basicInfo = serverInfo.getBasicInfo();
        long oldLpt = serverInfo.getBasicInfo().getLpt();
        long lpt = System.currentTimeMillis();
        serverInfo.getBasicInfo().setLpt(lpt);
        boolean success = updateAppRegistry(basicInfo.getHost(), basicInfo.getPort(), oldLpt, lpt, true);
        if (!success) {
            serverInfo.getBasicInfo().setLpt(0);
        }
        cleanupExpiredServers();
    }

    public int getPingIntervalSeconds() {
        return commonCachedProperties.getIntPropertyValue("ct.common.appregistry.ping-minutes", 5) * 60;
    }

    public long getShortExpiryTime(long now) {
        if (now <= 0) {
            now = System.currentTimeMillis();
        }
        long shortExpiryTime = now - 1000 * getPingIntervalSeconds() - 60000;
        return shortExpiryTime;
    }

    public List<String> getRegistryServers() {
        String registryCacheKey = getRegistryCacheKey();
        List<String> servers = null;
        String serversJson = (String) appRegistryCache.get(registryCacheKey);
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        if (serversJson != null) {
            try {
                servers = mapper.readValue(serversJson, new TypeReference<List<String>>() {
                });
            } catch (Exception e) {
                logger.error("Error Getting Server Registry", e);
            }
        }

        return servers;
    }

    public List<Object>[] getServerInfos(boolean onlyActive, String name, String... moreNames) {
        List<Object>[] serverInfos = null;
        List<String> servers = getRegistryServers();
        int i, j, pos, len = 0;
        if (servers != null && (len = servers.size()) > 0) {
            String pingCacheKey, host, hostNPort;
            int port;
            String[] pingKeys = new String[len];
            i = 0;
            do {
                hostNPort = StringUtils.trimToNull(servers.get(i));
                pos = hostNPort.indexOf(':');
                host = hostNPort.substring(0, pos);
                port = Integer.parseInt(hostNPort.substring(pos + 1));
                pingCacheKey = getRegistryPingCacheKey(host, port);
                pingKeys[i] = pingCacheKey;
                i++;
            } while (i < len);
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            Map<String, Object> pingResults = appRegistryCache.getMulti(pingKeys);
            if (pingResults != null) {
                long shortExpiryTime = getShortExpiryTime(0);
                serverInfos = new List[moreNames.length + 1];
                i = 0;
                do {
                    serverInfos[i] = new ArrayList<Object>(len);
                    i++;
                } while (i <= moreNames.length);
                i = 0;
                do {
                    long lpt = 0;
                    String lptStr = (String) pingResults.get(pingKeys[i]);
                    if (lptStr != null) {
                        lpt = Long.parseLong(lptStr);
                    }
                    if (lpt > 0 && !(onlyActive && lpt < shortExpiryTime)) {
                        hostNPort = StringUtils.trimToNull(servers.get(i));
                        pos = hostNPort.indexOf(':');
                        host = hostNPort.substring(0, pos);
                        port = Integer.parseInt(hostNPort.substring(pos + 1));
                        j = -1;
                        do {
                            String infoName = j == -1 ? name : moreNames[j];
                            j++;
                            String serverInfoKey = getRegistryInfoCacheKey(infoName, host, port);
                            String storedJson = (String) appRegistryCache.get(serverInfoKey);
                            ServerInfoAbstract javaServerInfo = serverInfo.findServerInfo(infoName);
                            List<Object> serverInfoList = serverInfos[j];
                            if (javaServerInfo != null) {
                                try {
                                    javaServerInfo = mapper.readValue(storedJson, javaServerInfo.getClass());
                                    if (javaServerInfo instanceof ServerInfoBasic) {
                                        ((ServerInfoBasic) javaServerInfo).setLpt(lpt);
                                    }
                                    serverInfoList.add(javaServerInfo);
                                } catch (Exception e) {
                                    logger.error("Error converting java json named " + infoName + ": " + storedJson, e);
                                }
                            } else {
                                serverInfoList.add(storedJson);
                            }
                        } while (j < moreNames.length);
                    }
                    i++;
                } while (i < len);
            }
        }

        return serverInfos;
    }

    private boolean addServer(Cache appRegistryCache, String host, int port, boolean isInternal) {
        ValueUpdater valueUpdater = null;
        ServerInfoBasic serverInfoBasic = serverInfo.getBasicInfo();
        if (isInternal) {
            valueUpdater = serverInfoBasic.getServerListUpdater(false);
        } else {
            valueUpdater = serverInfoBasic.getServerListUpdater(host, port, false);
        }
        boolean success = appRegistryCache.atomicUpdate(getRegistryCacheKey(), 3 * getExpirySeconds(), valueUpdater, 200);

        return success;
    }

    private void updateServerInfos(Cache appRegistryCache, String host, int port, boolean entryExists) {
        int i;
        ServerInfoAbstract[] allServerInfos = serverInfo.getServerInfos();
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        long now = System.currentTimeMillis();
        for (i = 0; i < allServerInfos.length; i++) {
            // Force update after a day since everything expires in 2 days
            ServerInfoAbstract nextServerInfo = allServerInfos[i];
            long lastUpdated = nextServerInfo.getLastUpdated();
            long lastSaved = nextServerInfo.getLastSaved();
            int expirySeconds = getExpirySeconds();
            if (!entryExists || (lastUpdated >= lastSaved - 100) || (now - lastSaved) / 1000 >= expirySeconds) {
                nextServerInfo.setDirty(true);
            }
            if (nextServerInfo.isDirty()) {
                String serverInfoKey = getRegistryInfoCacheKey(nextServerInfo.getName(), host, port);
                StringBuilderWriter sbw = new StringBuilderWriter();
                try {
                    nextServerInfo.setDirty(false);
                    nextServerInfo.setLastSaved(now);
                    mapper.writeValue(sbw, nextServerInfo);
                    appRegistryCache.syncPut(serverInfoKey, sbw.toString(), 3 * expirySeconds);
                } catch (Exception e) {
                    logger.error("Error Converting to Json for Server Registry", e);
                } finally {
                    nextServerInfo.setDirty(true);
                    nextServerInfo.setLastSaved(lastSaved);
                }
                try {
                    // Read Value and Double check before turning off dirty flag, since this is
                    // permanent info for 1 day unless updated
                    String storedJson = (String) appRegistryCache.get(serverInfoKey);
                    if (storedJson != null) {
                        ServerInfoAbstract testServerInfo = mapper.readValue(storedJson, nextServerInfo.getClass());
                        if (testServerInfo != null && testServerInfo.getLastSaved() == now) {
                            nextServerInfo.setDirty(false);
                            nextServerInfo.setLastSaved(now);
                        }
                    }
                } catch (Exception e) {
                    logger.info("Error Reading Back Stored Json for testing", e);
                }
            }
        }
    }

    private boolean validateServerEntry(String host, int port) {
        boolean isValid = true;
        long now = System.currentTimeMillis();
        long validationInterval = getValidateSeconds() * 1000;
        if ((now - serverInfo.getLastValidateTime()) >= validationInterval) {
            serverInfo.setLastValidateTime(now);
            String serverEntry = host + ':' + port;
            List<String> servers = getRegistryServers();
            if (servers == null || !servers.contains(serverEntry)) {
                isValid = false;
            }
        }

        return isValid;
    }

    /**
     * This is used store ping information for a server at <key_prefix>_PING. Can be made as a service also. The key is first read and compared with oldLpt and true is returned at end if same. Old
     * Keys are removed as a side task.
     *
     * @param host
     *            host host
     * @param port
     *            port
     * @param oldLpt
     *            used to validate <key_prefix>_host:port_PING
     * @param lpt
     *            New Value stored to the ping key
     * @return true if valid. Otherwise lpt should be set to 0 so that addServer is tried again.
     */
    private boolean updateAppRegistry(String host, int port, long oldLpt, long lpt, boolean isInternal) {
        boolean entryExists = true;
        boolean success = true;
        try {
            boolean updateEnabled = commonCachedProperties.getBooleanPropertyValue("ct.common.appregistry.enabled", false);
            if (updateEnabled && host != null && port > 0) {
                String pingCacheKey = getRegistryPingCacheKey(host, port);
                long currentLpt = 0;
                String currentLptStr = (String) appRegistryCache.get(pingCacheKey);
                if (currentLptStr != null) {
                    currentLpt = Long.parseLong(currentLptStr);
                }
                entryExists = currentLpt != 0 && currentLpt == oldLpt;
                if (entryExists) {
                    entryExists = validateServerEntry(host, port);
                }
                if (isInternal) {
                    updateServerInfos(appRegistryCache, host, port, entryExists);
                }
                appRegistryCache.syncPut(pingCacheKey, String.valueOf(lpt), getExpirySeconds());
                if (!entryExists) {
                    success = addServer(appRegistryCache, host, port, isInternal);
                }
            }
        } catch (Exception e) {
            logger.error("Error Updating App Registry", e);
        }

        return success;
    }

    private void cleanupExpiredServers() {
        try {
            long now = System.currentTimeMillis();
            long lastCleanupTime = serverInfo.getLastCleanupTime();
            int cleanupSeconds = getCleanupSeconds();
            int expirySeconds = getExpirySeconds();
            int cleanupTries = serverInfo.getCleanupTries();
            // cleanupTries is used to distribute cleanup amongst servers. As failed tries increases
            // the cleanup will kick in earlier than other servers. cleanupTries is reset to 0 on
            // Actually acquiring the lock and doing a cleanup
            if ((now - lastCleanupTime + (20000 - 200 * cleanupTries)) >= cleanupSeconds * 1000L) {
                serverInfo.setLastCleanupTime(now);
                String registryCacheKey = getRegistryCacheKey();
                String lockCacheKey = registryCacheKey + "_LOCK";
                Cache.LockStatus lockStatus = appRegistryCache.lock(lockCacheKey, 300, false);
                if (lockStatus == Cache.LockStatus.SUCCESS) {
                    serverInfo.setCleanupTries(0);
                    List<String> servers = getRegistryServers();
                    int i, pos, len = 0;
                    if (servers != null && (len = servers.size()) > 0) {
                        String pingCacheKey;
                        String[] pingKeys = new String[len];
                        i = 0;
                        do {
                            String hostNPort = StringUtils.trimToNull(servers.get(i));
                            pos = hostNPort.indexOf(':');
                            String host = hostNPort.substring(0, pos);
                            int port = Integer.parseInt(hostNPort.substring(pos + 1));
                            pingCacheKey = getRegistryPingCacheKey(host, port);
                            pingKeys[i] = pingCacheKey;
                            i++;
                        } while (i < len);
                        Map<String, Object> pingResults = appRegistryCache.getMulti(pingKeys);
                        List<String> expiredServers = null;
                        String[] expiredPingKeys = null;
                        int numExpiredKeys = 0;
                        if (pingResults != null) {
                            numExpiredKeys = len - pingResults.size();
                            expiredServers = new ArrayList<String>(numExpiredKeys);
                            expiredPingKeys = new String[numExpiredKeys];
                            numExpiredKeys = 0;
                            i = 0;
                            do {
                                pingCacheKey = pingKeys[i];
                                if (!pingResults.containsKey(pingCacheKey)) {
                                    expiredServers.add(servers.get(i));
                                    expiredPingKeys[numExpiredKeys] = pingCacheKey;
                                    numExpiredKeys++;
                                }
                                i++;
                            } while (i < len);
                        }
                        // In case any Expired Key has got added. Add back the entries to the App Registry Servers.
                        // This is in case the entry is deleted after the ping has added the key and to the Registry.
                        Map<String, Object> modifiedKeyResults = null;
                        ValueUpdater valueUpdater;
                        ServerInfoBasic basicInfo = serverInfo.getBasicInfo();
                        boolean success;
                        if (numExpiredKeys > 0) {
                            valueUpdater = basicInfo.getServerListUpdater(expiredServers, true);
                            success = appRegistryCache.atomicUpdate(registryCacheKey, 3 * expirySeconds, valueUpdater, 200);
                            if (success) {
                                serverInfo.setLastServerListTouchTime(now);
                            }
                            modifiedKeyResults = appRegistryCache.getMulti(expiredPingKeys);
                            if (modifiedKeyResults != null && modifiedKeyResults.size() > 0) {
                                List<String> serverKeys = new ArrayList<String>(modifiedKeyResults.size());
                                i = 0;
                                do {
                                    if (modifiedKeyResults.containsKey(expiredPingKeys[i])) {
                                        serverKeys.add(expiredServers.get(i));
                                    } else {
                                        // Just making sure it is deleted
                                        appRegistryCache.remove(expiredPingKeys[i]);
                                    }
                                    i++;
                                } while (i < numExpiredKeys);
                                valueUpdater = basicInfo.getServerListUpdater(serverKeys, false);
                                appRegistryCache.atomicUpdate(registryCacheKey, 3 * expirySeconds, valueUpdater, 200);
                            } else {
                                for (i = 0; i < numExpiredKeys; i++) {
                                    // Just making sure it is deleted
                                    appRegistryCache.remove(expiredPingKeys[i]);
                                }
                            }
                        } else if (now - serverInfo.getLastServerListTouchTime() > expirySeconds * 1000L) {
                            // Just touch the server list with current server so that it does not get expired
                            valueUpdater = basicInfo.getServerListUpdater(false);
                            success = appRegistryCache.atomicUpdate(registryCacheKey, 3 * expirySeconds, valueUpdater, 200);
                            if (success) {
                                serverInfo.setLastServerListTouchTime(now);
                            }
                        }
                    }
                } else {
                    serverInfo.setCleanupTries(cleanupTries + 1);
                }
            }
        } catch (Exception e) {
            logger.error("Error Updating App Registry", e);
        }
    }

    public final CachedProperties getCommonCachedProperties() {
        return commonCachedProperties;
    }

    public final void setCommonCachedProperties(CachedProperties pcachedProperties) {
        commonCachedProperties = pcachedProperties;
    }

    public final CacheFactory getCacheFactory() {
        return cacheFactory;
    }

    public final void setCacheFactory(CacheFactory pcacheFactory) {
        cacheFactory = pcacheFactory;
    }

    public final ServerInfo getServerInfo() {
        return serverInfo;
    }

    @Override
    public void setApplicationContext(ApplicationContext pctx) throws BeansException {
        serverInfo.getConfigInfo().updateConfig(pctx);
    }
}
