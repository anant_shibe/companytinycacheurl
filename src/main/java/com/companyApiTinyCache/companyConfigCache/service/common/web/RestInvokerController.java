package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.DefaultRestClientImpl;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestClient;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestRequest;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ClassExcludeCoverage
public class RestInvokerController extends AbstractController {

    private RestClient restClient = DefaultRestClientImpl.getInstance();

    public RestInvokerController() {
        setCacheSeconds(0);
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
                                                 HttpServletResponse response) throws Exception {
        InputStream reqIp = request.getInputStream();
        String req = GenUtil.readAsStringString(reqIp);

        boolean first = true;
        String url = null;
        Map<String, String> requestHeaders = new HashMap<String, String>();
        StringBuilder sb = new StringBuilder();
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = paramNames.nextElement();
            String paramVal = request.getParameter(paramName);
            if ("ri_url".equals(paramName)) {
                url = paramVal;
            } else if (paramName.startsWith("ri_rh_")) {
                requestHeaders.put(paramName.substring(6), paramVal);
            } else {
                if (!first) {
                    sb.append('&');
                }
                sb.append(paramName).append('=').append(URLEncoder.encode(paramVal, "UTF-8"));
                first = false;
            }
        }
        RestRequest restRequest = new RestRequest(url);
        restRequest.setHeaders(requestHeaders);
        String contentType = request.getHeader("content-type");
        restRequest.setContentType(contentType);
        if (sb.length() > 0) {
            restRequest.setQueryString(sb.toString());
        }
        if (!StringUtils.isEmpty(req)) {
            restRequest.setPayload(req);
        }
        RestResponse restResponse;
        if ("GET".equalsIgnoreCase(request.getMethod())) {
            restResponse = restClient.get(restRequest);
        } else {
            restResponse = restClient.post(restRequest);
        }
        Map<String, List<String>> responseHeaders = restResponse.getHeaders();
        if (responseHeaders != null && responseHeaders.size() > 0) {
            for (String headerName : responseHeaders.keySet()) {
                List<String> headerValues = responseHeaders.get(headerName);
                // Do not pass on Transfer-Encoding header as Servlet Container will
                // Decide on Transfer-Encoding
                if (headerName != null && !headerName.equalsIgnoreCase("Transfer-Encoding") && !headerName.equalsIgnoreCase("Content-Encoding") && !headerName.equalsIgnoreCase("Content-Length") && !headerName.equalsIgnoreCase("Connection")) {
                    for (String headerValue : headerValues) {
                        response.addHeader(headerName, headerValue);
                    }
                }
            }
        }
        response.setStatus(restResponse.getStatus());
        response.getWriter().print(restResponse.getContent());
        return null;
    }

}
