package com.companyApiTinyCache.companyConfigCache.service.common.service;

public interface CommonUtilityService {

    public void updateCompanyResourcesInFileSystem(int companyId, String resourceType) throws Exception;

}
