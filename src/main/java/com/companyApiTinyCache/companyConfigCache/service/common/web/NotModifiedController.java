package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@ClassExcludeCoverage
public class NotModifiedController extends AbstractController {

    public NotModifiedController() {
        setCacheSeconds(60);
    }

    private void handleEtag(HttpServletRequest request, HttpServletResponse response) throws Exception {
        File f = new File("G:/Temp/deldir/del.txt");
        long modifiedTime = f.lastModified();
        String checkHash = request.getHeader("If-None-Match");
        String hash = "W/\"" + DigestUtils.md5Hex(String.valueOf(modifiedTime)) + '"';
        if (hash.equals(checkHash)) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            System.out.println("Sending 304 for hash: " + hash);
        } else {
            System.out.println("Sending 200 for checkHash = " + checkHash + " Actual Hash: " + hash);
            response.setHeader("Etag", hash);
            InputStream ip = new FileInputStream(f);
            String s = GenUtil.readAsStringString(ip);
            ip.close();
            ServletOutputStream op = response.getOutputStream();
            op.print(s);
        }
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String etParam = request.getParameter("et");
        int etOption = 0;
        if (etParam != null) {
            etOption = Integer.parseInt(etParam);
        }
        if (etOption > 0) {
            handleEtag(request, response);
            return null;
        }
        File f = new File("/tmp/del.txt");
        long modifiedTime = f.lastModified();
        modifiedTime -= modifiedTime % 1000;
        long checkTime = request.getDateHeader("If-Modified-Since");
        if (modifiedTime > 0 && checkTime == modifiedTime) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            System.out.println("Sending 304 for modifiedTime: " + modifiedTime);
        } else {
            System.out.println("Sending 200 for checkTime = " + checkTime + " Actual modifiedTime: " + modifiedTime);
            response.setDateHeader("Last-Modified", modifiedTime);
            InputStream ip = new FileInputStream(f);
            String s = GenUtil.readAsStringString(ip);
            ip.close();
            ServletOutputStream op = response.getOutputStream();
            op.print(s);
        }
        return null;
    }
}
