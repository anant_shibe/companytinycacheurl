package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;

/**
 *
 */
public class UserProfile implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    private boolean profileUpdated = false;

    @JsonIgnore
    private boolean ctAdmin = false;

    private int peopleId;

    /**
     * Moved to CompanyDetail.
     */
    // @JsonIgnore @Deprecated
    // private boolean accountOwner;

    // @JsonIgnore
    // private int affiliateId;

    // @JsonProperty("department_id")
    // private int departmentId;

    // @JsonProperty("invite_id")
    // private int inviteId;

    // @JsonProperty("import_id")
    // private int importId;

    // @JsonIgnore @Deprecated
    // private int status_id;

    // @JsonProperty("status") @Deprecated
    // private String status;

    private String password;

    private String username; // usually, the email given during registration

    private String screenName;

    private Date createdDate;

    private Date updatedDate;

    private String apiKey;
    
    private List<GSTDetails> gstDetails;

    /*
     * private String status;
     *
     * public String getStatus() { return status; }
     *
     * public void setStatus(String status) { this.status = status; }
     */

    private boolean isRecentlyBooked;

    private Integer token;

    public boolean isRecentlyBooked() {
        return isRecentlyBooked;
    }

    public void setRecentlyBooked(boolean isRecentlyBooked) {
        this.isRecentlyBooked = isRecentlyBooked;
    }

    /*
     * Initializing the following fields as for spring mvc command object binding , data are mapped to properties of these object hence for the empty userprofile object these should not be null.
     */
    private PersonalDetail personalDetail = new PersonalDetail();

    private ContactDetail contactDetail = new ContactDetail();

    private TravelProfile travelProfile = new TravelProfile();

    private Map<Integer, CompanyDetail> companyDetails = new LinkedHashMap<Integer, CompanyDetail>();

    private List<UserProfile> travelers;

    /**
     * Roles will now be a part of CompanyDetail. Each User now basically belongs to a Company. People in the B2C belong to the company 'Cleartrip' with the specific "channel_type"
     */

    private Collection<Resource> resources;

    @JsonIgnore
    private FaultDetail faultDetail;

    @JsonIgnore
    private String dateFormatter = "yyyy-MM-dd'T'HH:mm:ss Z";

    @JsonIgnore
    private boolean firstTimeUser = false;

    @JsonIgnore
    @Deprecated
    private boolean userConfirmed; // indicates whether user has confirmed his email or not

    @JsonIgnore
    private boolean passwordUsedToLogin;

    private List<DepositAccount> depositAccounts;

    private String openidUrl;

    private String sourceOfRegistration;

    private String mailerSubscription;

    private String device;

    private String domain;

    @JsonProperty("domain")
    public String getDomain() {
        return domain;
    }

    @JsonProperty("domain")
    public void setDomain(String domain) {
        this.domain = domain;
    }

    @JsonIgnore
    private boolean removeUser;

    private Integer unreadMessageCount;

    private List<UserCardDetail> cardDetails;

    public UserProfile() {
        // Removing all the instantiation of all objects like contactDetail, personalDetail and travelProfile
        // To accomodate for JSON binding
    }

    @JsonIgnore
    public boolean isRemoveUser() {
        return removeUser;
    }

    @JsonIgnore
    public void setRemoveUser(boolean removeUser) {
        this.removeUser = removeUser;
    }

    @JsonProperty("resources")
    public Collection<Resource> getResources() {
        return resources;
    }

    @JsonProperty("resources")
    public void setResources(Collection<Resource> resources) {
        this.resources = resources;
    }

    // @JsonIgnore
    // public boolean isAccountOwner() {
    // return accountOwner;
    // }
    //
    // @JsonIgnore
    // public void setAccountOwner(boolean accountOwner) {
    // this.accountOwner = accountOwner;
    // }

    @JsonIgnore
    public void setProfileUpdated(boolean profileUpdated) {
        if (!this.profileUpdated) {
            this.profileUpdated = profileUpdated;
        }
    }

    @JsonIgnore
    public boolean isProfileUpdated() {
        return profileUpdated;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    @JsonProperty("created_date")
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @JsonIgnore
    public void setCreatedDate(String dateString) {
        this.createdDate = GenUtil.createParsedDate(dateString, dateFormatter);
    }

    @JsonProperty("updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    @JsonProperty("updated_date")
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @JsonIgnore
    public void setUpdatedDate(String dateString) {
        this.updatedDate = GenUtil.createParsedDate(dateString, dateFormatter);
    }

    @JsonProperty("personal_data")
    public PersonalDetail getPersonalDetail() {
        return personalDetail;
    }

    @JsonProperty("personal_data")
    public void setPersonalDetail(PersonalDetail personalDetail) {
        setProfileUpdated(personalDetail.isProfiledUpdated());
        this.personalDetail = personalDetail;
    }

    @JsonProperty("contact_data")
    public ContactDetail getContactDetail() {
        return contactDetail;
    }

    @JsonProperty("contact_data")
    public void setContactDetail(ContactDetail contactDetail) {
        setProfileUpdated(contactDetail.isProfiledUpdated());
        this.contactDetail = contactDetail;
    }

    @JsonProperty("travel_profile")
    public TravelProfile getTravelProfile() {
        return travelProfile;
    }

    @JsonProperty("travel_profile")
    public void setTravelProfile(TravelProfile travelProfile) {
        if (travelProfile != null) {
            setProfileUpdated(travelProfile.isProfiledUpdated());
        }
        this.travelProfile = travelProfile;
    }

    @JsonIgnore
    public FaultDetail getFaultDetail() {
        return faultDetail;
    }

    @JsonIgnore
    public void setFaultDetail(FaultDetail faultDetail) {
        this.faultDetail = faultDetail;
    }

    @JsonProperty("screen_name")
    public String getScreenName() {
        return screenName;
    }

    @JsonProperty("screen_name")
    public void setScreenName(String screenName) {
        if (StringUtils.isNotEmpty(screenName)) {
            setProfileUpdated(true);
        }
        this.screenName = screenName;
    }

    // @JsonIgnore @Deprecated
    // public int getStatus_id() {
    // return status_id;
    // }
    //
    // @JsonIgnore @Deprecated
    // public void setStatus_id(int status_id) {
    // // if (status_id != 0)
    // // setProfileUpdated(true);
    // this.status_id = status_id;
    // }

    /*
     * should be removed isActive should be deduced by helpers from status
     */
    // @JsonIgnore @Deprecated
    // public boolean isActive() {
    // if (status_id == 1)
    // return true;
    // else
    // return false;
    // }

    /*
     * should be removed isActive should be deduced by helpers from status
     */
    // @JsonIgnore @Deprecated
    // public boolean isActiveStatusPending() {
    // if (status_id == 2)
    // return true;
    // else
    // return false;
    // }

    // @JsonIgnore
    // public void setActive(boolean active) {
    // if (active) {
    // this.status_id = 1;
    // }
    // }

    // @JsonIgnore
    // public int getAffiliateId() {
    // return affiliateId;
    // }
    //
    // @JsonIgnore
    // public void setAffiliateId(int affiliateId) {
    // this.affiliateId = affiliateId;
    // }

    @JsonIgnore
    @Deprecated
    public int getUserId() {
        return peopleId;
    }

    @JsonIgnore
    @Deprecated
    public void setUserId(int userId) {
        this.peopleId = userId;
    }

    @JsonProperty("id")
    public int getPeopleId() {
        return peopleId;
    }

    @JsonProperty("id")
    public void setPeopleId(int peopleId) {
        this.peopleId = peopleId;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public boolean isFirstTimeUser() {
        return firstTimeUser;
    }

    @JsonIgnore
    public void setFirstTimeUser(boolean firstTimeUser) {
        this.firstTimeUser = firstTimeUser;
    }

    @JsonIgnore
    @Deprecated
    public boolean isUserConfirmed() {
        return userConfirmed;
    }

    @JsonIgnore
    @Deprecated
    public void setUserConfirmed(boolean userConfirmed) {
        this.userConfirmed = userConfirmed;
    }

    @JsonIgnore
    public boolean isPasswordUsedToLogin() {
        return passwordUsedToLogin;
    }

    @JsonIgnore
    public void setPasswordUsedToLogin(boolean passwordUsedToLogin) {
        this.passwordUsedToLogin = passwordUsedToLogin;
    }

    @JsonProperty("deposit_accounts")
    public List<DepositAccount> getDepositAccounts() {
        return depositAccounts;
    }

    @JsonProperty("deposit_accounts")
    public void setDepositAccounts(List<DepositAccount> pdepositAccounts) {
        depositAccounts = pdepositAccounts;
    }

    /**
     * Refer getDepositAccounts().
     *
     * @return List<Integer>
     * @author Ulhas
     */
    @JsonIgnore
    @Deprecated
    public List<Integer> getDepositAccountIds() {
        List<Integer> depositAccountsIds = new ArrayList<Integer>(1);
        for (DepositAccount da : depositAccounts) {
            depositAccountsIds.add(da.getAccountId());
        }
        return depositAccountsIds;
    }

    /**
     * Refer setDepositAccounts(List<DepositAccount>).
     *
     * @param pdepositAccountIds
     *             List of deposit account ids
     * @author Ulhas
     */
    @JsonIgnore
    @Deprecated
    public void setDepositAccountIds(List<Integer> pdepositAccountIds) {
        List<DepositAccount> das = new ArrayList<DepositAccount>(1);
        for (Integer i : pdepositAccountIds) {
            DepositAccount da = new DepositAccount();
            da.setAccountId(i.intValue());
            das.add(da);
        }
        depositAccounts = das;
    }

    @JsonProperty("api_key")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("api_key")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonIgnore
    public boolean isCtAdmin() {
        return ctAdmin;
    }

    @JsonIgnore
    public void setCtAdmin(boolean ctAdmin) {
        this.ctAdmin = ctAdmin;
    }

    @JsonProperty("people")
    public List<UserProfile> getTravelers() {
        return travelers;
    }

    @JsonProperty("people")
    public void setTravelers(List<UserProfile> travelers) {
        this.travelers = travelers;
    }
   
    @JsonProperty("gst_details")
    public List<GSTDetails> getGstDetails() {
		return gstDetails;
	}

    @JsonProperty("gst_details")
	public void setGstDetails(List<GSTDetails> gstDetails) {
		this.gstDetails = gstDetails;
	}

	@JsonIgnore
    @Deprecated
    public List<Traveller> getTravellers() {
        List<Traveller> travellers = new ArrayList<Traveller>(1);
        if (travelers != null) {
            for (UserProfile trav : travelers) {
                Traveller t = new Traveller();

                t.setPeopleId(trav.getPeopleId());
                t.setUserName(trav.getUsername());

                if (trav.getPersonalDetail() != null) {
                    t.setTitle(trav.getPersonalDetail().getTitle());
                    t.setFirstName(trav.getPersonalDetail().getFirstName());
                    t.setLastName(trav.getPersonalDetail().getLastName());
                    t.setNickName(trav.getPersonalDetail().getNickName()); // Added By Rajendra
                    t.setDob(trav.getPersonalDetail().getDob());
                    // t.setPaxType(paxType);
                    // t.setDepartmentId(trav.getDepartmentId());
                }
                travellers.add(t);
            }
        }
        return travellers;
    }

    @JsonIgnore
    @Deprecated
    public void setTravellers(List<Traveller> travellers) {
        List<UserProfile> travelers = new ArrayList<UserProfile>(1);
        for (Traveller t : travellers) {
            UserProfile u = new UserProfile();

            u.setPeopleId(t.getPeopleId());
            u.setUsername(t.getUserName());

            u.getPersonalDetail().setTitle(t.getTitle());
            u.getPersonalDetail().setFirstName(t.getFirstName());
            u.getPersonalDetail().setLastName(t.getLastName());
            u.getPersonalDetail().setNickName(t.getNickName()); // Added By Rajendra
            u.getPersonalDetail().setDob(t.getDob());
            // u.setDepartmentId(t.getDepartmentId());

            travelers.add(u);
        }
        this.travelers = travelers;
    }

    @JsonProperty("openid_url")
    public String getOpenidUrl() {
        return openidUrl;
    }

    @JsonProperty("openid_url")
    public void setOpenidUrl(String openidUrl) {
        this.openidUrl = openidUrl;
    }

    @JsonProperty("company_details")
    public List<CompanyDetail> getCompanyDetailsFromList() {
        List<CompanyDetail> companyDetailsList = null;
        if (this.companyDetails != null) {
            companyDetailsList = new ArrayList<CompanyDetail>();
            for (Entry<Integer, CompanyDetail> entry : this.companyDetails.entrySet()) {
                companyDetailsList.add(entry.getValue());
            }
        }
        return companyDetailsList;
    }

    @JsonProperty("company_details")
    public void setCompanyDetailsFromList(List<CompanyDetail> companyDetailsList) {
        this.companyDetails = new LinkedHashMap<Integer, CompanyDetail>();
        for (CompanyDetail cd : companyDetailsList) {
            this.companyDetails.put(cd.getCompanyId(), cd);
        }

    }

    @JsonIgnore
    public Map<Integer, CompanyDetail> getCompanyDetails() {
        return companyDetails;
    }

    @JsonIgnore
    public void setCompanyDetails(Map<Integer, CompanyDetail> companyDetail) {
        this.companyDetails = companyDetail;
    }

    @JsonProperty("unread_message_count")
    public Integer getUnreadMessageCount() {
        return unreadMessageCount;
    }

    @JsonProperty("unread_message_count")
    public void setUnreadMessageCount(int unreadMessageCount) {
        this.unreadMessageCount = new Integer(unreadMessageCount);
    }

    @JsonProperty("source_of_registration")
    public String getSourceOfRegistration() {
        return sourceOfRegistration;
    }

    @JsonProperty("source_of_registration")
    public void setSourceOfRegistration(String sourceOfRegistration) {
        this.sourceOfRegistration = sourceOfRegistration;
    }

    @JsonProperty("mailer_subscription")
    public String getMailerSubscription() {
        return mailerSubscription;
    }

    @JsonProperty("mailer_subscription")
    public void setMailerSubscription(String mailerSubscription) {
        this.mailerSubscription = mailerSubscription;
    }

    @JsonProperty("device")
    public String getDevice() {
        return device;
    }

    @JsonProperty("device")
    public void setDevice(String device) {
        this.device = device;
    }

    @JsonIgnore
    public boolean getExpressCheckoutEnabled() {
        boolean expressCheckoutEnabled = false;

        if (getContactDetail() != null) {
            ContactDetail cDetail = getContactDetail();
            if (cDetail.getOtherDetails() != null) {
                for (OtherDetail oDetail : cDetail.getOtherDetails()) {
                    if ("ExpressCheckout".equalsIgnoreCase(oDetail.getCategory()) && "ExpressCheckoutEnabled".equalsIgnoreCase(oDetail.getName())) {
                        if ("Y".equalsIgnoreCase(oDetail.getValue())) {
                            expressCheckoutEnabled = true;
                            break;
                        }
                    }
                }
            }
        }

        return expressCheckoutEnabled;
    }

    @JsonIgnore
    public List<String> getWallentCurrencyList() {
        List<String> walletCurrencyList = new ArrayList<String>();

        if (getContactDetail() != null) {
            ContactDetail cDetail = getContactDetail();
            if (cDetail.getOtherDetails() != null) {
                for (OtherDetail oDetail : cDetail.getOtherDetails()) {
                    if ("WALLET".equalsIgnoreCase(oDetail.getCategory()) && "currency".equalsIgnoreCase(oDetail.getName())) {
                        walletCurrencyList.add(oDetail.getValue());
                    }
                }
            }
        }

        return walletCurrencyList;
    }

    @JsonProperty("card_details")
    public List<UserCardDetail> getCardDetails() {
        return cardDetails;
    }

    @JsonProperty("card_details")
    public void setCardDetails(List<UserCardDetail> cardDetails) {
        this.cardDetails = cardDetails;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UserProfile)) {
            return false;
        }
        UserProfile user = (UserProfile) obj;

        if (peopleId != user.getPeopleId()) {
            return false;
        }
        if ((username != null || user.getUsername() != null) && !GenUtil.equals(username, user.getUsername())) {
            return false;
        }
        if ((screenName != null || user.getScreenName() != null) && !GenUtil.equals(screenName, user.getScreenName())) {
            return false;
        }
        if ((sourceOfRegistration != null || user.getSourceOfRegistration() != null) && !GenUtil.equals(sourceOfRegistration, user.getSourceOfRegistration())) {
            return false;
        }
        if ((mailerSubscription != null || user.getMailerSubscription() != null) && !GenUtil.equals(mailerSubscription, user.getMailerSubscription())) {
            return false;
        }
        if ((apiKey != null && user.getApiKey() != null) && !GenUtil.equals(apiKey, user.getApiKey())) {
            return false;
        }
        if ((personalDetail != null || user.getPersonalDetail() != null) && !GenUtil.equals(personalDetail, user.getPersonalDetail())) {
            return false;
        }
        if ((contactDetail != null || user.getContactDetail() != null) && !GenUtil.equals(contactDetail, user.getContactDetail())) {
            return false;
        }
        if ((travelProfile != null || user.getTravelProfile() != null) && !GenUtil.equals(travelProfile, user.getTravelProfile())) {
            return false;
        }
        if ((travelers != null || user.getTravelers() != null) && !GenUtil.sameCollections(travelers, user.getTravelers())) {
            return false;
        }
        if ((resources != null || user.getResources() != null) && !GenUtil.sameCollections(resources, user.getResources())) {
            return false;
        }
        if ((depositAccounts != null || user.getDepositAccounts() != null) && !GenUtil.sameCollections(depositAccounts, user.getDepositAccounts())) {
            return false;
        }
        if ((openidUrl != null || user.getOpenidUrl() != null) && !GenUtil.equals(openidUrl, user.getOpenidUrl())) {
            return false;
        }
        if ((companyDetails != null || user.getCompanyDetails() != null) && !GenUtil.sameMaps(companyDetails, user.getCompanyDetails())) {
            return false;
        }
        if (unreadMessageCount != user.getUnreadMessageCount()) {
            return false;
        }

        return true;
    }

    // Overriding the hashcode() to avoid checkstyle error
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @JsonIgnore
    public String getFirstName() {
        if (this.getPersonalDetail() != null) {
            return this.getPersonalDetail().getFirstName();
        } else {
            return "";
        }
    }

    @JsonIgnore
    public String getLastName() {
        if (this.getPersonalDetail() != null) {
            return this.getPersonalDetail().getLastName();
        } else {
            return "";
        }
    }

    @JsonIgnore
    public String getTitle() {

        if (this.getPersonalDetail() != null) {
            return this.getPersonalDetail().getTitle();
        } else {
            return "";
        }
    }

    @JsonIgnore
    public String getMobileNo() {

        if (this.getContactDetail() != null) {
            return this.getContactDetail().getMobile();
        } else {
            return "";
        }
    }

    @JsonProperty("token")
    public Integer getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(Integer token) {
        this.token = token;
    }

    public GSTDetails getDefaultGSTDetails() {
    	GSTDetails gstDetail = null;
    	if (CollectionUtils.isNotEmpty(gstDetails)) {
    		gstDetail = gstDetails.iterator().next();
    	}
    	return gstDetail;
    }
}
