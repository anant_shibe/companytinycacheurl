package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.messaging.MessageWithId;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.TimeKeeper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 **Contains properties for logging.
 */
public class CtBaseStats extends ConnectorStats implements MessageWithId {

    private static final long serialVersionUID = 3489801490756419030L;

    private static final Log log = LogFactory.getLog(ConnectorStats.class);

    protected String instanceId;

    protected int ctHttpStatus;

    protected int ctRequestSize;

    protected String ctRequest;

    protected int ctResponseSize;

    protected String ctResponse;

    protected String apacheCookie;

    protected long userId;

    protected String emailId;

    protected String hostIp;

    protected String sourceIp;

    protected String serverId;

    protected String requestId;

    protected String requestMethod;

    protected String url;
    
    protected String languagePreference;
    
	protected Map<String, String[]> requestParams;

    protected Map<String, String[]> requestHeaders;

    private List<String> timedOutConnectors;

    private long timeOut;

    private String serverIp;

    public String getLanguagePreference() {
 		return languagePreference;
 	}

 	public void setLanguagePreference(String languagePreference) {
 		this.languagePreference = languagePreference;
 	}
 	
    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getReleaseVersion() {
        return releaseVersion;
    }

    public void setReleaseVersion(String releaseVersion) {
        this.releaseVersion = releaseVersion;
    }

    private String releaseVersion;

    protected static boolean isLoggingEnabled(HttpServletRequest request, Map<String, String> properties, String baseLoggingProperty, String specificLoggingProperty) {
        boolean loggingEnabled = CachedProperties.getBooleanPropertyValue(properties, baseLoggingProperty, true) && CachedProperties.getBooleanPropertyValue(properties, specificLoggingProperty, true);
        if (!loggingEnabled) {
            loggingEnabled = GenUtil.getBooleanParameter(request, DEBUG_PARAM, false) || GenUtil.getBooleanParameter(request, LRR_PARAM, false)
                    || GenUtil.getBooleanParameter(request, LT_PARAM, false);
        }

        return loggingEnabled;
    }

    @Deprecated
    // Delete this method if today is after October-2010.
    protected static boolean isLoggingEnabled(HttpServletRequest request, Map<String, String> properties, String specificLoggingProperty) {
        return isLoggingEnabled(request, properties, "ct.air.logging", specificLoggingProperty);
    }

    @Override
    public void doInit(HttpServletRequest request, int pstatsType, CachedProperties commonCachedProperties, boolean lrr, boolean lt) {
        if (request != null) {
            super.doInit(request, pstatsType, commonCachedProperties, lrr, lt);
            hostIp = GenUtil.getRemoteAddr(request);
            if (GenUtil.getBooleanParameter(request, DEBUG_PARAM, false)) {
                lrr = true;
            }
            String usrId = StringUtils.trimToNull(request.getParameter("usrid"));
            if (usrId == null) {
                usrId = StringUtils.trimToNull(request.getHeader("usrid"));
            }
            if (usrId != null) {
                userId = Long.parseLong(usrId);
            }
            if (request.getRequestURL() != null) {
            	url = request.getRequestURL().toString();
            }
            requestMethod = request.getMethod();
            String hIp = StringUtils.trimToNull(request.getParameter("hstip"));
            if (hIp == null) {
                hIp = StringUtils.trimToNull(request.getHeader("hstip"));
            }
            if (hIp == null) {
                hIp = GenUtil.getRemoteAddr(request);
            }
            if (hIp != null) {
                hostIp = hIp;
            }
            String srcIp = StringUtils.trimToNull(request.getParameter("srcip"));
            if (srcIp == null) {
                srcIp = StringUtils.trimToNull(request.getHeader("srcip"));
            }
            if (srcIp != null) {
                sourceIp = srcIp;
            }
            String ac = StringUtils.trimToNull(request.getParameter("ac"));
            if (ac == null) {
                ac = StringUtils.trimToNull(request.getHeader("ac"));
            }
            if (ac != null) {
                apacheCookie = ac;
            }
            Enumeration<?> paramNames = request.getParameterNames();
            while (paramNames.hasMoreElements()) {
                String paramName = (String) paramNames.nextElement();
                String[] paramValues = request.getParameterValues(paramName);
                if (requestParams == null) {
                    requestParams = new HashMap<String, String[]>();
                }
                requestParams.put(paramName, paramValues);
            }
            List<String> tmpList = new ArrayList<String>(4);
            String[] tmpArray = new String[0];
            paramNames = request.getHeaderNames();
            while (paramNames.hasMoreElements()) {
                String paramName = (String) paramNames.nextElement();
                tmpList.clear();
                Enumeration<?> headerEnumeration = request.getHeaders(paramName);
                while (headerEnumeration.hasMoreElements()) {
                    String headerValue = (String) headerEnumeration.nextElement();
                    tmpList.add(headerValue);
                }
                String[] headerValues = tmpList.toArray(tmpArray);
                if (requestHeaders == null) {
                    requestHeaders = new HashMap<String, String[]>();
                }
                requestHeaders.put(paramName, headerValues);
                try {
                    requestHeaders.put("isBento", isBentoRequest(request));
                } catch (Exception ex) {
                    log.error("Error fetching if the request is Bento  " + ex);
                }
            }
            if (commonCachedProperties != null) {
                serverId = commonCachedProperties.getPropertyValue("ct.common.hostname", "unknownhost") + commonCachedProperties.getPropertyValue("ct.common.local.serverid");
            }
            if (commonCachedProperties != null) {
                serverIp = commonCachedProperties.getPropertyValue("ct.common.ip");
            }
         /*   Cookie langPreference = GenUtil.getCookieFromRequest(request, "lp");
            if (langPreference != null && langPreference.getValue() != null ) {
            	languagePreference =  langPreference.getValue();
            }*/
        }

    }

    private void hideCardInfo(Map<String, String[]> params) {
        if (params != null) {
            for (String paramName : params.keySet()) {
                String[] values = params.get(paramName);
                for (int i = 0; i < values.length; i++) {
                    values[i] = GenUtil.hideCardInfo(paramName, values[i]);
                }
            }
        }
    }

    public void hideCardInfo() {
        ctRequest = GenUtil.hideCardInfo(null, ctRequest);
        ctResponse = GenUtil.hideCardInfo(null, ctResponse);
        if (apiCallStats != null) {
            for (ApiCallStats stat : apiCallStats) {
                if (stat != null) {
                    stat.setRequest(GenUtil.hideCardInfo(null, stat.getRequest()));
                    stat.setResponse(GenUtil.hideCardInfo(null, stat.getResponse()));
                }
            }
        }
        hideCardInfo(requestHeaders);
        hideCardInfo(requestParams);
    }

    public void hidePassword() {
        if (requestParams != null) {
            String[] original = requestParams.get("password");
            if (original != null) {
                String[] blanked = new String[1];
                String replacement = original[0].replaceAll(".", "*");
                blanked[0] = replacement;
                requestParams.put("password", blanked);
            }
        }
    }

    public CtBaseStats deepCopy() {
        List<String> savedTimedOutConnectors = timedOutConnectors;
        timedOutConnectors = null;
        List<ApiCallStats> savedApiCallStats = apiCallStats;
        apiCallStats = null;
        List<LogMessage> savedMessages = messages;
        messages = null;
        TimeKeeper savedTimeKeeper = timeKeeper;
        if (savedTimeKeeper != null) {
            timeKeeper = new TimeKeeper();
        }
        CtBaseStats ctBaseStats = null;
        try {
            ctBaseStats = (CtBaseStats) GenUtil.deepCopy(this);
            if (inactiveStats != null) {
                // inactiveStats is transient and hence we need to do it manually
                BaseStats[] inactiveStatsCopy = inactiveStats.clone();
                int i;
                for (i = 0; i < inactiveStatsCopy.length; i++) {
                    CtBaseStats inactiveStats = null;
                    try {
                        inactiveStats = (CtBaseStats) inactiveStatsCopy[i];
                        inactiveStats = (CtBaseStats) (inactiveStats).deepCopy();
                        inactiveStatsCopy[i] = inactiveStats;
                    } catch (Exception e) {
                        inactiveStatsCopy[i] = null;
                        log.error("Error cloning inactive stats: " + inactiveStats + " setting to null", e);
                    }
                }
                ctBaseStats.inactiveStats = inactiveStatsCopy;
            }
        } catch (Exception e) {
            log.error("Error cloning stats: " + this, e);
        } finally {
            timedOutConnectors = savedTimedOutConnectors;
            apiCallStats = savedApiCallStats;
            messages = savedMessages;
            timeKeeper = savedTimeKeeper;
        }

        return ctBaseStats;
    }

    /**
     * Getter for instanceId.
     * @return the instanceId
     */
    public String getInstanceId() {
        return instanceId;
    }

    @Override
    public String getMessageId() {
        return instanceId;
    }

    /**
     * Setter for instanceId.
     * @param pinstanceId
     *            the instanceId to set
     */
    public void setInstanceId(String pinstanceId) {
        instanceId = pinstanceId;
    }

    /**
     * Getter for ctHttpStatus.
     * @return the ctHttpStatus
     */
    public int getCtHttpStatus() {
        return ctHttpStatus;
    }

    /**
     * Setter for ctHttpStatus.
     * @param pctHttpStatus
     *            the ctHttpStatus to set
     */
    public void setCtHttpStatus(int pctHttpStatus) {
        ctHttpStatus = pctHttpStatus;
    }

    /**
     * Getter for ctRequestSize.
     * @return the ctRequestSize
     */
    public int getCtRequestSize() {
        return ctRequestSize;
    }

    /**
     * Setter for ctRequestSize.
     * @param pctRequestSize
     *            the ctRequestSize to set
     */
    public void setCtRequestSize(int pctRequestSize) {
        ctRequestSize = pctRequestSize;
    }

    /**
     * Getter for ctRequest.
     * @return the ctRequest
     */
    public String getCtRequest() {
        return ctRequest;
    }

    /**
     * Setter for ctRequest.
     * @param pctRequest
     *            the ctRequest to set
     */
    public void setCtRequest(String pctRequest) {
        ctRequest = pctRequest;
    }

    /**
     * Getter for ctResponseSize.
     * @return the ctResponseSize
     */
    public int getCtResponseSize() {
        return ctResponseSize;
    }

    /**
     * Setter for ctResponseSize.
     * @param pctResponseSize
     *            the ctResponseSize to set
     */
    public void setCtResponseSize(int pctResponseSize) {
        ctResponseSize = pctResponseSize;
    }

    /**
     * Getter for ctResponse.
     * @return the ctResponse
     */
    public String getCtResponse() {
        return ctResponse;
    }

    /**
     * Setter for ctResponse.
     * @param pctResponse
     *            the ctResponse to set
     */
    public void setCtResponse(String pctResponse) {
        ctResponse = pctResponse;
    }

    /**
     * Getter for apacheCookie.
     * @return the apacheCookie
     */
    public String getApacheCookie() {
        return apacheCookie;
    }

    /**
     * Setter for apacheCookie.
     * @param papacheCookie
     *            the apacheCookie to set
     */
    public void setApacheCookie(String papacheCookie) {
        apacheCookie = papacheCookie;
    }

    /**
     * Getter for userId.
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Setter for userId.
     * @param puserId
     *            the userId to set
     */
    public void setUserId(long puserId) {
        userId = puserId;
    }

    /**
     * Getter for hostIp.
     * @return the hostIp
     */
    public String getHostIp() {
        return hostIp;
    }

    /**
     * Setter for hostIp.
     * @param phostIp
     *            the hostIp to set
     */
    public void setHostIp(String phostIp) {
        hostIp = phostIp;
    }

    /**
     * Getter for sourceIp.
     * @return the sourceIp
     */
    public String getSourceIp() {
        return sourceIp;
    }

    /**
     * Setter for sourceIp.
     * @param psourceIp
     *            the sourceIp to set
     */
    public void setSourceIp(String psourceIp) {
        sourceIp = psourceIp;
    }

    public Map<String, String[]> getRequestParams() {
        return requestParams;
    }

    public void setRequestParams(Map<String, String[]> prequestParams) {
        requestParams = prequestParams;
    }

    public Map<String, String[]> getRequestHeaders() {
        return requestHeaders;
    }

    public void setRequestHeaders(Map<String, String[]> prequestHeaders) {
        requestHeaders = prequestHeaders;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String pserverId) {
        serverId = pserverId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String prequestId) {
        requestId = prequestId;
    }

    /**
     * @return the timedOutConnectors
     */
    public List<String> getTimedOutConnectors() {
        return timedOutConnectors;
    }

    /**
     * @param timedOutConnectors
     *            the timedOutConnectors to set
     */
    public void setTimedOutConnectors(List<String> timedOutConnectors) {
        this.timedOutConnectors = timedOutConnectors;
    }

    /**
     * @return the timeOut
     */
    public long getTimeOut() {
        return timeOut;
    }

    /**
     * @param timeOut
     *            the timeOut to set
     */
    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public static void closeStats(HttpServletResponse response) {
		closeStats(response.getStatus());
	}

	public static void closeStats(int responseCode) {
		if (BaseStats.threadLocal() != null && BaseStats.threadLocal() instanceof CtBaseStats) {
        	CtBaseStats ctBaseStats = (CtBaseStats) BaseStats.threadLocal(); 
        	if (ctBaseStats != null) {
        		ctBaseStats.setCtHttpStatus(responseCode);
        		ctBaseStats.setCallOut(System.currentTimeMillis());
        	}
        }
    }

    public static String[] isBentoRequest(HttpServletRequest request) {
        String[] isBento = new String[1];
        if (isBentoCookiePresent(request) || isBentoHeaderPresent(request)) {
            isBento[0] = "true";
        } else {
            isBento[0] = "false";
        }
        return isBento;
    }

    public static Boolean isBentoHeaderPresent(HttpServletRequest request) {
        Boolean isBentoHeaderPresent = false;
        String bentoHeader = request.getHeader("isBento");
        if (StringUtils.isNotBlank(bentoHeader) && Boolean.valueOf(bentoHeader)) {
            isBentoHeaderPresent = true;
        }
        return isBentoHeaderPresent;
    }

    public static Boolean isBentoCookiePresent(HttpServletRequest request) {
        Boolean isBentoCookiePresent = false;
        Cookie isBentoCookie = GenUtil.getCookieFromRequest(request, "isBento");
        if (isBentoCookie != null && Boolean.valueOf(isBentoCookie.getValue())) {
            isBentoCookiePresent = true;
        }
        return isBentoCookiePresent;

    }
}