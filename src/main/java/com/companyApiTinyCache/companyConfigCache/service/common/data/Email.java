package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Email implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private int sequenceNumber;

    private String type;

    private String emailId;

    @JsonProperty("seq_no")
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    @JsonProperty("seq_no")
    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @JsonProperty("category")
    public String getType() {
        return type;
    }

    @JsonProperty("category")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("email_id")
    public String getEmailId() {
        return emailId;
    }

    @JsonProperty("email_id")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Email == false)
            return false;
        Email email = (Email) obj;
        if ((type != null && email.getType() != null) && !GenUtil.equals(type, email.getType())) {
            return false;
        }
        if ((emailId != null && email.getEmailId() != null) && !GenUtil.equals(emailId, email.getEmailId())) {
            return false;
        }

        return true;
    }

}
