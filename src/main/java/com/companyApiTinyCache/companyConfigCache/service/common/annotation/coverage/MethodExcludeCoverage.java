package com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
public @interface MethodExcludeCoverage {
    String info() default "";
}
