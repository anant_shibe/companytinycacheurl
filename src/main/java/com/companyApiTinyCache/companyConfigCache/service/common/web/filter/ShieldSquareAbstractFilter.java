package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.SecurityBean;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.web.filter.ss2.SS2;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ShieldSquareAbstractFilter.
 * @author amith
 */
@ClassExcludeCoverage
public abstract class ShieldSquareAbstractFilter implements Filter {

    private static final Log logger = LogFactory.getLog(ShieldSquareAbstractFilter.class);

    private CachedProperties commonCachedProperties;

    @Override
    public void destroy() {
    }

    /**
     * SSCALLTYPE.
     * @author amith
     */
    public static enum SSCALLTYPE {
        PAGE_LOAD(1), AJAX(3), FORM_SUBMIT(2);

        private int intValue;

        private SSCALLTYPE(int intValue) {
            this.intValue = intValue;
        }

        public int getIntValue() {
            return intValue;
        }
    };

    public abstract SSCALLTYPE getCallType();

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        boolean isEnabled = commonCachedProperties.getBooleanPropertyValue("ct.common.shieldsquare.enabled", false);
        boolean canContinue = true;
        boolean continueWithShield = callShieldSquare(request, commonCachedProperties);
        if (isEnabled && continueWithShield) {
            try {
                HttpServletRequest httpRequest = (HttpServletRequest) request;
                if (logger.isDebugEnabled()) {
                    logger.debug("ENTERING doFilter SS2");
                    logger.debug("Request " + httpRequest.getRequestURI());
                }
                SS2 ss2Obj = new SS2(commonCachedProperties);
                String shieldsquareUsername = "";
                int shieldsquareCallType = getCallType().getIntValue();
                String shieldsquarePid = "";

                SecurityBean bean = (SecurityBean) httpRequest.getAttribute(CommonEnumConstants.SESSION_SECURITY_BEAN_ID);

                if (bean != null) {
                    shieldsquareUsername = bean.getEmailAddress();
                }

                JsonNode ss2respJson = ss2Obj
                        .shieldsquareValidateRequest(httpRequest, (HttpServletResponse) response, shieldsquarePid, shieldsquareUsername, shieldsquareCallType);

                String ss2ResponseCode = null;
                if (ss2respJson != null && ss2respJson.get("responsecode") != null) {
                    ss2ResponseCode = ss2respJson.get("responsecode").toString().replaceAll("\"", "");
                }
                if ("0".equals(ss2ResponseCode)) {
                    logger.debug("responsecode: Allow");
                } else if ("2".equals(ss2ResponseCode)) {
                    logger.info("responsecode: 2. Show Captcha");
                } else if ("3".equals(ss2ResponseCode)) {
                    logger.info("responsecode: 3. Block");
                } else if ("4".equals(ss2ResponseCode)) {
                    logger.info("responsecode: 4. Feed Dummy Data");
                } else {
                    logger.error("responsecode: " + ss2ResponseCode);
                }
            } catch (Exception e) {
                logger.error("Exception in SS2 Filter; Exception: " + e, e);
            }
        }
        chain.doFilter(request, response);
        // decision can be taken here according to canContinue
    }

    public abstract boolean callShieldSquare(ServletRequest request, CachedProperties commonCachedProperties);

    protected ApplicationContext getContext(FilterConfig filterConfig) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        ApplicationContext ctx = this.getContext(config);
        commonCachedProperties = (CachedProperties) ctx.getBean("commonCachedProperties");
    }
}
