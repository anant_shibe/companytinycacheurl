package com.companyApiTinyCache.companyConfigCache.service.common.logging;

public enum ErrorSeverity {

    WARNING, CRITICAL;
}
