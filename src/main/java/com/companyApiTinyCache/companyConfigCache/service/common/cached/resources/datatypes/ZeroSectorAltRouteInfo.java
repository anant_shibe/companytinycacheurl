package com.companyApiTinyCache.companyConfigCache.service.common.cached.resources.datatypes;


import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Amit Kumar Sharma
 */
public class ZeroSectorAltRouteInfo {

    // IATA and Railway Station Code ...
    private String altFlightSourceIATA;
    private String altFlightDestinationIATA;
    private String altTrainsSourceStation;
    private String altTrainsDestinationStation;

    // ... And respective city names
    private String altTrainsSourceCity;
    private String altTrainsDestinationCity;

    public ZeroSectorAltRouteInfo() {
    }

    public boolean isAltFlightEnabled() {
        // both alt src and destination for flight should be provided
        boolean result = !StringUtils.isEmpty(altFlightSourceIATA) && !StringUtils.isEmpty(altFlightDestinationIATA);
        return result;
    }

    public boolean isAltTrainEnabled() {
        // both alt src and destination for train should be provided
        boolean result = !StringUtils.isEmpty(altTrainsSourceStation) && !StringUtils.isEmpty(altTrainsDestinationStation);
        return result;
    }

    public String getAltFlightSourceIATA() {
        return altFlightSourceIATA;
    }

    public void setAltFlightSourceIATA(String altFlightSourceIATA) {
        this.altFlightSourceIATA = altFlightSourceIATA;
    }

    public String getAltFlightDestinationIATA() {
        return altFlightDestinationIATA;
    }

    public void setAltFlightDestinationIATA(String altFlightDestinationIATA) {
        this.altFlightDestinationIATA = altFlightDestinationIATA;
    }

    public String getAltTrainsSourceStation() {
        return altTrainsSourceStation;
    }

    public void setAltTrainsSourceStation(String altTrainsSourceStation) {
        this.altTrainsSourceStation = altTrainsSourceStation;
    }

    public String getAltTrainsDestinationStation() {
        return altTrainsDestinationStation;
    }

    public void setAltTrainsDestinationStation(String altTrainsDestinationStation) {
        this.altTrainsDestinationStation = altTrainsDestinationStation;
    }

    public String getAltTrainsSourceCity() {
        return altTrainsSourceCity;
    }

    public void setAltTrainsSourceCity(String altTrainsSourceCity) {
        this.altTrainsSourceCity = altTrainsSourceCity;
    }

    public String getAltTrainsDestinationCity() {
        return altTrainsDestinationCity;
    }

    public void setAltTrainsDestinationCity(String altTrainsDestinationCity) {
        this.altTrainsDestinationCity = altTrainsDestinationCity;
    }
}
