package com.companyApiTinyCache.companyConfigCache.service.common.data;

public class CompanyConfigDetailsBean {

	private Integer companyId;

	private String name;

	private String screenName;

	private String companyCode;

	private String enabledProducts;

	private String supportNumber;

	private String resourcepath;

	private String couponCodeEnable;

	private String privacyLink;

	public String getPrivacyLink() {
		return privacyLink;
	}

	public void setPrivacyLink(String privacyLink) {
		this.privacyLink = privacyLink;
	}

	public String getCouponCodeEnable() {
		return couponCodeEnable;
	}

	public void setCouponCodeEnable(String couponCodeEnable) {
		this.couponCodeEnable = couponCodeEnable;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEnabledProducts() {
		return enabledProducts;
	}

	public void setEnabledProducts(String enabledProducts) {
		this.enabledProducts = enabledProducts;
	}

	public String getSupportNumber() {
		return supportNumber;
	}

	public void setSupportNumber(String supportNumber) {
		this.supportNumber = supportNumber;
	}

	public String getResourcepath() {
		return resourcepath;
	}

	public void setResourcepath(String resourcepath) {
		this.resourcepath = resourcepath;
	}

}
