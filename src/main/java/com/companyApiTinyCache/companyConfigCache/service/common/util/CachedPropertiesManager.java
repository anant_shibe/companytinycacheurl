package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.util.Map;

/**
 * Utility class similar to ObjectFromProperty to avoid repeated parsing of json properties. It can also save on HashMap lookups for property values.
 * A config loader generates a config pojo by reading required properties and doing required parsing.
 * This class keeps the config along with the resource that generated it (In configHolder). The getConfig method does a check if the resource has
 * changed. If so it reloads the config, otherwise it returns the existing config. Since the getConfig method does these checks it is better to
 * call getConfig once per method and assign it to a local variable as is done in HotelServiceImpl.
 *
 * Please See HotelServiceImpl for an example for parsing the property ct.hotel.dp-rates-sourcetypes.
 *
 * @author suresh
 *
 * @param <C> Config class. Best to have it as a nested class of a Spring Bean.
 */
public class CachedPropertiesManager<C> {
    private static class ConfigHolder<C> {
        private Map<String, String> cachedPropertyResource;

        private C config;
    }

    private ConfigLoader<C> configLoader;

    private CachedProperties cachedProperties;

    private volatile ConfigHolder<C> configHolder;

    public CachedPropertiesManager(CachedProperties pcachedProperties, ConfigLoader<C> pconfigLoader) {
        cachedProperties = pcachedProperties;
        configLoader = pconfigLoader;
        configHolder = new ConfigHolder<C>();
    }

    public C getConfig() {
        ConfigHolder<C> ch = configHolder;
        Map<String, String> currentResource = cachedProperties.getResource();
        if (ch.cachedPropertyResource != currentResource) {
            synchronized (this) {
                ch = configHolder;
                if (ch.cachedPropertyResource != currentResource) {
                    ch = new ConfigHolder<C>();
                    ch.cachedPropertyResource = currentResource;
                    ch.config = configLoader.loadConfig(currentResource);
                    configHolder = ch;
                }
            }
        }

        return ch.config;
    }
}
