package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.connector.UserAPIConnector;
import com.companyApiTinyCache.companyConfigCache.service.common.data.UserProfile;
import com.companyApiTinyCache.companyConfigCache.service.common.exceptions.JWTException;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import io.jsonwebtoken.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sudhanshu
 *
 */
@ClassExcludeCoverage
public class JWTServiceImpl implements JWTService {

	private static final Log logger = LogFactory.getLog(JWTServiceImpl.class);

	private static final String EXPIRY_IN_MINUTE = "ct.common.services.jwtservice.expiry_in_minute";
	private static final String SECRET_KEY = "ct.common.services.jwtservice.secret_key";
	private static final int DEFAULT_EXPIRY = 30;
	private static final int DEfAULT_PEOPLE_ID = -1;

	private UserAPIConnector userAPIConnector;
	private CachedProperties commonCachedProperties;

	public UserAPIConnector getUserAPIConnector() {
		return userAPIConnector;
	}

	public void setUserAPIConnector(UserAPIConnector userAPIConnector) {
		this.userAPIConnector = userAPIConnector;
	}

	public CachedProperties getCommonCachedProperties() {
		return commonCachedProperties;
	}

	public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
		this.commonCachedProperties = commonCachedProperties;
	}

	public String getToken(UserProfile puserProfile) {
		if (puserProfile != null) {
			return getToken(puserProfile.getPeopleId());
		} else {
			return getToken(DEfAULT_PEOPLE_ID);
		}
	}

	public String extractToken(HttpServletRequest request) {
		String authorizationHeader = request.getHeader("Authorization");
		// try to find out token in get param if possible
		if (authorizationHeader == null || authorizationHeader.length() < 8)
			return "";
		// Authorization header looks like Authorization: Bearer {$Token}
		return authorizationHeader.substring(7);
	}

	public String getKey() {
		return commonCachedProperties.getPropertyValue(SECRET_KEY);
	}

	public int getExpiry() {
		return commonCachedProperties.getIntPropertyValue(EXPIRY_IN_MINUTE, DEFAULT_EXPIRY);
	}

	public String getToken(long peopleId) {
		Map<String, Object> claims = new HashMap<>();
		Calendar calendar = Calendar.getInstance();
		Date iat = calendar.getTime();
		calendar.add(Calendar.MINUTE, getExpiry());
		Date exp = calendar.getTime();
		return Jwts.builder().claim("id", peopleId).setExpiration(exp).setIssuedAt(iat)
				.signWith(SignatureAlgorithm.HS512, getKey()).compact();
	}

	public void validateToken(HttpServletRequest request) throws JWTException {
		String token = extractToken(request);
		validateToken(token);
	}

	public void validateToken(String token) throws JWTException {
		try {
			Jws<Claims> claims = Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token);
		} catch (MissingClaimException e) {
			throw new JWTException("Missing claim. Could not identify user");
		} catch (IncorrectClaimException e) {
			throw new JWTException("Incorrect claim. Could not identify user");
		} catch (ExpiredJwtException e) {
			throw new JWTException("Token Expired");
		} catch (SignatureException e) {
			throw new JWTException("Wrong Signature");
		} catch (IllegalArgumentException e){
			throw new JWTException("Missing token");
		}
		// token ok here
	}

	public UserProfile getUserProfile(String token) throws JWTException {
		Jws<Claims> claims = null;
		try {
			claims = Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token);
		} catch (MissingClaimException e) {
			throw new JWTException("Missing claim. Could not identify user");
		} catch (IncorrectClaimException e) {
			throw new JWTException("Incorrect claim. Could not identify user");
		} catch (ExpiredJwtException e) {
			throw new JWTException("Token Expired.");
		} catch (SignatureException e) {
			throw new JWTException("Wrong Signature.");
		}
		int peopleId = Integer.parseInt(claims.getBody().get("id").toString());
		return userAPIConnector.getUserProfile(peopleId);
	}
}
