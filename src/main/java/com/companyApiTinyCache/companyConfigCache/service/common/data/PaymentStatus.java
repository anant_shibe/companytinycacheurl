package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.PaymentValidationStatusCode;

import java.util.Map;

/**
 * Status of a single payment returned by payment module.
 * 
 * @author Ramesh - Copied from Amlan's code in Hotels
 */
public class PaymentStatus {

    private PaymentValidationStatusCode statusCode;

    private String statusMessage;

    private boolean redirectionRequired;

    private String redirectionUrl;

    private String redirectionMethod;

    private Map<String, String> redirectionParameters;

    public PaymentValidationStatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(PaymentValidationStatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public boolean isRedirectionRequired() {
        return redirectionRequired;
    }

    public void setRedirectionRequired(boolean redirectionRequired) {
        this.redirectionRequired = redirectionRequired;
    }

    public String getRedirectionUrl() {
        return redirectionUrl;
    }

    public void setRedirectionUrl(String redirectionUrl) {
        this.redirectionUrl = redirectionUrl;
    }

    public String getRedirectionMethod() {
        return redirectionMethod;
    }

    public void setRedirectionMethod(String redirectionMethod) {
        this.redirectionMethod = redirectionMethod;
    }

    public Map<String, String> getRedirectionParameters() {
        return redirectionParameters;
    }

    public void setRedirectionParameters(Map<String, String> redirectionParameters) {
        this.redirectionParameters = redirectionParameters;
    }

}
