package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;

import java.util.ArrayList;
import java.util.TreeMap;

public class AdminBean {
    private CachedProperties commonCachedProperties;
    private static TreeMap<Long, Memory> memories = new TreeMap<Long, Memory>();

    public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }

    public Memory poll() {
        Runtime runtime = Runtime.getRuntime();
        Memory memory = new Memory();
        memory.setFree(runtime.freeMemory());
        memory.setTotal(runtime.maxMemory());
        int maxRecords = commonCachedProperties.getIntPropertyValue("ct.common.admin.list.max", 50);
        addMemory(memory, maxRecords);
        return memory;
    }

    private static synchronized void addMemory(Memory memory, int maxRecords) {
        memories.put(memory.getTimeMin(), memory);
        while (memories.size() > maxRecords) {
            Long key = memories.firstKey();
            memories.remove(key);
        }
    }

    public static synchronized ArrayList<Memory> getMemories() {
        return new ArrayList<Memory>(memories.values());
    }
}
