package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.rule.pojo.CashBack;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public interface ActionsEvaluator {

	public List<CashBack> evaluateActions(LinkedHashSet<Map<String, Object>> actions, Map<String, Object> attributes,Map<String, Object> miscAttributes, CachedProperties cachedProperties, Cache cache);
}
