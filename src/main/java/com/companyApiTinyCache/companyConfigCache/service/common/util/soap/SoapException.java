package com.companyApiTinyCache.companyConfigCache.service.common.util.soap;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.*;

@ClassExcludeCoverage
public class SoapException extends Exception {

    private static final long serialVersionUID = 1045334921464101740L;

    public SoapException(Exception cause) {
        super(cause.getMessage(), cause);
    }
}
