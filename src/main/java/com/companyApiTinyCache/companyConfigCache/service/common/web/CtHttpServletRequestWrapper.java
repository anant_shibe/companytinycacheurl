package com.companyApiTinyCache.companyConfigCache.service.common.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.*;

public class CtHttpServletRequestWrapper extends HttpServletRequestWrapper {

    private Set<String> removedParameters;

    private Map<String, String> addedParameters;

    public CtHttpServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    public void removeParameter(String name) {
        if (name == null) {
            throw new NullPointerException();
        }
        if (removedParameters == null) {
            removedParameters = new HashSet<String>();
        }
        removedParameters.add(name);
        if (addedParameters != null) {
            addedParameters.remove(name);
        }
    }

    public void addParameter(String name, String value) {
        if (name == null || value == null) {
            throw new NullPointerException();
        }
        if (addedParameters == null) {
            addedParameters = new HashMap<String, String>();
        }
        addedParameters.put(name, value);
        if (removedParameters != null) {
            removedParameters.remove(name);
        }
    }

    @Override
    public String getParameter(String name) {
        String value = null;
        if (removedParameters == null || !removedParameters.contains(name)) {
            if (addedParameters != null) {
                value = addedParameters.get(name);
            }
            if (value == null) {
                value = super.getParameter(name);
            }
        }

        return value;
    }

    // WARNING: Not an efficient implementation
    @Override
    public Map getParameterMap() {
        Map orgParameterMap = super.getParameterMap();
        Map parameterMap = orgParameterMap;
        if (removedParameters != null || addedParameters != null) {
            parameterMap = new HashMap(orgParameterMap);
            if (addedParameters != null) {
                for (String key : addedParameters.keySet()) {
                    String[] value = new String[1];
                    value[0] = addedParameters.get(key);
                    parameterMap.put(key, value);
                }
            }
            if (removedParameters != null) {
                for (String key : removedParameters) {
                    parameterMap.remove(key);
                }
            }
            parameterMap = Collections.unmodifiableMap(parameterMap);
        }

        return parameterMap;
    }

    // Warning: Not an efficient implementation
    @Override
    public Enumeration getParameterNames() {
        Enumeration orgParameterNames = super.getParameterNames();
        Enumeration parameterNames = orgParameterNames;
        if (removedParameters != null || addedParameters != null) {
            Vector params = new Vector();
            if (addedParameters != null) {
                params.addAll(addedParameters.keySet());
            }
            for (; orgParameterNames.hasMoreElements();) {
                String key = (String) orgParameterNames.nextElement();
                if (removedParameters == null || !removedParameters.contains(key)) {
                    if (addedParameters == null || !addedParameters.containsKey(key)) {
                        params.add(key);
                    }
                }
            }
            parameterNames = params.elements();
        }

        return parameterNames;
    }

    @Override
    public String[] getParameterValues(String name) {
        String[] values = null;
        if (removedParameters == null || !removedParameters.contains(name)) {
            if (addedParameters != null) {
                String value = addedParameters.get(name);
                if (value != null) {
                    values = new String[1];
                    values[0] = value;
                }
            }
            if (values == null) {
                values = super.getParameterValues(name);
            }
        }

        return values;
    }

}
