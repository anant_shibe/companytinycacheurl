package com.companyApiTinyCache.companyConfigCache.service.common.notification;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import org.apache.commons.lang.StringUtils;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 * Implementation for Email Sernder.
 * @author cleartrip
 */
@ClassExcludeCoverage
public class EmailSenderImpl implements EmailSender {

    private Session session = null;
    private Properties props = new Properties();
    private String host = "";
    private String port = "";
    private String user = "";
    private String pwd = "";

    public EmailSenderImpl(String pHost, String pPort, String pUser, String pPasswd) {
        this.host = pHost;
        this.port = pPort;
        this.user = pUser;
        this.pwd = pPasswd;
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        // What is this? Doesn't work if I uncomment below line.
        // props.put("mail.smtp.starttls.enable","true");
        // what is this?
        if (!StringUtils.isBlank(user)) {
            props.put("mail.smtp.auth", "true");
        }

        // props.put("mail.smtp.debug", "true");
        props.put("mail.smtp.socketFactory.port", port);
        // props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "true");
        session = Session.getDefaultInstance(props);
        // session.setDebug(true);
    }

    @Override
    public void sendEmail(String to, String from, String subjectMessage, String bodyMessage, String fromName, String contentType) throws MessagingException {
        MimeMessage mimeMessage = new MimeMessage(session);

        if (contentType != null) {
            mimeMessage.setContent(bodyMessage, contentType);
        } else {
            mimeMessage.setText(bodyMessage);
        }

        mimeMessage.setSubject(subjectMessage);
        if (fromName != null) {
            from = fromName + "<" + from + ">";
        }
        mimeMessage.setFrom(new InternetAddress(from));
        if (to.indexOf(',') >= 0) {
            for (String nextTo : to.split(",")) {
                mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(nextTo));
            }
        } else {
            mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(to));
        }
        mimeMessage.saveChanges();
        send(mimeMessage);
    }

    @Override
    public void sendEmail(String email, String from, String subjectMessage, String bodyMessage) throws MessagingException {
        sendEmail(email, from, subjectMessage, bodyMessage, null, null);
    }

    /**
     * @param message Message
     * @throws MessagingException
     */
    private void send(Message message) throws MessagingException {
        Transport transport = session.getTransport("smtp");
        if (!StringUtils.isBlank(user)) {
            transport.connect(user, pwd);
        } else {
            transport.connect();
        }
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();

    }

    public void sendEmail(Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String from, String subjectMessage, String bodyMessage) throws MessagingException {
        sendEmail(toList, ccList, bccList, from, subjectMessage, bodyMessage, null);
    }

    @Override
    @MethodExcludeCoverage
    public void sendEmail(Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String from, String subjectMessage, String bodyMessage, byte[] file, String fileName,
            String contentType) throws MessagingException {
        Message mimeMessage = new MimeMessage(session);
        mimeMessage.setSubject(subjectMessage);
        mimeMessage.setFrom(new InternetAddress(from));
        if (toList != null) {
            for (String to : toList) {
                mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(to));
            }
        }
        if (ccList != null) {
            for (String cc : ccList) {
                mimeMessage.addRecipient(RecipientType.CC, new InternetAddress(cc));
            }
        }
        if (bccList != null) {
            for (String bcc : bccList) {
                mimeMessage.addRecipient(RecipientType.BCC, new InternetAddress(bcc));
            }
        }

        Multipart multipart = new MimeMultipart();

        MimeBodyPart bodyMessagePart = new MimeBodyPart();
        bodyMessagePart.setText(bodyMessage);
        bodyMessagePart.setContent(bodyMessage, "text/html");

        MimeBodyPart attachFilePart = new MimeBodyPart();
        attachFilePart.setDataHandler(new DataHandler(new ByteArrayDataSource(file, contentType)));
        attachFilePart.setFileName(fileName);
        attachFilePart.setDisposition(Part.ATTACHMENT);

        multipart.addBodyPart(attachFilePart);
        multipart.addBodyPart(bodyMessagePart);

        mimeMessage.setContent(multipart);
        mimeMessage.saveChanges();

        send(mimeMessage);
    }

    @Override
    @MethodExcludeCoverage
    public void sendEmail(Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String from, String subjectMessage, String bodyMessage, byte[][] files,
            List<String> fileNames, List<String> contentTypes) throws MessagingException {
        Message mimeMessage = new MimeMessage(session);
        mimeMessage.setSubject(subjectMessage);
        mimeMessage.setFrom(new InternetAddress(from));
        if (toList != null) {
            for (String to : toList) {
                mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(to));
            }
        }
        if (ccList != null) {
            for (String cc : ccList) {
                mimeMessage.addRecipient(RecipientType.CC, new InternetAddress(cc));
            }
        }
        if (bccList != null) {
            for (String bcc : bccList) {
                mimeMessage.addRecipient(RecipientType.BCC, new InternetAddress(bcc));
            }
        }

        Multipart multipart = new MimeMultipart();

        MimeBodyPart bodyMessagePart = new MimeBodyPart();
        bodyMessagePart.setText(bodyMessage);
        bodyMessagePart.setContent(bodyMessage, "text/html");

        for (int i = 0; i < fileNames.size(); i++) {
            MimeBodyPart attachFilePart = new MimeBodyPart();
            attachFilePart.setDataHandler(new DataHandler(new ByteArrayDataSource(files[i], contentTypes.get(i))));
            attachFilePart.setFileName(fileNames.get(i));
            attachFilePart.setDisposition(Part.ATTACHMENT);
            multipart.addBodyPart(attachFilePart);
        }
        multipart.addBodyPart(bodyMessagePart);

        mimeMessage.setContent(multipart);
        mimeMessage.saveChanges();

        send(mimeMessage);
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the pwd
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * @param pwd the pwd to set
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    /*
     * // Uncomment below to run a test public static void main(String args[]) throws MessagingException { EmailSenderImpl es = new EmailSenderImpl("mailrelay.cleartrip.com", "25", "", "");
     * //es.sendEmail("vijay.parashar@cleartrip.com", "vijay.parashar@cleartrip.com", "Test Java Msg5", "Test Java Msg Body5"); List<String> toList = new ArrayList<String>(); String fileContent=
     * "Hello,from,vijay,Parashar"; byte[] file = fileContent.getBytes(); toList.add("vijay.parashar@cleartrip.com"); toList.add("mohammad.nadeem@cleartrip.com");
     * toList.add("ritesh.thounaojam@cleartrip.com"); toList.add("anirban.roy@cleartrip.com"); toList.add("suresh.mahalingam@cleartrip.com"); es.sendEmail(toList, null, null, "noreply@cleartrip.com",
     * "Test Mail With Attachment", "<html><body><br>Test Mail With Attachment <br> <br><br><br>Regards <br> Cleartrip Hotels Dev Team!!</body></html>", file, "test.csv", "application/csv"); //
     * send("mailrelay1.cleartrip.com", "test_dev.mahalingam@cleartrip.com", "suresh.mahalingam@cleartrip.com", "Test Java Msg", "Test Java Msg Body"); }
     */

    @Override
    public void sendEmail(Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String from, String subjectMessage, String bodyMessage, String contentType)
            throws MessagingException {
        MimeMessage mimeMessage = new MimeMessage(session);

        if (contentType != null) {
            mimeMessage.setContent(bodyMessage, contentType);
        } else {
            mimeMessage.setText(bodyMessage);
        }

        mimeMessage.setSubject(subjectMessage);
        mimeMessage.setFrom(new InternetAddress(from));
        if (toList != null) {
            for (String to : toList) {
                mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(to));
            }
        }
        if (ccList != null) {
            for (String cc : ccList) {
                mimeMessage.addRecipient(RecipientType.CC, new InternetAddress(cc));
            }
        }
        if (bccList != null) {
            for (String bcc : bccList) {
                mimeMessage.addRecipient(RecipientType.BCC, new InternetAddress(bcc));
            }
        }
        mimeMessage.saveChanges();
        send(mimeMessage);
    }
}
