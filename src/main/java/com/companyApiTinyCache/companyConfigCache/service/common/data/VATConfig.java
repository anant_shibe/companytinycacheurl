/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author sumanth
 *
 */
@ClassExcludeCoverage
public class VATConfig implements Serializable {


	private static final long serialVersionUID = 8592419276995193545L;

	private boolean showCheckBox;
	private boolean tickCheckBox;
	private boolean showTrn;
	private boolean showCompanyName;
	private boolean showAddress;
	private boolean itineraryHasVat;
	private String insuranceMessage;
	private String vatRegexPattern;
	private String vatPaymentMessage;
	private double vatPercentage;

	@JsonProperty("show_vat_cb")
	public boolean showCheckBox() {
		return showCheckBox;
	}

	@JsonProperty("show_vat_cb")
	public void setShowCheckBox(boolean showCheckBox) {
		this.showCheckBox = showCheckBox;
	}

	@JsonProperty("tick_vat_cb")
	public boolean tickCheckBox() {
		return tickCheckBox;
	}

	@JsonProperty("tick_vat_cb")
	public void setTickCheckBox(boolean tickCheckBox) {
		this.tickCheckBox = tickCheckBox;
	}

	@JsonProperty("show_trn")
	public boolean showTrn() {
		return showTrn;
	}

	@JsonProperty("show_trn")
	public void setShowTrn(boolean showTrn) {
		this.showTrn = showTrn;
	}

	@JsonProperty("show_company_name")
	public boolean showCompanyName() {
		return showCompanyName;
	}

	@JsonProperty("show_company_name")
	public void setShowCompanyName(boolean showCompanyName) {
		this.showCompanyName = showCompanyName;
	}

	@JsonProperty("show_address")
	public boolean showAddress() {
		return showAddress;
	}

	@JsonProperty("show_address")
	public void setShowAddress(boolean showAddress) {
		this.showAddress = showAddress;
	}

	@JsonProperty("has_vat")
	public boolean itineraryHasVat() {
		return itineraryHasVat;
	}

	@JsonProperty("has_vat")
	public void setItineraryHasVat(boolean itineraryHasVat) {
		this.itineraryHasVat = itineraryHasVat;
	}

	@JsonProperty("insurance_msg")
	public String getInsuranceMessage() {
		return insuranceMessage;
	}

	@JsonProperty("insurance_msg")
	public void setInsuranceMessage(String insuranceMessage) {
		this.insuranceMessage = insuranceMessage;
	}

	@JsonProperty("trn_regex")
	public String getVatPattern() {
		return vatRegexPattern;
	}

	@JsonProperty("trn_regex")
	public void setVatPattern(String vatRegexPattern) {
		this.vatRegexPattern = vatRegexPattern;
	}

	@JsonProperty("vat_payment_msg")
	public String getVatPaymentMessage() {
		return vatPaymentMessage;
	}

	@JsonProperty("vat_payment_msg")
	public void setVatPaymentMessage(String vatPaymentMessage) {
		this.vatPaymentMessage = vatPaymentMessage;
	}

	@JsonProperty("vat_percentage")
	public double getVatPercentage() {
		return vatPercentage;
	}

	@JsonProperty("vat_percentage")
	public void setVatPercentage(double vatPercentage) {
		this.vatPercentage = vatPercentage;
	}
}
