package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class ContactDetail implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private Collection<Email> emails;

    private Collection<WebSite> websites;

    private Collection<Address> addresses;

    private PhoneNumbers phoneNumbers = new PhoneNumbers();

    private Collection<OtherDetail> otherDetails;

    public ContactDetail() {
        otherDetails = new ArrayList<OtherDetail>();
    }

    @JsonIgnore
    private boolean profiledUpdated;

    @JsonIgnore
    public boolean isProfiledUpdated() {
        return profiledUpdated;
    }

    @JsonProperty("addresses")
    public Collection<Address> getAddresses() {
        return addresses;
    }

    @JsonProperty("addresses")
    public void setAddresses(Collection<Address> addresses) {
        if (addresses != null && addresses.size() > 0) {
            profiledUpdated = true;
        }
        this.addresses = addresses;
    }

    @JsonIgnore
    public void setWorkAddress(Address address) {
        boolean typeExists = false;
        if (address != null && StringUtils.isNotEmpty(address.getAddressType()))
            profiledUpdated = true;

        if (this.addresses == null || this.addresses.size() == 0) {
            Collection<Address> addrs = new ArrayList<Address>();
            addrs.add(address);
            this.setAddresses(addrs);
            return;
        }
        for (Address userAddress : this.addresses) {
            if (userAddress.getAddressType().equalsIgnoreCase(address.getAddressType())) {
                this.addresses.remove(userAddress);
                this.addresses.add(address);
                break;
            }
        }
    }

    @JsonProperty("phone_numbers")
    public Vector<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    @JsonProperty("phone_numbers")
    public void setPhoneNumbers(Vector<PhoneNumber> phoneNumbers) {
        if (phoneNumbers.size() > 0)
            profiledUpdated = true;
        if (this.phoneNumbers == null) {
            this.phoneNumbers = (PhoneNumbers) phoneNumbers;
        } else {
            for (PhoneNumber phoneNumber : phoneNumbers) {
                this.phoneNumbers.add(phoneNumber);
            }
        }
    }

    @JsonIgnore
    public void setLandLine(String landLine) {
        if (StringUtils.isNotEmpty(landLine))
            profiledUpdated = true;
        if (!(this.phoneNumbers.typeExists(CommonEnumConstants.PhoneType.LANDLINE))) {
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setType(CommonEnumConstants.PhoneType.LANDLINE.toString());
            phoneNumber.setNumber(landLine);
            this.phoneNumbers.add(phoneNumber);
        }
    }

    @JsonIgnore
    public void setMobile(String mobile) {
        if (StringUtils.isNotEmpty(mobile))
            profiledUpdated = true;
        if (!(this.phoneNumbers.typeExists(CommonEnumConstants.PhoneType.MOBILE))) {
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setType(CommonEnumConstants.PhoneType.MOBILE.toString());
            phoneNumber.setNumber(mobile);
            this.phoneNumbers.add(phoneNumber);
        }
    }

    @JsonIgnore
    public String getPhone(CommonEnumConstants.PhoneType type) {
        if (phoneNumbers != null) {
            for (PhoneNumber phoneNumber : phoneNumbers) {
                if (phoneNumber.getType().equals(type.toString())) {
                    return phoneNumber.getNumber();
                }
            }
        }
        return "";
    }

    @JsonIgnore
    public PhoneNumber getPhoneNumber(CommonEnumConstants.PhoneType type) {
        PhoneNumber tempPhoneNumber = null;
        if (phoneNumbers != null) {
            for (PhoneNumber phoneNumber : phoneNumbers) {
                if (phoneNumber.getType().equals(type.toString())) {
                    tempPhoneNumber = phoneNumber;
                }
            }
        }
        return tempPhoneNumber;
    }

    @JsonIgnore
    public String getLandLine() {
        return getPhone(CommonEnumConstants.PhoneType.LANDLINE);
    }

    @JsonIgnore
    public String getUserHomePhone() {
        if (phoneNumbers != null) {
            for (PhoneNumber phoneNumber : phoneNumbers) {
                if (phoneNumber.getType().equalsIgnoreCase("home")) {
                    return phoneNumber.getNumber();
                }
            }
        }
        return "";
    }

    @JsonIgnore
    public String getMobile() {
        return getPhone(CommonEnumConstants.PhoneType.MOBILE);
    }

    @JsonIgnore
    public String getWorkPhone() {
        return getPhone(CommonEnumConstants.PhoneType.WORK);
    }

    @JsonIgnore
    public String getOtherPhone() {
        return getPhone(CommonEnumConstants.PhoneType.OTHER);
    }

    @JsonIgnore
    public Address getAddress(CommonEnumConstants.AddressType type) {
        if (addresses != null) {
            for (Address address : addresses) {
                if (address.getAddressType().equals(type.toString())) {
                    return address;
                }
            }
        }
        return null;
    }

    @JsonIgnore
    public Address getHomeAddress() {
        return getAddress(CommonEnumConstants.AddressType.HOME);
    }

    @JsonIgnore
    public Address getWorkAddress() {
        return getAddress(CommonEnumConstants.AddressType.WORK);
    }

    @JsonIgnore
    public Address getShippingAddress() {
        return getAddress(CommonEnumConstants.AddressType.SHIPPING);
    }

    @JsonIgnore
    public Address getBillingAddress() {
        return getAddress(CommonEnumConstants.AddressType.BILLING);
    }

    @JsonIgnore
    public Address getOtherAddress() {
        return getAddress(CommonEnumConstants.AddressType.OTHER);
    }

    @JsonProperty("emails")
    public Collection<Email> getEmails() {
        return emails;
    }

    @JsonProperty("emails")
    public void setEmails(Collection<Email> emails) {
        if (emails.size() > 0)
            profiledUpdated = true;
        this.emails = emails;
    }

    @JsonIgnore
    public void setWorkEmail(String newEmail) {
        if (StringUtils.isNotEmpty(newEmail))
            profiledUpdated = true;
        if (this.emails == null || this.emails.size() == 0) {
            Collection<Email> newEmails = new ArrayList<Email>();
            Email workEmail = new Email();
            workEmail.setEmailId(newEmail);
            workEmail.setType(CommonEnumConstants.EmailType.WORK.toString());
            newEmails.add(workEmail);
            this.setEmails(newEmails);
            return;
        }

        for (Email userEmail : this.emails) {
            if (userEmail.getType().equalsIgnoreCase(CommonEnumConstants.EmailType.WORK.toString())) {
                userEmail.setEmailId(newEmail);
                break;
            }
        }
    }

    @JsonIgnore
    public String getEmail(CommonEnumConstants.EmailType type) {
        if (emails != null) {
            for (Email email : emails) {
                if (email.getType().equalsIgnoreCase(type.toString())) {
                    return email.getEmailId();
                }
            }
        }
        return "";
    }

    @JsonIgnore
    public String getWorkEmail() {
        return getEmail(CommonEnumConstants.EmailType.WORK);
    }

    @JsonIgnore
    public String getHomeEmail() {
        return getEmail(CommonEnumConstants.EmailType.HOME);
    }

    @JsonIgnore
    public String getOtherEmail() {
        return getEmail(CommonEnumConstants.EmailType.OTHER);
    }

    @JsonProperty("websites")
    public Collection<WebSite> getWebsites() {
        return websites;
    }

    @JsonProperty("websites")
    public void setWebsites(Collection<WebSite> websites) {
        if (websites.size() > 0)
            profiledUpdated = true;
        this.websites = websites;
    }

    @JsonProperty("other_details")
    public Collection<OtherDetail> getOtherDetails() {
        return otherDetails;
    }

    @JsonProperty("other_details")
    public void setOtherDetails(Collection<OtherDetail> otherDetails) {
        if (otherDetails == null) {
            this.otherDetails = new ArrayList<OtherDetail>();
        }
        if (otherDetails != null && otherDetails.size() > 0) {// at least one
            // otherDetail
            // is filled
            profiledUpdated = true;
        }
        this.otherDetails = otherDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ContactDetail == false)
            return false;
        ContactDetail contactDetail = (ContactDetail) o;
        if ((emails != null || contactDetail.getEmails() != null) && !GenUtil.sameCollections(emails, contactDetail.getEmails(), true)) {
            return false;
        }
        if ((websites != null && contactDetail.getWebsites() != null) && !GenUtil.sameCollections(websites, contactDetail.getWebsites(), true)) {
            return false;
        }
        if ((addresses != null && contactDetail.getAddresses() != null) && !GenUtil.sameCollections(addresses, contactDetail.getAddresses(), true)) {
            return false;
        }
        if ((phoneNumbers != null && contactDetail.getPhoneNumbers() != null) && !GenUtil.sameCollections(phoneNumbers, contactDetail.getPhoneNumbers(), true)) {
            return false;
        }
        if ((otherDetails != null && contactDetail.getOtherDetails() != null) && !GenUtil.sameCollections(otherDetails, contactDetail.getOtherDetails(), true)) {
            return false;
        }

        return true;
    }

}
