package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.PassengerType;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.PassengerTypeShort;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

public class Passenger implements Serializable {
    private static final long serialVersionUID = 1L;
    private int peopleId; // people id from accounts schema
    private int userId; // affiliate user id from wl schema
    private String title;
    private String firstName;
    private String lastName;
    private String type;
    private String typeDisplay;
    private String shortType;
    private Date dob;
    private String dobStr;
    private int age;
    private String passportNumber;
    private Date passportExpiryDate;
    private String mealPref;
    private String gender;

    private Collection<FrequentFlyerNumber> freqFlyerNumbers;

    public String getTitle() {
        return this.title;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the shortType
     */
    public String getShortType() {
        String paxType = "";
        if (this.type.equalsIgnoreCase("ADULT")) {
            paxType = "ADT";
        } else if (this.type.equalsIgnoreCase("CHILD")) {
            paxType = "CHD";
        } else if (this.type.equalsIgnoreCase("INFANT")) {
            paxType = "INF";
        } else if (this.type.equalsIgnoreCase("MALE_SENIOR")) {
            paxType = "SCM";
        } else if (this.type.equalsIgnoreCase("FEMALE_SENIOR")) {
            paxType = "SCF";
        }

        return paxType;
    }

    /**
     * @return the shortType
     */
    public String getProperType() {
        String paxType = "";
        if (this.type.equalsIgnoreCase("ADULT")) {
            paxType = PassengerType.ADULT.toProperCase();
        } else if (this.type.equalsIgnoreCase("CHILD")) {
            paxType = PassengerType.CHILD.toProperCase();
        } else if (this.type.equalsIgnoreCase("INFANT")) {
            paxType = PassengerType.INFANT.toProperCase();
        } else if (this.type.equalsIgnoreCase("MALE_SENIOR")) {
            paxType = "Senior";
        } else if (this.type.equalsIgnoreCase("FEMALE_SENIOR")) {
            paxType = "Senior";
        }

        return paxType;
    }

    public String getType() {
        return this.type;
    }

    public void setType(PassengerTypeShort type) {
        if (type.name().equalsIgnoreCase("ADT")) {
            this.type = PassengerType.ADULT.toString();
        } else if (type.name().equalsIgnoreCase("CHD")) {
            this.type = PassengerType.CHILD.toString();
        } else if (type.name().equalsIgnoreCase("INF")) {
            this.type = PassengerType.INFANT.toString();
        } else if ((type.name().equalsIgnoreCase("SCM"))) {
            this.type = PassengerType.MALE_SENIOR.toString();
        } else if ((type.name().equalsIgnoreCase("SCF"))) {
            this.type = PassengerType.FEMALE_SENIOR.toString();
        }
    }

    public void setType(String type) {
        // this.type=type;
        if ((type.equalsIgnoreCase("ADT") || type.equalsIgnoreCase("ADULT"))) {
            this.type = PassengerType.ADULT.toProperCase();
        } else if ((type.equalsIgnoreCase("CHD") || type.equalsIgnoreCase("CHILD"))) {
            this.type = PassengerType.CHILD.toProperCase();
        } else if ((type.equalsIgnoreCase("INF") || type.equalsIgnoreCase("INFANT"))) {
            this.type = PassengerType.INFANT.toProperCase();
        } else if ((type.equalsIgnoreCase("SCM") || type.equalsIgnoreCase("MALE_SENIOR"))) {
            this.type = PassengerType.MALE_SENIOR.toProperCase();
        } else if ((type.equalsIgnoreCase("SCF") || type.equalsIgnoreCase("FEMALE_SENIOR"))) {
            this.type = PassengerType.FEMALE_SENIOR.toProperCase();
        }
    }

    /**
     * @return the peopleId
     */
    public int getPeopleId() {
        return peopleId;
    }

    /**
     * @param peopleId
     *            the peopleId to set
     */
    public void setPeopleId(int peopleId) {
        this.peopleId = peopleId;
    }

    /**
     * @return the dob
     */
    public Date getDob() {
        return dob;
    }

    /**
     * @param dob
     *            the dob to set
     */
    public void setDob(Date dob) {
        this.dob = dob;
    }

    /**
     * @return the dobStr
     */
    public String getDobStr() {
        if (dob != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.format(this.dob);
        } else {
            return "yyyy-MM-dd";
        }
    }

    /**
     * @param age
     *            the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        if (age > 0)
            return age;

        if (dob != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dob);
            return GenUtil.getAge(cal.get(Calendar.DATE), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
        } else {
            return 0;
        }
    }

    /**
     * @return the personId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param personId
     *            the personId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the typeDisplay
     */
    public String getTypeDisplay() {
        return type.substring(0, 1).toUpperCase() + type.substring(1);
    }

    /**
     * @return the passportNumber
     */
    public String getPassportNumber() {
        return passportNumber;
    }

    /**
     * @param passportNumber
     *            the passportNumber to set
     */
    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    /**
     * @return the passportExpiryDate
     */
    public Date getPassportExpiryDate() {
        return passportExpiryDate;
    }

    /**
     * @param passportExpiryDate
     *            the passportExpiryDate to set
     */
    public void setPassportExpiryDate(Date passportExpiryDate) {
        this.passportExpiryDate = passportExpiryDate;
    }

    /**
     * @return the mealPref
     */
    public String getMealPref() {
        return mealPref;
    }

    /**
     * @param mealPref
     *            the mealPref to set
     */
    public void setMealPref(String mealPref) {
        this.mealPref = mealPref;
    }

    /**
     * @return the freqFlyerNumbers
     */
    public Collection<FrequentFlyerNumber> getFreqFlyerNumbers() {
        return freqFlyerNumbers;
    }

    /**
     * @param freqFlyerNumbers
     *            the freqFlyerNumbers to set
     */
    public void setFreqFlyerNumbers(Collection<FrequentFlyerNumber> freqFlyerNumbers) {
        this.freqFlyerNumbers = freqFlyerNumbers;
    }

    public double getTotalBaseFare() {
        return 0;
    }

    public double getTotalTaxes() {
        return 0;
    }

    public double getTotalDiscount() {
        return 0;
    }

    public double getTotalMarkup() {
        return 0;
    }

    public double getTotalFees() {
        return 0;
    }

    public double getTotalCashBack() {
        return 0;
    }

    public double getTotalFare() {
        return 0;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        if (StringUtils.isNotEmpty(gender))
            return gender;

        if (title.equals("Mr") || title.equals("Mstr"))
            return "Male";

        if (title.equals("Ms") || title.equals("Miss") || title.equals("Mrs"))
            return "Female";

        return gender;
    }

    /**
     * @param gender
     *            the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
}
