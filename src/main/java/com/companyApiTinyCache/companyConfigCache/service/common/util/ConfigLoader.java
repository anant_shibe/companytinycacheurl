package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.util.Map;

/**
 * Loader class loading a config for use with CachedPropertiesManager.
 *
 * @author suresh
 *
 * @param <C> Config Class.
 */
public interface ConfigLoader<C> {
    C loadConfig(Map<String, String> cachedPropertiesResource);
}
