package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.impl.JedisRedisCacheImpl;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Concrete implementation of CacheManager for MemcachedCacheImpl.
 *
 * @author sdesai
 */
public class CacheFactory implements DisposableBean {

    static enum CacheType {
        REDIS, MULTISPY_MEMCACHE
    }

    private static class CacheInfo {
        private Cache cache;

        private int refCount;

        @Override
        protected void finalize() {
            if (cache != null) {
                try {
                    cache.destroy();
                } catch (Exception e) {
                    logger.error("(INFO Only) Error closing unused cache " + cache.getConnectionString());
                }
            }
        }
    }

    private static final Log logger = LogFactory.getLog(CacheFactory.class);

    private volatile Map<String, ShardedCache []> cacheServerMap = Collections.EMPTY_MAP;

    private volatile Map<String, Cache> cacheMap = Collections.EMPTY_MAP;

    private CachedProperties commonCachedProperties;

    // Used to store Cache for each address so that the same cache is re-used when multiple properties point to the same server.
    // Used for the New Redis Cache.
    private Map<String, CacheInfo> addressCacheMap = new HashMap<String, CacheInfo>();

    private boolean isRedisServer(String serversProperty, String servers) {
        return serversProperty.contains(".redis.") || servers != null && (servers.indexOf("redis") >= 0 || servers.indexOf("6379") >= 0 || servers.startsWith("rd:") || (servers.indexOf("@") >= 0));
    }

    private String normalizeServers(String servers, int defaultPort) {
        if (servers.startsWith("rd:")) {
            servers = servers.substring(3);
        }

        StringBuilder sb = new StringBuilder();
        String [] shards = servers.split(",");
        for (String shard : shards) {
            sb.append(shard);
            if (shard.indexOf(':') < 0) {
                sb.append(':').append(defaultPort);
            }
            sb.append(',');
        }
        sb.setLength(sb.length() - 1);

        return sb.toString();
    }

    private String [] getNormalizedServers(String serversProperty, String servers, CacheType [] retCacheType) {
        CacheType cacheType = CacheType.MULTISPY_MEMCACHE;
        if (isRedisServer(serversProperty, servers)) {
            cacheType = CacheType.REDIS;
        }
        if (retCacheType != null) {
            retCacheType[0] = cacheType;
        }
        int port = 11211;
        if (cacheType == CacheType.REDIS) {
            port = 6379;
        }
        servers = normalizeServers(servers, port);
        String [] serversArr = servers.split(",");
        return serversArr;
    }

    private Cache createCache(String serversProperty, String server, CacheType cacheType) {
        Cache cache = null;
        if (cacheType == CacheType.REDIS) {
            cache = new JedisRedisCacheImpl(server, commonCachedProperties);
        } else {
            cache = new MultiSpyMemcachedCacheImpl(commonCachedProperties, serversProperty, server);
        }
        return cache;
    }

    private synchronized ShardedCache updateCache(String serversProperty, boolean propagateErrors) {
        int i, cacheIndex;
        cacheIndex = 0;
        if (propagateErrors) {
            cacheIndex = 1;
        }
        Map<String, ShardedCache []> cm = cacheServerMap;
        ShardedCache shardedCache = null;
        ShardedCache [] shardedCaches = cm.get(serversProperty);
        if (shardedCaches != null) {
            shardedCache = shardedCaches[cacheIndex];
        }
        Map<String, String> oldCachedPropertiesResource = null;
        String oldConnectionString = null;
        if (shardedCache != null) {
            oldCachedPropertiesResource = shardedCache.getCachedPropertiesResource();
            oldConnectionString = CachedProperties.getPropertyValue(oldCachedPropertiesResource, serversProperty, null);
        }
        Map<String, String> cachedPropertiesResource = commonCachedProperties.getResource();
        String connectionString = CachedProperties.getPropertyValue(cachedPropertiesResource, serversProperty, null);
        if (!connectionString.equals(oldConnectionString)) {
            CacheType [] cacheTypeHolder = new CacheType[1];
            String [] hostNames = getNormalizedServers(serversProperty, connectionString, cacheTypeHolder);
            Cache [] caches = new Cache[hostNames.length];
            CacheType cacheType = cacheTypeHolder[0];
            for (i = 0; i < hostNames.length; i++) {
                String host = hostNames[i];
                CacheInfo cacheInfo = addressCacheMap.get(host);
                if (cacheInfo == null) {
                    cacheInfo = new CacheInfo();
                    cacheInfo.cache = createCache(serversProperty, host, cacheType);
                    addressCacheMap.put(host, cacheInfo);
                }
                cacheInfo.refCount++;
                caches[i] = cacheInfo.cache;
            }
            shardedCache = new ShardedCache(cachedPropertiesResource, connectionString, caches, propagateErrors);
            cm = new HashMap<String, ShardedCache []>(cm);
            if (shardedCaches == null) {
                shardedCaches = new ShardedCache[2];
            } else {
                shardedCaches = Arrays.copyOf(shardedCaches, shardedCaches.length);
            }
            shardedCaches[cacheIndex] = shardedCache;
            cm.put(serversProperty, shardedCaches);
            Map<String, Cache> newCacheMap = new HashMap<String, Cache>();
            for (String prop : cm.keySet()) {
                ShardedCache[] scs = cm.get(prop);
                ShardedCache sc = scs[1];
                if (sc == null) {
                    sc = scs[0];
                }
                if (sc != null) {
                    newCacheMap.put(prop, sc);
                }
            }
            newCacheMap = Collections.unmodifiableMap(newCacheMap);
            cacheServerMap = cm;
            cacheMap = newCacheMap;
            if (oldConnectionString != null) {
                hostNames = getNormalizedServers(serversProperty, oldConnectionString, null);
                for (i = 0; i < hostNames.length; i++) {
                    String host = hostNames[i];
                    CacheInfo cacheInfo = addressCacheMap.get(host);
                    if (cacheInfo != null) {
                        if (--cacheInfo.refCount == 0) {
                            addressCacheMap.remove(host);
                        }
                    } else {
                        logger.error("UNEXPECTED: Old host: " + host + " not found in Address Cache Map when changing cache Server for property " + serversProperty + " from " + oldConnectionString
                                     + " to " + connectionString);
                    }
                }
            }
        }

        return shardedCache;
    }

    /**
     * {@inheritDoc}
     *
     * @return the created cache
     */
    public Cache getCacheForServer(String serversProperty, boolean propagateErrors) {
        Map<String, ShardedCache []> cm = cacheServerMap;
        int cacheIndex = 0;
        if (propagateErrors) {
            cacheIndex = 1;
        }
        ShardedCache cache = null;
        ShardedCache [] caches = cm.get(serversProperty);
        if (caches != null) {
            cache = caches[cacheIndex];
        }

        if (cache == null || cache.getCachedPropertiesResource() != commonCachedProperties.getResource()) {
            cache = updateCache(serversProperty, propagateErrors);
        }

        return cache;
    }

    public Cache getCacheForServer(String serversProperty) {
        return getCacheForServer(serversProperty, false);
    }

    /**
     * {@inheritDoc}
     *
     * @see DisposableBean#destroy()
     */
    @Override
    public synchronized void destroy() throws Exception {
        for (CacheInfo cacheInfo : addressCacheMap.values()) {
            cacheInfo.cache.destroy();
        }
    }

    /**
     * Setter for commonCachedProperties.
     *
     * @param pcommonCachedProperties
     *            the commonCachedProperties to set
     */
    public void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
    }

    public Map<String, Cache> getCacheMap() {
        return cacheMap;
    }

}
