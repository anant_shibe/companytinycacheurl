package com.companyApiTinyCache.companyConfigCache.service.common.org.springframework.web.servlet.mvc;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//TODO::Sathya remove this class
public abstract class SimpleFormController extends AbstractController {

    public String successView;

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        return null;
    }

    protected boolean isFormSubmission(HttpServletRequest request) {
        return false;
    }

    protected ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        return null;
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception{
        return null;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return onSubmit(request, response, null, null);
    }

    public void setSuccessView(String successView) {
        this.successView = successView;
    }

    public final String getSuccessView() {
        return successView;
    }
}
