package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.data.JSONSizeCheckerConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.notification.NotificationMediator;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.DefaultRestClientImpl;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestRequest;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestResponse;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@ClassExcludeCoverage
public class JSONSizeCheckerService implements JSONSizeCheckerConstants {
	private NotificationMediator notificationMediator;
	private CachedProperties commonCachedProperties;
	static Logger LOGGER = Logger.getLogger(JSONSizeCheckerService.class.getName());
	public static final String MAIL_SUBJECT = "Size Limit Exceeded Jsons";

	public JSONSizeCheckerService(CachedProperties commonCachedProperties, NotificationMediator notificationMediator) {
		this.commonCachedProperties = commonCachedProperties;
		this.notificationMediator = notificationMediator;
	}

	public Double calculateResponseSize(String apiUrl, int timesRetry) {

		int len = 0;
		RestRequest restRequest = new RestRequest(apiUrl.trim());
		RestResponse restResponse = null;

		for (int i = 0; i <= timesRetry; i++) {
			try {
				restResponse = DefaultRestClientImpl.getInstance().get(restRequest);
				String content = restResponse.getContent();
				len = content.getBytes().length;
			} catch (Exception e) {
				LOGGER.error("Error in calling endpoint: " + apiUrl);
			}

			if (restResponse != null && restResponse.getStatus() == 200) {
				break;
			}
		}

		return (double) len / (1024 * 1024);
	}

	public String createMessageBody(Map<String, Map<String, Double>> completeList) {
		String exceededListBody = "";
		String errorBody = "";

		for (String typeOfList : completeList.keySet()) {
			Map<String, Double> map = completeList.get(typeOfList);
			Double value = 0.0;
			if (typeOfList.equals("Exceeded List")) {
				for (String key : map.keySet()) {
					value = map.get(key);
					exceededListBody += key + ": " + value.toString() + "\n";
				}
			} else
				for (String key : map.keySet())
					errorBody += key + "\n";
		}

		String messageBody = "";
		if (!exceededListBody.isEmpty())
			messageBody += "\n"+exceededListBody;
		if (!errorBody.isEmpty())
			messageBody += "\nThere was an error in calling the following endpoints:\n" + errorBody;
		LOGGER.debug("MESSAGE BODY--------\n" + messageBody);
		return messageBody;
	}

	public Map<String, Map<String, Double>> createCompleteList() {

		int maxSize = commonCachedProperties.getIntPropertyValue(JSON_MAX_SIZE, 0);
		int timesRetry = commonCachedProperties.getIntPropertyValue(JSON_RETRY_TIMES, 0);

		LOGGER.debug("max size--------\n" + maxSize);
		LOGGER.debug("max size--------\n" + commonCachedProperties.getPropertyValue(JSON_ENDPOINTS));
		ArrayList<String> endpoints = (ArrayList<String>) APIUtil.deserializeJsonToObject(
				commonCachedProperties.getPropertyValue(JSON_ENDPOINTS), new ArrayList<String>());

		Map<String, Map<String, Double>> completeList;

		Map<String, Double> exceededList, errorList;
		
		if(maxSize >= 0)
		{
			completeList = new HashMap<String, Map<String, Double>>();
			exceededList = new HashMap<String, Double>();
			errorList = new HashMap<String, Double>();
			
			Double jsonSize = 0.0;
			
			for (String apiUrl : endpoints) {
				jsonSize = calculateResponseSize(apiUrl, timesRetry);
				if (jsonSize > maxSize) {
					LOGGER.debug(apiUrl + ": " + jsonSize);
					exceededList.put(apiUrl, jsonSize);
				}
	
				if (jsonSize == 0)
					errorList.put(apiUrl, jsonSize);
			}
	
			completeList.put("Exceeded List", exceededList);
			completeList.put("Error List", errorList);
	
			LOGGER.debug("COMPLETE LIST---------\n" + completeList);
		}
		else
			completeList = null;
		
		sendMail(completeList);

		return completeList;
	}

	public void sendMail(Map<String, Map<String, Double>> completeList) {
		if (notificationMediator == null)
			LOGGER.error("notification mediator is null");

		String emailAddress = commonCachedProperties.getPropertyValue(JSON_MAIL_RECIPIENTS);
		LOGGER.debug("recipients--------\n" + emailAddress);
		
		String message;
		
		if(completeList == null)
			message = "Maximum allowed size cannot be negative. Please check.";
		else
			message = createMessageBody(completeList);
		
		if (message.isEmpty())
			LOGGER.error("There is no exceeded JSON List");
		else {
			StringBuilder bodyMessage = new StringBuilder("Exceeded list:\n");
			bodyMessage.append(message);

			try {
				notificationMediator.transmitNotification(MAIL_SUBJECT, bodyMessage.toString(), emailAddress,
						"noreply@cleartrip.com", new HashMap<String, String>(1), new HashMap<String, String>(1));

			} catch (Exception e) {
				LOGGER.error(String.format("Error during sending an email notification to mail recepients : %s  ",
						emailAddress), e);
			}
		}
	}

}
