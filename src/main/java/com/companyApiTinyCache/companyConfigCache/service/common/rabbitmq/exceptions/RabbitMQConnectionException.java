package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

@ClassExcludeCoverage
public class RabbitMQConnectionException extends Exception {

	private static final long serialVersionUID = 1L;
	public RabbitMQConnectionException() {
		super();
	}
	public RabbitMQConnectionException(String message) {
		super(message);
	}

	public RabbitMQConnectionException(String messagge, Exception exception) {
		super(messagge, exception);
	}

	public RabbitMQConnectionException(Exception exception) {
		super(exception);
	}
    public RabbitMQConnectionException(Throwable cause) {
    	super(cause);
    }
	

}