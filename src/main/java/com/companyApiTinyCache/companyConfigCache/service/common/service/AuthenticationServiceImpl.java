package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.connector.CompanyAPIConnector;
import com.companyApiTinyCache.companyConfigCache.service.common.connector.UserAPIConnector;
import com.companyApiTinyCache.companyConfigCache.service.common.data.*;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants.UserAPIConnectorTimeouts;
import com.companyApiTinyCache.companyConfigCache.service.common.data.holder.UserProfileHolder;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.ConnectorStats;
import com.companyApiTinyCache.companyConfigCache.service.common.security.CTSecurityHeaders;
import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.ApiType;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CommonUrlResources;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Class providing user login and registration related services.
 * @author suresh
 */
public class AuthenticationServiceImpl implements AuthenticationService {

    // public static final float RAN_SEC = 2208988800;
    private static final Log logger = LogFactory.getLog(AuthenticationServiceImpl.class);

    private static final int SECONDS_IN_A_DAY = 3600 * 24 * 365;

    private static final String B2B_AUTH_COOKIE_NAME = "ct.services.authentication.b2b.auth-cookie.name";

    private boolean proxyEnable = false;
    private String proxyAddr;
    private String proxyPort;
    private String proxyAuthUsername;
    private String proxyAuthPassword;

    private CachedProperties commonCachedProperties;
    private UserAPIConnector userAPIConnector;
    private CompanyAPIConnector companyAPIConnector;
    private CachedUrlResources<CommonUrlResources> cachedUrlResources;
    private Random random;

    /**
     * This method sets the proxy and authenticates the same.
     */
    public void setProxy() {

        try {
            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(proxyAuthUsername, proxyAuthPassword.toCharArray());
                }
            });

            Properties systemSettings = System.getProperties();
            systemSettings.put("proxySet", "true");
            systemSettings.put("http.proxyHost", proxyAddr);
            systemSettings.put("http.proxyPort", proxyPort);
            System.setProperties(systemSettings);
        } catch (Exception e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
    }

    private static final byte[] defaultAuthKey = {65, (byte) 128, 127, (byte) 226, 30, (byte) 252, (byte) 136, (byte) 224, (byte) 135, (byte) 178, (byte) 142, (byte) 208, 104, (byte) 183, (byte) 183,
            23, (byte) 182, 11, 104, 102, 86, 107, 21, (byte) 240, (byte) 182, (byte) 242, (byte) 146, 107, 73, 11, 11, 3};

    /**
     * Gets the acubekey from the Acube server.
     * @return byte[]
     * @throws IOException On IO Exception
     */
    public byte[] getAcubeKey() throws IOException {

        int i;
        long start = System.currentTimeMillis();
        if (proxyEnable) {
            setProxy();
        }

        byte[] authKeyArr = new byte[32];

        String key = null;
        UrlResources<CommonUrlResources, String> urlResources = cachedUrlResources.getResource();

        if (urlResources != null) {
            EnumMap<CommonUrlResources, String> cachedUrlData = urlResources.getUrlData();
            key = cachedUrlData.get(CommonUrlResources.ACUBE_SERVER_KEY);
        }

        if (!StringUtils.isBlank(key)) {
            String newKey = removeChar(key, '|').trim();
            String[] tokens = null;
            tokens = newKey.split(" ", -1);
            for (i = 0; i < tokens.length; i++) {
                authKeyArr[i] = (byte) Integer.parseInt(tokens[i]);
            }
        } else {
            for (i = 0; i < defaultAuthKey.length; i++) {
                authKeyArr[i] = defaultAuthKey[i];
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Get Auth Key Time = " + (System.currentTimeMillis() - start));
        }
        return authKeyArr;
    }

    /**
     * This method takes in a string and removes the specified delimiter from the string.
     * @param s String
     * @param c Char - delimiter
     * @return String
     */
    public static String removeChar(String s, char c) {
        String r = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c) {
                r += s.charAt(i);
            }
        }
        return r;
    }

    /**
     * Creates the checksum from the given decrypted cookie by using SHA-1 hashing technique and returns the hex format.
     * @param decryptedCookie decrypted Auth Cookie
     * @return Checksum
     * @throws NoSuchAlgorithmException on invalid digest algo
     * @throws UnsupportedEncodingException on invalid encoding
     */
    public String getChecksum(String decryptedCookie) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        String tempStr;
        String hashStr = "";
        String[] splitCookie = decryptedCookie.split("\\|", -1);
        logger.debug("DecryptedCookie from getChecksum" + decryptedCookie);

        for (int i = 0; i < splitCookie.length; i++) {
            tempStr = splitCookie[i];
            if (i < splitCookie.length - 2) {
                hashStr += tempStr + "|";
            }
            if (i == splitCookie.length - 2) {
                hashStr += tempStr;
            }
        }
        byte[] hashedArr = new byte[32];
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(hashStr.getBytes("UTF-8"));

        hashedArr = md.digest();

        return GenUtil.asHex(hashedArr);

    }

    /**
     * This method gets the user details from the decrypted cookiee.
     * @param authCookie Auth Cookie
     * @return Map of Person Details
     */
    public HashMap<String, String> getPersonFromCookie(Cookie authCookie) {
        HashMap<String, String> user = new HashMap<String, String>();

        try {
            String cookie = authCookie.getValue();
            byte[] authKey = getAcubeKey();
            String decryptedCookie = SecurityUtil.aesDecrypt(URLDecoder.decode(cookie, "UTF-8"), authKey);
            String checksum = getChecksum(decryptedCookie);

            String[] splitCookie = decryptedCookie.split("\\|", -1);

            if (splitCookie[splitCookie.length - 1].trim().equals(checksum.toString().trim())) {
                user.put(CommonEnumConstants.UserDetails.USER_ID.toString(), splitCookie[3]);
                user.put(CommonEnumConstants.UserDetails.USER_NAME.toString(), splitCookie[4]);
                user.put(CommonEnumConstants.UserDetails.SCREEN_NAME.toString(), splitCookie[5]);
                user.put(CommonEnumConstants.UserDetails.USER_ROLES.toString(), splitCookie[6]);
                user.put(CommonEnumConstants.UserDetails.ACCESS_ROLES.toString(), splitCookie[9]);
            }
            logger.debug("AuthCookie contents  SplitCookie : USER_ID : " + splitCookie[3] + " USER_NAME : " + splitCookie[4] + " SCREEN_NAME : " + splitCookie[5]);
        } catch (Exception e) {
            logger.error("Exception while decrypting the cookie. Exception is:" + e.getMessage());
        }

        return user;
    }

    public String getScreenName(HashMap<String, String> user) {
        String screenName = user.get(CommonEnumConstants.UserDetails.SCREEN_NAME.toString());
        String userName = user.get(CommonEnumConstants.UserDetails.USER_NAME.toString());
        if (StringUtils.isNotEmpty(screenName)) {
            return screenName;
        } else {
            return userName;
        }
    }

    public String getUserName(HashMap<String, String> user) {
        return user.get(CommonEnumConstants.UserDetails.USER_NAME.toString());
    }

    /**
     * This method modifies the authcookie, by updating the last accessed timestamp, checksum, and the cookie expiry timestamp.
     * @param authCookie Cookie
     * @param domain Cookie Domain or null for default
     * @return Cookie
     */
    public Cookie modifyCookie(Cookie authCookie, String domain) {

        String cookie = authCookie.getValue();

        long now = System.currentTimeMillis();
        StringBuilder modifiedCookieValue = new StringBuilder();
        try {
            byte[] authKey = getAcubeKey();
            String decryptedCookie = SecurityUtil.aesDecrypt(URLDecoder.decode(cookie, "UTF-8"), authKey);
            logger.debug("DecryptedCookie from modifyCookie(): " + decryptedCookie);

            String[] splitCookie = decryptedCookie.split("\\|", -1);
            long changeTime = now / 1000;
            splitCookie[2] = String.valueOf(changeTime);

            for (int i = 0; i < splitCookie.length; i++) {
                modifiedCookieValue.append(splitCookie[i]).append("|");
            }

            int expiry = -1;
            if (splitCookie[7].equals("t")) {
                expiry = getAuthCookieAge();
            }

            modifiedCookieValue.deleteCharAt(modifiedCookieValue.length() - 1);

            String checksum = getChecksum(modifiedCookieValue.toString());
            splitCookie = modifiedCookieValue.toString().split("\\|", -1);
            splitCookie[splitCookie.length - 1] = checksum;

            modifiedCookieValue = new StringBuilder();
            for (int i = 0; i < splitCookie.length; i++) {
                modifiedCookieValue.append(splitCookie[i]).append("|");
            }
            modifiedCookieValue.deleteCharAt(modifiedCookieValue.length() - 1);

            String cookieValue = URLEncoder.encode(SecurityUtil.aesEncrypt(modifiedCookieValue.toString(), authKey), "UTF-8");

            authCookie.setValue(cookieValue);
            authCookie.setMaxAge(expiry);
            if (expiry >= 0) {
                authCookie.setMaxAge(authCookie.getMaxAge());
            }

            // sets the domain to cleartrip.com. This will make sure, that cookie works for all cleartrip domain sites.
            if (domain != null) {
                authCookie.setDomain(domain);
            }

            authCookie.setSecure(true);
            // need to set the path, otherwise it signout won't be recognized.
            authCookie.setPath("/");

        } catch (Exception e) {
            authCookie.setSecure(true);
            authCookie.setMaxAge(0);
            authCookie.setPath("/");
            logger.error("Exception is:" + e.getMessage(), e);
        }

        return authCookie;
    }

    @Override
    public boolean isUserAuthenticated(Map<String, String> person, List<AccessRoleDetail> allAccessRoles, String currAction) {

        boolean authorizedUser = true;

        try {
            String userRoles = (String) person.get(CommonEnumConstants.UserDetails.USER_ROLES.toString());
            if (userRoles != null && !userRoles.isEmpty()) {
                String[] loggedInUserRoles = userRoles.split(",", -1);

                // Find if the currAction present in the list of accessRolesMap and return the accessRoleId.
                long accessRoleId = getAccessRoleId(allAccessRoles, currAction);
                if (accessRoleId != -1) {
                    for (int i = 0; i < loggedInUserRoles.length; i++) {
                        if (loggedInUserRoles[i].equals(String.valueOf(accessRoleId))) {
                            authorizedUser = true;
                            break;
                        } else {
                            authorizedUser = false;
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        return authorizedUser;
    }

    /**
     * This method gets the accessRoleId for the given accessRole in the list of accessRoleDetail.
     * @param accessRolesMap List<AccessRoleDetail>
     * @param currAccessRole String
     * @return accessRoleId long
     */
    private long getAccessRoleId(List<AccessRoleDetail> accessRolesMap, String currAccessRole) {

        long accessRoleId = -1;
        if (accessRolesMap != null && !(accessRolesMap.isEmpty())) {
            for (AccessRoleDetail accessRole : accessRolesMap) {
                if (currAccessRole.indexOf(accessRole.getAccess_role()) != -1) {
                    accessRoleId = accessRole.getId();
                    break;
                }
            }
        }

        return accessRoleId;
    }

    /**
     * Set new cookie for the new user who is loggin in for the first time.
     * @param userProfile User Profile
     * @param accessRoles User Access Roles
     * @param domainName Domain Name
     * @param companyId Company Id
     * @return Auth cookie corresponding to params
     * @throws Exception On Exception
     */
    public Cookie generateCookie(UserProfile userProfile, Collection<Integer> accessRoles, String domainName, int companyId) throws Exception {
        String[] cookieContents = new String[10];
        cookieContents[0] = GenUtil.generateRandomAlphaNumeric(random);
        Date todate = new Date();

        cookieContents[1] = String.valueOf(todate.getTime() / 1000);
        cookieContents[2] = String.valueOf(todate.getTime() / 1000);
        cookieContents[3] = String.valueOf(userProfile.getPeopleId());
        cookieContents[4] = userProfile.getUsername();
        cookieContents[5] = getScreenName(userProfile);

        StringBuilder roles = new StringBuilder();
        if (accessRoles != null && !accessRoles.isEmpty()) {
            for (Integer accessRole : accessRoles) {

                roles.append(accessRole);
                roles.append(",");
            }
            if (roles.length() > 0) {
                roles.deleteCharAt(roles.length() - 1);
            }
        }
        cookieContents[6] = roles.toString();
        cookieContents[7] = "t";

        cookieContents[8] = String.valueOf(companyId);
        cookieContents[9] = getUserRoles(userProfile, companyId);

        StringBuilder cookieValue = new StringBuilder();
        for (String cookieContent : cookieContents) {
            cookieValue.append(cookieContent);
            cookieValue.append("|");
        }
        try {
            cookieValue.append(getChecksum(cookieValue.toString()));
        } catch (NoSuchAlgorithmException e) {
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        byte[] authKey = getAcubeKey();
        String cookieString = URLEncoder.encode(SecurityUtil.aesEncrypt(cookieValue.toString(), authKey), "UTF-8");
        Cookie cookie = new Cookie(getAuthCookieName(), cookieString);
        if (null != domainName) {
            cookie.setDomain(domainName);
        }
        cookie.setPath("/");
        return cookie;
    }

    public Cookie generateCookie(UserProfile userProfile, Collection<Integer> accessRoles, String domainName, int companyId, boolean superUserSignin, int defaultCompanyId) throws Exception {
        String[] cookieContents = new String[10];
        cookieContents[0] = GenUtil.generateRandomAlphaNumeric(random);
        Date todate = new Date();

        cookieContents[1] = String.valueOf(todate.getTime() / 1000);
        cookieContents[2] = String.valueOf(todate.getTime() / 1000);
        cookieContents[3] = String.valueOf(userProfile.getPeopleId());
        cookieContents[4] = userProfile.getUsername();
        cookieContents[5] = getScreenName(userProfile);

        StringBuilder roles = new StringBuilder();
        if (accessRoles != null && !accessRoles.isEmpty()) {
            for (Integer accessRole : accessRoles) {

                roles.append(accessRole);
                roles.append(",");
            }
            if (roles.length() > 0) {
                roles.deleteCharAt(roles.length() - 1);
            }
        }
        cookieContents[6] = roles.toString();
        cookieContents[7] = "t";

        cookieContents[8] = String.valueOf(companyId);

        if (!superUserSignin) {
            cookieContents[9] = getUserRoles(userProfile, companyId);
        } else {
            cookieContents[9] = getUserRoles(userProfile, defaultCompanyId);
        }

        StringBuilder cookieValue = new StringBuilder();
        for (String cookieContent : cookieContents) {
            cookieValue.append(cookieContent);
            cookieValue.append("|");
        }
        try {
            cookieValue.append(getChecksum(cookieValue.toString()));
        } catch (NoSuchAlgorithmException e) {
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        byte[] authKey = getAcubeKey();
        String cookieString = URLEncoder.encode(SecurityUtil.aesEncrypt(cookieValue.toString(), authKey), "UTF-8");
        Cookie cookie = new Cookie(getAuthCookieName(), cookieString);
        if (null != domainName) {
            cookie.setDomain(domainName);
        }
        cookie.setPath("/");
        return cookie;
    }

    private String getUserRoles(UserProfile userProfile, int companyId) {
        StringBuilder csvRoles = new StringBuilder();
        CompanyDetail companyDetail = userProfile.getCompanyDetails().get(companyId);
        if (null != companyDetail) {
            List<Role> roles = companyDetail.getRolesList();
            int iterCtr = 0;
            if (null != roles) {
                for (Role role : roles) {
                    csvRoles.append(role.getId());
                    iterCtr++;
                    if (iterCtr < roles.size()) {
                        csvRoles.append(",");
                    }
                }
            }
        }
        return csvRoles.toString();
    }

    private String getScreenName(UserProfile userProfile) {
        String screenName = userProfile.getScreenName();
        String userName = userProfile.getUsername();
        return (!StringUtils.isEmpty(screenName)) ? screenName : (!StringUtils.isEmpty(userName) ? userName : "");
    }

    /**
     * Gets the cookie from the HttpServletRequest else returns null.
     * @param httpReq HttpServletRequest
     * @return String
     */
    public Cookie getACubeAuthCookieFromRequest(HttpServletRequest httpReq) {
        Cookie authCookie = GenUtil.getCookieFromRequest(httpReq, getAuthCookieName());
        Cookie userMiscCookie = GenUtil.getCookieFromRequest(httpReq, "usermisc");
        if (userMiscCookie != null && authCookie != null) {
            // CBS-20279: Presence of User Misc cookie indicates new Signin with httpOnly cookie, so do the
            // same thing in modify cookie (Same cookie object will be sent back after modification).
            // If User Misc cookie is not present do not set HttpOnly so that UI can read and delete it.
            authCookie.setHttpOnly(true);
        }
        return authCookie;
    }

    /**
     * Gets the cookie from the HttpServletRequest else returns null.
     * @param httpReq HttpServletRequest
     * @return String
     */
    public Cookie getUnconfirmedCookieFromRequest(HttpServletRequest httpReq) {
        String unconfirmedCookieName = commonCachedProperties.getPropertyValue("ct.services.authentication.unconfirmed-cookiename");
        Cookie authCookie = GenUtil.getCookieFromRequest(httpReq, unconfirmedCookieName);
        return authCookie;
    }

    public boolean isUserLoggedIn(Cookie authCookie) throws NoSuchAlgorithmException, IOException {
        boolean isLoggedIn = false;
        if (isCookieValid(authCookie) && StringUtils.isNotEmpty(getScreenName(authCookie))) {
            isLoggedIn = true;
        }
        return isLoggedIn;
    }

    public String getScreenName(Cookie authCookie) {
        HashMap<String, String> user = getPersonFromCookie(authCookie);
        String username = getScreenName(user);
        return username;
    }

    public boolean isCookieValid(Cookie authCookie) {
        if (authCookie != null) {
            String cookieValue = authCookie.getValue();
            return StringUtils.isNotEmpty(cookieValue);
        }
        return false;
    }

    public UserProfile getUnconfirmedUser(String unconfirmedEmail, int companyId, int roleId) {

        UserProfile userProfile = new UserProfile();
        try {
            userProfile = userAPIConnector.userRegister(unconfirmedEmail, companyId, roleId, false, null, "B2C_USERREGISTRATION");

        } catch (Exception e) {
            logger.error("Error posting user register details to API. Null Response", e);
        }

        return userProfile;
    }

    @Override
    public UserProfile getUnconfirmedUser(String unconfirmedEmail, int companyId, int roleId, String source, String caller) {

        UserProfile userProfile = new UserProfile();
        try {
            userProfile = userAPIConnector.userRegister(unconfirmedEmail, companyId, roleId, false, source, caller);

        } catch (Exception e) {
            logger.error("Error posting user register details to API. Null Response", e);
        }

        return userProfile;
    }

    public Cookie generateUnconfirmedCookie(UserProfile userProfile, String domainName) {

        Cookie cookie = null;

        try {
            String unconfirmedCookieName = commonCachedProperties.getPropertyValue("ct.services.authentication.unconfirmed-cookiename");
            if (userProfile != null) {
                cookie = new Cookie(unconfirmedCookieName, URLEncoder.encode(userProfile.getUsername(), "UTF-8"));

                if (domainName != null) {
                    cookie.setDomain(domainName);
                }

                // need to set the path, otherwise it signout won't be recognized.
                cookie.setPath("/");
            }
        } catch (Exception e) {
            logger.error("Error in generateUnconfirmedCookie : ", e);
        }

        return cookie;
    }

    public Cookie generateUserIdCookie(UserProfile userProfile, String domainName) {

        Cookie cookie = null;
        try {
            String userName = null;
            String screenName = null;
            if (userProfile != null) {
                userName = userProfile.getUsername();
                screenName = StringUtils.trimToNull(userProfile.getScreenName());
            }
            if (userName != null) {
                String userIdCookieName = "userid";
                String cookieValue = userName;
                if (screenName != null) {
                    cookieValue = userName + '|' + screenName;
                }
                cookie = new Cookie(userIdCookieName, URLEncoder.encode(cookieValue, "UTF-8"));
                int expiry = 3600 * 24 * 365;
                cookie.setMaxAge(expiry);

                if (domainName != null) {
                    cookie.setDomain(domainName);
                }

                cookie.setPath("/");

                return cookie;
            }
        } catch (Exception e) {
            logger.error("Error in generateUnconfirmedCookie : ", e);
        }

        return cookie;
    }

    public int getUserIdFromCookie(Cookie authCookie) {
        HashMap<String, String> personFromCookie = getPersonFromCookie(authCookie);
        String peopleId = personFromCookie.get(CommonEnumConstants.UserDetails.USER_ID.toString());
        return StringUtils.isNotEmpty(peopleId) ? Integer.parseInt(peopleId) : 0;
    }

    /**
     * Registers a new user.
     * @param userLoginProfile User Login Information
     * @return User Profile of registered user
     * @throws IOException On IO Exception
     * @throws XPathExpressionException On xpath exception
     */
    @Override
    public UserProfile userRegistration(UserLoginProfile userLoginProfile) throws IOException, XPathExpressionException {

        UserProfile userProfile = new UserProfile();
        FaultDetail faultDetail = new FaultDetail();

        try {
            if (commonCachedProperties.getBooleanPropertyValue("ct.common.travellers.enable", false)) {
                userLoginProfile.setTravellersNeeded(true);
                userProfile = userAPIConnector.userRegister(userLoginProfile, SourceType.B2C.toString(), "B2C_USERREGISTRATION");
            } else {
                userLoginProfile.setTravellersNeeded(false);
                userProfile = userAPIConnector.userRegister(userLoginProfile, SourceType.B2C.toString(), "B2C_USERREGISTRATION");
            }
        } catch (Exception e) {
            logger.error("Error posting user register details to API. Null Response", e);
            faultDetail.setFaultCode("NULL");
            userProfile.setFaultDetail(faultDetail);
        }

        return userProfile;
    }

    public UserProfile userRegistration(UserLoginProfile userLoginProfile, String source, String caller) throws IOException, XPathExpressionException {

        UserProfile userProfile = new UserProfile();
        FaultDetail faultDetail = new FaultDetail();

        try {
            if (commonCachedProperties.getBooleanPropertyValue("ct.common.travellers.enable", false)) {
                userLoginProfile.setTravellersNeeded(true);
                userProfile = userAPIConnector.userRegister(userLoginProfile, source, caller);
            } else {
                userLoginProfile.setTravellersNeeded(false);
                userProfile = userAPIConnector.userRegister(userLoginProfile, source, caller);
            }
        } catch (Exception e) {
            logger.error("Error posting user register details to API. Null Response", e);
            faultDetail.setFaultCode("NULL");
            userProfile.setFaultDetail(faultDetail);
        }

        return userProfile;
    }

    /**
     * Registers a New User without password.
     * @param username User Name
     * @param companyId Company Id
     * @param roleId Role Id
     * @param product product
     * @param defaultEmailOption Email Option for user
     * @param domainName Domain
     * @return User Profile
     * @throws IOException On IO Exception
     * @throws XPathExpressionException On xpath Exception
     */
    @Override
    public UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, String domainName) throws IOException, XPathExpressionException {

        UserProfile userProfile = new UserProfile();
        FaultDetail faultDetail = new FaultDetail();

        try {
            if (commonCachedProperties.getBooleanPropertyValue("ct.common.travellers.enable", false)) {
                userProfile = userAPIConnector.userRegister(username, companyId, roleId, defaultEmailOption, true, domainName, product, "B2C_USERREGISTRATION");
            } else {
                userProfile = userAPIConnector.userRegister(username, companyId, roleId, defaultEmailOption, false, domainName, product, "B2C_USERREGISTRATION");
            }
        } catch (Exception e) {
            logger.error("Error posting user register details to API. Null Response", e);
            faultDetail.setFaultCode("NULL");
            userProfile.setFaultDetail(faultDetail);
        }

        return userProfile;
    }

    @Override
    public UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, String domainName, SourceType source)
            throws IOException, XPathExpressionException {

        UserProfile userProfile = new UserProfile();
        FaultDetail faultDetail = new FaultDetail();

        try {
            if (commonCachedProperties.getBooleanPropertyValue("ct.common.travellers.enable", false)) {
                userProfile = userAPIConnector.userRegister(username, companyId, roleId, defaultEmailOption, true, domainName, source.toString(), "B2C_USERREGISTRATION");
            } else {
                userProfile = userAPIConnector.userRegister(username, companyId, roleId, defaultEmailOption, false, domainName, source.toString(), "B2C_USERREGISTRATION");
            }
        } catch (Exception e) {
            logger.error("Error posting user register details to API. Null Response", e);
            faultDetail.setFaultCode("NULL");
            userProfile.setFaultDetail(faultDetail);
        }

        return userProfile;
    }

    @Override
    public UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption) throws IOException, XPathExpressionException {
        String domainName = null;
        return userRegistration(username, companyId, roleId, product, defaultEmailOption, domainName);
    }

    @Override
    public UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, SourceType source) throws IOException, XPathExpressionException {
        return userRegistration(username, companyId, roleId, product, defaultEmailOption, null, source);
    }

    /**
     * Authenticate the user and return appropriate message.
     * @param username User Name
     * @param password Password
     * @param request Http Request
     * @param domain Domain
     * @param cookiesList - array list to which auth cookie corresponding to succesful authentication is added
     * @return String
     * @throws IOException On IO Exception
     * @throws XPathExpressionException On xpath Exception
     */
    public UserProfile authenticateUser(String username, String password, HttpServletRequest request, String domain, List<Cookie> cookiesList) throws XPathExpressionException, IOException {
        int i;
        int len;
        UserProfile userProfile = new UserProfile();
        FaultDetail faultDetail = new FaultDetail();
        String signinServiceUrl = "";
        if (request.getAttribute("isUserAccountsDirected") != null && request.getAttribute("isUserAccountsDirected").equals(true)) {
            signinServiceUrl = SecurityUtil.replaceHost(getLoginUrlToUserAccounts(), request);
        } else {
            signinServiceUrl = SecurityUtil.replaceHost(getLoginUrl(), request);
        }
        try {

            i = signinServiceUrl.indexOf('?');
            if (i > 0) {
                signinServiceUrl = signinServiceUrl.substring(0, i);
            }

            RestUtil.setResetAuthentication(commonCachedProperties);

            String params = "username=" + username + "&password=" + password;
            boolean isPersistentLogin = isPersistAuthCookie(request);
            if (isPersistentLogin) {
                params += "&persistent_login=t";
            }

            if (commonCachedProperties.getBooleanPropertyValue("ct.expresscheckout.enabled", false)) {
                params += "&card_details=true";
            }

            if (commonCachedProperties.getBooleanPropertyValue("ct.common.travellers.enable", false)) {
                params += "&travellers=true&redirect=";
            } else {
                params += "&redirect=";
            }

            Map<String, String> acceptHeader = new HashMap<String, String>(1);
            acceptHeader.put("accept", "text/json");
            Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

            // Begin Logging
            ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(ApiType.OTHER, null, "SIGNIN", "", signinServiceUrl, username);
            }
            // End Logging
            List<String> dontLog = new ArrayList<String>();
            dontLog.add("ALL");

            int timeout = commonCachedProperties.getIntPropertyValue(UserAPIConnectorTimeouts.AUTHENTICATION.getProperty(), UserAPIConnectorTimeouts.AUTHENTICATION.getTimeout());
            RestResponse restResponse = RestUtil.post(signinServiceUrl, "no", params, null, acceptHeader, responseHeaders, "POST", timeout, null, null, dontLog, false);

            // Begin Logging
            if (connectorStat != null) {
                connectorStat.closeApiRequest(ApiType.OTHER, "SIGNIN", restResponse.getCode(), restResponse.getMessage(), true);
            }
            // End Logging

            if (restResponse != null && restResponse.getCode() == 200) {

                UserProfileHolder userProfileHolder = new UserProfileHolder();
                userProfileHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), userProfileHolder);
                userProfile = userProfileHolder.getUserProfile();

                if (cookiesList != null) {
                    List<String> cookieEntries = responseHeaders.get("Set-Cookie");
                    String cookieValue = null;
                    len = cookieEntries.size();

                    for (i = 0; i < len; i++) {

                        String cookieEntry = cookieEntries.get(i);
                        // cookie entry will look like ct-auth=abcdfasda; domain=/; expires= etc.
                        String cookieName = cookieEntry.substring(0, cookieEntry.indexOf("="));
                        int pos = cookieEntry.indexOf('=') + 1;

                        if ("unconfirmed-email".equals(cookieName) && request.getAttribute("isMobileSignin") != null && request.getAttribute("isMobileSignin").equals(true)) {
                            faultDetail.setFaultCode("ACCOUNT_FLAGGED");
                            faultDetail.setFaultMessage("ACCOUNT FLAGGED");
                            userProfile.setFaultDetail(faultDetail);
                        }

                        if (pos > 0) {
                            // get the value of the cookie by doing a substring of the appropriate indexes
                            int pos1 = cookieEntry.indexOf(';', pos);

                            if (pos1 < 0) {
                                pos1 = cookieEntry.length();
                            }
                            cookieValue = cookieEntry.substring(pos, pos1);

                            if (StringUtils.isNotBlank(cookieName) && StringUtils.isNotBlank(cookieValue)) {

                                Cookie cookie = new Cookie(cookieName, cookieValue);
                                cookie.setPath("/");
                                if (domain != null) {
                                    cookie.setDomain(domain);
                                }

                                if (cookieName.equals(getAuthCookieName()) && isPersistentLogin) {
                                    cookie.setMaxAge(getAuthCookieAge());
                                }

                                cookiesList.add(cookie);
                            }
                        }
                    }
                }
            } else if (restResponse.getCode() == 401) {
                if (restResponse.getFaultDetailFromJson() != null && restResponse.getFaultDetailFromJson().getFaultCode() != null) {
                    if (restResponse.getFaultDetailFromJson().getFaultCode().equals("17")) {
                        faultDetail.setFaultCode("ACCOUNT_FLAGGED");
                        faultDetail.setFaultMessage("ACCOUNT FLAGGED");
                    } else if (restResponse.getFaultDetailFromJson().getFaultCode().equals("1")) {
                        faultDetail.setFaultCode("INVALID_UNAME");
                        faultDetail.setFaultMessage("INVALID UNAME");
                    } else {
                        faultDetail.setFaultCode("LOGIN_FAIL");
                        faultDetail.setFaultMessage("LOGIN FAIL");
                    }
                }
                userProfile.setFaultDetail(faultDetail);
            } else {
                logger.error("Error posting authenticateUser details to API. Null Response");
                faultDetail.setFaultCode("NULL");
                userProfile.setFaultDetail(faultDetail);
            }
        } catch (Exception e) {
            logger.error("Error posting authenticateUser details to API. Null Response", e);
            faultDetail.setFaultCode("NULL");
            userProfile.setFaultDetail(faultDetail);
        }

        return userProfile;
    }

    private boolean isB2BCall(String source) {
        return SourceType.CORP.toString().equalsIgnoreCase(source) || SourceType.AGENCY.toString().equalsIgnoreCase(source) || SourceType.WL.toString().equalsIgnoreCase(source);
    }

    public UserProfile authenticateUser(String username, String password, HttpServletRequest request, String domain, List<Cookie> cookiesList, String source, String caller)
            throws XPathExpressionException, IOException {
        int i;
        int len;
        UserProfile userProfile = new UserProfile();
        FaultDetail faultDetail = new FaultDetail();
        String signinServiceUrl = "";
        String signinServiceUrlProperty = "";
        if (request.getAttribute("isUserAccountsDirected") != null && request.getAttribute("isUserAccountsDirected").equals(true)) {
            signinServiceUrlProperty = getLoginUrlToUserAccounts();
            if (StringUtils.isNotBlank(source) && isB2BCall(source)) {
                signinServiceUrlProperty = "ct.common." + source.toLowerCase() + ".authentication.login.url.useraccounts";
            }
        } else {
            signinServiceUrlProperty = getLoginUrl();
            if (StringUtils.isNotBlank(source) && isB2BCall(source)) {
                signinServiceUrlProperty = "ct.common." + source.toLowerCase() + ".authentication.login.url";
            }
        }

        signinServiceUrl = SecurityUtil.replaceHost(signinServiceUrlProperty, request);

        try {
            i = signinServiceUrl.indexOf('?');
            if (i > 0) {
                signinServiceUrl = signinServiceUrl.substring(0, i);
            }

            RestUtil.setResetAuthentication(commonCachedProperties);

            String params = "caller=" + caller + "&username=" + username + "&password=" + password;
            boolean isPersistentLogin = isPersistAuthCookie(request);
            if (isPersistentLogin) {
                params += "&persistent_login=t";
            }

            if (commonCachedProperties.getBooleanPropertyValue("ct.expresscheckout.enabled", false)) {
                params += "&card_details=true";
            }

            if (commonCachedProperties.getBooleanPropertyValue("ct.common.travellers.enable", false)) {
                params += "&travellers=true&redirect=";
            } else {
                params += "&redirect=";
            }

            Map<String, String> acceptHeader = new HashMap<String, String>(1);
            acceptHeader.put("accept", "text/json");
            acceptHeader.put("X-Forwarded-Proto", "https");
            Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

            // Begin Logging
            ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(ApiType.OTHER, null, "SIGNIN", "", signinServiceUrl, username);
            }
            // End Logging
            List<String> dontLog = new ArrayList<String>();
            dontLog.add("ALL");

            int timeout = commonCachedProperties.getIntPropertyValue(UserAPIConnectorTimeouts.AUTHENTICATION.getProperty(), UserAPIConnectorTimeouts.AUTHENTICATION.getTimeout());
            RestResponse restResponse = RestUtil.post(signinServiceUrl, "no", params, null, acceptHeader, responseHeaders, "POST", timeout, null, null, dontLog, false);

            // Begin Logging
            if (connectorStat != null) {
                connectorStat.closeApiRequest(ApiType.OTHER, "SIGNIN", restResponse.getCode(), restResponse.getMessage(), true);
            }
            // End Logging

            if (restResponse != null && restResponse.getCode() == 200) {

                UserProfileHolder userProfileHolder = new UserProfileHolder();
                if (logger.isDebugEnabled()) {
                    logger.debug("Rest Response for : " + signinServiceUrl + "URL is--- " + restResponse.getMessage());
                }
                userProfileHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), userProfileHolder);
                userProfile = userProfileHolder.getUserProfile();

                if (cookiesList != null) {
                    List<String> cookieEntries = responseHeaders.get("Set-Cookie");
                    String cookieValue = null;
                    len = cookieEntries.size();

                    for (i = 0; i < len; i++) {

                        String cookieEntry = cookieEntries.get(i);
                        // cookie entry will look like ct-auth=abcdfasda; domain=/; expires= etc.
                        String cookieName = cookieEntry.substring(0, cookieEntry.indexOf("="));
                        int pos = cookieEntry.indexOf('=') + 1;

                        if ("unconfirmed-email".equals(cookieName) && request.getAttribute("isMobileSignin") != null && request.getAttribute("isMobileSignin").equals(true)) {
                            faultDetail.setFaultCode("ACCOUNT_FLAGGED");
                            faultDetail.setFaultMessage("ACCOUNT FLAGGED");
                            userProfile.setFaultDetail(faultDetail);
                        }

                        if (pos > 0) {
                            // get the value of the cookie by doing a substring of the appropriate indexes
                            int pos1 = cookieEntry.indexOf(';', pos);

                            if (pos1 < 0) {
                                pos1 = cookieEntry.length();
                            }
                            cookieValue = cookieEntry.substring(pos, pos1);

                            if (StringUtils.isNotBlank(cookieName) && StringUtils.isNotBlank(cookieValue)) {

                                Cookie cookie = new Cookie(cookieName, cookieValue);
                                cookie.setPath("/");
                                if (domain != null) {
                                    cookie.setDomain(domain);
                                }

                                if (cookieName.equals(getAuthCookieName()) && isPersistentLogin) {
                                    cookie.setMaxAge(getAuthCookieAge());
                                }

                                cookiesList.add(cookie);
                            }
                        }
                    }
                }
            } else if (restResponse.getCode() == 401) {
                if (restResponse.getFaultDetailFromJson() != null && restResponse.getFaultDetailFromJson().getFaultCode() != null) {
                    if (restResponse.getFaultDetailFromJson().getFaultCode().equals("17")) {
                        faultDetail.setFaultCode("ACCOUNT_FLAGGED");
                        faultDetail.setFaultMessage("ACCOUNT FLAGGED");
                    } else if (restResponse.getFaultDetailFromJson().getFaultCode().equals("1")) {
                        faultDetail.setFaultCode("INVALID_UNAME");
                        faultDetail.setFaultMessage("INVALID UNAME");
                    } else {
                        faultDetail.setFaultCode("LOGIN_FAIL");
                        faultDetail.setFaultMessage("LOGIN FAIL");
                    }
                }
                userProfile.setFaultDetail(faultDetail);
            } else {
                logger.error("Error posting authenticateUser details to API. Null Response");
                faultDetail.setFaultCode("NULL");
                userProfile.setFaultDetail(faultDetail);
            }
        } catch (Exception e) {
            logger.error("Error posting authenticateUser details to API. Null Response", e);
            faultDetail.setFaultCode("NULL");
            userProfile.setFaultDetail(faultDetail);
        }

        return userProfile;
    }

    /**
     * Authenticate the user and return appropriate message. Start using authenticateUser(String username, String password, HttpServletRequest request, String domain, List<Cookie> cookiesList) instead
     * of this method. Cookies should be a list rather than array. Because authentication api sends multiple cookies and we need to set all.
     * @param username user name
     * @param password password
     * @param request Request
     * @param retCookie Auth Cookie to set in response
     * @return String
     * @throws IOException on IO Exception
     * @throws XPathExpressionException On xpath exception
     * @deprecated
     */
    public UserProfile authenticateUser(String username, String password, HttpServletRequest request, Cookie[] retCookie) throws XPathExpressionException, IOException {

        List<Cookie> cookiesList = new ArrayList<Cookie>(5);

        UserProfile userProfile = authenticateUser(username, password, request, CommonEnumConstants.CT_DOMAIN, cookiesList, SourceType.B2C.toString(),
                APIUtil.getSourceString("CHMM", "TRAVELLER", "ADDANDAUTHENTICATE"));

        String authCookieName = getAuthCookieName();
        if (cookiesList != null) {
            for (Cookie cookie : cookiesList) {
                if (cookie != null && cookie.getName().equalsIgnoreCase(authCookieName)) {
                    retCookie[0] = cookie;
                    break;
                }
            }
        }

        return userProfile;
    }

    /**
     * Returns user profile from user name.
     * @param username String
     * @param source source indicator
     * @param caller caller indicator for accounts API
     * @return UserProfile
     * @exception IOException in case of IO Exception
     */
    public UserProfile fetchUserProfile(String username, SourceType source, String caller, HttpServletRequest request) throws IOException {
        CTSecurityHeaders ctSecurityHeader = new CTSecurityHeaders(request, source);
        UserProfile userProfile = userAPIConnector.fetchUserProfile(username, source, caller, ctSecurityHeader);
        return userProfile;
    }

    /**
     * {@inheritDoc}
     * @see com.cleartrip.common.service.AuthenticationService#getAuthCookieName()
     */
    public String getAuthCookieName() {
        return SecurityUtil.getAuthCookieName(commonCachedProperties);
    }

    /**
     * Method to return age of authentication-cookie (ct-auth) in seconds.
     * @return
     */
    protected int getAuthCookieAge() {
        int ageInSeconds = SECONDS_IN_A_DAY;
        int ageInDays = commonCachedProperties.getIntPropertyValue("ct.services.authentication.auth-cookie.age", -1);
        if (ageInDays >= 0) {
            ageInSeconds = ageInDays * 86400;
        }

        return ageInSeconds;
    }

    /**
     * Checks if 'remember me' option is checked while signing in.
     * @param request
     * @return
     */
    protected boolean isPersistAuthCookie(HttpServletRequest request) {

        String persistentLoginParam = StringUtils.trimToEmpty(request.getParameter("persistent_login"));

        // Since we are checking for a checkbox, merely checking
        // it's presence will do the trick
        boolean isPersist = !StringUtils.isEmpty(persistentLoginParam);
        return isPersist;
    }

    /**
     * {@inheritDoc}
     * @see com.cleartrip.common.service.AuthenticationService#getLoginUrl()
     */
    public String getLoginUrl() {
        return commonCachedProperties.getPropertyValue("ct.common.authentication.login.url");
    }

    /**
     * {@inheritDoc}
     * @see com.cleartrip.common.service.AuthenticationService#getLoginUrlToUserAccounts()
     */
    public String getLoginUrlToUserAccounts() {
        return commonCachedProperties.getPropertyValue("ct.common.authentication.login.url.useraccounts");
    }

    /**
     * {@inheritDoc}
     * @see com.cleartrip.common.service.AuthenticationService#isSecurityEnable()
     */
    public boolean isSecurityEnable() {
        return commonCachedProperties.getBooleanPropertyValue("ct.common.authentication.security.enable", true);
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    public void setProxyAuthUsername(String proxyAuthUsername) {
        this.proxyAuthUsername = proxyAuthUsername;
    }

    public void setProxyAuthPassword(String proxyAuthPassword) {
        this.proxyAuthPassword = proxyAuthPassword;
    }

    public void setProxyEnable(boolean proxyEnable) {
        this.proxyEnable = proxyEnable;
    }

    public void setProxyAddr(String proxyAddr) {
        this.proxyAddr = proxyAddr;
    }

    /**
     * Setter for commonCachedProperties.
     * @param pcommonCachedProperties the commonCachedProperties to set
     */
    public void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
    }

    /**
     * Getter method for getUserAPIConnector.
     * @return the userAPIConnector
     */
    public UserAPIConnector getUserAPIConnector() {
        return userAPIConnector;
    }

    /**
     * @param userAPIConnector the userAPIConnector to set
     */
    public void setUserAPIConnector(UserAPIConnector userAPIConnector) {
        this.userAPIConnector = userAPIConnector;
    }

    /**
     * @return the companyAPIConnector
     */
    public CompanyAPIConnector getCompanyAPIConnector() {
        return companyAPIConnector;
    }

    /**
     * @param companyAPIConnector the companyAPIConnector to set
     */
    public void setCompanyAPIConnector(CompanyAPIConnector companyAPIConnector) {
        this.companyAPIConnector = companyAPIConnector;
    }

    /**
     * Setter for cachedUrlResources.
     * @param pcachedUrlResources the cachedUrlResources to set
     */
    public void setCachedUrlResources(CachedUrlResources<CommonUrlResources> pcachedUrlResources) {
        cachedUrlResources = pcachedUrlResources;
    }

    /**
     * Setter for random.
     * @param prandom the random to set
     */
    public void setRandom(Random prandom) {
        random = prandom;
    }

    public UserProfile authenticateUser(String username, String password, HttpServletRequest request, Cookie[] retCookie, String source, String caller) throws XPathExpressionException, IOException {

        List<Cookie> cookiesList = new ArrayList<Cookie>(5);
        // CBS-22302
        UserProfile userProfile = null;
        if (commonCachedProperties.getBooleanPropertyValue("ct.hotel.redirection.enabled.for.domains", false)) {
            userProfile = authenticateUser(username, password, request, SecurityUtil.getDomainFromRequest(request), cookiesList, source, caller);
        } else {
            userProfile = authenticateUser(username, password, request, CommonEnumConstants.CT_DOMAIN, cookiesList, source, caller);
        }
        // UserProfile userProfile = authenticateUser(username, password, request, CommonEnumConstants.CT_DOMAIN, cookiesList, source, caller);
        String authCookieName = getAuthCookieName();
        if (cookiesList != null) {
            for (Cookie cookie : cookiesList) {
                if (cookie != null && cookie.getName().equalsIgnoreCase(authCookieName)) {
                    retCookie[0] = cookie;
                    break;
                }
            }
        }

        return userProfile;
    }

    @Override
    public UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, String domainName, String source, String caller)
            throws IOException, XPathExpressionException {

        UserProfile userProfile = new UserProfile();
        FaultDetail faultDetail = new FaultDetail();

        try {
            if (commonCachedProperties.getBooleanPropertyValue("ct.common.travellers.enable", false)) {
                userProfile = userAPIConnector.userRegister(username, companyId, roleId, defaultEmailOption, true, domainName, source, caller);
            } else {
                userProfile = userAPIConnector.userRegister(username, companyId, roleId, defaultEmailOption, false, domainName, source, caller);
            }
        } catch (Exception e) {
            logger.error("Error posting user register details to API. Null Response", e);
            faultDetail.setFaultCode("NULL");
            userProfile.setFaultDetail(faultDetail);
        }

        return userProfile;
    }

    @Override
    @MethodExcludeCoverage
    public UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, String domainName, String source, String caller, String host)
            throws IOException, XPathExpressionException {

        UserProfile userProfile = new UserProfile();
        FaultDetail faultDetail = new FaultDetail();

        try {
            if (commonCachedProperties.getBooleanPropertyValue("ct.common.travellers.enable", false)) {
                userProfile = userAPIConnector.userRegister(username, companyId, roleId, defaultEmailOption, true, domainName, source, caller, host);
            } else {
                userProfile = userAPIConnector.userRegister(username, companyId, roleId, defaultEmailOption, false, domainName, source, caller, host);
            }
        } catch (Exception e) {
            logger.error("Error posting user register details to API. Null Response", e);
            faultDetail.setFaultCode("NULL");
            userProfile.setFaultDetail(faultDetail);
        }

        return userProfile;
    }

    @Override
    public Cookie generateUserTokenCookie(Integer userToken) throws Exception {
        String userTokenCookieName = commonCachedProperties.getPropertyValue("ct.services.authentication.user-token-cookiename", "tkn");
        Cookie cookie = new Cookie(userTokenCookieName, userToken.toString());
        cookie.setPath("/");

        return cookie;
    }

    @Override
    public Cookie getUserTokenCookie(HttpServletRequest httpReq) {
        String userTokenCookieName = commonCachedProperties.getPropertyValue("ct.services.authentication.user-token-cookiename", "tkn");
        Cookie userTokenCookie = GenUtil.getCookieFromRequest(httpReq, userTokenCookieName);
        return userTokenCookie;
    }

    @Override
    public String getUserToken(Cookie userTokenCookie) {
        String userToken = "";
        if (null != userTokenCookie) {
            try {
                userToken = userTokenCookie.getValue();
            } catch (Exception e) {
                logger.error("Error while decrypting userTokenCookie", e);
            }
        }
        return userToken;
    }

    @Override
    public int getB2BAuthCookieAge(String source) {
        String authCookieAgeJson = commonCachedProperties.getPropertyValue("ct.services.authentication.b2b.auth-cookie.age", "");
        int authCookieAge = 300;
        if (StringUtils.isNotBlank(authCookieAgeJson)) {
            Map<String, Integer> authCookieAgeMap = APIUtil.deserializeJsonToObject(authCookieAgeJson, Map.class);
            if (null != authCookieAgeMap && authCookieAgeMap.size() > 0 && null != authCookieAgeMap.get(source)) {
                authCookieAge = authCookieAgeMap.get(source);
            }
        }
        return authCookieAge;
    }

    @Override
    public Cookie generateB2BAuthCookie(String source) {
        String b2bAuthCookieName = commonCachedProperties.getPropertyValue(B2B_AUTH_COOKIE_NAME, "b2b-auth");
        int b2bAuthCookieAge = getB2BAuthCookieAge(source);
        Cookie b2bAuthCookie = new Cookie(b2bAuthCookieName, String.valueOf(b2bAuthCookieAge));
        b2bAuthCookie.setMaxAge(b2bAuthCookieAge);
        b2bAuthCookie.setPath("/");
        return b2bAuthCookie;
    }

    @Override
    public Cookie getB2BAuthCookie(HttpServletRequest httpReq) {
        String b2bAuthCookieName = commonCachedProperties.getPropertyValue(B2B_AUTH_COOKIE_NAME, "b2b-auth");
        Cookie b2bAuthCookie = GenUtil.getCookieFromRequest(httpReq, b2bAuthCookieName);
        return b2bAuthCookie;
    }

    @Override
    public Cookie modifyB2BAuthCookie(Cookie b2bAuthCookie, String source) {
        int b2bAuthCookieAge = getB2BAuthCookieAge(source);
        b2bAuthCookie.setMaxAge(b2bAuthCookieAge);
        return b2bAuthCookie;
    }
}
