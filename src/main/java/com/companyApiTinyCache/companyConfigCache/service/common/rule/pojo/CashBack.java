package com.companyApiTinyCache.companyConfigCache.service.common.rule.pojo;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

import java.io.Serializable;
import java.util.Date;

@ClassExcludeCoverage
public class CashBack implements Serializable {

	private static final long serialVersionUID = 838854469379782471L;
	private String message;
	private Double amount;
	private String messageTemplate;
	private Date triggerDate;
	private Date expiryDate;
	private WalletType walletType;
	
	public CashBack() {}

	private CashBack(String message, Double amount, String messageTemplate,Date triggerDate, Date expiryDate, WalletType walletType) {
		super();
		this.message = message;
		this.amount = amount;
		this.messageTemplate = messageTemplate;
		this.triggerDate = triggerDate;
		this.expiryDate = expiryDate;
		this.walletType = walletType;
	}

	public static CashBack createCashBack(String message, Double amount, Date triggerDate, Date expiryDate, String messageTemplate, WalletType walletType) throws CashBackCreateException {
		if (triggerDate == null || expiryDate == null) {
			throw new CashBackCreateException("Expiry date or Trigger date can't be null");
		}
		return new CashBack(message, amount, messageTemplate, triggerDate, expiryDate, walletType);
	}

	public String getMessage() {
		return message;
	}

	public Double getAmount() {
		return amount;
	}

	public String getMessageTemplate() {
		return messageTemplate;
	}

	public Date getTriggerDate() {
		return triggerDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	
	public void setMessage(String message) {
		this.message = message;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setMessageTemplate(String messageTemplate) {
		this.messageTemplate = messageTemplate;
	}

	public void setTriggerDate(Date triggerDate) {
		this.triggerDate = triggerDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public WalletType getWalletType() {
		return walletType;
	}

	public void setWalletType(WalletType walletType) {
		this.walletType = walletType;
	}

	@Override
	public String toString() {
		return "CashBack [message=" + message + ", amount=" + amount
				+ ", messageTemplate=" + messageTemplate + ", triggerDate="
				+ triggerDate + ", expiryDate=" + expiryDate + ", walletType="+ walletType +"]";
	}

	@SuppressWarnings("serial")
	public static class CashBackCreateException extends Exception {

		private String message;

		public CashBackCreateException(String message) {
			super();
			this.message = message;
		}

		public String getMessage() {
			return message;
		}

		@Override
		public String toString() {
			return "CashBackCreateException [message=" + message + "]";
		}
	}
}
