package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CalendarFareInfo;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author dhiraj
 *
 */
public class CalendarNewSearchServiceImpl implements CalendarSearchService {

    private static final String ROUNDTRIP = "ROUNDTRIP";

    private static final String RETURN = "RETURN";

    private static final String ONWARD = "ONWARD";

    private static final Log logger = LogFactory.getLog(CalendarNewSearchServiceImpl.class);

    public static final String CALENDAR_JSON_MEMCACHED_SERVER_PROPERTY_NAME = "ct.services.calendar.memcached.servers";

    private Cache cache;

    public CalendarNewSearchServiceImpl(CacheFactory pcacheFactory) {
        cache = pcacheFactory.getCacheForServer(CALENDAR_JSON_MEMCACHED_SERVER_PROPERTY_NAME);
    }

    @Override
    public String generateJsonString(String from, String to, String startDate, String endDate, String roundtripReturnAfter, String countryCode,
            boolean cheapestOnly, String version, String sourcetype, boolean sendAirline) {
        String responseJson = null;
        List<Integer> returnAfterList = getReturnAfterList(roundtripReturnAfter);
        Boolean isRoundtrip = (returnAfterList != null && returnAfterList.size() > 0);
        // version v1
        if (version == null || "v1".equalsIgnoreCase(version)) {
            // FIRST STEP : Get Return After List.
            Map<Date, Object> calendarData = getCalendarData(from, to, startDate, endDate, returnAfterList, countryCode, sourcetype);
            // make extra B2C call for mobile and fill the dates which are empty.
            if ("mobile".equalsIgnoreCase(sourcetype)) {
                Map<Date, Object> b2CcalendarData = getCalendarData(from, to, startDate, endDate, returnAfterList, countryCode, "B2C");
                fillEmptyDates(calendarData, b2CcalendarData, isRoundtrip);
            }
            // FINAL STEP : Convert Back to JSON.
            Map<String, List<CalendarFareInfo>> jsonMap = convertToJsonMap(calendarData, isRoundtrip);
            sortByPrice(jsonMap);
            if (cheapestOnly) {
                retainCheapestFareInfo(jsonMap);
            }
            responseJson = convertTojson(jsonMap);
            // V2
        } else if ("v2".equalsIgnoreCase(version)) {
            Map<String, Map<String, List<CalendarFareInfo>>> calendarDataV2 = getCalendarDataV2(from, to, startDate, endDate, countryCode, cheapestOnly, sourcetype, returnAfterList);
            Map<String, List<CalendarFareInfo>> calendarInfos = calendarDataV2.get(ONWARD);
            if ("mobile".equalsIgnoreCase(sourcetype)) {
                boolean isAlreadyCheapestFilter = cheapestOnly;
                if (!isAlreadyCheapestFilter) {
                    retainCheapest(calendarDataV2);
                }
                if (isRoundtrip) {
                    calendarInfos = getCheapestRoundtripCombination(calendarDataV2, returnAfterList);
                } else {
                    retainOnlyFewData(calendarInfos, sendAirline);
                }
                setCheapestForDateRange(calendarInfos);
            }
            responseJson = convertTojson(calendarInfos);
        } else if ("v3".equalsIgnoreCase(version)){
        	// Returns the airline wise cheapest fare
        	Map<String, List<CalendarFareInfo>> calendarData = getCalendarDataV3(from, to, startDate, endDate, countryCode, false, sourcetype, returnAfterList);
        	responseJson = convertTojson(calendarData);
        }
        return responseJson;
    }

    /** call this function only after sorting the value per date based on price.
     * @param jsonMap
     */
    private void setCheapestForDateRange(Map<String, List<CalendarFareInfo>> calendarInfos) {
        List<String> cheapestDates = new ArrayList<String>();
        double cheapest = Double.MAX_VALUE;
        for (String date : calendarInfos.keySet()) {
            List<CalendarFareInfo> infos = calendarInfos.get(date);
            if (infos != null && infos.size() > 0) {
                double price = getPrice(infos.get(0));
                if (cheapest == price) {
                    cheapestDates.add(date);
                } else if (price < cheapest) {
                    cheapestDates.clear();
                    cheapestDates.add(date);
                    cheapest = price;
                }
            }
        }
        for (String date : cheapestDates) {
            calendarInfos.get(date).get(0).setCheapest(true);
        }
    }
    private void retainOnlyFewData(Map<String, List<CalendarFareInfo>> jsonMap, boolean sendAirline) {
        for (List<CalendarFareInfo> onwardData : jsonMap.values()) {
                if (onwardData != null && onwardData.size() > 0) {
                    retainOnlyFewData(onwardData, sendAirline);
                }
        }
    }

    private void retainOnlyFewData(List<CalendarFareInfo> onwardData, boolean sendAirline) {
        CalendarFareInfo tempCalendarFareInfo = onwardData.iterator().next();
        onwardData.clear();
        CalendarFareInfo calendarFareInfoV2 = new CalendarFareInfo();
        onwardData.add(calendarFareInfoV2);
        calendarFareInfoV2.setPrice(tempCalendarFareInfo.getPrice());
        calendarFareInfoV2.setCachedTime(tempCalendarFareInfo.getCachedTime());
        if (sendAirline) {
            calendarFareInfoV2.setAirline(tempCalendarFareInfo.getAirline());
        }
    }

    private Map<String, List<CalendarFareInfo>> getCheapestRoundtripCombination(Map<String, Map<String, List<CalendarFareInfo>>> calendarDataV2, List<Integer> returnAfterList) {
        Map<String, List<CalendarFareInfo>> jsonMap = new HashMap<String, List<CalendarFareInfo>>();
        if (calendarDataV2 != null) {
            jsonMap = getCheapestRoundtripCombination(calendarDataV2.get(ONWARD), calendarDataV2.get(RETURN), calendarDataV2.get(ROUNDTRIP), returnAfterList);
        }
        return jsonMap;
    }
    
    private Map<String, List<CalendarFareInfo>> getAirlinewiseCheapestRoundtripCombination(Map<String, List<CalendarFareInfo>> onwardCalendarData, Map<String, List<CalendarFareInfo>> returnCalendarData, Map<String, List<CalendarFareInfo>> roundTripCalendarData, List<Integer> returnAfterList) {
		Map<String, List<CalendarFareInfo>> cheapestAirlinewiseRoundtripFareMap = new HashMap<>();
    	for(String onwardDate : onwardCalendarData.keySet()){
			String returnDate = getReturnSearchDate(onwardDate, returnAfterList);
			List<CalendarFareInfo> tempCalendarData = getAirlinewiseCheapestRoundtripCombination(onwardCalendarData.get(onwardDate), returnCalendarData.get(returnDate), roundTripCalendarData.get(onwardDate));
			cheapestAirlinewiseRoundtripFareMap.put(onwardDate, tempCalendarData);
		}
    	return cheapestAirlinewiseRoundtripFareMap;
	}
    
    private List<CalendarFareInfo> getAirlinewiseCheapestRoundtripCombination(List<CalendarFareInfo> onwardData, List<CalendarFareInfo> returnData, List<CalendarFareInfo> roundTripData){
    	List<CalendarFareInfo> cheapestAirlinewiseRoundtripFareList = new ArrayList<>();
    	
    	Map<String, CalendarFareInfo> onwardMap = new HashMap<>();
    	Map<String, CalendarFareInfo> returnMap = new HashMap<>();
    	Map<String, CalendarFareInfo> roundTripMap = new HashMap<>();
    	Map<String, CalendarFareInfo> onwardAndReturnCombinedMap = new HashMap<>();
    	
    	CalendarFareInfo cheapestOnwardFare = null;
    	CalendarFareInfo cheapestReturnFare = null;
    	CalendarFareInfo cheapestRoundtripFare = null;
    	Double cheapestFare;
    	
    	if(onwardData != null && onwardData.size() > 0){
    		cheapestFare = Double.parseDouble(onwardData.get(0).getPrice());
	    	for(int i=0;i<onwardData.size();i++){
	    		onwardMap.put(onwardData.get(i).getAirline(), onwardData.get(i));
	    		if(Double.parseDouble(onwardData.get(i).getPrice()) < cheapestFare){
	    			cheapestFare = Double.parseDouble(onwardData.get(i).getPrice());
	    			cheapestOnwardFare = onwardData.get(i);
	    		}
	    	}
    	}
    	if(returnData != null && returnData.size() > 0){
    		cheapestFare = Double.parseDouble(returnData.get(0).getPrice());
    		for (int i = 0; i < returnData.size(); i++) {
				returnMap.put(returnData.get(i).getAirline(), returnData.get(i));
				if(Double.parseDouble(returnData.get(i).getPrice()) < cheapestFare){
	    			cheapestFare = Double.parseDouble(returnData.get(i).getPrice());
	    			cheapestOnwardFare = returnData.get(i);
	    		}
			}
    	}
    	if(roundTripData != null && roundTripData.size() >0 ){
    		cheapestFare = Double.parseDouble(roundTripData.get(0).getPrice());
    		for (int i = 0; i < roundTripData.size(); i++) {
				roundTripMap.put(roundTripData.get(i).getOnwardAirline(), roundTripData.get(i));
				if(Double.parseDouble(roundTripData.get(i).getPrice()) < cheapestFare){
	    			cheapestFare = Double.parseDouble(roundTripData.get(i).getPrice());
	    			cheapestOnwardFare = roundTripData.get(i);
	    		}
			}
    	}
    	if (onwardMap != null && !onwardMap.isEmpty() && returnMap != null && !returnMap.isEmpty()) {
    		for(Entry<String, CalendarFareInfo> onward : onwardMap.entrySet()){
        		CalendarFareInfo onwardValue = onward.getValue();
        		CalendarFareInfo returnValue = returnMap.get(onward.getKey());
				if(returnValue != null){
					onwardAndReturnCombinedMap.put(onwardValue.getAirline(), combineOnwardAndReturnCalendarInfo(onwardValue, returnValue));
        		}
        	}
		}
    	//Adding the cheapest fare - any airline combination.
    	if(null != cheapestOnwardFare && null != cheapestReturnFare){
    		if(! cheapestOnwardFare.getAirline().equals(cheapestReturnFare.getAirline())){
    			CalendarFareInfo tmpCalenderFare = combineOnwardAndReturnCalendarInfo(cheapestOnwardFare, cheapestReturnFare);
    			if(null != cheapestRoundtripFare){
    				if(Double.parseDouble(tmpCalenderFare.getPrice()) < Double.parseDouble(cheapestRoundtripFare.getPrice())){
    					cheapestAirlinewiseRoundtripFareList.add(tmpCalenderFare);
    				} else {
						cheapestAirlinewiseRoundtripFareList.add(cheapestRoundtripFare);
						roundTripMap.remove(cheapestRoundtripFare.getAirline());
					}
    			}
    		}
    	}
    	//Adding Airline wise-Cheapest fare
    	if(onwardAndReturnCombinedMap != null && !onwardAndReturnCombinedMap.isEmpty()){
    		if(roundTripMap != null && !roundTripMap.isEmpty()){
    			for(Entry<String, CalendarFareInfo> combinedFareinfo : onwardAndReturnCombinedMap.entrySet()){
    				if(roundTripMap.get(combinedFareinfo.getKey()) != null){
    					if(Double.parseDouble(combinedFareinfo.getValue().getPrice()) < Double.parseDouble(roundTripMap.get(combinedFareinfo.getKey()).getPrice())){
        					cheapestAirlinewiseRoundtripFareList.add(combinedFareinfo.getValue());
        				} else {
							cheapestAirlinewiseRoundtripFareList.add(roundTripMap.get(combinedFareinfo.getKey()));
						}
    				}
    			}
    		} else {
				cheapestAirlinewiseRoundtripFareList.addAll(roundTripMap.values());
			}
    	} else {
    		if(roundTripMap != null && !roundTripMap.isEmpty()){
    			cheapestAirlinewiseRoundtripFareList.addAll(onwardAndReturnCombinedMap.values());
    		}
		}
    	
    	
    	/*if(onwardData != null && returnData != null && onwardData.size()>0 && returnData.size()>0){
    		for(int i=0;i<onwardData.size();i++){
    			for(int j=0;j<returnData.size();j++){
    				if(onwardData.get(i).getAirline().equalsIgnoreCase(returnData.get(j).getAirline())){
    					CalendarFareInfo tempCalendarData = new CalendarFareInfo();
    					tempCalendarData.setOnwardAirline(onwardData.get(i).getAirline());
    					tempCalendarData.setOnwardAirlineName(onwardData.get(i).getAirlineName());
						tempCalendarData.setOnwardAllFlightDetails(onwardData.get(i).getAllFlightDetails());
						tempCalendarData.setOnwardArrivalDate(onwardData.get(i).getArrivalDate());
						tempCalendarData.setOnwardArrivalTime(onwardData.get(i).getArrivalTime());
						tempCalendarData.setOnwardDepartureDate(onwardData.get(i).getDepartureDate());
						tempCalendarData.setOnwardDepartureTime(onwardData.get(i).getDepartureTime());
						tempCalendarData.setOnwardVia(onwardData.get(i).getVia());
    					tempCalendarData.setReturnAirline(returnData.get(j).getAirline());
						tempCalendarData.setReturnAirlineName(returnData.get(j).getAirlineName());
						tempCalendarData.setReturnAllFlightDetails(returnData.get(j).getAllFlightDetails());
						tempCalendarData.setReturnArrivalDate(returnData.get(j).getArrivalDate());
						tempCalendarData.setReturnArrivalTime(returnData.get(j).getArrivalTime());
						tempCalendarData.setReturnDepartureDate(returnData.get(j).getDepartureDate());
						tempCalendarData.setReturnDepartureTime(returnData.get(j).getDepartureTime());
						tempCalendarData.setReturnVia(returnData.get(j).getVia());
						tempCalendarData.setCachedTime(onwardData.get(i).getCachedTime() < returnData.get(j).getCachedTime()?onwardData.get(i).getCachedTime():returnData.get(j).getCachedTime());
    					tempCalendarData.setPrice(Double.toString(Double.parseDouble(onwardData.get(i).getPrice()) + Double.parseDouble(returnData.get(j).getPrice())));
    					boolean matchingAirlineFound = false;
    					if(roundTripData != null && roundTripData.size()>0){
	    					for(int k=0;k<roundTripData.size();k++){
	    						if(tempCalendarData.getOnwardAirline().equalsIgnoreCase(roundTripData.get(k).getOnwardAirline())){
	    							matchingAirlineFound = true;
	    							if(Double.parseDouble(tempCalendarData.getPrice())>Double.parseDouble(roundTripData.get(k).getPrice())){
	    								cheapestAirlinewiseRoundtripFareList.add(roundTripData.get(k));
	    							} else {
	    								cheapestAirlinewiseRoundtripFareList.add(tempCalendarData);
									}
	    						}
	    					}//for
    					}
    					if(!matchingAirlineFound){
    						cheapestAirlinewiseRoundtripFareList.add(tempCalendarData);
    					}
    				}//if
    			}//for
    		}//for
    	} else if ((onwardData == null || returnData == null || onwardData.size() <= 0 || returnData.size() <= 0 ) && roundTripData != null && roundTripData.size()>0) {
			cheapestAirlinewiseRoundtripFareList.addAll(roundTripData);
		}*/
    	return cheapestAirlinewiseRoundtripFareList;
    }
    
    private CalendarFareInfo combineOnwardAndReturnCalendarInfo(CalendarFareInfo onwardCalenderFare, CalendarFareInfo returnCalendarfare) {
		CalendarFareInfo combinedCalendarFareInfo = new CalendarFareInfo();
		
		combinedCalendarFareInfo.setOnwardAirline(onwardCalenderFare.getAirline());
		combinedCalendarFareInfo.setOnwardAirlineName(onwardCalenderFare.getAirlineName());
		combinedCalendarFareInfo.setOnwardAllFlightDetails(onwardCalenderFare.getAllFlightDetails());
		combinedCalendarFareInfo.setOnwardArrivalDate(onwardCalenderFare.getArrivalDate());
		combinedCalendarFareInfo.setOnwardArrivalTime(onwardCalenderFare.getArrivalTime());
		combinedCalendarFareInfo.setOnwardDepartureDate(onwardCalenderFare.getDepartureDate());
		combinedCalendarFareInfo.setOnwardDepartureTime(onwardCalenderFare.getDepartureTime());
		combinedCalendarFareInfo.setOnwardVia(onwardCalenderFare.getVia());
		combinedCalendarFareInfo.setReturnAirline(returnCalendarfare.getAirline());
		combinedCalendarFareInfo.setReturnAirlineName(returnCalendarfare.getAirlineName());
		combinedCalendarFareInfo.setReturnAllFlightDetails(returnCalendarfare.getAllFlightDetails());
		combinedCalendarFareInfo.setReturnArrivalDate(returnCalendarfare.getArrivalDate());
		combinedCalendarFareInfo.setReturnArrivalTime(returnCalendarfare.getArrivalTime());
		combinedCalendarFareInfo.setReturnDepartureDate(returnCalendarfare.getDepartureDate());
		combinedCalendarFareInfo.setReturnDepartureTime(returnCalendarfare.getDepartureTime());
		combinedCalendarFareInfo.setReturnVia(returnCalendarfare.getVia());
		combinedCalendarFareInfo.setCachedTime(onwardCalenderFare.getCachedTime() < returnCalendarfare.getCachedTime() ? onwardCalenderFare.getCachedTime() : returnCalendarfare.getCachedTime());
		combinedCalendarFareInfo.setPrice(Double.toString(Double.parseDouble(onwardCalenderFare.getPrice()) + Double.parseDouble(returnCalendarfare.getPrice())));
    	return combinedCalendarFareInfo;
	}

    private Map<String, List<CalendarFareInfo>> getCheapestRoundtripCombination(Map<String, List<CalendarFareInfo>> onwardCalendar, Map<String, List<CalendarFareInfo>> returnCalendar,
            Map<String, List<CalendarFareInfo>> roundtripCalendar, List<Integer> returnAfterList) {
        Map<String, List<CalendarFareInfo>> jsonMap = new LinkedHashMap<String, List<CalendarFareInfo>>();
        for (String onwardDate : onwardCalendar.keySet()) {
               String returnDate = getReturnSearchDate(onwardDate, returnAfterList);
               List<CalendarFareInfo> onwardData = onwardCalendar.get(onwardDate);
               List<CalendarFareInfo> returnData = returnCalendar.get(returnDate);
               List<CalendarFareInfo> roundtripData = roundtripCalendar.get(onwardDate);
               List<CalendarFareInfo> tempCalendarData = getCheapestRoundtripCombination(onwardData, returnData, roundtripData);
               jsonMap.put(onwardDate, tempCalendarData);
        }
        return jsonMap;
    }
    //list has to be sorted before calling this function.
    private List<CalendarFareInfo> getCheapestRoundtripCombination(List<CalendarFareInfo> onwardData, List<CalendarFareInfo> returnData, List<CalendarFareInfo> roundtripData) {
        List<CalendarFareInfo> roundtripCalendarData = new ArrayList<CalendarFareInfo>();
        if (onwardData == null || onwardData.size() < 1 || returnData == null || returnData.size() < 1) {
            return roundtripData;
        }
        CalendarFareInfo onwardFareInfo = onwardData.iterator().next();
        CalendarFareInfo returnFareInfo = returnData.iterator().next();
        CalendarFareInfo roundtripFareInfo =  null;
        if (roundtripData != null && roundtripData.size() > 0) {
             roundtripFareInfo = roundtripData.iterator().next();
        }
        CalendarFareInfo cheapestRoundtripFareInfo = getCheapestRoundtripCombination(onwardFareInfo, returnFareInfo, roundtripFareInfo);
        roundtripCalendarData.add(cheapestRoundtripFareInfo);
        return roundtripCalendarData;
    }

    private CalendarFareInfo getCheapestRoundtripCombination(CalendarFareInfo onwardFareInfo, CalendarFareInfo returnFareInfo, CalendarFareInfo roundtripFareInfo) {
        CalendarFareInfo cheapestRoundtripFareInfo = new CalendarFareInfo();
        if (roundtripFareInfo != null) {
            Double onwardPrice = Double.parseDouble(onwardFareInfo.getPrice());
            Double returnPrice = Double.parseDouble(returnFareInfo.getPrice());
            Double roundtripPrice = Double.parseDouble(roundtripFareInfo.getPrice());
            if ((onwardPrice + returnPrice) > roundtripPrice) {
                cheapestRoundtripFareInfo.setPrice(String.valueOf(roundtripPrice));
                cheapestRoundtripFareInfo.setCachedTime(roundtripFareInfo.getCachedTime());
            }  else {
                cheapestRoundtripFareInfo.setPrice(String.valueOf(onwardPrice + returnPrice));
                Long cacheTime = onwardFareInfo.getCachedTime() < returnFareInfo.getCachedTime() ? onwardFareInfo.getCachedTime() : returnFareInfo.getCachedTime();
                cheapestRoundtripFareInfo.setCachedTime(cacheTime);
            }
        } else {
            Double onwardPrice = Double.parseDouble(onwardFareInfo.getPrice());
            Double returnPrice = Double.parseDouble(returnFareInfo.getPrice());
            cheapestRoundtripFareInfo.setPrice(String.valueOf(onwardPrice + returnPrice));
            Long cacheTime = onwardFareInfo.getCachedTime() < returnFareInfo.getCachedTime() ? onwardFareInfo.getCachedTime() : returnFareInfo.getCachedTime();
            cheapestRoundtripFareInfo.setCachedTime(cacheTime);
        }
        return cheapestRoundtripFareInfo;
    }

    private void retainCheapest(Map<String, Map<String, List<CalendarFareInfo>>> calendarJson) {
        if (calendarJson != null) {
            for (Map<String, List<CalendarFareInfo>> jsonMap : calendarJson.values()) {
                retainCheapestFareInfo(jsonMap);
            }
        }
    }

    private void fillEmptyDates(Map<Date, Object> originalCalendarData, Map<Date, Object> extraCalendarData, Boolean isRoundtrip) {
        if (originalCalendarData != null && extraCalendarData != null) {
            for (Date date : originalCalendarData.keySet()) {
                if (isRoundtrip) {
                    Map<Date, List<CalendarFareInfo>> returnDates = (Map<Date, List<CalendarFareInfo>>) originalCalendarData.get(date);
                    Map<Date, List<CalendarFareInfo>> returnDatesExtras = (Map<Date, List<CalendarFareInfo>>) extraCalendarData.get(date);
                    for (Date returnDate : returnDates.keySet()) {
                        if (returnDates.get(returnDate) == null || returnDates.get(returnDate).size() < 1) {
                            if (returnDatesExtras.get(returnDate) != null && returnDatesExtras.get(returnDate).size() > 0) {
                                returnDates.put(returnDate, returnDatesExtras.get(returnDate));
                            }
                        }
                    }

                } else {
                    if (originalCalendarData.get(date) == null || ((List<CalendarFareInfo>) originalCalendarData.get(date)).size() < 1) {
                        if (extraCalendarData.get(date) != null && ((List<CalendarFareInfo>) extraCalendarData.get(date)).size() > 0) {
                            originalCalendarData.put(date, extraCalendarData.get(date));
                        }
                    }
                }
            }
        }
    }
    /**
     * @param from departureStation
     * @param to arrivalStation
     * @param startDate calendarStartDate
     * @param endDate calendarEndDate
     * @param countryCode sellingCountry
     * @param cheapestOnly true if only cheapest fare per day need else all fares of the day will be given
     * @param sourceType channel
     * @param returnAfterList null for OneWay and day-gap between journey for Roundtrip search(list but should contain only one value).
     * @return Map of calendarInfo
     */
    protected Map<String, Map<String, List<CalendarFareInfo>>> getCalendarDataV2(String from, String to, String startDate, String endDate, String countryCode,
            boolean cheapestOnly, String sourceType, List<Integer> returnAfterList) {
        Map<String, Map<String, List<CalendarFareInfo>>> finalResponse = new HashMap<String, Map<String, List<CalendarFareInfo>>>();
        // do oneway search
        Map<Date, Object> onwardCalendarData = getCalendarData(from, to, startDate, endDate, null, countryCode, sourceType);
        //fill empty data with b2c data
        if ("mobile".equalsIgnoreCase(sourceType)) {
            Map<Date, Object> b2CcalendarData = getCalendarData(from, to, startDate, endDate, null, countryCode, "B2C");
            fillEmptyDates(onwardCalendarData, b2CcalendarData, false);
        }
        Map<String, List<CalendarFareInfo>> onwardJsonMap = convertToJsonMap(onwardCalendarData, false);

        sortByPrice(onwardJsonMap);
        if (cheapestOnly) {
            retainCheapestFareInfo(onwardJsonMap);
        }
        finalResponse.put(ONWARD, onwardJsonMap);
        Map<Date, Object> returnCalendatData = null;
        Map<Date, Object> roundTripCalendarData = null;
        if (returnAfterList != null && returnAfterList.size() > 0) {
            // do return search
            String returnStartDate = getReturnSearchDate(startDate, returnAfterList);
            String returnEndDate = getReturnSearchDate(endDate, returnAfterList);
            returnCalendatData = getCalendarData(to, from, returnStartDate, returnEndDate, null, countryCode, sourceType);
            if ("mobile".equalsIgnoreCase(sourceType)) {
                Map<Date, Object> b2CcalendarData = getCalendarData(to, from, startDate, endDate, null, countryCode, "B2C");
                fillEmptyDates(returnCalendatData, b2CcalendarData, false);
            }
            Map<String, List<CalendarFareInfo>> returnJsonMap = convertToJsonMap(returnCalendatData, false);

            sortByPrice(returnJsonMap);
            if (cheapestOnly) {
                retainCheapestFareInfo(returnJsonMap);
            }
            finalResponse.put(RETURN, returnJsonMap);

            // do roundtrip search
            roundTripCalendarData = getCalendarData(from, to, startDate, endDate, returnAfterList, countryCode, sourceType);
            if ("mobile".equalsIgnoreCase(sourceType)) {
                Map<Date, Object> b2CcalendarData = getCalendarData(from, to, startDate, endDate, returnAfterList, countryCode, "B2C");
                fillEmptyDates(roundTripCalendarData, b2CcalendarData, true);
            }
            Map<String, List<CalendarFareInfo>> roundtripJsonMap = convertToJsonMap(roundTripCalendarData, true);

            sortByPrice(roundtripJsonMap);
            if (cheapestOnly) {
                retainCheapestFareInfo(roundtripJsonMap);
            }
            finalResponse.put(ROUNDTRIP, roundtripJsonMap);

//            responseJson = convertTojson(finalResponse);
        }
        return finalResponse;
    }
    
    protected Map<String, List<CalendarFareInfo>> getCalendarDataV3(String from, String to, String startDate, String endDate, String countryCode,
            boolean cheapestOnly, String sourceType, List<Integer> returnAfterList){
    	Map<String, List<CalendarFareInfo>> finalResponse = new HashMap<>();
    	Map<Date, Object> onwardCalendarData = null;
    	Map<Date, Object> returnCalendarData = null;
    	Map<Date, Object> roundTripCalendarData = null;
    	Map<String, List<CalendarFareInfo>> onwardJsonMap = null;
    	Map<String, List<CalendarFareInfo>> returnJsonMap = null;
    	Map<String, List<CalendarFareInfo>> roundTripJsonMap = null;
    	//Check for RT request
    	if(returnAfterList != null && returnAfterList.size() > 0){
    		String returnStartDate = getReturnSearchDate(startDate, returnAfterList);
            String returnEndDate = getReturnSearchDate(endDate, returnAfterList);
    		//Three calls for RT request.
            onwardCalendarData = getCalendarData(from, to, startDate, endDate, null, countryCode, sourceType);
            returnCalendarData = getCalendarData(to, from, returnStartDate, returnEndDate, null, countryCode, sourceType);
            roundTripCalendarData = getCalendarData(from, to, startDate, endDate, returnAfterList, countryCode, sourceType);
            //Filling empty dates with B2C date for specific source types.
            if("mobile".equalsIgnoreCase(sourceType) || "api".equalsIgnoreCase(sourceType)){
            	fillEmptyDates(onwardCalendarData, getCalendarData(from, to, startDate, endDate, null, countryCode, "B2C"), false);
            	fillEmptyDates(returnCalendarData, getCalendarData(to, from, returnStartDate, returnEndDate, null, countryCode, "B2C"), false);
            	fillEmptyDates(roundTripCalendarData, getCalendarData(from, to, startDate, endDate, returnAfterList, countryCode, "B2C"), true);
            }
            onwardJsonMap = convertToJsonMap(onwardCalendarData,false);    
            returnJsonMap = convertToJsonMap(returnCalendarData, false);
            roundTripJsonMap = convertToJsonMap(roundTripCalendarData, true);
            finalResponse = getAirlinewiseCheapestRoundtripCombination(onwardJsonMap, returnJsonMap, roundTripJsonMap, returnAfterList);
    	} else {
    		finalResponse = convertToJsonMap(getCalendarData(from, to, startDate, endDate, null, countryCode, sourceType),false);
    	}
    	sortByPrice(finalResponse);
    	return finalResponse;
    }

    private String getReturnSearchDate(String startDate, List<Integer> returnAfterList) {
        SimpleDateFormat format = getFormat();
        String dateString = null;
        try {
            Date date = format.parse(startDate);
            date = DateUtils.addDays(date, returnAfterList.iterator().next());
            dateString = format.format(date);
        } catch (ParseException e) {
            dateString = startDate;
        }
        return dateString;
    }

    private void sortByPrice(Map<String, List<CalendarFareInfo>> jsonMap) {
        if (jsonMap != null && jsonMap.size() > 0) {
            for (List<CalendarFareInfo> fareInfos : jsonMap.values()) {
                sortByPrice(fareInfos);
            }
        }
    }

    private void sortByPrice(List<CalendarFareInfo> fareInfos) {
        if (fareInfos != null && fareInfos.size() > 0) {
            Collections.sort(fareInfos, new Comparator<CalendarFareInfo>() {

                @Override
                public int compare(CalendarFareInfo o1, CalendarFareInfo o2) {
                    Double price1 = getPrice(o1);
                    Double price2 = getPrice(o2);

                    return price1.compareTo(price2);
                }
            });
        }
    }
    private Double getPrice(CalendarFareInfo fareInfo) {
        Double tempPrice = 99999999.99;
        try {
            tempPrice = Double.valueOf(fareInfo.getPrice());
        } catch (Exception e) {
            tempPrice = 99999999.99;
        }
        return tempPrice;
    }
    private void retainCheapestFareInfo(Map<String, List<CalendarFareInfo>> jsonMap) {
        if (jsonMap != null && jsonMap.size() > 0) {
            for (List<CalendarFareInfo> fareInfos : jsonMap.values()) {
                if (fareInfos != null && fareInfos.size() > 0) {
                    CalendarFareInfo tempFareInfo = fareInfos.get(0);
                    fareInfos.clear();
                    fareInfos.add(tempFareInfo);
                }
            }
        }
    }

    public Map<Date, Object> getCalendarData(String from, String to, String startDate, String endDate, List<Integer> returnAfterList, String countryCode, String source) {
        // TODO

        Map<Date, Object> datesToBeRetrieved = null;
        // NEXT STEP : Create the search keys.
        try {
            datesToBeRetrieved = getDatesToBeQueried(startDate, endDate, returnAfterList);

            // NEXT STEP : Create the Key Array From the Map (datesToBeRetrieved).
            String[] keyArray = convertToKeyArrayFromMap(datesToBeRetrieved, (returnAfterList != null && returnAfterList.size() > 0));

            // NEXT STEP : Add "From" & "To" to the keys
            if (source == null) {
                source = "B2C";
            }
            getCacheKey(keyArray, from, to, source);

            // NEXT STEP : Handle Country Code.
            handleCountryCode(keyArray, countryCode);

            // NEXT STEP : Retrieve from cache.
            Map<String, String> cacheOutput = retrieveFromCache(keyArray);

            // NEXT STEP : Convert to Object Structure (CalendarFareInfo)
            convertToObjectStructure(datesToBeRetrieved, cacheOutput, countryCode, returnAfterList);

            // NEXT STEP : Remove flights already departed
            datesToBeRetrieved = removeFlightsAlreadyDeparted(datesToBeRetrieved, (returnAfterList != null && returnAfterList.size() > 0));

            // NEXT STEP : Retain cheaper flights for international roundtrip BY AIRLINE.
            if (returnAfterList != null && returnAfterList.size() > 0) {
                datesToBeRetrieved = filterCheaperRoundtripFlights(datesToBeRetrieved);
            }

        } catch (ParseException e) {
            logger.error("Parse Exception  : ", e);
        }

        return datesToBeRetrieved;
    }

    public List<Integer> getReturnAfterList(String returnAfterString) {
        List<Integer> returnAfterList = null;
        if (StringUtils.isNotBlank(returnAfterString)) {
            returnAfterList = new ArrayList<Integer>();

            if (returnAfterString.contains(",")) {
                int firstArrayElement = Integer.parseInt(StringUtils.trim(returnAfterString.split(",")[0]));
                int lastArrayElement = Integer.parseInt(StringUtils.trim(returnAfterString.split(",")[1]));

                for (int i = firstArrayElement; i <= lastArrayElement; i++) {
                    returnAfterList.add(i);
                }

            } else {
                int returnAfter = Integer.parseInt(returnAfterString);
                returnAfterList.add(returnAfter);
            }
        }

        return returnAfterList;
    }

    /**
     * Format for startDateString and endDateString is yyyyMMdd e.g. 20110601 -> June 1 2011
     * This Method returns a Map<Dat,List<Date>>. KEY -> Onward Date. VALUE -> A List. The List will contain the all return dates for the given onward date (KEY).
     * @param startDateString start date
     * @param endDateString last date
     * @param returnAfterList return date difference
     * @return Map<Date, Date> date vlaues
     * @throws ParseException datepaser
     */
    public Map<Date, Object> getDatesToBeQueried(String startDateString, String endDateString, List<Integer> returnAfterList) throws ParseException {
        SimpleDateFormat format = getFormat();
        Map<Date, Object> dates = null;
        Calendar today = Calendar.getInstance();
        Date startDate = null;
        Date endDate = null;
        if (StringUtils.isNotBlank(startDateString)) {
            startDate = format.parse(startDateString);
            if (StringUtils.isNotBlank(endDateString)) {
                endDate = format.parse(endDateString);
            } else {
                // If end date is not specified then we look only 365 days ahead.
                endDate = DateUtils.addDays(startDate, 365);
            }
            dates = new LinkedHashMap<Date, Object>();
            for (Date d = startDate; d.getTime() <= endDate.getTime(); d = DateUtils.addDays(d, 1)) {
                Map<Date, List<CalendarFareInfo>> returnDates = new LinkedHashMap<Date, List<CalendarFareInfo>>();
                if (returnAfterList != null && returnAfterList.size() > 0) {
                    for (Integer returnAfter : returnAfterList) {
                        returnDates.put(DateUtils.addDays(d, returnAfter), null);
                    }
                    dates.put(d, returnDates);
                } else {
                    dates.put(d, null);
                }
            }
        }
        return dates;
    }

    public String[] convertToKeyArrayFromMap(Map<Date, Object> datesToBeRetrieved, boolean isRoundtrip) {
        List<String> keyArray = new ArrayList<String>();
        SimpleDateFormat format = getFormat();
        for (Entry<Date, Object> entry : datesToBeRetrieved.entrySet()) {

            // supports only 1000 entries. TO-DO: need to investigate more.
            if (keyArray.size() >= 1000) {
                break;
            }

            Date onwardDate = entry.getKey();
            if (entry.getValue() != null && isRoundtrip) {
                for (Entry<Date, List<CalendarFareInfo>> entry2 : ((Map<Date, List<CalendarFareInfo>>) entry.getValue()).entrySet()) {
                    Date returnDate = entry2.getKey();
                    String key = format.format(onwardDate) + "_" + format.format(returnDate);
                    keyArray.add(key);
                }
            } else {
                String key = format.format(onwardDate);
                keyArray.add(key);
            }
        }

        String[] keys = new String[keyArray.size()];
        keyArray.toArray(keys);
        return keys;
    }

    public void getCacheKey(String[] keyArray, String from, String to, String source) {
        if (StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to) && StringUtils.isNotBlank(source)) {
            for (int i = 0; i < keyArray.length; i++) {
                keyArray[i] = from + "_" + to + "_" + source + "_" + keyArray[i];
            }
        }
    }

    /**
     * The country Code is not PREPENDED only for "IN". For all others the countryCode is prepended in the keys. The check for country code = "IN" is hardcoded here. This can be changed to make use of
     * Currency (Object) if we later decide to change the key to include the currency (e.g INR, AED) instead on country code in the key.
     * @param keyArray keylist
     * @param countryCode countrycode(eg. IN)
     */
    public void handleCountryCode(String[] keyArray, String countryCode) {

        // TO SUPPORT INTERNATIONALIZATION. KEYS ARE DIFFERENT FOR DUBAI.
        if (StringUtils.isNotBlank(countryCode) && !countryCode.equalsIgnoreCase("IN")) {
            for (int i = 0; i < keyArray.length; i++) {
                keyArray[i] = countryCode + "_" + keyArray[i];
            }

        }
    }

    public Map<String, String> retrieveFromCache(String[] keys) {
        Map<String, String> jsonMap = (Map) cache.getMulti(keys);
        return jsonMap;
    }

    public void convertToObjectStructure(Map<Date, Object> datesToBeRetrieved, Map<String, String> cacheOutput, String countryCode, List<Integer> returnAfterList) throws ParseException {

        if (cacheOutput != null) {
            SimpleDateFormat format = getFormat();
            for (Entry<String, String> entry : cacheOutput.entrySet()) {
                // Break Down the Key into countryCode , from, to, departDate , returnDate
                String[] tokens = entry.getKey().split("_");
                String from = null;
                String to = null;
                String source = null;
                Date onwardDate = null;
                Date returnDate = null;

                try {
                    if (countryCode.equals("IN")) {
                        from = tokens[0];
                        to = tokens[1];
                        source = tokens[2];
                        onwardDate = format.parse(tokens[3]);
                        if (returnAfterList != null && returnAfterList.size() > 0) {
                            returnDate = format.parse(tokens[4]);
                        }
                    } else {
                        from = tokens[1];
                        to = tokens[2];
                        source = tokens[3];
                        onwardDate = format.parse(tokens[4]);
                        if (returnAfterList != null && returnAfterList.size() > 0) {
                            returnDate = format.parse(tokens[5]);
                        }
                    }

                    // Populate the data structure datesToBeRetrieved
                    List<Object> list = new ArrayList<Object>();
                    list = (List<Object>) APIUtil.deserializeJsonToObject(entry.getValue(), list);

                    List<CalendarFareInfo> fareInfos = new ArrayList<CalendarFareInfo>();
                    for (Object s : list) {
                        CalendarFareInfo fareInfo = new CalendarFareInfo();
                        fareInfo = (CalendarFareInfo) APIUtil.deserializeJsonToObject(APIUtil.serializeObjectToJson(s), fareInfo);
                        fareInfos.add(fareInfo);
                    }

                    if (returnDate != null) {
                        // If Return Date is NOT NULL, then its a rundtrip fare search and the value will contain a Map<Date, List<CalendarFareInfo>>
                        if (datesToBeRetrieved.get(onwardDate) != null) {
                            ((Map<Date, Object>) datesToBeRetrieved.get(onwardDate)).put(returnDate, fareInfos);
                        }
                    } else {
                        // If Return Date is NULL, then its a one way search and the value will contain a List<CalendarFareInfo>
                        datesToBeRetrieved.put(onwardDate, fareInfos);
                    }
                } catch (NumberFormatException e) {
                    // Stay Silent
                    int a = 0; // dummy vlaue for checkstyle
                }
            }
        }
    }

    public Map<Date, Object> removeFlightsAlreadyDeparted(Map<Date, Object> datesToBeRetrieved, boolean isRoundtrip) {
        Calendar today = Calendar.getInstance();
        int currentHour = today.get(Calendar.HOUR_OF_DAY);
        int currentMinute = today.get(Calendar.MINUTE);

        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        Map<Date, Object> filteredData = (Map<Date, Object>) GenUtil.deepCopy((Serializable) datesToBeRetrieved);
        for (Entry<Date, Object> entry : datesToBeRetrieved.entrySet()) {
            Date onwardDate = entry.getKey();

            if (onwardDate.getTime() == today.getTimeInMillis()) {
                if (entry.getValue() != null && isRoundtrip) {
                    for (Entry<Date, List<CalendarFareInfo>> entry2 : ((Map<Date, List<CalendarFareInfo>>) entry.getValue()).entrySet()) {
                        Date returnDate = entry2.getKey();
                        List<CalendarFareInfo> fareInfos = (List<CalendarFareInfo>) entry2.getValue();
                        List<CalendarFareInfo> newFareInfos = new ArrayList<CalendarFareInfo>();
                        if (fareInfos != null) {
                            for (CalendarFareInfo fareInfo : fareInfos) {
                                // Look at Onward Departure Time.
                                int departHour = Integer.parseInt(fareInfo.getOnwardDepartureTime().split(":")[0]);
                                int departMinute = Integer.parseInt(fareInfo.getOnwardDepartureTime().split(":")[1]);

                                if (departHour > currentHour || (departHour == currentHour && departMinute > currentMinute)) {
                                    newFareInfos.add(fareInfo);
                                }
                            }
                        }

                        ((Map<Date, List<CalendarFareInfo>>) filteredData.get(onwardDate)).put(returnDate, newFareInfos);
                    }
                } else {
                    List<CalendarFareInfo> fareInfos = (List<CalendarFareInfo>) entry.getValue();
                    List<CalendarFareInfo> newFareInfos = new ArrayList<CalendarFareInfo>();
                    if (fareInfos != null) {
                        for (CalendarFareInfo fareInfo : fareInfos) {
                            // Look at Departure Time.
                            int departHour = Integer.parseInt(fareInfo.getDepartureTime().split(":")[0]);
                            int departMinute = Integer.parseInt(fareInfo.getDepartureTime().split(":")[1]);

                            if (departHour > currentHour || (departHour == currentHour && departMinute > currentMinute)) {
                                newFareInfos.add(fareInfo);
                            }
                        }
                    }

                    filteredData.put(onwardDate, newFareInfos);
                }
            } else {
                Object value = entry.getValue();
                filteredData.put(onwardDate, value);
            }

        }

        return filteredData;
    }

    public Map<Date, Object> filterCheaperRoundtripFlights(Map<Date, Object> datesToBeRetrieved) {
        Map<Date, Object> filteredData = (Map<Date, Object>) GenUtil.deepCopy((Serializable) datesToBeRetrieved);

        // Map used to filter and retain cheaper fights from flights from different dates.
        Map<String, Map<Date, List<CalendarFareInfo>>> filterMap = null;

        for (Entry<Date, Object> entry : datesToBeRetrieved.entrySet()) {
            Date onwardDate = entry.getKey();

            filterMap = new LinkedHashMap<String, Map<Date, List<CalendarFareInfo>>>();
            for (Entry<Date, List<CalendarFareInfo>> entry2 : ((Map<Date, List<CalendarFareInfo>>) entry.getValue()).entrySet()) {
                Date returnDate = entry2.getKey();
                List<CalendarFareInfo> fareInfos = (List<CalendarFareInfo>) entry2.getValue();
                if (fareInfos != null) {
                    for (CalendarFareInfo fareInfo : fareInfos) {
                        // Look at Onward Airline and Return Airline combination.

                        String airlineCombi = fareInfo.getOnwardAirline() + "-" + fareInfo.getReturnAirline();

                        if (filterMap.get(airlineCombi) != null) {
                            // An airline combination for this is already present. So we will check the existing fare for all dates and either ignore this fare or add this fare and delete old fare.
                            Map<Date, List<CalendarFareInfo>> returnMap = filterMap.get(airlineCombi);

                            boolean isNewCheaper = false;
                            for (Entry<Date, List<CalendarFareInfo>> entry3 : returnMap.entrySet()) {
                                List<CalendarFareInfo> oldFareInfos = entry3.getValue();
                                for (CalendarFareInfo oldFareInfo : oldFareInfos) {
                                    if (Double.parseDouble(fareInfo.getPrice()) < Double.parseDouble(oldFareInfo.getPrice())) {
                                        isNewCheaper = true;
                                        break;
                                    }
                                }
                            }

                            if (isNewCheaper) {
                                returnMap = new LinkedHashMap<Date, List<CalendarFareInfo>>();
                                List<CalendarFareInfo> newFareInfos = new ArrayList<CalendarFareInfo>();

                                newFareInfos.add(fareInfo);
                                returnMap.put(returnDate, newFareInfos);
                                filterMap.put(airlineCombi, returnMap);
                            }

                        } else {
                            // An airline combination for this is not present. So we will add this fare.
                            Map<Date, List<CalendarFareInfo>> returnMap = new LinkedHashMap<Date, List<CalendarFareInfo>>();
                            List<CalendarFareInfo> newFareInfos = new ArrayList<CalendarFareInfo>();

                            newFareInfos.add(fareInfo);
                            returnMap.put(returnDate, newFareInfos);
                            filterMap.put(airlineCombi, returnMap);
                        }
                    }
                }
            }

            // CLEAR ALL FARES.
            Map<Date, List<CalendarFareInfo>> returnMap = (Map<Date, List<CalendarFareInfo>>) filteredData.get(onwardDate);
            for (Entry<Date, List<CalendarFareInfo>> entry2 : returnMap.entrySet()) {
                List<CalendarFareInfo> value = new ArrayList<CalendarFareInfo>();
                entry2.setValue(value);
            }

            // ADD CHEAPER FARES ONLY BY AIRLINE.
            for (Entry<String, Map<Date, List<CalendarFareInfo>>> filterEntry : filterMap.entrySet()) {
                String airlineCombi = filterEntry.getKey();
                for (Entry<Date, List<CalendarFareInfo>> entry2 : filterEntry.getValue().entrySet()) {
                    ((Map<Date, List<CalendarFareInfo>>) filteredData.get(onwardDate)).get(entry2.getKey()).addAll(entry2.getValue());
                }
            }

        }

        return filteredData;
    }

    public Map<String, List<CalendarFareInfo>> convertToJsonMap(Map<Date, Object> datesToBeRetrieved, boolean isRoundtrip) {
        Map<String, List<CalendarFareInfo>> jsonMap = new LinkedHashMap<String, List<CalendarFareInfo>>();
        SimpleDateFormat format = getFormat();
        for (Entry<Date, Object> entry : datesToBeRetrieved.entrySet()) {
            Date onwardDate = entry.getKey();
            String key = format.format(onwardDate);
            List<CalendarFareInfo> value = new ArrayList<CalendarFareInfo>();
            if (isRoundtrip) {
                for (Entry<Date, List<CalendarFareInfo>> entry2 : ((Map<Date, List<CalendarFareInfo>>) entry.getValue()).entrySet()) {
                    if (entry2.getValue() != null) {
                        value.addAll(entry2.getValue());
                    }
                }
            } else {
                if (entry.getValue() != null) {
                    value.addAll((List<CalendarFareInfo>) entry.getValue());
                }
            }

            jsonMap.put(key, value);
        }

        return jsonMap;
    }

    public String convertTojson(Map jsonMap) {
        String json = APIUtil.serializeObjectToJson(jsonMap);
        return json;
    }

    public int getDaysInMonth(int year, int month) {
        switch (month) {
        case 2:
            if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
                return 29;
            } else {
                return 28;
            }
        case 4:
            return 30;
        case 6:
            return 30;
        case 9:
            return 30;
        case 11:
            return 30;
        default:
            return 31;
        }
    }

    @Override
    public String getHotelCalendarJson(String city, String state, String country, String startDate, String endDate, String sellingCurrency) {
        String hotelCalendarJson = null;

        String[] keyArray = new String[1000];
        String[] requestKeys = new String[1000];
        String[] dateArray = new String[1000];
        String[] jsonString = new String[1000];

        String dtvalue = null;
        int dtvalueHour;
        int dtvalueMinute;

        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        int currentMonth = cal.get(Calendar.MONTH) + 1;
        int currentDay = cal.get(Calendar.DAY_OF_MONTH);
        int currentHour = cal.get(Calendar.HOUR_OF_DAY);
        int currentMinute = cal.get(Calendar.MINUTE);

        JsonFactory f = new JsonFactory();
        JsonParser jp;

        int startDay = Integer.parseInt(startDate.substring(6, 8));
        int startMonth = Integer.parseInt(startDate.substring(4, 6));
        int startYear = Integer.parseInt(startDate.substring(0, 4));
        int counter = 0;
        for (int i = 0; i < 1000; i++) {
            int days = getDaysInMonth(startYear, (startMonth - 1));
            if ((days == 28 && startDay == 29) || (days == 29 && startDay == 30) || (days == 30 && startDay == 31)) {
                if (startDay == 29 || startDay == 30 || startDay == 31) {
                    startDay = 01;
                    startMonth = startMonth + 1;
                }
            }
            if (startDay == 32) {
                startDay = 01;
                startMonth = startMonth + 1;
            }
            if (startMonth == 13) {
                startMonth = 01;
                startYear = startYear + 1;
            }

            String day = Integer.toString(startDay);
            String month = Integer.toString(startMonth);
            String year = Integer.toString(startYear);

            if (day.length() == 1) {
                day = "0" + day;
            }
            if (month.length() == 1) {
                month = "0" + month;
            }

            dateArray[i] = year + month + day;

            keyArray[i] = city + "_" + state + "_" + country + "_" + year + month + day;

            // TO SUPPORT INTERNATIONALIZATION. KEYS ARE DIFFERENT FOR DIFFERENT CURRENCIES.
            if (org.apache.commons.lang.StringUtils.isNotBlank(sellingCurrency)) {
                keyArray[i] = sellingCurrency + "_" + keyArray[i];
            } else {
                keyArray[i] = "INR" + "_" + keyArray[i];
            }
            keyArray[i] = keyArray[i].toLowerCase();

            startDay = startDay + 1;

            if (dateArray[i].equals(endDate)) {
                i = 1000;
            }
            counter++;
        }

        int keyCounter = 0;
        for (int j = 0; j < counter; j++) {
            for (int starRating = 0; starRating < 6; starRating++) {
                requestKeys[keyCounter++] = keyArray[j] + "_" + starRating;
            }
        }

        Map<String, String> jsonMap = (Map) cache.getMulti(requestKeys);
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        StringWriter sw = new StringWriter();
        Map<String, List<Map<String, String>>> jsonData = new HashMap<String, List<Map<String, String>>>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (Entry<String, String> entry : jsonMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String[] splitKey = key.split("_");

            List list = jsonData.get(splitKey[4]);
            if (list == null) {
                list = new ArrayList<String>();
                jsonData.put(splitKey[4], list);
            }

            Map<String, Object> map = null;
            try {
                map = (Map<String, Object>) mapper.readValue(value, Map.class);
                for (Entry<String, Object> e : map.entrySet()) {
                    if (e.getKey().equalsIgnoreCase("date")) {
                        Long d = (Long) e.getValue();
                        map.put(e.getKey(), sdf.format(d));
                    }
                }
            } catch (Exception e) {
                logger.error("Exception is:" + e.getMessage(), e);
            }

            list.add(map);
            // jsonData.put( splitKey[4] , value);
        }

        try {
            mapper.writeValue(sw, jsonData);
        } catch (Exception e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        return sw.toString();
    }

    /* (non-Javadoc)
     * @see com.cleartrip.common.service.CalendarSearchService#insertCalendarData(java.lang.String, java.lang.String, int)
     */
    @Override
    public void insertCalendarData(String key, String value, int expiryTime) {
        cache.syncPut(key, value, expiryTime);
    }

    /**
     * @return the format
     */
    public SimpleDateFormat getFormat() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        return format;
    }

}
