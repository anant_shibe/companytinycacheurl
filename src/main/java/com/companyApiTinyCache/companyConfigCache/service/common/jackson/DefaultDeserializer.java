package com.companyApiTinyCache.companyConfigCache.service.common.jackson;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.UntypedObjectDeserializer;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * DefaultDeserializer.
 */
@ClassExcludeCoverage
public class DefaultDeserializer extends UntypedObjectDeserializer {

    @Override
    public Object deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        if (jp.getCurrentToken() == JsonToken.START_ARRAY) {
            // for faster lookup during rule evaluation, HashSet is preferred over (default implementation by Jackson using) ArrayList
            return mapArrayToSet(jp, ctxt);
        } else {
            return super.deserialize(jp, ctxt);
        }
    }

    protected Set<Object> mapArrayToSet(JsonParser jp, DeserializationContext ctxt) throws IOException {
        Set<Object> result = new LinkedHashSet<Object>();
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            result.add(deserialize(jp, ctxt));
        }
        return result;
    }
}
