package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * EsLoggingQueue.
 * @author vijay
 */
@ClassExcludeCoverage
public class EsLoggingQueue {
    private static BlockingQueue<Map<String, Object>> queue = new ArrayBlockingQueue<Map<String, Object>>(200);
    private static final Log LOG = LogFactory.getLog(EsLoggingQueue.class);

    public EsLoggingQueue(CachedProperties commonCachedProperties) {
        init(commonCachedProperties);
    }

    /**
     * In case you wondering why this in public, stupid checkstyle doesn't allow me to checkin if I keep this method private, that's why its public.
     * @param commonCachedProperties properties.
     */
    public void init(CachedProperties commonCachedProperties) {
        LOG.debug("Initilazing EsLoggerDequeuer...");
        EsLoggerDequeuer esLoggerDequeuer = new EsLoggerDequeuer(queue, commonCachedProperties);
        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(esLoggerDequeuer);
    }

    public static void add(Map<String, Object> document) {
        if (!queue.offer(document)) {
            LOG.warn("Droping Message from Elastic Search Logging " + document);
        }
    }

    public static void addAll(List<Map<String, Object>> documents) {
        for (Map<String, Object> document : documents) {
            if (!queue.offer(document)) {
                LOG.warn("Droping Message from Elastic Search Logging " + document);
            }
        }
    }
}
