package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import org.apache.commons.lang.StringUtils;

import java.util.concurrent.locks.ReentrantLock;

/**
 * A class to create an Object from commonCachedProperties and change it only when the property has changed.
 *
 * ConfigManager is a simpler way to do this though it reads a Config object every time CachedProperties are
 * re-loaded.
 *
 * @author suresh
 *
 * @param <R>
 */
@ClassExcludeCoverage
public class ObjectFromProperty<R> {

    protected CachedProperties cachedProperties;

    protected String propertyName;

    protected volatile R object;

    protected volatile String propertyValue;

    private StringResourceLoader<R> stringResourceLoader;

    private ReentrantLock reentrantLock;

    protected void updateObject(String newPropertyValue) {
        if (stringResourceLoader == null) {
            throw new IllegalStateException("Either pass a non null StringResourceLoader or override ObjectFromProperty.updateObject");
        }
        propertyValue = newPropertyValue;
        R newObject = stringResourceLoader.loadResource(newPropertyValue);
        object = newObject;
    }

    public R checkAndGetObject() {
        String newPropertyValue = cachedProperties.getPropertyValue(propertyName);
        if (newPropertyValue != null) {
            newPropertyValue = newPropertyValue.trim();
        }
        if (!StringUtils.equals(newPropertyValue, propertyValue)) {
            // Using reentrant lock instead of synchronized to avoid waiting for
            // another thread loading the object
            if (reentrantLock.tryLock()) {
                try {
                    if (!StringUtils.equals(newPropertyValue, propertyValue)) {
                        updateObject(newPropertyValue);
                        propertyValue = newPropertyValue;
                    }
                } finally {
                    reentrantLock.unlock();
                }
            }
        }

        return object;
    }

    public ObjectFromProperty(CachedProperties pcachedProperties, String ppropertyName, StringResourceLoader<R> pstringResourceLoader, boolean loadInConstructor) {
        cachedProperties = pcachedProperties;
        propertyName = ppropertyName;
        stringResourceLoader = pstringResourceLoader;
        reentrantLock = new ReentrantLock();
        if (loadInConstructor) {
            checkAndGetObject();
        }
    }

    public ObjectFromProperty(CachedProperties pcachedProperties, String ppropertyName, boolean loadInConstructor) {
        this(pcachedProperties, ppropertyName, null, loadInConstructor);
    }

    /**
     * Getter for propertyName.
     *
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }
}
