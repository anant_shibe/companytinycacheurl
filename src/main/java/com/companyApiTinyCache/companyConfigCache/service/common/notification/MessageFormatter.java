/**
 * Created on Jan 6, 2008
 */
package com.companyApiTinyCache.companyConfigCache.service.common.notification;

import java.util.Map;

/**
 * Interface for classes capable of formatting the messages.
 * 
 * @author sanjeev desai
 * 
 *         Copyright 2008 Cleartrip Travel Services, Inc. All rights reserved. CLEARTRIP PROPRIETART/CONFIDENTIAL
 */
public interface MessageFormatter {

    /**
     * Replaces the substitution variables in the unformatted string with the values available in the map. e.g. unformattedStringMessage :- $name, Welcome to $companyname values :-
     * name=Sanjeev;companyname=Cleartrip
     * 
     * The formatted string will look like: Sanjeev, Welcome to Cleartrip
     * 
     * 
     * @param unformattedStringMessage
     * @param values
     *            Map<String, String>
     * 
     * @return the formatted string
     */
    String formatMessage(String unformattedStringMessage, Map<String, String> values) throws MessageFormatException;
}
