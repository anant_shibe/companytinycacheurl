/* 
 * @(#) LRUCache.java
 * 
 * Copyright 2010-2011 by Cleartrip Travel Services Pvt. Ltd.
 * Unit No 001, Ground Floor, DTC Bldg, 
 * Sitaram Mills Compound, N.M. Joshi Marg, 
 * Delisle Road, 
 * Lower Parel (E) 
 * Mumbai - 400011
 * India
 * 
 * This software is the confidential and proprietary information 
 * of Cleartrip Travel Services Pvt. Ltd. ("Confidential Information"). 
 * You shall not disclose such Confidential Information and shall 
 * use it only in accordance with the terms of license agreement you 
 * entered into with Cleartrip Travel Service Pvt. Ltd.
 */
package com.companyApiTinyCache.companyConfigCache.service.impl;

import org.apache.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Implementation of Least Recently Used cache. The cache holds no more than `maxSize` number of elements at any point of time. If additional elements are added once maximum-capacity is reached, the
 * LRU entry is dropped.
 * 
 * @author Amit Kumar Sharma
 * @param <T>
 *            The type of objects LRUCache will cache.
 */
public class LRUCache<T> extends LinkedHashMap<String, T> {

    private static final long serialVersionUID = 4617151061372020231L;
    private static final Logger logger = Logger.getLogger(LRUCache.class);

    private int maxSize;

    public LRUCache(int maxSize) {
        super(maxSize, 1.1f, true);
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<String, T> eldest) {
        if (size() > maxSize) {
            logger.debug("Approving removal of key=" + eldest.getKey() + " as maxSize is crossed.");
            return true;
        }
        return false;
    }

}
