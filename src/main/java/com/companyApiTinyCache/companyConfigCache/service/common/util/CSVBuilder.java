package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author cleartrip
 */
public class CSVBuilder {
    private static final String LINE_FEED = "\n";
    private static final char STR_DELIMITER = '"';
    private char txtDelimiter = ',';
    private boolean addDelimiter = false;
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");

    private StringBuilder sbr = new StringBuilder();
    private final NumberFormat nf;

    public CSVBuilder(String... headers) {
        nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        nf.setMaximumFractionDigits(3);
        for (String header : headers) {
            append(header);
        }
        appendLine();
    }

    public CSVBuilder(char delimiter, String... headers) {
        this.txtDelimiter = delimiter;
        nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        nf.setMaximumFractionDigits(3);
        for (String header : headers) {
            append(header);
        }
        appendLine();
    }

    public CSVBuilder append(String value) {
        if (addDelimiter) {
            sbr.append(txtDelimiter);
        }
        if (value != null) {
            sbr.append(STR_DELIMITER).append(value.replace(STR_DELIMITER, '\u201C')).append(STR_DELIMITER);
        }
        addDelimiter = true;
        return this;
    }

    public CSVBuilder append(Date value) {
        if (value == null) {
            append((String) null);
        } else {
            append(sdf.format(value));
        }
        return this;
    }

    public void appendLine() {
        sbr.append(LINE_FEED);
        addDelimiter = false;
    }

    @Override
    public String toString() {
        return sbr.toString();
    }

    public CSVBuilder append(Number value) {
        if (addDelimiter) {
            sbr.append(txtDelimiter);
        }
        if (value != null) {
            sbr.append(nf.format(value));
        }
        addDelimiter = true;
        return this;
    }
}
