package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.service.LogClickService;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogClickController extends AbstractController {

    private LogClickService logClickService;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String[] clickParams = request.getParameterValues("c");
        int i;
        if (clickParams != null) {
            for (i = 0; i < clickParams.length; i++) {
                String clickParam = StringUtils.trimToNull(clickParams[i]);
                if (clickParam != null) {
                    logClickService.logClick(clickParam);
                }
            }
        }

        response.setContentType("text/plain");
        return null;
    }

    public LogClickController() {
        setCacheSeconds(0);
    }

    /**
     * Setter for logClickService.
     * 
     * @param plogClickService
     *            the logClickService to set
     */
    public void setLogClickService(LogClickService plogClickService) {
        logClickService = plogClickService;
    }
}
