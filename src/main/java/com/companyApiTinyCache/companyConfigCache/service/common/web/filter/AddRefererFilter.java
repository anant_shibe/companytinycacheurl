package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * .
 * This filter adds the referer and other request parameters to cookie
 *
 * @author avatar
 */
@ClassExcludeCoverage
public class AddRefererFilter implements Filter {

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (!(request instanceof HttpServletRequest) || !(response instanceof HttpServletResponse)) {
            throw new ServletException("Can only process HttpServletResponse");
        }

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        Cookie cookie;
        boolean isRefererCleartrip = true;
        StringBuilder params = new StringBuilder();

        Enumeration headerNames = httpRequest.getHeaderNames();
        Map<String, String> attribuMap = new HashMap<String, String>();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            String headerValue = httpRequest.getHeader(headerName);
            if ("referer".equals(headerName.toLowerCase())) {
                String lch = headerValue.toLowerCase();
                boolean acceptHeader = true;
                int startPos = lch.indexOf("//");
                if (startPos > 0) {
                    startPos += 2;
                } else {
                    startPos = 0;
                }
                int firstSlashPos = lch.indexOf('/', startPos);
                if (firstSlashPos < 0) {
                    firstSlashPos = lch.length();
                }
                int pos = lch.indexOf(".cleartrip.");
                if (pos > 0 && pos < firstSlashPos) {
                    acceptHeader = false;
                }
                if (acceptHeader) {
                    attribuMap.put(headerName, headerValue);
                    isRefererCleartrip = false;
                }
                break;
            }
        }

        if (!isRefererCleartrip) {
            Enumeration requestParameters = request.getParameterNames();
            int i = 0;
            while (requestParameters.hasMoreElements()) {
                String key = (String) requestParameters.nextElement();
                String value = request.getParameter(key);
                if (i != 0) {
                    params.append("&");
                }
                params.append(key).append("=").append(value);
                i++;
            }
        }

        if (params.length() > 1) {
            attribuMap.put("params", params.toString());
        }

        for (String key : attribuMap.keySet()) {
            String value = attribuMap.get(key);
            if (value != null && !("".equals(value))) {
                value = URLEncoder.encode(value, "UTF-8");
                cookie = new Cookie("r_" + key, value);
                httpResponse.addCookie(cookie);
            }
        }

        chain.doFilter(httpRequest, httpResponse);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {

    }
}
