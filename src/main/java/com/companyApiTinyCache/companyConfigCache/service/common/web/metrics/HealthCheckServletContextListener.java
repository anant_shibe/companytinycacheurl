package com.companyApiTinyCache.companyConfigCache.service.common.web.metrics;

import com.companyApiTinyCache.companyConfigCache.service.common.metrics.CTHealthCheckRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.servlets.HealthCheckServlet;

public class HealthCheckServletContextListener extends HealthCheckServlet.ContextListener {

    @Override
    protected HealthCheckRegistry getHealthCheckRegistry() {
        return CTHealthCheckRegistry.getHealthCheckRegistry();
    }
}
