package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.impl;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

@ClassExcludeCoverage
public interface AMQPMessageBrokerConstatnts {
	//AMQP Connection fields
	String AMQP_HOST_KEY = "hostName";
	String AMQP_USER_NAME_KEY = "userName";
	String AMQP_PASSWORD_KEY = "password";
	String AMQP_PORT_KEY = "port";	
	String AMQP_VHOST = "vHost";
	String AMQP_AUTO_RECOVER = "autoRecoverable";
	
	//AMQP Exchange Names
	String AMQP_DIRECT = "direct";
	String AMQP_FANOUT = "fanout";
}