package com.companyApiTinyCache.companyConfigCache.service.common.connector;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.ArrayList;
import java.util.List;

/**
 * ProductAPIConnector for common code.
 * 
 * @author Saurabh
 * 
 */
public class ProductAPIConnector extends APIConnectorImpl {

    private static final Logger logger = LoggerFactory.getLogger(ProductAPIConnector.class);

    private static final String TRIPS_COUNT_URL = "ct.services.tm.api.trips-count.url";

    private CachedProperties cachedProperties;

    public boolean isPNRTicketed(String subDomain, String pnr) {
        int holdCount = 0;
        RestResponse restResponse = null;
        List<String> dontLog = new ArrayList<String>();
        dontLog.add("404");
        String tripsCountURL = getTripsCountAPIUrl() + "?gds-pnr=" + pnr;
        logger.info("Trips count URL->" + tripsCountURL);
        restResponse = getRestResponse(tripsCountURL, "", dontLog);
        String numberOfTrips = restResponse.getMessage();
        holdCount = Integer.parseInt(numberOfTrips.trim());
        return (holdCount == 0) ? false : true;
    }

    private String getTripsCountAPIUrl() {
        return this.cachedProperties.getPropertyValue(TRIPS_COUNT_URL);
    }

    public void setCachedProperties(CachedProperties pCachedProperties) {
        this.cachedProperties = pCachedProperties;
    }
}
