package com.companyApiTinyCache.companyConfigCache.service.common.connector;

import com.companyApiTinyCache.companyConfigCache.service.common.util.RestResponse;

import java.util.List;
import java.util.Map;

/**
 * Connector interface for hotel.
 * 
 * @author Ramesh Krishnamoorthy
 */
public interface APIConnector {

    /**
     * getRestResponse - connects to a given url and gets the resource asked for
     * 
     * @param url
     *            url of the resource being requested
     * 
     * @param queryString
     *            request parameters being passed to the url
     * 
     * @return <code>RestResponse</code> a RestResponse object containg the response code and message
     */
    RestResponse getRestResponse(String url, String queryString);

    /**
     * getRestResponse - connects to a given url and gets the resource asked for
     * 
     * @param url
     *            url of the resource being requested
     * 
     * @param queryString
     *            request parameters being passed to the url
     * 
     * @param dontLog
     *            list of errors which you don't want to see in the logs, for example, 404
     * 
     * @return <code>RestResponse</code> a RestResponse object containg the response code and message
     */
    RestResponse getRestResponse(String url, String queryString, List<String> dontLog);

    /**
     * getRestResponse - connects to a given url and gets the resource asked for
     * 
     * @param url
     *            url of the resource being requested
     * 
     * @param queryString
     *            request parameters being passed to the url
     * 
     * @param setProxy
     *            whether to set proxy
     * 
     * @return <code>RestResponse</code> a RestResponse object containg the response code and message
     */
    RestResponse getRestResponse(String url, String queryString, List<String> dontLog, boolean setProxy);

    /**
     * getRestResponse - connects to a given url and gets the resource asked for
     * 
     * @param url
     *            url of the resource being requested
     * 
     * @param queryString
     *            request parameters being passed to the url
     * 
     * @param setProxy
     *            whether to set proxy
     * 
     * @param requestHeaders
     *            requestHeaders to be sent
     * 
     * @return <code>RestResponse</code> a RestResponse object containg the response code and message
     */
    RestResponse getRestResponse(String url, String queryString, List<String> dontLog, boolean setProxy, Map<String, String> requestHeaders);
}
