package com.companyApiTinyCache.companyConfigCache.service.common.cached.resources;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedUrlResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.StreamResourceLoader;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Map;

public class WhitelabelViewResource extends CachedUrlResource<Map<String, Map<String, Map<String, String>>>> {

    public WhitelabelViewResource(CachedProperties pcommonCachedProperties) throws MalformedURLException {

        super(pcommonCachedProperties, "ct.services.whitelabel.view.resources.url.ctconfig", null, false);

        setLoader(new WhitelabelViewResourceLoader());

        refreshResource();
    }

    private class WhitelabelViewResourceLoader implements StreamResourceLoader<Map<String, Map<String, Map<String, String>>>> {

        /**
         * Loads CityList {@inheritDoc}
         * 
         * @see com.cleartrip.common.util.CachedEnumUrlResources#loadStreamResource(InputStream, Enum)
         */
        @SuppressWarnings("unchecked")
        @Override
        public Map<String, Map<String, Map<String, String>>> loadStreamResource(InputStream is) throws Exception {
            String viewResourcesJson = null;

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            String line = null;

            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {

                sb.append(line);
            }

            bufferedReader.close();

            viewResourcesJson = sb.toString();

            ObjectMapper mapper = JsonUtil.getObjectMapper();

            Map<String, Map<String, Map<String, String>>> map = (Map<String, Map<String, Map<String, String>>>) mapper.readValue(viewResourcesJson, Map.class);

            return map;
        }
    }
}
