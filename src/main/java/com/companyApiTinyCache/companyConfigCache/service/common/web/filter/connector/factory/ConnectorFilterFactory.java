package com.companyApiTinyCache.companyConfigCache.service.common.web.filter.connector.factory;


import com.companyApiTinyCache.companyConfigCache.service.common.web.filter.ConnectorFilter;
import com.companyApiTinyCache.companyConfigCache.service.common.web.filter.NoOptFilter;

import javax.servlet.Filter;

/**
 * Connector Filter factory class.
 * @author vaibhavmaniar
 */
public class ConnectorFilterFactory {

    private ConnectorFilterFactory() {}

    public static Filter create(boolean isConnectorSrcEnabled) {
        Filter filter;
        if(!isConnectorSrcEnabled) {
            filter = new ConnectorFilter();
        } else {
            filter = new NoOptFilter();
        }
        return filter;
    }

}
