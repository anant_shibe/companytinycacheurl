package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.cached.resources.CachedUIElements;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CurrencyData;
import com.companyApiTinyCache.companyConfigCache.service.common.service.AuthenticationService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Filter which determines the country from the IP Address.
 * @author suresh
 */
public class CtIPFilter implements Filter {

    private static final String COOKIE_LESS_URL_ATTR_NAME = "COOKIE_LESS_URL";

    private static final Log logger = LogFactory.getLog(CtIPFilter.class);

    private static final String CT_COUNTRY_RELATED_PROPERTIES_MAP = "ct.country.to.domain.specific.properties.mapping";
    protected FilterConfig filterConfig;
    protected CachedProperties cachedProperties;
    protected CachedCurrencyDataResource cachedCurrencyDataResource;
    protected CachedMobileHeadersResource cachedMobileHeadersResource;
    protected AuthenticationService authenticationService;
    protected CachedUIElements cachedUIElements;
    protected static final String CT_AUTH_PREF_COOKIE = "ct.services.authentication.pref-auth-cookiename";
    protected static final String CT_AUTH_COOKIE = "ct.services.authentication.auth-cookiename";

    public void setCachedMobileHeadersResource(CachedMobileHeadersResource cachedMobileHeadersResource) {
        this.cachedMobileHeadersResource = cachedMobileHeadersResource;
    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpReq = (HttpServletRequest) request;

        HttpServletResponse httpRes = (HttpServletResponse) response;

        Object cookieLessUrlAttrValue = request.getAttribute(COOKIE_LESS_URL_ATTR_NAME);
        if (cookieLessUrlAttrValue != null && "Y".equals(cookieLessUrlAttrValue.toString())) {
            chain.doFilter(request, response);
        } else {

            boolean isMobile = cachedMobileHeadersResource.isMobile(httpReq);

            String excludeURLs = filterConfig.getInitParameter("exclude-urls");

            boolean isExcludeURL = false;

            if (StringUtils.isNotBlank(excludeURLs)) {
                String requestURL = httpReq.getRequestURL().toString();
                String[] excludeURLArray = excludeURLs.split(",", -1);
                for (String excludeURL : excludeURLArray) {
                    if (requestURL.toLowerCase().contains(excludeURL)) {
                        isExcludeURL = true;
                        break;
                    }
                }
            }

            if (isExcludeURL) {
                chain.doFilter(request, response);
                return;
            }

            request.setAttribute("isMobileRequest", isMobile);
            if (isMobile) {
                request.setAttribute("mobilePlatform", cachedMobileHeadersResource.getMobilePlatform(httpReq).toString());
                request.setAttribute("mobileOsVersion", cachedMobileHeadersResource.getOSVersion(httpReq));
                request.setAttribute("isMobileHtml4Request", isOnlyHTML4Capable(httpReq));
                request.setAttribute("browser", cachedMobileHeadersResource.getMobileBrowserName(httpReq));
                request.setAttribute("sd_white_label", GenUtil.isSDEnabled(httpReq, cachedProperties));
            } else {
                request.setAttribute("isMobileHtml4Request", false);
            }
            String country = null, currency = null;

            // if user requests preferable for some country or currency by clicking on domain link, or by selecting the currency preference.
            String requestedCountry = request.getParameter(CommonConstants.SELECTED_COUNTRY);

            String requestedCurrency = request.getParameter(CommonConstants.SELECTED_CURRENCY);

            // get the user preference cookie

            Cookie prefCookie = getUserPreferenceCookie(httpReq);

            /*
             * As decided dont hit accounts DB for user profiles if (prefCookie == null) { userProfile = getUserProfile(httpReq); }
             */

            // country based on domain
            String urlCountry = SecurityUtil.getCountryFromDomain(httpReq);

            if (prefCookie != null && StringUtils.isNotBlank(prefCookie.getValue()) && prefCookie.getValue().split("\\|", -1).length == 2) {
                String value = prefCookie.getValue();
                String[] values = value.split("\\|", -1);
                country = values[0];
            } else {
                country = "IN";
            }

            Cookie currencyPrefCookie = GenUtil.getCookieFromRequest(httpReq, CommonConstants.CURRENCY_PREF);

            if (currencyPrefCookie == null || StringUtils.isBlank(currencyPrefCookie.getValue())) {
                currency = cachedProperties.getPropertyValue("ct.common." + country.toLowerCase() + ".selling.currency");
            } else {
                currency = currencyPrefCookie.getValue();
            }

            // if user has selected any country or currency explicitly then execute if code block
            if (StringUtils.isNotBlank(requestedCountry) || StringUtils.isNotBlank(requestedCurrency)) {

                if (StringUtils.isNotBlank(requestedCountry)) {
                    // update only the country preference in the cookie
                    addOrUpdateCountryInCookie(httpReq, httpRes, requestedCountry);
                    country = requestedCountry;
                    urlCountry = requestedCountry;
                    currency = cachedProperties.getPropertyValue("ct.common." + country.toLowerCase() + ".selling.currency");
                } else {
                    try {
                        // addOrUpdateCurrencyInCookie(httpReq, httpRes, requestedCurrency);
                        currency = requestedCurrency;
                        country = urlCountry;
                    } catch (Exception e) {
                        logger.error("exception while setting currency preference in cookie", e);
                    }
                }
            } else {

                if (prefCookie == null) {

                    Map<String, Map<String, String>> supportedCountryMap = GenUtil.getSupportedCountryListMap(cachedProperties);

                    // user might have set the preference in profile
                    String userCountry = null;

                    if (!urlCountry.equalsIgnoreCase("IN")) {
                        userCountry = urlCountry;
                    }

                    boolean countrySupported = false;

                    if (StringUtils.isBlank(userCountry)) {

                        // PRM-4478
                        String userCountryFromCSV = SecurityUtil.getCountryFromIpaddress(httpReq, cachedProperties);
                        boolean ipRedirection = false;
                        if (!userCountryFromCSV.equalsIgnoreCase(urlCountry)) {
                            if (!isMobile) {
                                ipRedirection = true;
                            }
                            httpReq.setAttribute("sourceCountry", userCountryFromCSV);
                            httpReq.setAttribute("supportedCountryMap", supportedCountryMap);
                        }
                        // after this change always if block will get executed since userCountry is blank .
                        if (StringUtils.isBlank(userCountry)) {
                            userCountry = urlCountry;
                        } else {
                            userCountry = StringUtils.trim(userCountry);
                        }

                        // for e.g. if user country is US,then we have to treat him as a .com customer.
                        boolean isDifferentUserCountryOtherthanDoamin = cachedProperties.getBooleanPropertyValue("ct.common.different.user.country.redirection.enabled", true);
                        for (Map<String, String> countryMap : supportedCountryMap.values()) {
                            String countryCode = countryMap.get("code");
                            if (ipRedirection) {
                            		if (isDifferentUserCountryOtherthanDoamin) {
	                                if (countryCode.equalsIgnoreCase(userCountryFromCSV)) {
	                                    httpRes.sendRedirect(countryMap.get("url"));
	                                    return;
	                                }
                            		}
                            } else if (countryCode.equalsIgnoreCase(userCountry)) {
                                if (isMobile) {
                                    if (countryMap.get("mobile").equalsIgnoreCase("true")) {
                                        countrySupported = true;
                                    }
                                } else {
                                    countrySupported = true;
                                }
                                break;
                            }
                        }

                        if (!countrySupported) {
                            userCountry = "IN";
                        }

                    } else if (isMobile) {

                        for (Map<String, String> countryMap : supportedCountryMap.values()) {
                            String countryCode = countryMap.get("code");
                            if (countryCode.equalsIgnoreCase(userCountry)) {
                                if (countryMap.get("mobile").equalsIgnoreCase("true")) {
                                    countrySupported = true;
                                }
                                break;
                            }
                        }
                        if (!countrySupported) {
                            userCountry = "IN";
                        }

                    }

                    // if requested domain and geo domain doesn't match then do try to redirect the user to the geo domain.
                    boolean isGeoDomainRedirectionEnabled = cachedProperties.getBooleanPropertyValue("ct.common.geo.domain.redirection.enabled", true);
                    if (isGeoDomainRedirectionEnabled) {
	                    if (!userCountry.equalsIgnoreCase(urlCountry)) {
	
	                        if (supportedCountryMap != null) {
	                            for (Map<String, String> countryMap : supportedCountryMap.values()) {
	                                String countryCode = countryMap.get("code");
	                                if (countryCode.equalsIgnoreCase(userCountry)) {
	                                    String url;
	                                    if (isMobile) {
	                                        url = countryMap.get("mobileurl");
	                                    } else {
	                                        url = countryMap.get("url");
	                                    }
	
	                                    httpRes.sendRedirect(url);
	                                    return;
	                                }
	                            }
	                        }
	                        urlCountry = "IN";
	                    }
                    }
                    country = urlCountry.toUpperCase();

                    currency = cachedProperties.getPropertyValue("ct.common." + country.toLowerCase() + ".selling.currency");

                    boolean setCookieDomain = GenUtil.isSetCookieDomain(httpReq);
                    Cookie c = createCtAuthPreferenceCookie(country, currency, setCookieDomain);
                    httpRes.addCookie(c);
                }
            }

            httpReq.setAttribute(CommonConstants.COUNTRY, urlCountry.toUpperCase());

            // httpReq.setAttribute(CommonConstants.DISP_CURRENCY, currency.toUpperCase());

            String countryCurrency = cachedProperties.getPropertyValue("ct.common." + urlCountry.toLowerCase() + ".selling.currency", "INR");

            /*
             * Currently DISP_CURRENCY and SELL_CURRENCY are used as one and the same . Also ,Preferred currency is not enabled currently in the web site . (which was stored in the accounts and we
             * were getting using userProfile) And also in the search results page if the user requests the preferred currency the scope will be only in that page and which is served using the java
             * script .
             */

            httpReq.setAttribute(CommonConstants.SELL_CURRENCY, countryCurrency.toUpperCase());

            httpReq.setAttribute(CommonConstants.DISP_CURRENCY, countryCurrency.toUpperCase());

            Cookie langPrefCookie = GenUtil.getCookieFromRequest((HttpServletRequest) request, "lang-pref");

            String queryLanguage = request.getParameter(CommonConstants.LANGUAGE);

            if (StringUtils.isNotEmpty(queryLanguage) && (queryLanguage.equals("en") || queryLanguage.equals("ar"))) {
                httpReq.setAttribute(CommonConstants.LANG_PREF, queryLanguage);
            } else if (langPrefCookie != null) {
                String cookieLanguage = langPrefCookie.getValue();
                if (StringUtils.isNotEmpty(cookieLanguage) && (cookieLanguage.equals("en") || cookieLanguage.equals("ar"))) {
                    httpReq.setAttribute(CommonConstants.LANG_PREF, langPrefCookie.getValue());
                } else {
                    httpReq.setAttribute(CommonConstants.LANG_PREF, "en");
                }
            } else {
                httpReq.setAttribute(CommonConstants.LANG_PREF, "en");
            }

            boolean isLanPrefCookieEnabled = cachedProperties.getBooleanPropertyValue("ct.services.language.pref.cookie.enabled", false);
            if (isLanPrefCookieEnabled) {
                String lpCookieName = cachedProperties.getPropertyValue("ct.services.language.preference.cookie.name", "lp");
                Cookie lpCookie = GenUtil.getCookieFromRequest((HttpServletRequest) request, lpCookieName);
                if (null != lpCookie && StringUtils.isNotBlank(lpCookie.getValue()) && !lpCookie.getValue().equalsIgnoreCase("en")) {
                    httpReq.setAttribute("langUrlPrefix", "/" + lpCookie.getValue());
                } else {
                    httpReq.setAttribute("langUrlPrefix", "");
                }
            }

            CurrencyData currencyData = cachedCurrencyDataResource.getResource().get(countryCurrency.toUpperCase());

            String symbol = currencyData.getSymbol();

            httpReq.setAttribute(CommonConstants.DISPLA_CURRENCY_TEXT, symbol);

            String language = cachedProperties.getPropertyValue("ct.common." + country.toLowerCase() + ".lang", "en");

            httpReq.setAttribute(CommonConstants.LANG, language);

            addCurrencyPrefCookie(httpReq, httpRes, currency, country);

            String ctAuthCookieName = cachedProperties.getPropertyValue(CT_AUTH_COOKIE);

            httpReq.setAttribute("auth", ctAuthCookieName);

            Map<String, Map<String, String>> countryList = GenUtil.getSupportedCountryListMap(cachedProperties);

            httpReq.setAttribute("countries_supported", countryList);

            setDomainSpecificProperties(httpReq);

            chain.doFilter(request, response);
        }
    }

    private boolean isOnlyHTML4Capable(HttpServletRequest request) {
        boolean isMobileHtml4Request = false;

        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (StringUtils.isNotBlank(cookie.getName()) && cookie.getName().equalsIgnoreCase("ct.html5.capable") && !StringUtils.isBlank(cookie.getValue())
                        && cookie.getValue().equalsIgnoreCase("false")) {
                    isMobileHtml4Request = true;
                    break;
                }
            }
        }

        return isMobileHtml4Request;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;

        ApplicationContext ctx = this.getContext(filterConfig);

        Object beanObj = null;
        beanObj = ctx.getBean("commonCachedProperties");
        if (beanObj instanceof CachedProperties) {
            cachedProperties = (CachedProperties) beanObj;
        }

        beanObj = ctx.getBean("cachedCurrencyDataResource");
        if (beanObj instanceof CachedCurrencyDataResource) {
            cachedCurrencyDataResource = (CachedCurrencyDataResource) beanObj;
        }

        beanObj = ctx.getBean("authenticationServiceBean");
        if (beanObj instanceof AuthenticationService) {
            authenticationService = (AuthenticationService) beanObj;
        }

        beanObj = ctx.getBean("commonCachedMobileHeadersResource");
        if (beanObj instanceof CachedMobileHeadersResource) {
            cachedMobileHeadersResource = (CachedMobileHeadersResource) beanObj;
        }

        beanObj = ctx.getBean("cachedUIElements");
        if (beanObj instanceof CachedUIElements) {
            cachedUIElements = (CachedUIElements) beanObj;
        }

    }

    /**
     * Allows test cases to override where application context obtained from.
     * @param filterConfig which can be used to find the <code>ServletContext</code>
     * @return the Spring application context
     */
    protected ApplicationContext getContext(FilterConfig filterConfig) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
    }

    /**
     * Creates a preferred cookie for country.
     * @param country
     * @param currency
     * @param setCookieDomain
     * @return
     */
    private Cookie createCtAuthPreferenceCookie(String country, String currency, boolean setCookieDomain) {

        String domain = country;

        if (domain.equalsIgnoreCase("IN")) {
            domain = "com";
        }
        String ctAuthPrefCookieName = cachedProperties.getPropertyValue(CT_AUTH_PREF_COOKIE);

        Cookie prefCookie = new Cookie(ctAuthPrefCookieName, country.toUpperCase() + "|" + currency.toUpperCase());

        /*
         * if(setCookieDomain) { domain = ".cleartrip." + domain.toLowerCase(); prefCookie.setDomain(domain); }
         */

        // need to set the path, otherwise it signout won't be recognized.
        prefCookie.setPath("/");

        return prefCookie;
    }

    /**
     * Gets the user preference cookie from request.
     * @param request
     * @return
     */
    private Cookie getUserPreferenceCookie(HttpServletRequest request) {

        String userPreferenceCookieName = cachedProperties.getPropertyValue(CT_AUTH_PREF_COOKIE);

        Cookie prefCookie = GenUtil.getCookieFromRequest(request, userPreferenceCookieName);

        return prefCookie;
    }

    private void addOrUpdateCountryInCookie(HttpServletRequest request, HttpServletResponse response, String country) {
        Cookie prefCookie = getUserPreferenceCookie(request);
        boolean setCookieDomain = GenUtil.isSetCookieDomain(request);

        if (prefCookie == null || (StringUtils.isBlank(prefCookie.getValue()) || (prefCookie.getValue().split("\\|", -1).length < 2))) {
            String currency = cachedProperties.getPropertyValue("ct.common." + country.toLowerCase() + ".selling.currency", "INR");
            prefCookie = createCtAuthPreferenceCookie(country.toUpperCase(), currency.toUpperCase(), setCookieDomain);
        } else {
            String[] values = prefCookie.getValue().split("\\|", -1);
            String currency = values[1];
            prefCookie = createCtAuthPreferenceCookie(country.toUpperCase(), currency.toUpperCase(), setCookieDomain);
        }

        response.addCookie(prefCookie);
    }

    private void addCurrencyPrefCookie(HttpServletRequest request, HttpServletResponse response, String currency, String country) {
        try {
            Cookie currencyPrefCookie = new Cookie(CommonConstants.CURRENCY_PREF, currency.toUpperCase());
            currencyPrefCookie.setPath("/");
            response.addCookie(currencyPrefCookie);
        } catch (Exception e) {
            logger.error("Error while setting pref cookie: ", e);
        }
    }

    /*
     * @author Gururaj Fetches all the domain related properties like phone number ,messages ,language and sets in httpRequest object CT_COUNTRY_RELATED_PROPERTIES_MAP is in locale.properties
     */
    @SuppressWarnings("unchecked")
    private void setDomainSpecificProperties(HttpServletRequest request) throws MalformedURLException {

        Map<String, Map<String, String>> countryToCountrySpecificPropertiesMap = null;

        countryToCountrySpecificPropertiesMap = cachedUIElements.getResource();

        // this part of the code is not used at all ,earlier I was reading it as string property file and converting it into map .
        // now onwards reading it as a json .
        if (countryToCountrySpecificPropertiesMap == null) {
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            String countryToCountrySpecificPropertiesString = cachedProperties.getPropertyValue(CT_COUNTRY_RELATED_PROPERTIES_MAP);

            if (StringUtils.isNotBlank(countryToCountrySpecificPropertiesString)) {
                try {
                    countryToCountrySpecificPropertiesMap = (Map<String, Map<String, String>>) mapper.readValue(countryToCountrySpecificPropertiesString, Map.class);
                } catch (Exception e) {
                    logger.error("Exception while reading", e);
                }
            }
        }

        if (countryToCountrySpecificPropertiesMap != null) {
            try {
                for (Entry<String, Map<String, String>> entry : countryToCountrySpecificPropertiesMap.entrySet()) {
                    if (entry.getKey().toString().equalsIgnoreCase(request.getAttribute("country").toString())) {
                        Map<String, String> domainCountryMap = entry.getValue();
                        for (Entry<String, String> entry1 : domainCountryMap.entrySet()) {
                            request.setAttribute(entry1.getKey(), entry1.getValue());
                        }
                        break;
                    }
                }
            } catch (Exception e) {
                logger.error("Error while getting Domain specific information from request: " + request.getRequestURL(), e);

            }
        }
    }

}
