/* 
 * @(#) CommonUIViewnameFactory.java
 * 
 * Copyright 2010-2011 by Cleartrip Travel Services Pvt. Ltd.
 * Unit No 001, Ground Floor, DTC Bldg, 
 * Sitaram Mills Compound, N.M. Joshi Marg, 
 * Delisle Road, 
 * Lower Parel (E) 
 * Mumbai - 400011
 * India
 * 
 * This software is the confidential and proprietary information 
 * of Cleartrip Travel Services Pvt. Ltd. ("Confidential Information"). 
 * You shall not disclose such Confidential Information and shall 
 * use it only in accordance with the terms of license agreement you 
 * entered into with Cleartrip Travel Service Pvt. Ltd.
 */
package com.companyApiTinyCache.companyConfigCache.service.common.web;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Common-UI modules (common-search-ui and common-book-ui) expose an optional functionality of returning only content-area (center portion) of the each view (JSP). To achieve this, the content-area of
 * each view-JSP was extracted out into another JSP. These new JSPs are under `ct-jsps/commonui/content-only` and follow the following naming convention:
 * <ul>
 * <li>If view-name is 'foo/bar' :</li>
 * <li>content-jsp name is 'foo-bar-content.jsp'</li>
 * </ul>
 * <br/>
 * For easy mapping of view-name to content-jsp view name, this factory is required. The factory maintains a map which maps viewName to contnetJspName.
 * 
 * @author Amit Kumar Sharma
 * @version 1.0
 */
public class CommonUIViewnameFactory {

    /** Lookup table which maps viewName to content-JSP */
    protected Map<String, String> lookupTable;

    public void setLookupTable(Map<String, String> lookupTable) {
        this.lookupTable = lookupTable;
    }

    /** Name of reqest parameter which indicates if only content is to be rendered */
    public static final String CONTENT_ONLY_PARAM = "content-only";

    /** Logger instance */
    protected static final Log logger = LogFactory.getLog(CommonUIViewnameFactory.class);

    public CommonUIViewnameFactory() {
    }

    /**
     * Utility function to extract boolean value of request param 'content-only'.
     * 
     * @param request
     * @return true iff content-only='true', false otherwise.
     */
    public static boolean extractContentonlyParam(HttpServletRequest request) {

        String contentOnlyParam = StringUtils.trimToEmpty(request.getParameter(CONTENT_ONLY_PARAM));
        boolean contentOnly = new Boolean(contentOnlyParam);

        return contentOnly;

    }

    /**
     * Resolver function to resolve the correct view name.
     * 
     * @param viewName
     * @param contentOnly
     * @param lang
     * @return
     */
    public String resolveViewName(String viewName, boolean contentOnly) {

        // 1. Lookup the correct view name
        String correctViewName = lookupTable.get(viewName);
        if (correctViewName == null) {
            logger.info("No content-view-name found for '" + viewName + "'");
            return viewName;
        }

        // 2. If only content is required, change the view name according to
        // language preference.
        // For complete page this value will be included in the actual JSP,
        // so let it be.
        if (contentOnly) {
            correctViewName = "air/" + correctViewName;
        } else {
            correctViewName = correctViewName + ".jsp";
        }

        return correctViewName;
    }

}
