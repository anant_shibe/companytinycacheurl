package com.companyApiTinyCache.companyConfigCache.service.common.security;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.SecurityUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Cleartrip
 */
@ClassExcludeCoverage
public class CTSecurityHeaders implements Serializable {

    private static final long serialVersionUID = 160548664223197638L;
    private Map<String, String> securityHeaders;
    private Cookie ctAuthCookie;
    private HttpServletRequest request;
    private SourceType sourceType;

    public CTSecurityHeaders(HttpServletRequest request, SourceType sourceType) {
        this.request = request;
        this.sourceType = sourceType;
        securityHeaders = new HashMap<String, String>();
    }

    public Cookie getCtAuthCookie(CachedProperties commonCachedProperties) {
        if (ctAuthCookie == null && request != null) {
            ctAuthCookie = GenUtil.getCookieFromRequest(request, SecurityUtil.getAuthCookieName(commonCachedProperties));
        }
        return ctAuthCookie;
    }

    public Map<String, String> getSecurityHeaders() {
        return securityHeaders;
    }

    public Map<String, String> getAllHeaders(CachedProperties commonCachedProperties) {
        securityHeaders.putAll(getAuthKeyForSourceType(commonCachedProperties, sourceType));
        return securityHeaders;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, String> getAuthKeyForSourceType(CachedProperties commonCachedProperties, SourceType sourceType) {
        Map<String, String> authKey = new HashMap<String, String>();
        String authKeyNameJson = commonCachedProperties.getPropertyValue("ct.common.auth-key.to.retrieve.userprofile", "{}");
        if (StringUtils.isNotBlank(authKeyNameJson)) {
            Map<String, Object> authKeyMap = APIUtil.deserializeJsonToObject(authKeyNameJson, Map.class);
            if (null != authKeyMap && authKeyMap.size() > 0 && authKeyMap.containsKey(sourceType.toString())) {
                Map<String, String> headers = (Map<String, String>) authKeyMap.get(sourceType.toString());
                if (null != headers) {
                    authKey.putAll(headers);
                    authKey.put("X-Forwarded-Proto", "https");
                }
            }
        }
        return authKey;
    }
}
