package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * A simple wrapper over a bounded LinkedBlockingQueue with a single message processing thread continuously
 * de-queuing messages and handing off to the specified BoundedQueueConsumer interface which decides what to
 * do with the message.
 *
 * Main purpose of this is to act as a wrapper over a Kafka or ActiveMQ sender and shield the request thread by
 * discarding messages in case sending to Kafka or ActiveMQ become slow due to server or network issues.
 *
 * @param <T> Type of the message
 *
 * @author suresh
 */
public class BoundedQueue<T> implements Runnable {

    private static final Log log = LogFactory.getLog(BoundedQueue.class);

    private BlockingQueue<T> q;

    private BoundedQueueConsumer<T> consumer;

    // Instantiated in a Singleton fashion on the first send call to avoid creating Threads for unused
    // BoundedQueues
    private volatile Thread listeningThread;

    public BoundedQueue(BoundedQueueConsumer<T> pconsumer, int maxQSize) {
        consumer = pconsumer;
        q = new ArrayBlockingQueue<T>(maxQSize);
    }

    @Override
    public void run() {
        T message;
        List<T> messages = new ArrayList<T>(64);
        do {
            try {
                messages.clear();
                if (q.drainTo(messages, 64) == 0) {
                    message = q.take();
                    messages.add(message);
                }
                int i, len = messages.size();
                for (i = 0; i < len; i++) {
                    try {
                        message = messages.get(i);
                        consumer.process(message);
                    } catch (Exception e) {
                        log.error("Error Processing Message ", e);
                    }
                }
            } catch (Exception e) {
                log.error("Unexpected Error in BoundedQueue ", e);
            }
        } while (true);
    }

    public void send(T message) {
        if (listeningThread == null) {
            synchronized (this) {
                if (listeningThread == null) {
                    listeningThread = new Thread(this);
                    listeningThread.start();
                }
            }
        }
        try {
            if (!q.offer(message, 200, TimeUnit.MILLISECONDS)) {
                log.error("Error Sending Message to JVM Q as Q is full. Q Size = " + q.size());
            }
        } catch (InterruptedException e) {
            log.error("Interrupted when Sending Message");
        }
    }
}
