package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

@ClassExcludeCoverage
public class RabbitMQConsumerException extends Exception {

	private static final long serialVersionUID = 1L;

	public RabbitMQConsumerException(String message) {
		super(message);
	}

	public RabbitMQConsumerException(Exception exception) {
		super(exception);
	}

	public RabbitMQConsumerException(String message, Exception exception) {
		super(message, exception);
	}
}
