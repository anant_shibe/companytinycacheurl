/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.notification;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Map;

/**
 * Service class to send emails.
 *
 * @author sanjeev
 */
public class NotificationMediatorImpl implements NotificationMediator {
    private static final Log logger = LogFactory.getLog(NotificationMediatorImpl.class);
    private MessageFormatter messageFormatter;
    private EmailSender emailSender;
    private Cache templateCache;

    public void transmitNotification(String subjectMessage, String bodyMessage, String toAddress, String fromAddress, Map<String, String> subjectValues, Map<String, String> bodyValues)
            throws MessageFormatException, MessagingException {

        transmitNotification(subjectMessage, bodyMessage, toAddress, fromAddress, subjectValues, bodyValues, null);
    }

    public void transmitNotification(String subjectMessage, String bodyMessage, String toAddress, String fromAddress, Map<String, String> subjectValues, Map<String, String> bodyValues,
            String fromName) throws MessageFormatException, MessagingException {
        transmitNotification(subjectMessage, bodyMessage, toAddress, fromAddress, subjectValues, bodyValues, fromName, null);
    }

    @Override
    public void transmitNotification(String subjectMessage, String bodyMessage, String toAddress, String fromAddress, Map<String, String> subjectValues, Map<String, String> bodyValues,
            String fromName, String contentType) throws MessageFormatException, MessagingException {
        // logger.info("Entered transmitNotification method");
        if (subjectValues != null) {
            subjectMessage = getFormattedMessage(subjectMessage, subjectValues);
        }
        if (bodyValues != null) {
            bodyMessage = getFormattedMessage(bodyMessage, bodyValues);
        }
        emailSender.sendEmail(toAddress, fromAddress, subjectMessage, bodyMessage, fromName, contentType);
        // logger.info("Leaving transmitNotification method");
    }

    @Override
    public void transmitNotification(String subjectTemplateName, String bodyTemplateName, Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String fromAddress,
            Map<String, String> subjectValues, Map<String, String> bodyValues) throws MessageFormatException, MessagingException {
        // logger.info("Entered transmitNotification (collection of to, cc n bcc) method");
        String subjectMessage = getFormattedMessage(subjectTemplateName, subjectValues);
        String bodyMessage = getFormattedMessage(bodyTemplateName, bodyValues);
        emailSender.sendEmail(toList, ccList, bccList, fromAddress, subjectMessage, bodyMessage);
        // logger.info("Leaving transmitNotification (collection of to, cc n bcc) method");
    }

    /**
	 *
	 */
    private String getFormattedMessage(String data, Map<String, String> values) throws MessageFormatException {
        // logger.info("Entered getFormattedMessage method");
        String content = messageFormatter.formatMessage(data, values);
        // logger.info("Leaving getFormattedMessage method");
        return content;
    }

    /**
     * @return the messageFormatter
     */
    public MessageFormatter getMessageFormatter() {
        return messageFormatter;
    }

    /**
     * @param messageFormatter
     *            the messageFormatter to set
     */
    public void setMessageFormatter(MessageFormatter messageFormatter) {
        this.messageFormatter = messageFormatter;
    }

    /**
     * @return the emailSender
     */
    public EmailSender getEmailSender() {
        return emailSender;
    }

    /**
     * @param emailSender
     *            the emailSender to set
     */
    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    /**
     * @return the templateCache
     */
    public Cache getTemplateCache() {
        return templateCache;
    }

    /**
     * @param templateCache
     *            the templateCache to set
     */
    public void setTemplateCache(Cache templateCache) {
        this.templateCache = templateCache;
    }

}
