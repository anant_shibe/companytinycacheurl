package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

@ClassExcludeCoverage
public class InfoMessage {

	private String message;
	private String supplier;
	private String errorCode;

	public InfoMessage(String supplier, String errorCode, String message) {
		this.supplier = supplier;
		this.errorCode = errorCode;
		this.message = message;
	}
	
	public InfoMessage() {
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
