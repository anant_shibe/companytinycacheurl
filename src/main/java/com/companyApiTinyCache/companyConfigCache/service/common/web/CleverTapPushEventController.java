package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author parvatkumar
 */
public class CleverTapPushEventController extends AbstractController implements CleverTapEnumConstants {

	private static final Log logger = LogFactory.getLog(CleverTapPushEventController.class);

	private CachedProperties cachedProperties;

	private CleverTapPushEventServiceImpl cleverTapPushEventService;

	private static final String MESSAGE_PUSHED = "{\"statusCode\":200,\"message\":\"Pushed Event to Kafka\"}";

	public CachedProperties getCachedProperties() {
		return cachedProperties;
	}

	public void setCachedProperties(CachedProperties cachedProperties) {
		this.cachedProperties = cachedProperties;
	}

	public CleverTapPushEventServiceImpl getCleverTrapPushEventService() {
		return cleverTapPushEventService;
	}

	public void setCleverTapPushEventService(CleverTapPushEventServiceImpl cleverTapPushEventService) {
		this.cleverTapPushEventService = cleverTapPushEventService;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		logger.info("Request For Pushing Event To CleverTap");
		PrintWriter pw = null;
		response.setContentType("text/json");
		try {
			// TODO make property names as constants
			boolean isCleverTapEnabled = cachedProperties.getBooleanPropertyValue("ct.clevertap.enabled", false);
			String pushEventResponseJson = cachedProperties.getPropertyValue("ct.clevertap.push.response.json");
			boolean isRespectedFrequencyCaps = cachedProperties.getBooleanPropertyValue("ct.air.respect.frequency.caps.enable", false);
			// String errorResponseJson =
			// cachedProperties.getPropertyValue("ct.clevertap.response.error.json");
			pw = response.getWriter();
			// Map<String,Object> errorResponseJson = null;
			ObjectMapper mapper = JsonUtil.getObjectMapper();
			Map<String, Object> pushEventResponseMap = null;
			Map<String, String> errorResponseMap = null;
			//mapper = JsonUtil.getObjectMapper();
			mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			if (StringUtils.isNotBlank(pushEventResponseJson)) {
				pushEventResponseMap = mapper.readValue(pushEventResponseJson, Map.class);
			}
			if (isCleverTapEnabled) {
				if (request.getMethod().equalsIgnoreCase("POST")) {
					// get the payload
					InputStream ip = request.getInputStream();
					String payloadJson = GenUtil.readAsStringString(ip);
					ip.close();
					// get the payload and convert it to eventDetails map
					Map<String, Object> eventDetailsMap = (Map<String, Object>) mapper.readValue(payloadJson,
							Map.class);
                    if(cleverTapPushEventService.isAsync()){
                        cleverTapPushEventService.pushEventDetailsToKafka(eventDetailsMap);
						response.setStatus(HttpServletResponse.SC_OK);
						pw.println(MESSAGE_PUSHED);
						return null;
                    }
					Map<String, String> eventDataMap = null;
					Map<String, Object> pushNotificationData = (Map<String, Object>) eventDetailsMap.get("data");

					Object eventData = eventDetailsMap.get(CleverTapEnumConstants.EVENT_DATA);
					StringWriter sw = new StringWriter();
					mapper.writeValue(sw, eventData);
					String jsonString = sw.toString();
					if (eventData != null) {
						eventDataMap = mapper.readValue(jsonString, Map.class);
					}
					if (MapUtils.isNotEmpty(pushNotificationData)) {
						if (StringUtils.isNotBlank((String) pushNotificationData.get("eventType"))) {
							if (StringUtils.equalsIgnoreCase((String) pushNotificationData.get("eventType"),
									"pushNotification")) {
								Map<String, Object> payLoadMap = (Map<String, Object>) pushNotificationData
										.get("payload");
								if(MapUtils.isNotEmpty(payLoadMap)) {
								 if(payLoadMap.get(RESPECT_FREQUENCY_CAPS) == null) {
								   payLoadMap.put(RESPECT_FREQUENCY_CAPS, isRespectedFrequencyCaps);
								 }
								}
								sendNotification(payLoadMap, response, pushEventResponseMap);
							}
						}
					}

					if (eventDetailsMap != null && !eventDetailsMap.isEmpty() && MapUtils.isNotEmpty(eventDataMap)) {
						List<String> errorMessages = new ArrayList<String>();
						Integer code = 0;
						if (cleverTapPushEventService.validateEventDetails(eventDetailsMap, errorMessages,
								pushEventResponseMap)) {
							String responseMsg = cleverTapPushEventService.createOrUpdateEvent(eventDetailsMap);
							Map<String, Object> cleverTrapResponseMap = mapper.readValue(responseMsg, Map.class);
							ArrayList<Object> unprocessedList = (ArrayList<Object>) cleverTrapResponseMap
									.get(CleverTapEnumConstants.UNPROCESSED);
							if (unprocessedList != null && !unprocessedList.isEmpty()) {
								String responseJsonctp = JsonUtil.toJson(unprocessedList.get(0));
								Map<String, Object> ctpResonseMap = mapper.readValue(responseJsonctp, Map.class);
								code = (Integer) ctpResonseMap.get(CleverTapEnumConstants.CODE);
							} else {
								if (StringUtils.isNotBlank(responseMsg)) {
									response.setStatus(HttpServletResponse.SC_OK);
									if (pushEventResponseMap != null) {
										pw.println("{\"statusCode\": 200, \"message\":\""
												+ pushEventResponseMap.get("success") + "\",\"Identity is\":\""
												+ (String) eventDataMap.get(CleverTapEnumConstants.USER_EMAIL) + "\"}");
									} else {
										pw.println(
												"{\"statusCode\":200,\"message\":\"Event Created Sucessfully.\",\"Identity Is\":\""
														+ (String) eventDataMap.get(CleverTapEnumConstants.USER_EMAIL)
														+ "\"}");
									}
								} else {
									if (pushEventResponseMap != null) {
										pw.println("{\"statusCode\":500,\"message\":\""
												+ pushEventResponseMap.get("failureResponse") + "\"}");
									} else {
										pw.println(
												"{\"statusCode\":500,\"message\":\"Sorry Unable To Get Response From Clevertrap.\"}");
									}
								}

							}
							if (code != null && code != 0) {
								errorResponseMap = (Map<String, String>) pushEventResponseMap.get("error");
								if (errorResponseMap != null && errorResponseMap.isEmpty()) {
									for (String key : errorResponseMap.keySet()) {
										if (code == Integer.parseInt(key)) {
											if (errorResponseMap != null) {
												pw.println("{\"statusCode\":200,\"message\":\""
														+ errorResponseMap.get(key) + "\"}");
											}
										}
									}
								} else {
									logger.error("Error while getting data for errorResponseMap");
								}
							}
						} else {
							if (pushEventResponseMap != null) {
								response.setStatus(HttpServletResponse.SC_OK);
								pw.println("{\"statusCode\":400,\"message\":\"" + errorMessages.get(0) + "\"}");
							} else {
								pw.println("{\"statusCode\":400,\"message\":\"EventDetails validation failed");
							}
						}
					} else {
						if (pushEventResponseMap != null) {
							response.setStatus(HttpServletResponse.SC_OK);
							pw.println("{\"statusCode\":400,\"message\":\"" + pushEventResponseMap.get("invPayload")
									+ "\"}");
						} else {
							pw.println(
									"{\"statusCode\":400,\"message\":\"Either invalid data was sent or no data was sent at all.");
						}
					}
				} else {
					if (pushEventResponseMap != null) {
						pw.println(
								"{\"statusCode\":405,\"message\":\"" + pushEventResponseMap.get("invMethod") + "\"}");
					} else {
						pw.println("{\"statusCode\":405,\"message\":\"This method is not allowed.\"}");
					}
				}
			} else {
				response.setStatus(HttpServletResponse.SC_OK);
				if (pushEventResponseMap != null) {
					pw.println("{\"statusCode\":202,\"message\":\"" + pushEventResponseMap.get("disabled") + "\"}");
				} else {
					pw.println(
							"{\"statusCode\":202,\"message\":\"We are sorry. This feature is currently disabled.\"}");
				}
			}

		} catch (Exception e) {
			logger.error("Exception occured while push event", e);
			response.setStatus(HttpServletResponse.SC_OK);
			if (pw == null) {
				pw = response.getWriter();
			}
			pw.println("{\"statusCode\":500,\"message\":\"Push Event Failed with exception: " + e.getClass().getName()
					+ "}");
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
		return null;
	}

	public void sendNotification(Map<String, Object> pushNotificationDetailsMap, HttpServletResponse response,
			Map<String, Object> pushNotificationEventMap) {
		PrintWriter printWriter = null;
		List<String> pushNotificationErrorMessages = null;
		String cleverTapResponseForNotification = null;
		ObjectMapper mapper = null;
		Writer writer = null;
		try {
			printWriter = response.getWriter();
			pushNotificationErrorMessages = new ArrayList<String>();
			if (MapUtils.isNotEmpty(pushNotificationDetailsMap)) {
				boolean isValid = cleverTapPushEventService.validatePushNotificationPayLoad(pushNotificationDetailsMap,
						pushNotificationErrorMessages, pushNotificationEventMap);
				if (isValid) {
					cleverTapResponseForNotification = cleverTapPushEventService.pushNotification(pushNotificationDetailsMap);
					if(StringUtils.isEmpty(cleverTapResponseForNotification)) {
						if (pushNotificationEventMap != null) {
							printWriter.println("{\"statusCode\":500,\"message\":\""
									+ pushNotificationEventMap.get("failureResponse") + "\"}");
						} else {
							printWriter.println(
									"{\"statusCode\":500,\"message\":\"Sorry Unable To Get Response From Clevertrap.\"}");
						}
					}else {
						mapper = new ObjectMapper();
						writer = new StringBuilderWriter(new StringBuilder(1024));
						JsonFactory jsonFactory = new JsonFactory();
						JsonGenerator jsonGenerator = jsonFactory.createGenerator(writer);
						jsonGenerator.useDefaultPrettyPrinter();
						mapper.writeValue(jsonGenerator, cleverTapResponseForNotification);
						printWriter.println(""+writer.toString());
					}
				} else {
					response.setStatus(HttpServletResponse.SC_OK);
					if (CollectionUtils.isNotEmpty(pushNotificationErrorMessages)) {
						printWriter.println("{\"statusCode\":400,\"message\":\"" + pushNotificationErrorMessages.get(0) + "\"}");
					} else {
						printWriter.println(
								"{\"statusCode\":400,\"message\":\"Either invalid data was sent or no data was sent at all.");
					}

				}
			} else {
				response.setStatus(HttpServletResponse.SC_OK);
				printWriter.println("{\"statusCode\":400,\"message\":\"There is no data sent as part of request payload\"}");
			}
		} catch (Exception e) {
			logger.error("Exception occured while push notification", e);
			response.setStatus(HttpServletResponse.SC_OK);
			if (printWriter == null) {
				try {
					printWriter = response.getWriter();
				} catch (Exception ex) {
					logger.error("Exception occured while creating printwriter reference",ex);
				}
			}
			printWriter.println("{\"statusCode\":500,\"message\":\"Push notification Failed with exception: " + e.getClass().getName()
					+ "}");
		}
		finally{
			if(printWriter != null) {
				printWriter.close();
			}
		}

	}
}
