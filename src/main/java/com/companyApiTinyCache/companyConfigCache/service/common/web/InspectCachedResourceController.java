package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedResource;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.zip.GZIPOutputStream;

public class InspectCachedResourceController extends AbstractController {

    public InspectCachedResourceController() {
        setCacheSeconds(0);
    }

    private ApplicationContext getContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.mvc.AbstractController#handleRequestInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String beanName = request.getParameter("beanName");
        ApplicationContext ctx = getContext();
        CachedResource cachedResource = (CachedResource) ctx.getBean(beanName);
        String description = cachedResource.getResourceDescription();
        String contentType = cachedResource.getResourceContentType();
        if (("false".equalsIgnoreCase(request.getParameter("json")) && "application/json".equals(contentType)) || description == null || "null".equalsIgnoreCase(description)
                || "<null>".equalsIgnoreCase(description)) {
            contentType = "text/plain";
        }
        response.setContentType(contentType);
        boolean isCompress = false;
        if (description != null && description.length() > 10000) {
            String encodings = request.getHeader("Accept-Encoding");
            if (encodings != null && encodings.indexOf("gzip") != -1) {
                isCompress = true;
            }
        }
        PrintWriter op = null;
        if (isCompress) {
            // Go with GZIP
            response.setHeader("Content-Encoding", "gzip");
            OutputStream out = new GZIPOutputStream(response.getOutputStream());
            op = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out)));
        } else {
            op = response.getWriter();
        }
        op.println("" + description);
        op.close();
        return null;
    }

}
