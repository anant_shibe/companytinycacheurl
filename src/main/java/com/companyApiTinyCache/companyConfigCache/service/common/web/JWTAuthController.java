package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.exceptions.JWTException;
import com.companyApiTinyCache.companyConfigCache.service.common.service.JWTService;
import com.companyApiTinyCache.companyConfigCache.service.common.service.JWTServiceImpl;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * End Points to talk to JWTAuthService
 * 
 * @author sudhanshu
 *
 */
@ClassExcludeCoverage
public class JWTAuthController extends AbstractController {

	private CachedProperties commonCachedProperties;
	private JWTService jwtService;

	public JWTAuthController(CachedProperties pcommonCachedProperties, JWTServiceImpl pjwtService) {
		commonCachedProperties = pcommonCachedProperties;
		jwtService = pjwtService;
	}

	@RequestMapping(value = "/getToken")
	public void generateToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String token = jwtService.getToken(null);
		String tokenString = "{ \"token\": \"" + token +  "\"}";
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(tokenString);
		out.flush();
	}

	@RequestMapping(value = "/verifyToken")
	public void verifyToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			jwtService.validateToken(request);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (JWTException ex) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		}
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getRequestURI().contains("getToken")) {
			generateToken(request, response);
		} else {
			verifyToken(request, response);
		}
		return null;
	}

}
