package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class PgFeeServiceImpl implements PgFeeService {

    private static final String PG_FEE = "ct.common.gatewayfee";

    private static final String PG_FEE_TAX = "ct.common.gatewayfee.tax.percent";

    private static final Log log = LogFactory.getLog(PgFeeServiceImpl.class);

    private CachedProperties cachedProperties;

    public PgFeeServiceImpl(CachedProperties cachedProperties) {
        this.cachedProperties = cachedProperties;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cleartrip.common.service.PgFeeService#getPgFee()
     * 
     * Method that returns the Payment type to PG Fee Map. Currently read from a property file The caller will use the data to calculate the payment gateway fee
     */
    @Override
    public Map<String, Map<String, Double>> getPgFee(String prod) {
        Map<String, Map<String, Double>> pgFeeMap = new LinkedHashMap<String, Map<String, Double>>();
        Map<String, Map<String, Map<String, String>>> prodPgFee = null;
        Map<String, Map<String, String>> pgFee = null;
        String pgFeeJson = cachedProperties.getPropertyValue(PG_FEE);

        try {
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            prodPgFee = mapper.readValue(pgFeeJson, Map.class);
        } catch (Exception e) {
            log.error("Error in reading PG Fee Json " + pgFeeJson, e);
        }

        if (prodPgFee != null) {
            if (StringUtils.isNotEmpty(prod)) {
                pgFee = prodPgFee.get(prod);
            } else {
                pgFee = prodPgFee.get("air"); // by default, get pgFee for air
            }
            for (String paymentType : pgFee.keySet()) {
                Map<String, Double> feeMap = new HashMap<String, Double>();
                Map<String, String> fee = pgFee.get(paymentType);

                String percentage = fee.get("p");
                String fixed = fee.get("f");

                if (!StringUtils.isEmpty(percentage)) {
                    double percentageVal = Double.parseDouble(percentage);
                    feeMap.put("p", percentageVal);
                } else if (!StringUtils.isEmpty(fixed)) {
                    double fixedVal = Double.parseDouble(percentage);
                    feeMap.put("f", fixedVal);
                }

                pgFeeMap.put(paymentType, feeMap);
            }
        }

        return pgFeeMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cleartrip.common.service.PgFeeService#getPgFeeTax()
     */
    @Override
    public double getPgFeeTaxPercent() {
        double pgFeeTax = 0.0;
        String pgFeeTaxStr = cachedProperties.getPropertyValue(PG_FEE_TAX);
        pgFeeTax = StringUtils.isNotEmpty(pgFeeTaxStr) ? Double.valueOf(pgFeeTaxStr) : 0.0;
        return pgFeeTax;
    }
}
