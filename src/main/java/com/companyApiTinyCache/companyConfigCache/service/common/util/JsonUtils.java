package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.StringWriter;

/**
 * Json Utility Class, uses fasterxml package of Jackson to
 * serialization/deserialization.
 * 
 * @author tj
 *
 */
@ClassExcludeCoverage
public final class JsonUtils {

	private static final Log LOGGER = LogFactory.getLog(JsonUtils.class);

	public static final ObjectMapper MAPPER = getNewObjectMapper();

	public static ObjectMapper getObjectMapper() {
		return MAPPER;
	}

	public static <T> T toObject(String json, Class<T> clazz) {
		long start = 0;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Input Json : " + json);
			start = System.currentTimeMillis();
		}

		T object = null;
		try {
			object = toObject_v2(json, clazz);
			// object = mapper.readValue(json, clazz);
		} catch (IOException e) {
			LOGGER.error("Exception while Deserializing Json to Object(" + clazz.getName() + ")\nJson: " + json, e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Time taken to bind Json to Object(" + clazz + ") : " + (System.currentTimeMillis() - start)
					+ "ms.");
		}
		return object;
	}
	
	public static <T> T toObject_v2(String json, Class<T> clazz) throws JsonProcessingException, IOException {
			return getObjectMapper()
					.readerFor(clazz)
					.readValue(json);
	}

	public static String toJson(Object object) {
		return toJson(object, false, MAPPER);
	}

	public static String toJson(Object object, boolean prettify) {
		return toJson(object, prettify, MAPPER);
	}

	public static String toJson(Object object, ObjectMapper mapper) {
		return toJson(object, false, mapper);
	}

	public static String toJson(Object object, boolean prettify, ObjectMapper mapper) {
		long start = 0;
		if (LOGGER.isDebugEnabled()) {
			start = System.currentTimeMillis();
		}

		StringWriter writer = new StringWriter();
		if (mapper == null) {
			mapper = getObjectMapper();
		}
		try {
			if (prettify) {
				mapper.writerWithDefaultPrettyPrinter().writeValue(writer, object);
			} else {
				mapper.writer().writeValue(writer, object);
			}
		} catch (IOException e) {
			LOGGER.error("Exception while Serializing Object(" + object + ") to Json.", e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Time taken to serialize Object(" + object + ") to Json: "
					+ (System.currentTimeMillis() - start) + "ms.");
		}
		return writer.toString();
	}

	private static ObjectMapper getNewObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		LOGGER.info("New ObjectMapper Created => " + mapper);
		mapper.registerModule(new Jdk8Module());
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setSerializationInclusion(Include.NON_NULL);
		return mapper;
	}

	public static <T> T toMap(String json, TypeReference<T> typeRef) {
		long start = 0;
		if (LOGGER.isDebugEnabled()) {
			start = System.currentTimeMillis();
		}

		T value = null;
		try {
			value = MAPPER.reader(typeRef).readValue(json);
		} catch (IOException e) {
			LOGGER.error("Exception while Deserializing Json to Map(" + typeRef.getType() + ")\nJson: " + json, e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Time taken to bind Json to Map of Type(" + typeRef.getType() + ") : "
					+ (System.currentTimeMillis() - start) + "ms.");
		}

		return value;
	}
}
