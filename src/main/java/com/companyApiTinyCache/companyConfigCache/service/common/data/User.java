package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;

/**
 * Refer UserProfile Class in common
 * 
 * @author Ulhas
 * 
 */
@Deprecated
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userId;
    private String userName;
    private String userFirstName;
    private String userLastName;

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFirstName() {
        return this.userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return this.userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }
}
