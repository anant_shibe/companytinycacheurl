/**
 * Created on Jan 6, 2008
 */
package com.companyApiTinyCache.companyConfigCache.service.common.notification;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;
import java.util.Map;

/**
 * Class capable of formatting the messages.
 * 
 * @author sanjeev desai
 * 
 *         Copyright 2008 Cleartrip Travel Services, Inc. All rights reserved. CLEARTRIP PROPRIETART/CONFIDENTIAL
 */
public class MessageFormatterImpl implements MessageFormatter {
    private static final Log logger = LogFactory.getLog(MessageFormatterImpl.class);

    VelocityEngine velocityEngine = new VelocityEngine();

    @Override
    /**
     * This method formats the string passed using Velocity Engine.
     *
     * @param unformattedStringMessage
     * @param values Map<String, String>
     *
     * @return the formatted string
     */
    public String formatMessage(String unformattedStringMessage, Map<String, String> values) throws MessageFormatException {
        // logger.info("Entered formatMessage() method");
        VelocityContext velocityContext = new VelocityContext(values);
        StringWriter sw = new StringWriter();
        try {
            // TODO: DO I need to check for return boolean value of velocityEngine.evaluate() API.
            velocityEngine.evaluate(velocityContext, sw, "message formatter", unformattedStringMessage);
        } catch (Exception e) {
            logger.error("Failed to format message: " + unformattedStringMessage, e);
            throw new MessageFormatException("failed to format message: " + unformattedStringMessage, e);
        }
        // logger.info("Leaving formatMessage() method");
        return sw.toString();
    }

    /**
     * @param velocityEngine
     *            the velocityEngine to set
     */
    public void setVelocityEngine(VelocityEngine ve) {
        velocityEngine = ve;
        try {
            ve.init();
        } catch (Exception e) {
            logger.error("Velocity Engines init() method failed. ", e);
        }
    }

}
