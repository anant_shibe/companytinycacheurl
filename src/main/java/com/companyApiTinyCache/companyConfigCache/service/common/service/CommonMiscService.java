package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.data.UserProfile;

import javax.servlet.http.HttpServletRequest;

/**
 * @author cleartrip
 */
public interface CommonMiscService {

    void subscribeToNewsLetters(UserProfile userProfile, boolean enableSubscription);

    void subscribeToNewsLetters(int peopleId, boolean enableSubscription) throws Exception;

    void subscribeToNewsLetters(String emailAddress, boolean enableSubscription, HttpServletRequest request) throws Exception;

}
