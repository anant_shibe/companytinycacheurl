package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.AffiliateLookup;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
*
* @author amith
*
*/
public class LookUpDataHolder {

    @JsonProperty("lookups")
    private List<AffiliateLookup> lookups;

    public LookUpDataHolder() {
        lookups = new ArrayList<AffiliateLookup>(1);
    }

    @JsonProperty("lookups")
    public List<AffiliateLookup> getLookups() {
        return lookups;
    }

    @JsonProperty("lookups")
    public void setLookups(List<AffiliateLookup> lookups) {
        this.lookups = lookups;
    }

}
