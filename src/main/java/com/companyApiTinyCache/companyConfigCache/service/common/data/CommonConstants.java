package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.VmResourceInfo;

/**
 *
 */
public final class CommonConstants {

    private CommonConstants() {
    }
    /**
     *
     */
    public enum CommonTemplate implements VmResourceInfo {

        MAIL_NEW_API_REQUEST_SUBJECT_VM("mail/new_api_affiliate_request_Subject.vm", TemplateType.REGULAR),
        MAIL_NEW_API_REQUEST_BODY_VM("mail/new_api_affiliate_request_Body.vm", TemplateType.REGULAR),
        CONATCT_US_SUBJECT_VM("mail/contact_us_subject.vm", TemplateType.REGULAR), CONTACT_US_BODY_VM("mail/contact_us_body.vm", TemplateType.REGULAR),
        EMAIL_CONTENT_VM("mail/emailContent.vm", TemplateType.REGULAR), RIYA_MAIL_CONTENT("mail/riya_mail_content.vm", TemplateType.REGULAR),
        EMAIL_SELECTED_FLIGHTS_CONTENT_VM("mail/emailSelectedFlights.vm", TemplateType.REGULAR), EMAIL_CHECK_AVAILABILITY_VM("mail/emailCheckAvailability.vm", TemplateType.REGULAR),
        EMAIL_CHECK_AVAILABILITY_CONFIRMATION_VM("mail/checkAvailabilityConfirmationEmail.vm", TemplateType.REGULAR);

        private CommonTemplate(String ptemplateName, TemplateType ptemplateType) {
            resourceName = ptemplateName;
            templateType = ptemplateType;
        }

        private String resourceName;

        private TemplateType templateType;

        /**
         * Getter for resourceName.
         *
         * @return the resourceName
         */
        public String getResourceName() {
            return resourceName;
        }

        /**
         * Getter for templateType.
         *
         * @return the templateType
         */
        public TemplateType getTemplateType() {
            return templateType;
        }
    }

    public static final String PREFERRED_COUNTRY_COOKIE = "pct";

    public static final String PREFERRED_CURRENCY_COOKIE = "pcr";

    public static final String SELECTED_COUNTRY = "ct";

    public static final String SELECTED_CURRENCY = "cr";

    public static final String DISP_CURRENCY = "disp_currency";

    public static final String SELL_CURRENCY = "sell_currency";

    public static final String COUNTRY = "country";

    public static final String DISPLA_CURRENCY_TEXT = "displayCurrencyText";

    public static final String LANG = "lang";

    public static final String CURRENCY_PREF = "currency-pref";

    public static final String UTF_8 = "UTF-8";

    public static final String HOTEL_SEARCH_RESULTS_PAGE_CODE = "hsr";

    public static final String LANG_PREF = "lang_pref";

    public static final String LANGUAGE = "l";

    public static final String BASELINE = "baseLine";
    
    public static final String SECRET_KEY = "C13@rtr1pT3xtCry";

    /**
     *
     */
    public static enum UserAPITemplate implements VmResourceInfo {

        // CREATE_COMPANY_TEMPLATE("CompanyCreateTemplate.vm",TemplateType.XML),
        // EDIT_COMPANY_TEMPLATE("CompanyEditTemplate.vm",TemplateType.XML),
        // REGISTER_USER_TEMPLATE("UserRegisterRequestTemplate.vm",TemplateType.XML),
        // UPDATE_USER_TEMPLATE("UserUpdateTemplate.vm",TemplateType.XML),
        // CREATE_USER_TEMPLATE("UserRegisterTemplate.vm",TemplateType.XML),
        // ADD_TRAVELLER_TEMPLATE("AddTravellerTemplate.vm",TemplateType.XML),
        // UPDATE_PASSWORD_TEMPLATE("UpdatePasswordTemplate.vm",TemplateType.XML);
        USER_AUTHENTICATION_TEMPLATE("userAuthenticationTemplate.vm", TemplateType.REGULAR), PEOPLE_MERGE_TEMPLATE("PeopleMergeTemplate.vm", TemplateType.REGULAR);

        private UserAPITemplate(String templateName, TemplateType templateType) {
            this.resourceName = templateName;
            this.templateType = templateType;
        }

        private String resourceName;
        private TemplateType templateType;

        @Override
        public TemplateType getTemplateType() {
            return this.templateType;
        }

        @Override
        public String getResourceName() {
            return this.resourceName;
        }

    }

    public enum UserAPIConnectorTimeouts {
    	COMMON_TIMEOUT("ct.common.userapi.connector.timeout", 15000), USER_REGISTER("ct.common.userapi.connector.register.timeout", 15000), PEOPLE("ct.common.userapi.connector.people.timeout", 15000),
    	AUTHENTICATION("ct.common.userapi.authentication.timeout", 10000), IMPORT("ct.common.userapi.import.timeout", 15000);
    	private String property;
    	private Integer timeout;
		private UserAPIConnectorTimeouts(String property, Integer timeout) {
			this.property = property;
			this.timeout = timeout;
		}
		public String getProperty() {
			return property;
		}
		public Integer getTimeout() {
			return timeout;
		}
    	
    }
}
