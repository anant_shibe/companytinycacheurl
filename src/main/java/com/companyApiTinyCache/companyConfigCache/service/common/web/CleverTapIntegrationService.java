package com.companyApiTinyCache.companyConfigCache.service.common.web;

import java.util.List;
import java.util.Map;

/**
 * @author parvatkumar
 *
 */
public interface CleverTapIntegrationService {
    String createOrUpdateEvent(Map<String,Object> eventDataDetails) throws Exception;
    boolean validateEventDetails(Map<String,Object> eventDetailsMap,List<String> errorMessages,Map<String,Object> pushEventResponseJson);
}
