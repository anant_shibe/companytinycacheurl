package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import com.companyApiTinyCache.companyConfigCache.service.common.analytics.cache.AnalyticsCachingService;
import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.ConnectorStats;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestClient;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestRequest;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.ApiType;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * AnalyticsAttributesGetter.
 *
 */
@ClassExcludeCoverage
public class AnalyticsAttributesGetter implements DisposableBean {

    private static final Map<String, String> EMPTY_ATTRIBUTES = Collections.emptyMap();

    private static final Log LOGGER = LogFactory.getLog(AnalyticsAttributesGetter.class);

    private final CachedProperties cachedProperties;

    private final RestClient restClient;

    private final AnalyticsCachingService analyticsCachingService;

	private static final String CT_SERVICES_AIR_ANALYTICS_CACHING_ENABLED = "ct.services.air.analytics.caching.enabled";

	private static final String CT_SERVICES_AIR_ANALYTICS_ENABLED_CHANNELS = "ct.services.air.analytics.channels.enabled";

    public AnalyticsAttributesGetter(CachedProperties pcachedProperties, RestClient prestClient, AnalyticsCachingService analyticsCachingService) {
        cachedProperties = pcachedProperties;
        restClient = prestClient;
		this.analyticsCachingService = analyticsCachingService;
	}

    @SuppressWarnings({ "rawtypes" })
	public Map getCountAttributesForUser(String apacheCookie, CommonEnumConstants.SourceType sourceType) {
		Optional<Map> analyticsAttributes = Optional.empty();
    	List<String> enabledChannels = Arrays.asList((cachedProperties.getPropertyValue(CT_SERVICES_AIR_ANALYTICS_ENABLED_CHANNELS, "B2C, ACCOUNT, MOBILE")).split(","));
    	if(enabledChannels.stream().anyMatch(s -> s.equalsIgnoreCase(sourceType.name()))) {
			if (cachedProperties.getBooleanPropertyValue(CT_SERVICES_AIR_ANALYTICS_CACHING_ENABLED, false)) {
				analyticsAttributes = analyticsCachingService.getAnalyticsAttributeFromCache(apacheCookie);
			}
			if(!analyticsAttributes.isPresent()) {
				analyticsAttributes = Optional.ofNullable(getCountAttributesFromAnalytics(apacheCookie));
			}
		}
		return analyticsAttributes.orElse(EMPTY_ATTRIBUTES);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map getCountAttributesFromAnalytics(String apacheCookie) {
		String userTagUrl = cachedProperties.getPropertyValue("ct.services.raterule.usertag.url");
        RestRequest restRequest = new RestRequest(userTagUrl);
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("apachecookie", apacheCookie);
        restRequest.setHeaders(headers);
        restRequest.setTimeout(cachedProperties.getIntPropertyValue("ct.services.raterule.usertag.timeout", 500));
        RestResponse restResponse = null;

        int responseCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		String restResponseMessage = null;
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		if (connectorStat != null) {
			connectorStat.addNewApiRequest(ApiType.ANALYTICS, null, "LoadUserData", null, userTagUrl, null);
		}
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("AnalyticsAttributeGetter.getCountAttributesFromAnalytics Request: %s", restRequest));
			}

			BaseStats.taskStarted("Get user search book counters from Analytics");
			restResponse = restClient.get(restRequest);
			responseCode = restResponse.getStatus();
			restResponseMessage = restResponse.getContent();
			BaseStats.taskFinished("Get user search book counters from Analytics");

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("AnalyticsAttributeGetter.getCountAttributesFromAnalytics Response: %s", restResponse));
			}

			if (restResponse.isOk()) {
				Map<String, Map<String, Object>> responseAttributes = JsonUtil.getObjectMapper().readValue(restResponse.getContent().trim(), Map.class);
				if (responseAttributes.containsKey("air")) {
					if(cachedProperties.getBooleanPropertyValue(CT_SERVICES_AIR_ANALYTICS_CACHING_ENABLED, false)) {
						analyticsCachingService.setAnalyticsAttributeInCache(apacheCookie, responseAttributes.get("air"));
					}
					return responseAttributes.get("air");
				} else {
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug(String.format("AnalyticsAttributesGetter.getCountAttributesForUser 'air' key is not found. Response received from URL: '%s' is '%s' for apcheCookie: '%s'",
							userTagUrl, restResponse, apacheCookie));
					}
					return EMPTY_ATTRIBUTES;
				}
			} else {
				LOGGER.error(String.format("AnalyticsAttributesGetter.getCountAttributesForUser Non success status code received. Response received from URL: '%s' is '%s' for apacheCookie: '%s'",
					userTagUrl, restResponse, apacheCookie));
				return EMPTY_ATTRIBUTES;
			}
		} catch (Exception e) {
			restResponseMessage = e.getMessage();
			LOGGER.error(String.format("AnalyticsAttributesGetter.getCountAttributesForUser Exception occurred: %s. Response received from URL: '%s' is '%s' for apacheCookie: '%s'",
				e.getMessage(), userTagUrl, restResponse, apacheCookie));
			return EMPTY_ATTRIBUTES;
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.ANALYTICS, "LoadUserData", responseCode, restResponseMessage, true);
			}
		}
	}

	public Map<String, Object> getCountAttributesFromAnalytics(Map<String, String> queryParams, Map<String, String> headers) {
		Map<String, Object> emptyMap = new HashMap<String, Object>();
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		try {
			String userTagUrl = cachedProperties.getPropertyValue("ct.services.raterule.usertag.url");
			boolean isLoggingEnabled = cachedProperties.getBooleanPropertyValue("ct.common.analytics.debug.enabled", false);
			RestRequest restRequest = new RestRequest(userTagUrl);
			restRequest.setTimeout(cachedProperties.getIntPropertyValue("ct.services.raterule.usertag.timeout", 500));
			restRequest.setHeaders(headers);
			String queryString = getQueryString(queryParams);
			restRequest.setQueryString(queryString);
			RestResponse restResponse = null;

			if (connectorStat != null) {
				connectorStat.addNewApiRequest(ApiType.ANALYTICS, null, "LoadUserData", null, userTagUrl, null);
			}

			BaseStats.taskStarted("Get user search book counters from Analytics");
			long starttime = System.currentTimeMillis();
			restResponse = restClient.get(restRequest);
			long endtime = System.currentTimeMillis();
			BaseStats.taskFinished("Get user search book counters from Analytics");
			if (isLoggingEnabled) {
				LOGGER.error("Analytics Request Attributes :::" );
				LOGGER.error("queryParams ::" + queryParams);
				LOGGER.error("headers ::" + headers);
				LOGGER.error("Response::" + restResponse);
				LOGGER.error("time takes::" + (endtime - starttime));
				
			}

			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.ANALYTICS, "LoadUserData", restResponse.getStatus(), restResponse.getContent(), true);
			}

			if (restResponse.isOk()) {
				Map<String, Map<String, Object>> responseAttributes = JsonUtil.getObjectMapper().readValue(restResponse.getContent().trim(), Map.class);
				if (responseAttributes.containsKey("air")) {
					return responseAttributes.get("air");
				} else {
					LOGGER.error("Unable to fetch air related attributes from response :::" + restResponse);
					return emptyMap;
				}
			} else {
				return emptyMap;
			} 
		}  catch (Exception exception) {
			LOGGER.error("Analytics call failed due to ::" + exception.getMessage() + "error.");
			return emptyMap;
		}
	}

	protected String getQueryString(Map<String, String> queryParams) {
		StringBuilder queryStringBuilder = new StringBuilder();
        if(MapUtils.isNotEmpty(queryParams)) {
        	for(String key : queryParams.keySet()) {
        		queryStringBuilder.append("&");
        		queryStringBuilder.append(key);
        		queryStringBuilder.append("=");
        		queryStringBuilder.append(queryParams.get(key));
        	}
        	return queryStringBuilder.substring(1);
        	}
        return StringUtils.EMPTY;
        }

    @Override
    public void destroy() throws Exception {
    }
}
