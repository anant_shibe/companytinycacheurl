package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.data.AccessRoleDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.data.UserLoginProfile;
import com.companyApiTinyCache.companyConfigCache.service.common.data.UserProfile;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public interface AuthenticationService {

    /**
     * This method sets the proxy and authenticates the same.
     */
    void setProxy();

    // /**
    // * Gets the acubekey from the Acube server.
    // * @return byte[]
    // * @throws MalformedURLException
    // * @throws IOException
    // */
    // public byte[] getAcubeKey() throws MalformedURLException, IOException;

    /**
     * Creates the checksum from the given decrypted cookie by using SHA-1 hashing technique and returns the hex format.
     *
     * @param decryptedCookie
     *            String
     * @return String
     * @throws NoSuchAlgorithmException
     *             Throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     *             Throws UnsupportedEncodingException
     */
    String getChecksum(String decryptedCookie) throws NoSuchAlgorithmException, UnsupportedEncodingException;

    /**
     * ** Method to get user details from the cookie.
     *
     * @param authCookie
     *          Cookie
     * @return HashMap< String, String>
     */
    HashMap<String, String> getPersonFromCookie(Cookie authCookie);

    /**
     * This method gets the username, if user first name exists else it returns the user emailid.
     *
     * @param user
     *          HashMap<String, String>
     * @return String
     */
    String getScreenName(HashMap<String, String> user);

    /**
     * Gets the login url from the properties file.
     *
     * @return String
     */
    String getLoginUrl();

    /**
     * returns a boolean value if the securityenable is set in the runtime properties.
     *
     * @return boolean
     */
    boolean isSecurityEnable();

    /**
     * Returns the auth cookie name set in the runtime properties.
     *
     * @return String
     */
    String getAuthCookieName();

    /**
     * Checks whether the user is authorized to view the specific page.
     * @param person
     *          person to be authenticated
     * @param allAccessRoles
     *          List of AccessRoleDetail
     * @param currAccessRole
     *          Current access role
     * @return boolean
     */
    boolean isUserAuthenticated(Map<String, String> person, List<AccessRoleDetail> allAccessRoles, String currAccessRole);

    /**
     * Generates the cookie for a new user.
     *
     * @param userProfile
     *            UserProfile
     * @param accessRoles
     *            Collection<AccessRoleDetail>
     * @param domainName
     *            Domainname
     * @param companyId
     *            Company ID
     * @return String
     * @throws Exception
     *            Exception
     */
    Cookie generateCookie(UserProfile userProfile, Collection<Integer> accessRoles, String domainName, int companyId) throws Exception;

    /**
     * Gets the cookie from the HttpServletRequest else returns null.
     *
     * @param httpReq
     *            HttpServletRequest
     * @return String
     */
    Cookie getACubeAuthCookieFromRequest(HttpServletRequest httpReq);

    /**
     * This method modifies the authcookie, by updating the last accessed timestamp, checksum, and the cookie expiry timestamp.
     *
     * @param authCookie
     *            Cookie
     * @param domain
     *            Domain
     * @return Cookie
     */
    Cookie modifyCookie(Cookie authCookie, String domain);

    boolean isUserLoggedIn(Cookie authCookie) throws NoSuchAlgorithmException, IOException;

    /**
     * This method gets the unconfirmed cookie from the request.
     *
     * @param httpReq
     *            HttpServletRequest
     * @return Cookie
     */
    Cookie getUnconfirmedCookieFromRequest(HttpServletRequest httpReq);

    Cookie generateUserIdCookie(UserProfile userProfile, String domainName);

    /**
     * Get the unconfirmed user.
     *
     * @param unconfirmedEmail
     *          unconfirmedEmail
     * @param companyId
     *          companyId
     * @param roleId
     *          roleId
     * @return UserProfile
     */
    UserProfile getUnconfirmedUser(String unconfirmedEmail, int companyId, int roleId);

    /**
     * Generate the unconfirmedCookie.
     *
     * @param userProfile
     *          UserProfile
     * @param domainName
     *          DomainName
     * @return Cookie
     */
    Cookie generateUnconfirmedCookie(UserProfile userProfile, String domainName);

    int getUserIdFromCookie(Cookie authCookie);

    String getUserName(HashMap<String, String> user);

    /**
     * Authenticate the user and return appropriate message.
     *
     * @param username
     *          Username
     * @param password
     *          Password
     * @param request
     *          HttpServletRequest
     * @param retCookie
     *          Cookie array
     * @return String
     * @throws IOException
     *          Throws IOException
     * @throws XPathExpressionException
     *          Throws XPathExpressionException
     */
    UserProfile authenticateUser(String username, String password, HttpServletRequest request, Cookie[] retCookie) throws XPathExpressionException, IOException;

    /**
     *
     * @param username
     *          UserName
     * @param companyId
     *          CompanyId
     * @param roleId
     *          RoleId
     * @param product
     *            Product
     * @param defaultEmailOption
     *          defaultEmailOption
     * @param domainName
     *          DomainName
     * @return UserProfile
     * @throws IOException
     *          Throws IOException
     * @throws XPathExpressionException
     *          Throws XPathExpressionException
     */
    UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, String domainName) throws IOException, XPathExpressionException;

    UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, String domainName, SourceType source) throws IOException,
            XPathExpressionException;

    UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, SourceType source) throws IOException, XPathExpressionException;

    UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption) throws IOException, XPathExpressionException;

    UserProfile userRegistration(UserLoginProfile userLoginProfile) throws IOException, XPathExpressionException;

    /**
     * Get the user profile from user name.
     *
     * @param username
     *          Username
     * @param source
     *          Source
     * @param caller
     *          Caller
     * @param request TODO
     * @return UserProfile
     * @throws IOException
     *          Throws IOException
     */
    UserProfile fetchUserProfile(String username, SourceType source, String caller, HttpServletRequest request) throws IOException;

    /**
     * Authenticate the user and return appropriate message.
     *
     * @param username
     *          Username
     * @param password
     *          Password
     * @param request
     *          HttpServletRequest
     * @param domain
     *          Domain
     * @param cookiesList
     *            - array list
     *
     * @return String
     * @throws IOException
     *          Throws IOException
     * @throws XPathExpressionException
     *          Throws XPathExpressionException
     */
    UserProfile authenticateUser(String username, String password, HttpServletRequest request, String domain, List<Cookie> cookiesList) throws XPathExpressionException, IOException;

    UserProfile authenticateUser(String username, String password, HttpServletRequest request, String domainName, List<Cookie> cookies, String string, String sourceString)
            throws XPathExpressionException, IOException;

    UserProfile getUnconfirmedUser(String unconfirmedEmail, int companyId, int roleId, String source, String caller);

    UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, String domainName, String source, String caller) throws IOException,
            XPathExpressionException;

    UserProfile userRegistration(UserLoginProfile userLoginProfile, String source, String caller) throws IOException, XPathExpressionException;

    UserProfile userRegistration(String username, int companyId, int roleId, String product, boolean defaultEmailOption, String domainName, String source, String caller, String host)
            throws IOException, XPathExpressionException;

    Cookie generateUserTokenCookie(Integer userToken) throws Exception;

    Cookie getUserTokenCookie(HttpServletRequest httpReq);

    String getUserToken(Cookie userTokenCookie);

    int getB2BAuthCookieAge(String source);

    Cookie generateB2BAuthCookie(String source);

    Cookie getB2BAuthCookie(HttpServletRequest httpReq);

    Cookie modifyB2BAuthCookie(Cookie b2bAuthCookie, String source);

}
