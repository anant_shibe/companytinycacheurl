package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.HashMap;

public class DepositAccount implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private int accountId;

    @JsonIgnore
    private String password;

    private int affiliateId;

    @JsonIgnore
    private String status;

    @JsonIgnore
    private String balanceType; // CR, DR

    @JsonIgnore
    private String accountName;

    @JsonIgnore
    private boolean creditAllowed;

    @JsonIgnore
    private double creditLimit;

    @JsonIgnore
    private double accountBalance;

    @JsonIgnore
    private String currencyCode;

    @JsonIgnore
    private String description;

    @JsonIgnore
    private String address1;

    @JsonIgnore
    private String city;

    @JsonIgnore
    private String state;

    @JsonIgnore
    private String country;

    @JsonIgnore
    private String zip;

    @JsonIgnore
    private double alert_balance;

    @JsonIgnore
    private String email_alert_to;

    @JsonIgnore
    private String sms_alert_to;

    @JsonIgnore
    private Deposit deposit;

    @JsonIgnore
    private HashMap<Integer, DepositSplitAmount> depositAccountSplitMap;

    @JsonIgnore
    public HashMap<Integer, DepositSplitAmount> getDepositAccountSplitMap() {
        return depositAccountSplitMap;
    }

    @JsonIgnore
    public void setDepositAccountSplitMap(HashMap<Integer, DepositSplitAmount> depositAccountSplitMap) {
        this.depositAccountSplitMap = depositAccountSplitMap;
    }

    @JsonIgnore
    public String getAddress1() {
        return address1;
    }

    @JsonIgnore
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @JsonIgnore
    public String getCity() {
        return city;
    }

    @JsonIgnore
    public void setCity(String city) {
        this.city = city;
    }

    @JsonIgnore
    public String getState() {
        return state;
    }

    @JsonIgnore
    public void setState(String state) {
        this.state = state;
    }

    @JsonIgnore
    public String getCountry() {
        return country;
    }

    @JsonIgnore
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonIgnore
    public Deposit getDeposit() {
        return deposit;
    }

    @JsonIgnore
    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    @JsonProperty("id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getAccountId() {
        return accountId;
    }

    @JsonProperty("id")
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    @JsonIgnore
    public double getAccountBalance() {
        return accountBalance;
    }

    @JsonIgnore
    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    @JsonIgnore
    public String getBalanceType() {
        return balanceType;
    }

    @JsonIgnore
    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    @JsonIgnore
    public String getStatus() {
        return status;
    }

    @JsonIgnore
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonIgnore
    public String getDescription() {
        return description;
    }

    @JsonIgnore
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("company_id")
    public int getAffiliateId() {
        return affiliateId;
    }

    @JsonProperty("company_id")
    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    @JsonIgnore
    public double getCreditLimit() {
        return creditLimit;
    }

    @JsonIgnore
    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    @JsonIgnore
    public String getAccountName() {
        return accountName;
    }

    @JsonIgnore
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @JsonIgnore
    public boolean isCreditAllowed() {
        return creditAllowed;
    }

    @JsonIgnore
    public void setCreditAllowed(boolean creditAllowed) {
        this.creditAllowed = creditAllowed;
    }

    @JsonIgnore
    public double getAlert_balance() {
        return alert_balance;
    }

    @JsonIgnore
    public void setAlert_balance(double alert_balance) {
        this.alert_balance = alert_balance;
    }

    @JsonIgnore
    public String getEmail_alert_to() {
        return email_alert_to;
    }

    @JsonIgnore
    public void setEmail_alert_to(String email_alert_to) {
        this.email_alert_to = email_alert_to;
    }

    @JsonIgnore
    public String getSms_alert_to() {
        return sms_alert_to;
    }

    @JsonIgnore
    public void setSms_alert_to(String sms_alert_to) {
        this.sms_alert_to = sms_alert_to;
    }

    @JsonIgnore
    public String getZip() {
        return zip;
    }

    @JsonIgnore
    public void setZip(String zip) {
        this.zip = zip;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonIgnore
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DepositAccount == false)
            return false;
        DepositAccount da = (DepositAccount) obj;
        if (accountId != da.getAccountId()) {
            return false;
        }
        return true;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

}
