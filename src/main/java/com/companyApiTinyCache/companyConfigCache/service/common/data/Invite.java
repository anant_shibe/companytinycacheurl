package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.Date;

public class Invite implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 8124992242320771342L;

    private int id;

    private String inviteeEmail;

    private int inviterId;

    private Date invitedDate;

    private Date acceptedDate;

    private Date abandonedDate;

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("invitee_email")
    public String getInviteeEmail() {
        return inviteeEmail;
    }

    @JsonProperty("invitee_email")
    public void setInviteeEmail(String inviteeEmail) {
        this.inviteeEmail = inviteeEmail;
    }

    @JsonProperty("inviter_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getInviterId() {
        return inviterId;
    }

    @JsonProperty("inviter_id")
    public void setInviterId(int inviterId) {
        this.inviterId = inviterId;
    }

    @JsonProperty("invited_date")
    public Date getInvitedDate() {
        return invitedDate;
    }

    @JsonProperty("invited_date")
    public void setInvitedDate(Date invitedDate) {
        this.invitedDate = invitedDate;
    }

    @JsonProperty("accepted_date")
    public Date getAcceptedDate() {
        return acceptedDate;
    }

    @JsonProperty("accepted_date")
    public void setAcceptedDate(Date acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    @JsonProperty("abandoned_date")
    public Date getAbandonedDate() {
        return abandonedDate;
    }

    @JsonProperty("abandoned_date")
    public void setAbandonedDate(Date abandonedDate) {
        this.abandonedDate = abandonedDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Invite == false)
            return false;
        Invite inv = (Invite) obj;

        if ((inviteeEmail != null || inv.getInviteeEmail() != null) && !GenUtil.equals(inviteeEmail, inv.getInviteeEmail())) {
            return false;
        }
        if (inviterId != inv.getInviterId()) {
            return false;
        }
        if ((invitedDate != null || inv.getInvitedDate() != null) && !GenUtil.equals(invitedDate, inv.getInvitedDate())) {
            return false;
        }
        if ((acceptedDate != null || inv.getAcceptedDate() != null) && !GenUtil.equals(acceptedDate, inv.getAcceptedDate())) {
            return false;
        }
        if ((abandonedDate != null || inv.getAbandonedDate() != null) && !GenUtil.equals(abandonedDate, inv.getAbandonedDate())) {
            return false;
        }
        return true;
    }

}
