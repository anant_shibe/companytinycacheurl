package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.io.Serializable;
import java.util.AbstractList;

/**
 * A simple unmodifiable list wrapping an array.
 *
 * @author Suresh
 *
 * @param <E>
 *            Type of list
 */
public class ArrayWrappingList<E> extends AbstractList<E> implements Serializable {

    private E[] items;

    public ArrayWrappingList(E[] array) {
        // Some Jazz to throw NPE if null array is passed
        int len = array.length;
        if (len >= 0) {
            items = array;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see AbstractList#get(int)
     */
    @Override
    public E get(int index) {
        return items[index];
    }

    /**
     * {@inheritDoc}
     *
     * @see java.util.AbstractCollection#size()
     */
    @Override
    public int size() {
        return items.length;
    }
}
