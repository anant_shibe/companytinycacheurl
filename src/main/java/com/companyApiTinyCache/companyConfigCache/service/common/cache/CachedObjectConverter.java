package com.companyApiTinyCache.companyConfigCache.service.common.cache;

/**
 * Indicates that a class has to be converted to String before inserting to the cachce and also to convert the String retrieved from the cache to Object representation.
 * 
 * @author sdesai
 * 
 */
public interface CachedObjectConverter {

    /**
     * Converts the string from the cache to object representation.
     * 
     * @param lineData
     *            to convert into object.
     */
    void convertToObject(String lineData);

    /**
     * Returns the string representation of the object to be cached.
     * 
     * @return String representation of the object to be cached
     */
    String convertToString();
}
