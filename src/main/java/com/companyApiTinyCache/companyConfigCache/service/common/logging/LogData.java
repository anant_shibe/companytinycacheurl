package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.UUID;

/**
 * Holds data which gets appended to all log statements.
 */
@ClassExcludeCoverage
public class LogData implements Serializable {
    private static final long serialVersionUID = 1L;

    private String requestId;
    private String hostName;
    private String hostPort;
    private String hostIp;
    private String uri;

    public static final String REQUEST_ID = "X-Uniq-ID";

    // This generates a fall back Request Id in case it is missing. Below is the format which includes ip addresses
    // for troubleshooting why it was missing:
    // <from_ip>-<source_ip>-<ip>-<id>
    // Below is info on individual fields in above:
    // <from_ip> : Ip from which the request has come just beyond the LB ip. This indicates the server which
    //             has failed to generate the request id. This is got from the ns-remote-addr which the LB adds.
    // <source_ip> : The Request ip address which will be usually the LB ip.
    // <ip> : ip of this machine
    // <id> : An id unique to this machine. UUID is used here.
    public static String generateRequestId(HttpServletRequest request, CachedProperties cachedProperties) {
        String fromIp = request.getHeader("ns-remote-addr");
        String sourceIp = request.getRemoteAddr();
        String ip = cachedProperties.getPropertyValue("ct.common.ip");
        String id = UUID.randomUUID().toString();
        String requestId = fromIp + '-' + sourceIp + '-' + ip + '-' + id;
        return requestId;
    }

    public LogData(HttpServletRequest request, CachedProperties cachedProperties) {
        setHostName(cachedProperties.getPropertyValue("ct.common.hostname"));
        setHostIp(cachedProperties.getPropertyValue("ct.common.ip"));
        setHostPort(cachedProperties.getPropertyValue("ct.common.port"));
        setUri(request.getRequestURI());
        String requestId = StringUtils.trimToNull(request.getHeader(REQUEST_ID));
        if (requestId == null) {
            requestId = generateRequestId(request, cachedProperties);
        }
        setRequestId(requestId);
    }

    public String asString() {
        StringBuilder builder = new StringBuilder();
        builder.append(StringUtils.isBlank(getHostName()) ? "<host>" : getHostName()).append(":").append(StringUtils.isBlank(getHostPort()) ? "<port>" : getHostPort()).append(" ")
                .append(StringUtils.isBlank(getHostIp()) ? "<ip>" : getHostIp()).append(" ").append(StringUtils.isBlank(getRequestId()) ? "<rId>" : getRequestId()).append(" ")
                .append(StringUtils.isBlank(getUri()) ? "<uri>" : getUri()).append(" ");
        return builder.toString();
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public void setHostPort(String hostPort) {
        this.hostPort = hostPort;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getHostName() {
        return hostName;
    }

    public String getHostPort() {
        return hostPort;
    }

    public String getHostIp() {
        return hostIp;
    }

    public String getUri() {
        return uri;
    }

    public String getRequestId() {
        return requestId;
    }
}
