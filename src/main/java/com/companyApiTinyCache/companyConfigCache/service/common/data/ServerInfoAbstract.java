package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;

public abstract class ServerInfoAbstract implements Serializable {

    private static final long serialVersionUID = 4552554523566243265L;

    private String name;

    private volatile long lastUpdated;

    private volatile long lastSaved;

    private boolean isDirty;

    public ServerInfoAbstract(String pname) {
        name = pname;
    }

    public String getName() {
        return name;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long plastUpdated) {
        lastUpdated = plastUpdated;
    }

    public long getLastSaved() {
        return lastSaved;
    }

    public void setLastSaved(long plastSaved) {
        lastSaved = plastSaved;
    }

    public boolean isDirty() {
        return isDirty;
    }

    public void setDirty(boolean pisDirty) {
        isDirty = pisDirty;
    }

}
