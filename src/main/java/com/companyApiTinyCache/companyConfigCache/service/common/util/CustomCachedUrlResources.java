package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CommonUrlResources;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.EnumMap;

public class CustomCachedUrlResources extends CachedUrlResources<CommonUrlResources> {

    private static final Log log = LogFactory.getLog(CustomCachedUrlResources.class);

    private static final String FOOTER_MARKER = "FooterContainer";

    private static final int FOOTER_MARKER_LEN = FOOTER_MARKER.length();

    public CustomCachedUrlResources(CachedProperties pcommonCachedProperties) {
        super(CommonUrlResources.class, pcommonCachedProperties);
    }

    /**
     * {@inheritDoc} Includes custom code to set derived resources in addition to generic load.
     * 
     * @see com.cleartrip.common.util.CachedUrlResources#loadResource(boolean)
     */
    @Override
    protected UrlResources<CommonUrlResources, String> loadResource(long cacheMillis, boolean force) throws Exception {
        UrlResources<CommonUrlResources, String> newResource = super.loadResource(cacheMillis, force);
        if (newResource != null) {
            try {
                EnumMap<CommonUrlResources, String> urlData = newResource.getUrlData();
                String commonFooter = urlData.get(CommonUrlResources.CT_UI_FOOTERS);
                if (!StringUtils.isBlank(commonFooter)) {
                    // Custom parsing
                    int pos = commonFooter.indexOf(FOOTER_MARKER);
                    if (pos > 0) {
                        pos += FOOTER_MARKER_LEN;
                        char ch;
                        do {
                            pos++;
                            ch = commonFooter.charAt(pos);
                        } while ((!Character.isWhitespace(ch) || ch == '\n' || ch == '\f' || ch == '\r') && ch != '<');
                    }
                    newResource.setCustom(pos);
                }
            } catch (Exception e) {
                log.error("Error Doing Custom Parsing for Hotel Footer", e);
                throw new RuntimeException(e);
            }
        }
        return newResource;
    }

}
