package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import java.io.Serializable;
import java.util.Map;

public class Expression implements Serializable {

    private static final long serialVersionUID = -7406655070076855434L;

    private String op;

    private Object lhs;

    private Object rhs;

    private int pos = -1;

    private RuleEvaluator ruleEvaluator;

    public Expression() {
        ruleEvaluator = RuleEvaluator.getInstance();
    }

    public Object evaluate(Map<String, Object> values, Expression[] currentNode) {

        if (currentNode != null) {
            currentNode[0] = this;
        }

        Object result = null;
        Object leftResult = lhs;
        Object rightResult = rhs;

        if (leftResult instanceof Expression) {
            leftResult = ((Expression) leftResult).evaluate(values, currentNode);
        }

        if (leftResult instanceof Boolean) {
            boolean b = ((Boolean) leftResult);
            if ("AND".equals(op) && !b || "OR".equals(op) && b) {
                result = b;
            }
        }

        if (result == null) {
            if (rightResult instanceof Expression) {
                rightResult = ((Expression) rightResult).evaluate(values, currentNode);
            }

            result = ruleEvaluator.evaluateExpression(values, op, leftResult, rightResult);
        }

        return result;
    }

    public Object evaluate(Map<String, Object> values) {
        return evaluate(values, null);
    }

    public Expression clone() {
        Object leftResult = lhs;
        Object rightResult = rhs;
        Expression exp = new Expression();
        exp.op = op;

        if (leftResult instanceof Expression) {
            leftResult = ((Expression) leftResult).clone();
        }
        if (rightResult instanceof Expression) {
            rightResult = ((Expression) rightResult).clone();
        }

        exp.lhs = leftResult;
        exp.rhs = rightResult;
        exp.pos = pos;

        return exp;
    }

    public void clear() {
        op = null;
        lhs = null;
        rhs = null;
        pos = -1;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String pop) {
        op = pop;
    }

    public Object getLhs() {
        return lhs;
    }

    public void setLhs(Object plhs) {
        lhs = plhs;
    }

    public Object getRhs() {
        return rhs;
    }

    public void setRhs(Object prhs) {
        rhs = prhs;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int ppos) {
        pos = ppos;
    }
}
