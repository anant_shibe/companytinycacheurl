/*
 * @(#) CompanyConfigurationFilter.java
 *
 * Copyright 2010-2011 by Cleartrip Travel Services Pvt. Ltd.
 * Unit No 001, Ground Floor, DTC Bldg,
 * Sitaram Mills Compound, N.M. Joshi Marg,
 * Delisle Road,
 * Lower Parel (E)
 * Mumbai - 400011
 * India
 *
 * This software is the confidential and proprietary information
 * of Cleartrip Travel Services Pvt. Ltd. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of license agreement you
 * entered into with Cleartrip Travel Service Pvt. Ltd.
 */
package com.companyApiTinyCache.companyConfigCache.service.common.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Filter to intercept all incoming requests and inject Comapny level config
 * details and default page elements for given company id.
 *
 * @author Amit Kumar Sharma
 *
 */
public class CompanyConfigurationFilter {

    private static final Log logger = LogFactory
            .getLog(CompanyConfigurationFilter.class);

    /**.
     * Attribute name against which the Http Error code will be set by this
     * filter
     */
    public static final String HTTP_ERROR_NUMBER = "ct.air.wl.http.error";

    /** Parameter name for company id. */
    public static final  String COMPANY_ID_PARAM = "companyid";

    /**.
     * Attribute name against which resolved afflicate object will be set in
     * request scope
     */
    public static final String COMPANY_ATTRIB = "company";

}
