package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import com.github.jedis.lock.JedisLock;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Response;
import redis.clients.jedis.Transaction;

import java.util.Collection;
import java.util.List;

public class JedisCacheImpl implements JedisCache {
    private static final int CACHED_MAX_KEY_LEN = 250;
    private Jedis jedis;

    public JedisCacheImpl(Jedis jedis) {
        this.jedis = jedis;
    }

    protected boolean encodeKeyRequired = true;
    private String nameSpace = "";
    private static final Log logger = LogFactory.getLog(JedisCacheImpl.class);

    @Override
    public String get(String key) throws CacheRetrieveException {
        // TODO Auto-generated method stub
        return jedis.get(key);
    }

    @Override
    public void put(String id, String obj) throws CacheStoreException {
        jedis.set(id, obj);
    }

    @Override
    public void put(String id, String obj, int expirySeconds) throws CacheStoreException {
        jedis.set(id, obj);
        jedis.expire(id, expirySeconds);
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public Transaction multi() {
        return jedis.multi();
    }

    public boolean lock(String key, int seconds) {
        boolean locked = false;
        try {
            String val = jedis.get(key);

            if (val != null) {
                Transaction t = jedis.multi();
                Response<Long> setRes = t.setnx(key, "1");
                t.expire(key, seconds);
                t.exec();
                Long setSuccess = setRes.get();
                locked = setSuccess != null && setSuccess > 0;
            }
        } catch (Exception e) {
            logger.error("Exception when try to lock on " + key, e);
        }

        return locked;
    }

    public List<String> sort(String listKey) {
        return jedis.sort(listKey);
    }

    public List<String> lrange(String key, long start, long end) {
        return jedis.lrange(key, start, end);
    }

    public JedisLock getLock(String lockKey) throws InterruptedException {
        JedisLock jedisLock = new JedisLock(lockKey);
        jedisLock.acquire(jedis);
        return jedisLock;
    }

    public void release(JedisLock jedisLock) throws InterruptedException {
        jedisLock.release(jedis);
    }

    @Override
    public List<String> hvals(String key) {
        return jedis.hvals(key);
    }

    @Override
    public String hget(String string, String flightInfo) {
        return jedis.hget(string, flightInfo);
    }

    @Override
    public void hset(String hashName, String flightInfo, String dummy) {
        // TODO Auto-generated method stub
        jedis.hset(hashName, flightInfo, dummy);

    }

    public void rpushx(String key, String value) {
        jedis.rpush(key, value);
    }

    public void zadd(String key, double score, String member) {
        jedis.zadd(key, score, member);
    }

    public Collection<String> zrangeByScore(String key, double min, double max) {
        return jedis.zrangeByScore(key, min, max);
    }

    public void zremrangeByScore(String key, double min, double max) {
        jedis.zremrangeByScore(key, min, max);
    }

    public Jedis getJedis() {
        return jedis;
    }

}
