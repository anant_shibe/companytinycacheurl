/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.util.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Element;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

public class XmlTransformer {
    private static final Log logger = LogFactory.getLog(XmlTransformer.class);

    public static String serialize(Element doc) {
        TransformerFactory tfactory = TransformerFactory.newInstance();
        Transformer serializer;
        try {
            serializer = tfactory.newTransformer();
            // Setup indenting to "pretty print"
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(new StringWriter());
            serializer.transform(new DOMSource(doc), result);
            String xmlString = result.getWriter().toString();
            return xmlString;
        } catch (TransformerException e) {
            // this is fatal, just dump the stack and throw a runtime exception
            logger.fatal("Exception is:" + e.getMessage(), e);
            return "";
        }
    }

}
