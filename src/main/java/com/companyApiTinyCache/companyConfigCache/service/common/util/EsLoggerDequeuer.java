package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * Worker for Posting it to ES.
 * @author vijay
 */
@ClassExcludeCoverage
public class EsLoggerDequeuer implements Runnable {
    private BlockingQueue<Map<String, Object>> queue;
    private static final Log log = LogFactory.getLog(EsLoggerDequeuer.class);
    private CachedProperties cachedProperties;

    public EsLoggerDequeuer(BlockingQueue<Map<String, Object>> queue, CachedProperties commonCachedProperties) {
        this.queue = queue;
        this.cachedProperties = commonCachedProperties;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (queue.isEmpty()) {
                    try {
                        Thread.sleep(5000);
                        log.debug("ES Queue Worker out of hibernation");
                    } catch (InterruptedException e) {
                        log.warn("Sleeping thread interrupted " + e.getMessage(), e);
                    }
                } else {
                    List<Map<String, Object>> docList = new ArrayList<Map<String, Object>>();
                    queue.drainTo(docList);
                    if (cachedProperties.getBooleanPropertyValue("ct.hotels.staticcontent.logging.v1.enabled", false)) {
                        log.info("ES Queue Worker Sending " + docList.size() + " items to ES");
                        logToElasticSearch(docList);
                    } else {
                        log.info("ES Queue Worker Disabled");
                    }
                    docList.clear();
                }
            } catch (Exception e) {
                log.error("Unhandled Exception" + e.getMessage(), e);
            }
        }
    }

    private void logToElasticSearch(List<Map<String, Object>> docList) {
        StringBuilder query = new StringBuilder();
        for (Map<String, Object> doc : docList) {
            for (String key : doc.keySet()) {
                try {
                    query.append(key).append("=").append(URLEncoder.encode(doc.get(key).toString(), "UTF-8")).append("&");
                } catch (UnsupportedEncodingException e) {
                    log.warn(e.getMessage() + " " + e.getStackTrace()[0]);
                }
            }
            try {
                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("X-CT-ESDOC", query.toString());
                long startTime = System.currentTimeMillis();
                RestUtil.get(cachedProperties.getPropertyValue("ct.hotel.apache.logstash.url"), null, requestHeaders, null, cachedProperties.getIntPropertyValue("ct.hotel.logstash.call.timeout", 50));
                log.info("Indexed Message into ES in " + (System.currentTimeMillis() - startTime) + " ms");
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            query.setLength(0);
        }
    }

}
