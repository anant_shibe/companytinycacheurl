package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.ConnectorStats;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.DefaultRestClientImpl;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestClient;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestRequest;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.ApiType;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.GETTimeouts;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cleartrip
 */
public final class RuleEvaluator {

    private static final Log logger = LogFactory.getLog(RuleEvaluator.class);

    private static final String EXTERNAL_URL = "ct.services.tm.api.raterule-api.url";

    private static final String COMMON_EXTERNAL_URL = "ct.services.common.api.raterule-api.url";

    static final Set<String> ALG_OPERATORS = new HashSet<String>(Arrays.asList("+", "-", "*", "/", "%", "MOD", "POW", "MIN", "MAX", "AVG"));

    static final Set<String> REL_OPERATORS = new HashSet<String>(Arrays.asList("==", "<", ">", "<=", ">=", "!=", "=~", "IN", "NOT IN", "SUBSET OF", "INFLAGS"));

    static final Set<String> LOG_OPERATORS = new HashSet<String>(Arrays.asList("AND", "OR", "NOT", "XOR"));

    static final Set<String> UNCAT_BOOL_VALUE_OPERATORS = new HashSet<String>(Arrays.asList("CACHE_KEY", "REDIS_ID_LINK", "REDIS_ISMEMBER"));

    static final Set<String> EXP_MAP_KEYS = new HashSet<String>(Arrays.asList("lhs", "op", "rhs"));

    static final Set<String> DATE_OPERATORS = new HashSet<String>(Arrays.asList("INCR", "DECR"));

    static final String TERNARY_OPERATOR = "?";

    private RestClient restClient = DefaultRestClientImpl.getInstance();

    private RedisIdLinkExecutor redisIdLinkExecutor;

    private CachedProperties commonCachedProperties;

    // Singleton
    private RuleEvaluator() {
        setRedisIdLinkExecutor(new RedisIdLinkExecutor());
    }

    private static RuleEvaluator instance = new RuleEvaluator();

    public static RuleEvaluator getInstance() {
        return instance;
    }

    public void setRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    public void setRedisIdLinkExecutor(RedisIdLinkExecutor redisIdLinkExecutor) {
        this.redisIdLinkExecutor = redisIdLinkExecutor;
    }

    public Object trimCondition(Map<String, Object> values, Map<String, Object> condition, boolean retainMap, CachedProperties cachedProperties, Cache cache,
                                Set<String> failedChecks, Map<String, String> cacheForCacheQueries, Map<String, Pattern> regexMatchers, JedisSentinelPool jedisSentinelPool, Map<String, Boolean> cacheForRedisQueries,JedisPool jedisPool) {
        String operator = (String) condition.get("op");
        Object lhs = condition.get("lhs");
        Object newLhs = lhs;
        boolean hasVariable = false;
        Object rhs = condition.get("rhs");
        Object newRhs = rhs;

        Object trimmedCondition = null;

        if (lhs instanceof Map) {
            newLhs = trimCondition(values, (Map<String, Object>) lhs, false, cachedProperties, cache, failedChecks, cacheForCacheQueries, regexMatchers, jedisSentinelPool, cacheForRedisQueries,jedisPool);
        }

        boolean useLhs = false;
        boolean useRhs = false;
        if (newLhs instanceof Boolean && ("AND".equals(operator) || "OR".equals(operator)) && !retainMap) {
            if ((Boolean) newLhs) {
                if ("OR".equals(operator)) {
                    trimmedCondition = Boolean.TRUE;
                } else {
                    useRhs = true;
                }
            } else {
                if ("AND".equals(operator)) {
                    trimmedCondition = Boolean.FALSE;
                } else {
                    useRhs = true;
                }
            }
        }

        if (trimmedCondition == null) {
            if (rhs instanceof Map) {
                newRhs = trimCondition(values, (Map<String, Object>) rhs, false, cachedProperties, cache, failedChecks, cacheForCacheQueries, regexMatchers, jedisSentinelPool, cacheForRedisQueries,jedisPool);
            }

            if (newRhs instanceof Boolean && ("AND".equals(operator) || "OR".equals(operator)) && !retainMap) {
                if ((Boolean) newRhs) {
                    if ("OR".equals(operator)) {
                        trimmedCondition = Boolean.TRUE;
                    } else {
                        useLhs = true;
                    }
                } else {
                    if ("AND".equals(operator)) {
                        trimmedCondition = Boolean.FALSE;
                    } else {
                        useLhs = true;
                    }
                }
            }
        }

        String s;
        Object o;
        if (trimmedCondition == null) {
            if (newLhs instanceof String) {
                s = (String) newLhs;
                if (representsVariable(s)) {
                    o = values.get(decodeVariableName(s));
                    if (o == null) {
                        hasVariable = true;
                    }
                }
            }
            if (newRhs instanceof String) {
                s = (String) newRhs;
                if (representsVariable(s)) {
                    o = values.get(decodeVariableName(s));
                    if (o == null) {
                        hasVariable = true;
                    }
                }
            }
            boolean isNewLhsMap = newLhs instanceof Map;
            boolean isNewRhsMap = newRhs instanceof Map;
            if (retainMap) {
                if (!isNewLhsMap) {
                    useLhs = false;
                }
                if (!isNewRhsMap) {
                    useRhs = false;
                }
            }
            if (!hasVariable && !(isNewLhsMap || isNewRhsMap) && !retainMap) {
                Boolean result = null;
                if ("EXT".equals(operator) || "EXT COM".equals(operator)) {
                    trimmedCondition = executeExternal(values, condition, cachedProperties);
                    if (trimmedCondition instanceof Boolean) {
                        result = (Boolean) trimmedCondition;
                        updateFailedChecks(lhs, rhs, failedChecks, result);
                    }
                } else if ("CACHE_KEY".equals(operator)) {
                    result = doCacheKeyLookup(values, condition, cache, cacheForCacheQueries);
                    updateFailedChecks(lhs, rhs, failedChecks, result);
                } else if ("REDIS_ID_LINK".equals(operator)) {
                    result = checkRedisIdLink(values, condition, jedisSentinelPool, cacheForRedisQueries,jedisPool);
                    updateFailedChecks(lhs, rhs, failedChecks, result);
                } else if ("REDIS_ISMEMBER".equals(operator)) {
                    result = redisIsMemberCall(values, condition, jedisSentinelPool, cacheForRedisQueries,jedisPool);
                    updateFailedChecks(lhs, rhs, failedChecks, result);
                } else {
                    result = evaluateCondition(values, operator, newLhs, newRhs, failedChecks, null, regexMatchers);
                }
                if (result != null) {
                    trimmedCondition = result;
                }
            }
        }
        if (trimmedCondition == null) {
            trimmedCondition = condition;
            if (useLhs) {
                trimmedCondition = newLhs;
            } else if (useRhs) {
                trimmedCondition = newRhs;
            } else if (lhs != newLhs || rhs != newRhs) {
                Map<String, Object> newCondition = new HashMap<String, Object>(condition);
                newCondition.put("lhs", newLhs);
                newCondition.put("rhs", newRhs);
                trimmedCondition = newCondition;
            }
        }

        return trimmedCondition;
    }

    public Object trimCondition(Map<String, Object> values, Map<String, Object> condition, CachedProperties cachedProperties, Cache cache, Set<String> failedChecks,
                                Map<String, String> cacheForCacheQueries, Map<String, Pattern> regexMatchers, JedisSentinelPool jedisSentinelPool, Map<String, Boolean> cacheForRedisQueries,JedisPool jedisPool) {
        return trimCondition(values, condition, false, cachedProperties, cache, failedChecks, cacheForCacheQueries, regexMatchers, jedisSentinelPool, cacheForRedisQueries,jedisPool);

    }

    public boolean evaluateCondition(Map<String, Object> values, Map<String, Object> condition, CachedProperties cachedProperties, Cache cache, Set<String> failedChecks,
                                     Map<String, String> cacheForCacheQueries, Map<String, Pattern> regexMatchers, JedisSentinelPool jedisSentinelPool, Map<String, Boolean> cacheForRedisQueries,JedisPool jedisPool) {
        return evaluateCondition(values, condition, cachedProperties, cache, failedChecks, null, cacheForCacheQueries, regexMatchers, jedisSentinelPool, cacheForRedisQueries, null,jedisPool);
    }

    public boolean evaluateCondition(Map<String, Object> values, Map<String, Object> condition, CachedProperties cachedProperties, Cache cache, Set<String> failedChecks, List<String> andedOperators,
                                     Map<String, String> cacheForCacheQueries, Map<String, Pattern> regexMatchers, JedisSentinelPool jedisSentinelPool, Map<String, Boolean> cacheForRedisQueries,JedisPool jedisPool) {
        return evaluateCondition(values, condition, cachedProperties, cache, failedChecks, andedOperators, cacheForCacheQueries, regexMatchers, jedisSentinelPool, cacheForRedisQueries, null,jedisPool);
    }

    public boolean evaluateCondition(Map<String, Object> values, Map<String, Object> condition, CachedProperties cachedProperties, Cache cache, Set<String> failedChecks, List<String> andedOperators,
                                     Map<String, String> cacheForCacheQueries, Map<String, Pattern> regexMatchers, JedisSentinelPool jedisSentinelPool, Map<String, Boolean> cacheForRedisQueries, RedisCommandCache resultMapper,JedisPool jedisPool) {
        String operator = (String) condition.get("op");
        Object lhs = condition.get("lhs");
        Object rhs = condition.get("rhs");

        if (lhs instanceof Map) {
            lhs = evaluateConditionMap(values, cachedProperties, cache, failedChecks, andedOperators, (Map<String, Object>) lhs, cacheForCacheQueries, regexMatchers, jedisSentinelPool, cacheForRedisQueries,
                    resultMapper,jedisPool);
        }

        if (("AND".equals(operator) || "OR".equals(operator)) && rhs instanceof Map) {
            Boolean leftBoolean = (Boolean) lhs;
            if ("AND".equals(operator) && !leftBoolean) {
                rhs = Boolean.FALSE;
            } else if ("OR".equals(operator) && leftBoolean) {
                rhs = Boolean.TRUE;
            } else {
                rhs = evaluateConditionMap(values, cachedProperties, cache, failedChecks, andedOperators, (Map<String, Object>) rhs, cacheForCacheQueries, regexMatchers, jedisSentinelPool,
                        cacheForRedisQueries, resultMapper,jedisPool);

                if ("OR".equals(operator) && rhs == Boolean.TRUE && failedChecks != null) {
                    failedChecks.clear();
                }
            }
        }

        boolean result;
        if ("CACHE_KEY".equals(operator)) {
            result = doCacheKeyLookup(values, condition, cache, cacheForCacheQueries);
            updateFailedChecks(condition.get("lhs"), condition.get("rhs"), failedChecks, result);
        } else if ("REDIS_ID_LINK".equals(operator)) {
            result = checkRedisIdLink(values, condition, jedisSentinelPool, cacheForRedisQueries,jedisPool);
            updateFailedChecks(condition.get("lhs"), condition.get("rhs"), failedChecks, result);
        } else if ("REDIS_ISMEMBER".equals(operator)) {
            result = redisIsMemberCall(values, condition, jedisSentinelPool, cacheForRedisQueries,jedisPool);
            updateFailedChecks(condition.get("lhs"), condition.get("rhs"), failedChecks, result);
        } else {
            result = evaluateCondition(values, operator, lhs, rhs, failedChecks, andedOperators, regexMatchers);
        }
        return result;
    }

    private Object evaluateConditionMap(Map<String, Object> values, CachedProperties cachedProperties, Cache cache, Set<String> failedChecks, List<String> andedOperators,
                                        Map<String, Object> condition, Map<String, String> cacheForCacheQueries, Map<String, Pattern> regexMatchers, JedisSentinelPool jedisSentinelPool, Map<String, Boolean> cacheForRedisQueries,
                                        RedisCommandCache resultMapper,JedisPool jedisPool) {
        String operator = (String) condition.get("op");

        Object result;
        if ("EXT".equals(operator) || "EXT COM".equals(operator)) {
            result = executeExternal(values, condition, cachedProperties);
        } else if ("CACHE_KEY".equals(operator)) {
            result = doCacheKeyLookup(values, condition, cache, cacheForCacheQueries);
            updateFailedChecks(condition.get("lhs"), condition.get("rhs"), failedChecks, (Boolean) result);
        } else if ("REDIS_ID_LINK".equals(operator)) {
            result = checkRedisIdLink(values, condition, jedisSentinelPool, cacheForRedisQueries,jedisPool);
            updateFailedChecks(condition.get("lhs"), condition.get("rhs"), failedChecks, (Boolean) result);
        } else if ("REDIS_ISMEMBER".equals(operator)) {
            result = redisIsMemberCall(values, condition, jedisSentinelPool, cacheForRedisQueries,jedisPool);
            updateFailedChecks(condition.get("lhs"), condition.get("rhs"), failedChecks, (Boolean) result);
        } else if ("REDIS_COMMAND".equals(operator)) {
            result = executeRedisCommand(values, condition, cachedProperties, jedisSentinelPool, resultMapper,jedisPool);
        } else {
            result = evaluateCondition(values, condition, cachedProperties, cache, failedChecks, andedOperators, cacheForCacheQueries, regexMatchers, jedisSentinelPool, cacheForRedisQueries, resultMapper,jedisPool);
        }
        return result;
    }

    private Boolean evaluateCondition(Map<String, Object> values, String operator, Object lhs, Object rhs, Set<String> failedChecks, List<String> andedOperators, Map<String, Pattern> regexMatchers) {
        Boolean result = Boolean.FALSE;

        Object[] operandValues;
        try {
            operandValues = getOperandValues(values, lhs, rhs);
        } catch (Exception e) {
            logger.debug(e);
            return result;
        }
        Object leftOperand = operandValues[0];
        Object rightOperand = operandValues[1];

        if (leftOperand instanceof Collection) {
            Collection<Object> collection = (Collection) leftOperand;

            for (Object object : collection) {
                result = (Boolean) executeOperator(operator, object, rightOperand, regexMatchers);

                if (andedOperators != null && andedOperators.contains(operator)) {
                    if (!result) {
                        break;
                    }
                } else {
                    if (result) {
                        break;
                    }
                }
            }
        } else {
            result = (Boolean) executeOperator(operator, leftOperand, rightOperand, regexMatchers);
        }

        updateFailedChecks(lhs, rhs, failedChecks, result);
        return result;
    }

    private void updateFailedChecks(Object lhs, Object rhs, Set<String> failedChecks, Boolean result) {
        if (failedChecks == null || result != Boolean.FALSE) {
            return;
        }

        boolean wasAdded = addConditionToFailedChecks(lhs, failedChecks);
        if (!wasAdded) {
            addConditionToFailedChecks(rhs, failedChecks);
        }
    }

    private boolean addConditionToFailedChecks(Object condition, Set<String> failedChecks) {
        if (condition instanceof String) {
            String str = (String) condition;
            if (representsVariable(str)) {
                failedChecks.add(decodeVariableName(str));
                return true;
            }
        }
        return false;
    }

    public Object evaluateExpression(Map<String, Object> values, Map<String, Object> expression, CachedProperties cachedProperties, Cache cache) {
        return evaluateExpression(values, expression, cachedProperties, cache, null, null,null);
    }

    @SuppressWarnings("rawtypes")
    public Object evaluateExpression(Map<String, Object> values, Map<String, Object> expression, CachedProperties cachedProperties, Cache cache, JedisSentinelPool jedisSentinelPool,
                                     RedisCommandCache hmGetResultMapper, JedisPool jedisPool) {
        Object lhs = expression.get("lhs");
        Object rhs = expression.get("rhs");

        if (lhs instanceof Map) {
            lhs = evaluateExpressionMap(values, cachedProperties, cache, lhs, jedisSentinelPool, hmGetResultMapper,jedisPool);
        }

        if (rhs instanceof Map) {
            rhs = evaluateExpressionMap(values, cachedProperties, cache, rhs, jedisSentinelPool, hmGetResultMapper,jedisPool);
        }

        String operator = (String) expression.get("op");
        return evaluateExpression(values, operator, lhs, rhs);
    }

    @SuppressWarnings({"rawtypes", "unchecked" })
    private Object evaluateExpressionMap(Map<String, Object> values, CachedProperties cachedProperties, Cache cache, Object expression, JedisSentinelPool jedisSentinelPool,
                                         RedisCommandCache resultMapper,JedisPool jedisPool) {
        Map<String, Object> expressionMap = (Map<String, Object>) expression;
        String operator = (String) expressionMap.get("op");

        if ("EXT".equals(operator) || "EXT COM".equals(operator)) {
            expression = executeExternal(values, expressionMap, cachedProperties);
        } else if ("CACHE_KEY".equals(operator)) {
            expression = doCacheKeyLookup(values, expressionMap, cache, null);
        } else if ("REDIS_ISMEMBER".equals(operator)) {
            expression = redisIsMemberCall(values, expressionMap, null, null,null);
        } else if ("DEFAULT".equals(operator)) {
            expression = executeDefaultOperatorRule(values, expressionMap, cache, cachedProperties, jedisSentinelPool, resultMapper,jedisPool);
        } else if ("MAP_GET".equals(operator)) {
            expression = doMapGet(values, expressionMap, cache, cachedProperties, jedisSentinelPool, resultMapper,jedisPool);
        } else if ("REDIS_COMMAND".equals(operator)) {
            expression = executeRedisCommand(values, expressionMap, cachedProperties, jedisSentinelPool, resultMapper,jedisPool);
        } else {
            expression = evaluateExpression(values, expressionMap, cachedProperties, cache, jedisSentinelPool, resultMapper,jedisPool);
        }
        return expression;
    }

    @SuppressWarnings({"rawtypes", "unchecked" })
    private Object executeRedisCommand(Map<String, Object> values, Map<String, Object> expressionMap, CachedProperties cachedProperties, JedisSentinelPool jedisSentinelPool, RedisCommandCache resultMapper,JedisPool jedisPool) {
        if (resultMapper.getResultCache() == null) {
            String command = (String) expressionMap.get("lhs");
            Map<String, Object> params = (Map<String, Object>) expressionMap.get("rhs");
            Class<?>[] methodParameterTypes = new Class[params.size()];
            Object[] methodParameterValue = new Object[params.size()];
            int i = 0;
            for (String key : params.keySet()) {
                Object value = params.get(key);
                if (value instanceof String && representsVariable(value.toString())) {
                    String paramAttributeKey = decodeVariableName(value.toString());
                    value = values.get(paramAttributeKey);
                }
                methodParameterTypes[i] = value.getClass();
                methodParameterValue[i] = value;
                i++;
            }
            Jedis jedis = null;
            try {
                if(jedisSentinelPool!=null) {
                    jedis = jedisSentinelPool.getResource();
                }
                else{
                    jedis = jedisPool.getResource();
                }
                Method redisCommand = Jedis.class.getMethod(command, methodParameterTypes);
                Object result = redisCommand.invoke(jedis, methodParameterValue);
                resultMapper.transform(result);
            } catch (JedisConnectionException e) {
                logger.error(e);
                if (jedis != null) {
                    if(jedisSentinelPool!=null) {
                        jedisSentinelPool.returnBrokenResource(jedis);
                    }
                    else{
                        jedisPool.returnBrokenResource(jedis);
                    }
                    jedis = null;
                }
            } catch (Exception e) {
                logger.error("Unable to Execute redis command" + e);
            } finally {
                if (jedis != null) {
                    if(jedisSentinelPool!=null) {
                        jedisSentinelPool.returnResource(jedis);
                    }
                    else{
                        jedisPool.returnResource(jedis);
                    }
                }
            }

        }
        return resultMapper.getResultCache();
    }

    @SuppressWarnings({"unchecked", "rawtypes" })
    private Object doMapGet(Map<String, Object> values, Map<String, Object> expressionMap, Cache cache, CachedProperties cachedProperties, JedisSentinelPool jedisSentinelPool,
                            RedisCommandCache resultMapper,JedisPool jedisPool) {
        String key = (String) expressionMap.get("lhs");
        if (representsVariable(key)) {
            key = decodeVariableName(key);
            key = (String) values.get(key);
        }

        Object result = null;
        Map<String, Object> map = (Map<String, Object>) expressionMap.get("rhs");
        if (map instanceof Map) {
            if (canEvaluate(map)) {
                map = (Map<String, Object>) evaluateExpressionMap(values, cachedProperties, cache, map, jedisSentinelPool, resultMapper,jedisPool);
            }
            if (map != null) {
                result = map.get(key);
            }
        }
        return result;
    }

    private boolean canEvaluate(Map<String, Object> expression) {
        return expression.containsKey("lhs") && expression.containsKey("op") && expression.containsKey("rhs");
    }

    private Object executeDefaultOperatorRule(Map<String, Object> values, Map<String, Object> expression, Cache cache, CachedProperties cachedProperties, JedisSentinelPool jedisSentinelPool,
                                              RedisCommandCache resultMapper,JedisPool jedisPool) {
        Object lhs = expression.get("lhs");
        if (lhs instanceof Map) {
            lhs = evaluateExpressionMap(values, commonCachedProperties, cache, lhs, jedisSentinelPool, resultMapper,jedisPool);
        }
        return lhs != null ? lhs : expression.get("rhs");
    }

    Object evaluateExpression(Map<String, Object> values, String operator, Object lhs, Object rhs) {
        Object[] operandValues;
        try {
            operandValues = getOperandValues(values, lhs, rhs);
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
        Object leftOperand = operandValues[0];
        Object rightOperand = operandValues[1];
        if(TERNARY_OPERATOR.equals(operator)){
            rightOperand = rhs;
        }
        return executeOperator(operator, leftOperand, rightOperand, null);
    }

    private Object[] getOperandValues(Map<String, Object> values, Object lhs, Object rhs) throws Exception {
        Object leftOperand = retrieveOperandValue(values, lhs);
        Object rightOperand = retrieveOperandValue(values, rhs);

        if (leftOperand != null && rightOperand == null) {
            rightOperand = convertType(rhs, leftOperand);
        } else if (leftOperand == null && rightOperand != null) {
            leftOperand = convertType(lhs, rightOperand);
        } else if (leftOperand == null && rightOperand == null) {
            leftOperand = lhs;
            rightOperand = rhs;

            leftOperand = convertType(leftOperand, rightOperand);
            rightOperand = convertType(rightOperand, leftOperand);
        }

        return new Object[] {leftOperand, rightOperand};
    }

    private Object retrieveOperandValue(Map<String, Object> values, Object operand) throws Exception {
        Object leftOperand = null;
        if (operand instanceof String) {
            String str = (String) operand;
            if (representsVariable(str)) {
                leftOperand = values.get(decodeVariableName(str));
                if (leftOperand == null) {
                    throw new Exception("Value missing for variable: " + str);
                }
            }
        }
        return leftOperand;
    }

    private static String decodeVariableName(String variableRepresentation) {
        return variableRepresentation.substring(1);
    }

    private static boolean representsVariable(String str) {
        return str.startsWith("$") && str.length() > 1;
    }

    private Object convertType(Object sourceObject, Object targetObject) {
        Object returnObject;

        if (targetObject instanceof Collection) {
            Object first = null;
            for (Object object : ((Collection) targetObject)) {
                first = object;
                break;
            }
            targetObject = first;
        }

        if (isConversionRequired(sourceObject, targetObject)) {
            if (sourceObject instanceof Collection) {
                Set<Object> set = new HashSet<Object>();
                for (Object object : (Collection) sourceObject) {
                    set.add(convertString((String) object, targetObject));
                }
                returnObject = set;
            } else {
                returnObject = convertString((String) sourceObject, targetObject);
            }
        } else {
            returnObject = sourceObject;
        }

        return returnObject;
    }

    private boolean isConversionRequired(Object sourceObject, Object targetObject) {
        boolean convert = true;
        if (targetObject instanceof String) {
            convert = false;
        } else if (sourceObject instanceof Collection) {
            Object first = null;
            for (Object object : ((Collection) sourceObject)) {
                first = object;
                break;
            }
            if (!(first instanceof String)) {
                convert = false;
            }
        } else if (!(sourceObject instanceof String)) {
            convert = false;
        }
        return convert;
    }

    private Object convertString(String strValue, Object targetObject) {
        Object object = strValue;

        int radix = 10;
        char c;
        if (targetObject instanceof Boolean) {
            object = Boolean.valueOf(strValue);
        } else if (targetObject instanceof Byte) {
            object = Byte.valueOf(strValue);
        } else if (targetObject instanceof Short) {
            object = Short.valueOf(strValue);
        } else if (targetObject instanceof Integer) {
            if (strValue.charAt(0) == '0' && strValue.length() > 2) {
                c = strValue.charAt(1);
                if (c == 'b' || c == 'x') {
                    radix = c == 'b' ? 2 : 16;
                    strValue = strValue.substring(2);
                }
            }
            object = Integer.valueOf(strValue, radix);
        } else if (targetObject instanceof Long) {
            if (strValue.charAt(0) == '0' && strValue.length() > 2) {
                c = strValue.charAt(1);
                if (c == 'b' || c == 'x') {
                    radix = c == 'b' ? 2 : 16;
                    strValue = strValue.substring(2);
                }
            }
            object = Long.valueOf(strValue, radix);
        } else if (targetObject instanceof Float) {
            object = Float.valueOf(strValue);
        } else if (targetObject instanceof Double) {
            object = Double.valueOf(strValue);
        } else if (targetObject instanceof Date) {
            DateFormat dateFormat = null;
            if (strValue.contains(" ")) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
            } else if (strValue.contains("-")) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            } else if (strValue.contains(":")) {
                dateFormat = new SimpleDateFormat("HH:mm:ss");
            } else if(StringUtils.isNumeric(strValue)) {
                //Added for cases where date Addition is needed
                return Integer.parseInt(strValue);
            }

            try {
                object = dateFormat.parse(strValue);
            } catch (Exception e) {
                logger.error("Invalid date " + strValue, e);
            }
        }

        return object;
    }

    private Object executeOperator(String operator, Object leftOperand, Object rightOperand, Map<String, Pattern> regexMatchers) {
        Object result = null;

        if (operator == null) {
            if (leftOperand != null && StringUtils.isNumeric(leftOperand.toString())) {
                result = Double.valueOf(leftOperand.toString());
            } else {
                result = Boolean.valueOf((String) leftOperand);
            }
        } else if (ALG_OPERATORS.contains(operator)) {
            result = executeAlgebric(operator, leftOperand, rightOperand);
        } else if (REL_OPERATORS.contains(operator)) {
            result = executeRelational(operator, leftOperand, rightOperand, regexMatchers);
        } else if (LOG_OPERATORS.contains(operator)) {
            result = executeLogical(operator, leftOperand, rightOperand);
        } else if (DATE_OPERATORS.contains(operator)) {
            result = executeDateOperations(leftOperand, operator, rightOperand);
        }else if (TERNARY_OPERATOR.equals(operator)) {
            result = executeTernaryOperations(leftOperand, operator, rightOperand);
        }

        return result;
    }

    private Object executeTernaryOperations(Object leftOperand, String operator, Object rightOperand) {
        Number result = null;
        Boolean leftBoolean = ((boolean) leftOperand);
        String rightValues = ((String) rightOperand);

        String[] values = rightValues.split("-");
        if(values.length==2) {
            result = leftBoolean ? Double.valueOf(values[0]): Double.valueOf(values[1]);
        }
        return result;
    }

    protected Date executeDateOperations(Object leftOperand, String operator, Object rightOperand) {
        Date result = null;

        if (leftOperand == null || StringUtils.isEmpty(operator) || rightOperand == null || !(StringUtils.isNumeric((rightOperand.toString())))) {
            return result; //null,basically
        }

        Date lOperand = null;
        if (!(leftOperand instanceof Date)) {
            lOperand = (Date) parseDate(String.valueOf(leftOperand));
        } else {
            lOperand = (Date) leftOperand;
        }

        Integer rOperand = Integer.parseInt(rightOperand.toString());

        if ("INCR".equalsIgnoreCase(operator)) {
            result = performDateAddition(lOperand, rOperand.intValue());
        } else if ("DECR".equalsIgnoreCase(operator)) {
            int value = rOperand.intValue() * -1;
            result = performDateAddition(lOperand, value);
        }
        return result;
    }

    private Number executeAlgebric(String operator, Object leftOperand, Object rightOperand) {
        Number result = null;

        if ("+".equals(operator) || "*".equals(operator) || "/".equals(operator) || "%".equals(operator) || "MOD".equals(operator) || "POW".equals(operator)) {
            Double leftDouble = ((Number) leftOperand).doubleValue();
            Double rightDouble = ((Number) rightOperand).doubleValue();

            if ("+".equals(operator)) {
                result = leftDouble + rightDouble;
            } else if ("*".equals(operator)) {
                result = leftDouble * rightDouble;
            } else if ("/".equals(operator)) {
                result = leftDouble / rightDouble;
            } else if ("%".equals(operator)) {
                result = leftDouble * rightDouble / 100;
            } else if ("MOD".equals(operator)) {
                result = leftDouble % rightDouble;
            } else if ("POW".equals(operator)) {
                result = Math.pow(leftDouble, rightDouble);
            }
        } else if ("-".equals(operator)) {
            if (rightOperand != null) {
                Double leftDouble = ((Number) leftOperand).doubleValue();
                Double rightDouble = ((Number) rightOperand).doubleValue();
                result = leftDouble - rightDouble;
            } else {
                Double leftDouble = ((Number) leftOperand).doubleValue();
                result = -leftDouble;
            }
        } else if ("MIN".equals(operator) || "MAX".equals(operator) || "AVG".equals(operator)) {
            Double leftDouble = ((Number) leftOperand).doubleValue();
            Double rightDouble = ((Number) rightOperand).doubleValue();

            if ("MIN".equals(operator)) {
                result = Math.min(leftDouble, rightDouble);
            } else if ("MAX".equals(operator)) {
                result = Math.max(leftDouble, rightDouble);
            } else if ("AVG".equals(operator)) {
                result = (leftDouble + rightDouble) / 2;
            }
        }
        return result;
    }

    private Boolean executeRelational(String operator, Object leftOperand, Object rightOperand, Map<String, Pattern> regexMatchers) {

        if ("==".equals(operator) || "<".equals(operator) || ">".equals(operator) || "<=".equals(operator) || ">=".equals(operator) || "!=".equals(operator)) {
            Comparable<Object> leftComparable = (Comparable<Object>) leftOperand;
            Comparable<Object> rightComparable = (Comparable<Object>) rightOperand;
            int comparison = leftComparable.compareTo(rightComparable);

            if (comparison == 0) {
                if ("==".equals(operator) || "<=".equals(operator) || ">=".equals(operator)) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            } else {
                if ("!=".equals(operator)) {
                    return Boolean.TRUE;
                }
            }

            if (comparison < 0) {
                if ("<".equals(operator) || "<=".equals(operator)) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }

            if (comparison > 0) {
                if (">".equals(operator) || ">=".equals(operator)) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if ("=~".equals(operator)) {
            String leftString = (String) leftOperand;
            String regex = (String) rightOperand;

            leftString = leftString.toLowerCase();
            Matcher matcher;
            if (regexMatchers != null && regexMatchers.containsKey(regex)) {
                matcher = regexMatchers.get(regex).matcher(leftString);
            } else {
                matcher = Pattern.compile(regex).matcher(leftString);
            }

            return matcher.matches();
        } else if ("IN".equals(operator) || "NOT IN".equals(operator)) {
            Collection<Object> collection = (Collection) rightOperand;
            boolean contains = collection.contains(leftOperand);

            if (("IN".equals(operator) && contains) || ("NOT IN".equals(operator) && !contains)) {
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        } else if ("INFLAGS".equals(operator)) {
            if (leftOperand instanceof Long) {
                return ((Long) leftOperand & (Long) rightOperand) != 0;
            } else {
                return ((Integer) leftOperand & (Integer) rightOperand) != 0;
            }
        }

        return Boolean.FALSE;
    }

    private Boolean executeLogical(String operator, Object leftOperand, Object rightOperand) {
        Boolean result = Boolean.FALSE;

        if ("AND".equals(operator) || "OR".equals(operator) || "XOR".equals(operator)) {
            Boolean leftBoolean = (Boolean) leftOperand;
            Boolean rightBoolean = (Boolean) rightOperand;

            if ("AND".equals(operator)) {
                result = leftBoolean && rightBoolean;
            } else if ("OR".equals(operator)) {
                result = leftBoolean || rightBoolean;
            } else if ("XOR".equals(operator)) {
                result = leftBoolean ^ rightBoolean;
            }
        } else if ("NOT".equals(operator)) {
            Boolean leftBoolean = (Boolean) leftOperand;
            result = !leftBoolean;
        }

        return result;
    }

    private Object executeExternal(Map<String, Object> values, Map<String, Object> condition, CachedProperties cachedProperties) {
        String result = null;
        StringBuilder sb = BaseStats.tmpStringBuilder();

        String operator = (String) condition.get("op");
        String suffix = (String) condition.get("lhs");
        Map<String, Object> parameters = (Map<String, Object>) condition.get("rhs");

        // e.g: http://chronicle.staging.cleartrip.com/tm/query/ + "promocode"
        String url = null;
        Integer timeout = null;

        if ("EXT".equals(operator)) {
            url = cachedProperties.getPropertyValue(EXTERNAL_URL);
            timeout = cachedProperties.getIntPropertyValue(GETTimeouts.TM_RATERULE_API.getTimeoutProperty(), GETTimeouts.TM_RATERULE_API.getTimeout());
        } else if ("EXT COM".equals(operator)) {
            url = cachedProperties.getPropertyValue(COMMON_EXTERNAL_URL);
            timeout = cachedProperties.getIntPropertyValue(GETTimeouts.COMMON_RATERULE_API.getTimeoutProperty(), GETTimeouts.COMMON_RATERULE_API.getTimeout());
        }

        url = url + suffix;
        String queryString = null;

        Map<String, Object> addnlExternalValidationParams;
        if ((addnlExternalValidationParams = getAddnlExternalValidationParams(values)) != null) {
            parameters = new LinkedHashMap<String, Object>(parameters);
            parameters.putAll(addnlExternalValidationParams);
        }

        if (parameters != null) {
            for (String name : parameters.keySet()) {
                Object value = parameters.get(name);

                if (value instanceof String) {
                    String strValue = (String) value;

                    if (representsVariable(strValue)) {
                        strValue = decodeVariableName(strValue);
                        value = values.get(strValue);
                    }
                }

                if (value == null) {
                    continue;
                }
                sb.append(name).append("=");

                if (value instanceof Collection) {
                    boolean first = true;

                    for (Object obj : (Collection) value) {
                        if (first) {
                            first = false;
                        } else {
                            sb.append(",");
                        }

                        sb.append(encode(obj));
                    }
                } else {
                    sb.append(encode(value));
                }

                sb.append("&");
            }

            queryString = sb.toString();
        }

        boolean isException = false;
        ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
        try {
            RestRequest restRequest = new RestRequest(url);
            if (timeout != null) {
                restRequest.setTimeout(timeout);
            }
            restRequest.setQueryString(queryString);
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(ApiType.OTHER, null, "RATE_RULE_EXT", "",url, "");
            }
            RestResponse restResponse = restClient.get(restRequest);
            if (connectorStat != null) {
                connectorStat.closeApiRequest(ApiType.OTHER, "RATE_RULE_EXT", restResponse.getStatus(), restResponse.getContent(), true);
            }
            result = restResponse.getContent().trim();
        } catch (Exception e) {
            sb.setLength(0);
            sb.append("Error from ").append(url).append("?").append(queryString);
            logger.error(sb.toString(), e);
            isException = true;
        } finally {
            if (isException) {
                if (connectorStat != null) {
                    connectorStat.closeApiRequest(ApiType.OTHER, "RATE_RULE_EXT", 500, "Error During Rate-Rule EXT operator evaluation:::" + queryString, true);
                }
            }
        }

        if (result != null) {
            result = result.trim();

            if (StringUtils.isNumeric(result) && parameters.containsKey("return-type") && ("number").equalsIgnoreCase((String) parameters.get("return-type"))) {
                return Integer.valueOf(result);
            }
        }

        BaseStats.returnTmpStringBuilder(sb);
        return result;
    }

    private Map<String, Object> getAddnlExternalValidationParams(Map<String, Object> values) {
        return (Map<String, Object>) values.get("couponCountAddnlValidationParams");
    }

    private String encode(Object obj) {
        String str = null;

        if (obj != null) {
            try {
                str = URLEncoder.encode(obj.toString(), "UTF-8");
            } catch (Exception e) {
                logger.error("URL encoding failed for " + obj.toString(), e);
            }
        }

        return str;
    }

    private Boolean doCacheKeyLookup(Map<String, Object> values, Map<String, Object> condition, Cache cache, Map<String, String> cacheForCacheQueries) {
        String result = "false";
        Object value = null;

        String cacheKey = (String) condition.get("lhs");
        String expected = (String) condition.get("rhs");

        if (representsVariable(cacheKey)) {
            cacheKey = decodeVariableName(cacheKey);
            cacheKey = (String) values.get(cacheKey);
        }

        if (cacheKey != null) {
            if (cacheForCacheQueries != null && cacheForCacheQueries.containsKey(cacheKey)) {
                value = cacheForCacheQueries.get(cacheKey);
            } else {
                value = cache.get(cacheKey);
                if (cacheForCacheQueries != null) {
                    cacheForCacheQueries.put(cacheKey, (String) value);
                }
            }
        }

        if (value != null) {
            result = "true";
        }

        return expected.equals(result);
    }

    private Boolean redisIsMemberCall(Map<String, Object> values, Map<String, Object> condition, JedisSentinelPool jedisSentinelPool, Map<String, Boolean> cacheForRedisQueries,JedisPool jedisPool) {
        Boolean result = Boolean.FALSE;

        String setKey = (String) condition.get("rhs");
        String memberKey = (String) condition.get("lhs");
        String member = (String) values.get(decodeVariableName(memberKey));

        if (member != null) {
            String cacheKey = setKey + "_" + member;
            if (cacheForRedisQueries != null && cacheForRedisQueries.containsKey(cacheKey)) {
                result = cacheForRedisQueries.get(cacheKey);
            } else {
                Jedis jedis = null;
                try {
                    if(jedisSentinelPool!=null) {
                        jedis = jedisSentinelPool.getResource();
                    }
                    else{
                        jedis = jedisPool.getResource();
                    }
                    result = jedis.sismember(setKey, member);
                    if (cacheForRedisQueries != null) {
                        cacheForRedisQueries.put(cacheKey, result);
                    }
                } catch (Exception e) {
                    logger.error("jedis operation during rule evaluation failed.", e);
                } finally {
                    try {
                        if(jedisSentinelPool!=null) {
                            jedisSentinelPool.returnResource(jedis);
                        }
                        else{
                            jedisPool.returnResource(jedis);
                        }
                    } catch (Exception e) {
                        logger.warn("Unable to return resource to jedis.", e);
                    }
                }
            }
        }

        return result;
    }

    private boolean checkRedisIdLink(Map<String, Object> values, Map<String, Object> condition, JedisSentinelPool jedisSentinelPool, Map<String, Boolean> cacheForRedisQueries,JedisPool jedisPool) {
        String setKeyPrefix = (String) condition.get("rhs");
        String memberKey = (String) condition.get("lhs");
        String member = (String) values.get(decodeVariableName(memberKey));

        boolean result = false;

        // EBL-3143 Checking ITI 500 errors of BaseStats: Sundaram
        if (member != null && StringUtils.isBlank(member)) {
            BaseStats baseStats = BaseStats.threadLocal();
            logger.warn(decodeVariableName(memberKey) + " is blank for RuleId: " + setKeyPrefix + ". " + baseStats.toString());
        }

        if (!StringUtils.isBlank(member)) {
            String cacheKey = setKeyPrefix + "_" + member;
            if (cacheForRedisQueries != null && cacheForRedisQueries.containsKey(cacheKey)) {
                result = cacheForRedisQueries.get(cacheKey);
            } else {
                try {
                    if(jedisSentinelPool!=null)
                        result = redisIdLinkExecutor.execute(jedisSentinelPool, setKeyPrefix, member);
                    else{
                        result = redisIdLinkExecutor.execute(jedisPool, setKeyPrefix, member);

                    }
                    if (cacheForRedisQueries != null) {
                        cacheForRedisQueries.put(cacheKey, result);
                    }
                } catch (Exception e) {
                    logger.error("jedis operation during rule evaluation failed.", e);
                }
            }
        }
        return result;
    }

    public static String getVariableName(Object operand) {
        if (!(operand instanceof String)) {
            return null;
        }

        String str = (String) operand;
        return representsVariable(str) ? decodeVariableName(str) : null;
    }

    private Date parseDate(String strValue) {
        DateFormat dateFormat = null;
        Date dateObject = null;
        if (strValue.contains(" ")) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        } else if (strValue.contains("-")) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        } else if (strValue.contains(":")) {
            dateFormat = new SimpleDateFormat("HH:mm:ss");
        }

        try {
            dateObject = dateFormat.parse(strValue);
        } catch (Exception e) {
            logger.error("Invalid date " + strValue, e);
        }

        return dateObject;
    }

    protected Date performDateAddition(Date leftOperand, int rightOperand) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(leftOperand);
            calendar.add(Calendar.DATE, rightOperand);
            return calendar.getTime();
        } catch (Exception exception) {
            logger.error("Error performing date addition :" + exception.getMessage());
            return null;
        }
    }

    public void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
        redisIdLinkExecutor.setCommonCachedProperties(commonCachedProperties);
    }

    @SuppressWarnings("rawtypes")
    public Object evaluateMessage(Map<String, Object> values, Map<String, Object> expression, CachedProperties cachedProperties, Cache cache, JedisSentinelPool jedisSentinelPool,
                                  RedisCommandCache hmGetResultMapper,JedisPool jedisPool) {
        return evaluateExpressionMap(values, cachedProperties, cache, expression, jedisSentinelPool, hmGetResultMapper,jedisPool);
    }

}
