package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.SecurityUtil;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Controller to return the domain URL based on the requestor's IP.
 *
 * @author suresh
 *
 */
@ClassExcludeCoverage
public class IpAddressToUrlMappingController extends AbstractController {

    protected CachedProperties commonCachedProperties;

    public IpAddressToUrlMappingController(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // fromIp
        String userCountryFromCSV = SecurityUtil.getCountryFromIpaddress(request, commonCachedProperties);

        Map<String, Map<String, String>> supportedCountryMap = GenUtil.getSupportedCountryListMap(commonCachedProperties);

        for (Map<String, String> countryMap : supportedCountryMap.values()) {
            String countryCode = countryMap.get("code");
            if (countryCode.equalsIgnoreCase(userCountryFromCSV)) {
                PrintWriter responseWriter = response.getWriter();
                String url = countryMap.get("url");
                url = SecurityUtil.replaceEnvPrefix(url, request);
                responseWriter.println(url);
                responseWriter.flush();
                responseWriter.close();
                return null;
            }
        }

        userCountryFromCSV = "IN";

        for (Map<String, String> countryMap : supportedCountryMap.values()) {
            String countryCode = countryMap.get("code");
            if (countryCode.equalsIgnoreCase(userCountryFromCSV)) {
                PrintWriter responseWriter = response.getWriter();
                String url = countryMap.get("url");
                url = SecurityUtil.replaceEnvPrefix(url, request);
                responseWriter.println(url);
                responseWriter.flush();
                responseWriter.close();
                return null;
            }
        }

        return null;
    }
}
