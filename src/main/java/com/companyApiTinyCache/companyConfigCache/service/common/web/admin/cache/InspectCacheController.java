package com.companyApiTinyCache.companyConfigCache.service.common.web.admin.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.util.tinycache.TinyCache;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.zip.GZIPOutputStream;

public class InspectCacheController extends AbstractController {

    public InspectCacheController() {
        setCacheSeconds(0);
    }

    private ApplicationContext getContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String beanName = request.getParameter("beanName");
        ApplicationContext ctx = getContext();
        TinyCache tinyCache = (TinyCache) ctx.getBean(beanName);

        // Get the content
        CharArrayWriter writer = new CharArrayWriter();
        tinyCache.printAllKeys(writer);
        String content = "[\n" + writer.toString() + "\n]";
        response.setContentType("text/plain");

        boolean isCompress = false;
        if (content != null && content.length() > 10000) {
            String encodings = request.getHeader("Accept-Encoding");
            if (encodings != null && encodings.indexOf("gzip") != -1) {
                isCompress = true;
            }
        }

        PrintWriter op = null;
        if (isCompress) {
            // Go with GZIP
            response.setHeader("Content-Encoding", "gzip");
            OutputStream out = new GZIPOutputStream(response.getOutputStream());
            op = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out)));
        } else {
            op = response.getWriter();
        }
        op.println(content);
        op.close();
        return null;
    }

}
