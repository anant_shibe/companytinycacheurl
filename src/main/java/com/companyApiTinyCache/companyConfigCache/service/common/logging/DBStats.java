package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ClassExcludeCoverage
public class DBStats implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String query;
	
	private long callIn;

    private long callOut;
    
    private String serviceName;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public long getCallIn() {
		return callIn;
	}

	public void setCallIn(long callIn) {
		this.callIn = callIn;
	}

	public long getCallOut() {
		return callOut;
	}

	public void setCallOut(long callOut) {
		this.callOut = callOut;
	}
	@MethodExcludeCoverage
	public static DBStats init(String serviceName, String query) {
		DBStats dbStats = new DBStats();
        dbStats.setServiceName(serviceName);
        dbStats.setQuery(query);
        dbStats.setCallIn(System.currentTimeMillis());  
        List<DBStats> dbStatsList = null;		
		//if (BaseStats.threadLocal() != null && BaseStats.threadLocal() instanceof ChmmStats) {
			ConnectorStats connectorStats = (ConnectorStats) BaseStats.threadLocal();        
	        if (connectorStats != null) { 
	        	if (connectorStats.getDbStats() != null) {
	        		dbStatsList = connectorStats.getDbStats();
	        	} else {
	        		dbStatsList = new ArrayList<DBStats>();
	        		connectorStats.setDbStats(dbStatsList);
	        	}
	        	dbStatsList.add(dbStats);        
	        }
		//}	
        return dbStats;
	}

}
