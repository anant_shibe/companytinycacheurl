package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.rule.pojo.CashBack;
import com.companyApiTinyCache.companyConfigCache.service.common.rule.pojo.WalletType;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;

public class CashBackEvaluator implements ActionsEvaluator {

	private static final Log LOG = LogFactory.getLog(CashBackEvaluator.class);
	private RuleEvaluator ruleEvaluator;

	/**
	 * Method is responsible for evaluating the actions and constructing a list of cashback actions. It is not responsible for any kind of validations
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CashBack> evaluateActions(LinkedHashSet<Map<String, Object>> actions, Map<String, Object> attributes, Map<String, Object> miscAttributes,CachedProperties cachedProperties, Cache cache) {
		List<CashBack> cashBackEvents = new ArrayList<CashBack>();
		try {
			for (Map<String, Object> action : actions) {
				Map<String, Object> amountExpression = (Map<String, Object>) action.get("amount");
				Map<String, Object> triggerDateExpression = (Map<String, Object>) action.get("trigger_date");
				Map<String, Object> expiryExpression = (Map<String, Object>) action.get("expiry_date");
				String walletType  = (String) miscAttributes.get("wallet_owner");
                WalletType wallet  = WalletType.CLEARTRIP;
                try{
					if(walletType!=null){
						wallet=  WalletType.valueOf(walletType.toUpperCase());
					}
				}catch(Exception e){
                	// Falls back to Cleartrip wallet mesagging
					LOG.error("Error during fecthing wallet type" + e);
				}

				if (amountExpression == null || triggerDateExpression == null || expiryExpression == null) {
					throw new CashBack.CashBackCreateException("Mandatory params missing");
				}
				Double amount = (Double)ruleEvaluator.evaluateExpression(attributes, amountExpression, cachedProperties, cache);
				Date triggerDate = (Date) ruleEvaluator.evaluateExpression(attributes, triggerDateExpression, cachedProperties, cache);
				Date expiryDate = (Date) ruleEvaluator.evaluateExpression(attributes, expiryExpression, cachedProperties, cache);
				String cashBackMessageTemplate = (String)action.get("msg");
				Map<String, Object> cashBackMessageValues = new HashMap<String, Object>();
				cashBackMessageValues.put("amount", amount);
				cashBackMessageValues.put("triggerDate", triggerDate);
				String cashBackMessage = getMessage(cashBackMessageTemplate, cashBackMessageValues);
				CashBack cashBackEvent = CashBack.createCashBack(cashBackMessage, amount, triggerDate, expiryDate, cashBackMessageTemplate, wallet);
				cashBackEvents.add(cashBackEvent);
			}
		} catch (CashBack.CashBackCreateException cause) {
			LOG.error("Error during action evalutation ::" + cause.toString());
		}
		catch (Exception cause) {
			LOG.error("Error during action evalutation ::" + getStackTrace(cause));
		}
		return cashBackEvents;
	}

	protected String getMessage(String templateString, Map<String, Object> attributes) {
		StrSubstitutor stringSubstitutor = new StrSubstitutor(attributes);
		return stringSubstitutor.replace(templateString);
	}

	public void setRuleEvaluator(RuleEvaluator ruleEvaluator) {
		this.ruleEvaluator = ruleEvaluator;
	}

	@MethodExcludeCoverage
	private String getStackTrace(Exception cause) {
	    StringBuffer sb = new StringBuffer(500);
	    StackTraceElement[] st = cause.getStackTrace();
	    sb.append(cause.getClass().getName() + ": " + cause.getMessage() + "\n");
	    for (int i = 0; i < st.length; i++) {
	      sb.append("\t at " + st[i].toString() + "\n");
	    }
	    return sb.toString();
	}
}
