package com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

@ClassExcludeCoverage
public class CtThreadPoolExecutor extends ThreadPoolExecutor {

    private String activeName;

    private String idleName;

    public CtThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory,
            RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
    }

    public CtThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
    }

    public CtThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
    }

    public CtThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    public CtThreadPoolExecutor(int corePoolSize, int maximumPoolSize, int queueCapacity) {
        super(corePoolSize, maximumPoolSize, 60, TimeUnit.SECONDS, queueCapacity > 0 ? new LinkedBlockingQueue<>(queueCapacity) : new LinkedBlockingQueue<Runnable>());
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        if (activeName != null) {
            t.setName(activeName);
        }
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        if (idleName != null) {
            Thread.currentThread().setName(idleName);
        }
    }

    public void setActiveName(String pactiveName) {
        activeName = pactiveName;
    }

    public void setIdleName(String pidleName) {
        idleName = pidleName;
    }

    @Override
    public Future<?> submit(Runnable task) {
        LogEnabledRunnable logEnabledTask = new LogEnabledRunnable(task);
        return super.submit(logEnabledTask);
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        LogEnabledRunnable logEnabledTask = new LogEnabledRunnable(task);
        return super.submit(logEnabledTask, result);
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        LogEnabledCallable<T> logEnabledTask = new LogEnabledCallable<T>(task);
        return super.submit(logEnabledTask);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        Collection<LogEnabledCallable<T>> logEnabledTasks = new ArrayList<LogEnabledCallable<T>>();
        for (Callable<T> task : tasks) {
            logEnabledTasks.add(new LogEnabledCallable<T>(task));
        }
        return super.invokeAll(logEnabledTasks);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        Collection<LogEnabledCallable<T>> logEnabledTasks = new ArrayList<LogEnabledCallable<T>>();
        for (Callable<T> task : tasks) {
            logEnabledTasks.add(new LogEnabledCallable<T>(task));
        }
        return super.invokeAll(logEnabledTasks, timeout, unit);
    }
}
