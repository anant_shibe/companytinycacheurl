package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;

public class MessagingFactoryImpl implements MessagingFactory {

    private CacheFactory cacheFactory;

    private ConnectionFactory connectionFactory;

    private Destination destination;

    private CachedProperties commonCachedProperties;

    private String maxWaitProperty;

    private MessageProcessor messageProcessor;

    private String cacheExpiryProperty;

    private Object messageReceiver;

    private MessageSender messageSender;

    public MessagingFactoryImpl(CacheFactory pcacheFactory, ConnectionFactory pconnectionFactory, Destination pdestination, CachedProperties pcommonCachedProperties, String pmaxWaitProperty,
            String pcacheExpiryProperty, MessageProcessor pmessageProcessor) {
        cacheFactory = pcacheFactory;
        connectionFactory = pconnectionFactory;
        destination = pdestination;
        commonCachedProperties = pcommonCachedProperties;
        maxWaitProperty = pmaxWaitProperty;
        cacheExpiryProperty = pcacheExpiryProperty;
        messageProcessor = pmessageProcessor;
    }

    public MessagingFactoryImpl(CacheFactory pcacheFactory, ConnectionFactory pconnectionFactory, Destination pdestination, CachedProperties pcommonCachedProperties, String pcacheExpiryProperty,
            MessageProcessor pmessageProcessor) {
        this(pcacheFactory, pconnectionFactory, pdestination, pcommonCachedProperties, null, pcacheExpiryProperty, pmessageProcessor);
    }

    public MessagingFactoryImpl(CacheFactory pcacheFactory, ConnectionFactory pconnectionFactory, Destination pdestination, CachedProperties pcommonCachedProperties, MessageProcessor pmessageProcessor) {
        this(pcacheFactory, pconnectionFactory, pdestination, pcommonCachedProperties, null, pmessageProcessor);
    }

    /**
     * @see com.cleartrip.common.messaging.MessagingFactory#getReceiver()
     */
    @Override
    public Object getReceiver() {
        if (messageReceiver == null) {
            messageReceiver = new MemcachedJmsMessageReceiver(cacheFactory, connectionFactory, destination, commonCachedProperties, maxWaitProperty, messageProcessor);
        }
        return messageReceiver;
    }

    /**
     * @see com.cleartrip.common.messaging.MessagingFactory#getSender()
     */
    @Override
    public MessageSender getSender() {
        if (messageSender == null) {
            messageSender = new MemcachedJmsMessageSender(cacheFactory, connectionFactory, destination, commonCachedProperties, cacheExpiryProperty);
        }

        return messageSender;
    }

    public final void setMessageProcessor(MessageProcessor pmessageProcessor) {
        messageProcessor = pmessageProcessor;
    }
}
