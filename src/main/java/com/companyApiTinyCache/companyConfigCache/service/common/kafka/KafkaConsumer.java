package com.companyApiTinyCache.companyConfigCache.service.common.kafka;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ClassExcludeCoverage
public class KafkaConsumer {

    private ConsumerConnector consumer;

    public KafkaConsumer(ConsumerConfig consumerConfig) {
        consumer = Consumer.createJavaConsumerConnector(consumerConfig);
    }

    public List<KafkaStream<byte[], byte[]>> getKafkaStream(String topic, int threadCount) {

        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic, threadCount);
        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
        List<KafkaStream<byte[], byte[]>> streams = consumerMap.get(topic);

        return streams;

    }
    
    public void close() {
        consumer.shutdown();
    }
}
