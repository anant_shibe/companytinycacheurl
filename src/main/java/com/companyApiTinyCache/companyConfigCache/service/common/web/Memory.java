package com.companyApiTinyCache.companyConfigCache.service.common.web;

public class Memory {
    private long free;
    private long total;
    private static final long START = System.currentTimeMillis();
    private long time = System.currentTimeMillis();

    public long getFree() {
        return free;
    }

    public long getFreeMB() {
        return getFree() / 1024 / 1024;
    }

    public void setFree(long free) {
        this.free = free;
    }

    public long getTotal() {
        return total;
    }

    public long getTotalMB() {
        return getTotal() / 1024 / 1024;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getTime() {
        return time;
    }

    public long getTimeMin() {
        return (time - START) / 60000;
    }

    @Override
    public String toString() {
        return "Free : " + getFree() + ", Used : " + getUsed() + ", Total : " + getTotal() + "\n";
    }

    public long getUsed() {
        return getTotal() - getFree();
    }

    public long getUsedMB() {
        return getUsed() / 1024 / 1024;
    }
}
