/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class PersonalDetail implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private String title;

    private String firstName;

    private String lastName;

    private String userName;

    private String gender;

    private String primaryEmail;

    private String countryOfResidence;

    private int countryOfResidenceId;

    private String homeAirport;

    private int homeAirportId;

    private String nickName;

    /**
     * Refer UserProfile.CompanyDetail for this info MOVED to CompanyDetail.
     */
    // @JsonProperty("company_id") @Deprecated
    // private int companyId;

    private Date dob;

    @JsonIgnore
    private String dateFormatter = "yyyy-MM-dd";

    @JsonIgnore
    private boolean profiledUpdated;

    private String countryPreference;

    private String langPreference;

    private String currencyPreference;

    private String concatName;

    @JsonIgnore
    public boolean isProfiledUpdated() {
        return profiledUpdated;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("nick_name")
    public String getNickName() {
        return nickName;
    }

    @JsonProperty("nick_name")
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("primary_email")
    public String getPrimaryEmail() {
        return primaryEmail;
    }

    @JsonProperty("primary_email")
    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    @JsonProperty("date_of_birth")
    public Date getDob() {
        return dob;
    }

    @JsonProperty("date_of_birth")
    public void setDob(Date dob) {
        if (dob != null) {
            profiledUpdated = true;
        }
        this.dob = dob;
    }

    @JsonIgnore
    public void setDob(String dobString) {
        if (StringUtils.isNotEmpty(dobString)) {
            profiledUpdated = true;
        }

        this.dob = GenUtil.createParsedDate(dobString, dateFormatter);
    }

    @JsonIgnore
    public String getFormattedDob() {
        if (null != dob) {
        return GenUtil.createParsedDate(this.dob, "dd/MM/yyyy");
        } else {
            return "";
        }
    }

    @JsonProperty("country_of_residence")
    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    @JsonProperty("country_of_residence")
    public void setCountryOfResidence(String countryOfResidence) {
        if (StringUtils.isNotEmpty(countryOfResidence)) {
            profiledUpdated = true;
        }
        this.countryOfResidence = countryOfResidence;
    }

    @JsonProperty("country_of_residence_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getCountryOfResidenceId() {
        return countryOfResidenceId;
    }

    @JsonProperty("country_of_residence_id")
    public void setCountryOfResidenceId(int countryOfResidenceId) {
        this.countryOfResidenceId = countryOfResidenceId;
    }

    @JsonProperty("home_airport_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getHomeAirportId() {
        return homeAirportId;
    }

    @JsonProperty("home_airport_id")
    public void setHomeAirportId(int homeAirportId) {
        if (homeAirportId != 0) {
            profiledUpdated = true;
        }
        this.homeAirportId = homeAirportId;
    }

    // @JsonProperty("company_id") @Deprecated
    // public int getCompanyId() {
    // return companyId;
    // }
    //
    // @JsonProperty("company_id") @Deprecated
    // public void setCompanyId(int companyId) {
    // this.companyId = companyId;
    // }

    @JsonProperty("home_airport")
    public String getHomeAirport() {
        return homeAirport;
    }

    @JsonProperty("home_airport")
    public void setHomeAirport(String homeAirport) {
        if (StringUtils.isNotEmpty(homeAirport)) {
            profiledUpdated = true;
        }
        this.homeAirport = homeAirport;
    }

    @JsonIgnore
    public String getUserName() {
        return userName;
    }

    @JsonIgnore
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("country_preference")
    public String getCountryPreference() {
        return countryPreference;
    }

    @JsonProperty("country_preference")
    public void setCountryPreference(String countryPreference) {
        this.countryPreference = countryPreference;
    }

    @JsonProperty("language")
    public String getLangPreference() {
        return langPreference;
    }

    @JsonProperty("language")
    public void setLangPreference(String langPreference) {
        this.langPreference = langPreference;
    }

    @JsonProperty("currency")
    public String getCurrencyPreference() {
        return currencyPreference;
    }

    @JsonProperty("currency")
    public void setCurrencyPreference(String currencyPreference) {
        this.currencyPreference = currencyPreference;
    }

    @JsonProperty("concat_name")
    public String getConcatName() {
        return concatName;
    }

    @JsonProperty("concat_name")
    public void setConcatName(String concatName) {
        this.concatName = concatName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PersonalDetail)) {
            return false;
        }
        PersonalDetail pd = (PersonalDetail) obj;
        if ((title != null || pd.getTitle() != null) && !GenUtil.equals(title, pd.getTitle())) {
            return false;
        }
        if ((firstName != null || pd.getFirstName() != null) && !GenUtil.equals(firstName, pd.getFirstName())) {
            return false;
        }
        if ((lastName != null || pd.getLastName() != null) && !GenUtil.equals(lastName, pd.getLastName())) {
            return false;
        }
        if ((userName != null || pd.getUserName() != null) && !GenUtil.equals(userName, pd.getUserName())) {
            return false;
        }
        if ((gender != null || pd.getGender() != null) && !GenUtil.equals(gender, pd.getGender())) {
            return false;
        }
        if ((primaryEmail != null || pd.getPrimaryEmail() != null) && !GenUtil.equals(primaryEmail, pd.getPrimaryEmail())) {
            return false;
        }
        if ((countryOfResidence != null || pd.getCountryOfResidence() != null) && !GenUtil.equals(countryOfResidence, pd.getCountryOfResidence())) {
            return false;
            // }if(countryOfResidenceId != pd.getCountryOfResidenceId()){
            // return false;
        }
        if ((homeAirport != null || pd.getHomeAirport() != null) && !GenUtil.equals(homeAirport, pd.getHomeAirport())) {
            return false;
            // }if(homeAirportId != pd.getHomeAirportId()){
            // return false;
        }
        if ((dob != null || pd.getDob() != null) && !GenUtil.equals(dob, pd.getDob())) {
            return false;
        }
        if ((countryPreference != null || pd.getCountryPreference() != null) && !GenUtil.equals(countryPreference, pd.getCountryPreference())) {
            return false;
        }
        if ((langPreference != null || pd.getLangPreference() != null) && !GenUtil.equals(langPreference, pd.getLangPreference())) {
            return false;
        }
        if ((currencyPreference != null || pd.getCurrencyPreference() != null) && !GenUtil.equals(currencyPreference, pd.getCurrencyPreference())) {
            return false;
        }

        return true;
    }

    // Overriding the hashcode() to avoid checkstyle error
    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
