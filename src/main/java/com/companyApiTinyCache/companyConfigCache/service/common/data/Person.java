package com.companyApiTinyCache.companyConfigCache.service.common.data;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;

public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    private int peopleId;
    private int userId;
    private int departmentId;
    private int approverId;
    private String userName;
    private String firstName;
    private String lastName;
    private String paxType;
    private String approverName;
    private int alternateApproverId;
    private int approvedById;
    private String escalatedOn;
    private int initiatorId;
    private String nickName;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPaxType() {
        String type = "";
        if (this.paxType.equalsIgnoreCase("ADT")) {
            type = "Adult";
        } else if (this.paxType.equalsIgnoreCase("CHD")) {
            type = "Child";
        } else if (this.paxType.equalsIgnoreCase("INF")) {
            type = "Infant";
        } else if (this.paxType.equalsIgnoreCase("SCM")) {
            type = "Senior Male";
        } else if (this.paxType.equalsIgnoreCase("SCF")) {
            type = "Senior Female";
        }
        return type;
    }

    public int getApproverId() {
        return approverId;
    }

    public void setApproverId(int approverId) {
        this.approverId = approverId;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public int getAlternateApproverId() {
        return alternateApproverId;
    }

    public void setAlternateApproverId(int alternateApproverId) {
        this.alternateApproverId = alternateApproverId;
    }

    public int getApprovedById() {
        return approvedById;
    }

    public void setApprovedById(int approvedById) {
        this.approvedById = approvedById;
    }

    public String getEscalatedOn() {
        return escalatedOn;
    }

    public void setEscalatedOn(String escalatedOn) {
        this.escalatedOn = escalatedOn;
    }

    public void setPaxType(String paxType) {
        this.paxType = paxType;
    }

    public int getPeopleId() {
        return this.peopleId;
    }

    public void setPeopleId(int peopleId) {
        this.peopleId = peopleId;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((paxType == null) ? 0 : paxType.hashCode());
        result = prime * result + ((peopleId == 0) ? 0 : ((Integer) peopleId).hashCode());
        result = prime * result + userId;
        result = prime * result + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (firstName == null) {
            if (other.firstName != null) {
                return false;
            }
        } else if (!firstName.equals(other.firstName)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (paxType == null) {
            if (other.paxType != null) {
                return false;
            }
        } else if (!paxType.equals(other.paxType)) {
            return false;
        }
        if (peopleId != other.peopleId) {
            return false;
        }
        if (userId != other.userId) {
            return false;
        }
        if (userName == null) {
            if (other.userName != null) {
                return false;
            }
        } else if (!userName.equals(other.userName)) {
            return false;
        }
        return true;
    }

    /**
     * @return the departmentId
     */
    public int getDepartmentId() {
        return departmentId;
    }

    /**
     * @param departmentId
     *            the departmentId to set
     */
    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
    }

    /**
     * @return the initiatorId
     */
    public int getInitiatorId() {
        return initiatorId;
    }

    /**
     * @param initiatorId
     *            the initiatorId to set
     */
    public void setInitiatorId(int initiatorId) {
        this.initiatorId = initiatorId;
    }

}
