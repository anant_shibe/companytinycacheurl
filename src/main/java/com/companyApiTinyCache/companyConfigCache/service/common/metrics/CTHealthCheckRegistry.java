package com.companyApiTinyCache.companyConfigCache.service.common.metrics;

// import com.codahale.metrics.health.HealthCheckRegistry;
import com.yammer.metrics.core.HealthCheckRegistry;

public final class CTHealthCheckRegistry {
	private static final HealthCheckRegistry HEALTH_CHECK_REGISTRY = new HealthCheckRegistry();
	
    public static HealthCheckRegistry getHealthCheckRegistry() {
        return HEALTH_CHECK_REGISTRY;
    }
}
