package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.enums.BookingStatusLabel;
import com.companyApiTinyCache.companyConfigCache.service.common.util.EnumResourceName;

import java.util.*;

/**
 *
 */
public final class CommonEnumConstants {

    private CommonEnumConstants() {
    }

    /**
     * Hotel Suppliers.
     */
    public static enum HotelSupplier {
    Desiya(
        1), Etours(
        2), WCT(
        3), Cleartrip(
        4);

    private int id;

    private HotelSupplier(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    }

    /** Report Type. */
    public enum ReportType {
    AIRLINEWISE_EXPENSE, BOOKING_DETAIL, CITYWISE_EXPENSE, EMPLOYEE_AIR_EXPENSE, EMPLOYEE_HOTEL_EXPENSE, EMPLOYEE_TRAIN_EXPENSE, PERIODIC_EXPENSE, SECTORWISE_EXPENSE, TYPE,
    BOOKING_DETAIL_WITH_APPROVALS;
    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String temp = super.toString().toLowerCase();
        temp = temp.replace('_', ' ');
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    }

    /** Passenger Type. */
    public enum PassengerType {
    ADULT, CHILD, INFANT, MALE_SENIOR, FEMALE_SENIOR;
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }

    public String toProperCase() {
        String temp = super.toString().toLowerCase();
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }
    };

    /** Gender. */
    public enum Gender {
    Male, Female, MALE, FEMALE, M, F
    };

    /** Passenger Type short form. */
    public enum PassengerTypeShort {
    ADT, CHD, INF, SCM, SCF;
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    };

    /** User Details. */
    public static enum UserDetails {
    USER_ID, USER_NAME, SCREEN_NAME, USER_ROLES, ACCESS_ROLES
    }

    /** User data key. */
    public static enum UserDataKey {
    USER_DATA_KEY, USER_NAME, USER_PROFILE
    }

    /** Address type. */
    public static enum AddressType {
    BILLING, HOME, OTHER, PROPERTY, SHIPPING, WORK;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String temp = super.toString().toLowerCase();
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    }

    /** User address type. */
    public static enum UserAddressType {
    HOME, OTHER, WORK;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String temp = super.toString().toLowerCase();
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    }

    /** Phone type. */
    public static enum PhoneType {
    LANDLINE, MOBILE, OTHER, PROPERTY, WORK;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String temp = super.toString().toLowerCase();
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    }

    /** User phone type. */
    public static enum UserPhoneType {
    FAX, HOME, MOBILE, OTHER, WORK;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String temp = super.toString().toLowerCase();
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    }

    /** Email type. */
    public static enum EmailType {
    WORK, HOME, PERSONAL, OTHER;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String temp = super.toString().toLowerCase();
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    }

    /** Company email type. */
    public static enum CompanyEmailType {
    HOME, OTHER, PROPERTY, WORK;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String temp = super.toString().toLowerCase();
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    }

    /** User email type. */
    public static enum UserEmailType {
    HOME, OTHER, WORK;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String temp = super.toString().toLowerCase();
        return temp.substring(0, 1).toUpperCase() + temp.substring(1);
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    }

    /** Produtc type. */
    public static enum ProductType {
    DOMESTIC_AIR, INTERNATIONAL_AIR, HOTEL, TRAIN;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String[] temp = super.toString().toLowerCase().split("_");
        String product = "";
        for (String name : temp) {
        product = product + " " + name.substring(0, 1).toUpperCase() + name.substring(1);
        }
        return product.trim();
    }

    @Override
    public String toString() {
        return super.toString();
    }
    }

    /** Approval condition type. */
    public static enum ApprovalConditionType {
    TOTAL_FARE;

    public String getName() {
        return super.toString().toLowerCase();
    }

    public String getDisplayValue() {
        String[] temp = super.toString().toLowerCase().split("_");
        String approvalCondition = "";
        for (String name : temp) {
        approvalCondition = approvalCondition + " " + name.substring(0, 1).toUpperCase() + name.substring(1);
        }
        return approvalCondition.trim();
    }
    }

    /** Faultcode. */
    public static enum FaultCode {
    LOGIN_FAIL, INVALID_UNAME, USER_EXISTS, REGISTER_FAIL, DEFAULT_CATCH_ALL;
    }

    /** Payment config property. */
    public static enum PaymentConfigProperty {
    LOYALTY_EARN, LOYALTY_BURN, PAYMENT_MODES, GATEWAY_FEES
    }

    /** These are the types activities for agency product dashboard. */
    public static enum ActivityType {
    TRIP_ACTIVITY, NOTE_ACTIVITY, TASK_ACTIVITY, EMAIL_ACTIVITY
    }

    /** Payment validation status code. */
    public static enum PaymentValidationStatusCode {
    // S-Success
    // F-Failure
    // I-Initializing
    S, F, I
    }

    /**
     * @author cleartrip
     */
    public interface LabelledBookingStatus {
    BookingStatusCode getStatus();

    Set<BookingStatusLabel> getLabels();
    }

    /** Booking status code. */
    public static enum BookingStatusCode
        implements LabelledBookingStatus {
    // A-Indicates seats are available
    // Z-no booking has been attempted yet for this booking-info
    // P-booking was successful for this booking-info
    // H-booking attempt failed for this booking-info
    // C-Airline Coupon Applied
    // FC-For cancellation (For Internal Use, Not for TM)
    // CS-Cancellation Successful in connector (For Internal Use, Not for TM)
    // CF-Cancellation Failed in connector (For Internal Use, Not for TM)
    // R-Reserved (this is for agency hold booking status)
    // E-Elapsed (agency hold booking that never got converted)
    // X-Cancelled (agency hold booking that was manually cancelled by the agency)
    // W for pending approval
    // B for pending Booking
    // J-TripRejected
    // M-Amended
    // S-Booking Initiated but confirmation will take time, will be pushed to Delayed Ticketing Queue
    // SBF- Booking failed as per response recieved from supplier
    // ABS - Booking under Air Booking Service {i.e. at present this system is having the booking entity}
        A, P, Z, Q, F, FC, CS, CF, K, E, X, B, J, M, D, Y, G, S, SBF,

        H(BookingStatusLabel.BOOK_FAIL), R(BookingStatusLabel.HOLD_BOOKING), C(BookingStatusLabel.AIRLINE_COUPON), W(BookingStatusLabel.PTA_WAIT),
        L(BookingStatusLabel.FLEXI_PAY);

    private final BookingStatusLabel defaultLabel;

    private BookingStatusCode() {
        this(BookingStatusLabel.NONE);
    }

    private BookingStatusCode(BookingStatusLabel defaultLabel) {
        this.defaultLabel = defaultLabel;
    }

    public BookingStatusLabel getDefaultLabel() {
        return defaultLabel;
    }

    @Override
    public BookingStatusCode getStatus() {
        return this;
    }

    @Override
    public Set<BookingStatusLabel> getLabels() {
        return EnumSet.of(defaultLabel);
    }

    public LabelledBookingStatus withLabels(BookingStatusLabel... labels) {
        return withLabels(this, labels);
    }

    private static LabelledBookingStatus withLabels(final BookingStatusCode status, final BookingStatusLabel... labels) {
        return new LabelledBookingStatus() {

        @Override
        public BookingStatusCode getStatus() {
            return status;
        }

        @Override
        public Set<BookingStatusLabel> getLabels() {
            Set<BookingStatusLabel> setOfLabels = EnumSet.of(BookingStatusLabel.NONE, labels);

            if (setOfLabels.size() > 1) {
            setOfLabels.remove(BookingStatusLabel.NONE);
            }

            return setOfLabels;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof LabelledBookingStatus)) {
            return false;
            }

            LabelledBookingStatus that = (LabelledBookingStatus) obj;

            return (this.getStatus() == that.getStatus()) && this.getLabels().equals(that.getLabels());
        }

        @Override
        public int hashCode() {
            return this.getStatus().hashCode() + this.getLabels().hashCode();
        }
        };
    }
    }

    /** Booking failure type. */
    public static enum BookingFailureType {
    // BOOKING_FAILED, PAYMENT_FAILED, PAYMENT_GIVEN_UP,
    // HOTEL_NOT_AVAILABLE, FLIGHT_NOT_AVAILABLE, IRCTC_DOWN, PRICE_CHANGED, EXCEPTION, TRAIN_NOT_AVAILABLE
    BF, PF, PG, HN, FN, ID, PC, X, TN
    }

    /** Transaction source type. */
    public static enum TxnSourceType {
    // Seen some exceptions in corp logs named "CommonEnumConstants$TxnSourceType.DEPOSIT_ACCOUNT" not found. So DEPOSIT_ACCOUNT is added to the list
    // Seen the error for SYNC also.
    //API_MOBILE added for ext-api customers like Ixigo who use our API from mobile apps (inaddition to Desktop)
    HQ, ACCOUNT, MOBILE, KIOSK, API, CORP, AGENCY, WL, DEPOSIT_ACCOUNT, SYNC, MOBILE_WL, API_MOBILE
    }

    /** Transaction status. */
    public static enum TxnStatus {
    O, C
    }

    // public static enum AffiliateStatusType {
    // ACTIVE(1), INACTIVE(2), PENDING(3);
    // private int id;
    //
    // private AffiliateStatusType(int id) {
    // this.id = id;
    // }
    //
    // public int getId() {
    // return id;
    // }
    // }

    /** Common URLs. */
    public static enum CommonUrlResources
        implements EnumResourceName {
    ACUBE_SERVER_KEY(
        "$ct.services.authentication.acube.url"), CT_UI_HOME_LOGO(
        "_brand_home.html"), CT_UI_HEADER_LOGO(
        "_brand.html"), CT_UI_PRIMARY_NAV(
        "_primary_nav.html"), CT_UI_FOOTERS(
        "_footer.html"), CT_UI_PHONE_NO_MAIN(
        "_main.html"), CT_UI_PHONE_NO_CONTACT(
        "_contact.html"), CT_UI_PHONE_NO_CALL_CHAPS(
        "_call_chaps.html"), CT_UI_PHONE_NO_CALL_COMMON(
        "_common.html"), CT_UI_HOTEL_OFFERS(
        "_hotel_offers.html"), CT_UI_HOTEL_ANNOUNCEMENTS(
        "_hotels.html"), CT_UI_HOTEL_RATING_MSG(
        "_hotel_rating_explained.html");

    private CommonUrlResources(String presourceName) {
        resourceName = presourceName;
    }

    private String resourceName;

    @Override
    public String getResourceName() {
        return resourceName;
    }
    }

    /** Logging levels. */
    public static enum LogLevel {
    DEBUG(
        1), INFO(
        2), WARNING(
        3), ERROR(
        4), FATAL(
        5);

    private int rank;

    private LogLevel(int prank) {
        rank = prank;
    }

    /**
     * Getter for rank.
     * @return the rank
     */
    public int getRank() {
        return rank;
    }
    }

    /** API type. */
    public static enum ApiType {
    SEARCH_AVAILABILITY, SEARCH_FARE, SEARCH_FARE_RULE, SEARCH_TAX, BOOK_SEAT_SELL, BOOK, CANCEL, TICKETING, TM_LOG, TM_UPDATE, PAYMENT_VALIDATE, PAYMENT, PAYMENT_STATUS, CAB_SAVE, CAB_BOOK,
    CAB_UPDATE, CAB_CANCEL, OTHER, RE_PRICE, RE_BOOK, FARE_INFO_PRICING, MINIRULE, RETRIEVE_PNR, BOOK_APP_LOCK, BAGGAGE_INFO, WEBCHECKIN, ATC_BOOK_SEAT_SELL, ATC_TICKETING,
        ANALYTICS, TM_RETRIEVE, FARERULE, ABS_BOOK
    }

    /**
     * Ideally this should be in AirEnumConstants. Has been added here because ApiCallStats from common refers this
     */
    public static enum DirectionType {
    ONWARD, RETURN, ROUNDTRIP
    }

    public static final String CT_DOMAIN = ".cleartrip.com";

    public static final String SESSION_SECURITY_BEAN_ID = "session_security_bean_id";

    public static final String CONCUR_USER_ID = "concur_user_id";

    public static final String UNCONFIRMED = "Unconfirmed";

    public static final String NET_BANKING_PARAM_KEY = "NET_BANKING_PARAM_KEY";

    public static final int HOTEL_NOT_AVAILABLE = 400;

    public static final String AIR_PAYMENT_BOOK_CRITERIA = "airPaymentBookCriteria";

    public static final String HOTEL_PAYMENT_BOOK_CRITERIA = "hotelPaymentBookCriteria";

    public static final String TRAIN_PAYMENT_BOOK_CRITERIA = "trainPaymentBookCriteria";

    public static final String AIR_SEARCH_CRITERIA = "airSearchCriteria";

    public static final String HOTEL_SEARCH_CRITERIA = "hotelSearchCriteria";

    public static final String TRAIN_SEARCH_CRITERIA = "trainSearchCriteria";

    public static final String BOOK_CRITERIA = "bookCriteria";

    public static final String ALL_TRIPS_LIST = "alltripslist";
    public static final String FAILED_TRIPS_LIST = "failedtripslist";
    public static final String COMPLETED_TRIPS_LIST = "completedtripslist";
    public static final String CANCELLED_TRIPS_LIST = "cancelledtripslist";
    public static final String UPCOMING_TRIPS_LIST = "upcomingtripslist";
    public static final String DEPARTMENTS_LIST = "departmentslist";

    public static final String APP_REGISTRY_KEY_PREFIX = "APP_REGISTRY";

    public static final String APP_REGISTRY_INFO_KEY_PREFIX = APP_REGISTRY_KEY_PREFIX + "_INFO";

    private static Map<String, String> getCountryCodeMap() {
    Map<String, String> countryCodeMap = new LinkedHashMap<String, String>(300);
    countryCodeMap.put("AB", "Abu Dhabi");
    countryCodeMap.put("AF", "Afghanistan");
    countryCodeMap.put("AJ", "Ajman");
    countryCodeMap.put("AL", "Albania");
    countryCodeMap.put("DZ", "Algeria");
    countryCodeMap.put("AS", "American Samoa");
    countryCodeMap.put("AD", "Andorra");
    countryCodeMap.put("AO", "Angola");
    countryCodeMap.put("AI", "Anguilla");
    countryCodeMap.put("AG", "Antigua and Barbuda");
    countryCodeMap.put("AR", "Argentina");
    countryCodeMap.put("AM", "Armenia");
    countryCodeMap.put("AW", "Aruba");
    countryCodeMap.put("AU", "Australia");
    countryCodeMap.put("AT", "Austria");
    countryCodeMap.put("AZ", "Azerbaijan");
    countryCodeMap.put("BS", "Bahamas");
    countryCodeMap.put("BH", "Bahrain");
    countryCodeMap.put("BD", "Bangladesh");
    countryCodeMap.put("BB", "Barbados");
    countryCodeMap.put("BY", "Belarus");
    countryCodeMap.put("BE", "Belgium");
    countryCodeMap.put("BZ", "Belize");
    countryCodeMap.put("BJ", "Benin");
    countryCodeMap.put("BM", "Bermuda");
    countryCodeMap.put("BT", "Bhutan");
    countryCodeMap.put("BO", "Bolivia");
    countryCodeMap.put("BA", "Bosnia and Herzegovina");
    countryCodeMap.put("BW", "Botswana");
    countryCodeMap.put("BR", "Brazil");
    countryCodeMap.put("IO", "British Indian Ocean Territory");
    countryCodeMap.put("VG", "British Virgin Islands");
    countryCodeMap.put("WI", "British West Indies");
    countryCodeMap.put("BH", "Bahrain");
    countryCodeMap.put("BN", "Brunei");
    countryCodeMap.put("BG", "Bulgaria");
    countryCodeMap.put("BF", "Burkina Faso");
    countryCodeMap.put("BU", "Burma");
    countryCodeMap.put("BI", "Burundi");
    countryCodeMap.put("KH", "Cambodia");
    countryCodeMap.put("CM", "Cameroon");
    countryCodeMap.put("CA", "Canada");
    countryCodeMap.put("CV", "Cape Verde");
    countryCodeMap.put("KY", "Cayman Islands");
    countryCodeMap.put("CF", "Central African Republic");
    countryCodeMap.put("TD", "Chad");
    countryCodeMap.put("CL", "Chile");
    countryCodeMap.put("CN", "China");
    countryCodeMap.put("CX", "Christmas Island");
    countryCodeMap.put("CC", "Cocos (Keeling) Islands");
    countryCodeMap.put("CO", "Colombia");
    countryCodeMap.put("KM", "Comoros");
    countryCodeMap.put("CG", "Congo");
    countryCodeMap.put("CK", "Cook Islands");
    countryCodeMap.put("CR", "Costa Rica");
    countryCodeMap.put("CI", "Cote d'Ivoire");
    countryCodeMap.put("HR", "Croatia");
    countryCodeMap.put("CU", "Cuba");
    countryCodeMap.put("CY", "Cyprus");
    countryCodeMap.put("CZ", "Czech Republic");
    countryCodeMap.put("CD", "Democratic Republic of Congo");
    countryCodeMap.put("DK", "Denmark");
    countryCodeMap.put("DJ", "Djibouti");
    countryCodeMap.put("DM", "Dominica");
    countryCodeMap.put("DO", "Dominican Republic");
    countryCodeMap.put("DB", "Dubai");
    countryCodeMap.put("XU", "East of the Urals");
    countryCodeMap.put("TP", "East Timor");
    countryCodeMap.put("EC", "Ecuador");
    countryCodeMap.put("EG", "Egypt");
    countryCodeMap.put("SV", "El Salvador");
    countryCodeMap.put("GQ", "Equatorial Guinea");
    countryCodeMap.put("ER", "Eritrea");
    countryCodeMap.put("EE", "Estonia");
    countryCodeMap.put("ET", "Ethiopia");
    countryCodeMap.put("FK", "Falkland Islands (Islas Malvinas)");
    countryCodeMap.put("FO", "Faroe Islands");
    countryCodeMap.put("FJ", "Fiji");
    countryCodeMap.put("FI", "Finland");
    countryCodeMap.put("YU", "Former Yugoslavia");
    countryCodeMap.put("FR", "France");
    countryCodeMap.put("GF", "French Guiana");
    countryCodeMap.put("PF", "French Polynesia");
    countryCodeMap.put("TF", "French Southern Territories");
    countryCodeMap.put("FJ", "Fujairah");
    countryCodeMap.put("GA", "Gabon");
    countryCodeMap.put("GM", "Gambia");
    countryCodeMap.put("XA", "Gaza");
    countryCodeMap.put("GE", "Georgia");
    countryCodeMap.put("DE", "Germany");
    countryCodeMap.put("GH", "Ghana");
    countryCodeMap.put("GI", "Gibraltar");
    countryCodeMap.put("GR", "Greece");
    countryCodeMap.put("GL", "Greenland");
    countryCodeMap.put("GD", "Grenada");
    countryCodeMap.put("GP", "Guadeloupe");
    countryCodeMap.put("GU", "Guam");
    countryCodeMap.put("GT", "Guatemala");
    countryCodeMap.put("xx", "Guernsey");
    countryCodeMap.put("GN", "Guinea");
    countryCodeMap.put("GW", "Guinea-Bissau");
    countryCodeMap.put("GY", "Guyana");
    countryCodeMap.put("HT", "Haiti");
    countryCodeMap.put("HM", "Heard And Mcdonald Islands");
    countryCodeMap.put("XH", "Held Territories");
    countryCodeMap.put("VA", "Holy See (Vatican City)");
    countryCodeMap.put("HN", "Honduras");
    countryCodeMap.put("HK", "Hong Kong");
    countryCodeMap.put("HU", "Hungary");
    countryCodeMap.put("IS", "Iceland");
    countryCodeMap.put("IN", "India");
    countryCodeMap.put("XI", "Indian Ocean Islands");
    countryCodeMap.put("ID", "Indonesia");
    countryCodeMap.put("IR", "Iran");
    countryCodeMap.put("IQ", "Iraq");
    countryCodeMap.put("IE", "Ireland");
    countryCodeMap.put("IM", "Isle of Man");
    countryCodeMap.put("IL", "Israel");
    countryCodeMap.put("IT", "Italy");
    countryCodeMap.put("JM", "Jamaica");
    countryCodeMap.put("JP", "Japan");
    countryCodeMap.put("JE", "Jersey");
    countryCodeMap.put("JO", "Jordan");
    countryCodeMap.put("KZ", "Kazakhstan");
    countryCodeMap.put("KE", "Kenya");
    countryCodeMap.put("KI", "Kiribati");
    countryCodeMap.put("KP", "Korea");
    countryCodeMap.put("KR", "Korea");
    countryCodeMap.put("KW", "Kuwait");
    countryCodeMap.put("KG", "Kyrgyzstan");
    countryCodeMap.put("LA", "Laos");
    countryCodeMap.put("LV", "Latvia");
    countryCodeMap.put("LB", "Lebanon");
    countryCodeMap.put("LS", "Lesotho");
    countryCodeMap.put("LR", "Liberia");
    countryCodeMap.put("LY", "Libya");
    countryCodeMap.put("LI", "Liechtenstein");
    countryCodeMap.put("LT", "Lithuania");
    countryCodeMap.put("LU", "Luxembourg");
    countryCodeMap.put("MO", "Macau");
    countryCodeMap.put("MK", "Macedonia");
    countryCodeMap.put("MG", "Madagascar");
    countryCodeMap.put("MW", "Malawi");
    countryCodeMap.put("MY", "Malaysia");
    countryCodeMap.put("MV", "Maldives");
    countryCodeMap.put("ML", "Mali");
    countryCodeMap.put("MT", "Malta");
    countryCodeMap.put("MH", "Marshall Islands");
    countryCodeMap.put("MQ", "Martinique");
    countryCodeMap.put("MR", "Mauritania");
    countryCodeMap.put("MU", "Mauritius");
    countryCodeMap.put("YT", "Mayotte");
    countryCodeMap.put("MX", "Mexico");
    countryCodeMap.put("FM", "Micronesia");
    countryCodeMap.put("MD", "Moldova");
    countryCodeMap.put("MC", "Monaco");
    countryCodeMap.put("MN", "Mongolia");
    countryCodeMap.put("ME", "Montenegro");
    countryCodeMap.put("MS", "Montserrat");
    countryCodeMap.put("MA", "Morocco");
    countryCodeMap.put("MZ", "Mozambique");
    countryCodeMap.put("MM", "Myanmar (Burma)");
    countryCodeMap.put("NA", "Namibia");
    countryCodeMap.put("NR", "Nauru");
    countryCodeMap.put("NP", "Nepal");
    countryCodeMap.put("NL", "Netherlands");
    countryCodeMap.put("AN", "Netherlands Antilles");
    countryCodeMap.put("NC", "New Caledonia");
    countryCodeMap.put("NZ", "New Zealand");
    countryCodeMap.put("NI", "Nicaragua");
    countryCodeMap.put("NE", "Niger");
    countryCodeMap.put("NG", "Nigeria");
    countryCodeMap.put("NU", "Niue");
    countryCodeMap.put("NF", "Norfolk Island");
    countryCodeMap.put("XB", "Northern Ireland");
    countryCodeMap.put("MP", "Northern Mariana Islands");
    countryCodeMap.put("NO", "Norway");
    countryCodeMap.put("OM", "Oman");
    countryCodeMap.put("PC", "Pacific Trust Territories");
    countryCodeMap.put("PK", "Pakistan");
    countryCodeMap.put("PW", "Palau");
    countryCodeMap.put("PS", "Palestine");
    countryCodeMap.put("PA", "Panama");
    countryCodeMap.put("PG", "Papua New Guinea");
    countryCodeMap.put("PY", "Paraguay");
    countryCodeMap.put("PE", "Peru");
    countryCodeMap.put("PH", "Philippines");
    countryCodeMap.put("PN", "Pitcairn Islands");
    countryCodeMap.put("PL", "Poland");
    countryCodeMap.put("PT", "Portugal");
    countryCodeMap.put("PR", "Puerto Rico");
    countryCodeMap.put("QA", "Qatar");
    countryCodeMap.put("RK", "Ras al-Khaimah");
    countryCodeMap.put("RE", "Reunion");
    countryCodeMap.put("RO", "Romania");
    countryCodeMap.put("RU", "Russia");
    countryCodeMap.put("RW", "Rwanda");
    countryCodeMap.put("KN", "Saint Kitts and Nevis");
    countryCodeMap.put("LC", "Saint Lucia");
    countryCodeMap.put("PM", "Saint Pierre and Miquelon");
    countryCodeMap.put("VC", "Saint Vincent and the Grenadines");
    countryCodeMap.put("SM", "San Marino");
    countryCodeMap.put("ST", "Sao Tome and Principe");
    countryCodeMap.put("SA", "Saudi Arabia");
    countryCodeMap.put("WY", "Scotland");
    countryCodeMap.put("SN", "Senegal");
    countryCodeMap.put("CS", "Serbia");
    countryCodeMap.put("RS", "Serbia");
    countryCodeMap.put("SC", "Seychelles");
    countryCodeMap.put("SR", "Sharjah");
    countryCodeMap.put("SL", "Sierra Leone");
    countryCodeMap.put("SG", "Singapore");
    countryCodeMap.put("SK", "Slovakia");
    countryCodeMap.put("SI", "Slovenia");
    countryCodeMap.put("SB", "Solomon Islands");
    countryCodeMap.put("SO", "Somalia");
    countryCodeMap.put("ZA", "South Africa");
    countryCodeMap.put("GS", "South Georgia");
    countryCodeMap.put("ES", "Spain");
    countryCodeMap.put("LK", "Sri Lanka");
    countryCodeMap.put("MF", "St, Martin");
    countryCodeMap.put("BL", "St. Barts (St. Barthelemy)");
    countryCodeMap.put("SH", "St. Helena");
    countryCodeMap.put("SD", "Sudan");
    countryCodeMap.put("SU", "Suriname");
    countryCodeMap.put("SJ", "Svalbard and Jan Mayen Islands");
    countryCodeMap.put("SZ", "Swaziland");
    countryCodeMap.put("SE", "Sweden");
    countryCodeMap.put("CH", "Switzerland");
    countryCodeMap.put("SY", "Syria");
    countryCodeMap.put("TW", "Taiwan");
    countryCodeMap.put("TJ", "Tajikistan");
    countryCodeMap.put("TZ", "Tanzania");
    countryCodeMap.put("TH", "Thailand");
    countryCodeMap.put("TG", "Togo");
    countryCodeMap.put("TK", "Tokelau");
    countryCodeMap.put("TO", "Tonga");
    countryCodeMap.put("TT", "Trinidad and Tobago");
    countryCodeMap.put("TN", "Tunisia");
    countryCodeMap.put("TR", "Turkey");
    countryCodeMap.put("TM", "Turkmenistan");
    countryCodeMap.put("TC", "Turks and Caicos Islands");
    countryCodeMap.put("TV", "Tuvalu");
    countryCodeMap.put("UM", "U.S. Minor Outlying Islands");
    countryCodeMap.put("UG", "Uganda");
    countryCodeMap.put("UA", "Ukraine");
    countryCodeMap.put("UQ", "Umm al-Quwain");
    countryCodeMap.put("AE", "United Arab Emirates");
    countryCodeMap.put("GB", "United Kingdom");
    countryCodeMap.put("US", "United States");
    countryCodeMap.put("UY", "Uruguay");
    countryCodeMap.put("UZ", "Uzbekistan");
    countryCodeMap.put("VU", "Vanuatu");
    countryCodeMap.put("VE", "Venezuela");
    countryCodeMap.put("VN", "Vietnam");
    countryCodeMap.put("VI", "Virgin Islands");
    countryCodeMap.put("WX", "Wales");
    countryCodeMap.put("WF", "Wallis and Futuna");
    countryCodeMap.put("EH", "Western Sahara");
    countryCodeMap.put("WS", "Western Samoa");
    countryCodeMap.put("YE", "Yemen");
    countryCodeMap.put("ZR", "Zaire (Dem Rep of Congo)");
    countryCodeMap.put("ZM", "Zambia");
    countryCodeMap.put("ZW", "Zimbabwe");
    return countryCodeMap;
    }

    public static final Map<String, String> COUNTRY_CODE_MAP = getCountryCodeMap();

    public static final long MILLIS_IN_DAY = 24 * 60 * 60 * (long) 1000;

    private static List<String> getHDFCBins() {
    List<String> hdfcBins = new ArrayList<String>();
    hdfcBins.add("434677");
    hdfcBins.add("434678");
    hdfcBins.add("528945");
    hdfcBins.add("556620");
    hdfcBins.add("517635");
    hdfcBins.add("517652");
    hdfcBins.add("421332");
    hdfcBins.add("421331");
    hdfcBins.add("421320");
    hdfcBins.add("556042");
    hdfcBins.add("418136");
    hdfcBins.add("553115");
    hdfcBins.add("515710");
    hdfcBins.add("524368");
    hdfcBins.add("552344");
    hdfcBins.add("531831");
    hdfcBins.add("526419");
    hdfcBins.add("524877");
    hdfcBins.add("421424");
    hdfcBins.add("421578");
    hdfcBins.add("405028");
    hdfcBins.add("404276");
    hdfcBins.add("438624");
    hdfcBins.add("486575");
    hdfcBins.add("416021");
    hdfcBins.add("418219");
    hdfcBins.add("418234");
    hdfcBins.add("418235");
    hdfcBins.add("418254");
    hdfcBins.add("418255");
    hdfcBins.add("418256");
    hdfcBins.add("418257");
    hdfcBins.add("418258");
    hdfcBins.add("418264");
    hdfcBins.add("421321");
    hdfcBins.add("421322");
    hdfcBins.add("421328");
    hdfcBins.add("428326");
    hdfcBins.add("430265");
    hdfcBins.add("490246");
    hdfcBins.add("418261");
    hdfcBins.add("461701");
    hdfcBins.add("461786");
    hdfcBins.add("461787");
    hdfcBins.add("464586");
    hdfcBins.add("483595");
    return hdfcBins;
    }

    public static final List<String> HDFC_BIN_LIST = getHDFCBins();

    /** Mobile express checkout cookie name. */
    public static final String EXPRESSO_COOKIE_NAME = "ct.mobile.air.espresso";

    /** Company Config. */
    public static enum CompanyConfig {
    HOLD_BOOKINGS, HOLD_TIME, SEARCH_ENABLED, INSURANCE_APPLICABLE, SEND_MARKETING_EMAIL_FLAG, DOM_AIRLINE_LIST, INTL_AIRLINE_LIST, SHOW_MULTICITY, SHOW_AIR_FARE_CALENDAR, USER_LOGIN_ENABLED,
    AFFILIATE_PARTNER_ID_EXPIRY_PERIOD, PAYMENT_TYPES, CC_PAYMENT_MODES, COMPANY_SCREEN_NAME, HOME_URL, SEARCH_HOME_URL, MULTICITY_HOME_URL, LOGO_ALT, LOGO_TITLE, DOM_AIR_TERMS_AND_CONDS_URL,
    DOM_AIR_BOOKING_POLICY_URL, INTL_AIR_TERMS_AND_CONDS_URL, INTL_AIR_BOOKING_POLICY_URL, SUPPORT_EMAIL_ID, SHOW_COUPON_CODE, INTL_AIR_SUPPORT_TIME, DOM_AIR_SUPPORT_TIME, MARKETING_EMAIL_OR_URL,
    CROSS_SELL_URL,
    CROSS_SELL_URL_FOR_CONF_EMAIL,
    POP_UNDER_URL,
    LOGO_URL,
    EMAIL_LOGO_URL,
    URL_PATH_PREFIX,
    CUSTOM_CSS,
    BILLING_CURRENCY,
    TWITTER_ID,
    SHOW_TRIPS_URL,
    SHOW_TRIPS_URL_EMAIL,
    DIRECT_DEBIT_REDIRECT_URL,
    OTP_URL,
    CALLBACK_MESSAGE,
    REFUND_MESSAGE,
    CORP_DEPOSIT_ACCT_INCENTIVE_PERCENTAGE,
    CORP_RESELLER_COMPANY_ID,
    COMPANY_CODE,
    RESOURCE_PATH,
    ENABLED_PRODUCTS,
    DEAL_OF_THE_DAY,
    PAY_AT_HOTEL,
    PAY_AT_HOTEL_CC,
    PART_PAY,
    PROHIBITED_SUPPLIERS,
    GOODIES,
    ROOMER,
    SINGLE_PAGE_BOOK_FLOW_ENABLE,
    SUPPORT_NUMBER,
    OPAQUE,
    PRIVACY_LINK,

    // Search Results Page
    SRP_TF_AIR_DOM_PHONE_NUM,
    SRP_LL_AIR_DOM_PHONE_NUM,
    SRP_TF_AIR_INTL_PHONE_NUM,
    SRP_LL_AIR_INTL_PHONE_NUM,

    // Book Review Page
    BRP_TF_AIR_DOM_PHONE_NUM,
    BRP_LL_AIR_DOM_PHONE_NUM,
    BRP_TF_AIR_INTL_PHONE_NUM,
    BRP_LL_AIR_INTL_PHONE_NUM,

    // Booking Error Pages
    BEP_TF_AIR_DOM_PHONE_NUM,
    BEP_LL_AIR_DOM_PHONE_NUM,
    BEP_TF_AIR_INTL_PHONE_NUM,
    BEP_LL_AIR_INTL_PHONE_NUM,

    // Mobile specific enums
    MOBILE_HTML5_ENABLE, MOBILE_HTML4_ENABLE, EXPRESS_CHECKOUT_ENABLE, MOBILE_HTML4_LOGO_URL, MOBILE_HTML5_LOGO_URL, MOBILE_HTML4_LOGO_ALT, MOBILE_HTML5_LOGO_ALT, MOBILE_HTML4_HOME_URL,
    MOBILE_HTML5_HOME_URL, MOBILE_HTML4_TITLE, MOBILE_HTML5_TITLE, MOBILE_HTML4_TRAINS_ENABLE, MOBILE_HTML5_TRAINS_ENABLE,
    MOBILE_HTML4_TRAINS_CALENDAR_ENABLE,
    MOBILE_HTML5_TRAINS_CALENDAR_ENABLE,
    MOBILE_HTML4_BUS_ENABLE,
    MOBILE_HTML5_BUS_ENABLE,
    NET_BANKING_ENABLE,
    AMEX_CARD_ENABLE,
    FEATURE_TAB_ENABLE,
    MOBILE_HOTEL_CUSTOMER_SUPPORT_PH,
    MOBILE_HOTEL_ENABLE,
    MOBILE_INTERNATIONAL_AIR_ENABLE,
    MOBILE_HTML5_PNRCHECK_ENABLE,
    MOBILE_HTML4_PNRCHECK_ENABLE,

    MOBILE_HTML5_WEEKEND_GETAWAY_ENABLE,
    MOBILE_HTML4_WEEKEND_GETAWAY_ENABLE,
    //To enable train running status on mobile website.
    MOBILE_HTML5_TRAINS_RUNNING_STATUS_ENABLE,

    // Mobile WL or Desktop WL
    MOBILE_ENABLED,
    DESKTOP_ENABLED,
    IS_PHONE_BOOKING_ENABLED,

    // Support number for SMS
    SMS_AIR_PHONE_NUM,

    // Mobile whitelabel constants
    FROM_EMAIL_ID,
    SMS_MASK,

    // Corporate Fares changes
    SG_PROMO_CODE, SG_DISCOUNT_TYPE, SG_DISCOUNT_AMOUNT, SG_APPLICABILITY_TYPE, SG_APPLICABLE_VALUES, SG_RETAIL_DISCOUNT_TYPE, SG_RETAIL_DISCOUNT_AMOUNT, SG_RETAIL_PROMO_CODE,
    CACHING_ID,
    CORP_FARES_ENABLED,
    GAL_TOUR_CODE,

    // GALILEO CORP FARE CHANGES
    GALILEO_CORP_AIRLINES, GALILEO_CORP_DEAL_CODE, GALILEO_CORP_FARES_ENABLED, GALILEO_INTL_CORP_AIRLINES, GALILEO_INTL_CORP_DEAL_CODE,
    GALILEO_INTL_CORP_FARES_ENABLED,

    // PRM-7205
    INDIGO_DISCOUNT_TYPE, INDIGO_DISCOUNT_AMOUNT, INDIGO_APPLICABILITY_TYPE, INDIGO_APPLICABLE_VALUES,
    INDIGO_RETAIL_DISCOUNT_TYPE,
    INDIGO_RETAIL_DISCOUNT_AMOUNT,
    INDIGO_PROMO_CODE,
    INDIGO_RETAIL_PROMO_CODE,
    // Ended
    // PRM-7777
    G8_CORP_FARES_ENABLED, G8_APPLICABILITY_TYPE, G8_APPLICABLE_VALUES, G8_CORP_TA_LOGIN_ID, G8_CORP_TA_USERNAME, G8_CORP_TA_PASSWORD, G8_DISCOUNT_AMOUNT, G8_DISCOUNT_TYPE,
    G8_RETAIL_DISCOUNT_TYPE, G8_RETAIL_DISCOUNT_AMOUNT, G8_PROMO_CODE, G8_RETAIL_PROMO_CODE,
    G8_CORP_PROMO_CODE, G8_ALLOWED_FAREGROUPS,

    // Ended
    // PRM-7117
    AMADEUS_CORP_AIRLINES, AMADEUS_CORP_DEAL_CODE, AMADEUS_CORP_FARES_ENABLED,
    // Ended

    HEADER, FOOTER, DOM_AIR_SUPPORT_PHONE_NO, UNKNOWN,

    // Corporate Pre-Trip Approval changes
    CORP_PTA_ENABLED, CORP_PTA_BOOKER, CORP_PTA_DEFAULT_APPROVER, CORP_PTA_ALT_APPROVER, CORP_PTA_ESCALATION_TIME, CORP_PTA_TRIP_EXPIRY_TIME, CORP_PTA_EMERGENCY_BOOKING_ALLOWED,
    CORP_PTA_PRICE_THRESHOLD_PERCENTAGE, CORP_PAYMENT_MODES,

    // AGENCY WHITE LEVEL PROPS
    AGENCY_ROLE_CONFIG, AGENCY_PAYMENT_MODES, AGENCY_FLEXI_PAYMENT_MODES, AGENCY_EXCLUDE_GOAIR_TTF, AGENCY_EXCLUDE_9W_TTF,

    // NEED TO STORE INTRODUCER'S(OUR REPRESENTATIVE) EMAIL WHILE DOMAIN REGISTRATION TO DISPLAY IT IN M.I.S. REPORTS
    COMPANY_SALES_REPRESENTATIVE,

    // CONFIG TO TELL WHETHER TO REFUND THE IMPORT FEE OR NOT
    CHARGE_IMPORT_FEE,
    //config for sending deal codes for intl search
    AMADEUS_INTL_CORP_AIRLINES,
    AMADEUS_INTL_CORP_DEAL_CODE,
    AMADEUS_INTL_CORP_FARES_ENABLED,
    INDIGO_CAL_GST_ON_DIS_BF,
    SG_CAL_GST_ON_DIS_BF,

    // Company Configuration for Hotel WL
    ISCFH, HEADERIMGLOC, HEADERIMGPOS, CFHOTELNAME, CSSTEMPLATE,

    //PRM-15077
    IS_CORP_RETAIL_INDIVIDUAL_CALLS,
    MOBILE_PAY_AT_HOTEL,
    MOBILE_PART_PAY,
    MOBILE_PROHIBITED_SUPPLIERS,
    MOBILE_GOODIES,
    MOBILE_ROOMER,MOBILE_DEAL_OF_THE_DAY,

    INTL_ST_DEFAULT_PERCENTAGE,
    INTL_ST_DEFAULT_PRICING_ELEMENTS,
    DOM_ST_DEFAULT_PERCENTAGE,
    DOM_ST_DEFAULT_PRICING_ELEMENTS,

    AI_DOM_ST_PERCENTAGE,
    AI_DOM_ST_PRICING_ELEMENTS,
    JET_DOM_ST_PERCENTAGE,
    JET_DOM_ST_PRICING_ELEMENTS,
    AMADEUS_INTERNATIONAL_INTL_ST_PERCENTAGE,
    AMADEUS_INTERNATIONAL_INTL_ST_PRICING_ELEMENTS,
    GALILEO_LFS_INTERNATIONAL_INTL_ST_PERCENTAGE,
    GALILEO_LFS_INTERNATIONAL_INTL_ST_PRICING_ELEMENTS,
    SG_DOM_ST_PERCENTAGE,
    SG_DOM_ST_PRICING_ELEMENTS,
    SPICEJET_INTL_ST_PERCENTAGE,
    SPICEJET_INTL_ST_PRICING_ELEMENTS,
    INDIGO_DOM_ST_PERCENTAGE,
    INDIGO_DOM_ST_PRICING_ELEMENTS,
    INDIGO_INTL_ST_PERCENTAGE,
    INDIGO_INTL_ST_PRICING_ELEMENTS,
    G8_DOM_ST_PERCENTAGE,
    G8_DOM_ST_PRICING_ELEMENTS,
    LB_DOM_ST_PERCENTAGE,
    LB_DOM_ST_PRICING_ELEMENTS,
    IX_DOM_ST_PERCENTAGE,
    IX_DOM_ST_PRICING_ELEMENTS,
    AIE_INTL_ST_PERCENTAGE,
    AIE_INTL_ST_PRICING_ELEMENTS,
    I5_DOM_ST_PERCENTAGE,
    I5_DOM_ST_PRICING_ELEMENTS,
    AIR_ASIA_INTL_ST_PERCENTAGE,
    AIR_ASIA_INTL_ST_PRICING_ELEMENTS,
    S2_DOM_ST_PERCENTAGE,
    S2_DOM_ST_PRICING_ELEMENTS,
    TIGER_AIRWAYS_INTL_ST_PERCENTAGE,
    TIGER_AIRWAYS_INTL_ST_PRICING_ELEMENTS,
    OP_DOM_ST_PERCENTAGE,
    OP_DOM_ST_PRICING_ELEMENTS,
    UK_DOM_ST_PERCENTAGE,
    UK_DOM_ST_PRICING_ELEMENTS,
    TRUJET_DOM_ST_PERCENTAGE,
    TRUJET_DOM_ST_PRICING_ELEMENTS,
    FLY_DUBAI_INTL_ST_PERCENTAGE,
    FLY_DUBAI_INTL_ST_PRICING_ELEMENTS,
    AIR_CARNIVAL_DOM_ST_PERCENTAGE,
    AIR_CARNIVAL_DOM_ST_PRICING_ELEMENTS,

    UNSELECT_INSURANCE,
    MANDATE_AIR_TAGS,
    MANDATE_HOTEL_TAGS,

    IMPORT_PNR_ENABLED,

    MAX_COUPON_LIMIT,
    MAX_GV_LIMIT,
    SEND_SMS,

    QWIKCILVER_GV_GROUP,

    IS_COMPANY_LEVEL_PASS_THRU,
    PASS_THRU_9W,
    PASS_THRU_AI,
    PASS_THRU_UK,
    PASS_THRU_CTA,

    VAT_NUMBER,
    VAT_HOLDER_NAME,
    VAT_HOLDER_ADDRESS,
    VAT_TYPE,

    //GST Invoice Email Id
    GST_INVOICE_EMAIL_ADDRESS;

    }

    public static final int ACCOUNTS_API_DELETE_VALUE = -999;

    /** Tag status. */
    public static enum TagStatus {
    A, D
    }

    /** Update itinerary status. */
    public enum UpdateItineraryStatus {
    Applicable, NotApplicable, RetrievePNRFailed, ParsingFailed
    };

    /**
     * HQ source type is required. PNR Sync is called with source type as HQ. Seen some exceptions named "CommonEnumConstats.SourseType.SYNC" not found so SYNC is added to the list . Seen some
     * exceptions in corp logs named "CommonEnumConstants$TxnSourceType.DEPOSIT_ACCOUNT" not found. So DEPOSIT_ACCOUNT is added to the list
     */
    public static enum SourceType {
    B2C, API, CORP, AGENCY, MOBILE, WL, ACCOUNT, HQ, SYNC, DEPOSIT_ACCOUNT, MOBILE_WL, ALERTONE, ACTIVITY_SUPPLIERS , IOS, API_MOBILE
    }

    public static enum AppAgents {
        WINDOWS , ANDROID , IOS
        }

    /** CACHE: Pass through Cache, CACHE_ONLY: Fetch only cache, DB: Fetch from DB and update cache, DB_ONLY: Only fetch from DB. */
    public static enum CacheSourceType {
    CACHE, CACHE_ONLY, DB, DB_ONLY
    }

    /** Camm Fare type. */
    public static enum CammFareType {
    B2C(
        0), DP(
        1), B2B(
        2);

    private int fareType;

    private CammFareType(int type) {
        fareType = type;
    }

    public int getFareType() {
        return fareType;
    }
    }

    /**
     * @author cleartrip
     */
    public static enum BookingInfoType {
    AIR, HOTEL
    }

    public static enum BookInternalCallStatus {
		P, R, B, X, N, F
	}

    public enum GETTimeouts {
  		SKYPICKER_SEARCH(120000, "ct.air.skypicker.search.timeout"),
  		SKYPICKER_CHECK_FLIGHTS(120000, "ct.air.skypicker.check.flight.timeout"),
  		TM_RATERULE_API(120000, "ct.air.tm.raterule.api.timeout"),
  		COMMON_RATERULE_API(120000, "ct.services.common.api.raterule-api.url.timeout"),
  		TM_FARERULE_API(120000, "ct.services.tm.api.domestic.farerules.url.timeout"),
  		TM_MINIRULE_API(120000, "ct.air.domestic.minirule.service.url.timeout"),
  		PLACES_API(120000, "ct.services.places.country.list.url.timeout"),
  		SMALLWORLD_API(120000, "ct.air.smallworld.airport.url.timeout"),
  		GROUP_COUPONS_URL(120000, "ct.services.tm.api.group-coupon.url.timeout"),
  		GROUP_COUPONS_CODES_URL(120000, "ct.services.get.goodie.coupon.codes.url.timeout"),
  		VALIDATE_COUPON_CODES_URL(120000, "ct.services.air.coupon.code.validate.url.timeout"),
  		TRIP_LIST_URL(120000, "ct.services.tm.api.trip-list.url.timeout"),
  		PAYMENT_STATUS_URL(120000, "ct.services.payment.api.status.url.timeout"),
  		DEPOSIT_ACCT_STATUS_URL(120000, "ct.services.payment.api.status.url.timeout"),
  		DEPOSIT_ACCT_CREATE_URL(120000, "ct.services.payment.api.status.url.timeout"),
  		DEPOSIT_ACCT_EDIT_URL(120000, "ct.services.payment.api.status.url.timeout"),
  		MARKETING_URL(120000, "ct.services.marketing.url.timeout"),
  		RETRIEVE_PNR_URL(120000, "ct.services.air.api.retrievepnr.url.timeout"),
  		TM_TXN_STATUS(120000, "ct.services.tm.txn.status.check.url.timeout"),
  		EMI_INTEREST_URL(120000, "ct.services.payment.api.emi-interests.url.timeout"),
  		CLICK_TO_PAY_URL(120000, "ct.book.click2pay.url.timeout"),
  		SHARE_TRIP_DETAILS_URL(120000, "ct.services.tm.share.trip.details.url.timeout"),
  		USER_API_URL(120000, "ct.services.user.old-api.profile.url.timeout"),
  		TRAVELLER_API_URL(120000, "ct.services.traveller.old-api.profile.url.timeout"),
  		TM_TICKET_EMAIL_URL(120000, "ct.services.tm.ticketemail.url.timeout"),
  		TM_TRIP_EMAIL_URL(120000, "ct.services.tm.tripemail.url.timeout"),
  		TM_VOUCHER_EMAIL_URL(120000, "ct.services.tm.voucher.email.url.timeout"),
  		TM_INVOICE_EMAIL_URL(120000, "ct.services.tm.invoiceemail.url.timeout"),
  		MSG_COUNT_URL(120000, "ct.agency.messages.url.new.timeout"),
  		DASHBOARD_URL(120000, "ct.agency.dashboard.info.url.timeout"),
  		POLICY_VIOLATION_URL(120000, "ct.corp.policy.violationReasons.url.timeout"),
  		TUXEDO_SEARCH_URL(120000, "ct.services.air.tuxedo.search.url.timeout"),
  		MULTICITY_SEARCH_URL(120000, "ct.services.multicity.search.url.timeout"),
  		CANCEL_PAGE_URL(120000, "ct.corp.air.cancelpageUIFrag.url.timeout"),
  		SEAT_RESERVATION_URL(120000, "ct.services.air.ui.seatreservation.url.timeout"),
  		SEAT_SELECTION_URL(120000, "ct.services.air.ui.seatselection.url.timeout"),
  		TRIP_DETAILS_PAGE_URL(120000, "ct.corp.air.tripdetailsUIFrag.url.timeout"),
  		SERVER_CONFIG_URL(120000, "ct.services.all.air.api.server.config.json.timeout"),
  		RAILS_PROPERTY_REFRESH_URL(120000, "ct.services.rails.property.update.cache.url.timeout"),
  		BAGGAGE_INFO_URL(120000, "ct.services.new.air.api.baggage.info.url.timeout"),
  		TRIP_INFO_URL(120000, "ct.services.new.air.api.trip.info.url.timeout"),
  		CHRONICLE_RETRIEVE_TRIP_URL(120000, "ct.services.tm.api.logging.url.timeout"),
  		CALENDAR_JSON_URL(120000, "ct.services.calendar.json.url.timeout"),
  		GEOLOCATION_JSON_URL(120000, "ct.services.places.ipligence.json.url.timeout"),
  		MOBILE_INTL_SEARCH(120000, "ct.services.mobile.intl.air.api.search.url.timeout"),
  		MOBILE_DOM_SEARCH(120000, "ct.services.mobile.air.api.search.url.timeout"),
  		DYNAMIC_LOADER_SEARCH_URL(120000, "ct.services.dynamic.loader.search.url.timeout"),
  		TRIP_URL(120000, "ct.tripurl.timeout"),
  		API_GATEWAY_ENDPOINT(120000, "ct.api.gateway.emdpoint.timeout"),
  		API_GATEWAY_AIR_ENDPOINT(30000, "ct.api.gateway.air.endpoint.get.timeout"),
  		API_GATEWAY_HOTEL_ENDPOINT(30000, "ct.api.gateway.hotel.endpoint.get.timeout"),
  		API_GATEWAY_AIR_RETRIEVE_TRIPS(30000, "ct.api.gateway.air.retrieve.trips.get.timeout"),
  		RAHAGEER_RETRIEVE_LOCKS(120000,"ct.bookapp.rahageer.retrieve.locks.timeout"),
  		RETRIEVE_WALLET_INFO_URL(30000,"ct.common.srp.effective.price.payment.timeout"),
    	USER_CARD_DETAIL_URL(30000,"ct.common.book.user.card.detail.timeout");

  		private GETTimeouts(Integer timeout, String timeoutProperty) {
  			this.timeout = timeout;
  			this.timeoutProperty = timeoutProperty;
  		}

  		public String getTimeoutProperty() {
			return timeoutProperty;
		}

		private Integer timeout;
  		private String timeoutProperty;
  		public Integer getTimeout() {
  			return timeout;
  		}
  	}

    public enum FlexiPayType {
        CANCELLATION_CHARGES, TOKEN_AMOUNT, RAHAGEER_FLOW;
        public String getName() {
            return super.toString().toLowerCase();
        }

        public String getDisplayValue() {
            String temp = super.toString().toLowerCase();
            temp = temp.replace('_', ' ');
            return temp.substring(0, 1).toUpperCase() + temp.substring(1);
        }

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    public enum FlexiPayLockType {
        INVENTORY, DERIVATIVE;
        public String getName() {
            return super.toString().toLowerCase();
        }

        public String getDisplayValue() {
            String temp = super.toString().toLowerCase();
            return temp.substring(0, 1).toUpperCase() + temp.substring(1);
        }

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    public enum GVSupportedProduct {
        AIR, HOTEL, LOCAL
    }

    public enum TaxType {

    	VAT("VAT");

    	String type;

    	private TaxType(String type) {
    		this.type = type;
    	}

    	public String getType() {
    		return type;
    	}

    	private static final TaxType[] taxTypes = TaxType.values();

    	public static TaxType forCode(String code) {
    		TaxType taxType = null;
            for (int i = 0; i < taxTypes.length; i++) {
                if (taxTypes[i].type.equalsIgnoreCase(code)) {
                	taxType = taxTypes[i];
                    break;
                }
            }

            return taxType;
        }

    }

}
