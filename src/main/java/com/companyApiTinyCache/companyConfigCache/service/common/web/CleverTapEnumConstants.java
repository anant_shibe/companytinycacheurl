package com.companyApiTinyCache.companyConfigCache.service.common.web;

/**
 * @author parvatkumar
 *
 */
public interface CleverTapEnumConstants {
	
	String CLEVER_TAP_EVENT_TIMEOUT = "ct.common.clevertap.pushevent.timeout";
	String END_POINT = "ct.clevertap.endpoint.url";
	String END_POINT_PUSH_NOTIFICATION="ct.clevertap.endpoint.push.notification.url";
	String IS_CLEVERTAP_ENABLED = "ct.clevertap.enabled";
	String NOTIFICATION_TYPE_KEY = "ct.air.clevertap.notification.types";
	String DELIMETER_FOR_NOTIFICATION_TYPE = ",";

	String DATE_FORMAT = "yyyy-MM-dd";
	String DEPART_DATE = "onwardDepDate";
	String RETURN_DATE = "retDepDate";
	// CREDENTIAL KEYS
	String ACCOUNT_ID = "X-CleverTap-Account-Id";
	String PASS_CODE = "X-CleverTap-Passcode";
	String CONTENT_TYPE = "Content-Type";
	String EXPLICIT_ALERT="explixit";
  
	
	//different status
	String STATUS_BOOK="Booked";

	
	// EventDetails Payload keys
	String IDIENTITY = "identity";
	String TIMESTAMP = "ts";
	String EVENT_TYPE = "type";
	String EVENT_NAME = "evtName";
	// MEDIA TYPES
	String MEDIATYPE_JSON = "application/json";
	// EVENT DATA DETAILS
	String EVENT_DATA = "evtData";
	String NOT_APPLICABLE="NA";
	//Attributes For Air_Msgs
	String MSG_TYPE = "msg_type";
	String MSG_CAMPAIGN = "msg_campaign";
	String MSG_VALUE = "msg_value";
	String PRODUCT = "product";
	String MSG_SENT_TO = "msg_sent_to";
	String MSG_SENT_DATE = "msg_sent_date";
	String MSG_SENT_TIME = "msg_sent_time";
	String USER_EMAIL = "user_email";
	String DEVICE_ID = "device_id";
	String ORIGIN = "origin";
	String DESTINATION = "destination";
	String JOURNEY_TYPE = "journey_type";
	String DEPARTURE_DATE = "departure_date";
	String EVT_RETURN_DATE = "return_date";
	String PAX_COUNT = "pax_count";
	String MSG_VALUE2 = "msg_value2";
	String DOMAIN = "domain";
	String SECTORS="sector";
	
	//Attributes for Air_Alerts
	String ALERT_TYPE="alert_type";
	String CATEGORY="category";
	String STATUS_AIR_ALERTS="status";
	String DX="dx";
	String UNSUBSCRIBE_REASON="unsubscribe_reason";
	String ALERT_TAG="alert_tag";
	String CREATED_FROM="created_from";
	String JOURNEY_TYPE_AIR_ALERTS="journey_type";
	String SEARCH_COUNT="search_count";
	String SOURCE="source";
	
	//CleverTap Mail Validation Fields
	String EMPTY_PAYLOAD = "emptyPayload";
	String NOTIFICATION_TYPE = "notificationType";
	String EMP_NOTIFICATION_TYPE = "emptyNotificationType";
	String INV_NOTIFICATION_TYPE = "invNotificationType";
	String TO = "to";
	String MANDATORY_FIELD_TO = "mandatoryFieldTo";
	String TAG_GROUP = "tag_group";
	String MANDATORY_TAG_GROUP = "mandatoryTagGroup";
	
	//Clevertap Identity Fields
    String FBID = "FBID";
    String GPID = "GPID";
    String EMAIL = "Email";
    String IDENTITY = "Identity";
    String OBJECT_ID = "objectid";
    String EMAIL_SUBJECT = "subject";
    String EMAIL_BODY = "body";
    String RESPECT_FREQUENCY_CAPS = "respect_frequency_caps";
    String SENDER_NAME = "sender_name";
    String CONTENT = "content";

	// CLEVER TRAP RESPONSE DETAILS
	String STATUS = "status";
	String PROCESSED = "processed";
	String UNPROCESSED = "unprocessed";
	String CODE = "code";
 
}
