package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;

/**
 * Receiver Spring Bean class instantiated on server to receive a message on Active MQ with the content in memcached/redis.
 *
 * @author suresh
 */
public class MemcachedJmsMessageReceiver implements DisposableBean {

    private static final Log logger = LogFactory.getLog(MemcachedJmsMessageReceiver.class);

    public static final String LOGGING_MEMCACHED_SERVER_PROPERTY_NAME = "ct.services.logging.memcached.servers";

    private Cache cache;

    private String maxWaitProperty;

    private CachedProperties commonCachedProperties;

    private MessageProcessor messageProcessor;

    private DefaultMessageListenerContainer messageListenerContainer;

    private MessageSerializer serializer;

    public MemcachedJmsMessageReceiver(CacheFactory pcacheFactory, ConnectionFactory pconnectionFactory, Destination pdestination, CachedProperties pcommonCachedProperties, String pmaxWaitProperty,
            MessageProcessor pmessageProcessor) {
        cache = pcacheFactory.getCacheForServer(LOGGING_MEMCACHED_SERVER_PROPERTY_NAME);
        commonCachedProperties = pcommonCachedProperties;
        maxWaitProperty = pmaxWaitProperty;
        if (maxWaitProperty == null) {
            maxWaitProperty = "ct.common.memcachedq.client.maxwaitseconds";
        }
        // Derive initial wait seconds from maxWaitProperty. This is presently an unlisted Property
        int pos = maxWaitProperty.lastIndexOf('.') + 1;
        if (pos <= 0) {
            pos = 0;
        }
        messageProcessor = pmessageProcessor;
        messageListenerContainer = new DefaultMessageListenerContainer();
        messageListenerContainer.setConnectionFactory(pconnectionFactory);
        messageListenerContainer.setDestination(pdestination);
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter();
        messageListenerAdapter.setDelegate(this);
        messageListenerAdapter.setDefaultListenerMethod("receive");
        messageListenerContainer.setMessageListener(messageListenerAdapter);
        messageListenerContainer.afterPropertiesSet();
    }

    public void receive(Object messageId) {
        String instanceId = (String) messageId;
        Object message = cache.get(instanceId);
        if (message != null) {
            try {
                if (serializer != null) {
                    message = serializer.deSerialize(message);
                }
                messageProcessor.process(message);
            } catch (Exception e) {
                logger.error("Error Processing Message Id : " + messageId, e);
            }
        } else {
            logger.error("Failed to get message for messageId " + instanceId);
        }
    }

    @Override
    public void destroy() throws Exception {
        messageListenerContainer.destroy();
    }

    /**
     * Setter for serializer.
     *
     * @param pserializer
     *            the serializer to set
     */
    public void setSerializer(MessageSerializer pserializer) {
        serializer = pserializer;
    }
}
