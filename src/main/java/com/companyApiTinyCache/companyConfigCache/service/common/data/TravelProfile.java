package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class TravelProfile implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private String passportNo;

    private Date passportDateOfIssue;

    private Date passportDateOfExpiry;

    @JsonIgnore
    private String passportDateOfIssueStr;

    @JsonIgnore
    private String passportDateOfExpiryStr;

    private String passportIssuingCountry;

    private int passportIssuingCountryId;

    private String nationality;

    private String mealPreference;

    private String seatPreference;

    private String specialRequests;

    private String homeAirport;

    private List<FrequentFlyerNumber> frequentFlyerNumbers;

    @JsonIgnore
    private boolean profiledUpdated;

    private String emailAlertsEnabled;

    private String smsAlertsEnabled;

    @JsonIgnore
    public boolean isProfiledUpdated() {
        return profiledUpdated;
    }

    @JsonProperty("passport_no")
    public String getPassportNo() {
        return passportNo;
    }

    @JsonProperty("passport_no")
    public void setPassportNo(String passportNo) {
        if (StringUtils.isNotEmpty(passportNo))
            profiledUpdated = true;
        this.passportNo = passportNo;
    }

    @JsonProperty("passport_date_of_issue")
    public Date getPassportDateOfIssue() {
        return passportDateOfIssue;
    }

    @JsonProperty("passport_date_of_issue")
    public void setPassportDateOfIssue(Date passportDateOfIssue) {
        if (passportDateOfIssue != null)
            profiledUpdated = true;
        this.passportDateOfIssue = passportDateOfIssue;
    }

    @JsonProperty("passport_date_of_expiry")
    public Date getPassportDateOfExpiry() {
        return passportDateOfExpiry;
    }

    @JsonProperty("passport_date_of_expiry")
    public void setPassportDateOfExpiry(Date passportDateOfExpiry) {
        if (passportDateOfExpiry != null)
            profiledUpdated = true;
        this.passportDateOfExpiry = passportDateOfExpiry;
    }

    @JsonProperty("passport_issuing_country")
    public String getPassportIssuingCountry() {
        return passportIssuingCountry;
    }

    @JsonProperty("passport_issuing_country")
    public void setPassportIssuingCountry(String passportIssuingCountry) {
        if (StringUtils.isNotEmpty(passportIssuingCountry))
            profiledUpdated = true;
        this.passportIssuingCountry = passportIssuingCountry;
    }

    @JsonProperty("passport_issuing_country_id")
    public int getPassportIssuingCountryId() {
        return passportIssuingCountryId;
    }

    @JsonProperty("passport_issuing_country_id")
    public void setPassportIssuingCountryId(int passportIssuingCountryId) {
        if (passportIssuingCountryId > 0)
            profiledUpdated = true;
        this.passportIssuingCountryId = passportIssuingCountryId;
    }

    @JsonProperty("nationality")
    public String getNationality() {
        return nationality;
    }

    @JsonProperty("nationality")
    public void setNationality(String nationality) {
        if (StringUtils.isNotEmpty(nationality))
            profiledUpdated = true;
        this.nationality = nationality;
    }

    @JsonProperty("meal_preference")
    public String getMealPreference() {
        return mealPreference;
    }

    @JsonProperty("meal_preference")
    public void setMealPreference(String mealPreference) {
        if (StringUtils.isNotEmpty(mealPreference))
            profiledUpdated = true;
        this.mealPreference = mealPreference;
    }

    @JsonProperty("seat_preference")
    public String getSeatPreference() {
        return seatPreference;
    }

    @JsonProperty("seat_preference")
    public void setSeatPreference(String seatPreference) {
        if (StringUtils.isNotEmpty(seatPreference))
            profiledUpdated = true;
        this.seatPreference = seatPreference;
    }

    @JsonProperty("special_request")
    public String getSpecialRequests() {
        return specialRequests;
    }

    @JsonProperty("special_request")
    public void setSpecialRequests(String specialRequests) {
        if (StringUtils.isNotEmpty(specialRequests))
            profiledUpdated = true;
        this.specialRequests = specialRequests;
    }

    @JsonProperty("frequent_flyer_numbers")
    public List<FrequentFlyerNumber> getFrequentFlyerNumbers() {
        return frequentFlyerNumbers;
    }

    @JsonProperty("frequent_flyer_numbers")
    public void setFrequentFlyerNumbers(List<FrequentFlyerNumber> frequentFlyerNumber) {
        if (frequentFlyerNumber != null && frequentFlyerNumber.size() > 0)
            profiledUpdated = true;
        this.frequentFlyerNumbers = frequentFlyerNumber;
    }

    @JsonIgnore
    public String getPassportDateOfIssueStr() {
        if (getPassportDateOfIssue() != null) {
            String dateStr = GenUtil.createParsedDate(getPassportDateOfIssue(), "dd-MMM-yyyy");
            return dateStr;
        } else {
            return "";
        }
    }

    @JsonIgnore
    public String getPassportDateOfExpiryStr() {
        if (getPassportDateOfExpiry() != null) {
            String dateStr = GenUtil.createParsedDate(getPassportDateOfExpiry(), "dd-MMM-yyyy");
            return dateStr;
        } else {
            return "";
        }
    }

    @JsonProperty("home_airport")
    public String getHomeAirport() {
        return homeAirport;
    }

    @JsonProperty("home_airport")
    public void setHomeAirport(String homeAirport) {
        this.homeAirport = homeAirport;
    }

    @JsonProperty("get_email_alerts")
    public String getEmailAlertsEnabled() {
        return emailAlertsEnabled;
    }

    @JsonProperty("get_email_alerts")
    public void setEmailAlertsEnabled(String emailAlertsEnabled) {
        this.emailAlertsEnabled = emailAlertsEnabled;
    }

    @JsonProperty("get_sms_alerts")
    public String getSmsAlertsEnabled() {
        return smsAlertsEnabled;
    }

    @JsonProperty("get_sms_alerts")
    public void setSmsAlertsEnabled(String smsAlertsEnabled) {
        this.smsAlertsEnabled = smsAlertsEnabled;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TravelProfile == false)
            return false;
        TravelProfile tp = (TravelProfile) o;
        if ((passportNo != null || tp.getPassportNo() != null) && !GenUtil.equals(passportNo, tp.getPassportNo())) {
            return false;
        }
        if ((passportDateOfIssue != null || tp.getPassportDateOfIssue() != null) && !GenUtil.equals(passportDateOfIssue, tp.getPassportDateOfIssue())) {
            return false;
        }
        if ((passportDateOfExpiry != null || tp.getPassportDateOfExpiry() != null) && !GenUtil.equals(passportDateOfExpiry, tp.getPassportDateOfExpiry())) {
            return false;
        }
        if ((passportIssuingCountry != null || tp.getPassportIssuingCountry() != null) && !GenUtil.equals(passportIssuingCountry, tp.getPassportIssuingCountry())) {
            return false;
        }
        if (passportIssuingCountryId != tp.getPassportIssuingCountryId()) {
            return false;
        }
        if ((nationality != null || tp.getNationality() != null) && !GenUtil.equals(nationality, tp.getNationality())) {
            return false;
        }
        if ((mealPreference != null || tp.getMealPreference() != null) && !GenUtil.equals(mealPreference, tp.getMealPreference())) {
            return false;
        }
        if ((seatPreference != null || tp.getSeatPreference() != null) && !GenUtil.equals(seatPreference, tp.getSeatPreference())) {
            return false;
        }
        if ((specialRequests != null || tp.getSpecialRequests() != null) && !GenUtil.equals(specialRequests, tp.getSpecialRequests())) {
            return false;
        }
        if ((homeAirport != null || tp.getHomeAirport() != null) && !GenUtil.equals(homeAirport, tp.getHomeAirport())) {
            return false;
        }
        if ((frequentFlyerNumbers != null || tp.getFrequentFlyerNumbers() != null) && !GenUtil.sameCollections(frequentFlyerNumbers, tp.getFrequentFlyerNumbers(), true)) {
            return false;
        }

        return true;
    }

}
