package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides methods for parsing and evaluating infix expressions. This is a good choice when rules need to be read and evaluated real time rather than parsed and loaded into jvm as a
 * CachedResource. The rule format is also more compact and human readable and the evaluation is much faster than parsing the json format and then evaluating it.
 * 
 * This class also provides a parse method for parsing an infix expression and generating an Expression tree for later use. Note that the evaluate method does not internally generate an expression
 * tree. So calling evaluate is faster than calling parse and the calling evaluate on the resulting expression. However once a parsed Expression is available evaluating it is much faster than
 * evaluating the infix expression.
 * 
 * Instead of the usual "Shunting-yard" algorithm using an operator and operand stack it uses a recursive method to do the same. This is slightly faster than the former possibly due to the System
 * stack being faster than an array. Unnecessary sections are ignored when evaluating boolean operators.
 * 
 * It uses RuleEvaluator to perform the individual operations.
 * 
 * See TestRules test case for examples.
 * 
 * @author suresh
 * 
 */
public class InfixExpression {

    private RuleEvaluator ruleEvaluator;

    private static final Log logger = LogFactory.getLog(InfixExpression.class);

    /**
     * Encapsulates an operator with details like priority, whether unary along with operator name
     * 
     * @author suresh
     * 
     */
    public static class Operator {
        String operator;
        int priority;
        boolean isUnary;
        boolean isSetOperator;

        public Operator(String poperator, int ppriority, boolean pisUnary, boolean pisSetOperator) {
            operator = poperator;
            priority = ppriority;
            isUnary = pisUnary;
            isSetOperator = pisSetOperator;
        }

        public Operator(String poperator, int ppriority) {
            this(poperator, ppriority, false, false);
        }

        @Override
        public boolean equals(Object obj) {
            boolean isEqual = false;
            if (obj == this) {
                isEqual = true;
            } else if (obj != null && obj instanceof Operator) {
                String otherOp = ((Operator) obj).operator;
                if (operator == otherOp || otherOp != null && otherOp.equals(operator)) {
                    isEqual = true;
                }
            }

            return isEqual;
        }

        @Override
        public int hashCode() {
            int code = -1;
            if (operator != null) {
                code = operator.hashCode();
            }

            return code;
        }
    }

    private static final String[] OPERATOR_NAMES;

    // Map used to get an Operator object from the operator name
    private static final Map<String, Operator> OPERATOR_MAP;

    private static final int ELEMENT_TYPE_OPEN_PARAN = 0;

    private static final int ELEMENT_TYPE_CLOSE_PARAN = 1;

    private static final int ELEMENT_TYPE_OPERATOR = 2;

    private static final int ELEMENT_TYPE_OPERAND = 3;

    private static final int TOKEN_TYPE_COMMA = 4;

    private static final int TOKEN_TYPE_STRING = 5;

    private static final int TOKEN_TYPE_NUMBER = 6;

    private static final int TOKEN_TYPE_DOUBLE = 7;

    private static final Operator OPEN_PARAN_OPERATOR = new Operator("(", -1);

    private static final Operator CLOSE_PARAN_OPERATOR = new Operator(")", 100);

    private static final Operator MINUS_OPERATOR;

    private static final Operator AND_OPERATOR;

    private static final Operator OR_OPERATOR;

    private static final int RESULT_TYPE_VALUE = 0;

    private static final int RESULT_TYPE_EXPR = 1;

    private static final int RESULT_TYPE_NONE = 2;

    static {
        OPERATOR_NAMES = new String[RuleEvaluator.LOG_OPERATORS.size() + RuleEvaluator.REL_OPERATORS.size() + RuleEvaluator.ALG_OPERATORS.size()];
        Map<String, Operator> opMap = new HashMap<String, Operator>();
        int i = 0;
        Operator op;
        Operator andOp = null;
        Operator orOp = null;
        for (String opName : RuleEvaluator.LOG_OPERATORS) {
            int priority = 1;
            boolean isUnary = false;
            if ("AND".equals(opName)) {
                priority = 2;
            } else if ("NOT".equals(opName)) {
                isUnary = true;
                priority = 3;
            }
            OPERATOR_NAMES[i] = opName;
            op = new Operator(OPERATOR_NAMES[i], priority, isUnary, false);
            opMap.put(opName, op);
            if ("AND".equals(opName)) {
                andOp = op;
            } else if ("OR".equals(opName)) {
                orOp = op;
            }
            i++;
        }
        AND_OPERATOR = andOp;
        OR_OPERATOR = orOp;
        for (String opName : RuleEvaluator.REL_OPERATORS) {
            int priority = 4;
            boolean isSet = false;
            if ("IN".equals(opName) || "NOT IN".equals(opName) || "SUBSET OF".equals(opName)) {
                isSet = true;
            }
            OPERATOR_NAMES[i] = opName.replace(' ', '_');
            op = new Operator(opName, priority, false, isSet);
            opMap.put(OPERATOR_NAMES[i], op);
            i++;
        }
        Operator minusOp = null;
        for (String opName : RuleEvaluator.ALG_OPERATORS) {
            int priority = 6;
            if ("+".equals(opName) || "-".equals(opName)) {
                priority = 5;
            } else if ("POW".equals(opName)) {
                priority = 7;
            } else if ("MIN".equals(opName) || "MAX".equals(opName) || "AVG".equals(opName)) {
                priority = 8;
            }
            OPERATOR_NAMES[i] = opName;
            op = new Operator(opName, priority);
            opMap.put(OPERATOR_NAMES[i], op);
            if ("-".equals(opName)) {
                minusOp = op;
            }
            i++;
        }
        MINUS_OPERATOR = minusOp;
        OPERATOR_MAP = opMap;
    }

    private StringBuilder sb;
    // Infix expression to parse and its length
    private String infix;
    private int infixLen;

    // Token Info
    private Object element;
    private int pos;
    private int errorPos;
    private int elementType;
    private int radix; // 10, 16 or 2 For numbers

    private Map<String, Object> values;

    // Below are temp fields used by recursive call
    private String message;

    private Expression exp;

    public InfixExpression(StringBuilder tmpSb) {
        sb = tmpSb;
        ruleEvaluator = RuleEvaluator.getInstance();
    }

    private ParseException createParseException(String message, Exception cause) {
        int i;
        int errPos = errorPos;
        if (errPos <= 0) {
            errPos = pos;
        }
        if (sb == null) {
            sb = new StringBuilder();
        } else {
            sb.setLength(0);
        }
        sb.append(message).append(", position: ").append(errPos).append('\n');
        sb.append(infix).append('\n');
        for (i = 0; i < errPos; i++) {
            char c = infix.charAt(i);
            if (!Character.isWhitespace(c)) {
                c = ' ';
            }
            sb.append(c);
        }
        sb.append('^');
        ParseException parseException = new ParseException(sb.toString(), errPos);
        if (cause != null) {
            parseException.initCause(cause);
        }
        return parseException;
    }

    /**
     * Returns next token in element and puts type in elementType which is one of the ELEMENT_TYPE or TOKEN_TYPE constants. element will be null on end of input.
     * 
     * @return Error message or null if successful
     */
    private String nextToken() {
        int orgC, c = -1;
        String message = null;
        while (pos < infixLen && ((c = infix.charAt(pos)) == ' ' || c == '\t')) {
            pos++;
        }
        if (pos >= infixLen) {
            element = null;
        } else {
            int start = pos;
            orgC = c;
            if (c == '$') {
                while (++pos < infixLen && ((c = infix.charAt(pos)) >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_'))
                    ;
                if (pos - start < 2) {
                    message = "Unexpected char when looking for identifier";
                }
                element = infix.substring(start, pos);
                elementType = ELEMENT_TYPE_OPERAND;
            } else if (c >= '0' && c <= '9' || c == '-') {
                int newElementType = TOKEN_TYPE_NUMBER;
                radix = 10;
                int endChar = '9';
                if (c == '0' && pos < infixLen - 1 && (c = infix.charAt(pos + 1)) == 'x' || c == 'b') {
                    pos++;
                    if (c == 'b') {
                        radix = 2;
                        endChar = '1';
                    } else {
                        radix = 16;
                    }
                    if (pos + 1 >= infixLen) {
                        pos++;
                        message = "Digit Expected";
                    }
                }
                if (message == null) {
                    while (++pos < infixLen) {
                        c = infix.charAt(pos);
                        if (c >= '0' && c <= endChar) {
                            continue;
                        }
                        if (radix == 10) {
                            if (c == '.' && newElementType == TOKEN_TYPE_NUMBER) {
                                newElementType = TOKEN_TYPE_DOUBLE;
                                continue;
                            }
                        } else if (radix == 16 && (c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F')) {
                            continue;
                        }
                        if (c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_' || c == '.') {
                            if (radix == 10) {
                                message = "Invalid Decimal Number";
                            } else if (radix == 2) {
                                message = "Invalid Binary Number";
                            } else {
                                message = "Invalid Hex Number";
                            }
                        }
                        break;
                    }
                }
                if (message == null) {
                    if (orgC == '-' && pos - start < 2) {
                        element = MINUS_OPERATOR;
                        elementType = ELEMENT_TYPE_OPERATOR;
                    } else {
                        element = infix.substring(start, pos);
                        elementType = newElementType;
                    }
                }
            } else if (c == '"') {
                StringBuilder sb1 = null;
                start++;
                while (++pos < infixLen) {
                    c = infix.charAt(pos);
                    if (c == '"') {
                        break;
                    }
                    if (c == '\\') {
                        if (sb1 == null) {
                            if (sb != null) {
                                sb.setLength(0);
                            } else {
                                sb = new StringBuilder();
                            }
                            sb1 = sb;
                            sb1.append(infix, start, pos);
                        }
                        pos++;
                        if (pos < infixLen) {
                            c = infix.charAt(pos);
                            switch (c) {
                            case 'n':
                                c = '\n';
                                break;
                            case 't':
                                c = '\t';
                                break;
                            case '\f':
                                c = '\f';
                                break;
                            }
                            sb.append(c);
                        }
                    } else if (sb1 != null) {
                        sb1.append(c);
                    }
                }
                if (c != '"') {
                    message = "Unterminated String";
                } else {
                    if (sb1 != null) {
                        element = sb1.toString();
                    } else {
                        element = infix.substring(start, pos);
                    }
                    pos++;
                    elementType = TOKEN_TYPE_STRING;
                }
            } else if (c == '(') {
                pos++;
                element = "(";
                elementType = ELEMENT_TYPE_OPEN_PARAN;
            } else if (c == ')') {
                pos++;
                element = ")";
                elementType = ELEMENT_TYPE_CLOSE_PARAN;
            } else if (c == ',') {
                pos++;
                element = ",";
                elementType = TOKEN_TYPE_COMMA;
            } else {
                // Try Operator
                if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z') {
                    while (++pos < infixLen && (((c = infix.charAt(pos)) >= 'A') && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_'))
                        ;
                } else {
                    if (++pos < infixLen) {
                        c = infix.charAt(pos);
                        if (!(c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_' || c == ' ' || c == '\t')) {
                            pos++;
                        }
                    }
                }
                String s = infix.substring(start, pos);
                Operator op = OPERATOR_MAP.get(s);
                if (op != null) {
                    elementType = ELEMENT_TYPE_OPERATOR;
                    element = op;
                } else {
                    message = "Invalid Operator";
                }
            }
        }

        return message;
    }

    /**
     * Reads the next set of integers or string between parantheses and puts them in element.
     * 
     * @return error message or null on success
     */
    private String nextSet() {
        String message = nextToken();
        boolean isComma = false;
        int setType = -1;
        List list = new ArrayList();
        if (message == null && elementType == ELEMENT_TYPE_OPEN_PARAN) {
            while ((message = nextToken()) == null) {
                if (element == null) {
                    message = "Unexpected End of Set";
                    break;
                }
                if (elementType == TOKEN_TYPE_COMMA) {
                    if (!isComma) {
                        message = "Unexpected comma";
                        break;
                    }
                } else if (elementType == TOKEN_TYPE_NUMBER) {
                    if (setType == 1) {
                        message = "String Type expected in set";
                        break;
                    }
                    list.add(element);
                    setType = 0;
                } else if (elementType == TOKEN_TYPE_STRING) {
                    if (setType == 0) {
                        message = "Number Type expected in set";
                        break;
                    }
                    list.add(element);
                } else if (elementType == ELEMENT_TYPE_CLOSE_PARAN) {
                    break;
                } else {
                    message = "Unexpected Type expected in set";
                }
                isComma = !isComma;
            }
        } else if (message == null) {
            message = "( expected at beginning of set";
        }
        if (message == null) {
            element = list;
            elementType = ELEMENT_TYPE_OPERAND;
        }

        return message;
    }

    /**
     * Returns the next element in element field. Uses nextToken or nextSet. elementType is filled with the appropriate ELEMENT_TYPE.. constant.
     * 
     * @return Error message or null on success
     */
    private String nextElement() {
        String message = null;
        if (elementType == ELEMENT_TYPE_OPERATOR && ((Operator) element).isSetOperator) {
            message = nextSet();
        } else {
            message = nextToken();
            if (message == null) {
                if (elementType == TOKEN_TYPE_NUMBER) {
                    // For now passing of numbers as String
                    elementType = ELEMENT_TYPE_OPERAND;
                } else if (elementType == TOKEN_TYPE_DOUBLE) {
                    elementType = ELEMENT_TYPE_OPERAND;
                    element = Double.parseDouble((String) element);
                } else if (elementType == TOKEN_TYPE_STRING) {
                    elementType = ELEMENT_TYPE_OPERAND;
                } else if (elementType == TOKEN_TYPE_COMMA) {
                    message = "Unexpected Comma";
                }
            }
        }

        return message;
    }

    // Resets object for evaluating next expression
    private void reset() {
        element = null;
        pos = 0;
        errorPos = 0;
        elementType = 0;
    }

    /**
     * This is the core method of this class used by both evaluate and parse methods. It is a recursive method which evaluates the expression from the current position till an operator with precedence
     * less than or equal to the passed stackOp is encountered or an unbalanced close parenthesis ) or the end of input is encountered. The unprocessed operator or ) is left in element field to be
     * processed by the caller which is usually the same method unless there is an error.
     * 
     * It is initially called with the OPEN_PARAN_OPERATOR which has the least precedence of -1.
     * 
     * @param stackOp
     *            The yet to be evaluated operator
     * @param resultType
     *            Result Type desired. RESULT_TYPE_EXPR is used to build expression. RESULT_TYPE_VALUE is used to evaluate. RESULT_TYPE_NONE is used internally in this method when evaluating to skip
     *            sections not required to be evaluated when evaluating boolean operators.
     * @return Required result.
     * @throws ParseException
     *             When a parse error is encountered.
     */
    private Object evaluateTill(Operator stackOp, int resultType) throws ParseException {
        Object lhs = null;
        Object rhs = null;
        message = nextElement();
        if (message != null) {
            throw createParseException(message, null);
        }
        Operator incomingOp = null;
        Object nextEl = element; // This is used as the look ahead element
        int svPos;
        if (nextEl == null) {
            throw createParseException("Operand ( or unary operator expected", null);
        }
        switch (elementType) {
        case ELEMENT_TYPE_OPEN_PARAN:
            // Recursively evaluate till ). This will be lhs
            lhs = evaluateTill(OPEN_PARAN_OPERATOR, resultType);
            // Read, validate and ignore )
            if (elementType != ELEMENT_TYPE_CLOSE_PARAN) {
                throw createParseException(") expected", null);
            }
            nextEl = null;
            break;
        case ELEMENT_TYPE_OPERAND:
            lhs = nextEl;
            nextEl = null;
            break;
        case ELEMENT_TYPE_OPERATOR:
            // Validate and evalute unary op as lhs
            incomingOp = (Operator) nextEl;
            svPos = pos;
            if (!incomingOp.isUnary) {
                throw createParseException("Operand or unary operator expected", null);
            }
            lhs = evaluateTill(incomingOp, resultType);
            if (resultType == RESULT_TYPE_EXPR) {
                exp = new Expression();
                exp.setLhs(lhs);
                exp.setOp(incomingOp.operator);
                exp.setPos(pos);
                lhs = exp;
            } else if (resultType == RESULT_TYPE_VALUE) {
                errorPos = svPos;
                lhs = ruleEvaluator.evaluateExpression(values, incomingOp.operator, lhs, null);
                errorPos = 0;
            }
            nextEl = element;
            break;
        default:
            throw createParseException("Operand or Unary operator expected", null);
        }
        // Examine operator or close bracket
        if (nextEl == null) {
            message = nextElement();
            if (message != null) {
                throw createParseException(message, null);
            }
            nextEl = element;
        }
        if (nextEl != null && elementType != ELEMENT_TYPE_OPERATOR && elementType != ELEMENT_TYPE_CLOSE_PARAN) {
            throw createParseException("Operator expected", null);
        }
        // Recursively evaluate rhs and store into lhs as long as incomingOp is of higher precedence
        // than passed op
        while (nextEl != null && elementType == ELEMENT_TYPE_OPERATOR && (incomingOp = (Operator) element).priority > stackOp.priority) {
            svPos = pos;
            if (resultType == RESULT_TYPE_EXPR) {
                rhs = evaluateTill(incomingOp, resultType);
                exp = new Expression();
                exp.setLhs(lhs);
                exp.setOp(incomingOp.operator);
                exp.setPos(svPos);
                exp.setRhs(rhs);
                lhs = exp;
            } else {
                if (resultType == RESULT_TYPE_VALUE && lhs instanceof Boolean) {
                    boolean b = ((Boolean) lhs);
                    // == works instead of .equals because we ensure that the constants are
                    // initialized with the same Operator instance filled in the operator array
                    if (AND_OPERATOR == incomingOp && !b || OR_OPERATOR == incomingOp && b) {
                        resultType = RESULT_TYPE_NONE;
                    }
                }
                rhs = evaluateTill(incomingOp, resultType);
                if (resultType == RESULT_TYPE_VALUE) {
                    errorPos = svPos;
                    lhs = ruleEvaluator.evaluateExpression(values, incomingOp.operator, lhs, rhs);
                    errorPos = 0;
                }
            }
            nextEl = element;
        }

        return lhs;
    }

    /**
     * Evaluates an infix expression.
     * 
     * @param expr
     *            Expression to evaluate
     * @param pvalues
     *            value map to substitute $ variables with
     * @return Result of evaluation
     * @throws ParseException
     *             During parse error or Runtime Exception
     */
    public Object evaluate(String expr, Map<String, Object> pvalues) throws ParseException {
        Object result = null;
        try {
            reset();
            infix = expr;
            infixLen = expr.length();
            values = pvalues;
            result = evaluateTill(OPEN_PARAN_OPERATOR, RESULT_TYPE_VALUE);
            if (element != null) {
                throw createParseException("Unexpected " + element + " at end of input", null);
            }
        } catch (RuntimeException e) {
            throw createParseException("Unexpected Error", e);
        }

        return result;
    }

    /**
     * Parses an infix expression to produce expression tree. The tree has the pos field filled with position of node in infix expression useful for error reporting.
     * 
     * @param expr
     *            Expression to parse
     * @return Result of parsing
     * @throws ParseException
     *             During parse error or Runtime Exception
     */
    public Expression parse(String expr) throws ParseException {
        Expression result = null;
        try {
            reset();
            infix = expr;
            infixLen = expr.length();
            result = (Expression) evaluateTill(OPEN_PARAN_OPERATOR, RESULT_TYPE_EXPR);
            if (element != null) {
                throw createParseException("Unexpected " + element + " at end of input", null);
            }
        } catch (RuntimeException e) {
            throw createParseException("Unexpected Error", e);
        }

        return result;
    }

    /**
     * Evaluates a parsed expression. Similiar to Expression.parse, but prints the position of error in case of an error.
     * 
     * @param expr
     *            Expression to evaluate
     * @param pvalues
     *            value map to substitute $ variables with
     * @return Result of evaluating expression
     */
    public Object evaluate(Expression expr, Map<String, Object> pvalues) {
        Object result = null;
        boolean isSuccess = false;
        Expression[] lastEvalNode = new Expression[1];
        try {
            result = expr.evaluate(pvalues, lastEvalNode);
            isSuccess = true;
        } finally {
            if (!isSuccess) {
                // Give info on error position
                if (lastEvalNode[0] != null) {
                    logger.info("GOT Error while Evaluating expression for Operator: " + lastEvalNode[0].getOp() + " at position: " + lastEvalNode[0].getPos());
                }
            }
        }

        return result;
    }
}
