/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FlightSegment implements Serializable {
    private static final long serialVersionUID = 1L;
    private String airlineCode;
    private String airlineName;
    private String from;
    private String fromCityName;
    private String fromAirportName;
    private String to;
    private String toCityName;
    private String toAirportName;
    private String finalDest;
    private String flightNumber;
    private String equipment;
    private String departs;
    private String departDateStr;
    private Date departDate;
    private String arrives;
    private String arriveDateStr;
    private Date arriveDate;
    private int duration;
    private String durationStr;
    private int layover;
    private String layoverStr;
    private String fareBasisClass;
    private String fareBasis;
    private String fareKey;
    private String fareRule;
    private String cabinType;
    private String cabinTypeStr;
    private String ticketType;
    private String ticketTypeStr;
    private String via;

    private double adultBase;
    private double childBase;
    private double infantBase;

    private int stops;
    private boolean hasStops;
    private boolean isStopOverSegment;

    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * @param airlineCode
     *            the airlineCode to set
     */
    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    /**
     * @return the airlineName
     */
    public String getAirlineName() {
        return airlineName;
    }

    /**
     * @param airlineName
     *            the airlineName to set
     */
    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from
     *            the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to
     *            the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the flightNumber
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * @param flightNumber
     *            the flightNumber to set
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * @return the departs
     */
    public String getDeparts() {
        return departs;
    }

    /**
     * @param departs
     *            the departs to set
     */
    public void setDeparts(String departs) {
        this.departs = departs;
    }

    /**
     * @return the arrives
     */
    public String getArrives() {
        return arrives;
    }

    /**
     * @param arrives
     *            the arrives to set
     */
    public void setArrives(String arrives) {
        this.arrives = arrives;
    }

    /**
     * @return the fareBasisClass
     */
    public String getFareBasisClass() {
        return fareBasisClass;
    }

    /**
     * @param fareBasisClass
     *            the fareBasisClass to set
     */
    public void setFareBasisClass(String fareBasisClass) {
        this.fareBasisClass = fareBasisClass;
    }

    /**
     * @return the fareBasis
     */
    public String getFareBasis() {
        return fareBasis;
    }

    /**
     * @param fareBasis
     *            the fareBasis to set
     */
    public void setFareBasis(String fareBasis) {
        this.fareBasis = fareBasis;
    }

    /**
     * @return the adultBase
     */
    public double getAdultBase() {
        return adultBase;
    }

    /**
     * @param adultBase
     *            the adultBase to set
     */
    public void setAdultBase(double adultBase) {
        this.adultBase = adultBase;
    }

    /**
     * @return the via
     */
    public String getVia() {
        return via;
    }

    /**
     * @param via
     *            the via to set
     */
    public void setVia(String via) {
        this.via = via;
    }

    /**
     * @return the stops
     */
    public int getStops() {
        return stops;
    }

    /**
     * @param stops
     *            the stops to set
     */
    public void setStops(int stops) {
        this.stops = stops;
    }

    /**
     * @return the fromCityName
     */
    public String getFromCityName() {
        return fromCityName;
    }

    /**
     * @param fromCityName
     *            the fromCityName to set
     */
    public void setFromCityName(String fromCityName) {
        this.fromCityName = fromCityName;
    }

    /**
     * @return the toCityName
     */
    public String getToCityName() {
        return toCityName;
    }

    /**
     * @param toCityName
     *            the toCityName to set
     */
    public void setToCityName(String toCityName) {
        this.toCityName = toCityName;
    }

    /**
     * @return the childBase
     */
    public double getChildBase() {
        return childBase;
    }

    /**
     * @param childBase
     *            the childBase to set
     */
    public void setChildBase(double childBase) {
        this.childBase = childBase;
    }

    /**
     * @return the infantBase
     */
    public double getInfantBase() {
        return infantBase;
    }

    /**
     * @param infantBase
     *            the infantBase to set
     */
    public void setInfantBase(double infantBase) {
        this.infantBase = infantBase;
    }

    /**
     * @return the equipment
     */
    public String getEquipment() {
        return equipment;
    }

    /**
     * @param equipment
     *            the equipment to set
     */
    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    /**
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @param duration
     *            the duration to set
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * @return the durationStr
     */
    public String getDurationStr() {
        int hr = duration / 3600;
        int tempmins = duration % 3600;
        int mins = tempmins / 60;
        if (mins > 0)
            if (hr > 0)
                durationStr = String.format("%dh %dm", hr, mins);
            else
                durationStr = String.format("%dm", mins);
        else
            // duration cannot be 0hr and 0min
            durationStr = String.format("%dh", hr);
        return durationStr;
    }

    /**
     * @return the durationStr
     */
    public String getLayoverStr() {
        int hr = layover / 3600;
        int tempmins = layover % 3600;
        int mins = tempmins / 60;
        if (mins > 0)
            if (hr > 0)
                layoverStr = String.format("%dh %dm", hr, mins);
            else
                layoverStr = String.format("%dm", mins);
        else
            layoverStr = String.format("%dh", hr);
        return layoverStr;
    }

    /**
     * @return the cabinType
     */
    public String getCabinType() {
        return cabinType;
    }

    /**
     * @param cabinType
     *            the cabinType to set
     */
    public void setCabinType(String cabinType) {
        this.cabinType = cabinType;
    }

    /**
     * @return the cabinTypeStr
     */
    public String getCabinTypeStr() {
        if (cabinType.equals("E")) {
            return "Economy";
        } else if (cabinType.equals("B")) {
            return "Business";
        } else if (cabinType.equals("P")) {
            return "Premium Economy";
        } else if (cabinType.equals("F")) {
            return "First";
        }

        return "Economy";
    }

    /**
     * @return the ticketType
     */
    public String getTicketType() {
        return ticketType;
    }

    /**
     * @param ticketType
     *            the ticketType to set
     */
    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    /**
     * @return the ticketTypeStr
     */
    public String getTicketTypeStr() {
        if (ticketType != null && ticketType.equals("P")) {
            return "Paper Ticket";
        }
        return "E-Ticket";
    }

    /**
     * @return the fromAirportName
     */
    public String getFromAirportName() {
        return fromAirportName;
    }

    /**
     * @param fromAirportName
     *            the fromAirportName to set
     */
    public void setFromAirportName(String fromAirportName) {
        this.fromAirportName = fromAirportName;
    }

    /**
     * @return the toAirportName
     */
    public String getToAirportName() {
        return toAirportName;
    }

    /**
     * @param toAirportName
     *            the toAirportName to set
     */
    public void setToAirportName(String toAirportName) {
        this.toAirportName = toAirportName;
    }

    /**
     * @return the fareKey
     */
    public String getFareKey() {
        return fareKey;
    }

    /**
     * @param fareKey
     *            the fareKey to set
     */
    public void setFareKey(String fareKey) {
        this.fareKey = fareKey;
    }

    /**
     * @return the fareRule
     */
    public String getFareRule() {
        return fareRule;
    }

    /**
     * @param fareRule
     *            the fareRule to set
     */
    public void setFareRule(String fareRule) {
        this.fareRule = fareRule;
    }

    /**
     * @return the layover
     */
    public int getLayover() {
        return layover;
    }

    /**
     * @param layover
     *            the layover to set
     */
    public void setLayover(int layover) {
        this.layover = layover;
    }

    /**
     * @return the departDateStr
     */
    public String getDepartDateStr() {
        return departDateStr;
    }

    /**
     * @param departDateStr
     *            the departDateStr to set
     */
    public void setDepartDateStr(String departDateStr) {
        this.departDateStr = departDateStr;
    }

    /**
     * @return the arriveDateStr
     */
    public String getArriveDateStr() {
        return arriveDateStr;
    }

    /**
     * @param arriveDateStr
     *            the arriveDateStr to set
     */
    public void setArriveDateStr(String arriveDateStr) {
        this.arriveDateStr = arriveDateStr;
    }

    /**
     * @return the departDate
     */
    public Date getDepartDate() {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.parse(this.departDateStr);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * @return the arriveDate
     */
    public Date getArriveDate() {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.parse(this.arriveDateStr);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * @return the finalDest
     */
    public String getFinalDest() {
        return finalDest;
    }

    /**
     * @param finalDest
     *            the finalDest to set
     */
    public void setFinalDest(String finalDest) {
        this.finalDest = finalDest;
    }

    /**
     * @return the hasStops
     */
    public boolean isHasStops() {
        return hasStops;
    }

    /**
     * @param hasStops
     *            the hasStops to set
     */
    public void setHasStops(boolean hasStops) {
        this.hasStops = hasStops;
    }

    /**
     * @return the isStopOverSegment
     */
    public boolean isStopOverSegment() {
        return isStopOverSegment;
    }

    /**
     * @param isStopOverSegment
     *            the isStopOverSegment to set
     */
    public void setStopOverSegment(boolean isStopOverSegment) {
        this.isStopOverSegment = isStopOverSegment;
    }
}
