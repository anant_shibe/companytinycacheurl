package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * PayDetail.
 * @author deepa
 */
public class PayDetail implements Serializable, Comparable<PayDetail> {
    private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("value")
    private String value;

    @JsonIgnore
    private String status;

    @JsonIgnore
    private List<String> domains;

    @JsonIgnore
    private List<String> channels;

    @JsonIgnore
    private Map<Integer, Double> emiOptionsMap;

    @JsonIgnore
    private boolean otpEnabled;

    @JsonIgnore
    private boolean autootp;

    @JsonIgnore
    private String otpInstruction;

    /**
     * Getter method for id.
     * @return the id
     */
    @JsonProperty("id")
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     * @param id the id to set
     */
    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter method for name.
     * @return the name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     * @param name the name to set
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Getter method for status.
     * @return the status
     */
    @JsonIgnore
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     * @param status the status to set
     */
    @JsonIgnore
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonIgnore
    public Map<Integer, Double> getEmiOptionsMap() {
        return emiOptionsMap;
    }

    @JsonIgnore
    public void setEmiOptionsMap(Map<Integer, Double> emiOptionsMap) {
        this.emiOptionsMap = emiOptionsMap;
    }

    @JsonIgnore
    public boolean isOtpEnabled() {
        return otpEnabled;
    }

    @JsonIgnore
    public void setOtpEnabled(boolean otpEnabled) {
        this.otpEnabled = otpEnabled;
    }

    @JsonIgnore
    public boolean isAutootp() {
        return autootp;
    }

    @JsonIgnore
    public void setAutootp(boolean autootp) {
        this.autootp = autootp;
    }

    @JsonIgnore
    public String getOtpInstruction() {
        return otpInstruction;
    }

    @JsonIgnore
    public void setOtpInstruction(String otpInstruction) {
        this.otpInstruction = otpInstruction;
    }

    @JsonIgnore
    public List<String> getDomains() {
        return domains;
    }

    @JsonIgnore
    public void setDomains(List<String> domains) {
        this.domains = domains;
    }

    @JsonIgnore
    public List<String> getChannels() {
        return channels;
    }

    @JsonIgnore
    public void setChannels(List<String> channels) {
        this.channels = channels;
    }

    @Override
    public int compareTo(PayDetail o) {
        return name.compareToIgnoreCase(o.getName());
    }
}
