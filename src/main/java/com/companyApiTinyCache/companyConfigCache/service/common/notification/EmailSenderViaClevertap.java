package com.companyApiTinyCache.companyConfigCache.service.common.notification;

import java.util.List;
import java.util.Map;

/**
 * @author parvatkumar
 *
 */
public interface EmailSenderViaClevertap {
     void sendEmail(Map<String, Object> payLoad);
     Map<String, Object> buildPayloadForMail(Map<String, Object> payLoad, String senderName);
     boolean validatePayload(Map<String, Object> payLoad, List<String> errorMessages,
 			Map<String, String> sendEmailResponseMap);
}
