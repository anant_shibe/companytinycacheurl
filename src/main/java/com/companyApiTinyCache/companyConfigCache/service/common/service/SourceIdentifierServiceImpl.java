package com.companyApiTinyCache.companyConfigCache.service.common.service;


import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedMobileHeadersResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Sudhanshu
 *
 */
@ClassExcludeCoverage
public class SourceIdentifierServiceImpl implements SourceIdentifierService {

	public static final String SOURCE_DESKTOP_HEADER_NAME = "ct.source.desktop.header.name";

	private CachedProperties commonCachedProperties;
	private CachedMobileHeadersResource commonCachedMobileHeadersResource;

	public CachedProperties getCommonCachedProperties() {
		return commonCachedProperties;
	}

	public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
		this.commonCachedProperties = commonCachedProperties;
	}

	public CachedMobileHeadersResource getCommonCachedMobileHeadersResource() {
		return commonCachedMobileHeadersResource;
	}

	public void setCommonCachedMobileHeadersResource(CachedMobileHeadersResource commonCachedMobileHeadersResource) {
		this.commonCachedMobileHeadersResource = commonCachedMobileHeadersResource;
	}

	public boolean isDesktop(HttpServletRequest request) {
		return !(request.getHeader(commonCachedProperties.getPropertyValue(SOURCE_DESKTOP_HEADER_NAME)) == null);
	}

	public String getSource(HttpServletRequest request) {
		if (isDesktop(request)) {
			return DESKTOP;
		} else if (commonCachedMobileHeadersResource.isMobileApp(request)) {
			return MOBILE_APP;
		} else if (commonCachedMobileHeadersResource.isMobile(request)) {
			return MOBILE_WEBSITE;
		} else {
			return UNKNOWN;
		}
	}
}
