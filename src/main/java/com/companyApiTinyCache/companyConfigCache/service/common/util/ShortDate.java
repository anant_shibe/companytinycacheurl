package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility class to represent a Date (without time component) as a short variable. The date is represented as
 * number of days since 1st Jan 2015 which is the date base. So 31/12/2014 is -1, 1/1/2015 is 0, 2/1/2015 is 1
 * and so on. The TimeZone is assumed to be IST for calculations in converting milliseconds time to short format.
 *
 * This short representation can be used to store dates in memory and cache.
 *
 * Dates from Apr 15, 1925 to Sep 18, 2104 can be represented. To use beyond 2104 change the DATE_BASE
 * in a build and rename cache keys where the short representation is stored.
 *
 * @author suresh
 *
 */
public final class ShortDate {

    // Below is what System.currentTimeMillis() returned at 1/1/2015 12:00 AM IST
    public static final long DATE_BASE = 1420050600000L;

    public static final String [] SHORT_WEEK_DAYS = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

    public static final int ALL_DAYS_FLAGS = 127;

    private ShortDate() {
    }

    public static short toShortDate(long millis) {
        long sd;
        short shortDate = 0;
        if (millis >= DATE_BASE) {
            sd = (millis - DATE_BASE) / GenUtil.MILLIS_PER_DAY;
            if (sd > Short.MAX_VALUE) {
                shortDate = Short.MAX_VALUE;
            } else {
                shortDate = (short) sd;
            }
        } else {
            sd = (millis - DATE_BASE - GenUtil.MILLIS_PER_DAY + 1) / GenUtil.MILLIS_PER_DAY;
            if (sd < Short.MIN_VALUE) {
                shortDate = Short.MIN_VALUE;
            } else {
                shortDate = (short) sd;
            }
        }

        return shortDate;
    }

    public static short toShortDate(long millis, long offset) {
        long sd;
        short shortDate = 0;
        long dateBase = DATE_BASE + offset;
        if (millis >= dateBase) {
            sd = (millis - dateBase) / GenUtil.MILLIS_PER_DAY;
            if (sd > Short.MAX_VALUE) {
                shortDate = Short.MAX_VALUE;
            } else {
                shortDate = (short) sd;
            }
        } else {
            sd = (millis - dateBase - GenUtil.MILLIS_PER_DAY + 1) / GenUtil.MILLIS_PER_DAY;
            if (sd < Short.MIN_VALUE) {
                shortDate = Short.MIN_VALUE;
            } else {
                shortDate = (short) sd;
            }
        }

        return shortDate;
    }

    public static long toMillis(short shortDate) {
        return DATE_BASE + shortDate * GenUtil.MILLIS_PER_DAY;
    }

    public static String toDateString(short shortDate, String fmt) {
        if (fmt == null) {
            fmt = "d/M/yyyy";
        }
        DateFormat df = new SimpleDateFormat(fmt);
        Date d = new Date(toMillis(shortDate));
        return df.format(d);
    }

    public static String toDateString(short shortDate) {
        return toDateString(shortDate, null);
    }

    /**
     * Converts an int containing bits from position 0-6 set corresponding to Sun, Mon.. to an array of Strings.
     * The result can be converted to a String format using Arrays.toString method. Used in CAMP to give readable
     * json output for applicable days.
     * @param weekdayFlags Flags corresponding to which week days are set
     * @return Array of Strings containing corresponding Short Week Days. Empty array if 0 is passed
     */
    public static String [] flagsToShortWeekdays(int weekdayFlags) {
        int i, flag, count;
        count = 0;
        for (i = 0, flag = 1; i < 7; i++, flag <<= 1) {
            if ((weekdayFlags & flag) != 0) {
                count++;
            }
        }
        String [] weekdays = new String[count];
        count = 0;
        for (i = 0, flag = 1; i < 7; i++, flag <<= 1) {
            if ((weekdayFlags & flag) != 0) {
                weekdays[count] = SHORT_WEEK_DAYS[i];
                count++;
            }
        }

        return weekdays;
    }

    // Dummy method to get coverage for Constructor
    public static void invokeConstructorForTestCase() {
        new ShortDate();
    }
}
