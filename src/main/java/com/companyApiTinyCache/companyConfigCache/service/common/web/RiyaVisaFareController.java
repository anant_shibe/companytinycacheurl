package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import com.companyApiTinyCache.companyConfigCache.service.common.org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class RiyaVisaFareController extends SimpleFormController {

    private CachedProperties commonCachedProperties;

    public RiyaVisaFareController(CachedProperties pcommonCachedProperties) {
        this.commonCachedProperties = pcommonCachedProperties;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String visaCountryName = request.getParameter("visaCountryName");
        String visaType = request.getParameter("visa_type");
        String urgencyType = request.getParameter("urgency_type");
        String visaEntryType = request.getParameter("visa_entry_type");
        String adultStr = StringUtils.isNotBlank(request.getParameter("adult")) ? request.getParameter("adult") : "0";
        String childStr = StringUtils.isNotBlank(request.getParameter("child")) ? request.getParameter("child") : "0";
        String infantStr = StringUtils.isNotBlank(request.getParameter("infant")) ? request.getParameter("infant") : "0";
        int adult = Integer.parseInt(adultStr);
        int child = Integer.parseInt(childStr);
        int infant = Integer.parseInt(infantStr);
        Map<String, Object> countryServiceCostList = getCountryServiceCostList();
        Map<String, Double> costMap = getServiceCost(countryServiceCostList, visaCountryName, visaType, adult, child, infant, visaEntryType);

        String result = "Visa Service Cost : Rs." + (int) Math.round(costMap.get("amount")) + " and Courier charges : Rs." + (int) Math.round(costMap.get("courier"));
        PrintWriter responseWriter = new PrintWriter(response.getWriter());
        responseWriter.println(result);
        responseWriter.flush();
        responseWriter.close();
        return null;
    }

    private Map<String, Object> getCountryServiceCostList() {
        Map<String, Object> paxTypeMap = null;
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        try {
            String countryCostStr = commonCachedProperties.getPropertyValue("ct.services.riya.visa.countries.cost.list");
            paxTypeMap = (Map<String, Object>) mapper.readValue(countryCostStr, Map.class);
        } catch (Exception e) {
            logger.error("Exception while loading unrestricted actions" + e.getMessage(), e);
        }
        return paxTypeMap;
    }

    private Map<String, Double> getServiceCost(Map<String, Object> countryServiceCostList, String visaForCountry, String visaType, int adult, int child, int infant, String visaEntryType) {

        double serviceCost = 0.0;
        double adultServiceCost = 0.0;
        double childServiceCost = 0.0;
        double infantServiceCost = 0.0;
        double courierCharges = 0.0;
        Map<String, Double> countryCostMap;
        ObjectMapper mapper = JsonUtil.getObjectMapper();

        if (adult != 0) {
            Map<String, Object> entryTypeMap = (Map<String, Object>) countryServiceCostList.get("ADT");
            Map<String, Object> countryMap = (Map<String, Object>) entryTypeMap.get(visaEntryType);

            if (countryMap.containsKey(visaForCountry)) {
                countryCostMap = (Map<String, Double>) countryMap.get(visaForCountry);
            } else {
                countryCostMap = (Map<String, Double>) countryMap.get("default");
            }
            adultServiceCost = countryCostMap.get("amount");
            serviceCost += adultServiceCost * adult;
            courierCharges += countryCostMap.get("courier");
        }
        if (child != 0) {
            Map<String, Object> entryTypeMap = (Map<String, Object>) countryServiceCostList.get("ADT");
            Map<String, Object> countryMap = (Map<String, Object>) entryTypeMap.get(visaEntryType);

            if (countryMap.containsKey(visaForCountry)) {
                countryCostMap = (Map<String, Double>) countryMap.get(visaForCountry);
            } else {
                countryCostMap = (Map<String, Double>) countryMap.get("default");
            }

            childServiceCost = countryCostMap.get("amount");
            serviceCost += childServiceCost * child;
            courierCharges += countryCostMap.get("courier");
        }

        if (infant != 0) {
            Map<String, Object> entryTypeMap = (Map<String, Object>) countryServiceCostList.get("ADT");
            Map<String, Object> countryMap = (Map<String, Object>) entryTypeMap.get(visaEntryType);

            if (countryMap.containsKey(visaForCountry)) {
                countryCostMap = (Map<String, Double>) countryMap.get(visaForCountry);
            } else {
                countryCostMap = (Map<String, Double>) countryMap.get("default");
            }

            infantServiceCost = countryCostMap.get("amount");
            serviceCost += infantServiceCost * infant;
            courierCharges += countryCostMap.get("courier");
        }

        Map<String, Double> costMap = new HashMap<String, Double>();
        costMap.put("amount", serviceCost);
        costMap.put("courier", courierCharges);

        return costMap;
    }
}
