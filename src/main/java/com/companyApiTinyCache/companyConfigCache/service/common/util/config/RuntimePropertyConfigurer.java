package com.companyApiTinyCache.companyConfigCache.service.common.util.config;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.Properties;

public class RuntimePropertyConfigurer extends PropertyPlaceholderConfigurer {

    private static final Log log = LogFactory.getLog(RuntimePropertyConfigurer.class);

    private CachedProperties cachedProperties;

    private RuntimePropertyConfigurer(CachedProperties pcachedProperties) {
        cachedProperties = pcachedProperties;
    }

    /**
     * {@inheritDoc}
     * 
     * @see PropertyPlaceholderConfigurer#resolvePlaceholder(String, Properties)
     */
    @Override
    protected String resolvePlaceholder(String placeholder, Properties props) {
        String value = cachedProperties.getPropertyValue(placeholder);
        return value;
    }

}
