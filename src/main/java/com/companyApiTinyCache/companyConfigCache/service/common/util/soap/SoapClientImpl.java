package com.companyApiTinyCache.companyConfigCache.service.common.util.soap;

import org.apache.axis.Message;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class SoapClientImpl implements SoapClient {

    private static final Log logger = LogFactory.getLog(SoapClientImpl.class);

    // Singleton
    private SoapClientImpl() {
    }

    private static final SoapClientImpl INSTANCE = new SoapClientImpl();

    public static SoapClientImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public SoapResponse invoke(SoapRequest request) throws SoapException {
        final int RETRY_COUNT = request.getRetryCount();
        int retryAttempts = 0;
        InputStream requestInputStream = null;
        SOAPEnvelope requestMessage = null;
        Call call = null;

        try {
            Service service = new Service();
            call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(request.getTargetEndpointUrl()));
            call.setSOAPActionURI(request.getRequestUrl());
            requestInputStream = new ByteArrayInputStream(request.getRequestContents().getBytes());
            Message axisMessage = new Message(requestInputStream);
            requestMessage = axisMessage.getSOAPEnvelope();
            // Not handling headers, as of now.
        } catch (Exception e) {
            throw new SoapException(e);
        } finally {
            if (requestInputStream != null) {
                IOUtils.closeQuietly(requestInputStream);
            }
        }

        while (true) {
            try {
                SOAPEnvelope responseMessage = (SOAPEnvelope) call.invoke(requestMessage);
                return new SoapResponse(responseMessage);
            } catch (Exception e) {
                if (retryAttempts < RETRY_COUNT) {
                    logger.error(String.format("SoapClientImpl.invoke: Error occurred: %s", e.getMessage()));
                    logger.info(String.format("SoapClientImpl.invoke: Retry attempt count %s < configured retry count %s, retrying again...", retryAttempts, RETRY_COUNT));
                    retryAttempts++;
                    continue; // Retry the request.
                }
                logger.info(String.format("SoapClientImpl.invoke: Retry attempt count %s reached configured retry count %s. giving up.", retryAttempts, RETRY_COUNT));
                throw new SoapException(e);
            }
        }
    }
}
