package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.RequestTrackingListener.RequestInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.zip.GZIPOutputStream;

/**
 * Controller for /common/requests to list active and slow tomcat requests. Uses the getActiveRequests method of
 * RequestTrackingListener to get the active requests.
 *
 * @author suresh
 */
@ClassExcludeCoverage
public class ListRequestsController extends AbstractController {

    /**
     * Json Bean to capture the active and slow counts for a URL running in Tomcat.
     *
     * @author suresh
     */
    public static class PathInfo implements Comparable<PathInfo> {
        private String path;

        private int active;

        private int slow;

        @Override
        public int compareTo(PathInfo o) {
            int result = o.slow - slow;
            if (result == 0) {
                result = o.active - active;
            }
            return result;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String ppath) {
            path = ppath;
        }

        public int getActive() {
            return active;
        }

        public void setActive(int pactive) {
            active = pactive;
        }

        public int getSlow() {
            return slow;
        }

        public void setSlow(int pslow) {
            slow = pslow;
        }
    }

    private static final Log log = LogFactory.getLog(RequestTrackingListener.class);

    private static final int NONE = 0;

    private static final int SLOW = 1;

    private static final int ALL = 2;

    private CachedProperties commonCachedProperties;

    private boolean jsonFormat;

    private volatile long lastSaveTime;

    public ListRequestsController() {
        setCacheSeconds(0);
    }

    private void addStackTrace(Thread t, StringBuilder sb, String prefix) {
        StackTraceElement[] traceElements = t.getStackTrace();
        for (StackTraceElement traceElement : traceElements) {
            sb.append(prefix).append("        at ").append(traceElement.toString()).append('\n');
        }
        if (traceElements.length > 0) {
            sb.setLength(sb.length() - 1);
        }
    }

    private String getStackTrace(RequestInfo requestInfo, StringBuilder sb) {
        sb.setLength(0);
        addStackTrace(requestInfo.getRequestThread(), sb, "");

        Vector<Thread> childThreads = requestInfo.getActiveChildThreads();
        int i, len;
        if (childThreads != null && (len = childThreads.size()) > 0) {
            for (i = 0; i < len;) {
                int requestNo = i + 1;
                sb.append("\n    BEGIN CHILD THREAD " + requestNo + '\n');
                addStackTrace(childThreads.get(i), sb, "    ");
                sb.append("\n    END CHILD THREAD " + requestNo);
                i = requestNo;
            }
        }

        return sb.toString();
    }

    private static String getQueryString(Map<String, String[]> paramMap, StringBuilderWriter sbw) throws UnsupportedEncodingException {
        sbw.clear();
        if (paramMap != null && paramMap.size() > 0) {
            int i = 0;
            Set<Entry<String, String[]>> entries = paramMap.entrySet();
            for (Entry<String, String[]> entry : entries) {
                String [] values = entry.getValue();
                for (String value : values) {
                    String key = entry.getKey();
                    value = GenUtil.hideCardInfo(key, value);
                    if ("password".equalsIgnoreCase(key) || "pwd".equalsIgnoreCase(key) || "pswd".equalsIgnoreCase(key)) {
                        value = "******";
                    }
                    if (i > 0) {
                        sbw.write('&');
                    }
                    sbw.write(key);
                    sbw.write('=');
                    sbw.write(URLEncoder.encode(value, "UTF-8"));
                    i++;
                }
            }
        }
        return sbw.toString();
    }

    private void writeTextResponse(Map<String, Object> jsonMap, Writer writer) throws IOException {
        writer.write("        ACTIVE REQUESTS INFO\n");
        writer.write("        ====================\n\n");
        Map<String, String> params = (Map<String, String>) jsonMap.get("params");
        writer.write("Options from URL params/defaulted:: time:" + params.get("time") + ", log:" + params.get("log") + " (slow|none|all)" + ", jsonFormat:" + params.get("jsonFormat") + "\n\n");
        writer.write("Active Requests: " + jsonMap.get("active") + ", Slow Requests: " + jsonMap.get("slow") + "\n");
        writer.write("Host: " + jsonMap.get("host") + ", Ip: " + jsonMap.get("ip") + ", Port: " + jsonMap.get("port") + ", Current Time: " + new Date((Long) jsonMap.get("current_time")) + "\n");
        List<PathInfo> pathInfos = (List<PathInfo>) jsonMap.get("urls");
        if (pathInfos != null) {
            writer.write("\nACTIVE URLS");
            writer.write("\n-----------\n");
            for (PathInfo pathInfo : pathInfos) {
                writer.write("URL: " + pathInfo.path + ", Total Active: " + pathInfo.active + ", Total Slow: " + pathInfo.slow + '\n');
            }
        }
        List<Map<String, Object>> requestDetails = (List<Map<String, Object>>) jsonMap.get("logs");
        if (requestDetails != null) {
            StringBuilderWriter sbw = new StringBuilderWriter();
            writer.write("\nREQUEST LOGS");
            writer.write("\n------------\n");
            ObjectMapper mapper = new ObjectMapper();
            for (Map<String, Object> requestDetailMap : requestDetails) {
                writer.write("\nREQUEST ID " + requestDetailMap.get("id") + '\n');
                writer.write("Age: " + requestDetailMap.get("age") + ", Slow: " + requestDetailMap.get("slow") + ", URL: " + requestDetailMap.get("url") + "\nParams::\n");
                Map<String, String[]> paramMap = (Map<String, String[]>) requestDetailMap.get("params");
                writer.write(getQueryString(paramMap, sbw));
                writer.write("\nTrace::\n");
                writer.write((String) requestDetailMap.get("trace"));
                writer.write("\nHeaders::\n");
                sbw.clear();
                mapper.writeValue(sbw, requestDetailMap.get("headers"));
                writer.write(sbw.toString());
                writer.write('\n');
            }
        }
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
                                                 HttpServletResponse response) throws Exception {
        List<RequestInfo> requestInfos = RequestTrackingListener.getActiveRequests();
        Map<String, Object> jsonMap = new LinkedHashMap<String, Object>();
        int active = 0;
        int slow = 0;
        String timeParam = StringUtils.trimToNull(request.getParameter("time"));
        if (timeParam == null) {
            timeParam = "5";
        }
        long slowMillis = Math.round(1000 * Double.parseDouble(timeParam));
        if (slowMillis < 0) {
            slowMillis = Long.MAX_VALUE;
        }
        String logParam = StringUtils.trimToNull(request.getParameter("log"));
        if (logParam == null) {
            logParam = "slow";
        } else {
            logParam = logParam.toLowerCase();
        }
        long saveFrequency = 300;
        String saveFrequencyParam = null;
        if (jsonFormat) {
            saveFrequencyParam = StringUtils.trimToNull(request.getParameter("save_frequency"));
            if (saveFrequencyParam == null) {
                saveFrequencyParam = "300";
            }
            saveFrequency = 1000 * Long.parseLong(saveFrequencyParam);
        }
        StringBuilder sb = null;
        Map<String, PathInfo> pathMap = null;
        List<Map<String, Object>> requestDetails = null;
        long now = System.currentTimeMillis();
        String host = commonCachedProperties.getPropertyValue("ct.common.hostname");
        String ip = commonCachedProperties.getPropertyValue("ct.common.ip");
        String port = commonCachedProperties.getPropertyValue("ct.common.port");
        boolean includeUrls = true;
        int logOption = NONE;
        if ("slow".equals(logParam)) {
            logOption = SLOW;
        } else if ("all".equals(logParam)) {
            logOption = ALL;
        }
        if (jsonFormat) {
            boolean noLogging = true;
            if (logOption != NONE) {
                if (saveFrequency >= 0 && now - lastSaveTime >= saveFrequency) {
                    if (logOption == SLOW) {
                        for (RequestInfo requestInfo : requestInfos) {
                            if (now - requestInfo.getStartTime() >= slowMillis) {
                                noLogging = false;
                                break;
                            }
                        }
                    } else {
                        noLogging = false;
                    }
                    if (!noLogging) {
                        noLogging = true;
                        synchronized (this) {
                            if (now - lastSaveTime >= saveFrequency) {
                                lastSaveTime = now;
                                noLogging = false;
                            }
                        }
                    }
                }
            }
            if (noLogging) {
                logOption = NONE;
                includeUrls = false;
            }
        } else {
            RequestTrackingListener.LAST_INSPECT_TIME_MILLIS = now;
        }
        for (RequestInfo requestInfo : requestInfos) {
            long age = now - requestInfo.getStartTime();
            boolean isSlow = age >= slowMillis;
            Map<String, Object> requestDetailMap = null;
            HttpServletRequest activeRequest = (HttpServletRequest) requestInfo.getRequest();
            if ((logOption == ALL || isSlow && logOption == SLOW) && requestInfo.getEndTime() == 0) {
                requestDetailMap = new LinkedHashMap<String, Object>();
                requestDetailMap.put("id", requestInfo.getId());
                requestDetailMap.put("age", age);
                requestDetailMap.put("slow", isSlow);
                requestDetailMap.put("url", activeRequest.getRequestURI());
                requestDetailMap.put("params", activeRequest.getParameterMap());
                if (sb == null) {
                    sb = new StringBuilder();
                }
                requestDetailMap.put("trace", getStackTrace(requestInfo, sb));
                requestDetailMap.put("headers", GenUtil.getRequestHeaders(activeRequest, false));
            }
            if (requestInfo.getEndTime() == 0) {
                active++;
                if (isSlow) {
                    slow++;
                }
                if (includeUrls) {
                    if (pathMap == null) {
                        pathMap = new HashMap<String, PathInfo>();
                    }
                    String path = activeRequest.getRequestURI();
                    PathInfo pathInfo = pathMap.get(path);
                    if (pathInfo == null) {
                        pathInfo = new PathInfo();
                        pathMap.put(path, pathInfo);
                    }
                    pathInfo.path = path;
                    pathInfo.active++;
                    if (isSlow) {
                        pathInfo.slow++;
                    }
                }
                if (requestDetailMap != null) {
                    if (requestDetails == null) {
                        requestDetails = new ArrayList<Map<String, Object>>();
                    }
                    requestDetails.add(requestDetailMap);
                }
            }
        }
        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put("time", timeParam);
        params.put("log", logParam);
        params.put("jsonFormat", String.valueOf(jsonFormat));
        if (jsonFormat) {
            params.put("save_frequency", saveFrequencyParam);
        }
        jsonMap.put("params", params);
        jsonMap.put("active", active);
        jsonMap.put("slow", slow);
        jsonMap.put("host", host);
        jsonMap.put("ip", ip);
        jsonMap.put("port", port);
        jsonMap.put("current_time", now);
        if (pathMap != null) {
            List<PathInfo> urls = new ArrayList<PathInfo>(pathMap.values());
            Collections.sort(urls);
            jsonMap.put("urls", urls);
        }
        if (requestDetails != null) {
            jsonMap.put("logs", requestDetails);
        }
        String saveFile = null;
        boolean traceEnabled = commonCachedProperties.getBooleanPropertyValue(RequestTrackingListener.ENABLE_PROPERTY, true);
        String dirStr = StringUtils.trimToNull(commonCachedProperties.getPropertyValue("ct.common.request-tracking.log.dir"));
        if (traceEnabled && dirStr != null && (!jsonFormat || requestDetails != null)) {
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.setLength(0);
            }
            sb.append(dirStr);
            if (!dirStr.endsWith("/")) {
                sb.append('/');
            }
            DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
            DateFormat timeFormat = new SimpleDateFormat("HH_mm_ss");
            Date currentDate = new Date(now);
            sb.append(dateFormat.format(currentDate)).append('/');
            sb.append("request_log_").append(host);
            sb.append('_').append(timeFormat.format(currentDate)).append(".txt");
            saveFile = sb.toString();
            if (jsonFormat) {
                File sf = new File(saveFile);
                File parent = sf.getParentFile();
                if (parent.exists() || parent.mkdirs()) {
                    Writer fileWriter = null;
                    try {
                        fileWriter = new BufferedWriter(new FileWriter(saveFile));
                        writeTextResponse(jsonMap, fileWriter);
                    } catch (Exception e) {
                        log.error("Unable to Save Log to " + saveFile + ". Error: " + e.getMessage());
                    } finally {
                        if (fileWriter != null) {
                            fileWriter.close();
                        }
                    }
                } else {
                    log.error("Unable to Save Log to " + saveFile + ". Cannot create parent directory.");
                }
            }
        }
        if (jsonFormat) {
            jsonMap.remove("urls");
            jsonMap.remove("logs");
            Writer writer = response.getWriter();
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            mapper.writeValue(writer, jsonMap);
            writer.write('\n');
            return null;
        } else {
            ModelAndView modelAndView = new ModelAndView("listRequests");
            modelAndView.addObject("traceEnabled", traceEnabled);
            modelAndView.addObject("enableProperty", RequestTrackingListener.ENABLE_PROPERTY);
            modelAndView.addObject("host", host);
            modelAndView.addObject("ip", ip);
            if (port != null) {
                modelAndView.addObject("port", port);
            }
            if (traceEnabled && dirStr == null) {
                modelAndView.addObject("errorMsg", "Please set the property ct.common.request-tracking.log.dir to a valid directory to save logs");
            }
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.setLength(0);
            }
            StringBuilderWriter sbw = new StringBuilderWriter(sb);
            writeTextResponse(jsonMap, sbw);
            String requestInfo = sbw.toString();
            modelAndView.addObject("requestInfo", requestInfo);
            if (saveFile != null) {
                modelAndView.addObject("saveFile", saveFile);
            }
            ByteArrayOutputStream bop = new ByteArrayOutputStream();
            Writer byteArrayWriter = new OutputStreamWriter(new GZIPOutputStream(bop));
            byteArrayWriter.write(requestInfo);
            byteArrayWriter.close();
            String requestInfoData = new String(Base64.encodeBase64(bop.toByteArray()));
            modelAndView.addObject("requestInfoData", requestInfoData);
            return modelAndView;
        }
    }

    public void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
    }

    public void setJsonFormat(boolean pjsonFormat) {
        jsonFormat = pjsonFormat;
    }
}
