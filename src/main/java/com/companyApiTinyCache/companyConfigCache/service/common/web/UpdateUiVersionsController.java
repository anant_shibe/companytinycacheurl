/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CustomCachedUrlResources;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @author Ramesh
 * 
 */
public class UpdateUiVersionsController extends AbstractController {

    protected static final Log logger = LogFactory.getLog(UpdateUiVersionsController.class);

    private CachedProperties cachedUiProperties;

    private CustomCachedUrlResources cachedUrlResources;

    public UpdateUiVersionsController(CachedProperties pcachedUiProperties, CustomCachedUrlResources pcachedUrlResources) {
        cachedUiProperties = pcachedUiProperties;
        cachedUrlResources = pcachedUrlResources;
        setCacheSeconds(0);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.mvc.AbstractFormController#handleRequestInternal(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String message = "success";
        try {
            cachedUiProperties.refreshResource(0);
            cachedUrlResources.refreshResource(0);
        } catch (Exception e) {
            logger.error("Error:", e);
            message = "failure";
        }
        PrintWriter responseWriter = response.getWriter();
        responseWriter.println(message);
        responseWriter.close();
        return null;
    }

}
