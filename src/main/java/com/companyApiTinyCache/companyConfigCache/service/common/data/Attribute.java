/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

/**
 * @author ramesh
 * 
 */
public class Attribute {
    private int attrId;
    private String attrName;
    private String attrDataType;

    @SuppressWarnings("unused")
    private String delimitedArrId;

    /**
     * @return the attrId
     */
    public int getAttrId() {
        return attrId;
    }

    /**
     * @param attrId
     *            the attrId to set
     */
    public void setAttrId(int attrId) {
        this.attrId = attrId;
    }

    /**
     * @return the attrName
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * @param attrName
     *            the attrName to set
     */
    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    /**
     * 
     * @return the attrDataType
     */
    public String getAttrDataType() {
        return attrDataType;
    }

    /**
     * 
     * @param attrDataType
     *            String
     */
    public void setAttrDataType(String attrDataType) {
        this.attrDataType = attrDataType;
    }

    /**
     * 
     * @return delimitedArrId
     */
    public String getDelimitedArrId() {
        String delimitedArrId = getAttrId() + "|" + getAttrDataType();
        return delimitedArrId;
    }

    /**
     * 
     * @param delimitedArrId
     */
    public void setDelimitedArrId(String delimitedArrId) {
        this.delimitedArrId = delimitedArrId;
    }
}
