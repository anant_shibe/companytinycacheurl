package com.companyApiTinyCache.companyConfigCache.service.common.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * A GC friendly ByteArrayOutputStream which has a getInputStream method to read the internal byte array without having to copy the contents.
 *
 * @author Suresh
 *
 */
public class ReusableByteArrayOutputStream extends ByteArrayOutputStream {

    public ReusableByteArrayOutputStream() {
        super();
    }

    public ReusableByteArrayOutputStream(int size) {
        super(size);
    }

    public ByteArrayInputStream getInputStream() {
        ByteArrayInputStream bip = new ByteArrayInputStream(buf, 0, count);
        return bip;
    }

    public byte [] getBuf() {
        return buf;
    }

    public int getCount() {
        return count;
    }
}
