package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

public interface MessagingFactory {

    Object getReceiver();

    MessageSender getSender();
}
