package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GSTDetails implements Serializable {

	private static final long serialVersionUID = 4971323286624627255L;
	private String gstNumber;
    private String gstHolderName;
    private String gstHolderAddress;
    private String gstHolderStateName;
    private String gstHolderStateCode;
    private String gstInvoiceEmailAddress;

    @JsonProperty("gst_number")
    public String getGstNumber() {
        return gstNumber;
    }

    @JsonProperty("gst_number")
    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    @JsonProperty("gst_holder_name")
    public String getGstHolderName() {
        return gstHolderName;
    }

    @JsonProperty("gst_holder_name")
    public void setGstHolderName(String gstHolderName) {
        this.gstHolderName = gstHolderName;
    }

    @JsonProperty("gst_holder_address")
    public String getGstHolderAddress() {
        return gstHolderAddress;
    }

    @JsonProperty("gst_holder_address")
    public void setGstHolderAddress(String gstHolderAddress) {
        this.gstHolderAddress = gstHolderAddress;
    }

    @JsonProperty("gst_holder_state_name")
    public String getGstHolderStateName() {
        return gstHolderStateName;
    }

    @JsonProperty("gst_holder_state_name")
    public void setGstHolderStateName(String gstHolderStateName) {
        this.gstHolderStateName = gstHolderStateName;
    }

    @JsonProperty("gst_holder_state_code")
    public String getGstHolderStateCode() {
        return gstHolderStateCode;
    }

    @JsonProperty("gst_holder_state_code")
    public void setGstHolderStateCode(String gstHolderStateCode) {
        this.gstHolderStateCode = gstHolderStateCode;
    }

    @JsonProperty("gst_invoice_email_address")
    public String getGstInvoiceEmailAddress() {
        return gstInvoiceEmailAddress;
    }

    @JsonProperty("gst_invoice_email_address")
    public void setGstInvoiceEmailAddress(String gstInvoiceEmailAddress) {
        this.gstInvoiceEmailAddress = gstInvoiceEmailAddress;
    }

}
