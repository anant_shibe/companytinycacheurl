package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Map;

/**
 * SAX Handler which reuses a StringBuilder from BaseStats for storing chars.
 *
 * @author Suresh
 *
 */
public class CtDefaultHandler extends DefaultHandler {

    protected StringBuilder stringBuilder;

    protected ObjectFromProperty<Map<String, Object>> taxCodeMapInfo;

    @Override
    public void startDocument() throws SAXException {
        stringBuilder = BaseStats.tmpStringBuilder();
    }

    @Override
    public void endDocument() throws SAXException {
        stringBuilder = BaseStats.returnTmpStringBuilder(stringBuilder);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        stringBuilder = BaseStats.returnTmpStringBuilder(stringBuilder);
        super.fatalError(e);
    }

	public String getTaxCode(String code) {
		String taxCode = null;

		if (taxCodeMapInfo != null) {
			Map<String, Object> taxCodeMap = taxCodeMapInfo.checkAndGetObject();
			if (taxCodeMap != null) {
				taxCode = (String) taxCodeMap.get(code);
			}
			if (StringUtils.isBlank(taxCode)) {
				taxCode = "AIRLINE-MSC";
			}
		}
		return taxCode;
	}
}
