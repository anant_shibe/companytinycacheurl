package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.util.config.CtConfigConsuleProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.DefaultRestClientImpl;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestClient;
import com.companyApiTinyCache.companyConfigCache.service.common.util.rest.RestRequest;
import com.cleartrip.platform.commons.ctconfig.java.CTConfig;
import com.cleartrip.platform.commons.ctconfig.java.eventlistener.RefreshEventListener;
import com.cleartrip.platform.commons.ctconfig.java.eventlistener.ResourceRefreshEventListener;
import com.cleartrip.platform.commons.ctconfig.java.newrelic.NewRelicInsightConnector;
import com.cleartrip.platform.commons.ctconfig.java.store.ConfigStoreUtils;
import com.cleartrip.platform.commons.ctconfig.java.store.DefaultConfigStore;
import com.cleartrip.platform.commons.ctconfig.store.ConfigStore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.orbitz.consul.Consul;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Cached Properties which consolidates properties from multiple sources. The sources are specified in a String in CSV format. Format for each section is: <URL|path|classpath:path>[;refreshTime in
 * millis]. classpath: stuff are loaded only during startup. refreshTime is useful to have a specific refresh time. The final properties is built by loading properities from each source and merging
 * them. Default properties dir is ctb.
 *
 * @author Suresh
 *
 */
public class CachedProperties extends CachedResource<Map<String, String>> {

    private static final Log logger = LogFactory.getLog(CachedProperties.class);

    private static final String NEWRELIC_API_INSERT_KEY = "xxOQSlwv-CMnwWjBdUKZKnuTU6Yit_T6";
    private static final String INSIGHTS_URL = "http://insights-collector.newrelic.com/v1/accounts/1835861/events";

    private static class SourceInfo {
        private String source;
        private long refreshInterval;
        private long lastRefreshTime;
        private long modifiedTime;
        private String[] keys;
        private String[] values;
        private String prevTrace;

        public boolean isUrl() {
            String src = source;
            return src != null && (src.indexOf("http:") == 0 || src.indexOf("https:") == 0);
        }
    }

    private Consul consul;

    private static final List<String> DIGEST_EXCLUDED_PROPERTIES = new ArrayWrappingList<String>(new String[] {"ct.common.contexts.excludelist", "ct.common.hostid", "ct.common.hostname",
            "ct.common.local.properties-file", "ct.common.local.serverid", "ct.common.servertype", "ct.common.port", "ct.common.ip", "ct.ctconfig.enabled"});

    private List<SourceInfo> sourceInfoList;

    private String sources;

    private Map<String, String> overridingProps;

    private boolean overridingPropsModified;

    private CachedProperties propertiesForSource;

    private String prevSources;

    private List<String> newPropertyKeys;

    private List<String> newPropertyVals;

    private volatile Map<String, String> propertyMap;

    // Whether to merge properties on reload. Currently always true. Use a Setter
    // if it needs to be changed and move refreshResource to afterPropertiesSet
    private boolean merge;

    private DefaultConfigStore defaultConfigStore;

    private final CtConfigConsuleProperties consulProperties;


    private void updateKeyVals(SourceInfo sourceInfo) {
        String[] keys = sourceInfo.keys;
        String[] vals = sourceInfo.values;
        for (int i = 0; i < keys.length; i++) {
            if (keys[i].endsWith("_VER")) {
                keys[i] = keys[i].replace('.', '_');
                keys[i] = keys[i].replace('-', '_');
                vals[i] = ".v" + vals[i];
            }
        }
    }

    private InputStream openUrlResource(SourceInfo sourceInfo, boolean force) throws IOException {
        String url = sourceInfo.source;
        URL loc = new URL(url);
        HttpURLConnection con = (HttpURLConnection) loc.openConnection();
        con.setConnectTimeout(15000);
        con.setReadTimeout(5000);
        int status = 0;
        /*
         * Removing Modified check since it can cause bugs if(!force && sourceInfo.modifiedTime > 0) { con.setIfModifiedSince(sourceInfo.modifiedTime); status = con.getResponseCode(); }
         */
        InputStream in = null;
        if (status != HttpURLConnection.HTTP_NOT_MODIFIED) {
            in = con.getInputStream();
        }
        sourceInfo.modifiedTime = con.getLastModified();
        return in;
    }

    private InputStream openFileResource(SourceInfo sourceInfo, boolean force) throws FileNotFoundException {
        boolean isDebug = logger.isDebugEnabled();
        boolean requiresRefresh = force;
        String file = sourceInfo.source;
        File f = new File(file);
        long currentFileModifiedTime = f.lastModified();
        if (!force) {
            requiresRefresh = currentFileModifiedTime != sourceInfo.modifiedTime;
        }
        if (isDebug) {
            logger.debug("openFileResource: For Source: " + sourceInfo.source + " requiresRefresh = " + requiresRefresh + " currentFileModifiedTime = " + new Date(currentFileModifiedTime) + '('
                    + currentFileModifiedTime + ')' + " Stored Modified Time = " + new Date(sourceInfo.modifiedTime) + '(' + sourceInfo.modifiedTime + ')');
        }
        InputStream in = null;
        if (requiresRefresh) {
            if (currentFileModifiedTime > 0) {
                in = new FileInputStream(file);
            } else {
                // In case file is deleted we want to keep an empty map
                in = new ByteArrayInputStream(new byte[0]);
            }
            sourceInfo.modifiedTime = currentFileModifiedTime; // Changed First itself to avoid repeating errors
        }

        return in;
    }

    private InputStream openClasspathResource(SourceInfo sourceInfo, boolean force) {
        InputStream in = null;
        if (sourceInfo.modifiedTime <= 0) {
            sourceInfo.modifiedTime = System.currentTimeMillis();
            String path = sourceInfo.source;
            int pos = path.indexOf(':');
            path = path.substring(pos + 1);
            in = this.getClass().getResourceAsStream(path);
        }

        return in;
    }

    private InputStream openResource(SourceInfo sourceInfo, boolean force) throws IOException {
        InputStream in = null;
        String source = StringUtils.trimToNull(sourceInfo.source);
        if (source != null && source.indexOf("${") < 0) {
            if (sourceInfo.isUrl()) {
                in = openUrlResource(sourceInfo, force);
            } else if (source.indexOf("classpath:") == 0) {
                in = openClasspathResource(sourceInfo, force);
            } else {
                in = openFileResource(sourceInfo, force);
            }
        }
        return in;
    }

    private static String replacePlaceholders(String s, Map<String, String> map, Map<String, String> backupMap, boolean simpleValueOnly) {
        StringBuilder sb = null;
        String retVal = s;
        if (s != null && map != null) {
            boolean replaced;
            do {
                replaced = false;
                int copiedTill = 0;
                int pos = 0, pos1;
                while ((pos = s.indexOf("${", pos)) >= 0) {
                    int beginIndex = pos;
                    pos += 2;
                    pos1 = s.indexOf('}', pos);
                    if (pos1 > 0) {
                        String key = s.substring(pos, pos1);
                        String value = map.get(key);
                        if (value == null && backupMap != null) {
                            value = backupMap.get(key);
                        }
                        if (simpleValueOnly) {
                            String checkValue = replacePlaceholders(value, map, backupMap, false);
                            if (!GenUtil.equals(value, checkValue)) {
                                value = null;
                            }
                        }
                        if (value != null) {
                            if (sb == null) {
                                sb = new StringBuilder(s.length());
                            }
                            sb.append(s, copiedTill, beginIndex);
                            copiedTill = pos1 + 1;
                            sb.append(value);
                            replaced = true;
                        }
                    }
                }
                if (replaced) {
                    sb.append(s, copiedTill, s.length());
                    s = sb.toString();
                    if (!s.equals(retVal)) {
                        retVal = s;
                        sb.setLength(0);
                    } else {
                        replaced = false;
                    }
                }
            } while (replaced);
        }
        return retVal;
    }

    private synchronized boolean updateOverridingProperties() {
        boolean modified = false;
        if (newPropertyKeys != null) {
            int len = newPropertyKeys.size();
            for (int i = 0; i < len; i++) {
                String key = newPropertyKeys.get(i);
                String value = newPropertyVals.get(i);
                String currentValue = overridingProps.get(key);
                if (value == null) {
                    if (currentValue != null) {
                        overridingProps.remove(key);
                        modified = true;
                    }
                } else if (!value.equals(currentValue)) {
                    overridingProps.put(key, StringUtils.trim(value));
                    modified = true;
                }
            }
            newPropertyKeys.clear();
            newPropertyVals.clear();
        }

        return modified;
    }

    private boolean updateSources(Map<String, String> newResources) {
        int i, len;
        boolean isDebug = logger.isDebugEnabled();
        boolean modified = false;
        Map<String, String> backupProperties = null;
        if (propertiesForSource != null) {
            backupProperties = propertiesForSource.getResource();
        }
        String newSources = replacePlaceholders(sources, newResources, backupProperties, true);
        if (isDebug) {
            logger.debug("Got New Sources: " + newSources);
            logger.debug("Got Old Sources: " + prevSources);
            logger.debug("Compare Result: " + newSources.equals(prevSources));
        }
        if (!newSources.equals(prevSources)) {
            SourceInfo sourceForInitialProps = null;
            modified = true;
            len = 0;
            i = 0;
            if (sourceInfoList != null) {
                len = sourceInfoList.size();
                if (len > 0) {
                    SourceInfo firstSource = sourceInfoList.get(0);
                    if (firstSource.source == null) {
                        sourceForInitialProps = firstSource;
                        i = 1;
                    }
                }
            }
            String[] sourcesArr = newSources.split(",");
            List<SourceInfo> newSourceInfoList = new ArrayList<SourceInfo>(sourcesArr.length + i);
            if (sourceForInitialProps != null) {
                newSourceInfoList.add(sourceForInitialProps);
            }
            for (i = 0; i < sourcesArr.length; i++) {
                String newSource = sourcesArr[i].trim();
                long refreshTime = 0;
                int pos = newSource.indexOf(';');
                if (pos > 0) {
                    refreshTime = Long.parseLong(newSource.substring(pos + 1));
                    newSource = newSource.substring(0, pos).trim();
                }
                SourceInfo newSourceInfo = null;
                for (int j = 0; j < len; j++) {
                    SourceInfo sourceInfo = sourceInfoList.get(j);
                    if (newSource.equals(sourceInfo.source)) {
                        newSourceInfo = sourceInfo;
                        newSourceInfo.refreshInterval = refreshTime;
                        break;
                    }
                }
                if (newSourceInfo == null) {
                    newSourceInfo = new SourceInfo();
                    newSourceInfo.keys = new String[0];
                    newSourceInfo.refreshInterval = refreshTime;
                    newSourceInfo.source = newSource;
                    newSourceInfo.values = new String[0];
                }
                newSourceInfoList.add(newSourceInfo);
            }
            prevSources = newSources;
            sourceInfoList = newSourceInfoList;
            setName(CachedProperties.class.getSimpleName() + '(' + sources + ')');
        }
        // Check if any properties are set programatically
        if (updateOverridingProperties()) {
            overridingPropsModified = true;
            modified = true;
        }

        return modified;
    }

    protected Map<String, String> createMap(Map<String, String> initialProperties) {
        Map<String, String> map;
        if (initialProperties == null) {
            map = new HashMap<String, String>();
        } else {
            map = new HashMap<String, String>(initialProperties);
        }
        return map;
    }

    public CachedProperties(Map<String, String> pinitialProperties, CachedProperties ppropertiesForSource, String psources, Map<String, String> poverridingProps, CtConfigConsuleProperties consulProperties) {
        int i;
        this.consulProperties = consulProperties;
        resource = createMap(pinitialProperties);
        if (pinitialProperties != null) {
            sourceInfoList = new ArrayList<SourceInfo>(1);
            SourceInfo newSourceInfo = new SourceInfo();
            Set<String> keySet = resource.keySet();
            int numKeys = keySet.size();
            newSourceInfo.keys = new String[numKeys];
            newSourceInfo.values = new String[numKeys];
            i = 0;
            for (String key : keySet) {
                newSourceInfo.keys[i] = key;
                newSourceInfo.values[i] = resource.get(key);
                i++;
            }
            sourceInfoList.add(newSourceInfo);
        }
        if (poverridingProps != null) {
            overridingProps = new HashMap<String, String>(poverridingProps);
        }
        sources = psources.replaceAll("\\\\\\{", "\\{").replaceAll(",", ", ");

        String runtimeOverrideFile = System.getProperty("runtime.override.properties.file");
        if(runtimeOverrideFile != null) {
            sources = sources + "," + runtimeOverrideFile;
        }

        final String profile = System.getProperty("spring.profiles.active");
        if(profile == null || profile.isEmpty() || profile.contains("qa") || profile.contains("local")) { //This means only local or qa environment will get custom.properties
            sources = sources + "," + getLocalCustomPropertiesSourceDir() + "custom.properties";
        }
        setName(CachedProperties.class.getSimpleName() + '(' + psources + ')');
        propertiesForSource = ppropertiesForSource;
        merge = true;
        resourceContentType = "text/plain";

        refreshResource();
    }

    private String getLocalCustomPropertiesSourceDir() {
        String fileSeparator = System.getProperty("file.separator");
        return System.getProperty("user.home") + fileSeparator + "ctb" + fileSeparator;
    }

    public CachedProperties(String psources) { this(null, null, psources, null,null); }



    private Map<String, String> loadProperties(long cacheMillis, boolean force, Map<String, String> [] propertyMapHolder, boolean [] isReloadInBackgroundForUrlSources) throws Exception {
        boolean isDebug = logger.isDebugEnabled();
        if (isDebug) {
            String contact = "9916128558 OR suresh.mahalingam@cleartrip.com";
            logger.debug("WELCOME to Debug Messages for Refreshing Cached Properties");
            logger.debug("If you are facing a problem with refreshing of properties");
            logger.debug("  1. Save this page without the force URL param");
            logger.debug("  2. Add the &force=1 URL param and save that debug page");
            logger.debug("Contact " + contact + " if the property change you are expecting is still not reflecting on the properties page");
            logger.debug("Please note down the property having the problem and also in which property file you changed it.");
            logger.debug("Entering loadProperties");
        }
        int i, j, numKeys, len;
        boolean isModified = false;
        len = 0;
        len = sourceInfoList.size();
        StringBuilderWriter sw = null;
        PrintWriter errorWriter = null;
        long now = System.currentTimeMillis();
        SourceInfo nextSource = null;
        for (i = 0; i < len; i++) {
            InputStream is = null;
            try {
                nextSource = sourceInfoList.get(i);
                boolean loadNextResource = false;
                if (cacheMillis == 0) {
                    loadNextResource = true;
                } else if (now - nextSource.lastRefreshTime >= nextSource.refreshInterval) {
                    if (nextSource.isUrl() && nextSource.lastRefreshTime > 0) {
                        isReloadInBackgroundForUrlSources[0] = true;
                    } else {
                        loadNextResource = true;
                    }
                }
                if (loadNextResource) {
                    if (isDebug) {
                        logger.debug("Processing Source: " + nextSource.source);
                    }
                    nextSource.lastRefreshTime = now;
                    Map<String, String> map = null;
                    if (overridingPropsModified && "runtime:override".equals(nextSource.source)) {
                        overridingPropsModified = false;
                        map = overridingProps;
                        if (isDebug) {
                            logger.debug("Using Overriding Properties since it has been updated at runtime");
                        }
                    } else if ("config:ctconfig".equals(nextSource.source) /* && resource != null */) {
                        if (defaultConfigStore == null) {
                            final CachedProperties cacheProps = this;

                            String ctConfigConsulServer = consulProperties.getServer();
                            String ctConfigProfileName = consulProperties.getProfileName();

                            if (ctConfigConsulServer != null && ctConfigProfileName != null) {
                                logger.info("Init CTConfig with Vertical: shared-properties App: common");
                                logger.info("Init CTConfig with Server: " + ctConfigConsulServer + " Profile: " + ctConfigProfileName);
                                // Multithread instance not supported
                                if(consul == null) {
                                    consul = CTConfig.buildConsul(ctConfigConsulServer);
                                }
                                CTConfig.initializeCTConfig(consul, NetworkUtil.getHostName(), consulProperties.getBranchName(), Vertical.COMMON.getName(), () -> {
                                    logger.error("Received CachedProperties Refresh Event. Go to refresh Now.");
                                    cacheProps.refreshResource(0, false);
                                    logger.error("CachedProperties refresh completed");
                                }, NewRelicInsightConnector.builder().insightsUrl(INSIGHTS_URL).apiKey(NEWRELIC_API_INSERT_KEY).build(), ctConfigProfileName.split(","));
                                ConfigStore configStore = ConfigStoreUtils.getDefaultConfigStore();
                                defaultConfigStore = (DefaultConfigStore) configStore;
                            } else {
                                logger.warn("CTConfig init variables not set");
                            }
                        }

                        if (defaultConfigStore != null) {
                            map = new HashMap<String, String>();
                            Map<String, Object> consulProps = defaultConfigStore.getAllProperties();
                            for (String key : consulProps.keySet()) {
                                map.put(key, consulProps.get(key).toString());
                            }
                        }
                    } else {
                        is = openResource(nextSource, force);
                        if (isDebug) {
                            logger.debug("Got Properties Stream " + is);
                        }
                        if (is != null) {
                            Properties props = new Properties();
                            props.load(is);
                            map = (Map<String, String>) ((Map) props);
                        }
                    }

                    if (map != null) {
                        if (isDebug) {
                            logger.debug("Got Map from source: " + nextSource.source);
                        }
                        Set<String> keySet = map.keySet();
                        numKeys = keySet.size();
                        String[] oldKeys = nextSource.keys;
                        String[] oldValues = nextSource.values;
                        nextSource.keys = new String[numKeys];
                        nextSource.values = new String[numKeys];
                        j = 0;
                        for (String key : keySet) {
                            nextSource.keys[j] = key;
                            nextSource.values[j] = map.get(key);
                            j++;
                        }
                        updateKeyVals(nextSource);
                        boolean currentSourceModified = false;
                        if (j == oldKeys.length) {
                            while (j > 0) {
                                j--;
                                if (!nextSource.keys[j].equals(oldKeys[j]) || !nextSource.values[j].equals(oldValues[j])) {
                                    isModified = true;
                                    currentSourceModified = true;
                                    break;
                                }
                            }
                        } else {
                            currentSourceModified = true;
                            isModified = true;
                        }
                        if (isDebug) {
                            if (currentSourceModified) {
                                logger.debug("Got Modified Map for source " + nextSource.source);
                            } else {
                                logger.debug("Map Un-modified for source " + nextSource.source);
                            }
                        }
                    } else if (isDebug) {
                        logger.debug("Did not get Map from source: " + nextSource.source);
                    }
                } else if (isDebug) {
                    logger.debug("Ignoring Source: " + nextSource.source + " since it is not due for refresh.");
                }
                if (isDebug) {
                    logger.debug("Map LISTING for source: " + nextSource.source);
                    for (j = 0; j < nextSource.keys.length; j++) {
                        logger.debug(nextSource.keys[j] + ':' + nextSource.values[j]);
                    }
                    logger.debug("END Map LISTING for source: " + nextSource.source);
                }
            } catch (Exception e) {
                if (isDebug) {
                    logger.debug("Couldn't load Properties from: " + nextSource.source, e);
                }
                if (errorWriter == null) {
                    sw = new StringBuilderWriter();
                    errorWriter = new PrintWriter(sw);
                } else {
                    sw.clear();
                }
                logger.error("Exception is:" + e.getMessage(), e);
                String trace = sw.toString();
                if (!trace.equals(nextSource.prevTrace)) {
                    nextSource.prevTrace = trace;
                    logger.error("*****ERROR***** Couldn't load Properties from: " + nextSource.source);
                }
                throw new RuntimeException(e);
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (Exception e) {
                    is = null;
                }
            }
        }
        Map<String, String> newResource = null;
        if (isModified) {
            newResource = merge ? createMap(resource) : createMap(null);
            if (isDebug) {
                logger.debug("One or More Sources Modified. Generating New Resource. merge = " + merge + ". Initial MAP");
                for (String key : newResource.keySet()) {
                    logger.debug(key + '=' + newResource.get(key));
                }
                logger.debug("END INITIAL MAP LISTING");
            }
            for (i = 0; i < len; i++) {
                nextSource = sourceInfoList.get(i);
                numKeys = nextSource.keys.length;
                if (isDebug) {
                    logger.debug("Updating Map with source: " + nextSource.source);
                }
                for (j = 0; j < numKeys; j++) {
                    newResource.put(nextSource.keys[j], StringUtils.trim(nextSource.values[j]));
                    if (isDebug) {
                        logger.debug("Updating " + nextSource.keys[j] + '=' + StringUtils.trim(nextSource.values[j]));
                    }
                }
                if (isDebug) {
                    logger.debug("DONE Updating Map with source: " + nextSource.source);
                }
            }
            if (isDebug) {
                logger.debug("Done Loading Properties. Replacing PlaceHolders...");
            }
            // Return a copy of the properties for getting property map without placeholder replacement
            propertyMapHolder[0] = new HashMap<String, String>(newResource);
            boolean isReplaced = false;
            Set<String> keySet = newResource.keySet();
            j = 0;
            do {
                isReplaced = false;
                if (isDebug) {
                    logger.debug("Replacing PlaceHolders. Round " + j);
                }
                for (String key : keySet) {
                    String value = newResource.get(key);
                    String updatedValue = replacePlaceholders(value, newResource, null, true);
                    if (isDebug) {
                        if (value.indexOf("${") > 0) {
                            logger.debug("key = " + key + "value = " + value + " updatedValue = " + updatedValue);
                        }
                    }
                    if (value != updatedValue) {
                        isReplaced = true;
                        newResource.put(key, StringUtils.trim(updatedValue));
                    }
                }
                if (isDebug) {
                    logger.debug("DONE Replacing PlaceHolders. Round " + j);
                }
                j++;
            } while (isReplaced);
            if (isDebug) {
                logger.debug("Done Generating New Resource. Final MAP");
                for (String key : newResource.keySet()) {
                    logger.debug(key + '=' + newResource.get(key));
                }
                logger.debug("END FINAL MAP LISTING");
            }
        } else if (isDebug) {
            logger.debug("No Property Sources Modified. Retaining Same Properties");
        }
        if (isDebug) {
            logger.debug("Leaving loadProperties");
        }
        return newResource;
    }

    public void registerConsulEventListener(String key, RefreshEventListener refreshEventListener) {
        if(consul != null) {
            logger.error("Registered resource with CtConfig : "+key);
            CTConfig.watchKey(consul, key , refreshEventListener);
        }
    }

    public String[]  getArray(String property, String delimeter){
        String propVal = getPropertyValue(property, "");
        return propVal.split(delimeter);
    }

    public void registerConsulEventListener(String key, String vertical, ResourceRefreshEventListener refreshEventListener) {
        if(consul != null) {
            logger.error("Registered resource with CtConfig : "+key);
            CTConfig.watchResources(consul, refreshEventListener, vertical, key);
        }
    }

    public void registerConsulEventListener(String [] key, String vertical, ResourceRefreshEventListener refreshEventListener) {
        if(consul != null) {
            logger.error("Registering resources with CtConfig : "+key.toString());
            CTConfig.watchResources(consul, refreshEventListener, vertical, key);
        }
    }

    @Override
    protected Map<String, String> loadResource(long cacheMillis, boolean force) throws Exception {
        boolean isDebug = logger.isDebugEnabled();
        Map<String, String> newResource = null;
        Map<String, String> oldResource = resource;
        boolean isModified = updateSources(oldResource);
        int count = 3;
        boolean [] isReloadInBackgroundForUrlSources = new boolean[1];
        Map<String, String> [] propertyMapHolder = new Map[1];
        do {
            if (isDebug) {
                logger.debug("loadResource: Loading properties for count = " + count);
            }
            Map<String, String> updatedResource = loadProperties(cacheMillis, force, propertyMapHolder, isReloadInBackgroundForUrlSources);
            if (updatedResource == null) {
                if (isDebug) {
                    logger.debug("loadResource: No Property changes for count = " + count);
                }
                break;
            } else {
                newResource = updatedResource;
                if (isDebug) {
                    logger.debug("loadResource: Loaded New Property for count = " + count);
                }
            }
            isModified = updateSources(updatedResource);
            if (isDebug) {
                if (isModified) {
                    logger.debug("loadResource: Source change detected for count = " + count + ". This is due to property based sources. Will try Loading Properties again.");
                }
            }
            count--;
        } while (isModified && count > 0);

        if (propertyMapHolder[0] != null) {
            propertyMap = Collections.unmodifiableMap(propertyMapHolder[0]);
        }
        // This step does not alter the actual contents of the new map.
        // For existing keys and values use the Original String objects for the same
        // As this will make String.equals comparison with old values faster.
        if (newResource != null && oldResource != null) {
            for (String oldKey : oldResource.keySet()) {
                String newValue = newResource.get(oldKey);
                if (newValue != null) {
                    String oldValue = oldResource.get(oldKey);
                    if (newValue.equals(oldValue)) {
                        newValue = oldValue;
                    }
                    newResource.put(oldKey, StringUtils.trim(newValue));
                }
            }
        }

        if (isReloadInBackgroundForUrlSources[0]) {
            final CachedProperties r = this;
            Thread t = new Thread() {
                @Override
                public void run() {
                    r.refreshResource(0);
                }
            };
            t.start();
        }

        return newResource;
    }

    /**
     * Sets a property which overrides the others. If value is null then the overriding property is removed. The updation happens only during a refresh and not immediately.
     *
     * @param key
     *            property key
     * @param value
     *            property value
     */
    public synchronized void setProperty(String key, String value) {
        if (newPropertyKeys == null) {
            newPropertyKeys = new ArrayList<String>();
            newPropertyVals = new ArrayList<String>();
            overridingProps = new HashMap<String, String>();
        }
        int i, len;
        len = newPropertyKeys.size();
        for (i = 0; i < len; i++) {
            if (key.equals(newPropertyKeys.get(i))) {
                newPropertyVals.set(i, value);
                break;
            }
        }
        if (i == len) {
            newPropertyKeys.add(key);
            newPropertyVals.add(value);
        }
    }

    /**
     * Get the property value corresponding to a key.
     *
     * @param props
     *            Properties to use
     *
     * @param code
     *            property key.
     * @param defaultValue Default Value to return if property is not found
     * @return property value.
     */
    public static String getPropertyValue(Map<String, String> props, String code, String defaultValue) {
        String value = null;
        if (props != null) {
            value = props.get(code);
        }
        if (value == null) {
            value = defaultValue;
        }

        return value;
    }

    /**
     * Get the property value corresponding to a key.
     *
     * @param code
     *            property key.
     * @param defaultValue Default Value to return if property is not found
     * @return property value.
     */
    public String getPropertyValue(String code, String defaultValue) {
        return getPropertyValue(getResource(), code, defaultValue);
    }

    /**
     * Get the property value corresponding to a key.
     *
     * @param code
     *            property key.
     *
     * @return property value.
     */
    public String getPropertyValue(String code) {
        return getPropertyValue(getResource(), code, null);
    }

    /**
     * Returns a Long Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param props
     *            Properties to use
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @return property value
     */
    public static double getDoublePropertyValue(Map<String, String> props, String code, double defaultValue) {
        double propertyValue = defaultValue;
        String valueInPropertiesFile = getPropertyValue(props, code, null);
        if (valueInPropertiesFile != null) {
            try {
                propertyValue = Double.parseDouble(valueInPropertiesFile);
            } catch (NumberFormatException e) {
                if (logger.isInfoEnabled()) {
                    logger.info("ERROR: " + "found non numeric property in method getLongPropertyValue()." + "SUSPECTED CAUSE: " + "Reloadable property " + code + " not correctly set."
                            + "Using default value to proceed. default Value =" + defaultValue + ".", e);
                }
                propertyValue = defaultValue; // use default value
            }
        }
        return propertyValue;
    }

    /**
     * Returns a Long Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @return property value
     */
    public double getDoublePropertyValue(String code, double defaultValue) {
        return getDoublePropertyValue(getResource(), code, defaultValue);
    }

    /**
     * Returns a Long Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param props
     *            Properties to use
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @return property value
     */
    public static long getLongPropertyValue(Map<String, String> props, String code, long defaultValue) {
        long propertyValue = defaultValue;
        String valueInPropertiesFile = getPropertyValue(props, code, null);
        if (valueInPropertiesFile != null) {
            try {
                propertyValue = Long.parseLong(valueInPropertiesFile);
            } catch (NumberFormatException e) {
                if (logger.isInfoEnabled()) {
                    logger.info("ERROR: " + "found non numeric property in method getLongPropertyValue()." + "SUSPECTED CAUSE: " + "Reloadable property " + code + " not correctly set."
                            + "Using default value to proceed. default Value =" + defaultValue + ".", e);
                }
                propertyValue = defaultValue; // use default value
            }
        }
        return propertyValue;
    }

    /**
     * Returns a Long Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @return property value
     */
    public long getLongPropertyValue(String code, long defaultValue) {
        return getLongPropertyValue(getResource(), code, defaultValue);
    }

    /**
     * Returns a Integer Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param props
     *            Properties to use
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @return property value
     */
    public static int getIntPropertyValue(Map<String, String> props, String code, int defaultValue) {
        int propertyValue = defaultValue;
        String valueInPropertiesFile = getPropertyValue(props, code, null);
        if (valueInPropertiesFile != null) {
            try {
                propertyValue = Integer.parseInt(valueInPropertiesFile);
            } catch (NumberFormatException e) {
                if (logger.isInfoEnabled()) {
                    logger.info("ERROR: " + "found non numeric property in method getIntPropertyValue()." + "SUSPECTED CAUSE: " + "Reloadable property " + code + " not correctly set."
                            + "Using default value to proceed. default Value =" + defaultValue + ".", e);
                }
                propertyValue = defaultValue; // use default value
            }
        }
        return propertyValue;
    }

    /**
     * Returns a Integer Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @return property value
     */
    public int getIntPropertyValue(String code, int defaultValue) {
        return getIntPropertyValue(getResource(), code, defaultValue);
    }

    /**
     * Returns a Boolean Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param props
     *            Properties to use
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @return property value
     */
    public static boolean getBooleanPropertyValue(Map<String, String> props, String code, boolean defaultValue) {
        boolean propertyValue = defaultValue;
        String valueInPropertiesFile = getPropertyValue(props, code, null);
        if (valueInPropertiesFile != null) {
            try {
                propertyValue = Boolean.parseBoolean(valueInPropertiesFile);
            } catch (Exception e) {
                if (logger.isInfoEnabled()) {
                    logger.info("ERROR: " + "found non boolean property in method getBooleanPropertyValue()." + "SUSPECTED CAUSE: " + "Reloadable property " + code + " not correctly set."
                            + "Using default value to proceed. default Value =" + defaultValue + ".", e);
                }
                propertyValue = defaultValue; // use default value
            }
        }
        return propertyValue;
    }

    /**
     * Returns a Boolean Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @return property value
     */
    public boolean getBooleanPropertyValue(String code, boolean defaultValue) {
        return getBooleanPropertyValue(getResource(), code, defaultValue);
    }

    /**
     * Returns a Enum Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param props
     *            Properties to use
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @param enumClass
     *            Class of the Enum
     * @param <K> Type of Enum
     * @return property value
     */
    public static <K extends Enum<K>> K getEnumPropertyValue(Map<String, String> props, String code, K defaultValue, Class<K> enumClass) {
        K propertyValue = defaultValue;
        String valueInPropertiesFile = getPropertyValue(props, code, null);
        if (valueInPropertiesFile != null) {
            try {
                propertyValue = K.valueOf(enumClass, valueInPropertiesFile);
            } catch (Exception e) {
                if (logger.isInfoEnabled()) {
                    logger.info("ERROR: " + "found non enum property in method getEnumPropertyValue()." + "SUSPECTED CAUSE: " + "Reloadable property " + code + " not correctly set."
                            + "Using default value to proceed. default Value =" + defaultValue + ".", e);
                }
                propertyValue = defaultValue; // use default value
            }
        }
        return propertyValue;
    }

    /**
     * Returns a Enum Property Value. Uses Default Value as a fallback in which case the failure is logged.
     *
     * @param code
     *            property Key.
     * @param defaultValue
     *            Default value to be used in case property not present or is not a number
     * @param enumClass
     *            Class of the Enum
     * @param <K> Enum Type
     * @return property value
     */
    public <K extends Enum<K>> K getEnumPropertyValue(String code, K defaultValue, Class<K> enumClass) {
        return getEnumPropertyValue(getResource(), code, defaultValue, enumClass);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.cleartrip.common.util.CachedResource#getResourceDescription()
     */
    @Override
    public String getResourceDescription() {
        StringBuilder sb = new StringBuilder(512);
        Map<String, String> props = getResource();
        if (props != null) {
            for (String key : props.keySet()) {
                sb.append(key).append('=').append(props.get(key)).append('\n');
            }
        }
        return sb.toString();
    }

    @Override
    protected String computeDigest() {
        String digest = null;
        Map<String, String> r = resource;
        if (r != null) {
            try {
                byte[] equalToBytes = "=".getBytes("UTF-8");
                byte[] newLineBytes = "\n".getBytes();
                Set<String> keys = new TreeSet<String>();
                keys.addAll(r.keySet());
                keys.removeAll(DIGEST_EXCLUDED_PROPERTIES);
                MessageDigest md = MessageDigest.getInstance("SHA-1");
                for (String key : keys) {
                    md.update(key.getBytes("UTF-8"));
                    md.update(equalToBytes);
                    String val = r.get(key);
                    if (val != null) {
                        md.update(val.getBytes("UTF-8"));
                    }
                    md.update(newLineBytes);
                }
                byte[] hashedArr = md.digest();
                byte[] encodedBytes = Base64.encodeBase64(hashedArr);
                digest = new String(encodedBytes);
            } catch (Exception e) {
                logger.error("Error in computing cachedProperties digest", e);
            }
        }

        return digest;
    }

    public Map<String, String> getPropertyMap() {
        return propertyMap;
    }

    public CtConfigConsuleProperties getConsulProperties() {
        return consulProperties;
    }

    class ConfigChangeRequest{
        @JsonProperty("commitMessage")
        private String commitMessage;
        @JsonProperty("property")
        private String property;
        @JsonProperty("value")
        private String value;

        public ConfigChangeRequest(String commitMessage, String property, String value) {
            this.commitMessage = commitMessage;
            this.property = property;
            this.value = value;
        }

        public String getCommitMessage() {
            return commitMessage;
        }

        public void setCommitMessage(String commitMessage) {
            this.commitMessage = commitMessage;
        }

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class CtConfigRequest {
        @JsonProperty("fileName")
        private String fileName;
        @JsonProperty("fileContent")
        private String fileContent;

        public CtConfigRequest(String fileName, String fileContent) {
            this.fileName = fileName;
            this.fileContent = fileContent;
        }

        public String getFileContent() {
            return fileContent;
        }

        public void setFileContent(String fileContent) {
            this.fileContent = fileContent;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }
    }

    public void updatePropertyValue(String property, String value, String commitMessage) {
        Runnable runnable = () -> {
            try {
                String url = getPropertyValue("ct.service.ct-config.update.property.url");
                String token = getPropertyValue("ct.service.ct-config.update.property.token");
                String timeout = getPropertyValue("ct.service.ct-config.update.property.timeout","30000");
                RestClient restClient = DefaultRestClientImpl.getInstance();
                ConfigChangeRequest configChangeRequest = new ConfigChangeRequest(commitMessage, property, value);
                String requestBody = APIUtil.serializeObjectToJson(configChangeRequest);
                RestRequest request = new RestRequest(url);
                request.setContentType("application/json");
                request.setPayload(requestBody);
                request.setTimeout(Integer.parseInt(timeout));
                Map<String, String> headers = new HashMap<>();
                headers.put("authorization", token);
                request.setHeaders(headers);
                restClient.put(request);
            } catch (Exception e) {
                    logger.error("Error while changing ct-config property ", e);
                }

        };

        try {
            CompletableFuture<Void> future = CompletableFuture.runAsync(runnable);
        } catch (Exception e) {
            logger.error("Error while changing ct-config property ", e);
        }

    }

    public void updateCtConfigFileContent(String fileName, String fileContent) {
            CompletableFuture<Void> future = CompletableFuture.runAsync(()->{
                try {
                    logger.error("Updating CT config resource : "+ fileName);
                    logger.debug("Content \n"+fileContent);

                    String url = getPropertyValue("ct.service.ct-config.update.property.url.v1");
                    String token = getPropertyValue("ct.service.ct-config.update.property.token");

                    CtConfigRequest ctConfigRequest = new CtConfigRequest(fileName, fileContent);
                    String requestBody = APIUtil.serializeObjectToJson(ctConfigRequest);

                    RestClient restClient = DefaultRestClientImpl.getInstance();
                    RestRequest request = new RestRequest(url);
                    request.setContentType("application/json");
                    request.setPayload(requestBody);
                    request.setTimeout(15000);
                    Map<String, String> headers = new HashMap<>();
                    headers.put("authorization", token);
                    request.setHeaders(headers);
                    RestResponse response = restClient.put(request);

                    if(response.getStatus()!=202) {
                        logger.error("update failed for filename : "+fileName);
                    }
                } catch (Exception e) {
                    logger.error("Error while changing ct-config property : "+fileName, e);
                }
            });
    }




}
