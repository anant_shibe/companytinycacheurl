package com.companyApiTinyCache.companyConfigCache.service.common.logging;

public class LogThreadLocal {
    private static final ThreadLocal<LogData> threadLocal = new InheritableThreadLocal<LogData>();

    public static LogData getLogData() {
        if (getThreadLocal() == null)
            return null;
        return getThreadLocal().get();
    }

    public static void setLogData(LogData logData) {
        threadLocal.set(logData);
    }

    public static ThreadLocal<LogData> getThreadLocal() {
        return threadLocal;
    }
}
