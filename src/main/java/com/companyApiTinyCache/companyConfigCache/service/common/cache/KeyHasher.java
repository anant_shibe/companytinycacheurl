package com.companyApiTinyCache.companyConfigCache.service.common.cache;

/**
 * Hashing interface used for Sharding across servers. For Redis the API provided sharding is not used as it does not support apis like mget and scripting.
 * It is assumed that the KeyHasher is initialized with an array of Cache's of different ConnectionStrings.
 * The getCacheForKey returns the cache to which the given key maps to based on the Consistent Hashing Algorithm implementation.
 *
 * @author suresh
 */
public interface KeyHasher {
    Cache getCacheForKey(String key);
}
