package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Rule.
 */
public class Rule extends LinkedHashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	/**
	 * VariableValues.
	 */
	public static class VariableValues extends HashMap<String, Object> {
		private static final long serialVersionUID = 1L;
	}

	public static final String CONDITION = "condition";
	public static final String CONSEQUENCE = "consequence";
	public static final String COUNTRY = "country";
	public static final String ID = "id";
	public static final String MISC = "misc";
	public static final String RULE_TYPE = "rule_type";
	public static final String SOURCE = "source";
	public static final String DISPLAY_TEXT = "display_text";
}
