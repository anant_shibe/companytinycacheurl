package com.companyApiTinyCache.companyConfigCache.service.common.connector;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.data.*;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants.UserAPIConnectorTimeouts;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants.UserAPITemplate;
import com.companyApiTinyCache.companyConfigCache.service.common.data.holder.UserProfileHolder;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.ConnectorStats;
import com.companyApiTinyCache.companyConfigCache.service.common.security.CTSecurityHeaders;
import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestUtil.Char_Encoding_Type_Enum;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.ApiType;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;

/**
 *
 */
@ClassExcludeCoverage
public class UserAPIConnector extends APIConnectorImpl {

	private static final String CT_TM_DISABLE_MAIL = "ct.common.tm.disable.mail";

	private static final String X_CT_NO_EMAIL = "X_CT_NO_EMAIL";

	private static final String X_CT_Source_Type = "X-CT-Source-Type";

	private static final String X_CT_REG_DOMAIN_NAME = "x-CT-Reg-Domain-Name";

	private static final String PEOPLE_ID_PLACEHOLDER_REGEX = "\\$\\{people-id}";

	private CachedProperties commonCachedProperties;

	protected CachedVms<UserAPITemplate> cachedVms;

	private static final String PEOPLE_RETRIEVE_URL_PROPERTY = "ct.services.api.people.retrieve.url";

	private static final String PEOPLE_SEARCH_URL_PROPERTY = "ct.services.api.people.search.url";

	private static final Log logger = LogFactory.getLog(UserAPIConnector.class);

	private Integer timeout = UserAPIConnectorTimeouts.COMMON_TIMEOUT.getTimeout();
	private Integer peopleTimeout = UserAPIConnectorTimeouts.PEOPLE.getTimeout();
	/**
	 * Returns the TM Mail Option. This is functionally a configuration based property which tells whether the Emailing a new user option is enabled / disabled.
	 * @return tmMailOption
	 * @author Ulhas
	 */
	private String getTransactionManagerMailOption(String source) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = JsonUtil.getObjectMapper();
		String mapString = commonCachedProperties.getPropertyValue(CT_TM_DISABLE_MAIL);
		try {
			map = (Map<String, String>) mapper.readValue(mapString, Map.class);
			if (StringUtils.isNotBlank(source) && map.containsKey(source)) {
				return map.get(source);
			} else {
				return map.get("default");
			}

		} catch (Exception e) {
			logger.equals("Error in getTransactionManagerMailOption");
		}
		return "true";
	}


	/**
	 * Searches the UserProfile for the specified username.
	 * @param username username
	 * @return UserProfile
	 * @throws IOException
	 * @author Ulhas
	 */
	@Deprecated
	public UserProfile fetchUserProfile(String username) {
		UserProfile userProfile = getUserProfileFromCondition("username=" + username, true, true);
		return userProfile;
	}

	/**
	 * Searches the UserProfile for the specified Api-Key.
	 * @param apiKey apiKey
	 * @return UserProfile
	 * @author Ulhas
	 */
	@Deprecated
	public UserProfile getUserProfileFromApiKey(String apiKey) {
		UserProfile userProfile = getUserProfileFromCondition("api_key=" + apiKey);
		return userProfile;
	}

	/**
	 * Searches the UserProfile for the specified User-Name.
	 * @param userName userName
	 * @return UserProfile
	 * @author Ulhas
	 */
	@Deprecated
	public UserProfile getUserProfileFromUserName(String userName) {
		UserProfile userProfile = getUserProfileFromCondition("username=" + userName);
		return userProfile;
	}

	/**
	 * Searches the UserProfile for the specified User-Name.
	 * @param userName userName
	 * @param sourceType SourceType
	 * @param sourceId - used for logging at accounts' end
	 * @return UserProfile
	 */
	@MethodExcludeCoverage
	@Deprecated
	public UserProfile getUserProfileFromUserName(String userName, SourceType sourceType, String sourceId) {
		UserProfile userProfile = getUserProfileFromCondition("username=" + userName, sourceType, sourceId);
		return userProfile;
	}

	/**
	 * Searches the UserProfile for the specified condition. The Condition needs to be supported by the API. The method is declared private so that it is not available publicly outside this Class. If
	 * a new condition based retrieval is needed, then it should be available at the api. A new method will be introduced with the new condition, which will internally call this private method.
	 * @param condition condition
	 * @return UserProfile
	 * @author Ulhas
	 */
	@Deprecated
	private UserProfile getUserProfileFromCondition(String condition) {
		List<String> dontLog = new ArrayList<String>();
		dontLog.add("404"); // no need to log the error when the user is not
		// found in the central schema

		String urlLocation = commonCachedProperties.getPropertyValue(PEOPLE_SEARCH_URL_PROPERTY);

		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

		RestResponse restResponse = null;
		UserProfile userProfile = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		try {
			// Begin Logging
			if (connectorStat != null) {
				connectorStat.addNewApiRequest(ApiType.OTHER, null, "RETRIEVE_USER_PROFILE", "", urlLocation, condition);
			}
			// End Logging

			logger.info("Get User Profile from condition(" + condition + ") Request : " + urlLocation);

			restResponse = RestUtil.get(urlLocation, condition, requestHeaders, responseHeaders, dontLog, Char_Encoding_Type_Enum.UTF);
			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}
			logger.debug(" Response : " + restResponse.getMessage());
			if (GenUtil.hasSuccessCode(restResponse)) {
				UserProfileHolder responseHolder = new UserProfileHolder();
				responseHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), responseHolder);
				userProfile = responseHolder.getUserProfile();
			}
		} catch (Exception e) {
			logger.error("Exception during the API call to Search User Profile : ", e);
			isException = true;
			responseCode = 500;
			responseMessage =  "Exception during the API call to Search User Profile : " +  e.getMessage();
		} finally {
			// Begin Logging
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "RETRIEVE_USER_PROFILE", responseCode, responseMessage, !isException);
			}
			// End Logging
		}

		return userProfile;
	}



	/**
	 * Searches the UserProfile for the specified condition. The Condition needs to be supported by the API. The method is declared private so that it is not available publicly outside this Class. If
	 * a new condition based retrieval is needed, then it should be available at the api. A new method will be introduced with the new condition, which will internally call this private method.
	 * @param condition
	 * @param sourceId - used for logging at accounts' end
	 * @return UserProfile
	 */
	@MethodExcludeCoverage
	private UserProfile getUserProfileFromCondition(String condition, SourceType sourceType, String sourceId) {
		List<String> dontLog = new ArrayList<String>();
		dontLog.add("404"); // no need to log the error when the user is not
		// found in the central schema

		String urlLocation = commonCachedProperties.getPropertyValue(PEOPLE_SEARCH_URL_PROPERTY);

		if (SourceType.WL.equals(sourceType)) {
			urlLocation = "ct.services.api.wl.people.search.url";
		}

		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

		RestResponse restResponse = null;
		UserProfile userProfile = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();

		try {
			if (connectorStat != null) {
				connectorStat.addNewApiRequest(ApiType.OTHER, null, "RETRIEVE_USER_PROFILE", "", urlLocation, condition);
			}
			// End Logging

			logger.info("Get User Profile from condition(" + condition + ") Request : " + urlLocation);

			if (StringUtils.isNotBlank(sourceId)) {
				condition += "&source=" + sourceId;
			}

			restResponse = RestUtil.get(urlLocation, condition, requestHeaders, responseHeaders, dontLog, Char_Encoding_Type_Enum.UTF);

			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}
			logger.debug(" Response : " + restResponse.getMessage());
			if (GenUtil.hasSuccessCode(restResponse)) {
				UserProfileHolder responseHolder = new UserProfileHolder();
				responseHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), responseHolder);
				userProfile = responseHolder.getUserProfile();
			}
		} catch (Exception e) {
			logger.error("Exception during the API call to Search User Profile : ", e);
			isException = true;
			responseCode = 500;
			responseMessage = "Exception during the API call to Search User Profile : " + e.getMessage();
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "RETRIEVE_USER_PROFILE", responseCode, responseMessage, !isException);
			}
		}

		return userProfile;
	}

	/**
	 * Searches the UserProfile for the specified condition. The Condition needs to be supported by the API. The method is declared private so that it is not available publicly outside this Class. If
	 * a new condition based retrieval is needed, then it should be available at the api. A new method will be introduced with the new condition, which will internally call this private method.
	 * @param condition
	 * @return UserProfile
	 * @author Ulhas
	 */
	private UserProfile getUserProfileFromCondition(String condition, boolean isTravelersNeeded, boolean isCardDetailsNeeded) {
		List<String> dontLog = new ArrayList<String>();
		dontLog.add("404"); // no need to log the error when the user is not
		// found in the central schema

		String urlLocation = commonCachedProperties.getPropertyValue(PEOPLE_SEARCH_URL_PROPERTY);

		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

		RestResponse restResponse = null;
		UserProfile userProfile = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		try {

			if (isTravelersNeeded) {
				if (condition == null || "".equals(condition)) {
					condition = "travellers=true";
				} else {
					condition += "&travellers=true";
				}
			}

			if (isCardDetailsNeeded) {
				if (condition == null || "".equals(condition)) {
					condition = "card_details=true";
				} else {
					condition += "&card_details=true";
				}
			}

			if (connectorStat != null) {
				connectorStat.addNewApiRequest(ApiType.OTHER, null, "RETRIEVE_USER_PROFILE", "", urlLocation, condition);
			}
			// End Logging

			logger.info("Get User Profile from condition(" + condition + ") Request : " + urlLocation);

			restResponse = RestUtil.get(urlLocation, condition, requestHeaders, responseHeaders, dontLog, Char_Encoding_Type_Enum.UTF);
			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}

			logger.debug(" Response : " + restResponse.getMessage());

			if (GenUtil.hasSuccessCode(restResponse)) {
				UserProfileHolder responseHolder = new UserProfileHolder();
				responseHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), responseHolder);
				userProfile = responseHolder.getUserProfile();
			}
		} catch (Exception e) {
			logger.error("Exception during the API call to Search User Profile : ", e);
			isException = true;
			responseCode = 500;
			responseMessage =  "Exception during the API call to Search User Profile : " + e.getMessage();
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "RETRIEVE_USER_PROFILE", responseCode, responseMessage, !isException);
			}
		}

		return userProfile;
	}

	/**
	 * Retrieves the UserProfile for the specified People-Id.
	 * @param peopleId peopleId
	 * @return UserProfile
	 * @author Ulhas
	 */
	@Deprecated
	public UserProfile getUserProfile(int peopleId) {
		return getPeopleProfile(peopleId, false, false, null);
	}

	/**
	 * Retrieves the UserProfile for the specified People-Id.
	 * @param peopleId PeopleId
	 * @param sourceType - used to distinguish the property to fetch the URL to hit
	 * @param sourceId - used for logging at accounts' end
	 * @return UserProfile
	 */
	@MethodExcludeCoverage
	@Deprecated
	public UserProfile getUserProfile(int peopleId, SourceType sourceType, String sourceId) {
		return getPeopleProfile(peopleId, false, false, null, sourceType, sourceId);
	}

	/**
	 * Retrieves the UserProfile along with the Travellers that belong to that User.
	 * @param peopleId peopleId
	 * @return UserProfile
	 * @author Ulhas
	 */
	@Deprecated
	public UserProfile getUserProfileWithTravellersInfo(int peopleId) {
		return getPeopleProfile(peopleId, true, false, null);
	}

	/**
	 * Retrieves the UserProfile along with the Travellers that belong to that User.
	 * @param peopleId peopleId
	 * @param sourceType - used to distinguish the property to fetch the URL to hit
	 * @param sourceId - used for logging at accounts' end
	 * @return UserProfile
	 */
	@MethodExcludeCoverage
	@Deprecated
	public UserProfile getUserProfileWithTravellersInfo(int peopleId, SourceType sourceType, String sourceId) {
		return getPeopleProfile(peopleId, true, false, null, sourceType, sourceId);
	}

	/**
	 * Retrieves the UserProfile along with the Travellers that belong to that User.
	 * @param peopleId peopleId
	 * @param request HttpServletRequest
	 * @return UserProfile
	 * @author Ulhas
	 */
	@Deprecated
	public UserProfile getUserProfileWithTravellersInfoAndCardDetails(int peopleId, HttpServletRequest request) {
		Cookie authCookieNew = getNewAuthCookie(request);
		List<Cookie> cookies = null;
		if (authCookieNew != null) {
			cookies = new ArrayList<Cookie>();
			cookies.add(authCookieNew);
		}

		return getPeopleProfile(peopleId, true, true, cookies);
	}

	/**
	 * @param peopleId peopleId
	 * @param request HttpServletRequest
	 * @param sourceType - used to distinguish the property to fetch the URL to hit
	 * @param sourceId - used for logging at accounts' end
	 * @return UserProfile
	 */
	@MethodExcludeCoverage
	@Deprecated
	public UserProfile getUserProfileWithTravellersInfoAndCardDetails(int peopleId, HttpServletRequest request, SourceType sourceType, String sourceId) {
		CTSecurityHeaders securityHeaders = new CTSecurityHeaders(request, sourceType);
		Cookie authCookieNew = securityHeaders.getCtAuthCookie(commonCachedProperties);
		List<Cookie> cookies = null;
		if (authCookieNew != null) {
			cookies = new ArrayList<Cookie>();
			cookies.add(authCookieNew);
		}

		return getPeopleProfile(peopleId, true, true, cookies, sourceType, sourceId);
	}

	/**
	 * Retrieves the UserProfile for the specified People-Id. The travellers that belong to the User are retrieved conditionally controlled by the parameter isTravelersNeeded
	 * @param peopleId
	 * @param isTravelersNeeded
	 * @return UserProfile
	 * @author Ulhas
	 */
	@Deprecated
	private UserProfile getPeopleProfile(int peopleId, boolean isTravelersNeeded, boolean isCardDetailsNeeded, List<Cookie> cookies) {

		RestResponse restResponse = null;
		String urlLocation = commonCachedProperties.getPropertyValue(PEOPLE_RETRIEVE_URL_PROPERTY);

		urlLocation = urlLocation.replaceAll(PEOPLE_ID_PLACEHOLDER_REGEX, String.valueOf(peopleId));
		// Begin Logging
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		if (connectorStat != null) {
			connectorStat.addNewApiRequest(ApiType.OTHER, null, "GET_USER_PROFILE", "", urlLocation, "");
		}
		// End Logging

		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

		String queryString = "";
		if (isTravelersNeeded) {
			queryString = "travellers=true";
		}

		if (isCardDetailsNeeded) {
			if ("".equals(queryString)) {
				queryString += "card_details=true";
			} else {
				queryString += "&card_details=true";
			}
		}

		if (cookies != null && cookies.size() > 0) {
			StringBuilder cookieStr = new StringBuilder();
			for (int i = 0; i < cookies.size(); i++) {
				Cookie cookie = cookies.get(i);
				cookieStr.append(cookie.getName()).append("=").append(cookie.getValue()).append("; ");
			}
			cookieStr.setLength(cookieStr.length() - 2); // this is to remove the last 2 characters - "; "
			requestHeaders.put("Cookie", cookieStr.toString());
		}

		List<String> dontLog = new ArrayList<String>();
		dontLog.add("404");

		UserProfile userProfile = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;

		try {

			logger.info("Get User Profile Request :" + urlLocation);

			restResponse = RestUtil.get(urlLocation, queryString, requestHeaders, responseHeaders, dontLog, Char_Encoding_Type_Enum.UTF,timeout);
			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}
			logger.info(" Response : " + restResponse.getMessage());
			if (restResponse.getCode() == 200) {
				UserProfileHolder userProfileHolder = new UserProfileHolder();
				userProfileHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), userProfileHolder);
				userProfile = userProfileHolder.getUserProfile();
				if (userProfile == null) {
					userProfile = userProfileHolder.getTravelerProfile();
				}
			} else {
				logger.warn("Error getting user profile from API for ID " + peopleId + ". Error code: " + restResponse.getCode());
				userProfile = null;
			}
		} catch (Exception e) {
			logger.warn("Exception thrown during the GET User Profile call : ", e);
			isException = true;
			responseCode = 500;
			responseMessage = "Exception thrown during the GET User Profile call : " + e.getMessage();
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "GET_USER_PROFILE", responseCode, responseMessage, !isException);
			}
		}

		return userProfile;
	}

	/**
	 * Retrieves the UserProfile for the specified People-Id. The travellers that belong to the User are retrieved conditionally controlled by the parameter isTravelersNeeded
	 * @param sourceType - used to distinguish the property to fetch the URL to hit
	 * @param sourceId - used for logging at accounts' end
	 * @return UserProfile
	 */
	/**
	 * @param peopleId - id
	 * @param isTravelersNeeded boolean
	 * @param isCardDetailsNeeded - boolean
	 * @param cookies - cookies
	 * @param sourceType - sourceType
	 * @param sourceId - sourceId
	 * @return userProfile
	 */
	@MethodExcludeCoverage
	public UserProfile getPeopleProfile(int peopleId, boolean isTravelersNeeded, boolean isCardDetailsNeeded, List<Cookie> cookies, SourceType sourceType, String sourceId) {

		RestResponse restResponse = null;
		String urlLocation;
		String queryString = "";

		if (StringUtils.isNotBlank(sourceType.toString())) {
			queryString = "source=" + sourceType.toString();
		}
		if (sourceType == SourceType.CORP || sourceType == SourceType.AGENCY || sourceType == SourceType.WL) {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api." + sourceType.toString().toLowerCase() + ".people.retrieve.url");
		} else {
			urlLocation = commonCachedProperties.getPropertyValue(PEOPLE_RETRIEVE_URL_PROPERTY);
		}

		urlLocation = urlLocation.replaceAll(PEOPLE_ID_PLACEHOLDER_REGEX, String.valueOf(peopleId));
		// Begin Logging
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		if (connectorStat != null) {
			connectorStat.addNewApiRequest(ApiType.OTHER, null, "GET_USER_PROFILE", "", urlLocation, "");
		}
		// End Logging

		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

		if (isTravelersNeeded) {
			if (StringUtils.isNotBlank(queryString)) {
				queryString += "&travellers=true";
			} else {
				queryString = "travellers=true";
			}
		}

		if (isCardDetailsNeeded) {
			if ("".equals(queryString)) {
				queryString += "card_details=true";
			} else {
				queryString += "&card_details=true";
			}
		}

		if (StringUtils.isNotBlank(sourceId)) {
			if ("".equals(queryString)) {
				queryString += "caller=" + sourceId;
			} else {
				queryString += "&caller=" + sourceId;
			}
		}

		if (cookies != null && cookies.size() > 0) {
			StringBuilder cookieStr = new StringBuilder();
			for (int i = 0; i < cookies.size(); i++) {
				Cookie cookie = cookies.get(i);
				cookieStr.append(cookie.getName()).append("=").append(cookie.getValue()).append("; ");
			}
			cookieStr.setLength(cookieStr.length() - 2); // this is to remove the last 2 characters - "; "
			requestHeaders.put("Cookie", cookieStr.toString());
		}

		requestHeaders.putAll(CTSecurityHeaders.getAuthKeyForSourceType(commonCachedProperties, sourceType));

		List<String> dontLog = new ArrayList<String>();
		dontLog.add("404");

		UserProfile userProfile = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;
		try {

			logger.info("Get User Profile Request :" + urlLocation);

			restResponse = RestUtil.get(urlLocation, queryString, requestHeaders, responseHeaders, dontLog, Char_Encoding_Type_Enum.UTF,timeout);

			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}

			logger.info(" Response : " + restResponse.getMessage());
			if (restResponse.getCode() == 200) {
				UserProfileHolder userProfileHolder = new UserProfileHolder();
				userProfileHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), userProfileHolder);
				userProfile = userProfileHolder.getUserProfile();
				if (userProfile == null) {
					userProfile = userProfileHolder.getTravelerProfile();
				}
			} else {
				logger.warn("Error getting user profile from API for ID " + peopleId + ". Error code: " + restResponse.getCode());
				userProfile = null;
			}
		} catch (Exception e) {
			logger.warn("Exception thrown during the GET User Profile call : ", e);
			isException = true;
			responseCode =  500;
			responseMessage = "Exception thrown during the GET User Profile call : " + e.getMessage();
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "GET_USER_PROFILE", responseCode, responseMessage, !isException);
			}
		}

		return userProfile;
	}

	private UserProfile userRegister(UserProfile userProfile, boolean defaultEmailOption, boolean isTravellersNeeded, String domainName, String source, String caller) {

		UserProfileHolder userProfileHolder = new UserProfileHolder();
		userProfileHolder.setUserProfile(userProfile);
		String urlLocation = "";
		if (StringUtils.isNotBlank(source) && isB2BCall(source)) {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api." + source.toLowerCase() + ".user.register.url");
		} else {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api.user.register.url");
		}
		urlLocation += "?caller=" + caller;
		String jsonPostMessage = APIUtil.serializeObjectToJson(userProfileHolder);
		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");

		if (defaultEmailOption) {
			String transactionManagerMailOption = getTransactionManagerMailOption(source);
			if (StringUtils.isNotEmpty(transactionManagerMailOption)) {
				requestHeaders.put(X_CT_NO_EMAIL, transactionManagerMailOption);
			}
		} else {
			requestHeaders.put(X_CT_NO_EMAIL, "false");
		}

		if (StringUtils.isNotEmpty(source)) {
			requestHeaders.put(X_CT_Source_Type, source);
		}

		if (StringUtils.isNotBlank(domainName)) {
			requestHeaders.put(X_CT_REG_DOMAIN_NAME, domainName);
		}

		if (isTravellersNeeded) {
			urlLocation += "&travellers=true";
		}

		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();
		FaultDetail faultDetail = null;
		UserProfile responseUserProfile = null;
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		RestResponse restResponse = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;

		try {
			// Begin Logging

			if (connectorStat != null) {
				connectorStat.addNewApiRequest(ApiType.OTHER, null, "USER_REGISTER", "", urlLocation, jsonPostMessage);
			}
			// End Logging

			logger.info("Create User Request : " + jsonPostMessage);

			int resgisterTimeout = commonCachedProperties.getIntPropertyValue(UserAPIConnectorTimeouts.USER_REGISTER.getProperty(), UserAPIConnectorTimeouts.USER_REGISTER.getTimeout());
			restResponse = RestUtil.post(urlLocation, jsonPostMessage, "", "text/json", requestHeaders, responseHeaders, "POST", resgisterTimeout);

			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}

			logger.info("User Register Response : " + restResponse.getCode() + " : " + restResponse.getMessage());

			if (GenUtil.hasSuccessCode(restResponse)) {
				userProfileHolder = new UserProfileHolder();
				userProfileHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), userProfileHolder);
				responseUserProfile = userProfileHolder.getUserProfile();

				// if 201 --- its a first time user
				if (restResponse.getCode() == 201) {
					responseUserProfile.setFirstTimeUser(true);
				}
			} else if (restResponse.getCode() == 409) {
				// if 409 --- its a conflict and the user already exists
				try {
					faultDetail = restResponse.getFaultDetailFromJson();
					responseUserProfile = new UserProfile();
					responseUserProfile.setFaultDetail(faultDetail);
					responseUserProfile.setPeopleId(0);
				} catch (Exception e) {
					logger.error("**** ERROR WHILE GETTING FAULT DETAIL FROM REST RESPONSE *** ", e);
				}
			}
		} catch (Exception e) {
			logger.error("Exception returned in the API Call to Create User :", e);
			isException = true;
			responseCode = 500;
			responseMessage = "Exception returned in the API Call to Create User " + e.getMessage();
		} finally {
			// Begin Logging
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "USER_REGISTER", responseCode, responseMessage, !isException);
			}
			// End Logging
		}

		return responseUserProfile;
	}

	/**
	 * @param userLoginProfile UserLoginProfile
	 * @param source Source
	 * @param caller Caller
	 * @return UserProfile
	 */
	public UserProfile userRegister(UserLoginProfile userLoginProfile, String source, String caller) {

		UserProfileHolder userProfileHolder = new UserProfileHolder();
		UserProfile userProfile = new UserProfile();
		userProfile.setUsername(userLoginProfile.getUserName());
		userProfile.setSourceOfRegistration(userLoginProfile.getProduct());
		userProfile.setMailerSubscription(userLoginProfile.getMailerSubscribeAction());
		userProfile.setDevice(userLoginProfile.getDevice());

		if (userLoginProfile.getHost() != null) {
			userProfile.setDomain(userLoginProfile.getHost());
		}

		Map<Integer, CompanyDetail> companyDetails = new LinkedHashMap<Integer, CompanyDetail>(1);
		CompanyDetail companyDetail = new CompanyDetail();
		companyDetail.setCompanyId(userLoginProfile.getCompanyId());
		List<Role> roles = new ArrayList<Role>();
		Role role = new Role();
		role.setId(userLoginProfile.getRoleId());
		roles.add(role);
		companyDetail.setRolesFromList(roles);

		companyDetails.put(userLoginProfile.getCompanyId(), companyDetail);
		userProfile.setCompanyDetails(companyDetails);

		userProfileHolder.setUserProfile(userProfile);

		return userRegister(userProfile, userLoginProfile.isEnableEmail(), userLoginProfile.isTravellersNeeded(), userLoginProfile.getDomainName(), userLoginProfile.getProduct(), caller);
	}

	/**
	 * Registers the user against a company using the companyId of the company.
	 * @param username UserName
	 * @param companyId CompanyId
	 * @param roleId RoleId
	 * @param defaultEmailOption defaultEmailOption
	 * @param isTravellersNeeded isTravellersNeeded
	 * @param domainName domainName
	 * @param source Source
	 * @param caller Caller
	 * @return UserProfile
	 */
	public UserProfile userRegister(String username, int companyId, int roleId, boolean defaultEmailOption, boolean isTravellersNeeded, String domainName, String source, String caller) {

		UserProfile userProfile = userRegister(username, companyId, roleId, defaultEmailOption, false, domainName, source, caller, null);
		return userRegister(userProfile, defaultEmailOption, isTravellersNeeded, domainName, source, caller);
	}

	/**
	 * When domain name is not needed to be passed in the register call.
	 * @param username UserName
	 * @param companyId CompanyId
	 * @param roleId RoleId
	 * @param defaultEmailOption defaultEmailOption
	 * @param isTravellersNeeded isTravellersNeeded
	 * @param source source
	 * @param caller caller
	 * @return UserProfile
	 */
	public UserProfile userRegister(String username, int companyId, int roleId, boolean defaultEmailOption, boolean isTravellersNeeded, String source, String caller) {
		return userRegister(username, companyId, roleId, defaultEmailOption, false, null, source, caller);
	}

	/**
	 * When no travellers is needed in the response when registering the user.
	 * @param username uesrname
	 * @param companyId companyId
	 * @param roleId roleId
	 * @param defaultEmailOption defaultEmailOption
	 * @param source Source
	 * @param caller caller
	 * @return UserProfile
	 */
	public UserProfile userRegister(String username, int companyId, int roleId, boolean defaultEmailOption, String source, String caller) {
		return userRegister(username, companyId, roleId, defaultEmailOption, false, source, caller);
	}




	public UserProfile userRegister(String username, int companyId, int roleId, boolean defaultEmailOption, boolean isTravellersNeeded, String domainName, String source, String caller, String host) {

		UserProfileHolder userProfileHolder = new UserProfileHolder();
		UserProfile userProfile = new UserProfile();
		userProfile.setUsername(username);
		userProfile.setSourceOfRegistration(source);

		if (host != null) {
			userProfile.setDomain(host);
		}

		Map<Integer, CompanyDetail> companyDetails = new LinkedHashMap<Integer, CompanyDetail>(1);
		CompanyDetail companyDetail = new CompanyDetail();
		companyDetail.setCompanyId(companyId);
		List<Role> roles = new ArrayList<Role>();
		Role role = new Role();
		role.setId(roleId);
		roles.add(role);
		companyDetail.setRolesFromList(roles);

		companyDetails.put(companyId, companyDetail);
		userProfile.setCompanyDetails(companyDetails);

		userProfileHolder.setUserProfile(userProfile);

		return userRegister(userProfile, defaultEmailOption, isTravellersNeeded, domainName, source, caller);
	}


	/**
	 * Searches the UserProfile for the specified username.
	 * @param username Username
	 * @param sourceType Source
	 * @param caller Caller
	 * @param ctSecurityHeader ctSecurityHeader
	 * @return UserProfile
	 * @throws MalformedURLException
	 * @throws IOException
	 * @author Abhishek
	 */
	public UserProfile fetchUserProfile(String username, SourceType sourceType, String caller, CTSecurityHeaders ctSecurityHeader) {
		UserProfile userProfile = getUserProfileFromCondition("username=" + username, true, true, sourceType.toString(), caller, ctSecurityHeader);
		return userProfile;
	}

	/**
	 * Searches the UserProfile for the specified condition. The Condition needs to be supported by the API. The method is declared private so that it is not available publicly outside this Class. If
	 * a new condition based retrieval is needed, then it should be available at the api. A new method will be introduced with the new condition, which will internally call this private method.
	 * @param condition
	 * @param isCardDetailsNeeded
	 * @param caller
	 * @param ctSecurityHeader TODO
	 * @param isTravellerNeeded
	 * @return UserProfile
	 * @author Abhishek
	 */
	private UserProfile getUserProfileFromCondition(String condition, boolean isTravelersNeeded, boolean isCardDetailsNeeded, String source, String caller, CTSecurityHeaders ctSecurityHeader) {
		List<String> dontLog = new ArrayList<String>();
		dontLog.add("404"); // no need to log the error when the user is not
		// found in the central schema
		String urlLocation = "";
		if (StringUtils.isNotBlank(source) && isB2BCall(source)) {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api." + source.toLowerCase() + ".people.search.url");
		} else {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api.people.search.url");
		}

		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

		RestResponse restResponse = null;
		UserProfile userProfile = null;
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;
		try {
			if (StringUtils.isBlank(condition)) {
				condition = "";
			}
			condition += "&caller=" + caller;
			if (isTravelersNeeded) {
				if (condition == null || "".equals(condition)) {
					condition = "&travellers=true";
				} else {
					condition += "&travellers=true";
				}
			}

			if (isCardDetailsNeeded) {
				if (condition == null || "".equals(condition)) {
					condition = "card_details=true";
				} else {
					condition += "&card_details=true";
				}
			}

			Cookie cookie = ctSecurityHeader.getCtAuthCookie(commonCachedProperties);
			if (cookie != null) {
				StringBuilder cookieStr = new StringBuilder();
				cookieStr.append(cookie.getName()).append("=").append(cookie.getValue());
				requestHeaders.put("Cookie", cookieStr.toString());
			} else {
				if (StringUtils.isNotBlank(source)) {
					requestHeaders.putAll(CTSecurityHeaders.getAuthKeyForSourceType(commonCachedProperties, SourceType.valueOf(source)));
				}
			}
			
			// Begin Logging
			if (connectorStat != null) {
				connectorStat.addNewApiRequest(ApiType.OTHER, null, "RETRIEVE_USER_PROFILE", "", urlLocation, condition);
			}
			// End Logging

			logger.info("Get User Profile from condition(" + condition + ") Request : " + urlLocation);

			restResponse = RestUtil.get(urlLocation, condition, requestHeaders, responseHeaders, dontLog, Char_Encoding_Type_Enum.UTF,peopleTimeout);

			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}
			logger.debug(" Response : " + restResponse.getMessage());

			if (GenUtil.hasSuccessCode(restResponse)) {
				UserProfileHolder responseHolder = new UserProfileHolder();
				responseHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), responseHolder);
				userProfile = responseHolder.getUserProfile();
			}
		} catch (Exception e) {
			logger.error("Exception during the API call to Search User Profile : ", e);
			isException = true;
			responseCode = 500;
			responseMessage = "Exception during the API call to Search User Profile :" + e.getMessage();
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "RETRIEVE_USER_PROFILE", responseCode, responseMessage, !isException);
			}
		}

		return userProfile;
	}


	/**
	 * Retrieves the UserProfile for the specified People-Id.
	 * @param peopleId peopleId
	 * @param source Source
	 * @param caller Caller
	 * @param ctSecurityHeaders TODO
	 * @return UserProfile
	 * @author Abhishek
	 */
	public UserProfile getUserProfile(int peopleId, String source, String caller, CTSecurityHeaders ctSecurityHeaders) {
		return getPeopleProfile(peopleId, false, false, source, caller, ctSecurityHeaders);
	}

	/**
	 * Retrieves the UserProfile for the specified People-Id. The travellers that belong to the User are retrieved conditionally controlled by the parameter isTravelersNeeded
	 * @param peopleId peopleId
	 * @param isTravelersNeeded isTravelersNeeded
	 * @param isCardDetailsNeeded isCardDetailsNeeded
	 * @param source source
	 * @param caller caller
	 * @param ctSecurityHeaders TODO
	 * @return UserProfile
	 * @author Abhishek
	 */
	public UserProfile getPeopleProfile(int peopleId, boolean isTravelersNeeded, boolean isCardDetailsNeeded, String source, String caller, CTSecurityHeaders ctSecurityHeaders) {

		RestResponse restResponse = null;
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();
		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");

		String urlLocation = commonCachedProperties.getPropertyValue("ct.services.api.people.retrieve.url");
		String queryString = "";
		// sending source forr new HTTPS flow. Sending it by default for ON/OFF  case.
		if (StringUtils.isNotBlank(source)) {
			queryString = "source=" + source;
		}
		if (StringUtils.isNotBlank(source)) {
		    if (isB2BCall(source)) {
    			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api." + source.toLowerCase() + ".people.retrieve.url");
    			requestHeaders.putAll(CTSecurityHeaders.getAuthKeyForSourceType(commonCachedProperties, SourceType.valueOf(source)));
		    } else if (source.equalsIgnoreCase("B2C") && StringUtils.isNotBlank(caller) && caller.contains("EFFECTIVE_PRICE")) { // Doing this since it has to land in WL accounts machine
		        urlLocation = commonCachedProperties.getPropertyValue("ct.services.api.effective.price.people.retrieve.url");
		    }
		}

		urlLocation = urlLocation.replaceAll(PEOPLE_ID_PLACEHOLDER_REGEX, String.valueOf(peopleId));
		// Begin Logging
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		if (connectorStat != null) {
			connectorStat.addNewApiRequest(ApiType.OTHER, null, "GET_USER_PROFILE", "", urlLocation, "");
		}
		// End Logging

		queryString += "&caller=" + caller;
		if (isTravelersNeeded) {
			queryString += "&travellers=true";
		}

		if (isCardDetailsNeeded) {
			queryString += "&card_details=true";
		}

		addSecurityHeadersToRequestHeader(source, ctSecurityHeaders, requestHeaders);
		List<String> dontLog = new ArrayList<String>();
		dontLog.add("404");

		UserProfile userProfile = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;
		try {

			logger.info("Get User Profile Request :" + urlLocation);
			restResponse = RestUtil.get(urlLocation, queryString, requestHeaders, responseHeaders, dontLog, Char_Encoding_Type_Enum.UTF,timeout);
			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}
			if (restResponse.getCode() == 200) {
				UserProfileHolder userProfileHolder = new UserProfileHolder();
				userProfileHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), userProfileHolder);
				userProfile = userProfileHolder.getUserProfile();
				if (userProfile == null) {
					userProfile = userProfileHolder.getTravelerProfile();
				}
			} else {
				logger.warn("Error getting user profile from API for ID " + peopleId + ". Error code: " + restResponse.getCode());
				userProfile = null;
			}
		} catch (Exception e) {
			logger.warn("Exception thrown during the GET User Profile call : ", e);
			isException = true;
			responseCode = 500;
			responseMessage = "Exception thrown during the GET User Profile call : " +  e.getMessage();
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "GET_USER_PROFILE", responseCode, responseMessage, !isException);
			}
		}
		return userProfile;
	}

	private void addSecurityHeadersToRequestHeader(String source, CTSecurityHeaders ctSecurityHeaders, Map<String, String> requestHeaders) {
		Map<String, String> securityHeaders = null;
		if (ctSecurityHeaders != null) {
			Cookie cookie = ctSecurityHeaders.getCtAuthCookie(commonCachedProperties);
			if (cookie != null) {
				StringBuilder cookieStr = new StringBuilder();
				cookieStr.append(cookie.getName()).append("=").append(cookie.getValue());
				requestHeaders.put("Cookie", cookieStr.toString());
			}
			securityHeaders = ctSecurityHeaders.getAllHeaders(commonCachedProperties);
		} else {
			securityHeaders = CTSecurityHeaders.getAuthKeyForSourceType(commonCachedProperties, SourceType.valueOf(source));
		}
		requestHeaders.putAll(securityHeaders);
	}

	/**
	 * Updates the specified UserProfile.
	 * @param userProfile UserProfile
	 * @param source Source
	 * @param caller Caller
	 * @return boolean
	 * @author Abhishek
	 */
	public boolean updateUserProfile(UserProfile userProfile, String source, String caller) {
		return updateUserProfile(userProfile, source, caller, null);
	}

	/**
	 * Updates the specified UserProfile. the source parameter was being used earlier in the VM Template being used. Functionally its not required now, however the signature is being maintained so
	 * that backward compatibility is maintained.
	 * @param userProfile UserProfile
	 * @param source Source
	 * @param caller Caller
	 * @param request HttpServletRequest
	 * @return isUpdated
	 * @author Abhishek
	 */
	public boolean updateUserProfile(UserProfile userProfile, String source, String caller, HttpServletRequest request) {
		boolean isUpdated = false;

		String urlLocation = "";
		if (StringUtils.isNotBlank(source) && isB2BCall(source)) {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api." + source.toLowerCase() + ".people.update.url");
		} else {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api.people.update.url");
		}

		urlLocation = urlLocation.replaceAll(PEOPLE_ID_PLACEHOLDER_REGEX, String.valueOf(userProfile.getPeopleId()));
		urlLocation += "?mode=update";
		urlLocation += "&caller=" + caller;

		UserProfileHolder userHolder = new UserProfileHolder();
		userHolder.setUserProfile(userProfile);

		String jsonPostMessage = APIUtil.serializeObjectToJson(userHolder);
		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		RestResponse restResponse = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;
		try {
			if (connectorStat != null) {
				connectorStat.addNewApiRequest(ApiType.OTHER, null, "UPDATE_USER_PROFILE", "", urlLocation, jsonPostMessage);
			}

			logger.info("Update People Profile Request : " + urlLocation + " : " + jsonPostMessage);

			// we need to pass along cookies to the user API. This is introduced for the accountsV2 revamp project
			// where the cookie is required determine whether to send the user to the new html5 based UI or the old UI
			Cookie authCookieNew = getNewAuthCookie(request);
			List<Cookie> cookies = null;
			if (authCookieNew != null) {
				cookies = new ArrayList<Cookie>();
				cookies.add(authCookieNew);
			}

			restResponse = RestUtil.post(urlLocation, jsonPostMessage, "", "text/json", requestHeaders, responseHeaders, "POST", peopleTimeout, null, null, null, false, cookies);
			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}
			logger.debug(" Response : " + restResponse.getMessage());

			// Added for BookFlow ReDesign
			// Added this condition so that it doesn't affect the CORP and AGENCY CBS-17141
			if (source != null && !source.equalsIgnoreCase("CP") && !source.equalsIgnoreCase("AG")) {
				UserProfile tUserProfile = null;
				if (restResponse.getCode() == 200) {
					UserProfileHolder userProfileHolder = new UserProfileHolder();
					userProfileHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), userProfileHolder);
					tUserProfile = userProfileHolder.getUserProfile();
					if (tUserProfile == null) {
						tUserProfile = userProfileHolder.getTravelerProfile();
					}
				} else {
					logger.warn("Error getting user profile from API for ID " + userProfile.getPeopleId() + ". Error code: " + restResponse.getCode());
					tUserProfile = null;
				}
				// if(userProfile != null && userProfile.getContactDetail() != null && tUserProfile != null && tUserProfile.getContactDetail() != null) {
				// //userProfile.setContactDetail(tUserProfile.getContactDetail());
				// userProfile.getContactDetail().setWorkEmail(tUserProfile.getContactDetail().getEmail(EmailType.WORK));
				// userProfile.getContactDetail().setMobile(tUserProfile.getContactDetail().getMobile());
				// }
			}
			// Ended
			if (GenUtil.hasSuccessCode(restResponse)) {
				isUpdated = true;
			}
		} catch (Exception e) {
			logger.error("Exception returned in the API call to Update User : ", e);
			isException = true;
			responseCode = 500;
			responseMessage = "Exception returned in the API call to Update User :" +  e.getMessage();
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "UPDATE_USER_PROFILE", responseCode, responseMessage, !isException);
			}
		}

		return isUpdated;
	}

	@MethodExcludeCoverage
	public UserProfile getUserProfileFromUserName(String userName, String sourceType, String caller, CTSecurityHeaders ctSecurityHeaders) {
		UserProfile userProfile = getUserProfileFromCondition("username=" + userName, sourceType, caller, ctSecurityHeaders);
		return userProfile;
	}

	/**
	 * Searches the UserProfile for the specified condition. The Condition needs to be supported by the API. The method is declared private so that it is not available publicly outside this Class. If
	 * a new condition based retrieval is needed, then it should be available at the api. A new method will be introduced with the new condition, which will internally call this private method.
	 * @param condition
	 * @param caller - used for logging at accounts' end
	 * @param ctSecurityHeaders TODO
	 * @return UserProfile
	 */
	@MethodExcludeCoverage
	private UserProfile getUserProfileFromCondition(String condition, String source, String caller, CTSecurityHeaders ctSecurityHeaders) {
		List<String> dontLog = new ArrayList<String>();
		dontLog.add("404"); // no need to log the error when the user is not
		// found in the central schema

		String urlLocation = "";
		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("accept", "text/json");
		addSecurityHeadersToRequestHeader(source, ctSecurityHeaders, requestHeaders);
		//HTTPs
		if (StringUtils.isNotBlank(source)) {
			condition += "&source=" + source;
		}
		if (StringUtils.isNotBlank(source) && isB2BCall(source)) {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api." + source.toLowerCase() + ".people.search.url");
			requestHeaders.putAll(CTSecurityHeaders.getAuthKeyForSourceType(commonCachedProperties, SourceType.valueOf(source)));
		} else {
			urlLocation = commonCachedProperties.getPropertyValue("ct.services.api.people.search.url");
		}
		Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

		RestResponse restResponse = null;
		UserProfile userProfile = null;
		int responseCode = 200;
		String responseMessage = "";
		boolean isException = false;
		ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
		try {
			if (connectorStat != null) {
				connectorStat.addNewApiRequest(ApiType.OTHER, null, "RETRIEVE_USER_PROFILE", "", urlLocation, condition);
			}
			// End Logging

			logger.info("Get User Profile from condition(" + condition + ") Request : " + urlLocation);

			if (StringUtils.isNotBlank(caller)) {
				condition += "&caller=" + caller;
			}

			restResponse = RestUtil.get(urlLocation, condition, requestHeaders, responseHeaders, dontLog, Char_Encoding_Type_Enum.UTF);
			if (restResponse != null) {
				responseCode = restResponse.getCode();
				responseMessage = restResponse.getMessage();
			}
			logger.debug(" Response : " + restResponse.getMessage());
			if (GenUtil.hasSuccessCode(restResponse)) {
				UserProfileHolder responseHolder = new UserProfileHolder();
				responseHolder = (UserProfileHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), responseHolder);
				userProfile = responseHolder.getUserProfile();
			}
		} catch (Exception e) {
			isException = true;
			responseCode = 500;
			responseMessage = "Exception during the API call to Search User Profile :::" + e.getMessage();
			logger.error("Exception during the API call to Search User Profile : ", e);
		} finally {
			if (connectorStat != null) {
				connectorStat.closeApiRequest(ApiType.OTHER, "RETRIEVE_USER_PROFILE", responseCode, responseMessage, !isException);
			}
		}

		return userProfile;
	}

	private boolean isB2BCall(String source) {
		return SourceType.CORP.toString().equalsIgnoreCase(source) || SourceType.AGENCY.toString().equalsIgnoreCase(source) || SourceType.WL.toString().equalsIgnoreCase(source);
	}


	public void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
		commonCachedProperties = pcommonCachedProperties;
	}

	/**
	 * @return the cachedVms
	 */
	public CachedVms<UserAPITemplate> getCachedVms() {
		return cachedVms;
	}

	/**
	 * @param cachedVms the cachedVms to set
	 */
	public void setCachedVms(CachedVms<UserAPITemplate> cachedVms) {
		this.cachedVms = cachedVms;
	}

	private Cookie getNewAuthCookie(HttpServletRequest request) {
		return null;
		/*
		 * String newCookieName = commonCachedProperties.getPropertyValue(USER_PROFILE_NEW_DESIGN_COOKIE_PROPERTY); Cookie[] cookies = null; Cookie authCookieNew = null; if (request !=
		 * null) { cookies = request.getCookies(); } if (cookies != null) { for (int i = 0; i < cookies.length; i++) { if (cookies[i].getName().equalsIgnoreCase(newCookieName)) {
		 * authCookieNew = cookies[i]; break; } } } return authCookieNew;
		 */
	}

}
