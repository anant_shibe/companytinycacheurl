package com.companyApiTinyCache.companyConfigCache.service;


import com.companyApiTinyCache.companyConfigCache.service.impl.GenericTinyCache;

public class SimpleTinyCacheTest {

    GenericTinyCache<Integer> cache = null;

    private void testCache() {
        cache = new GenericTinyCache<Integer>(10);

        // 1. Add 10 integers
        for (int i = 1; i <= 20; i++) {
            cache.put(i + "", i);
        }
    }

    public static void main(String[] args) {
        new SimpleTinyCacheTest().testCache();
    }
}
