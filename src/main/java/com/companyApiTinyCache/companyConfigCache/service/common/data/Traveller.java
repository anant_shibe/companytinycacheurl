package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.util.Date;

/**
 * Refer UserProfile Object in common
 * 
 * @author ulhas
 */
@Deprecated
public class Traveller extends Person {

    private String title;

    private Date dob;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

}
