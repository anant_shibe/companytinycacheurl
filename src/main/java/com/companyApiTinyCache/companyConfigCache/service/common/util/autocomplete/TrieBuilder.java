package com.companyApiTinyCache.companyConfigCache.service.common.util.autocomplete;

/**
 * Represents builder for a Read Only Trie.
 * 
 * @author suresh
 * 
 * @param <E>
 *            Entity to be stored against keys
 */
public interface TrieBuilder<E> extends ReadonlyTrie<E> {

    /**
     * Called to add the next Key for constructing the Trie. The same key can be added with different entities. The Trie is actually generated with the build call after which add and build calls give
     * {@link IllegalStateException}
     * 
     * @param key
     *            Key
     * @param entity
     *            Entity corresponding to the Key
     */
    void add(String key, E entity);

    /**
     * Builds the Read Only Trie.
     */
    void build();
}
