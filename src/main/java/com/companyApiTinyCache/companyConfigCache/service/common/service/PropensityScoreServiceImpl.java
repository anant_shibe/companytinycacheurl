package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedHashSet;
import java.util.Set;


public class PropensityScoreServiceImpl implements PropensityScoreService {
	
	private CachedProperties commonCachedProperties;
	private static final Log log = LogFactory.getLog(PropensityScoreServiceImpl.class);
	
	private static final String PROPENSITY_SCORE_URL = "ct.services.common.api.propensity.score.url";
	private static final String PROPENSITY_SCORE_TIMEOUT = "ct.services.common.api.propensity.score.timeout";

	/**
	 * Returns user score depending on user activities. Data platform team analyzes past user 
	 * activity and returns the score accordingly
	 * @param uid
	 * @return User score
	 * @throws Exception 
	 */
	@Override
	public int getScore(String uid){
		String response = "";
		
		try {
			Set<String> queryParamAndValue = new LinkedHashSet<String>();
			queryParamAndValue.add("uid=" + uid);
			
			int timeout = commonCachedProperties.getIntPropertyValue(PROPENSITY_SCORE_TIMEOUT,3000);
			
			String url = commonCachedProperties.getPropertyValue(PROPENSITY_SCORE_URL);
			String queryParams = StringUtils.join(queryParamAndValue, '&');

	        RestResponse restResponse = RestUtil.get(url,queryParams, null, null,timeout);
	        response = restResponse.getMessage();
		}catch (Exception e) {
            log.error("error while getting propensity score : ", e);
        }
		return 0;
	}

	
	public final void setCommonCachedProperties(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }


}
