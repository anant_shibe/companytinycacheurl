package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.data.ThisServer;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class CtServletContextListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }

    protected ApplicationContext getContext(ServletContext servletContext) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        ApplicationContext context = getContext(servletContext);

        final String appName = System.getProperty("app.context.path");
        final String serverInfo = servletContext.getServerInfo();

        //Update current release
        ThisServer thisServer = (ThisServer) context.getBean("thisServer");
        if (thisServer != null) {
            thisServer.addApp(appName, serverInfo);
        }
//
//        //Update beta environment as well
//        ThisServer thisServerBeta = (ThisServer) context.getBean("thisServerBeta");
//        if (thisServerBeta != null) {
//            thisServerBeta.addApp(appName, serverInfo);
//        }
    }

}