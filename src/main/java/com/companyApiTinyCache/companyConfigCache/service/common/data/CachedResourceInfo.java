package com.companyApiTinyCache.companyConfigCache.service.common.data;

public class CachedResourceInfo {

    private String name;

    private String beanName;

    private String className;

    public String getName() {
        return name;
    }

    public void setName(String pname) {
        name = pname;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String pbeanName) {
        beanName = pbeanName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String pclassName) {
        className = pclassName;
    }

}
