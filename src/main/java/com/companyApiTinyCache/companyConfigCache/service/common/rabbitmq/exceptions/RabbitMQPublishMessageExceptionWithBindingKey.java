package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

@ClassExcludeCoverage
public class RabbitMQPublishMessageExceptionWithBindingKey extends Exception{

	private static final long serialVersionUID = 1L;
	public RabbitMQPublishMessageExceptionWithBindingKey(Exception exception) {
		super(exception);
	}
	public RabbitMQPublishMessageExceptionWithBindingKey(String message) {
		super(message);
	}
	public RabbitMQPublishMessageExceptionWithBindingKey(String message, Exception exception) {
		super(message, exception);
	}

}
