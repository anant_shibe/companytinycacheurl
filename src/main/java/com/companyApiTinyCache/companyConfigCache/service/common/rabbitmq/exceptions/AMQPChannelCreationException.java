package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

/**
 * @author parvatkumar
 * custom exception for channel creation
 */
@ClassExcludeCoverage
public class AMQPChannelCreationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AMQPChannelCreationException() {
		super();
	}

	public AMQPChannelCreationException(String message) {
		super(message);
	}

	public AMQPChannelCreationException(String message, Throwable cause) {
		super(message, cause);
	}

	public AMQPChannelCreationException(Throwable cause) {
		super(cause);
	}

}
