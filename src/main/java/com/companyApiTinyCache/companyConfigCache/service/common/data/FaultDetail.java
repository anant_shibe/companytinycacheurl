/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author deepa
 * 
 */
public class FaultDetail implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private String faultCode;

    private String faultMessage;

    /**
     * Getter method for faultCode
     * 
     * @return the faultCode
     */
    @JsonProperty("fault_code")
    public String getFaultCode() {
        return faultCode;
    }

    /**
     * Setter method for faultCode
     * 
     * @param faultCode
     *            the faultCode to set
     */
    @JsonProperty("fault_code")
    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    /**
     * Getter method for faultMessage
     * 
     * @return the faultMessage
     */
    @JsonProperty("fault_message")
    public String getFaultMessage() {
        return faultMessage;
    }

    /**
     * Setter method for faultMessage
     * 
     * @param faultMessage
     *            the faultMessage to set
     */
    @JsonProperty("fault_message")
    public void setFaultMessage(String faultMessage) {
        this.faultMessage = faultMessage;
    }

}
