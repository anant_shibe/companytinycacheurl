package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.AffiliateImport;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
*
* @author amith
*
*/
public class AffiliateImportHolder {

    @JsonProperty("company_import")
    private AffiliateImport affiliateImport;

    @JsonProperty("company_import")
    public AffiliateImport getAffiliateImport() {
        return affiliateImport;
    }

    @JsonProperty("company_import")
    public void setAffiliateImport(AffiliateImport affiliateImport) {
        this.affiliateImport = affiliateImport;
    }

}
