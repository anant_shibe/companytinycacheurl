/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import org.apache.commons.lang.StringUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Ramesh
 *
 */
public class DepositAccountTransaction {
    private String id;
    private String type;
    private String code;
    private String status;
    private String amount;
    private String closingBalance;
    private String closingBalanceType;
    private String txnDate;
    private Date date;
    private String acceptedDateStr;
    private Date acceptedDate;
    private String userId;
    private String userEmail;
    private String ref1;
    private String ref2;
    private String ref3;
    private String ref4;
    private String ref5;
    private Date chequeDate;
    private String paxNames;
    private String paxDesignation;
    private String paxDepartment;
    private String tripTag;
    private String product;
    private String tripName;
    private String airlineHotelTrainName;
    private String ticketVoucherNumber;
    private String startDate;
    private String returnDate;
    private String airlinePnr;
    private String bookingClass;
    private String bookingStatus;
    private String cancelledDate;

    public Date getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(Date chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getClosingBalanceType() {
        return closingBalanceType;
    }

    public void setClosingBalanceType(String closingBalanceType) {
        this.closingBalanceType = closingBalanceType;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the ref1
     */
    public String getRef1() {
        return ref1;
    }

    /**
     * @param ref1
     *            the ref1 to set
     */
    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    /**
     * @return the ref2
     */
    public String getRef2() {
        return ref2;
    }

    /**
     * @param ref2
     *            the ref2 to set
     */
    public void setRef2(String ref2) {
        this.ref2 = ref2;
    }

    /**
     * @return the ref3
     */
    public String getRef3() {
        return ref3;
    }

    /**
     * @param ref3
     *            the ref3 to set
     */
    public void setRef3(String ref3) {
        this.ref3 = ref3;
    }

    /**
     * @return the ref4
     */
    public String getRef4() {
        return ref4;
    }

    /**
     * @param ref4
     *            the ref4 to set
     */
    public void setRef4(String ref4) {
        this.ref4 = ref4;
    }

    /**
     * @return the ref5
     */
    public String getRef5() {
        return ref5;
    }

    /**
     * @param ref5
     *            the ref5 to set
     */
    public void setRef5(String ref5) {
        this.ref5 = ref5;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the userEmail
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * @param userEmail
     *            the userEmail to set
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the closingBalance
     */
    public String getClosingBalance() {
        return closingBalance;
    }

    /**
     * @param closingBalance
     *            the closingBalance to set
     */
    public void setClosingBalance(String closingBalance) {
        this.closingBalance = closingBalance;
    }

    /**
     * @return the txnDate
     */
    public String getTxnDate() {
        return txnDate;
    }

    /**
     * @param txnDate
     *            the txnDate to set
     */
    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        Date tempDate = null;
        if (StringUtils.isNotEmpty(this.txnDate)) {
            Calendar cal = Calendar.getInstance();
            String[] dateParts = this.txnDate.split("-");
            int day = Integer.parseInt(dateParts[0]);
            int month = Integer.parseInt(dateParts[1]) - 1; // month is zero based, hence -1
            int year = Integer.parseInt(dateParts[2]);
            cal.set(year, month, day);
            tempDate = cal.getTime();
        }
        return tempDate;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the acceptedDate
     */
    public Date getAcceptedDate() {
        Date tempDate = null;
        if (StringUtils.isNotEmpty(this.acceptedDateStr)) {
            Calendar cal = Calendar.getInstance();
            String[] dateParts = this.acceptedDateStr.split("-");
            int day = Integer.parseInt(dateParts[0]);
            int month = Integer.parseInt(dateParts[1]) - 1; // month is zero based, hence -1
            int year = Integer.parseInt(dateParts[2]);
            cal.set(year, month, day);
            tempDate = cal.getTime();
        }
        return tempDate;
    }

    /**
     * @param acceptedDate
     *            the acceptedDate to set
     */
    public void setAcceptedDate(Date acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    /**
     * @return the acceptedDateStr
     */
    public String getAcceptedDateStr() {
        return acceptedDateStr;
    }

    /**
     * @param acceptedDateStr
     *            the acceptedDateStr to set
     */
    public void setAcceptedDateStr(String acceptedDateStr) {
        this.acceptedDateStr = acceptedDateStr;
    }

    public String getPaxNames() {
        return paxNames;
    }

    public void setPaxNames(String paxNames) {
        this.paxNames = paxNames;
    }

    public String getPaxDesignation() {
        return paxDesignation;
    }

    public void setPaxDesignation(String paxDesignation) {
        this.paxDesignation = paxDesignation;
    }

    public String getPaxDepartment() {
        return paxDepartment;
    }

    public void setPaxDepartment(String paxDepartment) {
        this.paxDepartment = paxDepartment;
    }

    public String getTripTag() {
        return tripTag;
    }

    public void setTripTag(String tripTag) {
        this.tripTag = tripTag;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getAirlineHotelTrainName() {
        return airlineHotelTrainName;
    }

    public void setAirlineHotelTrainName(String airlineHotelTrainName) {
        this.airlineHotelTrainName = airlineHotelTrainName;
    }

    public String getTicketVoucherNumber() {
        return ticketVoucherNumber;
    }

    public void setTicketVoucherNumber(String ticketVoucherNumber) {
        this.ticketVoucherNumber = ticketVoucherNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getAirlinePnr() {
        return airlinePnr;
    }

    public void setAirlinePnr(String airlinePnr) {
        this.airlinePnr = airlinePnr;
    }

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(String cancelledDate) {
        this.cancelledDate = cancelledDate;
    }
}
