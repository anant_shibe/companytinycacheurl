package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.notification.EmailSender;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * Captcha Util class to handle different kinds of Captcha being used. Test Class - com.cleartrip.common.util.TestCtCaptchaUtil
 * 
 */
public abstract class CtCaptchaUtil {

    private static final Log logger = LogFactory.getLog(CtCaptchaUtil.class);

    public static final String RECAPTCHA = "reCaptcha";
    private static final String RECAPTCHA_CHALLENGE_FIELD = "recaptcha_challenge_field";
    private static final String RECAPTCHA_RESPONSE_FIELD = "recaptcha_response_field";

    public static class ReCaptchaFailureNotificationResponse {
        private boolean wasNotificationSent;

        private void setNotificationSent(boolean wasNotificationSent) {
            this.wasNotificationSent = wasNotificationSent;
        }

        public boolean wasNotificationSent() {
            return wasNotificationSent;
        }
    }

    public static String getAptPublicKeyForReCaptcha(CachedProperties cachedProperties, HttpServletRequest request) {
        try {
            String domain = getAptDomainForReCaptcha(cachedProperties, request);
            if (StringUtils.isNotBlank(domain)) {
                return cachedProperties.getPropertyValue("ct.common.recaptcha.public.key.for.domain." + domain);
            }
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    /**
     * This method doesn't send notification mail on unexpected failure scenario. Also, the class calling this method will not be logged.
     * 
     * @param cachedProperties
     * @param request
     * @param captchaType
     * @return
     * @throws Exception
     */
    public static final boolean isValidCaptchaAnswer(CachedProperties cachedProperties, HttpServletRequest request, String captchaType) throws Exception {
        return isValidCaptchaAnswer(cachedProperties, request, captchaType, null, null);
    }

    /**
     * This method doesn't not log the class calling it.
     * 
     * @param cachedProperties
     * @param request
     * @param captchaType
     * @param emailSender
     *            - for sending notification mail on unexpected failure
     * @return
     * @throws Exception
     */
    public static final boolean isValidCaptchaAnswer(CachedProperties cachedProperties, HttpServletRequest request, String captchaType, EmailSender emailSender) throws Exception {
        return isValidCaptchaAnswer(cachedProperties, request, captchaType, emailSender, null);
    }

    /**
     * 
     * @param cachedProperties
     * @param request
     * @param captchaType
     * @param emailSender
     *            - for sending notification mail on unexpected failure
     * @param callingClass
     *            - for logging the calling class
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public static final boolean isValidCaptchaAnswer(CachedProperties cachedProperties, HttpServletRequest request, String captchaType, EmailSender emailSender, Class callingClass) throws Exception {

        if (RECAPTCHA.equals(captchaType)) {
            String remoteAddr = request.getRemoteAddr();
            String challenge = request.getParameter(RECAPTCHA_CHALLENGE_FIELD);
            String uresponse = request.getParameter(RECAPTCHA_RESPONSE_FIELD);
            String privateKey = getAptPrivateKeyForReCaptcha(cachedProperties, request, RECAPTCHA);

            if (StringUtils.isBlank(privateKey)) {
                throw new Exception("Blank reCaptcha private key");
            }

            ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
            reCaptcha.setPrivateKey(privateKey);

            if (logger.isInfoEnabled()) {
                logger.info("privateKey-->" + privateKey);
            }

            ReCaptchaResponse reCaptchaResponse = null;
            String reCaptchaExceptionMessage = null;

            try {
                reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);

            } catch (Exception e) {
                logger.error("reCaptcha exception during checkAnswer", e);
                reCaptchaExceptionMessage = e.getMessage();
            }

            return isReCaptchaResponseValid(cachedProperties, emailSender, callingClass, reCaptchaResponse, reCaptchaExceptionMessage);

        } else {
            throw new Exception("Invalid Captcha Type");
        }
    }

    /**
     * @param cachedProperties
     * @param emailSender
     *            - for sending notification on unexpected failure
     * @param callingClass
     *            - for logging the calling class
     * @param reCaptchaErrorMessage
     *            - unexpected failure message
     */
    @SuppressWarnings("rawtypes")
    protected static ReCaptchaFailureNotificationResponse handleReCaptchaUnexpectedFailure(CachedProperties cachedProperties, EmailSender emailSender, Class callingClass, String reCaptchaErrorMessage) {
        ReCaptchaFailureNotificationResponse notificationResponse = null;
        boolean isNotificationToBeSent = cachedProperties.getBooleanPropertyValue("ct.common.recaptcha.send.notification.on.unexpected.failure", true);

        if (emailSender != null && isNotificationToBeSent) {
            String notificationSender = cachedProperties.getPropertyValue("ct.common.recaptcha.unexpected.failure.notification.sender");
            String recipientsString = cachedProperties.getPropertyValue("ct.common.recaptcha.unexpected.failure.notification.recipients");

            if (StringUtils.isNotBlank(notificationSender) && StringUtils.isNotBlank(recipientsString)) {
                List<String> recipientsList = fetchRecipientsList(recipientsString);
                notificationResponse = sendReCaptchaFailureNotification(emailSender, callingClass, reCaptchaErrorMessage, notificationSender, recipientsList);
            } else {
                logger.error("Can't send reCaptcha failure notification from sender : " + notificationSender + " to recipients : " + recipientsString);
            }
        } else {
            logger.info("Notification sending on reCaptcha unexpected failure skipped.");
        }

        if (callingClass != null) {
            logger.info("reCaptcha calling " + callingClass);
        }

        return notificationResponse;
    }

    /**
     * The various unexpected failure cases are obtained from the open-source reCaptcha source code.
     */
    private static boolean isReCaptchaUnexpectedFailureMessage(String reCaptchaErrorMessage) {
        return reCaptchaErrorMessage.contains("Null read from server") || reCaptchaErrorMessage.contains("No answer returned from recaptcha")
                || reCaptchaErrorMessage.contains("recaptcha4j-missing-error-message");
    }

    private static List<String> fetchRecipientsList(String recipientsString) {
        String[] recipientsArray = recipientsString.split(",");
        List<String> recipientsList = new ArrayList<String>();
        for (String recipient : recipientsArray) {
            recipientsList.add(recipient.trim());
        }
        return recipientsList;
    }

    private static String getAptDomainForReCaptcha(CachedProperties cachedProperties, HttpServletRequest request) throws MalformedURLException {
        String host = SecurityUtil.getHostWithoutPort(request);
        Map<String, String> hostPatternToDomainMapForRecaptcha = getHostPatternToDomainMapForReCaptcha(cachedProperties);
        String domain = null;

        if ((hostPatternToDomainMapForRecaptcha != null) && (hostPatternToDomainMapForRecaptcha.size() > 0)) {
            for (String hostPattern : hostPatternToDomainMapForRecaptcha.keySet()) {
                if (host.matches(hostPattern)) {
                    domain = hostPatternToDomainMapForRecaptcha.get(hostPattern);
                    break;
                }
            }
        }

        return domain;
    }

    private static String getAptPrivateKeyForReCaptcha(CachedProperties cachedProperties, HttpServletRequest request, String captchaType) {
        try {
            String domain = getAptDomainForReCaptcha(cachedProperties, request);
            if (StringUtils.isNotBlank(domain)) {
                return cachedProperties.getPropertyValue("ct.common.recaptcha.private.key.for.domain." + domain);
            }
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    private static Map<String, String> getHostPatternToDomainMapForReCaptcha(CachedProperties cachedProperties) {
        Map<String, String> hostPatternToDomainMapForReCaptcha = null;
        String jsonString = cachedProperties.getPropertyValue("ct.common.host.pattern.to.domain.map.for.recaptcha");
        ObjectMapper mapper = JsonUtil.getObjectMapper();

        try {
            hostPatternToDomainMapForReCaptcha = mapper.readValue(jsonString, Map.class);
        } catch (Exception e) {
            logger.error(e);
        }

        return hostPatternToDomainMapForReCaptcha;
    }

    @SuppressWarnings("rawtypes")
    private static boolean isReCaptchaResponseValid(CachedProperties cachedProperties, EmailSender emailSender, Class callingClass, ReCaptchaResponse reCaptchaResponse,
            String reCaptchaExceptionMessage) {
        if (reCaptchaResponse != null && reCaptchaResponse.isValid()) {
            return true;
        } else {
            String reCaptchaErrorMessage;

            if (reCaptchaResponse == null) {
                reCaptchaErrorMessage = reCaptchaExceptionMessage;
                handleReCaptchaUnexpectedFailure(cachedProperties, emailSender, callingClass, reCaptchaErrorMessage);
            } else {
                reCaptchaErrorMessage = reCaptchaResponse.getErrorMessage();
                if (isReCaptchaUnexpectedFailureMessage(reCaptchaErrorMessage)) {
                    handleReCaptchaUnexpectedFailure(cachedProperties, emailSender, callingClass, reCaptchaErrorMessage);
                }
            }

            return false;
        }
    }

    @SuppressWarnings("rawtypes")
    private static ReCaptchaFailureNotificationResponse sendReCaptchaFailureNotification(EmailSender emailSender, Class callingClass, String reCaptchaErrorMessage, String notificationSender,
            List<String> recipientsList) {
        ReCaptchaFailureNotificationResponse notificationResponse = new ReCaptchaFailureNotificationResponse();

        String subjectMessage = "reCaptcha Unexpected Failure Notification";
        String bodyMessage = "reCaptcha failed unexpectedly with this error message - " + reCaptchaErrorMessage;

        if (callingClass != null) {
            bodyMessage += "\n reCaptcha calling " + callingClass;
        }

        try {
            emailSender.sendEmail(recipientsList, null, null, notificationSender, subjectMessage, bodyMessage);
            notificationResponse.setNotificationSent(true);
            logger.info("Sent reCaptcha failure notification email to " + recipientsList);
        } catch (MessagingException e) {
            notificationResponse.setNotificationSent(false);
            logger.error("Error while sending reCaptcha failure notification to " + recipientsList, e);
        }

        return notificationResponse;
    }

}
