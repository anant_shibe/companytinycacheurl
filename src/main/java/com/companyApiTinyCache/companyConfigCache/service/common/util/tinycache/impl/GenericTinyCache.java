/* 
 * @(#) SimpleTinyCache.java
 * 
 * Copyright 2010-2011 by Cleartrip Travel Services Pvt. Ltd.
 * Unit No 001, Ground Floor, DTC Bldg, 
 * Sitaram Mills Compound, N.M. Joshi Marg, 
 * Delisle Road, 
 * Lower Parel (E) 
 * Mumbai - 400011
 * India
 * 
 * This software is the confidential and proprietary information 
 * of Cleartrip Travel Services Pvt. Ltd. ("Confidential Information"). 
 * You shall not disclose such Confidential Information and shall 
 * use it only in accordance with the terms of license agreement you 
 * entered into with Cleartrip Travel Service Pvt. Ltd.
 */
package com.companyApiTinyCache.companyConfigCache.service.common.util.tinycache.impl;

import com.companyApiTinyCache.companyConfigCache.service.common.util.tinycache.TinyCache;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.Map.Entry;

public class GenericTinyCache<T> implements TinyCache<T> {

    private static final Logger logger = Logger.getLogger(GenericTinyCache.class);

    protected volatile LRUCache<T> cache;
    protected int maxSize;

    public GenericTinyCache(int maxSize) {
        this.maxSize = maxSize;
        this.cache = new LRUCache<T>(maxSize);
        logger.debug("GenericTinyCache initialization complete.");
    }

    @Override
    public T put(String key, T value) {

        synchronized (cache) {

            T oldValue = null;
            if (value == null) {
                oldValue = cache.remove(key);
            } else {
                oldValue = cache.put(key, value);
                logger.debug("Added: key=" + key + " | oldVal=" + oldValue + " | newVal=" + value);
                logger.debug("LRUCache.size=" + cache.size());
            }

            return oldValue;

        }
    }

    @Override
    public void remove(String key) {
        synchronized (cache) {
            cache.remove(key);
        }
    }

    @Override
    public T get(String key) {

        T value = cache.get(key);
        logger.debug("get: key=" + key + " | value=" + value);

        return value;
    }

    @Override
    public void flush() {
        synchronized (cache) {
            cache.clear();
        }
        logger.debug("Cache flushed!");
        logger.debug("Tiny Cache flushed ! at : " + new Timestamp((new java.util.Date()).getTime()));

    }

    @Override
    public int size() {
        synchronized (cache) {
            return cache.size();
        }
    }

    /**
     * Print key-value pair to System.out.
     * 
     * @param key
     * @throws IOException
     */
    public void printKey(String key) throws IOException {
        Writer writer = null;
        try {
            writer = new PrintWriter(System.out);
            printKey(key, writer);
            logger.debug("Key value in Tiny Cache : " + key + " at : " + new Timestamp((new java.util.Date()).getTime()));
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Print all key-value pairs to System.out.
     * 
     * @param key
     * @throws IOException
     */
    public void printAllKeys() throws IOException {
        Writer writer = null;
        try {
            writer = new PrintWriter(System.out);
            printAllKeys(writer);
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void printKey(String key, Writer writer) throws IOException {
        synchronized (cache) {
            T value = cache.get(key);
            writer.write("-----------------------------\n");
            writer.write("<" + key + ">\n" + value + "\n");
        }

    }

    @Override
    public void printAllKeys(Writer writer) throws IOException {
        synchronized (cache) {
            for (Entry<String, T> entry : cache.entrySet()) {
                writer.write("-----------------------------\n");
                writer.write("<" + entry.getKey() + ">\n" + entry.getValue() + "\n");
            }
        }

    }

    public void refresh() {
        // Empty impl
    }
}
