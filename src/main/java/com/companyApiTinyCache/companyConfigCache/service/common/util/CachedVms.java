package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import java.util.EnumMap;
import java.util.Map;

public class CachedVms<K extends Enum<K> & VmResourceInfo> extends CachedEnumResources<K, Template> {

    private String resourcePrefix;

    // One velocityEngine for REGULAR and one for XML type
    private Map<VmResourceInfo.TemplateType, VelocityEngine> velocityEngines;

    private void setVmProperties(VelocityEngine ve, String filePrefix) {
        ve.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.Log4JLogChute");
        ve.setProperty("runtime.log.logsystem.log4j.logger", "org.apache.velocity");
        ve.setProperty(VelocityEngine.RESOURCE_MANAGER_DEFAULTCACHE_SIZE, "1");
        ve.setProperty(VelocityEngine.RESOURCE_LOADER, "file, class");
        ve.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
        ve.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH, filePrefix);
        ve.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_CACHE, "false");
        ve.setProperty("file.resource.loader.modificationCheckInterval", "0");
        ve.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    }

    private static String fixPath(String path) {
        String fileSeparator = System.getProperty("file.separator");
        if (!path.endsWith(fileSeparator) && !path.endsWith("/")) {
            path += '/';
        }
        return path;
    }

    static {
        Velocity.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.Log4JLogChute");
        Velocity.setProperty("runtime.log.logsystem.log4j.logger", "org.apache.velocity");
    }

    public CachedVms(Class<K> c, String filePrefix, String commonPrefix) {
        super(c, null);
        filePrefix = fixPath(filePrefix);
        commonPrefix = fixPath(commonPrefix);
        resourcePrefix = commonPrefix;
        String filePath = filePrefix + commonPrefix;
        setPath(filePath);
        velocityEngines = new EnumMap<VmResourceInfo.TemplateType, VelocityEngine>(VmResourceInfo.TemplateType.class);
        for (K resourceKey : enumVals) {
            VmResourceInfo.TemplateType templateType = resourceKey.getTemplateType();
            VelocityEngine ve = velocityEngines.get(templateType);
            if (ve == null) {
                ve = new VelocityEngine();
                setVmProperties(ve, filePrefix);
                switch (templateType) {
                case XML:
                    ve.setProperty(VelocityEngine.EVENTHANDLER_REFERENCEINSERTION, "org.apache.velocity.app.event.implement.EscapeXmlReference");
                    // Skip Xml escape for variables beginning with X
                    ve.setProperty("eventhandler.escape.xml.match", "/\\$\\!?\\{?[^X{!].*/");
                    break;
                case HTML:
                    ve.setProperty(VelocityEngine.EVENTHANDLER_REFERENCEINSERTION, "org.apache.velocity.app.event.implement.EscapeHtmlReference");
                    // Skip html escape for variables beginning with X
                    ve.setProperty("eventhandler.escape.html.match", "/\\$\\!?\\{?[^X{!].*/");
                    break;
                default:
                    break;
                }
                velocityEngines.put(templateType, ve);
            }
        }
        refreshResource();
    }

    /**
     * {@inheritDoc}
     * 
     * @throws Exception
     *             On Error
     * @throws ParseErrorException
     *             On Error
     * @throws ResourceNotFoundException
     *             On Error
     * @see com.cleartrip.common.util.CachedEnumResources#loadResource(Enum, boolean)
     */
    @Override
    protected Template loadResource(K resourceKey, boolean isFileResource) throws ResourceNotFoundException, ParseErrorException, Exception {
        VmResourceInfo.TemplateType templateType = resourceKey.getTemplateType();
        VelocityEngine ve = velocityEngines.get(templateType);
        String resource = resourcePrefix + resourceKey.getResourceName();
        Template template = ve.getTemplate(resource);
        return template;
    }

    @Override
    protected String computeDigest() {
        return "0";
    }

}
