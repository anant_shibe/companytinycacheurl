package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import com.companyApiTinyCache.companyConfigCache.service.common.template.JedisExecutor;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * JedisExecutor for REDIS_ID_LINK operator.
 *
 */
public class RedisIdLinkExecutor extends JedisExecutor<Boolean> {

    private static final long DEFAULT_REFRESH_TIME = 300000;

    private static final Log log = LogFactory.getLog(RedisIdLinkExecutor.class);

    private static class CacheEntry {
        private long refreshTime;
        private boolean exists;
        private Set<Long> hotelIds;
        public boolean hotelIdExists(long hotelId) {
            return exists && hotelIds.contains(hotelId);
        }
    }

    private static class CacheEntryHolder {
        private Lock lock = new ReentrantLock();

        private volatile CacheEntry entry;

        public CacheEntry getCacheEntry(long refreshTime, Jedis jedis, String key) {
            long now = System.currentTimeMillis();
            CacheEntry cacheEntry = entry;
            boolean locked;
            if (cacheEntry != null) {
                locked = false;
                if ((now - cacheEntry.refreshTime) > refreshTime) {
                    // Acquiring lock is optional. We can use old entry if already locked
                    locked = lock.tryLock();
                }
            } else {
                locked = true;
                lock.lock();
            }
            if (locked) {
                try {
                    cacheEntry = entry;
                    if (cacheEntry == null || (now - cacheEntry.refreshTime) > refreshTime) {
                        boolean exists = true;
                        Set<String> jedisHotelIds = jedis.smembers(key);
                        Set<Long> hotelIds = null;
                        if (jedisHotelIds == null || jedisHotelIds.size() == 0) {
                            exists = jedis.exists(key);
                            if (exists) {
                                hotelIds = new HashSet<Long>();
                            }
                        } else {
                            hotelIds = new HashSet<Long>();
                            for (String jedisHotelId : jedisHotelIds) {
                                hotelIds.add(Long.parseLong(jedisHotelId));
                            }
                        }
                        cacheEntry = new CacheEntry();
                        cacheEntry.exists = exists;
                        cacheEntry.hotelIds = hotelIds;
                        cacheEntry.refreshTime = now;
                        entry = cacheEntry;
                        if (log.isInfoEnabled()) {
                            log.info("Successfully Updated JVM Redis Mirror Cache Entry for Key " + key);
                        }
                    }
                } finally {
                    lock.unlock();
                }
            }

            return cacheEntry;
        }
    }

    private volatile Map<String, CacheEntryHolder> redisMirror;

    // This would be null unless set
    private CachedProperties commonCachedProperties;

    public void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
    }

    private CacheEntryHolder initCacheEntry(String key) {
        Map<String, CacheEntryHolder> rm = redisMirror;
        CacheEntryHolder holder = null;
        if (rm != null) {
            holder = rm.get(key);
        }
        if (holder == null) {
            synchronized (this) {
                Map<String, CacheEntryHolder> rm1 = redisMirror;
                if (rm1 != rm) {
                    rm = rm1;
                    if (rm != null) {
                        holder = rm.get(key);
                    }
                }
                if (holder == null) {
                    holder = new CacheEntryHolder();
                    rm = rm == null ? new HashMap<String, CacheEntryHolder>() : new HashMap<String, CacheEntryHolder>(rm);
                    rm.put(key, holder);
                    redisMirror = rm;
                }
            }
        }

        return holder;
    }

    private CacheEntry getCacheEntry(long refreshTime, Jedis jedis, String key) {
        CacheEntry entry = initCacheEntry(key).getCacheEntry(refreshTime, jedis, key);

        return entry;
    }

    private boolean executeOnRedisMirror(long refreshTime, Jedis jedis, Object... params) {
        String setKeyPrefix = (String) params[0];
        String member = (String) params[1];
        long hotelId = Long.parseLong(member);

        String includesKey = getIncludesKey(setKeyPrefix);
        CacheEntry includesEntry = getCacheEntry(refreshTime, jedis, includesKey);

        if (includesEntry.hotelIdExists(hotelId)) {
            return true;
        }

        String excludesKey = getExcludesKey(setKeyPrefix);
        CacheEntry excludesEntry = getCacheEntry(refreshTime, jedis, excludesKey);

        return excludesEntry.exists ? !excludesEntry.hotelIdExists(hotelId) : !includesEntry.exists;
    }

    @Override
    protected Boolean executeInternal(Jedis jedis, Object... params) {
        String setKeyPrefix = (String) params[0];
        String member = (String) params[1];
        try {
            if (setKeyPrefix.contains("HOTEL_ID") || setKeyPrefix.contains("ACTIVITY_ID") || setKeyPrefix.contains("VARIANT_ID") || setKeyPrefix.contains("EVENT_ID")) {
                long refreshTime = DEFAULT_REFRESH_TIME;
                if (commonCachedProperties != null) {
                    refreshTime = commonCachedProperties.getLongPropertyValue("ct.hotel.raterule.redis-mirror.refresh.millis", DEFAULT_REFRESH_TIME);
                }
                if (refreshTime > 0) {
                    boolean result = executeOnRedisMirror(refreshTime, jedis, params);
                    if (log.isDebugEnabled()) {
                        log.debug("Successfully Checked JVM Cache for prefix " + setKeyPrefix + ", member " + member + " and got result " + result);
                    }
                    return result;
                }
            }
        } catch (Exception e) {
            log.error("Exception on JVM Hotel ID Rule Check, will fall back to Redis", e);
        }

        String includesKey = getIncludesKey(setKeyPrefix);

        if (jedis.sismember(includesKey, member)) {
            return true;
        }

        String excludesKey = getExcludesKey(setKeyPrefix);

        return jedis.exists(excludesKey) ? !jedis.sismember(excludesKey, member) : !jedis.exists(includesKey);
    }

    static String getIncludesKey(String setKeyPrefix) {
        return setKeyPrefix + ":INCLUDES";
    }

    static String getExcludesKey(String setKeyPrefix) {
        return setKeyPrefix + ":EXCLUDES";
    }
}
