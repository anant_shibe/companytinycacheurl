/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.notification;

import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Map;

/**
 * @author sanjeev
 * 
 */
public interface NotificationMediator {

    /**
     * 
     * @param templateName
     * @param toAddress
     * @param fromAddress
     * @param subjectValues
     * @param bodyValues
     */
    public void transmitNotification(String subjectTemplateName, String bodyTemplateName, String toAddress, String fromAddress, Map<String, String> subjectValues, Map<String, String> bodyValues)
            throws MessageFormatException, MessagingException;

    /**
     * 
     * @param templateName
     * @param toList
     * @param ccList
     * @param bccList
     * @param fromAddress
     * @param subjectValues
     * @param bodyValues
     */
    public void transmitNotification(String subjectTemplateName, String bodyTemplateName, Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String fromAddress,
            Map<String, String> subjectValues, Map<String, String> bodyValues) throws MessageFormatException, MessagingException;

    public void transmitNotification(String subjectTemplateName, String bodyTemplateName, String toAddress, String fromAddress, Map<String, String> subjectValues, Map<String, String> bodyValues,
            String fromName) throws MessageFormatException, MessagingException;

    public void transmitNotification(String subjectTemplateName, String bodyTemplateName, String toAddress, String fromAddress, Map<String, String> subjectValues, Map<String, String> bodyValues,
            String fromName, String contentType) throws MessageFormatException, MessagingException;

}
