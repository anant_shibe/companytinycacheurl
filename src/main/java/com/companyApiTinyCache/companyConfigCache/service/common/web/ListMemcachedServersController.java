package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller to list memcached/redis servers (/common/memcached_servers).
 *
 * @author suresh
 */
public class ListMemcachedServersController extends AbstractController {

    private static final Log log = LogFactory.getLog(ListMemcachedServersController.class);

    private CachedProperties commonCachedProperties;

    private CacheFactory cacheFactory;

    public ListMemcachedServersController() {
        setCacheSeconds(0);
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Cache> propCacheMap = cacheFactory.getCacheMap();
        Map<String, List<String>> serverCachePropertyMap = new HashMap<String, List<String>>();
        for (String propName : propCacheMap.keySet()) {
            String server = commonCachedProperties.getPropertyValue(propName);
            List<String> cacheProperties = serverCachePropertyMap.get(server);
            if (cacheProperties == null) {
                cacheProperties = new ArrayList<String>();
                serverCachePropertyMap.put(server, cacheProperties);
            }
            cacheProperties.add(propName);
        }

        return new ModelAndView("listMemcachedServers", "serverPropertyMap", serverCachePropertyMap);
    }

    public final void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
    }

    public void setCacheFactory(CacheFactory pcacheFactory) {
        cacheFactory = pcacheFactory;
    }

}
