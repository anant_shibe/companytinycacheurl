package com.companyApiTinyCache.companyConfigCache.service.common.util.autocomplete;

import java.util.Iterator;

/**
 * An iterator which returns the entities matching a prefix in a Trie with additional info about current item.
 * 
 * @author suresh
 * 
 * @param <E>
 *            The Entity Type. Each iteration returns the List of Entities associated wih a Key.
 */
public interface TrieIterator<E> extends Iterator<E[]> {
    /**
     * Indicates if the current result of iteration matches the prefix searched for exactly. The Entity is returned in the iterators next() call. So the check corresponds to the key for the Entity
     * returned from the previous next call. This can be accomplished by comparing prefix with getMatchedKey but this might be more efficient.
     * 
     * @return True if prefix matched false otherwise
     */
    boolean isMatchesPrefix();

    /**
     * Returns the current key matched. The Entity is returned in the iterators next() call. So the Key corresponds to the Entity returned from the previous next call.
     * 
     * @return Currently Matched Key
     */
    String getMatchedKey();

    int getNumEntities();

    int getPrevCommonPrefixLen();

    int getNextCommonPrefixLen();

    int getNumEntities(int prefixLen);
}
