package com.companyApiTinyCache.companyConfigCache.service.common.cached.resources;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedUrlResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.StreamResourceLoader;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class FreqFlyerCompatibleAirlinesResource extends CachedUrlResource<Map<String, Set<String>>> {

    private static Map<String, Set<String>> ffCompatibleAirlineCodesMap;

    private FreqFlyerExcludedAirlinesResource ffExcludedAirlinesResource;

    public static Map<String, Set<String>> getCompatibleAirlineCodesMap() {
        return ffCompatibleAirlineCodesMap;
    }

    /**
     * useful for testing purposes
     */
    public static void setCompatibleAirlineCodesMap(Map<String, Set<String>> compatibleAirlineCodesMap) {
        ffCompatibleAirlineCodesMap = compatibleAirlineCodesMap;
    }

    public FreqFlyerCompatibleAirlinesResource(CachedProperties cachedProperties, FreqFlyerExcludedAirlinesResource ffExcludedAirlinesResource) throws MalformedURLException {
        super(cachedProperties, "ct.services.air.freq-flyer-compatible-airlines.url.ctconfig", null, false);
        this.ffExcludedAirlinesResource = ffExcludedAirlinesResource;
        setLoader(new FreqFlyerCompatibleAirlinesResourceLoader());
        refreshResource();
    }

    private class FreqFlyerCompatibleAirlinesResourceLoader implements StreamResourceLoader<Map<String, Set<String>>> {

        @Override
        public Map<String, Set<String>> loadStreamResource(InputStream is) throws Exception {
            String line = null;
            StringBuilder sb = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
            String compatibleAirlineCodesJson = sb.toString();

            ObjectMapper mapper = JsonUtil.getObjectMapper();
            ffCompatibleAirlineCodesMap = mapper.readValue(compatibleAirlineCodesJson, new TypeReference<Map<String, LinkedHashSet<String>>>() {
            });

            removeExcludedAirlineEntries();
            // UPRM 2551
            handleAdditionalMappingRequired();

            return ffCompatibleAirlineCodesMap;
        }

        // remove map entry for an excluded airline from compatible airline codes map only if that excluded airline
        // is present in the key of the map (it need not be removed if present in value of a map entry
        // - since there will be no issue with it & its logically correct to retain it in value) (discussed with Mangesh)
        // as of 21/06/12, excluded airline codes which are also in key of the map : VY, AB, EI, DJ, XR, KF, AX, WX, F9, JZ
        // (these may also come from other connectors like Travel Fusion which don't support FF - Amit Ghosh)
        private void removeExcludedAirlineEntries() {
            Set<String> ffExcludedDomAirlineCodes = ffExcludedAirlinesResource.getResourceValue().get("dom");
            Set<String> ffExcludedIntlAirlineCodes = ffExcludedAirlinesResource.getResourceValue().get("intl");

            Set<String> excludedAirlineCodes = new LinkedHashSet<String>();
            if (ffExcludedDomAirlineCodes != null) {
                excludedAirlineCodes.addAll(ffExcludedDomAirlineCodes);
            }

            if (ffExcludedIntlAirlineCodes != null) {
                excludedAirlineCodes.addAll(ffExcludedIntlAirlineCodes);
            }

            ffCompatibleAirlineCodesMap.keySet().removeAll(excludedAirlineCodes);
        }

        /*
         * adds entry to map that are applicable but are not given in the json viz S2 entries are not defined in Json but are applicable
         */
        private void handleAdditionalMappingRequired() {
            Set<String> airlineCodesApplicableTo9W = ffCompatibleAirlineCodesMap.get("9W");
            /*
             * Check if there entry corresponding to 9W exists in the map Null check would ensure the code does not breaks even if 9W entry is removed from json
             */
            if (airlineCodesApplicableTo9W != null) {
                airlineCodesApplicableTo9W.add("S2");
            }

            /*
             * Check if there entry corresponding to S2 exists in the map Null check would ensure the code does not breaks even if S2 entry is added to json
             */
            Set<String> airlineCodesApplicableToS2 = ffCompatibleAirlineCodesMap.get("S2");
            if (airlineCodesApplicableToS2 == null) {
                airlineCodesApplicableToS2 = new TreeSet<String>();
            }
            airlineCodesApplicableToS2.add("9W");
            ffCompatibleAirlineCodesMap.put("S2", airlineCodesApplicableToS2);
        }
    }

}
