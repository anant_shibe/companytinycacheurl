package com.companyApiTinyCache.companyConfigCache.service.common.logging;

public enum CtStep {

    OTHER, SEARCH, SS1, SS2, SS3, PREPAYMEMNT, BOOK, TICKET, CANCEL, HOLD_SEAT
}
