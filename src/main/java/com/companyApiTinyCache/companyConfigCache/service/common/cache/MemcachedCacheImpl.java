package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.danga.MemCached.ErrorHandler;
import com.danga.MemCached.MemCachedClient;
import com.danga.MemCached.SockIOPool;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Concrete implementation of Cache for memcached.
 *
 * @author sdesai
 */
public class MemcachedCacheImpl extends CacheBase implements Cache {

    /** Class logger. */
    private static final Log logger = LogFactory.getLog(MemcachedCacheImpl.class);

    /** Maximum length for memcached keys, as per the memcached protocol. */
    private static final String PRIMARY_POOL = "primarypool";
    private static final String BACKUP_POOL = "backuppool";
    private MemCachedClient mcc;
    private MemCachedClient bkpmcc;
    private ThreadLocal<Throwable> clientException = new ThreadLocal<Throwable>();
    private CachedProperties commonCachedProperties;

    /**
     * Exception thrown when class cannot be instantiated or fields cannot be set. Exception thrown when class cannot be instantiated or fields cannot be set.
     */
    static class ClassNotLoadableException extends RuntimeException {
        private static final long serialVersionUID = 6965181426187637836L;

        /**
         * Constructs a new runtime exception with <code>null</code> as its detail message. The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
         */
        public ClassNotLoadableException() {
            super();
        }

        /**
         * Constructs a new runtime exception with the specified detail message. The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
         *
         * @param message
         *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
         */
        public ClassNotLoadableException(final String message) {
            super(message);
        }

        /**
         * Constructs a new runtime exception with the specified detail message and cause.
         * <p>
         * Note that the detail message associated with <code>cause</code> is <i>not</i> automatically incorporated in this runtime exception's detail message.
         *
         * @param message
         *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
         * @param cause
         *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.)
         * @since 1.4
         */
        public ClassNotLoadableException(final String message, final Throwable cause) {
            super(message, cause);
        }

        /**
         * Constructs a new runtime exception with the specified cause and a detail message of <tt>(cause==null ? null : cause.toString())</tt> (which typically contains the class and detail message
         * of <tt>cause</tt>). This constructor is useful for runtime exceptions that are little more than wrappers for other throwables.
         *
         * @param cause
         *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.)
         * @since 1.4
         */
        public ClassNotLoadableException(Throwable cause) {
            super(cause);
        }
    }

    /**
     * The primary constructor for creating the memcache instance. Cache framework allows you to store your java value objects as it is into memcache, or you have the option to store them as String.
     * Following are some of the advantages of storing value objects as String: 1. Takes less memory. As per our experiment it took 1/4 of the memory taken by other java objects. 2. Since memcache
     * server is distributed, and passing heavy objects will incur network load. Passing string objects over network is advantageous over other objects like Map, List.
     *
     * To store your value objects as String, API expects following 2 parameters:
     *
     * a) Value Class - The full path of the class name. This value class should implement CachedObjectConverter. b) Collection Class - If you are passing Collection of objects as value objects, then
     * specify the implementation collection class. The value object within collection class should be of type CachedObjectConverter.
     *
     *
     * @param nameSpace
     *            namespace / cache segment to use for this cache.
     * @param valueClass
     *            String represing full path of value class.
     * @param collClass
     *            String representing the collection class name in which value objects will be added.
     * @param rscp
     *            MemcachedCacheProperties
     *
     * @throws CacheException
     *             if cache cannot be created
     */
    protected MemcachedCacheImpl(MemcachedCacheProperties rscp, CachedProperties pcommonCachedProperties, String serversProperty)
            throws CacheException {
        // Don't use an error handler. Error handlers have 'tricky' semantics:
        // in particular, they should not throw exceptions,
        // as that interfers with socket cleanup
        commonCachedProperties = pcommonCachedProperties;
        mcc = new MemCachedClient(null, new MemcachedErrorHandler());

        logger.info("=======================");
        logger.info("memcached servers:");

        String memcachedServers = commonCachedProperties.getPropertyValue(serversProperty, "ct.services.memcached.primaryservers");

        if (memcachedServers == null) {
            memcachedServers = "localhost:11211";
        }
        final String[] servers = memcachedServers.split(",");

        final Integer[] weights = new Integer[servers.length];
        for (int i = 0; i < servers.length; i++) {
            logger.info("   " + servers[i]);
            weights[i] = 1; // TODO -- get server weight from configuration
        }
        logger.info("=======================");

        SockIOPool pool = SockIOPool.getInstance();
        pool.setServers(servers);
        pool.setWeights(weights);

        int initConnnections = rscp.getInitConn();
        logger.info("pool.setInitConn(" + initConnnections + ")");
        pool.setInitConn(initConnnections);

        int minConnnections = rscp.getMinConn();
        logger.info("pool.setMinConn(" + minConnnections + ")");
        pool.setMinConn(minConnnections);

        int maxConnections = rscp.getMaxConn();
        logger.info("pool.setMinConn(" + maxConnections + ")");
        pool.setMaxConn(maxConnections);

        int maxIdleTime = rscp.getMaxIdle();
        logger.info("pool.setMaxIdle(" + maxIdleTime + ")");
        pool.setMaxIdle(maxIdleTime);

        int maintSleepTime = rscp.getMaintSleep();
        logger.info("pool.setMaintSleep(" + maintSleepTime + ")");
        pool.setMaintSleep(maintSleepTime);

        boolean nagle = rscp.getNagle();
        logger.info("pool.setNagle(" + nagle + ")");
        pool.setNagle(nagle);

        int socketConnection = rscp.getSocketTO();
        logger.info("pool.setSocketTO(" + socketConnection + ")");
        pool.setSocketTO(socketConnection);

        pool.setSocketConnectTO(0);
        pool.setHashingAlg(SockIOPool.NEW_COMPAT_HASH);
        pool.initialize();
        // mcc.setPoolName(PRIMARY_POOL);
        boolean compressEnable = rscp.getCompressEnable();
        logger.info("mcc.setCompressEnable(" + compressEnable + ")");
        mcc.setCompressEnable(compressEnable);

        int theCompressThreshold = rscp.getCompressThreshold();
        logger.info("mcc.setCompressThreshold(" + theCompressThreshold + ")");
        mcc.setCompressThreshold(theCompressThreshold);

        // We'll 'sanitize' the keys ourselves
        mcc.setSanitizeKeys(false);
        mcc.setPrimitiveAsString(true);

        // configure pool and client for backup if required
        if (isCacheBackupEnabled()) {
            String backupServers = "";
            if (backupServers == null || "".equals(backupServers.trim())) {
                backupServers = commonCachedProperties.getPropertyValue("ct.services.memcached.backupservers");
            }
            if (backupServers != null && !"".equals(backupServers.trim())) {
                // everthing is correctly set. Proceed with initializing bkp.
                bkpmcc = new MemCachedClient(null, new MemcachedErrorHandler());
                final String[] bkpservers = backupServers.split(",");

                final Integer[] bkpweights = new Integer[bkpservers.length];
                for (int i = 0; i < bkpservers.length; i++) {
                    logger.info("   " + bkpservers[i]);
                    bkpweights[i] = 1;
                }
                logger.info("=======================");

                SockIOPool bkppool = SockIOPool.getInstance(BACKUP_POOL);
                bkppool.setServers(bkpservers);
                bkppool.setWeights(bkpweights);

                logger.info("bkppool.setInitConn(" + initConnnections + ")");
                bkppool.setInitConn(initConnnections);

                logger.info("bkppool.setMinConn(" + minConnnections + ")");
                bkppool.setMinConn(minConnnections);

                logger.info("bkppool.setMinConn(" + maxConnections + ")");
                bkppool.setMaxConn(maxConnections);

                logger.info("bkppool.setMaxIdle(" + maxIdleTime + ")");
                bkppool.setMaxIdle(maxIdleTime);

                logger.info("bkppool.setMaintSleep(" + maintSleepTime + ")");
                bkppool.setMaintSleep(maintSleepTime);

                logger.info("bkppool.setNagle(" + nagle + ")");
                bkppool.setNagle(nagle);

                logger.info("bkppool.setSocketTO(" + socketConnection + ")");
                bkppool.setSocketTO(socketConnection);

                bkppool.setSocketConnectTO(0);
                bkppool.setHashingAlg(SockIOPool.NEW_COMPAT_HASH);
                bkppool.initialize();
                bkpmcc.setPoolName(BACKUP_POOL);

                logger.info("bkpmcc.setCompressEnable(" + compressEnable + ")");
                bkpmcc.setCompressEnable(compressEnable);

                logger.info("bkpmcc.setCompressThreshold(" + theCompressThreshold + ")");
                bkpmcc.setCompressThreshold(theCompressThreshold);

                // We'll 'sanitize' the keys ourselves
                bkpmcc.setSanitizeKeys(false);
                bkpmcc.setPrimitiveAsString(true);
            } else { // end if non empty bkp server list
                // bkp server list found empty. Not acceptable.
                throw new CacheException("memcached backup servers not set in the runtime property ct.services.memcached.backupservers");
            } // end else block for empty bkp server list
        } // end if block for bkp servers.

    } // end method.

    @Override
    public Object getNativeConnection(String key) {
        return mcc;
    }

    /**
     * Flush all the data from the cache.
     *
     * {@inheritDoc}
     */
    public void clear() throws CacheDeleteException {
        boolean ret = mcc.flushAll();
        boolean bkpret = false;
        if (isCacheBackupEnabled()) {
            bkpret = bkpmcc.flushAll();
        }
        if (!(ret || bkpret)) {
            throw new CacheDeleteException("Could not flush all cache servers", getAndClearClientException());
        }
    }

    /**
     * Following are the rules for the get method:
     *
     * When not to process the value object: <br>
     * ===================================== <br>
     * 1. If valueClass of the MemcachedImpl object is null. 2. If obj is a not a instace of CachedObjectConverter <br>
     * <br>
     * When to process the value object: <br>
     * ================================= <br>
     * 1. If valueClass is defined and value object is of type CachedObjectConverter. 2. If collClass is defined then, process each of the strings, call convertToObject method on each value object and
     * add it to collection. 3. If no collClass defined, then just process the single string stored for object and return the corresponding object by calling convertToObject method. (This logic is
     * handled in getValueObject() api)
     *
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Object get(String key, Charset charset) throws CacheRetrieveException {
        checkGetArgs(key);
        Object obj = mcc.get(getKeyAsString(key));

        if (obj == null) {
            try {
                checkForGetError(key);
            } catch (CacheRetrieveException cre) {
                if (isCacheBackupEnabled()) {

                    obj = bkpmcc.get(getKeyAsString(key));
                    if (obj == null) {
                        checkForGetError(key);
                    }
                } else { // backup is not enabled.
                    throw cre;
                }
            }
            // object is null in primary... hence check in secondary as well.
            if (isCacheBackupEnabled()) {
                obj = bkpmcc.get(getKeyAsString(key));
                if (obj == null) {
                    checkForGetError(key);
                }
            } // end check for cache bkp enabled.
        } // end check for object is null.
        return obj;
    }

    /**
     * Get a map of key value objects.
     *
     * @param ids
     *            array of object ids
     *
     * @param charset Character set specifying encoding
     * @return cached object, NULL if cache miss
     *
     * @throws CacheRetrieveException
     *             if an error occured during key retrieval
     */
    public Map<String, Object> getMulti(String[] ids, Charset charset) throws CacheRetrieveException {

        final int length = ids.length;
        // logger.info("Length of the snp ids requested : " + ids.length);

        // memcache only accepts string as a key. Hence convert all long values
        // into strings and store it in a array.
        final String[] keys = new String[length];
        for (int i = 0; i < length; i++) {
            keys[i] = getKeyAsString(ids[i]);
        }
        // get the objects from memcache.
        Map<String, Object> map = mcc.getMulti(keys);

        return map;
    }

    @Override
    public Long incrBy(String key, long integer) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long incr(String key) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long decr(String key) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long decrBy(String key, long integer) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    /**
     * Get the value objects for multiple ids.
     *
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<Object> get(String[] ids) throws CacheRetrieveException {
        checkGetArgs(ids);

        final int length = ids.length;
        // logger.info("Length of the snp ids requested : " + ids.length);

        // memcache only accepts string as a key. Hence convert all long values
        // into strings and store it in a array.
        final String[] keys = new String[length];
        for (int i = 0; i < length; i++) {
            keys[i] = getKeyAsString(ids[i]);
        }
        // get the objects from memcache.
        Object[] objects = mcc.getMultiArray(keys);

        if (objects == null) { // cache miss OR error -- determine which it is
            try {
                checkForGetError(ids);
            } catch (CacheRetrieveException cre) {
                if (isCacheBackupEnabled()) {
                    objects = bkpmcc.getMultiArray(keys);
                    if (objects == null) {
                        checkForGetError(ids);
                    }
                } else { // backup is not enabled.
                    throw cre;
                }
            } // end catch block
             // object is null in primary... hence check in secondary as well.
            if (isCacheBackupEnabled()) {
                objects = bkpmcc.getMultiArray(keys);
                if (objects == null) {
                    checkForGetError(ids);
                }
            } // end check for cache bkp enabled.
        }
        if (isAllNullsInObjectArray(objects)) {
            // object is null in primary... hence check in secondary as well.
            if (isCacheBackupEnabled()) {
                objects = bkpmcc.getMultiArray(keys);
                if (objects == null) {
                    checkForGetError(ids);
                }
            } // end check for cache bkp enabled.
        }

        ArrayList<Object> cachedObjects = new ArrayList<Object>();

        if (objects != null) {
            // now calculate the length of the objects retrived from memcache.
            if (objects.length < length) {
                throw new CacheRetrieveException("Could not retrive all objects, requested " + length + ", got " + objects.length, getAndClearClientException());
            }
            cachedObjects = new ArrayList<Object>(objects.length);
            for (Object obj : objects) {
                cachedObjects.add(obj);
            }
        } // object not null.
        return cachedObjects;
    }

    /**
     * Following are the rules for the put method.<br>
     * <br>
     *
     * When not to process the value object:<br>
     * ==================================== <br>
     * 1. If value object is not of type CachedObjectConverter then add the value object to memcache as it is. 2. If value object is a collection, and elements within the collection are not of type
     * CachedObjectConverter then just put the collection in memcache. <br>
     * When to process the value object: <br>
     * ================================ <br>
     * 1. If value object is of type CachedObjectConverter, call convertToString() method on the value object and store the corresponding string inside memcache. 2. If value object is a collection and
     * elemetns within the collection are of type CachedObjectConverter then convert the entire collection in to a single string by calling the convertToString() method of each element and append it
     * to each of the objects. Currently we are using "^^" as the delimiter for record separation.
     *
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public boolean put(String key, Object val) throws CacheStoreException {
        boolean ret;
        checkPutArgs(key, val);
        ret = mcc.set(getKeyAsString(key), val);
        boolean bkpret = false;
        if (isCacheBackupEnabled()) {
            bkpret = bkpmcc.set(getKeyAsString(key), val);
        }
        if (!(ret || bkpret)) {
            throw new CacheStoreException("Could not store value for key " + key, getAndClearClientException());
        }
        return ret;
    }

    /**
     *
     * This method will be used if date of expiry needs to be mentioned.
     *
     * Following are the rules for the put method.<br>
     * <br>
     *
     * When not to process the value object:<br>
     * ==================================== <br>
     * 1. If value object is not of type CachedObjectConverter then add the value object to memcache as it is. 2. If value object is a collection, and elements within the collection are not of type
     * CachedObjectConverter then just put the collection in memcache. <br>
     * When to process the value object: <br>
     * ================================ <br>
     * 1. If value object is of type CachedObjectConverter, call convertToString() method on the value object and store the corresponding string inside memcache. 2. If value object is a collection and
     * elemetns within the collection are of type CachedObjectConverter then convert the entire collection in to a single string by calling the convertToString() method of each element and append it
     * to each of the objects. Currently we are using "^^" as the delimiter for record separation.
     *
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public boolean put(String key, Object val, Date dateOfExpiry) throws CacheStoreException {
        boolean ret;
        checkPutArgs(key, val);
        ret = mcc.set(getKeyAsString(key), val, dateOfExpiry);
        boolean bkpret = false;
        if (isCacheBackupEnabled()) {
            bkpret = bkpmcc.set(getKeyAsString(key), val, dateOfExpiry);
        }
        if (!(ret || bkpret)) {
            throw new CacheStoreException("Could not store value for key " + key, getAndClearClientException());
        }
        return ret;
    }

    @Override
    public boolean put(String key, Object val, int expirySeconds) throws CacheStoreException {
        Date dateOfExpiry = new Date(System.currentTimeMillis() + expirySeconds * 1000L);
        return put(key, val, dateOfExpiry);
    }

    @Override
    public boolean add(String key, Object val, int expirySeconds) throws CacheStoreException {
        boolean ret = mcc.add(getKeyAsString(key), val, expirySeconds);
        // TODO Auto-generated method stub
        return ret;
    }

    /**
     * Removes the key from cache.
     *
     * @param key
     *            Key
     * @return boolean true - if either of the servers successfully deleted the key. false - if both the servers failed to remove or if the key doesn't exist in the cache.
     */
    public boolean remove(String key) {
        boolean ret = mcc.delete(getKeyAsString(key));
        boolean bkpret = false;
        if (isCacheBackupEnabled()) {
            bkpret = bkpmcc.delete(getKeyAsString(key));
        }
        if (!(ret || bkpret)) {
            // both the servers failed to remove.
            logger.warn("Cache delete error occured on both primary and secondary server for the key: " + key + ". Either key doesn't exist in cache or exception occured while removing the key");
            return false;
        }
        return true;
    }

    // public void putAll(Map map) {
    // doPutAll(map);
    // }

    /**
     * Gets the memcache statistics. Primary statistics attributes are:
     *
     * 1. Number of elements in memcache 2. Amount of memory occupied by memcache. 3. Number of miss happened on the cache. 4. Number of evictions happened.
     *
     * @return Map map of statistics.
     * @throws CacheException
     *             exceptio is thrown if statistics returned is null.
     */
    @SuppressWarnings("unchecked")
    public Map getStatistics() throws CacheException {
        final Map ret = mcc.stats();
        if (ret == null) {
            throw new CacheException("Could not get stats from memcached server", getAndClearClientException());
        }

        return ret;
    }

    // /**
    // * Put a map of objects of type <K,V>. Inherited from map interface
    // *
    // * @param map map of objects
    // */
    // public void putAll(Map map) {
    // doPutAll(map);
    // }

    /**
     * Get and clear the client error.
     *
     * @param key
     *            --
     * @param getStopwatch
     *            --
     * @throws CacheRetrieveException
     *             if an error was set during the previous get operation.
     */
    private void checkForGetError(Object key) throws CacheRetrieveException {
        Throwable cause = getAndClearClientException();
        if (cause != null) {
            throw new CacheRetrieveException("Error when retrieving key " + key, cause);
        }
    }

    /**
     * Get the last client error set by mcc.
     *
     * @return Throwable last set by mcc
     */
    private Throwable getAndClearClientException() {
        Throwable cause = clientException.get();
        if (cause != null) {
            clientException.set(null);
        }
        return cause;
    }

    /**
     *
     * The isCacheBackupEnabled method for isCacheBackupEnabled.
     *
     * @return true or false.
     */
    private boolean isCacheBackupEnabled() {
        boolean cacheBackupEnabled = false;
        // Check if memcached itself is enabled or not, then check if backup cache is enabled or not.
        if (commonCachedProperties.getBooleanPropertyValue("ct.common.memcache.enabled", false)) {
            cacheBackupEnabled = commonCachedProperties.getBooleanPropertyValue("ct.services.memcached.backupservers.enabled", false);
        }
        return cacheBackupEnabled;
    }

    /**
     * This is a object null tester utitlty method.
     *
     * @param arr
     *            array.
     * @return true or false.
     */
    private boolean isAllNullsInObjectArray(Object[] arr) {
        if (arr == null) {
            return true;
        }
        boolean retval = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null) {
                retval = false;
                break;
            } // end if
        } // end for
        return retval;
    } // end method.

    /**
     * Error handler for memcached client.
     *
     * This is an inner class as it is not intended to be extended -- error handlers have somewhat tricky semantics, in particular they should *not* throw exceptions, as that will interfere with
     * socket cleanup
     *
     * @author twilsher
     */
    class MemcachedErrorHandler implements ErrorHandler {

        /**
         * {@inheritDoc}
         *
         * @see com.danga.MemCached.ErrorHandler#handleErrorOnDelete(com.danga.MemCached.MemCachedClient, Throwable, String)
         */
        public void handleErrorOnDelete(MemCachedClient arg0, Throwable arg1, String arg2) {
            myErrorHandler(arg1);
        }

        /**
         * {@inheritDoc}
         *
         * @see com.danga.MemCached.ErrorHandler#handleErrorOnFlush(com.danga.MemCached.MemCachedClient, Throwable)
         */
        public void handleErrorOnFlush(MemCachedClient arg0, Throwable arg1) {
            myErrorHandler(arg1);
        }

        /**
         * {@inheritDoc}
         *
         * @see com.danga.MemCached.ErrorHandler#handleErrorOnGet(com.danga.MemCached.MemCachedClient, Throwable, String)
         */
        public void handleErrorOnGet(MemCachedClient arg0, Throwable arg1, String key) {
            // Store the offending exception
            setThrowable(clientException, arg1);
        }

        /**
         * {@inheritDoc}
         *
         * @see com.danga.MemCached.ErrorHandler#handleErrorOnGet(com.danga.MemCached.MemCachedClient, Throwable, String[])
         *
         *      Need to flag that an error happened, as the memcached client returns null both in case of errors and when there is a genuine cache miss.
         *
         *      For now, we flag that an error happened by using a ThreadLocal to pass the flag. That is an expensive way of signalling errors, but the assumption is that cache misses are very
         *      infrequent, and this approach minimizes patching of the Memcache client code itself. If cache misses turns out to be fairly common (and therefore expensive) the memcached client code
         *      should be rewritten to throw an exception on error directly.
         */
        public void handleErrorOnGet(MemCachedClient arg0, Throwable arg1, String[] key) {
            setThrowable(clientException, arg1);
        }

        /**
         * Set the throwable last thrown by mcc.
         *
         * @param threadLocal
         *            -
         * @param throwable
         *            -
         */
        private void setThrowable(ThreadLocal<Throwable> threadLocal, Throwable throwable) {
            if (throwable == null) {
                throwable = new UnknownError("No Exception thrown");
            }
            threadLocal.set(throwable);
        }

        /**
         * {@inheritDoc}
         *
         * @see com.danga.MemCached.ErrorHandler#handleErrorOnInit(com.danga.MemCached.MemCachedClient, Throwable)
         */
        public void handleErrorOnInit(MemCachedClient arg0, Throwable arg1) {
            myErrorHandler(arg1);
        }

        /**
         * {@inheritDoc}
         *
         * @see com.danga.MemCached.ErrorHandler#handleErrorOnSet(com.danga.MemCached.MemCachedClient, Throwable, String)
         */
        public void handleErrorOnSet(MemCachedClient arg0, Throwable arg1, String arg2) {
            myErrorHandler(arg1);
        }

        /**
         * {@inheritDoc}
         *
         * @see com.danga.MemCached.ErrorHandler#handleErrorOnStats(com.danga.MemCached.MemCachedClient, Throwable)
         */
        public void handleErrorOnStats(MemCachedClient arg0, Throwable arg1) {
            myErrorHandler(arg1);
        }

        /**
         * My error handler.
         *
         * @param t
         *            throwable.
         */
        private void myErrorHandler(Throwable t) {
            final String err = "memcache error! ";
            // log.error(err);
            // throw new RuntimeException(err, t); //must throw exception to
            // abort index-update daemon thread
        }
    }
}
