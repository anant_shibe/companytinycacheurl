package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import com.cleartrip.clearhttp.api.HttpApi;
import com.cleartrip.clearhttp.api.HttpClient;
import com.cleartrip.clearhttp.api.HttpResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.service.AuthenticationService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.http.HttpStatus;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.web.context.support.WebApplicationContextUtils;
import scala.util.Try;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Optional;

public class FrequentFlyerFilter implements Filter {

    private static final Log logger = LogFactory.getLog(FrequentFlyerFilter.class);

    protected FilterConfig filterConfig;
    protected CachedProperties cachedProperties;
    private AuthenticationService authenticationService;
    private HttpClient httpClient;
    private HttpApi httpFreqFlyerUserApi;
    private static final String CT_FREQ_FLYER_SERVICE_BASE_URL = "ct.freq-flyer-service.base-url";
    private static final String CT_FREQ_FLYER_VIRTUAL_COMPANY_ID = "ct.freq-flyer.virtual.company-id";
    private static final String CT_AIR_FF_SERVICE_CONCURRENCY = "ct.air.freq-flyer-service.concurrency";
    private static final String CT_AIR_FF_SERVICE_TIMEOUT = "ct.air.freq-flyer-service.timeout";
    private static final String CT_AIR_FREQ_FLYER_PROGRAM_ENABLED = "ct.air.freq-flyer-program.enabled";
    private static final String CT_AIR_FREQ_FLYER_INTL_CORP_SEARCH_ENABLED = "ct.air.freq-flyer.intl.corp.search.enabled";
    private static final String CT_AIR_FREQ_FLYER_PROGRAM_COOKIE_MAX_AGE_MINUTES = "ct.air.freq-flyer-program.cookie.max-age.minutes";
    private static final String CLEARTRIP = "cleartrip.com";
    private static final String ME_DOMAINS="bh.cleartrip.com, kw.cleartrip.com, qa.cleartrip.com, om.cleartrip.com, me.cleartrip.com";
    private static final String DEMO_CLTP_COMPANY_ID = "101";
    private static final String FREQ_FLYER_COOKIE_NAME = "freq-flyer";
    private static final String AUTH_COOKIE_NAME = "ct-auth";
    private static final String FREQ_FLYER_USER_END_POINT = "/r1/freqFlyerService/v1/profile/user/${userId},${domain}";
    private static final String MOBILE_APP = "mobileApp";

    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;

        ApplicationContext ctx = this.getContext(filterConfig);

        Object beanObj;
        beanObj = ctx.getBean("commonCachedProperties");
        if (beanObj instanceof CachedProperties) {
            cachedProperties = (CachedProperties) beanObj;
        }

        beanObj = ctx.getBean("authenticationServiceBean");
        if (beanObj instanceof AuthenticationService) {
            authenticationService = (AuthenticationService) beanObj;
        }

        beanObj = ctx.getBean("clearHttpClient");
        if (beanObj instanceof HttpClient) {
            this.httpClient = (HttpClient) beanObj;
            try {
                this.httpFreqFlyerUserApi = getHttpFreqFlyerUserApi();
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Allows test cases to override where application context obtained from.
     * @param filterConfig which can be used to find the <code>ServletContext</code>
     * @return the Spring application context
     */
    protected ApplicationContext getContext(FilterConfig filterConfig) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        boolean isFFProgramEnabled = cachedProperties.getBooleanPropertyValue(CT_AIR_FREQ_FLYER_PROGRAM_ENABLED, false);
        if(isFFProgramEnabled) {
            setCorpFareParamsForFrequentFlyer(request, response);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

    private boolean isRequestFromB2COrMobile(ServletRequest request, boolean isMobile) {
        String source = StringUtils.trimToNull(request.getParameter("source"));
        // source == null is equivalent to B2C
        return isMobile || source == null || (StringUtils.isNotBlank(source) && source.equalsIgnoreCase(CommonEnumConstants.SourceType.B2C.name()));
    }

    private URL getFreqFlyerServiceBaseUrl() throws MalformedURLException {
        return new URL(cachedProperties.getPropertyValue(CT_FREQ_FLYER_SERVICE_BASE_URL,"http://freq-flyer-service.cltp.com:9001"));
    }

    private HttpApi getHttpApi(URL httpURL, String methodType, String endPoint) {
        int concurrency = cachedProperties.getIntPropertyValue(CT_AIR_FF_SERVICE_CONCURRENCY, 20);
        int timeout = cachedProperties.getIntPropertyValue(CT_AIR_FF_SERVICE_TIMEOUT, 500);
        return httpClient.apiSpecBuilder(httpURL.getHost(), httpURL.getHost(),
                httpURL.getPort(), concurrency, timeout).build().apiBuilder(methodType, endPoint)
                .withHeader("content-type", "application/json").build();
    }

    private HttpApi getHttpFreqFlyerUserApi() throws MalformedURLException {
        return getHttpApi(getFreqFlyerServiceBaseUrl(), HttpMethod.GET.name(), FREQ_FLYER_USER_END_POINT);
    }

    private Optional<Cookie> getCookieFromRequest(HttpServletRequest httpServletRequest, String cookieName) {
        return Optional.ofNullable(GenUtil.getCookieFromRequest(httpServletRequest, cookieName));
    }

    private Optional<Integer> getUserIdFromAuthCookie(Cookie authCookie) {
        try {
            HashMap<String, String> map = authenticationService.getPersonFromCookie(authCookie);
            return Optional.of(Integer.parseInt(String.valueOf(map.get(CommonEnumConstants.UserDetails.USER_ID.toString()))));
        } catch (Exception e) {
            logger.info("Could-not get user-id from auth cookie");
        }
        return Optional.empty();
    }

    private void addNotFFCookieToResponse(String domain, HttpServletResponse httpServletResponse) {
        Cookie isNotFFCookie = new Cookie(FREQ_FLYER_COOKIE_NAME,"false");
        int cookieAgeMinutes = cachedProperties.getIntPropertyValue(CT_AIR_FREQ_FLYER_PROGRAM_COOKIE_MAX_AGE_MINUTES, 10);
        isNotFFCookie.setMaxAge(60*cookieAgeMinutes);
        isNotFFCookie.setDomain(domain);
        isNotFFCookie.setPath("/");
        httpServletResponse.addCookie(isNotFFCookie);
    }

    private boolean isFrequentFlyer(Integer loggedInUserId, String domain, ServletResponse response) {
        domain = domain.substring(domain.indexOf(CLEARTRIP));
        Try<HttpResponse> httpResponseTry = httpFreqFlyerUserApi.execute(ImmutableMap.of("userId", loggedInUserId,"domain", domain));
        if(httpResponseTry.isSuccess()) {
            if (httpResponseTry.get().status() == HttpStatus.OK_200) {
                return true;
            }
            else {
                logger.info("User not a freq-flyer, adding cookie stating the same");
                addNotFFCookieToResponse(domain, (HttpServletResponse) response);
            }
        }
        else {
            logger.error("Failure while making get call to freq-flyer-service from airservice filter");
        }
        return false;
    }

    private void setCorpFareParamsForFrequentFlyer(ServletRequest request, ServletResponse response) {
        boolean isIntlSearch = isIntlSearchRequest(request);
        boolean isIntlCorpEnabled = cachedProperties.getBooleanPropertyValue(CT_AIR_FREQ_FLYER_INTL_CORP_SEARCH_ENABLED, false);
        if(isIntlSearch && !isIntlCorpEnabled)
            return;

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        Optional<Cookie> authCookie = getCookieFromRequest(httpServletRequest, AUTH_COOKIE_NAME);

        // For mobile app get authCookie from request headers
        boolean isMobileApp = isRequestFromMobileApp(request);
        if(isMobileApp) {
            authCookie = getAuthCookieForMobile(authCookie, request);
        }
        else {
            // Verify that country is IN for B2C
            if(!isCountryIN(httpServletRequest)) {
                return;
            }
        }
        if(!authCookie.isPresent()) {
            return;
        }
        String domain = authCookie.get().getDomain();
        if(!StringUtils.contains(domain, CLEARTRIP)) {
            return;
        }
        boolean isB2COrMobile = isRequestFromB2COrMobile(request, isMobileApp);
        if(!isB2COrMobile) {
            return;
        }
        // Is not a freq-flyer cookie is only in case of user is signed-in with specific domains (having cleartrip)
        Optional<Cookie> isNotFFOptionalCookie = getCookieFromRequest(httpServletRequest, FREQ_FLYER_COOKIE_NAME);
        if(isNotFFOptionalCookie.isPresent()) {
            return;
        }
        Optional<Integer> loggedInUserId = getUserIdFromAuthCookie(authCookie.get());
        if(!loggedInUserId.isPresent()) {
            return;
        }
        Integer userId = loggedInUserId.get();
        if(isFrequentFlyer(userId, domain, response)) {
            logger.info("Freq Flyer is true for domain = " + domain + " and userId = " + userId);
            setCorpFareParams(request);
        }
    }

    private boolean isRequestFromMobileApp(ServletRequest request) {
        String source = StringUtils.trimToEmpty(request.getParameter("source"));
        String host = StringUtils.trimToEmpty(((HttpServletRequest) request).getHeader("r_host"));
        if (host.contains(CLEARTRIP) && !isMEDomain(host) && host.equalsIgnoreCase("www.cleartrip.com")
                && source.equalsIgnoreCase(CommonEnumConstants.SourceType.MOBILE.name())
                && Boolean.valueOf(request.getParameter(MOBILE_APP))) {
            return true;
        }
        return false;
    }

    private boolean isIntlSearchRequest(ServletRequest request) {
        String inltSearch = StringUtils.trimToEmpty(request.getParameter("intl"));
        return inltSearch.equalsIgnoreCase("true") || inltSearch.equalsIgnoreCase("y");
    }

    private Optional<Cookie> getAuthCookieForMobile(Optional<Cookie> authCookie, ServletRequest request) {
        String r_cookie = StringUtils.trimToEmpty(((HttpServletRequest)request).getHeader("r_cookie"));
        if(r_cookie.contains("ct-auth=")) {
            String cookieValue = r_cookie.substring(r_cookie.indexOf("ct-auth=") + 8);
            cookieValue = cookieValue.substring(0, cookieValue.indexOf(";"));
            authCookie = Optional.of(new Cookie(AUTH_COOKIE_NAME, cookieValue));
            authCookie.get().setDomain(CLEARTRIP);
        }
        return authCookie;
    }

    private void setCorpFareParams(ServletRequest request) {
        request.setAttribute("showCorpFareForUser", "true");
        String companyId = cachedProperties.getPropertyValue(CT_FREQ_FLYER_VIRTUAL_COMPANY_ID, DEMO_CLTP_COMPANY_ID);
        request.setAttribute("companyid", companyId);
        request.setAttribute("enableCorpFare", "true");
    }

    private boolean isMEDomain(String domain) {
        return domain != null && !domain.isEmpty() &&
                ME_DOMAINS.contains(domain);
    }

    private boolean isCountryIN(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getAttribute("country") != null &&
                ((String)httpServletRequest.getAttribute("country")).equalsIgnoreCase("IN");
    }

}