package com.companyApiTinyCache.companyConfigCache.service.common.analytics.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.Optional;

public class AnalyticsCachingServiceImpl implements AnalyticsCachingService {

    private static final Log logger = LogFactory.getLog(AnalyticsCachingServiceImpl.class);

    private CachedProperties cachedProperties;

    private Cache cache;

    private static final String CT_SERVICES_AIR_ANALYTICS_MEMCACHED_SERVERS = "ct.services.air.memcached.servers";

    private static final String CT_SERVICES_AIR_ANALYTICS_CACHE_EXIPRY_IN_SECS = "ct.services.air.analytics.caching.expiry.secs";

    public AnalyticsCachingServiceImpl(CachedProperties cachedProperties, CacheFactory cacheFactory) {
        this.cachedProperties = cachedProperties;
        this.cache = cacheFactory.getCacheForServer(CT_SERVICES_AIR_ANALYTICS_MEMCACHED_SERVERS);
    }

    @Override
    public Optional<Map> getAnalyticsAttributeFromCache(String cacheKey) {
        Optional<Map> analyticsAttributeMap = Optional.empty();
        try {
            Object analyticsAttributes = cache.get(cacheKey);
            if(analyticsAttributes != null) {
                String analyticsAttributeJson = (String) analyticsAttributes;
                analyticsAttributeMap = Optional.of(JsonUtils.toObject(analyticsAttributeJson, Map.class));
            }
        }
        catch (Exception ex) {
            logger.error("Error while fetching analytics attribute from cache. " + ex.getMessage());
        }
        return analyticsAttributeMap;
    }

    @Override
    public void setAnalyticsAttributeInCache(String key, Map analyticsAttributeMap) {
        try {
            if(MapUtils.isNotEmpty(analyticsAttributeMap)) {
                String analyticsAttributeJson = JsonUtils.toJson(analyticsAttributeMap);
                cache.put(key, analyticsAttributeJson, cachedProperties.getIntPropertyValue(CT_SERVICES_AIR_ANALYTICS_CACHE_EXIPRY_IN_SECS, 300));
            }
        }
        catch (Exception ex) {
            logger.error("Error while setting analytics attribute into cache. " + ex.getMessage());
        }
    }
}
