/*
 * @(#) CompanyApiTinyCache.java
 *
 * Copyright 2010-2011 by Cleartrip Travel Services Pvt. Ltd.
 * Unit No 001, Ground Floor, DTC Bldg,
 * Sitaram Mills Compound, N.M. Joshi Marg,
 * Delisle Road,
 * Lower Parel (E)
 * Mumbai - 400011
 * India
 *
 * This software is the confidential and proprietary information
 * of Cleartrip Travel Services Pvt. Ltd. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of license agreement you
 * entered into with Cleartrip Travel Service Pvt. Ltd.
 */
package com.companyApiTinyCache.companyConfigCache.service.impl;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.connector.CompanyAPIConnector;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Affiliate;
import com.companyApiTinyCache.companyConfigCache.service.common.data.AffiliateConfig;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CompanyConfig;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * A more specific implementation of TinyCache for caching Affiliate objects.
 *
 * @author Amit Kumar Sharma
 * @see Affiliate
 */
public class CompanyApiTinyCache extends GenericTinyCache<Affiliate> {

    private static final String SERVER_PROPERTY = "ct.services.company.detail.memcached.server";

    private CompanyAPIConnector companyAPIConnector;

    private CachedProperties cachedProperties;

    private static final Log logger = LogFactory.getLog(CompanyApiTinyCache.class);

    private Cache memCache;

    public CompanyApiTinyCache(CompanyAPIConnector companyAPIConnector, int maxSize, CacheFactory cacheFactory) {
        super(maxSize);
        this.companyAPIConnector = companyAPIConnector;
        memCache = cacheFactory.getCacheForServer(SERVER_PROPERTY);
    }

    public void refresh() {

        synchronized (cache) {

            String key = null;
            Affiliate company = null;

            LRUCache<Affiliate> newCache = new LRUCache<Affiliate>(maxSize);

            // Iterate over the cache elements
            Iterator<String> e = cache.keySet().iterator();
            while (e.hasNext()) {
                key = e.next();
                // logger.error("Refreshing Key value in API Tiny Cache : "+key+" at : "+new Timestamp((new java.util.Date()).getTime()));

                // Check if newCache already has a mapping for the
                // given key. If so, skip this iteration.
                // This can happen as the cache contains 2 mappings
                // for a given affiliate-object :
                // Mapping #1: {key=companyId, value=AffiliateObj}
                // Mapping #2: {key=domainName, value=AffiliateObj}
                // Once a Affiliate object is discovered for a given key
                // (either by domain or companyId), mapping for both the
                // keys are added to the newCache.
                //
                // Hence, skip processing if newCache already contains the key :-]
                if (newCache.containsKey(key)) {
                    continue;
                }

                try {
                    Integer companyId = Integer.parseInt(key);
                    company = companyAPIConnector.getCompanyByCompanyIdWithoutPeople(companyId);
                    logger.debug("Found key (companyId) = " + companyId);
                    // logger.error("Found key (companyId) = "+companyId+" at : "+new Timestamp((new java.util.Date()).getTime()));
                } catch (NumberFormatException nex) {
                    // This means key is the domain-name
                    company = companyAPIConnector.getCompanyBySubdomainWithoutPeople(key, SourceType.B2C.toString(), APIUtil.getSourceString("COMMON", "COMPANYTINYAPICACHE", "REFRESH"), false);
                    logger.debug("Found key (domain) = " + key);
                    // logger.error("Found key (domain) = "+key+" at : "+new Timestamp((new java.util.Date()).getTime()));
                }

                // Add the mappings (against both domain name and company-id in newCache)
                if (company != null) {
                    Integer companyId = company.getAffiliateId();
                    newCache.put(company.getDomainName(), company); // key = domain
                    newCache.put(companyId.toString(), company); // key = companyid

                    logger.debug("Added mappings for domain=" + key + " ,companyId = " + companyId);
                    // logger.error("Added mappings for domain=" + key +" ,companyId = "+companyId+" at : "+new Timestamp((new java.util.Date()).getTime()));
                }

            }

            // Once map-iteration is done, clear the existing
            // map and add all the new elements to it.
            // This should be done before relinquishing the lock on
            // the cache-object.
            cache.clear();
            cache.putAll(newCache);
        }

        logger.info(">>>>>>>>>> CompanyApiTinyCache refreshed >>>>>>>>>>");
    }

    /**
     * Check in the cache if Affiliate already stored if not get it from the Companyconfig and cache it.
     *
     * @param companyId - companyId for fetching Companyconfig
     * @param isCorpSpecialFares - Corp special fares enabled or not
     * @return Affiliate - Affiliate object
     */
    @SuppressWarnings("unchecked")
    public Affiliate getCompanyByCompanyIdWithoutPeople(int companyId, boolean isCorpSpecialFares,boolean callCompanyConfigApi) {
        // pick this from memcache
        // put switch
        Object obj = null;

        logger.info("Memcache swicth enabled. Picking data from memcache server " + SERVER_PROPERTY);
        obj = memCache.get(String.valueOf(companyId));


        if (callCompanyConfigApi || obj == null) {
            // fetch affiliate from company config api if not present in cache
            logger.info("Data not available in cache. Fetching data via Company API Connector");
            boolean getOnlyCC = cachedProperties.getBooleanPropertyValue("ct.air.get.only.company.config.api", true);
            Affiliate affiliate =  null;
            if (getOnlyCC) {
            	affiliate = companyAPIConnector.getCompanyConfigOnlyByCompanyId(companyId);
            } else {
            	affiliate = companyAPIConnector.getCompanyByCompanyIdWithoutPeople(companyId);
            }
            if (!isCorpSpecialFares) {
                logger.info("isCorpSpecialFares disabled. Overriding fetched data with sme property.");
                updateAffiliateConfigsForNonCorpSpecialFares(companyId, affiliate);
            }
            logger.info("updating memcache with new data fetched from company api connector.");
            int expiryDateInHours = cachedProperties.getIntPropertyValue("ct.air.company.detail.memcache.expiry.in.hours", 24);
            long milliseconds = TimeUnit.HOURS.toMillis(expiryDateInHours);

            milliseconds += System.currentTimeMillis();
            Date dateOfExpiry = new Date(milliseconds);
            memCache.syncPut(String.valueOf(companyId), affiliate, dateOfExpiry);
            logger.info("successfully updated memcache with new data for key " + String.valueOf(companyId) + " expiring at " + dateOfExpiry);


            return affiliate;
        }

        if (!isCorpSpecialFares) {
            updateAffiliateConfigsForNonCorpSpecialFares(companyId, (Affiliate) obj);
        }

        return (Affiliate) obj;
    }

    /**
     * Check in the cache if Affiliate already stored if not get it from the Companyconfig and cache it.
     *
     * @param domainName - domainName for fetching Companyconfig
     * @return Affiliate - Affiliate object
     */
    @MethodExcludeCoverage
    @SuppressWarnings("unchecked")
    public Affiliate getCompanyByDomainNameWithoutPeople(String domainName) {
        Object obj = null;
        boolean memcachedEnabled = cachedProperties.getBooleanPropertyValue("ct.wl.company.detail.from.memcached.enabled", true);
        if (memcachedEnabled) {
            logger.info("Memcache switch enabled. Picking data from memcache server " + SERVER_PROPERTY);
            obj = memCache.get(domainName);
        } else {
            logger.info("Memcache swicth disabled. Picking data from tiny cache");
            obj = cache.get(String.valueOf(domainName));
        }
        if (obj == null) {
            // fetch affiliate from company config api if not present in cache
            logger.info("Data not available in cache. Fetching data via Company API Connector");
            Affiliate affiliate = companyAPIConnector.getCompanyBySubdomainWithoutPeople(domainName, SourceType.B2C.toString(), APIUtil.getSourceString("COMMON", "COMPANYCONFIG", "FILTER"), false);
            if (memcachedEnabled) {
                logger.info("updating memcache with new data fetched from company api connector.");
                putToCache(domainName, affiliate);
            } else {
                synchronized (cache) {
                    logger.info("updating tiny cache with new data fetched from company api connector.");
                    cache.put(domainName, affiliate);
                    return affiliate;
                }
            }
            return affiliate;
        }
        return (Affiliate) obj;
    }

    @MethodExcludeCoverage
    @SuppressWarnings("unchecked")
    public void putToCache(String domainName, Affiliate affiliate) {
        if (affiliate != null) {
            Date dateOfExpiry = null;
            int expiryDateInHours = cachedProperties.getIntPropertyValue("ct.air.company.detail.memcache.expiry.in.hours", 24);
            long milliseconds = (expiryDateInHours / 3600) * 1000;
            milliseconds = System.currentTimeMillis() + milliseconds;
            dateOfExpiry = new Date(milliseconds);
            memCache.syncPut(domainName, affiliate, dateOfExpiry);
            logger.info("successfully updated memcache with new data for key " + domainName + " expiring at " + dateOfExpiry);
        }
    }

    public void updateAffiliateConfigsForNonCorpSpecialFares(int companyId, Affiliate affiliate) {
        if (affiliate.getChannelName() != SourceType.CORP) {
            Map<CompanyConfig, AffiliateConfig> map = affiliate.getAffiliateConfigs();

            String smeCorpConfigs = cachedProperties.getPropertyValue("ct.air.sme.corp.fares.company.config.values");

            ObjectMapper mapper = JsonUtil.getObjectMapper();
            Map<String, String> smeCorpConfigMap = null;

            try {
                smeCorpConfigMap = (Map<String, String>) mapper.readValue(smeCorpConfigs, Map.class);
            } catch (Exception e) {
                logger.error("Exception is:" + e.getMessage(), e);
            }

            if (smeCorpConfigMap != null) {
                String cachingId = smeCorpConfigMap.get("CACHING_ID");

                Set<String> keys = smeCorpConfigMap.keySet();

                if (StringUtils.isNotBlank(cachingId)) {
                    // only if caching id is not null or empty do below
                    for (String key : keys) {
                        AffiliateConfig affiliateConfig = new AffiliateConfig();
                        affiliateConfig.setConfigName(CompanyConfig.valueOf(key));
                        affiliateConfig.setConfigValue(smeCorpConfigMap.get(key));
                        affiliateConfig.setAffiliateId(companyId);
                        if(map != null) {
                            map.put(CompanyConfig.valueOf(key), affiliateConfig);
                        }
                    }
                }
            }
        }
    }

    /**
     * Given a key look up the Affiliate cache and return the value.
     *
     * @param companyId - companyId
     * @param key - Key
     * @param isCorpSpecialFares - Corp special fares or not
     * @return String - String
     */
    public String getKeyFromCache(Long companyId, String key, boolean isCorpSpecialFares) {

        if (companyId != null && key != null && companyId.intValue() > 0) {

            Affiliate affiliate = getCompanyByCompanyIdWithoutPeople(companyId.intValue(), isCorpSpecialFares,false);
            if (affiliate != null) {
                Map<CompanyConfig, AffiliateConfig> map = affiliate.getAffiliateConfigs();

                /**
                 * If cache has corrupt data, get affiliate details from company api
                 */
                if(map == null){
                    logger.error("Cache corrupted for companyId - "+companyId+", Getting data from Api");
                    affiliate = getCompanyByCompanyIdWithoutPeople(companyId.intValue(), isCorpSpecialFares, true);
                    if(affiliate != null){
                        map = affiliate.getAffiliateConfigs();
                    }
                }
                // PRM-7205
                if (key.contains("6E")) {
                    key = key.replace("6E", "INDIGO");
                }
                // Ended
                if (key != null && map.get(CompanyConfig.valueOf(key)) != null) {
                    return map.get(CompanyConfig.valueOf(key)).getConfigValue();
                }
            }
        }
        return null;
    }

    /**
     * Check in the cache if Affiliate already stored if not get it from the Companyconfig and cache it.
     *
     * @param companyId - CompanyId to get the company config
     * @param source - Calling source
     * @param caller - Caller Id to be sent to accounts api
     * @return Affiliate - Affiliate Object
     */
    @SuppressWarnings("unchecked")
    public Affiliate getCompanyByCompanyIdWithoutPeople(int companyId, String source, String caller) {
        logger.info("Memcache swicth enabled. Picking data from memcache server " + SERVER_PROPERTY);
        Object obj = memCache.get(String.valueOf(companyId));
        if (obj == null) {
            // fetch affiliate from company config api if not present in the memcache
            logger.info("Data not available in cache. Fetching data via Company API Connector");
            Affiliate affiliate = companyAPIConnector.getCompanyByCompanyIdWithoutPeople(companyId, source, caller);
            logger.info("updating memcache with new data fetched from company api connector.");
            Date dateOfExpiry = null;
            int expiryDateInHours = cachedProperties.getIntPropertyValue("ct.air.company.detail.memcache.expiry.in.hours", 24);
            long milliseconds = (expiryDateInHours / 3600) * 1000;
            milliseconds = System.currentTimeMillis() + milliseconds;
            dateOfExpiry = new Date(milliseconds);
            memCache.syncPut(String.valueOf(companyId), affiliate, dateOfExpiry);
            logger.info("successfully updated memcache with new data for key " + String.valueOf(companyId) + " expiring at " + dateOfExpiry);
            return affiliate;
        }
        return (Affiliate) obj;
    }

    public void setCachedProperties(CachedProperties cachedProperties) {
        this.cachedProperties = cachedProperties;
    }

    /**
     * Checks if Corp Fares are enabled for this companyId or not.
     * This method breaks the myth of creating a company Id for a partner only if they want Corp Fares.
     * @param companyId
     * @return isCorpFareEnabled
     */
    public boolean isCorpFareEnabled(int companyId) {
    	boolean isCorpFareEnabled = false;
    	if (companyId > 0) {
    		String value = getKeyFromCache(Long.valueOf(companyId), "CORP_FARES_ENABLED", true);	//  passing true so that no update is performed
    		if (value != null && (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("y") || value.equalsIgnoreCase("yes"))) {
    			isCorpFareEnabled = true;
    		}
    	}
    	
    	return isCorpFareEnabled;
    }

}
