package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonSecurityFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

@ClassExcludeCoverage
public class SearchSecurityFilter extends CommonSecurityFilter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        long startTime = System.currentTimeMillis();
        request.setAttribute("start_time", Long.toString(startTime));

        super.doFilter(request, response, chain);
    }

    /**
     * This doInit() gets the bean from the application context.
     * 
     * @throws ServletException
     */
    public void doInit() throws ServletException {
        super.doInit();
    }

}
