package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.AffiliateTourCode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author amith
 *
 */
public class AffiliateTourCodesHolder implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 4212269194108333446L;

    @JsonProperty("company_tourcodes")
    private List<AffiliateTourCode> affiliateTourCodes;

    public AffiliateTourCodesHolder() {
        affiliateTourCodes = new ArrayList<AffiliateTourCode>(1);
    }

    @JsonProperty("company_tourcodes")
    public List<AffiliateTourCode> getAffiliatetourCodes() {
        return affiliateTourCodes;
    }

    @JsonProperty("company_tourcodes")
    public void setAffiliatetourCodes(List<AffiliateTourCode> affiliatetourCodes) {
        this.affiliateTourCodes = affiliatetourCodes;
    }
}
