package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AffiliateTourCode implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = -7358453946611268505L;

    @JsonIgnore
    private int affiliateId;

    private String airlineCode;

    private String tourCode;

    @JsonIgnore
    public int getAffiliateId() {
        return affiliateId;
    }

    @JsonIgnore
    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    @JsonProperty("airline_code")
    public String getAirlineCode() {
        return airlineCode;
    }

    @JsonProperty("airline_code")
    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    @JsonProperty("tourcode")
    public String getTourCode() {
        return tourCode;
    }

    @JsonProperty("tourcode")
    public void setTourCode(String tourCode) {
        this.tourCode = tourCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + affiliateId;
        result = prime * result + ((airlineCode == null) ? 0 : airlineCode.hashCode());
        result = prime * result + ((tourCode == null) ? 0 : tourCode.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AffiliateTourCode other = (AffiliateTourCode) obj;
        if (affiliateId != other.affiliateId)
            return false;
        if (airlineCode == null) {
            if (other.airlineCode != null)
                return false;
        } else if (!airlineCode.equals(other.airlineCode))
            return false;
        if (tourCode == null) {
            if (other.tourCode != null)
                return false;
        } else if (!tourCode.equals(other.tourCode))
            return false;
        return true;
    }

}
