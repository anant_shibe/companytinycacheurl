package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.RestResponse;

public class PromoServiceInvokerResponse {

	private boolean isKafkaPushSuccessful;
	private RestResponse response;

	/**
	 * Request Params
	 */
	private String payLoad;
	private String payloadType;
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPayLoad() {
		return payLoad;
	}

	public void setPayLoad(String payLoad) {
		this.payLoad = payLoad;
	}

	public String getPayloadType() {
		return payloadType;
	}

	public void setPayloadType(String payloadType) {
		this.payloadType = payloadType;
	}

	public boolean isKafkaPushSuccessful() {
		return isKafkaPushSuccessful;
	}

	public void setKafkaPushSuccessful(boolean isKafkaPushSuccessful) {
		this.isKafkaPushSuccessful = isKafkaPushSuccessful;
	}

	public RestResponse getResponse() {
		return response;
	}

	public void setResponse(RestResponse response) {
		this.response = response;
	}
}
