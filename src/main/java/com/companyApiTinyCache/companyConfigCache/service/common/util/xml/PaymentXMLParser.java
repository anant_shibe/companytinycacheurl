package com.companyApiTinyCache.companyConfigCache.service.common.util.xml;

import com.companyApiTinyCache.companyConfigCache.service.common.data.PayDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;

/**
 * PaymentXMLParser.
 * @author deepa
 */
public final class PaymentXMLParser {

    private PaymentXMLParser() {
        // TODO Auto-generated constructor stub
    }

    private static final Log logger = LogFactory.getLog(PaymentXMLParser.class);

    public static List<PayDetail> getCardTypes(String xml) {

        Document document;
        List<PayDetail> payDetails = new ArrayList<PayDetail>();
        try {
            document = XPathHelper.createDocument(xml);
            Element rootElement = document.getDocumentElement();

            NodeList cardTypes = XPathHelper.selectNodes("//card-type", rootElement);
            for (int i = 0; i <= cardTypes.getLength() - 1; i++) {
                Node cardNode = cardTypes.item(i);
                PayDetail payDetail = new PayDetail();
                payDetail.setId(XPathHelper.selectNodeIntValue("id", (Element) cardNode));
                payDetail.setName(XPathHelper.selectNodeValue("name", (Element) cardNode));
                payDetail.setStatus(XPathHelper.selectNodeValue("status", (Element) cardNode));
                payDetails.add(payDetail);
            }

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        }

        return payDetails;
    }

    @SuppressWarnings("unchecked")
    public static List<PayDetail> getThirdPartyWallets(String json) {

        List<PayDetail> payDetails = new ArrayList<PayDetail>();
        try {
            ObjectMapper objectMapper = JsonUtil.getObjectMapper();
            List<Map<String, Object>> wallets = objectMapper.readValue(json, List.class);
            for (Map<String, Object> wallet : wallets) {
                PayDetail payDetail = new PayDetail();
                payDetail.setId(Integer.parseInt(String.valueOf(wallet.get("id"))));
                payDetail.setName(String.valueOf(wallet.get("display_name")));
                payDetail.setValue(String.valueOf(wallet.get("value")));
                payDetail.setStatus(String.valueOf(wallet.get("status")));
                payDetail.setDomains((List<String>) wallet.get("domains"));
                payDetail.setChannels((List<String>) wallet.get("channels"));
                payDetails.add(payDetail);
            }

        } catch (Exception e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        return payDetails;
    }

    public static List<PayDetail> getBankTypes(String xml) {

        Document document;
        List<PayDetail> payDetails = new ArrayList<PayDetail>();
        try {
            document = XPathHelper.createDocument(xml);
            Element rootElement = document.getDocumentElement();

            NodeList cardTypes = XPathHelper.selectNodes("//bank", rootElement);
            for (int i = 0; i <= cardTypes.getLength() - 1; i++) {
                Node bankNode = cardTypes.item(i);
                PayDetail payDetail = new PayDetail();
                payDetail.setId(XPathHelper.selectNodeIntValue("id", (Element) bankNode));
                payDetail.setName(XPathHelper.selectNodeValue("name", (Element) bankNode));
                payDetail.setStatus(XPathHelper.selectNodeValue("status", (Element) bankNode));

                /* Code added to add the domain information in PAYDETAIL OBJECT */
                List<String> netBankingBankdomainList = null;
                /* Get <domains> tag for the bank */
                Node domainsNode = DOMHelper.selectNode("domains", bankNode);

                /* domains node is only available for NB banks */
                if (domainsNode != null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("domains found for the bank detail ");
                    }

                    Iterable<Node> domainsAvailableForbank = DOMHelper.selectNodes("domain", domainsNode);
                    // create domain-name list only if list of domain tag found
                    if (domainsAvailableForbank != null) {
                        netBankingBankdomainList = new ArrayList<String>();
                        Iterator<Node> domainsAvailableForbankIterator = domainsAvailableForbank.iterator();

                        /* Iterate through list of all domains for a bank */
                        while (domainsAvailableForbankIterator.hasNext()) {
                            Node domainNode = domainsAvailableForbankIterator.next();
                            netBankingBankdomainList.add(DOMHelper.selectNodeValue("", domainNode));
                        }
                    }
                }

                payDetail.setDomains(netBankingBankdomainList);
                // ****CODE ADDED TO ADD EMI OPTIONS IN PAYDETAIL OBJECT.
                // NodeList emiOptions = XPathHelper.selectNodes("//emi-option", (Element) cardNode);
                Node emiOptionsNode = DOMHelper.selectNode("emi-options", bankNode);
                Iterable<Node> emiOptionNodes = DOMHelper.selectNodes("emi-option", emiOptionsNode);
                Map<Integer, Double> emiOptionsMap = new HashMap<Integer, Double>();
                for (Node emiOptionNode : emiOptionNodes) {
                    emiOptionsMap.put(DOMHelper.selectNodeIntValue("emi", emiOptionNode), DOMHelper.selectNodeDoubleValue("amount", emiOptionNode));
                }
                payDetail.setEmiOptionsMap(emiOptionsMap);
                // ****

                int nodeCount = XPathHelper.selectNodeCount("otp-mode", (Element) bankNode);
                if (nodeCount > 0) {
                    payDetail.setOtpEnabled(true);
                    String otpMode = XPathHelper.selectNodeValue("otp-mode", (Element) bankNode);
                    if (otpMode != null && "MANUAL".equalsIgnoreCase(otpMode)) {
                        payDetail.setAutootp(false);
                        payDetail.setOtpInstruction(XPathHelper.selectNodeValue("otp-instruction", (Element) bankNode));
                    } else {
                        payDetail.setAutootp(true);
                    }
                } else {
                    payDetail.setOtpEnabled(false);
                }
                payDetails.add(payDetail);
            }

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        }

        return payDetails;
    }

    public static List<PayDetail> getCashCardTypes(String xml) {

        Document document;
        List<PayDetail> payDetails = new ArrayList<PayDetail>();
        try {
            document = XPathHelper.createDocument(xml);
            Element rootElement = document.getDocumentElement();

            NodeList cardTypes = XPathHelper.selectNodes("//card", rootElement);
            for (int i = 0; i <= cardTypes.getLength() - 1; i++) {
                Node cardNode = cardTypes.item(i);
                PayDetail payDetail = new PayDetail();
                payDetail.setId(XPathHelper.selectNodeIntValue("id", (Element) cardNode));
                payDetail.setName(XPathHelper.selectNodeValue("name", (Element) cardNode));
                payDetail.setStatus(XPathHelper.selectNodeValue("status", (Element) cardNode));
                payDetails.add(payDetail);
            }

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error("Exception is:" + e.getMessage(), e);
        }

        return payDetails;
    }
}
