package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class AffiliateLookupRole {

    private int id;

    private String role;

    private String description;

    private Date createDate;

    private Date updateDate;

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("role")
    public String getRole() {
        return role;
    }

    @JsonProperty("role")
    public void setRole(String role) {
        this.role = role;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("create_date")
    public Date getCreateDate() {
        return createDate;
    }

    @JsonProperty("create_date")
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @JsonProperty("update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    @JsonProperty("update_date")
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

}
