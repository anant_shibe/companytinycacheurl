package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.io.InputStream;

public interface StreamResourceLoader<R> {
    R loadStreamResource(InputStream is) throws Exception;
}
