package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.ConnectorStats;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedResource;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.LogLevel;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * Refreshes a CachedResource in jvm identified by the beanName parameter.
 *
 * @author suresh
 *
 */
public class RefreshCachedResourceController extends AbstractController {

    public RefreshCachedResourceController() {
        setCacheSeconds(0);
    }

    private ApplicationContext getContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.web.servlet.mvc.AbstractController#handleRequestInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ConnectorStats stats = new ConnectorStats();
        try {
            stats.setLogLevel(LogLevel.WARNING);
            stats.doInit(request, BaseStats.HOTEL, null, false, false);
            BaseStats.THREADLOCAL.set(stats);
            String beanName = request.getParameter("beanName");
            boolean forceRefresh = "1".equals(request.getParameter("force"));
            ApplicationContext ctx = getContext();
            CachedResource cachedResource = (CachedResource) ctx.getBean(beanName);
            boolean refreshed = cachedResource.refreshResource(0, forceRefresh);
            String message = refreshed ? "Successfully Refreshed Resource" : "Skipped Refresh";
            response.setContentType("text/plain");
            PrintWriter op = response.getWriter();
            op.println(message);
            String logMessages = stats.getLogOutput();
            if (logMessages != null) {
                op.println();
                op.println("WARNING: There Could be an Error in the Resource Data.");
                op.println();
                op.println("Please go through the below logs for Warnings and Errors:");
                op.println();
                op.println(logMessages);
            }
        } finally {
            BaseStats.THREADLOCAL.remove();
        }
        return null;
    }

}
