package com.companyApiTinyCache.companyConfigCache.service.common.data;

public class HealthCheckResponse {

    private int statusCode;
    private String url;
    private String urlToCheck;
    private String appName;

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrlToCheck(String urlToCheck) {
        this.urlToCheck = urlToCheck;
    }

    public String getUrlToCheck() {
        return urlToCheck;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppName() {
        return appName;
    }
}
