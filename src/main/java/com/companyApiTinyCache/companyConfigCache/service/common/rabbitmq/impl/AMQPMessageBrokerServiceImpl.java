package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.impl;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.config.RabbitMQConnectionConfig;
import com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions.*;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;

@ClassExcludeCoverage
public class AMQPMessageBrokerServiceImpl implements AMQPMessageBrokerService, AMQPMessageBrokerConstatnts {
	/**
	 * create AMQP connection using the credentials in map keys of input map
	 * should be {"hostName", "userName", "vHost"(optional), "password", "port"}
	 * 
	 * @param connectionCredentials
	 * @return
	 */
	public Connection getConnection(Map<String, Object> connectionCredentials) throws RabbitMQConnectionException {
		String hostName = null;
		String userName = null;
		String password = null;
		String vHost = null;
		String portAsString = null;
		boolean isAutorecover = false;
		Connection connection = null;
		int port = 156729;
		if (MapUtils.isNotEmpty(connectionCredentials)) {
			hostName = (String) connectionCredentials.get(AMQP_HOST_KEY);
			userName = (String) connectionCredentials.get(AMQP_USER_NAME_KEY);
			password = (String) connectionCredentials.get(AMQP_PASSWORD_KEY);
			vHost = (String) connectionCredentials.get(AMQP_VHOST);
			portAsString = (String) connectionCredentials.get(AMQP_PORT_KEY);
            isAutorecover = (boolean) connectionCredentials.get(AMQP_AUTO_RECOVER);
		}
		try {
			connection = getConnection(hostName, userName, password, portAsString, vHost, isAutorecover);
		} catch (Exception exception) {
			throw new RabbitMQConnectionException(exception.getMessage());
		}
		return connection;

	}

	/**
	 * creates a connection using amqp broker uri and the format for
	 * amqp_brokeruri is amqp_URI = "amqp://" amqp_authority [ "/" vhost ] [ "?"
	 * query ] amqp_authority = [ amqp_userinfo "@" ] host [ ":" port ]
	 * amqp_userinfo = username [ ":" password ] username = *( unreserved /
	 * pct-encoded / sub-delims ) password = *( unreserved / pct-encoded /
	 * sub-delims ) vhost = segment
	 * 
	 * @param amqp_brokerUri
	 * @return
	 */
	public Connection getConnection(final String amqp_brokerUri) throws RabbitMQConnectionException {
		Connection connection = null;
		// Validation of url needs to be added
		if (isNotBlank(amqp_brokerUri)) {
			try {
				ConnectionFactory factory = new ConnectionFactory();
				factory.setUri(amqp_brokerUri);
				connection = factory.newConnection();
			} catch (Exception exception) {
				if (exception.getMessage() != null) {
					throw new RabbitMQConnectionException(exception.getMessage());
				} else {
					throw new RabbitMQConnectionException(exception.getCause());
				}
			}
		} else {
			throw new RabbitMQConnectionException("Broker URI field can not be blank");
		}
		return connection;
	}

	/**
	 * This method creates a connection with rabbitmq server based on the data
	 * in connectionconfig
	 * 
	 * @param connectionConfigs
	 * @return
	 */
	public Connection getConnection(RabbitMQConnectionConfig connectionConfigs) throws RabbitMQConnectionException {
		Connection connection = null;
		if (connectionConfigs != null) {
			String hostName = connectionConfigs.getHostName();
			String vhostName = connectionConfigs.getVhostName();
			String userName = connectionConfigs.getUserName();
			String password = connectionConfigs.getPassword();
			String portAsString = connectionConfigs.getPort();
			boolean isAutoRecover = connectionConfigs.isAutoRecoverable();
			try {
				connection = getConnection(hostName, userName, password, portAsString, vhostName, isAutoRecover);
			} catch (Exception exception) {
				throw new RabbitMQConnectionException(exception.getMessage());
			}
		} else {
			throw new RabbitMQConnectionException(
					"Plz provide mandatory connection parameters as part of config object for creating connection");
		}
		return connection;
	}

	/**
	 * creates a connection to amqp server if all fields are provided if any one
	 * of these is empty then it will point to local by default
	 * 
	 * @param hostName
	 * @param userName
	 * @param password
	 * @param portNo
	 * @param vHost
	 * @return
	 * @throws Exception
	 */
	public Connection getConnection(String hostName, String userName, String password, String portNoAsString,
			String vHost, boolean isAutorecoverable) throws Exception {
		Connection connnection = null;
		try {
			ConnectionFactory factory = new ConnectionFactory();
			if (isNotBlank(hostName)) {
				factory.setHost(hostName);
			} else {
				throw new Exception("HostName can not be empty");
			}
			if (isNotBlank(userName)) {
				factory.setUsername(userName);
			} else {
				throw new Exception("Username can not be empty");
			}
			if (isNotBlank(password)) {
				factory.setPassword(password);
			} else {
				throw new Exception("Password can not be empty");
			}
			if (isNotBlank(vHost)) {
				factory.setVirtualHost(vHost);
			}
			if (isNotBlank(portNoAsString)) {
				int portNo = Integer.parseInt(portNoAsString);
				factory.setPort(portNo);
			} else {
				throw new Exception("port no for amqp can not be empty");
			}
			if(isAutorecoverable) {
				factory.setAutomaticRecoveryEnabled(isAutorecoverable);
			}
			connnection = factory.newConnection();
		} catch (Exception exception) {
			throw exception;
		}
		return connnection;
	}

	/**
	 * this method will create a queue like produer will produce to queue and
	 * consumer is goint to read from queue no binding and no exchange and no
	 * routing concept is there.
	 * 
	 * @param queueName
	 * @param message
	 * @param connection
	 * @throws RabbitMQConnectionException
	 */
	public void publishToAMQPQueue(final String queueName, String message, Connection connection)
			throws RabbitMQPublishMesaageException {
		Channel channel = null;
		if (isNotBlank(queueName) && isNotBlank(message) && connection != null) {
			try {
				channel = connection.createChannel();
				channel.queueDeclare(queueName, true, false, false, null);
				channel.basicPublish("", queueName, null, message.getBytes());
			} catch (Exception exception) {
				throw new RabbitMQPublishMesaageException("Exception occured during publishing message to queue",
						exception);
			} finally {
				try {
					if (channel != null) {
						channel.close();
					}
					if (connection != null) {
						connection.close();
					}
				} catch (Exception exception) {
					throw new RabbitMQPublishMesaageException("Exception occured during closing the connection",
							exception);
				}
			}
		} else {
			if (isBlank(queueName)) {
				throw new RabbitMQPublishMesaageException("Queue name can not be empty plz provide queuename");
			} else if (isBlank(message)) {
				throw new RabbitMQPublishMesaageException("message can not be empty it must contains some data");
			} else if (connection == null) {
				throw new RabbitMQPublishMesaageException(
						"Connection can not be null so plz create connection before adding message to queue");
			}
		}
	}

	public Channel createChannel(Connection connection, String queueName, boolean isDurable)
			throws RabbitMQCreateChannelException {
		Channel channel = null;
		try {
			if (connection != null && isNotBlank(queueName)) {
				channel = connection.createChannel();
				channel.queueDeclare(queueName, true, false, false, null);
			}
		} catch (Exception exception) {
			throw new RabbitMQCreateChannelException(exception.getMessage());
		}
		return channel;
	}
	
	public Channel createChannelWithBindingDetails(Connection connection, final String queueName, final String routingKey,
			final String exchangeName ) {
		Channel channel = null;
		if(isNotBlank(queueName) && isNotBlank(routingKey) && isNotBlank(exchangeName) && connection != null) {
			try {
			channel = connection.createChannel();
			channel.queueDeclare(queueName, true, false, false, null);
			channel.exchangeDeclare(exchangeName, AMQP_DIRECT, true);
			channel.queueBind(queueName, exchangeName, routingKey);
			} catch(Exception exception) {
				throw new AMQPChannelCreationException(exception);
			}
		} else {
			if(isBlank(queueName)) {
				throw new AMQPChannelCreationException("Queue name can not be empty");
			} else if (isBlank(routingKey)) {
				throw new AMQPChannelCreationException("routing key can not be empty");
			} else if (isBlank(exchangeName)) {
				throw new AMQPChannelCreationException("exchange name can not be blank");
			} else if (connection == null) {
				throw new AMQPChannelCreationException("connection is null");
			}
		}
		return channel;
	}

	/**
	 * method is used to pushing data to queue all fields are mandatory
	 * 
	 * @param queueName
	 * @param bindingKey
	 * @param routingKey
	 * @param exchangeName
	 * @param message
	 * @param connection
	 * @throws RabbitMQPublishMessageExceptionWithBindingKey
	 */
	public void pushToAMQPQueueWithBindingAndRoutingKey(final String queueName,
			final String routingKey, final String exchangeName, final String message, Connection connection)
			throws RabbitMQPublishMessageExceptionWithBindingKey {
		Channel channel = null;
		  try {
		        channel = createChannelWithBindingDetails(connection, queueName, routingKey, exchangeName);
				channel.basicPublish(exchangeName, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN,
						message.getBytes());
			} catch (Exception exception) {
				throw new RabbitMQPublishMessageExceptionWithBindingKey(
						"Exception occured during adding data to queue ", exception);
			} finally {
				try {
					if (channel != null) {
						channel.close();
					}
					if (connection != null) {
						connection.close();
					}
				} catch (Exception exception) {
					throw new RabbitMQPublishMessageExceptionWithBindingKey(
							"Exception during closing channel and connection", exception);
				}
			}
	}

	/*public void consumeMessageFromQueue(Connection connection, String queueName, int prefetchCount, boolean autoAck,
			IMarkerRabitMQConsumer amqpConsumer) throws RabbitMQConsumerException {
			try {
				Channel channel = connection.createChannel();
				channel.queueDeclare(queueName, true, false, false, null);
				if (prefetchCount <= 0) {
					channel.basicQos(1);
				} else {
					channel.basicQos(prefetchCount);
				}
				final Consumer consumer = new DefaultConsumer(channel) {
					@Override
					public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties,
							byte[] body) throws IOException {
						String message = new String(body, "UTF-8");
						try {
							amqpConsumer.proocessMessage(message);
							channel.basicAck(envelope.getDeliveryTag(), false);
						} catch (Exception exception) {
							channel.basicAck(envelope.getDeliveryTag(), true);
							// throw customize exception
						}
					}

				};
				channel.basicConsume(queueName, autoAck, consumer);
			} catch (Exception exception) {
				throw new RabbitMQConsumerException("Exception occured during reading message from queue:", exception);
			} 
	}
*/}

