package com.companyApiTinyCache.companyConfigCache.service.common.util.rest;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Simple Mock Rest Client returning preset response info.
 *
 * @author suresh
 */
@ClassExcludeCoverage
public class MockRestClientImpl implements RestClient {

    private RestResponse restResponse;

    public MockRestClientImpl(String responseContent, int responseStatus, Map<String, List<String>> responseHeaders) {
        restResponse = new RestResponse(responseContent);
        restResponse.setStatus(responseStatus);
        restResponse.setHeaders(responseHeaders);
    }

    public MockRestClientImpl(String responseContent, int responseStatus) {
        this(responseContent, responseStatus, null);

    }

    public MockRestClientImpl(String responseContent) {
        this(responseContent, 200, null);
    }

    @Override
    public RestResponse get(RestRequest request) throws IOException {
        return restResponse;
    }

    @Override
    public RestResponse post(RestRequest request) throws IOException {
        return restResponse;
    }

    @Override
    public RestResponse put(RestRequest request) throws IOException {
        return restResponse;
    }

    @Override
    public RestResponse delete(RestRequest request) throws IOException {
        return restResponse;
    }

    public RestResponse getRestResponse() {
        return restResponse;
    }

    public void setRestResponse(RestResponse prestResponse) {
        restResponse = prestResponse;
    }

}
