package com.companyApiTinyCache.companyConfigCache.service.common.connector;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Affiliate;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CompanyDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.data.FaultDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.data.UserProfile;
import com.companyApiTinyCache.companyConfigCache.service.common.data.holder.AffiliateHolder;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.ConnectorStats;
import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that talks to Accounts API to get Company Details based on provided criteria.
 * @author Abhishek
 */
@ClassExcludeCoverage
public class CompanyAPIConnector extends APIConnectorImpl {

    private static final Log logger = LogFactory.getLog(CompanyAPIConnector.class);

    private CachedProperties commonCachedProperties;

    private static final String COMPANY_CREATE_URL_PROPERTY = "ct.services.api.company.create.url";
    private static final String COMPANY_CREATE_URL_TIMEOUT_PROPERTY = "ct.services.api.company.create.url.timeout";

    private static final String COMPANY_RETRIEVE_URL_PROPERTY = "ct.services.api.company.retrieve.url";
    private static final String COMPANY_CONFIG_ONLY_RETRIEVE_URL_PROPERTY = "ct.services.api.company.config.only.retrieve.url";
    private static final String COMPANY_CONFIG_ONLY_RETRIEVE_URL_TIMEOUT_PROPERTY = "ct.services.api.company.config.only.retrieve.url.timeout";

    private static final String COMPANY_SEARCH_URL_PROPERTY = "ct.services.api.company.search.url";

    private static final String COMPANY_SEARCH_URL_TIMEOUT_PROPERTY = "ct.services.api.company.search.url.timeout";
    private static final String COMPANY_RETRIEVE_URL_TIMEOUT_PROPERTY= "ct.services.company.users.retrieve.url.timeout";


    private static final String X_CT_Source_Type = "X-CT-Source-Type";

    private static final String AFFILIATE_ID_PLACEHOLDER_REGEX = "\\$\\{affiliate-id}";

    /**
     * Create a new Affiliate.
     * @param affiliate Affiliate
     * @return Affiliate
     * @author Ulhas
     */
    public Affiliate createCompany(Affiliate affiliate) {
        return createCompany(affiliate, "");
    }

    /**
     * Creates a new Affiliate.
     * @param affiliate Affiliate
     * @param source String
     * @return Affiliate
     */
    public Affiliate createCompany(Affiliate affiliate, String source) {
        Affiliate company = new Affiliate();

        AffiliateHolder affiliateHolder = new AffiliateHolder();
        affiliateHolder.setAffiliate(affiliate);

        String urlLocation = commonCachedProperties.getPropertyValue(COMPANY_CREATE_URL_PROPERTY);
        int timeout = commonCachedProperties.getIntPropertyValue(COMPANY_CREATE_URL_TIMEOUT_PROPERTY,15000);
        String jsonPostMessage = APIUtil.serializeObjectToJson(affiliateHolder);
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("accept", "text/json");
        if (StringUtils.isNotEmpty(source)) {
            requestHeaders.put(X_CT_Source_Type, source);
        }

        Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

        try {

            logger.info("Create Company Request : " + jsonPostMessage);

            // Begin Logging
            ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(CommonEnumConstants.ApiType.OTHER, null, "CREATE_COMPANY", "", urlLocation, jsonPostMessage);
            }
            // End Logging

            RestResponse restResponse = RestUtil.post(urlLocation, jsonPostMessage, "", "text/json", requestHeaders, responseHeaders, "POST", timeout);

            // Begin Logging
            if (connectorStat != null) {
                connectorStat.closeApiRequest(CommonEnumConstants.ApiType.OTHER, "CREATE_COMPANY", restResponse.getCode(), restResponse.getMessage(), true);
            }
            // End Logging

            logger.info("Create Company Response : " + restResponse.getMessage());

            if (GenUtil.hasSuccessCode(restResponse)) {
                affiliateHolder = new AffiliateHolder();
                affiliateHolder = (AffiliateHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), affiliateHolder);
                company = affiliateHolder.getAffiliate();
            } else {
                FaultDetail faultDetail = restResponse.getFaultDetailFromJson();
                company = new Affiliate();
                company.setAffiliateId(0);
                company.setFaultDetail(faultDetail);
            }
        } catch (IOException e) {
            logger.error("IO exception returned in the API call to Create Affiliate : ", e);
        }
        return company;
    }



    /**
     * Retrieve company by Company ID along without ALL the People belonging to the company.
     * @param companyId int
     * @return Affiliate
     * @author Ulhas
     */
    public Affiliate getCompanyByCompanyIdWithoutPeople(int companyId) {
        return getCompanyByCompanyId(companyId, false, false);
    }

    /**
     * Retrieve company by Company ID along without ALL the People belonging to the company.
     * @param companyId int
     * @param source String
     * @param caller String
     * @return Affiliate
     */
    public Affiliate getCompanyByCompanyIdWithoutPeople(int companyId, String source, String caller) {
        return getCompanyByCompanyId(companyId, false, false, source, caller);
    }


    /**
     * Retrieve company by company ID. People belonging to the company will be retrieved ONLY IF REQUIRED.
     * @param companyId int
     * @param isTravellersRequired boolean
     * @param isUsersRequired boolean
     * @return Affiliate
     */
    private Affiliate getCompanyByCompanyId(int companyId, boolean isUsersRequired, boolean isTravellersRequired) {
        Affiliate company = new Affiliate();
        String urlLocation = commonCachedProperties.getPropertyValue(COMPANY_RETRIEVE_URL_PROPERTY);
        urlLocation = urlLocation.replaceAll(AFFILIATE_ID_PLACEHOLDER_REGEX, String.valueOf(companyId));

        String queryString = "";
        if (isUsersRequired) {
            queryString = "&users=true";
        } else {
            queryString = "&users=false";
        }
        if (isTravellersRequired) {
            queryString += "&travellers=true";
        } else {
            queryString += "&travellers=false";
        }

        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("accept", "text/json");
        Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

        try {

            // Begin Logging
            ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(CommonEnumConstants.ApiType.OTHER, null, "RETRIEVE_COMPANY_BY_ID", "NA", urlLocation, "");
            }
            // End Logging

            RestResponse restResponse = RestUtil.get(urlLocation, queryString, requestHeaders, responseHeaders);

            // Begin Logging
            if (connectorStat != null) {
                connectorStat.closeApiRequest(CommonEnumConstants.ApiType.OTHER, "RETRIEVE_COMPANY_BY_ID", restResponse.getCode(), restResponse.getMessage(), true);
            }
            // End Logging

            logger.info("Retrieve Company By Company ID - Response : " + restResponse.getMessage());

            if (GenUtil.hasSuccessCode(restResponse)) {
                AffiliateHolder affiliateHolder = new AffiliateHolder();
                affiliateHolder = (AffiliateHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), affiliateHolder);
                company = affiliateHolder.getAffiliate();
            } else {
                company = null;
            }
        } catch (Exception e) {
            logger.error("IO exception returned in the API call to Retrieve Affiliate by **AFFILIATE ID** : ", e);
        }
        return company;
    }
    public Affiliate getCompanyConfigOnlyByCompanyId(int companyId) {
        Affiliate company = new Affiliate();
        String urlLocation = commonCachedProperties.getPropertyValue(COMPANY_CONFIG_ONLY_RETRIEVE_URL_PROPERTY);
        int timeout = commonCachedProperties.getIntPropertyValue(COMPANY_CONFIG_ONLY_RETRIEVE_URL_TIMEOUT_PROPERTY,15000);
        String queryString = "caller=air&id=" + companyId;

        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("accept", "text/json");
        Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

        try {

            // Begin Logging
            ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(CommonEnumConstants.ApiType.OTHER, null, "RETRIEVE_COMPANY_BY_ID", "NA", urlLocation, "");
            }
            // End Logging

            RestResponse restResponse = RestUtil.get(urlLocation, queryString, requestHeaders, responseHeaders,timeout);

            // Begin Logging
            if (connectorStat != null) {
                connectorStat.closeApiRequest(CommonEnumConstants.ApiType.OTHER, "RETRIEVE_COMPANY_BY_ID", restResponse.getCode(), restResponse.getMessage(), true);
            }
            // End Logging

            logger.info("Retrieve Company By Company ID - Response : " + restResponse.getMessage());

            if (GenUtil.hasSuccessCode(restResponse)) {
                AffiliateHolder affiliateHolder = new AffiliateHolder();
                affiliateHolder = (AffiliateHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), affiliateHolder);
                company = affiliateHolder.getAffiliate();
            } else {
                company = null;
            }
        } catch (Exception e) {
            logger.error("IO exception returned in the API call to Retrieve Affiliate by **AFFILIATE ID** : ", e);
        }
        return company;
    }

    private Affiliate getCompanyByCompanyId(int companyId, boolean isUsersRequired, boolean isTravellersRequired, String source, String caller) {
        Affiliate company = new Affiliate();
        String urlLocation = "";
        int timeout = commonCachedProperties.getIntPropertyValue(COMPANY_RETRIEVE_URL_TIMEOUT_PROPERTY,15000);
        if (StringUtils.isNotBlank(source) && isB2BCall(source)) {
            urlLocation = commonCachedProperties.getPropertyValue("ct.services.api." + source.toLowerCase() + ".company.retrieve.url");
        } else {
            urlLocation = commonCachedProperties.getPropertyValue("ct.services.api.company.retrieve.url");
        }
        urlLocation = urlLocation.replaceAll(AFFILIATE_ID_PLACEHOLDER_REGEX, String.valueOf(companyId));

        String queryString = "";
        if (CommonEnumConstants.SourceType.CORP.toString().equalsIgnoreCase(source)) {
            queryString += "id=" + companyId;
        }
        if (isUsersRequired) {
            if (CommonEnumConstants.SourceType.CORP.toString().equalsIgnoreCase(source)) {
                urlLocation = commonCachedProperties.getPropertyValue("ct.services.corp.company.users.retrieve.url");
            } else {
                queryString += "&users=true";
            }
        } else {
            queryString += "&users=false";
        }
        if (isTravellersRequired) {
            if (!isUsersRequired && CommonEnumConstants.SourceType.CORP.toString().equalsIgnoreCase(source)) {
                urlLocation = commonCachedProperties.getPropertyValue("ct.services.corp.company.users.retrieve.url");
            } else {
                queryString += "&travellers=true";
            }
        } else {
            queryString += "&travellers=false";
        }

        if (StringUtils.isNotBlank(caller)) {
            queryString += "&caller=" + caller;
        }

        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("accept", "text/json");
        Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();
        RestResponse restResponse = null;
        ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();

        try {

            // Begin Logging
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(CommonEnumConstants.ApiType.OTHER, null, "RETRIEVE_COMPANY_BY_ID", "NA", urlLocation, "");
            }
            // End Logging

            restResponse = RestUtil.get(urlLocation, queryString, requestHeaders, responseHeaders,timeout);

        } catch (Exception e) {
            logger.error("IO exception returned in the API call to Retrieve Affiliate by **AFFILIATE ID** : ", e);
        } finally {
            // Begin Logging
            if (connectorStat != null) {
                connectorStat.closeApiRequest(CommonEnumConstants.ApiType.OTHER, "RETRIEVE_COMPANY_BY_ID", restResponse.getCode(), restResponse.getMessage(), true);
            }
            // End Logging

            logger.info("Retrieve Company By Company ID - Response : " + restResponse.getMessage());
        }
        
        try {
            if (GenUtil.hasSuccessCode(restResponse)) {
                AffiliateHolder affiliateHolder = new AffiliateHolder();
                affiliateHolder = (AffiliateHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), affiliateHolder);
                company = affiliateHolder.getAffiliate();
                if (CommonEnumConstants.SourceType.CORP.toString().equalsIgnoreCase(source) && company != null && company.getAffiliateUserProfiles() != null) {
                    for (UserProfile userProfile : company.getAffiliateUserProfiles()) {
                        if (userProfile.getCompanyDetails() != null && userProfile.getCompanyDetails().keySet().size() == 1 && userProfile.getCompanyDetails().keySet().contains(null)) {
                            CompanyDetail companyDetail = userProfile.getCompanyDetails().get(null);
                            userProfile.getCompanyDetails().remove(null);
                            companyDetail.setCompanyId(company.getAffiliateId());
                            userProfile.getCompanyDetails().put(company.getAffiliateId(), companyDetail);
                        }
                    }
                }
            } else {
                company = null;
            }
        } catch (Exception e) {
            logger.error("Error while deserialising company json to affiliate object for company id : " + companyId, e);
            company = null;
        }

        return company;
    }

    /**
     * Searches for an Affiliate based on the specified condition. Permitted condition :- -> by company-name -> by company-subdomain -> by a tag id that belongs to the company.
     * This method has been specifically made PRIVATE so that the condition scenario is not available outside. For creating new condition based searching, you can create a public method in this class
     * and internally call this method with that condition.
     * @param condition String
     * @return affiliate
     * @author Ulhas
     */
    private Affiliate getCompanyByCondition(String condition) {
        Affiliate company = null;
        List<String> dontLog = new ArrayList<String>();
        dontLog.add("404"); // no need to log the error when the company is not found in the central schema

        int timeout = commonCachedProperties.getIntPropertyValue(COMPANY_SEARCH_URL_TIMEOUT_PROPERTY,15000);

        String urlLocation = commonCachedProperties.getPropertyValue(COMPANY_SEARCH_URL_PROPERTY);

        String queryString = condition;
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("accept", "text/json");
        Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

        try {

            // Begin Logging
            ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(CommonEnumConstants.ApiType.OTHER, null, "SEARCH_COMPANY", "", urlLocation);
            }
            // End Logging

            RestResponse restResponse = RestUtil.get(urlLocation, queryString, requestHeaders, responseHeaders, dontLog, RestUtil.Char_Encoding_Type_Enum.UTF,timeout);

            // Begin Logging
            if (connectorStat != null) {
                connectorStat.closeApiRequest(CommonEnumConstants.ApiType.OTHER, "SEARCH_COMPANY", restResponse.getCode(), restResponse.getMessage(), true);
            }
            // End Logging

            logger.info("Search Company By Condition (" + condition + ") Response : " + restResponse.getMessage());

            if (GenUtil.hasSuccessCode(restResponse)) {
                AffiliateHolder affiliateHolder = new AffiliateHolder();
                affiliateHolder = (AffiliateHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), affiliateHolder);
                company = affiliateHolder.getAffiliate();
            }
        } catch (Exception e) {
            logger.error("IO exception returned in the API call to Search Affiliate by **CONDITION:" + condition + "** : ", e);
        }
        return company;
    }

    /**
     * Searches for an Affiliate based on the specified condition.
     * @param condition String
     * @param source String
     * @param caller String
     * @param callVersionedAPI boolean==
     * @return Affiliate
     */
    private Affiliate getCompanyByCondition(String condition, String source, String caller, boolean callVersionedAPI) {
        Affiliate company = null;
        List<String> dontLog = new ArrayList<String>();
        dontLog.add("404"); // no need to log the error when the company is not found in the central schema

        String urlLocation = "";

        int timeout = commonCachedProperties.getIntPropertyValue(COMPANY_SEARCH_URL_TIMEOUT_PROPERTY,15000);

        if (callVersionedAPI) {
            urlLocation = commonCachedProperties.getPropertyValue("ct.services.versioned.api." + source.toLowerCase() + ".company.search.url");
        } else if (StringUtils.isNotBlank(source) && isB2BCall(source)) {
            urlLocation = commonCachedProperties.getPropertyValue("ct.services.api." + source.toLowerCase() + ".company.search.url");
        } else {
            urlLocation = commonCachedProperties.getPropertyValue("ct.services.api.company.search.url");
        }

        String queryString = condition + "&caller=" + caller;
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("accept", "text/json");
        Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

        try {

            // Begin Logging
            ConnectorStats connectorStat = (ConnectorStats) BaseStats.threadLocal();
            if (connectorStat != null) {
                connectorStat.addNewApiRequest(CommonEnumConstants.ApiType.OTHER, null, "SEARCH_COMPANY", "", urlLocation);
            }
            // End Logging

            RestResponse restResponse = RestUtil.get(urlLocation, queryString, requestHeaders, responseHeaders, dontLog, RestUtil.Char_Encoding_Type_Enum.UTF,timeout);

            // Begin Logging
            if (connectorStat != null) {
                connectorStat.closeApiRequest(CommonEnumConstants.ApiType.OTHER, "SEARCH_COMPANY", restResponse.getCode(), restResponse.getMessage(), true);
            }
            // End Logging

            logger.info("Search Company By Condition (" + condition + ") Response : " + restResponse.getMessage());

            if (GenUtil.hasSuccessCode(restResponse)) {
                AffiliateHolder affiliateHolder = new AffiliateHolder();
                affiliateHolder = (AffiliateHolder) APIUtil.deserializeJsonToObject(restResponse.getMessage(), affiliateHolder);
                company = affiliateHolder.getAffiliate();
            }
        } catch (Exception e) {
            logger.error("IO exception returned in the API call to Search Affiliate by **CONDITION:" + condition + "** : ", e);
        }
        return company;
    }



    /**
     * Retrieve company by subdomain without the people belonging to that company.
     * @param subDomain String
     * @return Affiliate
     * @author Ulhas
     */
    public Affiliate getCompanyBySubdomainWithoutPeople(String subDomain) {
        String condition = "domain=" + subDomain + "&users=false&travellers=false";
        return getCompanyByCondition(condition);
    }

    /**
     * Retrieve company by subdomain without the people belonging to that company.
     * @param subDomain String
     * @param source String
     * @param caller String
     * @param callVersionedAPI boolean
     * @return Affiliate
     */
    public Affiliate getCompanyBySubdomainWithoutPeople(String subDomain, String source, String caller, boolean callVersionedAPI) {
        String condition = "domain=" + subDomain + "&users=false&travellers=false";
        return getCompanyByCondition(condition, source, caller, callVersionedAPI);
    }

    /**
     * @return the cachedProperties
     */
    public CachedProperties getCommonCachedProperties() {
        return commonCachedProperties;
    }

    /**
     * @param cachedProperties
     *            the cachedProperties to set
     */
    public void setCommonCachedProperties(CachedProperties cachedProperties) {
        this.commonCachedProperties = cachedProperties;
    }





    private boolean isB2BCall(String source) {
        return CommonEnumConstants.SourceType.CORP.toString().equalsIgnoreCase(source) || CommonEnumConstants.SourceType.AGENCY.toString().equalsIgnoreCase(source) || CommonEnumConstants.SourceType.WL.toString().equalsIgnoreCase(source);
    }

}
