package com.companyApiTinyCache.companyConfigCache.service.common.util.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;

public class XPathHelper {
    private static final Log logger = LogFactory.getLog(XPathHelper.class);

    public static Document createDocument(String xml) throws ParserConfigurationException, SAXException, IOException {
        StringReader reader = new StringReader(xml);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(new InputSource(reader));
    }

    public static String selectNodeValue(XPath xPath, String expression, Element document) {
        String res = "";
        Node node = null;
        try {
            node = (Node) xPath.evaluate(expression, document, XPathConstants.NODE);
        } catch (XPathExpressionException e) {

        }
        if (node == null)
            res = "";
        else
            res = node.getTextContent();
        return res;
    }

    public static String selectNodeValue(String expression, Element document) {
        XPath xPath = javax.xml.xpath.XPathFactory.newInstance().newXPath();
        return selectNodeValue(xPath, expression, document);
    }

    public static int selectNodeIntValue(XPath xPath, String expression, Element document) {
        String res = selectNodeValue(xPath, expression, document);
        if (res.equals(""))
            res = "0";

        return Integer.parseInt(res);
    }

    public static int selectNodeIntValue(String expression, Element document) {
        XPath xPath = javax.xml.xpath.XPathFactory.newInstance().newXPath();
        return selectNodeIntValue(xPath, expression, document);
    }

    public static double selectNodeDoubleValue(XPath xPath, String expression, Element document) {
        String res = selectNodeValue(xPath, expression, document);
        if (res.equals(""))
            res = "0";

        return Double.parseDouble(res);
    }

    public static double selectNodeDoubleValue(String expression, Element document) {
        XPath xPath = javax.xml.xpath.XPathFactory.newInstance().newXPath();
        return selectNodeDoubleValue(xPath, expression, document);
    }

    public static int selectNodeCount(String expression, Element document) {
        NodeList nodeSet = null;
        XPath xPath = javax.xml.xpath.XPathFactory.newInstance().newXPath();
        try {
            nodeSet = (NodeList) xPath.evaluate(expression, document, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {

        }
        return (nodeSet == null) ? 0 : nodeSet.getLength();
    }

    public static NodeList selectNodes(XPath xPath, String expression, Element document) {
        NodeList nodeSet = null;
        try {
            nodeSet = (NodeList) xPath.evaluate(expression, document, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        return nodeSet;
    }

    public static NodeList selectNodes(String expression, Element document) {
        XPath xPath = javax.xml.xpath.XPathFactory.newInstance().newXPath();
        return selectNodes(xPath, expression, document);
    }

    public static NodeList selectNodes(XPathExpression xPathExpression, Element document) {
        NodeList nodeSet = null;
        try {
            nodeSet = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        return nodeSet;
    }

    public static String selectNodeValue(String expression, Element document, int occurance) {
        NodeList nodeSet = null;
        XPath xPath = javax.xml.xpath.XPathFactory.newInstance().newXPath();
        try {
            nodeSet = (NodeList) xPath.evaluate(expression, document, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {

        }
        if (nodeSet != null) {
            Node node = nodeSet.item(occurance - 1);
            return (node == null) ? "" : node.getTextContent();
        }
        return "";
    }
}
