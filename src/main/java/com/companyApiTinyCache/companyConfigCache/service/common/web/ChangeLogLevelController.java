package com.companyApiTinyCache.companyConfigCache.service.common.web;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class ChangeLogLevelController extends AbstractController {

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean logLevelChanged = false;
        Logger logger = LogManager.getRootLogger();

        String levelName = request.getParameter("LEVEL");
        Level level = Level.toLevel(request.getParameter("LEVEL"));

        if (StringUtils.isNotBlank(levelName)) {
            String className = request.getParameter("CLASS");
            String pacakgeName = request.getParameter("PACKAGE");

            // if class and package both are blank, then set the log level at root level
            if (StringUtils.isBlank(className) && StringUtils.isBlank(pacakgeName)) {
                logger.setLevel(level);
                logLevelChanged = true;
            } else {
                if (StringUtils.isNotBlank(className)) {
                    logger = LogManager.getLogger(className);
                    if (logger != null) {
                        logger.setLevel(level);
                        logLevelChanged = true;
                    }
                }

                if (StringUtils.isNotBlank(pacakgeName)) {
                    logger = LogManager.getLogger(pacakgeName);
                    if (logger != null) {
                        logger.setLevel(level);
                        logLevelChanged = true;
                    }
                }
            }
        }

        PrintWriter pw = response.getWriter();
        response.setStatus(200);
        if (logLevelChanged) {
            pw.println("Log level changed to " + levelName);
        } else {
            pw.println("Error: Log level not changed because " + levelName + " is incorrect. Pass LEVEL=[DEBUG,INFO,WARN,ERROR,FATAL]");
        }
        return null;
    }
}
