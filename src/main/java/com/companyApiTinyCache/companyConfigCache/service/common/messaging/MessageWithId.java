package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

public interface MessageWithId {
    String getMessageId();
}
