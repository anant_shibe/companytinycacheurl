package com.companyApiTinyCache.companyConfigCache.service.common.cached.resources;

import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class CachedUIElements extends CachedResource<Map<String, Map<String, String>>> {

    private static final String UI_ELEMENTS_URL = "ct.ui.elements.url";

    private CachedProperties cachedProperties;

    public CachedUIElements(CachedProperties cachedProperties) {
        this.cachedProperties = cachedProperties;
        refreshResource();
    }

    @Override
    protected Map<String, Map<String, String>> loadResource(long cacheMillis, boolean force) throws Exception {
        String url = cachedProperties.getPropertyValue(UI_ELEMENTS_URL);
        RestResponse response = RestUtil.get(url, "");
        String json = response.getMessage();

        ObjectMapper mapper = JsonUtil.getObjectMapper();
        Map<String, Map<String, String>> ctUIElements = mapper.readValue(json, Map.class);

        return ctUIElements;
    }

}
