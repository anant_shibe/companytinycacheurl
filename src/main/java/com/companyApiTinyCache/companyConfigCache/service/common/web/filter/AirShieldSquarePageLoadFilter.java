package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;

import javax.servlet.ServletRequest;

/**
 * ShieldSquarePageLoadFilter.
 * @author amith
 */
 @ClassExcludeCoverage
public class AirShieldSquarePageLoadFilter extends ShieldSquareAbstractFilter {

    @Override
    public SSCALLTYPE getCallType() {
        return SSCALLTYPE.PAGE_LOAD;
    }

    /*check if callcount "CC" is 1 - meaning first call from UI.
    subsequent calls from UI should not call shieldSquare
    and if other endpoints are added by default sending TRUE.
  */
    @Override
    public boolean callShieldSquare(ServletRequest request, CachedProperties commonCachedProperties) {


        return false;
    }

}
