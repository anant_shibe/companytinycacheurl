/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.data.FaultDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.data.holder.FaultDetailsHolder;
import com.companyApiTinyCache.companyConfigCache.service.common.util.xml.XPathHelper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

/**
 * @author Ramesh Krishnamoorthy
 */
public class RestResponse {
    private int code = -1;
    private String message;

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = Integer.parseInt(code);
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public FaultDetail getFaultDetails() throws XPathExpressionException {
        FaultDetail faultDetail = new FaultDetail();
        try {
            Document document = XPathHelper.createDocument(message);
            Element rootElement = document.getDocumentElement();
            faultDetail.setFaultCode(XPathHelper.selectNodeValue("//fault/fault-code", rootElement));
            faultDetail.setFaultMessage(XPathHelper.selectNodeValue("//fault/fault-message", rootElement));

        } catch (ParserConfigurationException pe) {
            faultDetail = null;
        } catch (SAXException se) {
            faultDetail = null;
        } catch (IOException e) {
            faultDetail = null;
        }
        return faultDetail;
    }

    public FaultDetail getFaultDetailFromJson() {
        FaultDetailsHolder faultDetailsHolder = new FaultDetailsHolder();
        FaultDetail faultDetail = new FaultDetail();
        ObjectMapper mapper = JsonUtil.getObjectMapper();

        try {
            faultDetailsHolder = mapper.readValue(message, faultDetailsHolder.getClass());
            if (null != faultDetailsHolder.getFaultDetail()) {
                faultDetail = faultDetailsHolder.getFaultDetail();
            }
        } catch (JsonParseException e) {
            faultDetail = null;
        } catch (JsonMappingException e) {
            faultDetail = null;
        } catch (IOException e) {
            faultDetail = null;
        }

        return faultDetail;

    }
}
