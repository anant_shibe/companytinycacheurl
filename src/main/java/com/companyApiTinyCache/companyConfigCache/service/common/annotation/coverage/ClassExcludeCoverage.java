package com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
public @interface ClassExcludeCoverage {
    String info() default "";
}
