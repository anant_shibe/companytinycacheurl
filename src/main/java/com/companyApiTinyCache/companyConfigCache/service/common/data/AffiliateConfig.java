package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.jackson.CompanyConfigEnumDeserializer;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CompanyConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

public class AffiliateConfig implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 6977138451281454844L;

    private int id;

    private int affiliateId;

    /**
     * The value for this property comes from a Enum - CommonEnumConstants.CompanyConfig
     */
    private CompanyConfig configName;

    private String configValue;

    private Date createdAt;

    private String createdBy;

    private Date updatedAt;

    private String updatedBy;

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("company_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getAffiliateId() {
        return affiliateId;
    }

    @JsonProperty("company_id")
    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    @JsonProperty("config_name")
    public CompanyConfig getConfigName() {
        return configName;
    }

    @JsonDeserialize(using = CompanyConfigEnumDeserializer.class)
    @JsonProperty("config_name")
    public void setConfigName(CompanyConfig configName) {
        this.configName = configName;
    }

    @JsonProperty("config_value")
    public String getConfigValue() {
        return configValue;
    }

    @JsonProperty("config_value")
    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    @JsonProperty("created_at")
    public Date getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonProperty("updated_at")
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updated_by")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AffiliateConfig == false)
            return false;
        AffiliateConfig affConf = (AffiliateConfig) obj;
        if ((configName != null || affConf.getConfigName() != null) && !configName.equals(affConf.getConfigName())) {
            return false;
        }
        if ((configValue != null || affConf.getConfigValue() != null) && !configValue.equals(affConf.getConfigValue())) {
            return false;
        }
        if ((createdBy != null || affConf.getCreatedBy() != null) && !createdBy.equals(affConf.getCreatedBy())) {
            return false;
        }
        if ((updatedBy != null || affConf.getUpdatedBy() != null) && !updatedBy.equals(affConf.getUpdatedBy())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
