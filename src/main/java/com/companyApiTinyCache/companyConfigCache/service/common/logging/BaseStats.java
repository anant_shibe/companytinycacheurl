package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.io.ReusableByteArrayOutputStream;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.TimeKeeper;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.LogLevel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * Base Stats.
 *
 */
public class BaseStats implements Serializable {

    private static final long serialVersionUID = -7305022037082424699L;

    private static final Log logger = LogFactory.getLog(BaseStats.class);
    private static String faultData = null;

    public static final int AIR = 0;

    public static final int HOTEL = 1;

    public static final int CAB = 2;

    public static final int LOCAL = 3;

    public static final ThreadLocal<BaseStats> THREADLOCAL = new ThreadLocal<BaseStats>();

    public static final String DEBUG_PARAM = "debug";

    public static final String LRR_PARAM = "lrr";

    public static final String LT_PARAM = "lt";

    protected LogLevel logLevel;

    protected boolean ignoreInfoOnlyLogs;

    protected List<LogMessage> messages;

    protected boolean logRequestResponse;

    protected TimeKeeper timeKeeper;

    protected transient StringBuilder sb;

    protected transient ReusableByteArrayOutputStream bop;

    protected String routeHappyCookie;

    protected boolean isRouteHappySearch;

    protected String flightIndex;

    protected CtStep ctStep = CtStep.OTHER;
    /**
     * A new member variable to log warning messages to summary instead of logging it to tomcat logs An example of a warning message is "UDF is zero for search criteria blah blah".
     */
    protected Map<String, String> informationLogs;

    protected int statsType;

    protected transient BaseStats[] inactiveStats;

    public void addInformationMessages(Map<String, String> msgMap) {
        if (informationLogs == null) {
            informationLogs = new HashMap<String, String>();
        }
        if (msgMap != null) {
            informationLogs.putAll(msgMap);
        }
    }

    public CtStep getCtStep() {
        return ctStep;
    }

    public void setCtStep(CtStep ctStep) {
        this.ctStep = ctStep;
    }

    public BaseStats() {
        logLevel = LogLevel.ERROR;
        ignoreInfoOnlyLogs = true;
    }

    public static BaseStats threadLocal() {
        return THREADLOCAL.get();
    }

    public static Optional<BaseStats> threadLocalOptional() {
        return Optional.ofNullable(THREADLOCAL.get());
    }

    // Index returned: -1: pstatsType not found 0 - this, 1 - inactiveStats[0], 2 - inactiveStats[1]...
    private int getStatsIndex(int pstatsType) {
        int i, statsIndex = -1;
        if (statsType == pstatsType) {
            statsIndex = 0;
        } else if (inactiveStats != null) {
            for (i = 0; i < inactiveStats.length; i++) {
                if (inactiveStats[i].statsType == pstatsType) {
                    statsIndex = i + 1;
                    break;
                }
            }
        }

        return statsIndex;
    }

    public BaseStats[] getInactiveStats() {
        BaseStats[] inactiveStatsCopy = null;
        if (inactiveStats != null) {
            inactiveStatsCopy = inactiveStats.clone();
        }

        return inactiveStatsCopy;
    }

    public BaseStats getStats(int pstatsType) {
        BaseStats stats = null;
        int statsIndex = getStatsIndex(pstatsType);
        if (statsIndex >= 0) {
            stats = statsIndex == 0 ? this : inactiveStats[statsIndex - 1];
        }
        return stats;
    }

    // Not Thread Safe
    public void addInactiveStats(BaseStats stats) {
        if (stats != null) {
            int statsIndex = getStatsIndex(stats.statsType);
            if (statsIndex >= 0) {
                throw new IllegalArgumentException("Stats Type: " + stats.statsType + " already exists at index: " + statsIndex);
            }
            BaseStats[] newInactiveStats;
            if (inactiveStats == null) {
                newInactiveStats = new BaseStats[1];
            } else {
                newInactiveStats = Arrays.copyOf(inactiveStats, inactiveStats.length + 1);
            }
            newInactiveStats[newInactiveStats.length - 1] = stats;
            inactiveStats = newInactiveStats;
        }
    }

    /**
     * Switches Active Stats to given type if present.
     *
     * @param statsType
     *            Stats Type to switch to
     * @return - Previously active Stats
     */
    private static BaseStats switchActiveStats(int statsType, int index) {
        BaseStats stats = THREADLOCAL.get();
        if (stats != null) {
            if (index < 0) {
                index = stats.getStatsIndex(statsType);
            }
            if (index > 0) {
                BaseStats newStats = stats.inactiveStats[index - 1];
                stats.inactiveStats[index - 1] = stats;
                newStats.inactiveStats = stats.inactiveStats;
                THREADLOCAL.set(newStats);
                stats.inactiveStats = null;
            } else if (index < 0) {
                THREADLOCAL.remove();
            }
        }

        return stats;
    }

    /**
     * Switches Active Stats to given type if present.
     *
     * @param statsType
     *            Stats Type to switch to
     * @return - Previously active Stats
     */
    public static BaseStats switchActiveStats(int statsType) {
        return switchActiveStats(statsType, -1);
    }

    public static boolean restoreActiveStats(BaseStats stats) {
        boolean restored = false;
        if (stats != null) {
            BaseStats currentStats = THREADLOCAL.get();
            if (currentStats != null) {
                int index = currentStats.getStatsIndex(stats.statsType);
                if (index >= 0) {
                    restored = true;
                    switchActiveStats(stats.statsType, index);
                }
            } else {
                restored = true;
                THREADLOCAL.set(stats);
            }
        }
        return restored;
    }

    public void log(LogLevel level, String name, String message, Throwable e) {
        if (level == null) {
            level = LogLevel.INFO;
        }
        if (ignoreInfoOnlyLogs && level.getRank() > LogLevel.INFO.getRank() && message != null && message.indexOf("INFO ONLY") >= 0) {
            level = LogLevel.INFO;
        }
        if (level.getRank() >= logLevel.getRank()) {
            if (messages == null) {
                messages = new ArrayList<LogMessage>(8);
            }
            LogMessage logMessage = new LogMessage(name, message, level, e);
            messages.add(logMessage);
        }
    }

    public void mergeLogs(BaseStats stats, String subTaskName) {
        if (stats != null) {
            int i, messageCount = 0;
            List<LogMessage> statMessages = stats.getMessages();
            stats.setMessages(null);
            if (statMessages != null) {
                messageCount = statMessages.size();
            }
            if (messageCount > 0) {
                if (subTaskName != null) {
                    log(logLevel, "BaseStats", "BEGIN LOGS FROM SUBTASK " + subTaskName, null);
                }
                if (messages == null) {
                    messages = new ArrayList<LogMessage>();
                }
                i = 0;
                do {
                    messages.add(statMessages.get(i));
                    i++;
                } while (i < messageCount);
                if (subTaskName != null) {
                    log(logLevel, "BaseStats", "END LOGS FROM SUBTASK " + subTaskName, null);
                }
            }
        }
    }

    public void doInit(HttpServletRequest request, int pstatsType, CachedProperties commonCachedProperties, boolean lrr, boolean lt) {
        statsType = pstatsType;
        if (request != null) {
            String llParam = null;
            LogLevel ll = null;
            if (GenUtil.getBooleanParameter(request, DEBUG_PARAM, false)) {
                ll = LogLevel.DEBUG;
            } else if ((llParam = StringUtils.trimToNull(request.getParameter("ll"))) != null) {
                try {
                    ll = LogLevel.valueOf(llParam);
                } catch (Exception e) {
                    logger.error("Error in initialising base stats. ", e);
                }
            }
            if (ll != null) {
                setLogLevel(ll);
            }
            if (ll == LogLevel.DEBUG) {
                lt = true;
            }
            lt = GenUtil.getBooleanParameter(request, LT_PARAM, lt);
            if (lt) {
                setTimeKeeper(new TimeKeeper());
            }
            lrr = GenUtil.getBooleanParameter(request, LRR_PARAM, lrr);
            setLogRequestResponse(lrr);
        }

    }

    @JsonIgnore
    public boolean isDebug() {
        return logLevel != null && LogLevel.DEBUG.getRank() >= logLevel.getRank();
    }

    public void error(String name, String message, Exception e) {
        log(LogLevel.ERROR, name, message, e);
    }

    public void error(String name, String message) {
        log(LogLevel.ERROR, name, message, null);
    }

    /**
     * Getter for logLevel.
     *
     * @return the logLevel
     */
    public LogLevel getLogLevel() {
        return logLevel;
    }

    /**
     * Setter for logLevel.
     *
     * @param plogLevel
     *            the logLevel to set
     */
    public void setLogLevel(LogLevel plogLevel) {
        logLevel = plogLevel;
        if (logLevel == null) {
            logLevel = LogLevel.INFO;
        }
    }

    /**
     * Getter for messages.
     *
     * @return the messages
     */
    @JsonIgnore
    public List<LogMessage> getMessages() {
        return messages;
    }

    /**
     * Setter for messages.
     *
     * @param pmessages
     *            the messages to set
     */
    public void setMessages(List<LogMessage> pmessages) {
        messages = pmessages;
    }

    public static TimeKeeper sgetTimeKeeper() {
        TimeKeeper timeKeeper = null;
        BaseStats baseStats = THREADLOCAL.get();
        if (baseStats != null) {
            timeKeeper = baseStats.getTimeKeeper();
        }

        return timeKeeper;
    }

    /**
     * Logs Task Start after creating one. Will be attached as the child of the most recent unfinished task if found. Otherwise it is put as a root Task.
     *
     * @param taskName
     *            Task name
     * @param time
     *            milliseconds time or 0 to use System.currentTimeMillis()
     * @return milliseconds time
     */
    public static long taskStarted(String taskName, long time) {
        long currentMillis = 0;
        TimeKeeper timeKeeper = sgetTimeKeeper();
        if (timeKeeper != null) {
            currentMillis = timeKeeper.taskStarted(taskName, time);
        }

        return currentMillis;
    }

    /**
     * Easiest and safest to use. Equivalent to taskStarted(taskName, 0)
     *
     * @param taskName
     *            taskName
     * @return milliseconds time
     */
    public static long taskStarted(String taskName) {
        return taskStarted(taskName, 0);
    }

    /**
     * Logs Task End for the most recent open task with the given taskName. Logs error if not found. Automatically calls taskFinished on all the child tasks.
     *
     * @param taskName
     *            Task name
     * @param time
     *            milliseconds time or 0 to use System.currentTimeMillis()
     * @return milliseconds time
     */
    public static long taskFinished(String taskName, long time) {
        long currentMillis = 0;
        TimeKeeper timeKeeper = sgetTimeKeeper();
        if (timeKeeper != null) {
            currentMillis = timeKeeper.taskFinished(taskName, time);
        }

        return currentMillis;
    }

    /**
     * Easiest and safest to use. Equivalent to taskFinished(taskName, 0)
     *
     * @param taskName
     *            taskName
     * @return milliseconds time
     */
    public static long taskFinished(String taskName) {
        return taskFinished(taskName, 0);
    }

    public StringBuilder tmpSb() {
        StringBuilder stringBuilder = sb;
        if (stringBuilder == null) {
            stringBuilder = new StringBuilder();
        }
        stringBuilder.setLength(0);
        sb = null;

        return stringBuilder;
    }

    public StringBuilder returnTmpSb(StringBuilder stringBuilder) {
        if (stringBuilder != null) {
            sb = stringBuilder;
        }
        return null;
    }

    public static StringBuilder tmpStringBuilder() {
        StringBuilder sb = null;
        BaseStats baseStats = BaseStats.THREADLOCAL.get();
        if (baseStats != null) {
            sb = baseStats.tmpSb();
        }
        if (sb == null) {
            sb = new StringBuilder();
        }

        return sb;
    }

    public static StringBuilder returnTmpStringBuilder(StringBuilder stringBuilder) {
        BaseStats baseStats = BaseStats.THREADLOCAL.get();
        StringBuilder sb = null;
        if (baseStats != null) {
            sb = baseStats.returnTmpSb(stringBuilder);
        }

        return sb;
    }

    public static StringBuilderWriter tmpWriter() {
        StringBuilderWriter sbw = null;
        BaseStats baseStats = BaseStats.THREADLOCAL.get();
        if (baseStats != null) {
            sbw = new StringBuilderWriter(baseStats.tmpSb());
        }
        if (sbw == null) {
            sbw = new StringBuilderWriter();
        }

        return sbw;
    }

    public static StringBuilderWriter returnTmpWriter(StringBuilderWriter sbw) {
        if (sbw != null) {
            returnTmpStringBuilder(sbw.getBuf());
        }

        return null;
    }

    public ReusableByteArrayOutputStream tmpBop() {
        ReusableByteArrayOutputStream op = bop;
        if (op == null) {
            op = new ReusableByteArrayOutputStream(128);
        }
        op.reset();
        bop = null;
        return op;
    }

    public ReusableByteArrayOutputStream returnTmpBop(ReusableByteArrayOutputStream op) {
        if (op != null) {
            bop = op;
        }
        return null;
    }

    public static ReusableByteArrayOutputStream tmpReusableByteArrayOutputStream() {
        BaseStats baseStats = BaseStats.THREADLOCAL.get();
        ReusableByteArrayOutputStream op = null;
        if (baseStats != null) {
            op = baseStats.tmpBop();
        }
        if (op == null) {
            op = new ReusableByteArrayOutputStream(128);
        }

        return op;
    }

    public static ReusableByteArrayOutputStream returnTmpReusableByteArrayOutputStream(ReusableByteArrayOutputStream op) {
        BaseStats baseStats = BaseStats.THREADLOCAL.get();
        ReusableByteArrayOutputStream tmpOp = null;
        if (baseStats != null) {
            tmpOp = baseStats.returnTmpBop(op);
        }

        return tmpOp;
    }

    public String getLogOutput() {
        String logOutput = null;
        int i, messageCount = 0;
        if (messages != null) {
            messageCount = messages.size();
        }
        if (messageCount > 0) {
            DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
            StringBuilderWriter writer = tmpWriter();
            PrintWriter pw = new PrintWriter(writer);
            i = 0;
            do {
                LogMessage message = messages.get(i);
                pw.print(dateFormat.format(new Date(message.getTime())));
                pw.print(' ');
                if(null != this.getCtStep()) {
                    pw.print(this.getCtStep().name());
                    pw.print(' ');
                }
                pw.print(message.getLogLevel());
                pw.print(" [");
                pw.print(message.getName());
                pw.print("] <");
                pw.print(message.getMessage());
                pw.println('>');
                Throwable e = message.getException();
                if (e != null) {
                    logger.error("Exception is:" + e.getMessage(), e);
                }
                i++;
            } while (i < messageCount);
            logOutput = writer.toString();
            writer = BaseStats.returnTmpWriter(writer);
        }
        return logOutput;
    }

    /**
     * Getter for logRequestResponse.
     *
     * @return the logRequestResponse
     */
    public boolean isLogRequestResponse() {
        return logRequestResponse;
    }

    /**
     * Setter for logRequestResponse.
     *
     * @param plogRequestResponse
     *            the logRequestResponse to set
     */
    public void setLogRequestResponse(boolean plogRequestResponse) {
        logRequestResponse = plogRequestResponse;
    }

    /**
     * Getter for timeKeeper.
     *
     * @return the timeKeeper
     */
    @JsonIgnore
    public TimeKeeper getTimeKeeper() {
        return timeKeeper;
    }

    /**
     * Setter for timeKeeper.
     *
     * @param ptimeKeeper
     *            the timeKeeper to set
     */
    public void setTimeKeeper(TimeKeeper ptimeKeeper) {
        timeKeeper = ptimeKeeper;
    }

    /**
     * @return Map A map of message types and messages
     */
    public Map<String, String> getInformationLogs() {
        return informationLogs;
    }

    /**
     * @param informationLogs
     *            Map of informationLogs
     */
    public void setInformationLogs(Map<String, String> informationLogs) {
        this.informationLogs = informationLogs;
    }

    public String getRouteHappyCookie() {
        return routeHappyCookie;
    }

    public void setRouteHappyCookie(String routeHappyCookie) {
        this.routeHappyCookie = routeHappyCookie;
    }

    public boolean isRouteHappySearch() {
        return isRouteHappySearch;
    }

    public void setRouteHappySearch(boolean isRouteHappySearch) {
        this.isRouteHappySearch = isRouteHappySearch;
    }

    public String getFlightIndex() {
        return flightIndex;
    }

    public void setFlightIndex(String flightIndex) {
        this.flightIndex = flightIndex;
    }


    public static void recordFaultData(String soapPartAsString) {
        faultData = soapPartAsString;
    }

    public String fetchAndClearFaultData() {
        String sFaultData = faultData;
        faultData = null;
        return sFaultData;
    }

    public void clearFaultData() {
        faultData = null;
    }
}
