package com.companyApiTinyCache.companyConfigCache.service.common.enums;

import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.BookingInfoType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.EnumMap;
import java.util.Map;

import static java.util.Collections.singletonMap;

/**
 * An integral number associated with a label, known as status-history, is logged to TM.<br/><br/>
 *
 * status-history of a label is a power of 2, so that multiple labels in a transaction
 * give a unique status-history when combined with bit-OR operation.<br/><br/>
 *
 * status-history is replacement for status-reason.<br/>
 * So, each booking-info would have some status-history after a transaction (default 0).
 */
@SuppressWarnings("unchecked")
public enum BookingStatusLabel {
    NONE,
    BOOK_FAIL(air(0), hotel(0)),
    PL_HOLD_FAIL(air(1)),
    AIRLINE_COUPON(air(2)),
    PRICELOCK(air(3)),
    AMENDMENT(air(4)),
    PTA_WAIT(air(5), hotel(1)),
    IMPORT_PNR(air(6)),
    HOLD_BOOKING(air(7),
            hotel(2)), // DECLINED(air(8)) omitted, since not updated from java
    PAY_BY_CASH(air(9), hotel(3)), FLEXI_PAY(air(10)), FLEXI_PAY_FAILED(air(11));

    private static Map<BookingInfoType, Number> air(int index) {
        return singletonMap(BookingInfoType.AIR, History.fromBitIndex(index));
    }

    private static Map<BookingInfoType, Number> hotel(int index) {
        return singletonMap(BookingInfoType.HOTEL, History.fromBitIndex(index));
    }

    // Construction

    private final Map<BookingInfoType, Number> historyMappings;

    private BookingStatusLabel() {
        historyMappings = new EnumMap<BookingInfoType, Number>(BookingInfoType.class);

        for (BookingInfoType bookingInfoType : BookingInfoType.values()) {
            historyMappings.put(bookingInfoType, History.ZERO);
        }
    }

    private BookingStatusLabel(Map<BookingInfoType, Number>... historyMappings) {
        this();

        for (Map<BookingInfoType, Number> historyMapping : historyMappings) {
            verifyHistoryMappingBeingAdded(historyMapping);
            this.historyMappings.putAll(historyMapping);
        }
    }

    private void verifyHistoryMappingBeingAdded(Map<BookingInfoType, Number> historyMapping) {
        if (historyMapping == null) {
            throw new IllegalArgumentException("null entry attempt for " + this.name());
        }

        for (BookingInfoType key : historyMapping.keySet()) {
            if (key == null) {
                throw new IllegalArgumentException("null-key entry attempt for " + this.name());
            }

            if (historyMappings.get(key) != History.ZERO) {
                throw new IllegalArgumentException(String.format("overwrite entry attempt for %s | %s", this.name(), key));
            }
        }
    }

    // --- Construction


    // Client Interface

    /**
     * @param bookingInfoType - AIR,...
     *
     * @return An integral number denoting status-history contribution of BookingStatusLabel for bookingInfoType.
     */
    public Number getHistoryContribution(BookingInfoType bookingInfoType) {
        return historyMappings.get(bookingInfoType);
    }

    /**
     * @param bookingInfoType - AIR,...
     * @param labels - BookingStatusLabels
     *
     * @return An integral number representing the combined value of status-history contributions of labels.
     */
    public static Number evalStatusHistory(BookingInfoType bookingInfoType, Iterable<BookingStatusLabel> labels) {
        Number statusHistory = History.ZERO;

        for (BookingStatusLabel label : labels) {
            statusHistory = History.combine(statusHistory, label.getHistoryContribution(bookingInfoType));
        }

        return statusHistory;
    }

    // --- Client Interface


    /**
     * Helper class to operate with status-history values.
     */
    public static final class History {

        private static final Log LOGGER = LogFactory.getLog(History.class);

        private interface Computer<T extends Number> {
            T pow2(int index) throws IndexOutOfBoundsException;
            T createNonNegative(String str) throws NumberFormatException;
            T bitOr(Number n1, Number n2);
            T andNot(Number n1, Number n2);
        }

        private static final Computer<Integer> intComputer = new Computer<Integer>() {

            private int maxPow2Index() {
                return Integer.SIZE - 2;
            }

            private void assertPow2Index(int index) {
                if (index < 0 || index > maxPow2Index()) {
                    throw new IndexOutOfBoundsException("invalid index " + index);
                }
            }

            private int value(Number n) {
                return n.intValue();
            }

            @Override
            public Integer pow2(int index) throws IndexOutOfBoundsException {
                assertPow2Index(index);
                return 1 << index;
            }

            @Override
            public Integer createNonNegative(String str) throws NumberFormatException {
                int i = Integer.parseInt(str);
                if (i < 0) {
                    throw new NumberFormatException(String.format("For input string: \"%s\"", str));
                }
                return i;
            }

            @Override
            public Integer bitOr(Number n1, Number n2) {
                return value(n1) | value(n2);
            }

            @Override
            public Integer andNot(Number n1, Number n2) {
                return value(n1) & ~value(n2);
            }
        };


        // Default Computer

        private static final Computer<?> comp = intComputer;

        static Computer<?> getComputer() {
            return comp;
        }

        static Computer<Integer> getIntComputer() {
            return intComputer;
        }


        // Client Interface

        public static final Number ZERO = fromString("0");

        /**
         * @param index - indicates bit index of a status label when its status-history is
         * represented as a binary string; the bit value at this index would be 1.
         * index starts from 0 upto a max permitted value, depending on the Number type used for status-history.
         *
         * @return An integral number equivalent to Math.pow(2, index), if index is within permitted bounds
         * (i.e. a non-negative history value confirming to the maintained Number type can be generated);
         * otherwise History.ZERO is returned.
         */
        public static Number fromBitIndex(int index) {
            try {
                return comp.pow2(index);
            } catch (IndexOutOfBoundsException e) {
                LOGGER.error(e);
                return ZERO;
            }
        }

        /**
         * @param str - number-convertible string
         *
         * @return An integral number confirming to the maintained Number type for status-history.
         * If conversion to this type fails or results in negative number, History.ZERO is returned.
         */
        public static Number fromString(String str) {
            try {
                return comp.createNonNegative(str);
            } catch (NumberFormatException e) {
                LOGGER.error(e);
                return ZERO;
            }
        }

        /**
         * @param contribution1 - status-history contribution (of label1)
         * @param contribution2 - status-history contribution (of label2)
         *
         * @return Combined history value using bit-wise OR operation on
         * contribution1 and contribution2, after converting them to maintained
         * Number type for status-history.<br/><br/>
         *
         * Correctness of the combined value is guaranteed only if the arguments
         * were first obtained {@link #fromBitIndex(int)}, {@link #fromString(String)}...
         * since conversion to the maintained type may round/truncate them.
         */
        public static Number combine(Number contribution1, Number contribution2) {
            return comp.bitOr(contribution1, contribution2);
        }

        /**
         * @param contribution - status-history contribution (of a label)
         * @param from - status-history value from which 'contribution' is to be erased.
         *
         * @return History value after erasing 'contribution'.<br/><br/>
         *
         * Correctness of the reverted value is guaranteed only if the arguments
         * were first obtained {@link #fromBitIndex(int)}, {@link #fromString(String)}...
         * since conversion to the maintained type may round/truncate them.
         */
        public static Number revert(Number contribution, Number from) {
            return comp.andNot(from, contribution);
        }
    }

}
