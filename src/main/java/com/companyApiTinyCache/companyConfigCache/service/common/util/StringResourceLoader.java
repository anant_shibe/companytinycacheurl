package com.companyApiTinyCache.companyConfigCache.service.common.util;

public interface StringResourceLoader<R> {
    R loadResource(String s);
}
