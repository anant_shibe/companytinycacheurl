package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.data.HealthCheckResponse;

import java.util.List;

public interface HealthCheckService {

    List<HealthCheckResponse> checkUrls(String appName);
}
