package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CachedResource Containing Currency Conversion Rates to INR.
 * @author suresh
 *
 */
public class CachedCurrencyRates extends CachedUrlResource<Map<String, Double>> {

    private static final Log logger = LogFactory.getLog(CachedUrlResource.class);

    private static final String CURRENCY_JS = "currency.js";

    public CachedCurrencyRates(CachedProperties pcommonCachedProperties) throws MalformedURLException {
        super(pcommonCachedProperties, "ct.services.currencyjs.url.ctconfig", null, false);
        refreshResource();
    }

    /**
     * @see com.cleartrip.common.util.CachedUrlResource#loadStreamResource(InputStream)
     */
    @Override
    protected Map<String, Double> loadStreamResource(InputStream is) throws Exception {
        Map<String, Double> currencyRates = new HashMap<String, Double>();
        Reader reader = new InputStreamReader(is);
        StringBuilder sb = new StringBuilder(128);
        char[] buf = new char[128];
        int len;
        while ((len = reader.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        reader.close();
        String json = sb.toString();
        int i = json.indexOf('[');
        int pos = json.indexOf(']');
        json = json.substring(i, pos + 1);
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        List<Map<String, Object>> currencyConversions = mapper.readValue(json, List.class);
        len = currencyConversions.size();
        for (i = 0; i < len; i++) {
            Map<String, Object> currencyConversionMap = currencyConversions.get(i);
            String toCurrency = ((String) currencyConversionMap.get("code")).trim().toUpperCase();
            sb.setLength(0);
            String key = sb.append(toCurrency).append("-INR").toString();
            Object rateObj = currencyConversionMap.get("rate");
            double rate;
            if (rateObj instanceof String) {
                rate = Double.parseDouble((String) rateObj);
            } else {
                rate = (Double) rateObj;
            }
            currencyRates.put(key, rate);
        }

        return currencyRates;
    }


}
