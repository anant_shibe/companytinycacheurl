/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.web.admin.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.util.tinycache.TinyCache;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Suresh
 * 
 */
public class ListCacheController extends AbstractController {

    public ListCacheController() {
        setCacheSeconds(0);
    }

    private ApplicationContext getContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    private void getCacheBeanNames(ApplicationContext ctx, Map<String, TinyCache> beanNames) {

        if (ctx != null) {
            @SuppressWarnings("unchecked")
            Map<String, TinyCache> tinyCacheBeans = ctx.getBeansOfType(TinyCache.class, false, false);

            if (tinyCacheBeans != null && tinyCacheBeans.size() > 0) {
                beanNames.putAll(tinyCacheBeans);
            }
            getCacheBeanNames(ctx.getParent(), beanNames);
        }
    }

    @Override
    public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ApplicationContext ctx = getContext();
        Map<String, TinyCache> allBeansMap = new TreeMap<String, TinyCache>();
        getCacheBeanNames(ctx, allBeansMap);
        int i, len = allBeansMap.size();
        Map<String, Map<String, TinyCache>> packageMap = new TreeMap<String, Map<String, TinyCache>>();

        for (String beanName : allBeansMap.keySet()) {
            String packageName = getPackageName(allBeansMap.get(beanName));
            Map<String, TinyCache> beanMap = packageMap.get(packageName);
            if (beanMap == null) {
                beanMap = new TreeMap<String, TinyCache>();
                packageMap.put(packageName, beanMap);
            }
            beanMap.put(beanName, allBeansMap.get(beanName));

        }
        return new ModelAndView("listCaches", "packageMap", packageMap);
    }

    private String getPackageName(TinyCache tinyCache) {
        String className = tinyCache.getClass().getName();
        className = className.substring(14);// Remove com.cleartrip.
        String packageName = "";
        int index = className.indexOf('.');
        if (index > 0) {
            packageName = className.substring(0, index);
        }
        return packageName;
    }

}
