package com.companyApiTinyCache.companyConfigCache.service.common.util;

/**
 * Specifies the resourceName method for an Enum Resource in CachedEnumResources.
 * 
 * @author Suresh
 * 
 */
public interface EnumResourceName {
    String getResourceName();
}
