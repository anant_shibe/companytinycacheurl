package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

@ClassExcludeCoverage
public class RabbitMQCreateChannelException extends Exception {

	private static final long serialVersionUID = -6383585151033801707L;
	public RabbitMQCreateChannelException() {
		super();
	}
	public RabbitMQCreateChannelException(String message) {
		super(message);
	}
	public RabbitMQCreateChannelException(Throwable cause) {
		super(cause);
	}
	public RabbitMQCreateChannelException(String message, Throwable cause) {
		super(message, cause);
	}

}

