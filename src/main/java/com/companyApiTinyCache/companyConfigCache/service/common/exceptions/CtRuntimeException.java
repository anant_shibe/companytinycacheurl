package com.companyApiTinyCache.companyConfigCache.service.common.exceptions;

/**
 * The Class CtRuntimeException.
 */
public class CtRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 8128969971920559352L;

    /**
     * Instantiates a new ct runtime exception.
     */
    public CtRuntimeException() {
    }

    /**
     * Instantiates a new ct runtime exception.
     * 
     * @param message
     *            the message
     */
    public CtRuntimeException(String message) {
        super(message);
    }

    /**
     * Instantiates a new ct runtime exception.
     * 
     * @param cause
     *            the cause
     */
    public CtRuntimeException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new ct runtime exception.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public CtRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

}
