package com.companyApiTinyCache.companyConfigCache.service.common.util.soap;

/*
 * This class provides a simple API to invoke SOAP services
 */
public interface SoapClient {
    SoapResponse invoke(SoapRequest request) throws SoapException;
}
