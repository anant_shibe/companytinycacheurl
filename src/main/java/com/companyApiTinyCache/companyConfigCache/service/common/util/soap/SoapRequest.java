package com.companyApiTinyCache.companyConfigCache.service.common.util.soap;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.*;

import java.util.Map;

@ClassExcludeCoverage
public class SoapRequest {

    private String targetEndpointUrl;

    private String requestUrl;

    private String requestContents;

    private Map<String, String> headers;

    private int retryCount; // No. of times to retry the request, in case of Axis fault. Defaults to 0

    public String getTargetEndpointUrl() {
        return targetEndpointUrl;
    }

    public void setTargetEndpointUrl(String targetEndpointUrl) {
        this.targetEndpointUrl = targetEndpointUrl;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestContents() {
        return requestContents;
    }

    public void setRequestContents(String requestContents) {
        this.requestContents = requestContents;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

}
