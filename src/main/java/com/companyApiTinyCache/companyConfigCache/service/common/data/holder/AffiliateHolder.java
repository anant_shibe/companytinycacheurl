package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.Affiliate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
*
* @author amith
*
*/
public class AffiliateHolder implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = -3267238802287239208L;

    @JsonProperty("company")
    private Affiliate affiliate;

    @JsonProperty("company")
    public Affiliate getAffiliate() {
        return affiliate;
    }

    @JsonProperty("company")
    public void setAffiliate(Affiliate affiliate) {
        this.affiliate = affiliate;
    }

}
