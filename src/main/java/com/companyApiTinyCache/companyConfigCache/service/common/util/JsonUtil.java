package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import org.apache.commons.io.input.CharSequenceReader;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/**
 * The Class JsonUtil.
 */
public final class JsonUtil {

    private static final ObjectMapper mapper = getNewObjectMapper();

    private JsonUtil() {
    }

    /**
     * Parses the collection.
     *
     * @param <T> Type of the collection
     * @param content json to parse
     * @param clazz
     *            the clazz
     * @return the collection
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static <T> List<T> parseCollection(String content, Class<T> clazz) throws IOException {
        List<T> collection = null;
        if (content != null) {
            CollectionLikeType collectionLikeType = getObjectMapper().getTypeFactory().constructCollectionLikeType(List.class, clazz);
            //JavaType javaType = CollectionType.typed(ArrayList.class, SimpleType.construct(clazz, null));
            ObjectMapper mapper = getObjectMapper();
            collection = mapper.readValue(content, collectionLikeType);
        }

        return collection;
    }

    /**
     * Parses the collection.
     *
     * @param <T>
     *            the generic type
     * @param inputStream
     *            the input stream
     * @param clazz
     *            the clazz
     * @return the list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static <T> List<T> parseCollection(InputStream inputStream, Class<T> clazz) throws IOException {
        CollectionLikeType collectionLikeType = getObjectMapper().getTypeFactory().constructCollectionLikeType(ArrayList.class, clazz);
    	//JavaType javaType = CollectionType.typed(ArrayList.class, SimpleType.construct(clazz, null));
        ObjectMapper mapper = getObjectMapper();
        List<T> collection = mapper.readValue(inputStream, collectionLikeType);

        return collection;
    }

    /**
     * Parses the collection.
     *
     * @param <T>
     *            the generic type
     * @param reader
     *            the reader
     * @param clazz
     *            the clazz
     * @return the list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static <T> List<T> parseCollection(Reader reader, Class<T> clazz) throws IOException {
        CollectionLikeType collectionLikeType = getObjectMapper().getTypeFactory().constructCollectionLikeType(ArrayList.class, clazz);
    	//JavaType javaType = CollectionType.typed(ArrayList.class, SimpleType.construct(clazz, null));
        ObjectMapper mapper = getObjectMapper();
        List<T> collection = mapper.readValue(reader, collectionLikeType);
        return collection;
    }

    /**
     * Parses the map.
     *
     * @param <S>
     *            the generic type
     * @param <T>
     *            the generic type
     * @param inputStream
     *            the input stream
     * @param keyClass
     *            the key class
     * @param valClass
     *            the val class
     * @return the map
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static <S, T> Map<S, T> parseMap(InputStream inputStream, Class<S> keyClass, Class<T> valClass) throws IOException {
        MapType mapType = getObjectMapper().getTypeFactory().constructMapType(Map.class, keyClass, valClass);
    	//JavaType javaType = MapType.typed(Map.class, SimpleType.construct(keyClass, null), SimpleType.construct(valClass, null));
        ObjectMapper mapper = getObjectMapper();
        Map<S, T> map = mapper.readValue(inputStream, mapType);
        return map;
    }

    /**
     * Parses the map.
     *
     * @param <S>
     *            the generic type
     * @param <T>
     *            the generic type
     * @param content
     *            the content
     * @param keyClass
     *            the key class
     * @param valClass
     *            the val class
     * @return the map
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static <S, T> Map<S, T> parseMap(String content, Class<S> keyClass, Class<T> valClass) throws IOException {
        Map<S, T> map = null;
        if (content != null) {
            MapType mapType = getObjectMapper().getTypeFactory().constructMapType(Map.class, keyClass, valClass);
        	//JavaType javaType = MapType.typed(Map.class, SimpleType.construct(keyClass, null), SimpleType.construct(valClass, null));
            ObjectMapper mapper = getObjectMapper();
            map = mapper.readValue(content, mapType);
        }
        return map;
    }

    /**
     * Parses the map.
     *
     * @param <S>
     *            the generic type
     * @param <T>
     *            the generic type
     * @param reader
     *            the reader
     * @param keyClass
     *            the key class
     * @param valClass
     *            the val class
     * @return the map
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static <S, T> Map<S, T> parseMap(Reader reader, Class<S> keyClass, Class<T> valClass) throws IOException {
        MapType mapType = getObjectMapper().getTypeFactory().constructMapType(Map.class, keyClass, valClass);
    	//JavaType javaType = MapType.typed(Map.class, SimpleType.construct(keyClass, null), SimpleType.construct(valClass, null));
        ObjectMapper mapper = getObjectMapper();
        Map<S, T> map = mapper.readValue(reader, mapType);
        return map;
    }

    /**
     * Parses the array.
     *
     * @param <T> Type of the array
     * @param content Json to parse
     * @param clazz
     *            the clazz
     * @return the t[]
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static <T> T[] parseArray(String content, Class<T> clazz) throws IOException {
        T [] arr = null;
        if (content != null) {
            List<T> l = parseCollection(content, clazz);
            arr = l.toArray((T []) Array.newInstance(clazz, l.size()));
        }
        return arr;
    }

    /**
     * Parses the array.
     *
     * @param <T>
     *            the generic type
     * @param inputStream
     *            the input stream
     * @param clazz
     *            the clazz
     * @return the t[]
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] parseArray(InputStream inputStream, Class<T> clazz) throws IOException {
        List<T> l = parseCollection(inputStream, clazz);
        T [] arr = l.toArray((T []) Array.newInstance(clazz, l.size()));
        return arr;
    }

    /**
     * @return The singleton instance of ObjectMapper with default settings.
     *  If custom config is to be added to the ObjectMapper, use {@link #getNewObjectMapper()} instead.
     */
    public static ObjectMapper getObjectMapper() {
        return mapper;
    }

    // Skip TimeZone as of now.
    @SuppressWarnings("unused")
    private static class TimeZoneDeserializer extends JsonDeserializer<TimeZone> {
        @Override
        public TimeZone deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            return null;
        }

    }

    /**
     * Ideal for using in those scenarios where custom config is to be added to the ObjectMapper.
     * Consider maintaining the returned mapper as a singleton instead of calling this method
     * for every usage of the mapper (known to improve performance).
     *
     * @return a new instance of ObjectMapper with default settings
     */
    public static ObjectMapper getNewObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper;
    }

    public static void writeJson(Object o, Writer w, boolean prettyPrint) throws JsonGenerationException, IOException {
        if (o != null) {
            ObjectMapper m = getObjectMapper();
            if (prettyPrint) {
                JsonFactory jsonFactory = new JsonFactory();
                JsonGenerator jgen = jsonFactory.createGenerator(w);
                jgen.useDefaultPrettyPrinter();
                m.writeValue(jgen, o);
            } else {
                m.writeValue(w, o);
            }
        }
    }

    public static String toJson(Object o, StringBuilderWriter sbw, boolean prettyPrint) throws JsonGenerationException, IOException {
        String s = null;
        if (o != null) {
            if (sbw != null) {
                sbw.clear();
            } else {
                sbw = new StringBuilderWriter();
            }
            writeJson(o, sbw, prettyPrint);
            s = sbw.toString();
        }

        return s;
    }

    public static String toJson(Object o, boolean prettyPrint) throws JsonGenerationException, IOException {
        return toJson(o, null, prettyPrint);
    }

    public static String toJson(Object o) throws JsonGenerationException, IOException {
        return toJson(o, null, false);
    }

    /**
     * Utility method to recursively change all Map instances to a TreeMap in an object parsed using Jackson.
     * Re-saving it with Pretty print option makes all keys in sorted order so that a diff can be run on another
     * json sorted using this utility. This way of comparing cannot handle lists which have the order changed.
     *
     * Please see the "main" method which uses this api for providing a program to standardizing a json in sorted format.
     *
     * Wrote this to compare results of show India data from jvm in http://hotel-mis.cltp.com:9001/r1/ctb-admin/
     * for hotels after migrating json api from json-lib to jackson since json-lib is terribly slow to parse large jsons
     * in Java 8 for some reason.
     *
     * Other methods of json diff which could not be used:
     *
     * http://tlrobinson.net/projects/javascript-fun/jsondiff/ :: (Javascript based. So browser crashed because of huge json)
     *
     * https://www.npmjs.com/package/json-diff :: Would have worked but did not have the patience to setup node.js and install this package
     *
     * changeNumberToString parameter was added to change Integers to String fully or under a specific tag (See commented code). This was
     * because the older Cache Hash Loader incorrectly generated a mixture of String and Integers under pseudonyms which resulted in a lot
     * of spurious diffs. After adding this option got 0 diffs for IN and AE listings.
     *
     * @param jsonObject The Object parsed from a json using jackson api
     * @param changeNumberToString Convert all Numbers (int and double) to Strings, used in recursion call also
     * to change all numbers under particular tag to a String. See commented code.
     * @param sbw Temp StringBuilderWriter
     * @return Sorted json Object obtained by recursively replacing all Map instances by TreeMap instances
     * @throws IOException On IOException
     */
    public static Object sortParsedJson(Object jsonObject, boolean changeNumberToString, StringBuilderWriter sbw) throws IOException {
        Object newJsonObject = jsonObject;
        if (jsonObject != null) {
            if (jsonObject instanceof Map) {
                Map<String, Object> jsonMap = (Map<String, Object>) jsonObject;
                Map<String, Object> newJsonMap = new TreeMap<String, Object>();
                for (String key : jsonMap.keySet()) {
                    /* Uncomment below code to change all numbers under "psuedonyms" tag to Strings
                    if (key.equals("psuedonyms")) {
                        changeNumberToString = true;
                    }
                    */
                    Object sortedVal = sortParsedJson(jsonMap.get(key), changeNumberToString, sbw);
                    /*
                    if (key.equals("offer") && sortedVal instanceof List) {
                        TreeMap<String, Object> sortedEntries = new TreeMap<String, Object>();
                        List l = (List) sortedVal;
                        for (Object item : l) {
                            sortedEntries.put(toJson(item, sbw, false), item);
                        }
                        l.clear();
                        l.addAll(sortedEntries.values());
                    }
                    */
                    newJsonMap.put(key, sortedVal);
                }
                newJsonObject = newJsonMap;
            } else if (jsonObject instanceof List) {
                List<Object> jsonList = (List<Object>) jsonObject;
                int i = 0, len = jsonList.size();
                for (i = 0; i < len; i++) {
                    jsonList.set(i, sortParsedJson(jsonList.get(i), changeNumberToString, sbw));
                }
            }
        }
        if (changeNumberToString && newJsonObject instanceof Number) {
            newJsonObject = newJsonObject.toString();
        }
        // Below was added to ignore small double number differences for testing chmm jvm cache implementation
        // comment if you need exact double matching
        if (newJsonObject instanceof Double) {
            double d = (Double) newJsonObject;
            long l = Math.round(d * 100000000);
            newJsonObject = l / 100000000.0;
        }
        return newJsonObject;
    }

    public static String sortJson(String json, StringBuilderWriter sbw, boolean prettyPrint) throws IOException {
        ObjectMapper m = getObjectMapper();
        Object jsonObject = m.readValue(json, Object.class);
        jsonObject = sortParsedJson(jsonObject, false, sbw);
        String newJson = toJson(jsonObject, sbw, prettyPrint);
        return newJson;
    }

    public static String sortJson(String json) throws IOException {
        return sortJson(json, (StringBuilderWriter) null, true);
    }

    public static void sortJson(Object o, StringBuilder sb, boolean prettyPrint) throws IOException {
        StringBuilderWriter sbw = new StringBuilderWriter(sb);
        sb.setLength(0);
        writeJson(o, sbw, false);
        Reader r = new CharSequenceReader(sb);
        ObjectMapper m = getObjectMapper();
        Object jsonObject = m.readValue(r, Object.class);
        sb.setLength(0);
        jsonObject = sortParsedJson(jsonObject, false, sbw);
        sb.setLength(0);
        writeJson(jsonObject, sbw, prettyPrint);
    }

    public static void main(String [] args) throws Exception {
        if (args.length == 0) {
            // System.out.println("Usage: java JsonUtil <inputFile> [outputFile default inputFile_sorted.<extn>]");
            System.exit(1);
        }
        ObjectMapper m = getObjectMapper();
        File inputFile = new File(args[0]);
        File outputFile;
        if (args.length > 1) {
            outputFile = new File(args[1]);
        } else {
            String outputFileName;
            String inputFileName = inputFile.getName();
            int pos = inputFileName.lastIndexOf('.');
            if (pos >= 0) {
                outputFileName = inputFileName.substring(0, pos) + "_sorted" + inputFileName.substring(pos);
            } else {
                outputFileName = inputFileName + "_sorted";
            }
            outputFile = new File(inputFile.getParentFile(), outputFileName);
        }
        Object jsonObject = m.readValue(inputFile, Object.class);
        jsonObject = sortParsedJson(jsonObject, false, null);
        JsonFactory jsonFactory = new JsonFactory();
        JsonGenerator jgen = jsonFactory.createGenerator(outputFile, JsonEncoding.UTF8);
        jgen.useDefaultPrettyPrinter();
        m.writeValue(jgen, jsonObject);
        // System.out.println("Successfully saved sorted json to: " + outputFile);
    }
}
