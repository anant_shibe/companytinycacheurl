package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.LogLevel;
import com.fasterxml.jackson.databind.JsonMappingException;

import java.io.Serializable;

public class LogMessage implements Serializable {

    private static final long serialVersionUID = 1891404728983361210L;

    private String name;

    private String message;

    private LogLevel logLevel;

    private Throwable exception;

    private long time;

    public LogMessage(String pname, String pmessage, LogLevel plogLevel, Throwable pexception) {
        if (plogLevel == null) {
            plogLevel = LogLevel.INFO;
        }
        name = pname;
        message = pmessage;
        logLevel = plogLevel;
        // The below jazz because JsonMappingException.JsonLocation not Serializable
        if (pexception instanceof JsonMappingException) {
            JsonMappingException jmp = (JsonMappingException) pexception;
            pexception = new Exception(jmp.getMessage());
        }
        exception = pexception;
        time = System.currentTimeMillis();
    }

    public LogMessage(String pmessage, LogLevel plogLevel) {
        this(null, pmessage, plogLevel, null);
    }

    public LogMessage(String pmessage) {
        this(null, pmessage, null, null);
    }

    public LogMessage() {
    }

    public String getName() {
        return name;
    }

    public void setName(String pname) {
        name = pname;
    }

    /**
     * Getter for message.
     * 
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setter for message.
     * 
     * @param pmessage
     *            the message to set
     */
    public void setMessage(String pmessage) {
        message = pmessage;
    }

    /**
     * Getter for logLevel.
     * 
     * @return the logLevel
     */
    public LogLevel getLogLevel() {
        return logLevel;
    }

    /**
     * Setter for logLevel.
     * 
     * @param plogLevel
     *            the logLevel to set
     */
    public void setLogLevel(LogLevel plogLevel) {
        logLevel = plogLevel;
    }

    /**
     * Getter for exception.
     * 
     * @return the exception
     */
    public Throwable getException() {
        return exception;
    }

    /**
     * Setter for exception.
     * 
     * @param pexception
     *            the exception to set
     */
    public void setException(Throwable pexception) {
        if (pexception instanceof JsonMappingException) {
            JsonMappingException jmp = (JsonMappingException) pexception;
            pexception = new Exception(jmp.getMessage());
        }
        exception = pexception;
    }

    /**
     * Getter for time.
     * 
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * Setter for time.
     * 
     * @param ptime
     *            the time to set
     */
    public void setTime(long ptime) {
        time = ptime;
    }

}
