package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.ValueUpdater;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ServerInfoBasic extends ServerInfoAbstract {

    private static final Log logger = LogFactory.getLog(ServerInfoBasic.class);

    private static class HostPortComparator implements Comparator<String> {

        @Override
        public int compare(String s1, String s2) {
            int pos1, pos2;
            int result = Integer.MAX_VALUE;
            if (s1 != null && s2 != null && (pos1 = s1.indexOf(':')) > 0 && (pos2 = s2.indexOf(':')) > 0) {
                try {
                    String h1 = s1.substring(0, pos1);
                    String p1Str = s1.substring(pos1 + 1);
                    int p1 = Integer.parseInt(p1Str);
                    String h2 = s2.substring(0, pos2);
                    String p2Str = s2.substring(pos2 + 1);
                    int p2 = Integer.parseInt(p2Str);
                    result = h1.compareTo(h2);
                    if (result == 0) {
                        result = p1 - p2;
                    }
                } catch (Exception e) {
                    logger.error("Error hostNPort comparing: " + s1 + " and " + s2, e);
                }
            }
            if (result == Integer.MAX_VALUE) {
                result = GenUtil.compare(s1, s2);
            }

            return result;
        }

    }

    public static final HostPortComparator HOST_PORT_COMPARATOR = new HostPortComparator();

    private static class ServerListUpdater implements ValueUpdater {

        private ServerInfoBasic parent;

        private List<String> serverList;

        private boolean isDelete;

        public ServerListUpdater(ServerInfoBasic pparent, List<String> pserverList, boolean pisDelete) {
            parent = pparent;
            serverList = pserverList;
            isDelete = pisDelete;
        }

        @Override
        public Object update(Object value, int tryIndex) {
            int len;
            Object updatedValue = ValueUpdater.SKIP_UPDATE;
            List<String> servers = null;
            List<String> newServers = null;
            String serversJson = (String) value;
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            if (serversJson != null) {
                try {
                    servers = mapper.readValue(serversJson, new TypeReference<List<String>>() {
                    });
                } catch (Exception e) {
                    logger.error("Error Updating Server Registry", e);
                }
            }
            List<String> modifiedServers = serverList;
            if (modifiedServers == null) {
                modifiedServers = new ArrayList<String>();
            }
            if (serverList == null && parent != null) {
                String h = StringUtils.trimToNull(parent.host);
                int p = parent.getPort();
                modifiedServers.add(h + ':' + p);
            }
            if (servers != null || !isDelete && modifiedServers.size() > 0) {
                if (servers == null) {
                    servers = new ArrayList<String>();
                }
                if (isDelete) {
                    if (servers.removeAll(modifiedServers)) {
                        newServers = servers;
                    }
                } else {
                    for (String hostNPort : modifiedServers) {
                        if (!servers.contains(hostNPort)) {
                            servers.add(hostNPort);
                        }
                    }
                    Collections.sort(servers, HOST_PORT_COMPARATOR);
                    newServers = servers;
                }
                if (newServers != null) {
                    len = 512;
                    if (serversJson != null) {
                        len = serversJson.length();
                    }
                    StringBuilderWriter sbw = new StringBuilderWriter(new StringBuilder(len));
                    try {
                        mapper.writeValue(sbw, newServers);
                        updatedValue = sbw.toString();
                    } catch (Exception e) {
                        logger.error("Error Converting to Json for Server Registry", e);
                    }
                }
            }

            return updatedValue;
        }

    }

    private static final long serialVersionUID = -1997980398606593082L;

    private ServerInfo serverInfo;

    private volatile String[] serverInfoNames;

    private volatile String host;

    private volatile String ipAddress;

    private volatile int runtimePort;

    private volatile int configPort;

    private volatile String type;

    private List<String> apps;

    private volatile long lpt;

    private volatile String buildTag;

    private volatile String buildTime;

    private volatile String svnRevision;

    private final ServerListUpdater insertServerListUpdater = new ServerListUpdater(this, null, false);

    private final ServerListUpdater deleteServerListUpdater = new ServerListUpdater(this, null, false);

    public ServerInfoBasic(ServerInfo pserverInfo, boolean initializeIpAddress) {
        super("BASIC");
        serverInfo = pserverInfo;
        if (initializeIpAddress) {
            ipAddress = GenUtil.getLocalIpAddress();
        }
    }

    public ServerInfoBasic() {
        this(null, false);
    }

    public static final Comparator<ServerInfoBasic> hostComparator = new HostComparator();

    private static class HostComparator implements Comparator<ServerInfoBasic> {

        @Override
        public int compare(ServerInfoBasic s1, ServerInfoBasic s2) {
            int result = 0;
            if (s1 != null) {
                if (s2 != null) {
                    result = GenUtil.compare(s1.host, s2.host);
                    if (result == 0) {
                        result = s1.getPort() - s2.getPort();
                    }
                } else {
                    result = 1;
                }
            } else if (s2 != null) {
                result = -1;
            }

            return result;
        }

    }

    public ValueUpdater getServerListUpdater(boolean pisDelete) {
        return pisDelete ? deleteServerListUpdater : insertServerListUpdater;
    }

    public ValueUpdater getServerListUpdater(String phost, int pport, boolean pisDelete) {
        phost = StringUtils.trimToNull(phost);
        List<String> serverList = new ArrayList<String>();
        if (phost != null) {
            serverList.add(phost + ':' + pport);
        }
        ServerListUpdater serverListUpdater = new ServerListUpdater(null, serverList, pisDelete);
        return serverListUpdater;
    }

    public ValueUpdater getServerListUpdater(List<String> pserverList, boolean pisDelete) {
        ServerListUpdater serverListUpdater = new ServerListUpdater(null, pserverList, pisDelete);
        return serverListUpdater;
    }

    public synchronized void addApp(String app) {
        if (app != null) {
            int len = 0;
            if (apps != null) {
                len = apps.size();
            }
            List<String> newApps = new ArrayList<String>(len + 1);
            if (len > 0) {
                newApps.addAll(apps);
            }
            newApps.add(app);
            Collections.sort(newApps);
            apps = Collections.unmodifiableList(newApps);
        }
        setLastUpdated(System.currentTimeMillis());
    }

    public final List<String> getApps() {
        return apps;
    }

    public synchronized final void setApps(List<String> papps) {
        List<String> newApps = papps;
        if (papps != null) {
            newApps = new ArrayList<String>(papps);
            Collections.sort(newApps);
            newApps = Collections.unmodifiableList(newApps);
        }
        apps = newApps;
        setLastUpdated(System.currentTimeMillis());
    }

    @JsonIgnore
    public long getLpt() {
        return lpt;
    }

    public void setLpt(long plpt) {
        lpt = plpt;
    }

    public String[] getServerInfoNames() {
        int i;
        String[] infoNames = serverInfoNames;
        if (infoNames == null && serverInfo != null) {
            ServerInfoAbstract[] serverInfos = serverInfo.getServerInfos();
            if (serverInfos != null) {
                infoNames = new String[serverInfos.length];
                for (i = 0; i < infoNames.length; i++) {
                    infoNames[i] = serverInfos[i].getName();
                }
            }
            serverInfoNames = infoNames;
        }
        return infoNames;
    }

    public void setServerInfoNames(String[] pserverInfoNames) {
        serverInfoNames = pserverInfoNames;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String phost) {
        host = phost;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String pipAddress) {
        ipAddress = pipAddress;
    }

    public int getRuntimePort() {
        return runtimePort;
    }

    public void setRuntimePort(int pruntimePort) {
        runtimePort = pruntimePort;
    }

    public int getConfigPort() {
        return configPort;
    }

    public void setConfigPort(int pconfigPort) {
        configPort = pconfigPort;
    }

    public int getPort() {
        int p = runtimePort;
        if (p <= 0) {
            p = configPort;
        }

        return p;
    }

    public String getType() {
        return type;
    }

    public void setType(String ptype) {
        type = ptype;
    }

    public String getBuildTag() {
        return buildTag;
    }

    public void setBuildTag(String pbuildTag) {
        buildTag = pbuildTag;
    }

    public String getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(String pbuildTime) {
        buildTime = pbuildTime;
    }

    public String getSvnRevision() {
        return svnRevision;
    }

    public void setSvnRevision(String psvnRevision) {
        svnRevision = psvnRevision;
    }

}
