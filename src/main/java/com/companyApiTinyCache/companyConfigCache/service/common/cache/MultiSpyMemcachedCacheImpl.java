package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Map;

/**
 * Cache implementation which uses distributes the calls among a set of clients for get and set calls.

 * @author suresh
 */
@ClassExcludeCoverage
public class MultiSpyMemcachedCacheImpl extends CacheBase {

    private static class ClientWorkers {
        private SpyMemcachedCacheImpl[] workers;

        private int workerIndex;

        public ClientWorkers(CachedProperties pcachedProperties, String pserversProperty, String servers, int numWorkers) {
            workers = new SpyMemcachedCacheImpl[numWorkers];
            int i;
            for (i = 0; i < numWorkers; i++) {
                workers[i] = new SpyMemcachedCacheImpl(pcachedProperties, pserversProperty, servers);
            }
        }

        public synchronized SpyMemcachedCacheImpl getWorker() {
            workerIndex++;
            if (workerIndex >= workers.length) {
                workerIndex = 0;
            }

            return workers[workerIndex];
        }

        public void destroy() throws Exception {
            int i;
            for (i = 0; i < workers.length; i++) {
                workers[i].destroy();
            }
        }
    }

    private static final String NUM_GETTERS_SUFFIX = ".numgetters";

    private static final String NUM_SETTERS_SUFFIX = ".numsetters";

    private static final String DEFAULT_NUM_GETTERS_PROPERTY = DEFAULT_SERVERS_PROPERTY + NUM_GETTERS_SUFFIX;

    private static final String DEFAULT_NUM_SETTERS_PROPERTY = DEFAULT_SERVERS_PROPERTY + NUM_SETTERS_SUFFIX;

    private static final String SERVICES_PATTERN = ".services";

    private CachedProperties commonCachedProperties;

    private ClientWorkers clientsForGet;

    private ClientWorkers clientsForPut;

    private String serversProperty;

    private String numGettersProperty;

    private String numSettersProperty;

    public MultiSpyMemcachedCacheImpl(CachedProperties pcommonCachedProperties, String pserversProperty, String servers) {
        commonCachedProperties = pcommonCachedProperties;
        if (pserversProperty == null) {
            pserversProperty = DEFAULT_SERVERS_PROPERTY;
        }
        serversProperty = pserversProperty;
        String commoninzedServersProperty = serversProperty;
        int pos = commoninzedServersProperty.indexOf(SERVICES_PATTERN);
        if (pos > 0) {
            // Remove .services
            commoninzedServersProperty = commoninzedServersProperty.substring(0, pos) + commoninzedServersProperty.substring(pos + SERVICES_PATTERN.length());
        }
        numGettersProperty = commoninzedServersProperty + NUM_GETTERS_SUFFIX;
        if (commonCachedProperties.getPropertyValue(numGettersProperty) == null && commonCachedProperties.getPropertyValue(DEFAULT_NUM_GETTERS_PROPERTY) != null) {
            numGettersProperty = DEFAULT_NUM_GETTERS_PROPERTY;
        }
        numSettersProperty = commoninzedServersProperty + NUM_SETTERS_SUFFIX;
        if (commonCachedProperties.getPropertyValue(numSettersProperty) == null && commonCachedProperties.getPropertyValue(DEFAULT_NUM_SETTERS_PROPERTY) != null) {
            numSettersProperty = DEFAULT_NUM_SETTERS_PROPERTY;
        }
        int numGetters = commonCachedProperties.getIntPropertyValue(numGettersProperty, 3);
        int numSetters = commonCachedProperties.getIntPropertyValue(numSettersProperty, 1);

        if (!commonCachedProperties.getBooleanPropertyValue(pserversProperty + ".encode-key", true)) {
            this.setEncodeKeyRequired(false);
        }

        if (numGetters > 0) {
            clientsForGet = new ClientWorkers(pcommonCachedProperties, pserversProperty, servers, numGetters);
        }
        if (numSetters > 0) {
            clientsForPut = new ClientWorkers(pcommonCachedProperties, pserversProperty, servers, numSetters);
            if (numGetters <= 0) {
                clientsForGet = clientsForPut;
            }
        } else {
            clientsForPut = clientsForGet;
        }
    }

    @Override
    public Object getNativeConnection(String key) {
        return clientsForGet.getWorker();
    }

    @Override
    public void clear() throws CacheDeleteException {
        SpyMemcachedCacheImpl client = clientsForGet.getWorker();
        if (client != null) {
            client.clear();
        }
    }

    @Override
    public Object get(String key, Charset charset) throws CacheRetrieveException {
        SpyMemcachedCacheImpl client = clientsForGet.getWorker();
        Object value = null;
        if (client != null) {
            value = client.get(key, charset);
        }

        return value;
    }

    @Override
    public Map<String, Object> getMulti(String[] ids, Charset charset) throws CacheRetrieveException {
        Map<String, Object> valueMap = null;
        SpyMemcachedCacheImpl client = clientsForGet.getWorker();
        if (client != null) {
            valueMap = client.getMulti(ids, charset);
        }

        return valueMap;
    }

    @Override
    public Long incrBy(String key, long integer) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long incr(String key) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long decr(String key) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    @Override
    public Long decrBy(String key, long integer) throws CacheStoreException {
        throw  new CacheStoreException("Not implemented");
    }

    public Map getStatistics() throws CacheException {
        Map<String, String> stats = null;
        SpyMemcachedCacheImpl client = clientsForGet.getWorker();
        if (client != null) {
            stats = client.getStatistics();
        }

        return stats;
    }

    @Override
    public boolean put(String key, Object val, int expirySeconds) throws CacheStoreException {
        boolean status = false;
        SpyMemcachedCacheImpl client = clientsForPut.getWorker();
        if (client != null) {
            status = client.put(key, val, expirySeconds);
        }

        return status;
    }

    @Override
    public boolean syncPut(String key, Object val, int expirySeconds) throws CacheStoreException {
        boolean status = false;
        SpyMemcachedCacheImpl client = clientsForPut.getWorker();
        if (client != null) {
            status = client.syncPut(key, val, expirySeconds);
        }

        return status;
    }

    @Override
    public boolean add(String key, Object val, int expirySeconds) throws CacheStoreException {
        boolean status = false;
        SpyMemcachedCacheImpl client = clientsForPut.getWorker();
        if (client != null) {
            status = client.add(key, val, expirySeconds);
        }

        return status;
    }

    @Override
    public boolean put(String key, Object val, Date dateOfExpiry) throws CacheStoreException {
        boolean status = false;
        SpyMemcachedCacheImpl client = clientsForPut.getWorker();
        if (client != null) {
            status = client.put(key, val, dateOfExpiry);
        }

        return status;
    }

    @Override
    public boolean atomicUpdate(String id, int expirySeconds, ValueUpdater updater, int maxTries) {
        boolean status = false;
        SpyMemcachedCacheImpl client = clientsForGet.getWorker();
        if (client != null) {
            status = client.atomicUpdate(id, expirySeconds, updater, maxTries);
        }

        return status;
    }

    @Override
    public boolean remove(String key) {
        boolean status = false;
        SpyMemcachedCacheImpl client = clientsForPut.getWorker();
        if (client != null) {
            status = client.remove(key);
        }

        return status;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.cleartrip.common.cache.CacheBase#destroy()
     */
    @Override
    public void destroy() throws Exception {
        clientsForGet.destroy();
        if (clientsForPut != clientsForGet) {
            clientsForPut.destroy();
        }
    }
}
