package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.notification.EmailSender;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedVms;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import com.companyApiTinyCache.companyConfigCache.service.common.org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.EnumMap;
import java.util.Map;

/**
 * ContactUsController - Contact us email sender.
 */
public class ContactUsController extends SimpleFormController {

    private static final Log logger = LogFactory.getLog(ContactUsController.class);

    private EmailSender emailSender;

    private CachedVms<CommonConstants.CommonTemplate> commonCachedVms;

    private CachedProperties commonCachedProperties;

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        return new Object();
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        PrintWriter responseWriter = new PrintWriter(response.getWriter());
        try {
            String defaultEmailAddress = commonCachedProperties.getPropertyValue("ct.common.contact.us.default", "information@cleartrip.com");
            String tripId = request.getParameter("trip_id");
            String description = request.getParameter("description");
            String emailAddress = request.getParameter("email_address");
            String productType = request.getParameter("product_type");
            String issueType = request.getParameter("issue_type");
            String subject = "";
            if (StringUtils.isNotBlank(productType)) {
            	subject = request.getParameter("subjectLine");
            } else {
            	productType = issueType;
            	subject = getSubjectLineFromProperty(issueType);
            }
            String toEmailKey = "ct.services.contact.us." + productType.toLowerCase();
            String toEmailAddresses = commonCachedProperties.getPropertyValue(toEmailKey, defaultEmailAddress);
            EnumMap<CommonConstants.CommonTemplate, Template> templates = commonCachedVms.getResource();

            Template template = templates.get(CommonConstants.CommonTemplate.CONATCT_US_SUBJECT_VM);
            VelocityContext context = new VelocityContext();
            context.put("subject", subject);
            StringBuilderWriter writer = new StringBuilderWriter(new StringBuilder(256));
            String subjectMessage = GenUtil.mergeTemplateIntoString(template, context, writer);

            template = templates.get(CommonConstants.CommonTemplate.CONTACT_US_BODY_VM);
            context = new VelocityContext();
            context.put("description", description);
            context.put("tripid", tripId);
            context.put("cust_email_address", emailAddress);
            writer = new StringBuilderWriter(new StringBuilder(256));
            String bodyMessage = GenUtil.mergeTemplateIntoString(template, context, writer);

            String fromEmailAddress = emailAddress;

            logger.info("Subject: " + subjectMessage);
            logger.info("Body: " + bodyMessage);

            emailSender.sendEmail(toEmailAddresses, fromEmailAddress, subjectMessage, bodyMessage, null, null);
            responseWriter.println("success");

        } catch (Exception e) {
            logger.error("Error Sending Email", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            responseWriter.println("failure");
        } finally {
            responseWriter.flush();
            responseWriter.close();
        }
        return null;
    }

	@SuppressWarnings("unchecked")
	private String getSubjectLineFromProperty(String issueType) {
		String issueToSubjectMappingJson = commonCachedProperties.getPropertyValue("ct.services.support.issue.to.subject.mapping.json",
				"{\"support\":\"Support\",\"feedback\":\"Feedback\",\"grievance\":\"Grievance\",\"experience\":\"Experience\"}");
		Map<String, String> issueToSubjectMap = APIUtil.deserializeJsonToObject(issueToSubjectMappingJson, Map.class);
		return issueToSubjectMap.get(issueType);
	}

	public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    public void setCommonCachedVms(CachedVms<CommonConstants.CommonTemplate> commonCachedVms) {
        this.commonCachedVms = commonCachedVms;
    }

    public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }

}
