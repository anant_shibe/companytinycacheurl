package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.text.SimpleDateFormat;
import java.util.*;

public class DateRangeUtil {
    private final Calendar CALENDAR = Calendar.getInstance();
    private final int field;
    private final SimpleDateFormat sdf;
    private final SortedSet<DateRange> ranges = new TreeSet<DateRange>();

    public static void main(String[] args) {
        int field = Calendar.DATE;
        DateRangeUtil dateRangeUtil = new DateRangeUtil(field, "dd MMM''yy");
        Calendar cal = Calendar.getInstance();

        dateRangeUtil.addDate(cal.getTime());
        cal.add(field, 1);
        dateRangeUtil.addDate(cal.getTime());

        cal.add(field, 2);
        dateRangeUtil.addDate(cal.getTime());

        cal.add(field, -1);
        dateRangeUtil.addDate(cal.getTime());

        cal.add(field, 10);
        dateRangeUtil.addDate(cal.getTime());

        cal = Calendar.getInstance();
        cal.add(field, 1);
        dateRangeUtil.removeDate(cal.getTime());

        // System.out.println(dateRangeUtil);
    }

    public DateRangeUtil() {
        this(Calendar.DATE, "dd MMM''yy");
    }

    public DateRangeUtil(int field, String format) {
        this.field = field;
        sdf = new SimpleDateFormat(format);
    }

    public void addRange(final Date from, final Date to) {
        Calendar fromCal = (Calendar) CALENDAR.clone();
        fromCal.setTime(from);
        set(fromCal, Calendar.MONTH, 0);
        set(fromCal, Calendar.DAY_OF_MONTH, 0);
        set(fromCal, Calendar.HOUR_OF_DAY, 0);
        set(fromCal, Calendar.MINUTE, 0);
        set(fromCal, Calendar.SECOND, 0);
        set(fromCal, Calendar.MILLISECOND, 0);

        Calendar toCal = (Calendar) CALENDAR.clone();
        toCal.setTime(to);
        set(toCal, Calendar.MONTH, 0);
        set(toCal, Calendar.DAY_OF_MONTH, 0);
        set(toCal, Calendar.HOUR_OF_DAY, 0);
        set(toCal, Calendar.MINUTE, 0);
        set(toCal, Calendar.SECOND, 0);
        set(toCal, Calendar.MILLISECOND, 0);

        if (fromCal.after(toCal))
            return;

        ranges.add(new DateRange(fromCal, toCal));
        return;
    }

    public void removeRange(final Date from, final Date to) {
        Calendar fromCal = (Calendar) CALENDAR.clone();
        fromCal.setTime(from);
        set(fromCal, Calendar.MONTH, 0);
        set(fromCal, Calendar.DAY_OF_MONTH, 0);
        set(fromCal, Calendar.HOUR_OF_DAY, 0);
        set(fromCal, Calendar.MINUTE, 0);
        set(fromCal, Calendar.SECOND, 0);
        set(fromCal, Calendar.MILLISECOND, 0);

        Calendar toCal = (Calendar) CALENDAR.clone();
        toCal.setTime(to);
        set(toCal, Calendar.MONTH, 0);
        set(toCal, Calendar.DAY_OF_MONTH, 0);
        set(toCal, Calendar.HOUR_OF_DAY, 0);
        set(toCal, Calendar.MINUTE, 0);
        set(toCal, Calendar.SECOND, 0);
        set(toCal, Calendar.MILLISECOND, 0);

        if (fromCal.after(toCal))
            return;
        DateRange excluded = new DateRange(fromCal, toCal);
        Iterator<DateRange> iterator = ranges.iterator();
        List<DateRange> rangesToAdd = new ArrayList<DateRange>();
        while (iterator.hasNext()) {
            DateRange range = iterator.next();
            if (!range.from.before(excluded.from) && !range.to.after(excluded.to)) {
                iterator.remove();
                continue;
            }
            if (range.from.before(excluded.from) && range.to.after(excluded.to)) {
                Calendar preFrom = (Calendar) excluded.from.clone();
                preFrom.add(field, -1);
                Calendar postTo = (Calendar) excluded.to.clone();
                postTo.add(field, 1);
                DateRange newRange = new DateRange(postTo, range.to);
                rangesToAdd.add(newRange);
                range.to = preFrom;
                continue;
            }
            if (range.from.before(excluded.from) && range.to.after(excluded.from)) {
                range.to = (Calendar) excluded.from.clone();
                range.to.add(field, -1);
                continue;
            }
            if (range.from.before(excluded.to) && range.to.after(excluded.to)) {
                range.from = (Calendar) excluded.to.clone();
                range.to.add(field, 1);
                continue;
            }
        }

        ranges.addAll(rangesToAdd);
        return;
    }

    public void addDate(final Date date) {
        addRange(date, date);
    }

    public void removeDate(final Date date) {
        removeRange(date, date);
    }

    private void set(final Calendar date, final int field, final int value) {
        if (this.field < field) {
            date.set(field, value);
        }
    }

    @Override
    public String toString() {
        StringBuilder sbr = new StringBuilder();
        merge();
        for (DateRange dateRange : ranges) {
            sbr.append(", ").append(dateRange.toString());
        }
        if (sbr.length() > 0)
            return sbr.substring(2);
        return "No date added";
    }

    private void merge() {
        DateRange preRange = null;
        Iterator<DateRange> iterator = ranges.iterator();
        while (iterator.hasNext()) {
            DateRange dateRange = iterator.next();
            if (preRange != null && preRange.merge(dateRange)) {
                iterator.remove();
            } else {
                preRange = dateRange;
            }
        }
    }

    private class DateRange implements Comparable<DateRange> {
        private Calendar from;
        private Calendar to;

        public DateRange(Calendar from, Calendar to) {
            this.from = (Calendar) from.clone();
            this.to = (Calendar) to.clone();
        }

        public boolean merge(DateRange dateRange) {
            if (justAfter(dateRange.to)) {
                from = dateRange.from;
                dateRange.to = to;
                return true;
            } else if (justBefore(dateRange.from)) {
                to = dateRange.to;
                dateRange.from = from;
                return true;
            }
            return false;
        }

        public boolean justBefore(Calendar date) {
            Calendar temp = (Calendar) to.clone();
            temp.add(field, 1);
            return temp.equals(date);
        }

        public boolean justAfter(Calendar date) {
            Calendar temp = (Calendar) from.clone();
            temp.add(field, -1);
            return temp.equals(date);
        }

        @SuppressWarnings("unused")
        public boolean contains(Calendar date) {
            return !from.after(date) && !to.before(date);
        }

        @Override
        public int compareTo(DateRange dateRange) {
            int retCode = 0;
            retCode = from.compareTo(dateRange.from);
            if (retCode != 0)
                return retCode;
            return to.compareTo(dateRange.to);
        }

        @Override
        public String toString() {
            if (from.equals(to)) {
                return sdf.format(from.getTime());
            }
            return sdf.format(from.getTime()) + "-" + sdf.format(to.getTime());
        }
    }
}
