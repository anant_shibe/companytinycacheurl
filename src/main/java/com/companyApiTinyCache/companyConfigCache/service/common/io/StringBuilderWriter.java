package com.companyApiTinyCache.companyConfigCache.service.common.io;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

import java.io.Writer;

/**
 * Just like Java's StringWriter but uses a StringBuilder. High time java gets one.
 * 
 * @author Suresh
 */
@ClassExcludeCoverage
public class StringBuilderWriter extends Writer {
    StringBuilder sb;

    public StringBuilderWriter(StringBuilder psb) {
        if (psb == null) {
            psb = new StringBuilder();
        }
        sb = psb;
    }

    public StringBuilderWriter() {
        this(null);
    }

    /**
     * {@inheritDoc}
     * 
     * @see Writer#append(char)
     */
    @Override
    public Writer append(char c) {
        sb.append(c);
        return this;
    }

    /**
     * {@inheritDoc}
     *
     * @see Writer#append(CharSequence, int, int)
     */
    @Override
    public Writer append(CharSequence csq, int start, int end) {
        sb.append(csq, start, end);
        return this;
    }

    /**
     * {@inheritDoc}
     *
     * @see Writer#append(CharSequence)
     */
    @Override
    public Writer append(CharSequence csq) {
        sb.append(csq);
        return this;
    }

    /**
     * {@inheritDoc} This implementation of close does nothing.
     *
     * @see Writer#close()
     */
    @Override
    public void close() {
    }

    /**
     * {@inheritDoc} This implementation of flush does nothing.
     *
     * @see Writer#flush()
     */
    @Override
    public void flush() {
    }

    /**
     * {@inheritDoc}
     *
     * @see Writer#write(char[], int, int)
     */
    @Override
    public void write(char[] cbuf, int off, int len) {
        sb.append(cbuf, off, len);
    }

    /**
     * {@inheritDoc}
     *
     * @see Writer#write(char[])
     */
    @Override
    public void write(char[] cbuf) {
        sb.append(cbuf);
    }

    /**
     * {@inheritDoc}
     *
     * @see Writer#write(int)
     */
    @Override
    public void write(int c) {
        sb.append((char) c);
    }

    /**
     * {@inheritDoc}
     *
     * @see Writer#write(String, int, int)
     */
    @Override
    public void write(String str, int off, int len) {
        sb.append(str, off, len);
    }

    /**
     * {@inheritDoc}
     *
     * @see Writer#write(String)
     */
    @Override
    public void write(String str) {
        sb.append(str);
    }

    /**
     * Returns the contents of StringBuilder.
     */
    @Override
    public String toString() {
        return sb.toString();
    }

    /**
     * Sets the Lenght of internal StringBuilder to 0 for reuse.
     */
    public void clear() {
        sb.setLength(0);
    }

    /**
     * Getter for sb.
     * 
     * @return the sb
     */
    public StringBuilder getBuf() {
        return sb;
    }
}
