package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * A simple holder to hold a Redis Lua Script and its sha1 digest.
 *
 * @author suresh
 *
 */
public class RedisScript {

    /*public static final RedisScript DELAYED_DEQUE_SCRIPT = new RedisScript("local m = redis.call('zrevrangebyscore',KEYS[1],ARGV[1],'-inf','LIMIT','0','1')\n" + "if m[1] ~= nil then\n"
            + "    redis.call('zrem',KEYS[1],m[1])\n" + "end\n" + "return m[1]");*/

    /**
     * Operates on a sorted set with timestamp as the score. Takes one key which is the key of the set and a timestamp as argument. Returns the oldest key older than the passed timestamp after
     * removing it from the set.
     */
    public static final RedisScript DELAYED_DEQUE_SCRIPT = new RedisScript("local m = redis.call('zrangebyscore',KEYS[1], '-inf', ARGV[1],'LIMIT','0','1')\n" + "if m[1] ~= nil then\n"
            + "    redis.call('zrem',KEYS[1],m[1])\n" + "end\n" + "return m[1]");

    /**
     * Though Redis provides a SETNX to atomically add a key if it does not exist, it does not take an expiry time. This Redis Lua script provides an equivalent of memcached add command.
     * It takes a key, an expiry time and a value and atomically sets it with the expiry time if it does not exist. This is used for the lock functionality as well to acquire a
     * distributed lock on a cache server with an expiry time. The lock is considered acquired if the script returns 1.
     */
    public static final RedisScript ADD_SCRIPT = new RedisScript("local e = 1 - redis.call('exists',KEYS[1])\nif e == 1 then\n    redis.call('setex',KEYS[1],ARGV[1],ARGV[2])\nend\nreturn e");

    /**
     * remove the specified key from the redis cache.
     */
    public static final RedisScript REMOVE_SCRIPT = new RedisScript("local m = redis.call('del',KEYS[1])\n");

    /**
     * remove the keys matching a pattern from the redis cache.
     */
    public static final RedisScript REMOVE_KEYS_MATCHING_PATTERN_SCRIPT = new RedisScript("return redis.call('del', unpack(redis.call('keys', ARGV[1])))");

    /**
     * find all the keys matching a pattern from the redis cache.
     */
    public static final RedisScript FIND_KEYS_MATCHING_PATTERN_SCRIPT = new RedisScript("return redis.call('keys', ARGV[1])");
    
    /**
     * remove the element from a sorted set.
     */
    public static final RedisScript REMOVE_KEY_FROM_SET_SCRIPT = new RedisScript("return redis.call('zrem', KEYS[1], ARGV[1])\n");
    
    /**
     * get the score of the element in a sorted set.
     */
    public static final RedisScript GET_ITEM_SCORE_IN_SET = new RedisScript("return redis.call('zscore', KEYS[1], ARGV[1])\n");

    public static final RedisScript GET_SEARCH_ID_SCRIPT = new RedisScript("return redis.call('incrby', KEYS[1], ARGV[1])\n");

    /** Class logger. */
    private static final Log log = LogFactory.getLog(RedisScript.class);

    private String script;

    private String sha1sum;

    public RedisScript(String pscript) {
        script = pscript;
        try {
            sha1sum = GenUtil.sha1sum(script);
        } catch (Exception e) {
            log.error("Error generating digest: ", e);
        }
    }

    public String getScript() {
        return script;
    }

    public String getSha1sum() {
        return sha1sum;
    }

}
