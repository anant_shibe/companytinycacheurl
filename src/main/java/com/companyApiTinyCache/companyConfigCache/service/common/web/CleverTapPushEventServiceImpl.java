package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.kafka.KafkaProducer;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestUtil;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;

/**
 * @author parvatkumar
 *
 */
@ClassExcludeCoverage
public class CleverTapPushEventServiceImpl implements CleverTapIntegrationService, CleverTapEnumConstants {

	private static final Log logger = LogFactory.getLog(CleverTapPushEventServiceImpl.class);

	private static final String CLEVER_TAP_UPLOAD_EVNT_URL = "ct.common.clevertap.uploadevent.url";

	private static final String CLEVER_TAP_ACCOUNT_ID_VAL = "ct.clevertap.acc.id";

	private static final String CLEVER_TAP_ACCOUNT_PASSCODE_VAL = "ct.common.clevertap.passcode";

	private static final String KAFKA_BROKER_LIST_PROPERTY ="ct.services.analytics.kafka.broker.list";

	private static final String CT_CLEVERTAP_ASYNC_ENABLED = "ct.clevertap.async.enabled";

	private CachedProperties cachedProperties;
	
	private final Map<String, String> requestHeaders = new HashMap<String, String>();

	private KafkaProducer kafkaProducer;

	private String topic;

	private boolean isAsyncEnabled;

	public CleverTapPushEventServiceImpl(CachedProperties cachedProperties, String topic) {
		this.cachedProperties = cachedProperties;
		this.topic = topic;
		isAsyncEnabled = cachedProperties.getBooleanPropertyValue(CT_CLEVERTAP_ASYNC_ENABLED,false);
		String brokerList = StringUtils.trimToNull(cachedProperties.getPropertyValue(KAFKA_BROKER_LIST_PROPERTY));
		if (isAsyncEnabled && brokerList != null) {
			kafkaProducer = new KafkaProducer(brokerList);
		}
	}
	
	public void addCredentials(){
		requestHeaders.put(ACCOUNT_ID, cachedProperties.getPropertyValue(CLEVER_TAP_ACCOUNT_ID_VAL));
		requestHeaders.put(PASS_CODE, cachedProperties.getPropertyValue(CLEVER_TAP_ACCOUNT_PASSCODE_VAL));
		requestHeaders.put(CONTENT_TYPE, "application/json");
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public String createOrUpdateEvent(Map<String, Object> eventDetailsMap) {
		// read credentials for clevertrap from cachedproperties
		String url;
		RestResponse response = null;
		ObjectMapper mapper = null;
		String jsonResponse = null;
		Map<String, Object> evtData = null;
		if (eventDetailsMap != null && !eventDetailsMap.isEmpty()) {
			if (eventDetailsMap.get(EVENT_TYPE).equals("profile") || eventDetailsMap.get(EVENT_TYPE).equals("event")) {
				// credentials
				url = cachedProperties.getPropertyValue(CLEVER_TAP_UPLOAD_EVNT_URL);
				int timeOut = cachedProperties.getIntPropertyValue(CLEVER_TAP_EVENT_TIMEOUT, 60000);
				addCredentials();

                mapper = new ObjectMapper();
				List<Map<String, Object>> eventDetailsMapList = new ArrayList<Map<String, Object>>();
				evtData = (Map<String, Object>) eventDetailsMap.get(EVENT_DATA);

				eventDetailsMap.put(CleverTapEnumConstants.IDIENTITY, evtData.get(CleverTapEnumConstants.USER_EMAIL));
				eventDetailsMap.put(CleverTapEnumConstants.TIMESTAMP, (int) (new Date().getTime() / 1000));
				Map<String, List<Map<String, Object>>> eventDetailsMapRequest = new HashMap<String, List<Map<String, Object>>>();
				eventDetailsMapList.add(eventDetailsMap);
				eventDetailsMapRequest.put("d", eventDetailsMapList);
				mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
				mapper.setSerializationInclusion(Include.NON_EMPTY);
				String requestJson = null;
				try {
					requestJson = mapper.writeValueAsString(eventDetailsMapRequest);
				} catch (Exception e) {
					logger.error("Exception In JSON Processing:"  + e);
				}

				try {
					response = RestUtil.post(url, requestJson, null, "application/json", requestHeaders, null, "POST",
							timeOut);
					jsonResponse = response.getMessage();
					return jsonResponse;

				} catch (Exception e) {
					logger.error("Exception in getResponseFromCleverTrap");
				}

			}

		}
		return null;

	}
	
	public String pushNotification(Map<String, Object> pushNotificationDetails){
		String pushNotificationUrl;
		RestResponse response;
		ObjectMapper mapper = null;
		String requestJson = null;
		String clevertapResponse = null;
		int timeOut = cachedProperties.getIntPropertyValue(CLEVER_TAP_EVENT_TIMEOUT, 60000);
		pushNotificationUrl = cachedProperties.getPropertyValue(END_POINT_PUSH_NOTIFICATION);
		if(MapUtils.isNotEmpty(pushNotificationDetails)) {
			mapper = new ObjectMapper();
			mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
			mapper.setSerializationInclusion(Include.NON_EMPTY);
			addCredentials();
			try
			{
				requestJson = mapper.writeValueAsString(pushNotificationDetails);
			}
			catch(Exception e){
				logger.error("Exception In JSON Processing:" ,e);
			}
			try {
				response = RestUtil.post(pushNotificationUrl, requestJson, null, "application/json", requestHeaders, null, "POST",
						timeOut);
				clevertapResponse = response.getMessage();
				return clevertapResponse;

			} catch (Exception e) {
				logger.error("Exception in getResponseFromCleverTrap");
			}
		}
		
		return null;
	}

	public boolean validatePushNotificationPayLoad(Map<String, Object> pushNotificationDetails,
			List<String> pushNotificationErrorMessages, Map<String, Object> pushEventResponseMap) {
		if (MapUtils.isEmpty(pushNotificationDetails)) {
			if (MapUtils.isNotEmpty(pushEventResponseMap)) {
				pushNotificationErrorMessages.add((String) pushEventResponseMap.get("invPayload"));
			} else {
				pushNotificationErrorMessages.add("Either invalid data was sent or no data was sent at all.");
			}
			return false;
		}
		@SuppressWarnings("unchecked")
		Map<String, Object> identityDetails = (Map<String, Object>) pushNotificationDetails.get("to");
		if (MapUtils.isEmpty(identityDetails)) {
			if (MapUtils.isNotEmpty(pushEventResponseMap)) {
				pushNotificationErrorMessages.add((String) pushEventResponseMap.get("invIdentity"));
			} else {
				pushNotificationErrorMessages.add("There is no identity field as part of json which is mandatory");
			}
			return false;
		}
		String tagGroupDetails = (String)pushNotificationDetails.get("tag_group");
		if (StringUtils.isBlank(tagGroupDetails)) {
				if (MapUtils.isNotEmpty(pushEventResponseMap)) {
					pushNotificationErrorMessages.add((String) pushEventResponseMap.get("tagGroup"));
				} else {
					pushNotificationErrorMessages
							.add("There is no tag_group field is there as part of json which is manadatory");
				}
				return false;
		}
		return true;
	}

	public void pushEventDetailsToKafka(Map<String, Object> eventDetailsMap) {
		kafkaProducer.send(APIUtil.serializeObjectToJson(eventDetailsMap).getBytes(),topic);
	}

	public boolean isAsync(){
		return isAsyncEnabled;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean validateEventDetails(Map<String, Object> eventDetailsMap, List<String> errorMessages,
			Map<String, Object> pushEventResponseMap) {

		if (eventDetailsMap == null || eventDetailsMap.isEmpty()) {
			if (pushEventResponseMap != null) {
				errorMessages.add((String) pushEventResponseMap.get("invPayload"));
			} else {
				errorMessages.add("Either invalid data was sent or no data was sent at all.");
			}
			return false;
		}
		Map<String, String> eventData = (Map<String, String>) eventDetailsMap.get(EVENT_DATA);
		String userMail = eventData.get(USER_EMAIL);
		if (StringUtils.isBlank(userMail)) {
			if (pushEventResponseMap != null) {
				errorMessages.add((String) pushEventResponseMap.get("mailFieldEmpty"));
			} else {
				errorMessages.add("Mail id is not their as part of json");
			}
			return false;
		} else {
			if (StringUtils.split(userMail, "@").length != 2) {
				if (pushEventResponseMap != null) {
					errorMessages.add((String) pushEventResponseMap.get("invMail"));
				} else {
					errorMessages.add("Mail Id must not be empty or Inavlid Mail");
				}
				return false;

			}
		}

		String eventName = (String) eventDetailsMap.get(EVENT_NAME);
		if (StringUtils.isBlank(eventName)) {
			if (pushEventResponseMap != null) {
				errorMessages.add((String) pushEventResponseMap.get("invEvntName"));
			} else {
				errorMessages.add("Event Name is Not Sent As Part Of Request JSON");
			}
			return false;
		}
		String eventType = (String) eventDetailsMap.get(EVENT_TYPE);
		if (StringUtils.isBlank(eventType)) {
			if (pushEventResponseMap != null) {
				errorMessages.add((String) pushEventResponseMap.get("blankEventType"));
			} else {
				errorMessages.add("Event type field is blank");
			}
			return false;
		} else {

			if (!StringUtils.isBlank(eventType) && !eventType.equals("event")) {
				if (pushEventResponseMap != null) {
					errorMessages.add((String) pushEventResponseMap.get("invEventType"));
				} else {
					errorMessages.add("Event Type Is not there As Part Of Request JSON");
				}
				return false;
			}
		}
		return true;
	}
}
