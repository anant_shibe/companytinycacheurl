package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.data.PromoServiceInvokerResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.kafka.KafkaProducerWrapper;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.ApiCallStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.ConnectorStats;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.ApiType;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@ClassExcludeCoverage
public class PromoServiceInvoker {

	private KafkaProducerWrapper kafkaProducer;
	private CachedProperties commonCachedProperties;

	private static final Log LOG = LogFactory.getLog(PromoServiceInvoker.class);

	public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
		this.commonCachedProperties = commonCachedProperties;
	}

	public void init() {
		try {
			String brokerList = StringUtils
					.trimToNull(commonCachedProperties.getPropertyValue("ct.services.promo.serivce.kafka.broker.list"));
			if (brokerList != null) {
				// Creating a sync producer
				kafkaProducer = new KafkaProducerWrapper(brokerList, false);
			}
		} catch (Exception exception) {
			LOG.error("+++++++++++ Error During Kafka set up for promo service +++++++++++++ ::"
					+ exception.getMessage());
		}
	}

	public void destroy() {
		if (kafkaProducer != null) {
			kafkaProducer.close();
		}
	}

	/**
	 * 
	 * @param promoDetails
	 *            - JSON payload that ought to pushed to kafka || used to call the
	 *            end-point
	 * @param bookStats
	 * @return {@link PromoServiceInvokerResponse} - POJO, that passes on the
	 *         required info to the calling method. This will be used as the basis
	 *         for logging.
	 */
	public PromoServiceInvokerResponse invokePromoService(String promoDetails) {

		String url = commonCachedProperties.getPropertyValue("ct.services.promo.service.endpoint");

		PromoServiceInvokerResponse response = new PromoServiceInvokerResponse();

		try {
			if (commonCachedProperties.getBooleanPropertyValue("ct.service.promo.invoke.rest.endpoint", false)) {
				response.setKafkaPushSuccessful(false);
				response.setPayLoad(promoDetails);
				response.setPayloadType("application/json");
				response.setUrl(url);
				RestResponse restResponse = RestUtil.post(url, promoDetails, null, "application/json");
				response.setResponse(restResponse);
				LOG.error("Code ::" + restResponse.getCode());
				LOG.error("Rest Response ::" + restResponse.getMessage());

			} else {
				kafkaProducer.send(promoDetails.getBytes(),
						commonCachedProperties.getPropertyValue("ct.service.promo.service.kafka.topic"));
				response.setPayLoad(promoDetails);
				response.setPayloadType("application/json");
				response.setUrl(url);
				response.setKafkaPushSuccessful(true);
			}
		} catch (Exception exception) {
			ApiCallStats apiStats = null;
			ConnectorStats connectorStats = (ConnectorStats) BaseStats.threadLocal();
			if (connectorStats != null) {
				apiStats = connectorStats.addNewApiRequestAndReturn(ApiType.OTHER, null, "Invoke Promo Service",
						"Promotions App", url, promoDetails);
			}
			try {

				response.setKafkaPushSuccessful(false);
				response.setKafkaPushSuccessful(false);
				response.setPayLoad(promoDetails);
				response.setPayloadType("application/json");
				response.setUrl(url);
				// Fallback in case there is something wrong with kafka || The
				// switch to call the end point fails
				RestResponse restResponse = RestUtil.post(
						commonCachedProperties.getPropertyValue("ct.services.promo.service.endpoint"), promoDetails,
						null, "application/json");
				response.setResponse(restResponse);
				if (apiStats != null) {
					connectorStats.closeApiRequest(ApiType.OTHER, "Invoke Promo Service", restResponse.getCode(),
							restResponse.getMessage(), true);
				}
			} catch (IOException e) {
				LOG.error("Error during promo servie invocation");
				RestResponse errorResponse = new RestResponse();
				errorResponse.setCode("500");
				errorResponse.setMessage("Retry failed for Invoking Promo Service ::" + e.getLocalizedMessage());
				if (apiStats != null) {
					connectorStats.closeApiRequest(apiStats, 500, "", false);
				}
			}
		}
		return response;
	}

	public PromoServiceInvokerResponse activatePromotion(String tripRef, String operation, String promoDetails,
			String topic) {
		String url = commonCachedProperties.getPropertyValue("ct.services.promo.service.status.update.endpoint");

		PromoServiceInvokerResponse response = new PromoServiceInvokerResponse();

		try {
			if (commonCachedProperties.getBooleanPropertyValue("ct.service.promo.invoke.rest.endpoint", false)) {
				response.setKafkaPushSuccessful(false);
				// response.setPayLoad(promoDetails);
				// response.setPayloadType("plain/text");
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("x-action", operation);
				url = String.format(url, tripRef);
				response.setUrl(url);
				RestResponse restResponse = RestUtil.post(url, "", headers, null);
				response.setResponse(restResponse);
				LOG.error("Code ::" + restResponse.getCode());
				LOG.error("Rest Response ::" + restResponse.getMessage());

			} else {
				kafkaProducer.send(tripRef.getBytes(), topic);
				response.setPayLoad(tripRef);
				response.setPayloadType("text/plain");
				response.setUrl(url);
				response.setKafkaPushSuccessful(true);
			}
		} catch (Exception exception) {
			LOG.error("Error during pushing to kafka. Making promotion service API call");
			try {
				response.setKafkaPushSuccessful(false);
				response.setKafkaPushSuccessful(false);
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("x-action", operation);
				url = String.format(url, tripRef);
				response.setUrl(url);
				RestResponse restResponse = RestUtil.post(url, "", headers, null);
				response.setResponse(restResponse);
				LOG.error("Code ::" + restResponse.getCode());
				LOG.error("Rest Response ::" + restResponse.getMessage());
			} catch (Exception e) {
				LOG.error("Error during promo servie invocation");
				RestResponse errorResponse = new RestResponse();
				errorResponse.setCode("500");
				errorResponse.setMessage("Retry failed for Invoking Promo Service ::" + e.getLocalizedMessage());
			}

		}
		return response;
	}
}
