package com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * This is a Wrapper over a BlockingQueue for use with a Single Consumer Thread. This is created for pushing click logs to Redis Server by a single consumer Thread in pipeline mode which is supposed
 * to have better performance. The Consumer Thread can optionally quit on an empty Q in which case the first writer to the empty Q should restart the consumer thread.
 * 
 * Note: Only One consumer should be receiving for a SingleConsumerQueue. This can be ensured by using add method to acquire an idle consumer status and the remove method to set back the consumer
 * status to idle and quit the consumer thread in case of an empty Q.
 * 
 * @author suresh
 * 
 * @param <E>
 */
public class SingleConsumerQueue<E> {

    public static enum QueueStatus {
        ACTIVE, IDLE
    };

    // Since there is only one consumer
    private static final int FETCH_SIZE = 256;

    private BlockingQueue<E> q;

    private int lindex;

    private List<E> l;

    // Status of the Consumer: IDLE initially
    private volatile QueueStatus status;

    public SingleConsumerQueue() {
        q = new LinkedBlockingQueue<E>();
        l = new ArrayList<E>(FETCH_SIZE);
        status = QueueStatus.IDLE;
    }

    /**
     * Adds an item to the Q. If this method returns true then the caller should start the consumer thread.
     * 
     * @param e
     *            item to add
     * @param acquireIfIdle
     *            true if an idle consumer status should be acquired by the caller
     * @return true if the Q was idle and the status has been acquired as ACTIVE by the caller in which case the caller should start the consumer thread. if acquireIfIdle is passed as false then
     *         always false is returned.
     */
    public boolean add(E e, boolean acquireIfIdle) {
        boolean acquired = false;
        q.offer(e);
        if (acquireIfIdle && status == QueueStatus.IDLE) {
            synchronized (this) {
                if (status == QueueStatus.IDLE) {
                    status = QueueStatus.ACTIVE;
                    acquired = true;
                }
            }
        }

        return acquired;
    }

    /**
     * Removes an item from Q. If releaseIfEmpty is passed as true and the method returns null, it means the consumer status has been set to IDLE because of an empty Q and that the current consumer
     * thread should quit. A new thread will be started by the caller of the next add method re-acquiring the IDLE status to ACTIVE.
     * 
     * @param timeoutMicros
     *            Max time to wait on an empty Q
     * @param releaseIfEmpty
     *            If true then the status is set to IDLE if the Q is emtpy and null is returned. So the consumer thread has to quit if and only if true is passed and null is returned.
     * @return The item removed or null if the Q is emtpy
     */
    public E remove(long timeoutMicros, boolean releaseIfEmpty) {
        int len = l.size();
        E e = null;
        if (lindex >= len) {
            l.clear();
            q.drainTo(l, FETCH_SIZE);
            len = l.size();
            lindex = 0;
            if (len == 0 && timeoutMicros > 0) {
                try {
                    e = q.poll(timeoutMicros, TimeUnit.MICROSECONDS);
                } catch (InterruptedException ie) {
                }
            }
        }

        if (len > 0) {
            e = l.get(lindex);
            lindex++;
        }

        if (releaseIfEmpty && e == null) {
            status = QueueStatus.IDLE;
            if (q.size() > 0) {
                boolean reAcquired = false;
                synchronized (this) {
                    if (status == QueueStatus.IDLE) {
                        status = QueueStatus.ACTIVE;
                        reAcquired = true;
                    }
                }
                if (reAcquired) {
                    e = remove(0, false);
                    if (e == null) {
                        throw new IllegalStateException("Unexpected empty Q after re-acquiring");
                    }
                }
            }
        }

        return e;
    }
}
