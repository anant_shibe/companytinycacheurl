package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Enumeration;

public class RequestForwardServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -6126011047795196903L;

    private static final int BUF_SIZE = 4096;

    /**
     * {@inheritDoc}
     * 
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        byte[] buf = new byte[BUF_SIZE];

        String baseUrl = getInitParameter("baseUrl");
        if (baseUrl == null) {
            baseUrl = "http://www.cleartrip.com";
        }
        final String userName = getInitParameter("userName");
        final String password = getInitParameter("password");
        if (userName != null && password != null) {
            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userName, password.toCharArray());
                }
            });
        }
        String path = request.getRequestURI();
        if (!path.endsWith("/blank.html")) {
            String queryString = request.getQueryString();
            if (!StringUtils.isBlank(queryString)) {
                queryString = '?' + queryString;
            } else {
                queryString = "";
            }
            URL url = new URL(baseUrl + path + queryString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(request.getMethod());
            int i = 0;
            Enumeration<String> e = request.getHeaderNames();
            while (e.hasMoreElements()) {
                String requestHeaderName = e.nextElement();
                if (!"host".equalsIgnoreCase(requestHeaderName)) {
                    con.setRequestProperty(requestHeaderName, request.getHeader(requestHeaderName));
                }
                i++;
            }
            InputStream reqIp = request.getInputStream();
            String req = GenUtil.readAsStringString(reqIp);
            reqIp.close();
            if (!StringUtils.isEmpty(req)) {
                con.setDoOutput(true);
                Writer writer = new OutputStreamWriter(con.getOutputStream());
                writer.write(req);
                writer.close();
            }
            i = 0;
            String headerKey, headerField;
            int status = con.getResponseCode();
            while ((headerField = con.getHeaderField(i)) != null) {
                headerKey = con.getHeaderFieldKey(i);
                // Do not pass on Transfer-Encoding header as Servlet Container will
                // Decide on Transfer-Encoding
                if (headerKey != null && !headerKey.equalsIgnoreCase("Transfer-Encoding")) {
                    response.setHeader(headerKey, headerField);
                }
                i++;
            }
            InputStream ip = null;
            try {
                ip = con.getInputStream();
            } catch (IOException ioe) {
            }
            if (ip != null) {
                response.setStatus(status);
                OutputStream op = null;
                int len;
                while ((len = ip.read(buf)) > 0) {
                    if (op == null) {
                        op = response.getOutputStream();
                    }
                    op.write(buf, 0, len);
                }
                ip.close();
                if (op != null) {
                    op.close();
                }
            } else {
                response.sendError(status);
                try {
                    ip = con.getErrorStream();
                    ip.close();
                } catch (Exception ioe) {
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
