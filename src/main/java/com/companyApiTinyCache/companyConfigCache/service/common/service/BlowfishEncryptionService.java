package com.companyApiTinyCache.companyConfigCache.service.common.service;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class BlowfishEncryptionService {

    private Cipher cipher;
    private SecretKey secretKey;
    private String EMPTY_STRING = "";
    private final Log logger = LogFactory.getLog(BlowfishEncryptionService.class);

    public BlowfishEncryptionService(String key) {
        try {
            cipher = Cipher.getInstance("Blowfish");
            secretKey = new SecretKey() {
                /**
                 * 
                 */
                private static final long serialVersionUID = 1L;

                @Override
                public String getFormat() {
                    return "RAW";
                }

                @Override
                public byte[] getEncoded() {
                    return Base64.decodeBase64(key.getBytes());
                }

                @Override
                public String getAlgorithm() {
                    return "Blowfish";
                }
            };
        } catch (Exception e) {
            logger.error("Error While Initilizing BlowFish Encripter Class");
        }
    }

    public String encrypt(String inputText) {
        byte[] encrypted = EMPTY_STRING.getBytes();
        try {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            encrypted = cipher.doFinal(inputText.getBytes());
        } catch (Exception e) {
            logger.error("Error while Encripting");
        }
        return new String(Base64.encodeBase64(encrypted));
    }

    public String decrypt(String encrypted) {
        byte[] decrypted = EMPTY_STRING.getBytes();
        byte[] decode = Base64.decodeBase64(encrypted.getBytes());
        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            decrypted = cipher.doFinal(decode);
        } catch (Exception e) {
            System.out.println("Error while Decrypting");
        }
        return new String(decrypted);
    }
}
