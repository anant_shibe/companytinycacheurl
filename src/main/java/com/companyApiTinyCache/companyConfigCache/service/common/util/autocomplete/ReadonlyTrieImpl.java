package com.companyApiTinyCache.companyConfigCache.service.common.util.autocomplete;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/**
 * An readonly trie implementation. Being readonly it is thread safe.
 * 
 * Implementation is using parallel nextStates and nextStateSymbols arrays (i.e same size and indexes) for all state transitions. Negative next state means leaf node and it points to corresponding
 * entity array. A stateOffset array points to the offset in state transition arrays for transitions corresponding to the state.
 * 
 * A quick lookup array for each state of size 26 is included to quickly get next state for lowercase chars since that is expected to be the majority. For others a binary search is done through
 * nextSymbols since it is sorted by symbols.
 * 
 * @author suresh
 * 
 * @param <E>
 */
public class ReadonlyTrieImpl<E> implements TrieBuilder<E>, Serializable {

    private static final long serialVersionUID = 7568218918524142360L;

    private static final Log logger = LogFactory.getLog(ReadonlyTrieImpl.class);

    private class KeyInfo extends IndexInfo {
        private static final long serialVersionUID = -2404533155466060099L;

        private transient List<E> entities;

        public KeyInfo(String pkey, E entity) {
            super(pkey, 0);
            if (entity != null) {
                if (entities == null) {
                    entities = new ArrayList<E>(4);
                }
                entities.add(entity);
            }
        }
    }

    private static class IndexInfo implements Comparable<IndexInfo>, Serializable {
        private static final long serialVersionUID = 2044136434701203130L;
        protected String key;
        protected int numEntities;

        private IndexInfo(String pkey, int pnumEntities) {
            key = pkey;
            numEntities = pnumEntities;
        }

        @Override
        public int compareTo(IndexInfo k1) {
            // TODO Auto-generated method stub
            return key.compareTo(k1.key);
        }
    }

    private class BuildInfo {
        private NavigableSet<IndexInfo> keyInfos = new TreeSet<IndexInfo>();
        private int keyInfoIndex;
        private int numStates = 1;
        private int numNextStates;
        private int stateForKeyPrefixes[];
        private String prevKey, key;
        private int numKeys;
        private File keyFile;
        private ObjectInputStream keyIp;
        private boolean addCalled;
        private String keyArr[]; // Used after add0 calls are made if 2 pass add is used
        private int entityIndexArr[]; // Points to entityEnd Index for each key in keyArr (for 2 pass add)
        private int currentEntityIndex = 1;
        private String errorMsg;
        public IndexInfo[] keys;
    }

    private class TrieIteratorImpl implements TrieIterator<E> {
        private StringBuilder sb;

        private String matchedKey;

        private String prefix;

        private int nextStateIndexStk[];

        private int nextStateEndIndexStk[];

        private int stkPtr;

        private int nextStkPtr;

        private int prevNextStkPtr;

        private int numEntities;

        private int prevEntityIndex;

        private E entityArr[];

        private boolean exactMatch;

        public TrieIteratorImpl(String pprefix) {
            int i, state = 0;
            prefix = pprefix;
            prevEntityIndex = 1;
            entityArr = (E[]) Array.newInstance(entityType, 2);
            int nextStateIndex = -1, prevStateIndex = -1;
            int prefixLen = pprefix.length();
            for (i = 0; i < prefixLen;) {
                nextStateIndex = getNextStateIndex(state, pprefix.charAt(i));
                if (nextStateIndex < 0) {
                    break;
                } else if (nextStateIndex > stateOffsets[state]) {
                    prevStateIndex = nextStateIndex - 1;
                }
                state = nextStates[nextStateIndex];
                i++;
                if (state <= 0) {
                    break;
                }
            }
            if (i >= prefixLen && nextStates.length > 0) {
                int stackSize = 1;
                if (state >= 0) {
                    stackSize = stateDepths[state];
                    if (stackSize != -1) {
                        stackSize = (stackSize & 0xFF) + 2;
                    } else {
                        stackSize = longestKeyLen - prefixLen + 1;
                    }
                }
                nextStateIndexStk = new int[stackSize];
                nextStateEndIndexStk = new int[stackSize];
                nextStkPtr = 1;
                nextStateIndexStk[0] = nextStateIndex;
                nextStateEndIndexStk[0] = nextStateIndex + 1;
                if (state <= 0 || nextStateSymbols[stateOffsets[state]] == 0) {
                    exactMatch = true;
                }
                if (prevStateIndex >= 0) {
                    while ((prevEntityIndex = nextStates[prevStateIndex]) > 0) {
                        prevStateIndex = stateOffsets[prevEntityIndex + 1] - 1;
                    }
                    prevEntityIndex = -prevEntityIndex;
                }
            }
        }

        @Override
        public boolean hasNext() {
            return nextStkPtr > 0;
        }

        @Override
        public E[] next() {
            if (nextStkPtr == 0) {
                throw new NoSuchElementException("No Next Element");
            }
            prevNextStkPtr = 2;
            int newStkPtr = 0;
            if (sb != null) {
                matchedKey = null;
                int newSbLen = prefix.length() + nextStkPtr - 2;
                if (newSbLen < sb.length()) {
                    sb.setLength(newSbLen);
                }
            }
            int newNextStkPtr = 0;
            newStkPtr = nextStkPtr - 1;
            if (stkPtr > 0) {
                prevNextStkPtr = nextStkPtr;
                nextStateIndexStk[newStkPtr]++;
                exactMatch = false;
            }
            int nextStateIndex = nextStateIndexStk[newStkPtr];
            int nextStateEndIndex = nextStateEndIndexStk[newStkPtr];
            int state = 0;
            do {
                newStkPtr++;
                if (nextStateIndex + 1 < nextStateEndIndex) {
                    newNextStkPtr = newStkPtr;
                }
                if (nextStateIndex >= 0) {
                    state = nextStates[nextStateIndex];
                }
                if (state >= 0) {
                    nextStateIndex = stateOffsets[state];
                    nextStateEndIndex = stateOffsets[state + 1];
                    nextStateIndexStk[newStkPtr] = nextStateIndex;
                    nextStateEndIndexStk[newStkPtr] = nextStateEndIndex;
                }
            } while (state >= 0);
            if (newNextStkPtr == 0) {
                newNextStkPtr = nextStkPtr - 1;
                while (newNextStkPtr > 0) {
                    newNextStkPtr--;
                    nextStateIndex = nextStateIndexStk[newNextStkPtr];
                    nextStateEndIndex = nextStateEndIndexStk[newNextStkPtr];
                    if (nextStateIndex + 1 < nextStateEndIndex) {
                        newNextStkPtr++;
                        break;
                    }
                }
            }
            nextStkPtr = newNextStkPtr;
            int entityIndex = -nextStates[nextStateIndexStk[newStkPtr - 1]];
            int len = entityIndex - prevEntityIndex;
            if (entityArr.length <= len) {
                entityArr = Arrays.copyOf(entityArr, len + 1);
            }
            entityArr[len] = null;
            System.arraycopy(entities, prevEntityIndex, entityArr, 0, len);
            numEntities = len;
            prevEntityIndex = entityIndex;
            stkPtr = newStkPtr;

            return entityArr;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Iterator is Read only");
        }

        @Override
        public boolean isMatchesPrefix() {
            return exactMatch;
        }

        @Override
        public String getMatchedKey() {
            if (stkPtr == 0) {
                throw new IllegalStateException("Cannot call getMatchedKey without calling next()");
            }
            if (matchedKey == null) {
                if (sb == null) {
                    int stkLen = nextStateIndexStk.length;
                    if (stkLen > 256) {
                        stkLen = 256;
                    }
                    sb = new StringBuilder(prefix.length() + stkLen);
                    sb.append(prefix);
                }
                int stkIndex = sb.length() - prefix.length() + 1;
                while (stkIndex < stkPtr) {
                    int nextStateIndex = nextStateIndexStk[stkIndex];
                    char ch = nextStateSymbols[nextStateIndex];
                    if (ch != 0) {
                        sb.append(ch);
                    }
                    stkIndex++;
                }
                matchedKey = sb.toString();
            }

            return matchedKey;
        }

        @Override
        public int getPrevCommonPrefixLen() {
            return prefix.length() + prevNextStkPtr - 2;
        }

        @Override
        public int getNextCommonPrefixLen() {
            return prefix.length() + nextStkPtr - 2;
        }

        @Override
        public int getNumEntities(int prefixLen) {
            int iteratorPrefixLen = prefix.length();
            int stkIndex = prefixLen - iteratorPrefixLen;
            if (stkIndex < 0) {
                throw new IllegalArgumentException("PrefixLen: " + prefixLen + " should not be less than iterator prefix len " + iteratorPrefixLen);
            }
            if (stkIndex >= stkPtr) {
                throw new IllegalArgumentException("PrefixLen: " + prefixLen + " should not be greator than current keyLen " + (iteratorPrefixLen + stkPtr));
            }
            int numKeys = numEntities;
            int state = 0;
            int nextStateIndex = nextStateIndexStk[stkIndex];
            if (nextStateIndex >= 0) {
                state = nextStates[nextStateIndex];
            }
            if (state >= 0) {
                numKeys = stateNumKeys[state];
                if (numKeys != -1) {
                    numKeys &= 0xFFFF;
                } else {
                    numKeys = 0xFFFF;
                }
            }

            return numKeys;
        }

        @Override
        public int getNumEntities() {
            return numEntities;
        }

    }

    // This is an Array of size 127 and contains quick lookup Index for Ascii characters included for this via
    // createQuickLookupInfo({<chars>}) call. To check if a character can be looked up 1st check if it is
    // between 0-127, then check if charToQuickLookupIndex[<char>] >= 0 in which case that index can be used
    // with quickSymbolLookups array after applying offset for the current state. Constructor defaults this
    // to Lowercase alphabets a-z
    int charToQuickLookupIndex[];

    // Number of quickLookup characters (26 by default corresponding to a-z)
    int numQuickLookups;

    // Using Arrays Rather than Classes to save space on Object references which should be 8 bytes a reference

    // One Entry for each state. Points to starting Offset in nextStateSymbols/nextStates for Next States.
    // Length can be determined by subtracting from next entry.
    private int stateOffsets[];

    // numQuickLookups * states entries. Contains the Offset of the Child relative to State start for
    // Symbol got by charToQuickLookupIndex[<char>] if val >= 0 and <char> between 0-127
    private byte quickSymbolLookups[];

    // Length of longest suffix String from this state - 1. Used to determine stack size for suffix iterator
    // Treated as unsigned byte. -1 indicates the longest suffix is longer than 255 chars in which case
    // a stack of length longestKeyLen - prefixLen is allocated for iterator
    private byte stateDepths[];

    // Num Keys under each state interpretted as unsigned.
    private short stateNumKeys[];

    // Contains characters for Next State in sorted order. This can be used to binary search characters not covered by
    // quickLookup and non-ascii characters. It is also used to iterate over prefix matches in sorted order.
    // If 0 is the 1st char it means that the prev state is a final state. This is used to cover Keys which is a prefix
    // of another key and so requires a next state after a final state
    private char nextStateSymbols[];

    // Parallel array to nextStateSymbols. Contains next States corresponding to symbols. If <= 0 it means that there is
    // no next state (Next is actually a Final/Leaf State) and the value indicates the offset of the corresponding
    // Entities from entities array
    private int nextStates[];

    // Entities corresponding to keys. Zero index is not used because nextState is negative of entity index.
    // The negative nextStates point to this array. Since a key can have multiple entities the begin index
    // should be found by looking at previous key
    private E entities[];

    private Class<E> entityType;

    private int longestKeyLen;

    // Temp info for building Trie. Will be set to null after build call
    private BuildInfo buildInfo;

    private void createQuickLookupInfo(char includeChars[]) {
        if (includeChars.length > 0) {
            charToQuickLookupIndex = new int[127];
            int i;
            for (i = 0; i < charToQuickLookupIndex.length; i++) {
                charToQuickLookupIndex[i] = -1;
            }
            numQuickLookups = includeChars.length;
            for (i = 0; i < includeChars.length; i++) {
                charToQuickLookupIndex[includeChars[i]] = i;
            }
        }
    }

    public ReadonlyTrieImpl(Class<E> pentityType) {
        entityType = pentityType;
        // Not using below quick lookup since it uses up 26 bytes per state
        // createQuickLookupInfo(new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'});
        stateOffsets = new int[0];
        quickSymbolLookups = new byte[0];
        nextStateSymbols = new char[0];
        nextStates = new int[0];
        entities = (E[]) Array.newInstance(entityType, 0);
        buildInfo = new BuildInfo();
    }

    // Separate method has been added in the hope that GC kicks in to free up buildInfo.keyInfos before
    // saveKeys is called
    private void checkAndProcessAdd0() {
        NavigableSet<IndexInfo> indexInfos = buildInfo.keyInfos;
        String[] keyArr;
        int i, len;
        if ((len = indexInfos.size()) > 0) {
            // Means add0 calls have been made for 2 phase add
            // Create keyArr and entityIndexArr and save keys
            keyArr = new String[len];
            int entityIndexArr[] = new int[len];
            i = 0;
            for (IndexInfo indexInfo : indexInfos) {
                keyArr[i] = indexInfo.key;
                entityIndexArr[i] = indexInfo.numEntities;
                i++;
            }
            buildInfo.keyArr = keyArr;
            buildInfo.entityIndexArr = entityIndexArr;
            indexInfos = null;
            buildInfo.keyInfos = null;
        }
    }

    @Override
    public void add(String key, E entity) {
        int i, len;
        if (buildInfo == null) {
            throw new IllegalStateException("Cannot add after Build is complete");
        }
        String[] keyArr;
        if (!buildInfo.addCalled) {
            buildInfo.addCalled = true;
            checkAndProcessAdd0();
            int entityIndexArr[] = buildInfo.entityIndexArr;
            if (entityIndexArr != null) {
                // add0 has been called, so save Keys and allocate entities
                saveKeys();
                int entityIndex = 1;
                for (i = 0; i < entityIndexArr.length; i++) {
                    int numEntities = entityIndexArr[i];
                    entityIndexArr[i] = entityIndex;
                    entityIndex += numEntities;
                }
                entities = (E[]) Array.newInstance(entityType, entityIndex); // One extra since 0 is not used
            }
        }
        keyArr = buildInfo.keyArr;
        if (keyArr != null) {
            // Means add0 has been called
            i = Arrays.binarySearch(keyArr, key);
            if (i < 0) {
                throw new IllegalStateException("Key " + key + " was not called with add0 call");
            }
            entities[buildInfo.entityIndexArr[i]++] = entity;
        } else {
            NavigableSet<IndexInfo> indexInfos = buildInfo.keyInfos;
            KeyInfo keyInfo = new KeyInfo(key, entity);
            KeyInfo keyInfo1 = (KeyInfo) indexInfos.ceiling(keyInfo);
            if (keyInfo1 != null && keyInfo1.compareTo(keyInfo) == 0) {
                keyInfo1.entities.add(entity);
                keyInfo = keyInfo1;
            } else {
                indexInfos.add(keyInfo);
            }
        }
        len = key.length();
        if (len > longestKeyLen) {
            longestKeyLen = len;
        }
    }

    /**
     * Additional method for lesser memory utilization for many duplicate keys and hence many entities. After 1 cycle of add0 calls, exactly the same calls should be made for add, though the order
     * could be different. This is outside the TrieBuilder interface. The add0 calls are used to collect the unique keys and the entity count for each keys. The entities are stored in the add calls.
     * 
     * @param key
     * @param entity
     */
    public void add0(String key, E entity) {
        if (buildInfo == null) {
            throw new IllegalStateException("Cannot call add0 after Build is complete");
        }
        if (buildInfo.addCalled) {
            throw new IllegalStateException("Cannot call add0 after add is called");
        }
        NavigableSet<IndexInfo> indexInfos = buildInfo.keyInfos;
        IndexInfo indexInfo = new IndexInfo(key, 0);
        IndexInfo indexInfo1 = indexInfos.ceiling(indexInfo);
        if (indexInfo1 != null && indexInfo1.compareTo(indexInfo) == 0) {
            indexInfo = indexInfo1;
        } else {
            indexInfos.add(indexInfo);
        }
        indexInfo.numEntities++;
    }

    public static int getCommonPrefixLen(String s1, String s2) {
        int commonPrefixLen = 0;
        if (s1 != null && s2 != null) {
            int len1 = s1.length();
            int len2 = s2.length();
            int i, len = len1;
            if (len2 < len) {
                len = len2;
            }
            for (i = 0; i < len; i++) {
                if (s1.charAt(i) != s2.charAt(i)) {
                    break;
                }
                commonPrefixLen++;
            }
        }

        return commonPrefixLen;
    }

    // For computing Array lengths
    private void processNextKeyPass1(IndexInfo indexInfo) {
        String key = indexInfo.key;
        int prevKeyLen = 0, keyLen = key.length();
        int i, commonPrefixLen = 0;
        String prevKey = buildInfo.prevKey;
        if (prevKey != null) {
            prevKeyLen = prevKey.length();
            commonPrefixLen = getCommonPrefixLen(prevKey, key);
        }
        int newNextStates = keyLen - commonPrefixLen;
        buildInfo.numNextStates += newNextStates;
        buildInfo.numStates += newNextStates - 1;
        if (prevKey != null && commonPrefixLen == prevKeyLen) {
            buildInfo.numStates++;
            buildInfo.numNextStates++;
        }
        buildInfo.currentEntityIndex += indexInfo.numEntities;
        int[] entityIndexArr = buildInfo.entityIndexArr;
        if (entityIndexArr != null && buildInfo.errorMsg == null) {
            i = buildInfo.keyInfoIndex - 1;
            if (entityIndexArr[i] != buildInfo.currentEntityIndex) {
                buildInfo.errorMsg = "For Key: " + key + "; Got " + indexInfo.numEntities + " Entities in add0 but got " + (entityIndexArr[i] - (buildInfo.currentEntityIndex - indexInfo.numEntities))
                        + " in add";
            }
        }
    }

    // For computing State Offsets. First store the number of next entries for each state in stateOffsets
    // which will be converted to offsets
    private void processNextKeyPass2(String key) {
        int i, parentState, newState, prevKeyLen = 0, keyLen = key.length();
        int commonPrefixLen = 0;
        String prevKey = buildInfo.prevKey;
        if (prevKey != null) {
            prevKeyLen = prevKey.length();
            commonPrefixLen = getCommonPrefixLen(prevKey, key);
        }
        int[] stateForKeyPrefixes = buildInfo.stateForKeyPrefixes;
        if (prevKey != null && commonPrefixLen == prevKeyLen) {
            if (commonPrefixLen > 0) {
                newState = buildInfo.numStates++;
                stateForKeyPrefixes[commonPrefixLen - 1] = newState;
                stateOffsets[newState]++;
            }
        }
        parentState = 0;
        if (commonPrefixLen > 0) {
            parentState = stateForKeyPrefixes[commonPrefixLen - 1];
        }
        for (i = commonPrefixLen; i < keyLen - 1; i++) {
            stateOffsets[parentState]++;
            newState = buildInfo.numStates++;
            stateForKeyPrefixes[i] = newState;
            parentState = newState;
        }
        stateOffsets[parentState]++;
    }

    private void processNextKeyPass3(int entityIndex, String key) {
        int i, parentState, prevStateIndex, newState, prevKeyLen = 0, keyLen = key.length(), nextStateIndex;
        int commonPrefixLen = 0;
        String prevKey = buildInfo.prevKey;
        if (prevKey != null) {
            prevKeyLen = prevKey.length();
            commonPrefixLen = getCommonPrefixLen(prevKey, key);
        }
        int[] stateForKeyPrefixes = buildInfo.stateForKeyPrefixes;
        if (prevKey != null && commonPrefixLen == prevKeyLen) {
            if (commonPrefixLen > 0) {
                newState = buildInfo.numStates++;
                stateForKeyPrefixes[commonPrefixLen - 1] = newState;
                nextStateIndex = stateOffsets[newState + 1]++;
                parentState = 0;
                if (commonPrefixLen > 1) {
                    parentState = stateForKeyPrefixes[commonPrefixLen - 2];
                }
                prevStateIndex = stateOffsets[parentState + 1] - 1;
                int prevEntityIndex = nextStates[prevStateIndex];
                nextStates[prevStateIndex] = newState;
                nextStateSymbols[nextStateIndex] = 0;
                nextStates[nextStateIndex] = prevEntityIndex;
            }
        }
        parentState = 0;
        if (commonPrefixLen > 0) {
            parentState = stateForKeyPrefixes[commonPrefixLen - 1];
        }
        char ch = 0;
        for (i = commonPrefixLen; i < keyLen - 1; i++) {
            newState = buildInfo.numStates++;
            stateForKeyPrefixes[i] = newState;
            prevStateIndex = stateOffsets[parentState + 1]++;
            nextStateSymbols[prevStateIndex] = key.charAt(i);
            nextStates[prevStateIndex] = newState;
            parentState = newState;
        }
        ch = 0;
        if (keyLen > 0) {
            ch = key.charAt(keyLen - 1);
        }
        prevStateIndex = stateOffsets[parentState + 1]++;
        nextStateSymbols[prevStateIndex] = ch;
        nextStates[prevStateIndex] = -entityIndex;
    }

    // Converts the lengths from pass2 to offsets. Offset for state 1 is 0, 2 is that of 1 and so on. This is so
    // that they can be used as current positions for adding next entries in final step after which they will
    // be offsets for state given by index as required
    private void generateStateOffsetsFromLength() {
        int i, total;
        total = 0;
        for (i = 0; i < stateOffsets.length; i++) {
            total += stateOffsets[i];
        }
        for (i = stateOffsets.length - 1; i >= 1; i--) {
            total -= stateOffsets[i - 1];
            stateOffsets[i] = total;
        }
        stateOffsets[0] = 0;
    }

    private void fillQuickLookups(int state) {
        if (charToQuickLookupIndex != null) {
            byte ofs = 0;
            int startIndex = stateOffsets[state];
            int endIndex = stateOffsets[state + 1];
            while (startIndex < endIndex) {
                char ch = nextStateSymbols[startIndex];
                if (ch > 0 && ch <= 127) {
                    int lookupIndex = charToQuickLookupIndex[ch];
                    if (lookupIndex >= 0) {
                        lookupIndex += numQuickLookups * state;
                        quickSymbolLookups[lookupIndex] = ofs;
                    }

                }
                int nextState = nextStates[startIndex];
                if (nextState > 0) {
                    fillQuickLookups(nextState);
                }
                startIndex++;
                ofs++;
            }
        }
    }

    // If there is not enough memory for keys and Trie this could serialize and save buildInfo.keyInfos to a file
    // and set it to null, in whch case nextKey should be modified to read from file
    private void saveKeys() {
        NavigableSet<IndexInfo> indexInfos = buildInfo.keyInfos;
        String[] keyArr = buildInfo.keyArr;
        int[] entityIndexArr = buildInfo.entityIndexArr;
        ObjectOutputStream oop = null;
        int i;
        if (indexInfos != null) {
            buildInfo.numKeys = indexInfos.size();
        } else {
            // add0 has been called
            buildInfo.numKeys = keyArr.length;
        }
        try {
            buildInfo.keyFile = File.createTempFile("ct_hotel_suggester_keys", ".ser");
            oop = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(buildInfo.keyFile)));
            if (indexInfos != null) {
                IndexInfo indexInfo;
                while ((indexInfo = indexInfos.pollFirst()) != null) {
                    oop.writeObject(new IndexInfo(indexInfo.key, indexInfo.numEntities));
                }
            } else {
                // add0 has been called
                for (i = 0; i < keyArr.length; i++) {
                    oop.writeObject(new IndexInfo(keyArr[i], entityIndexArr[i]));
                }
            }
        } catch (Exception e) {
            logger.error("Error Saving Keys to file " + buildInfo.keyFile.getPath() + ", defaulting to ArrayList", e);
            IndexInfo keys[] = new IndexInfo[buildInfo.numKeys];
            buildInfo.keys = keys;
            i = 0;
            if (indexInfos != null) {
                for (IndexInfo keyInfo : indexInfos) {
                    keys[i] = new IndexInfo(keyInfo.key, keyInfo.numEntities);
                    i++;
                }
            } else {
                for (; i < keyArr.length; i++) {
                    keys[i] = new IndexInfo(keyArr[i], entityIndexArr[i]);
                }
            }
        } finally {
            try {
                if (oop != null) {
                    oop.close();
                }
            } catch (Exception e) {
            }
        }
        buildInfo.keyInfos = null;
    }

    private void removeSavedKeys() {
        buildInfo.keyFile.delete();
    }

    // Gets nextKey from memory/file. Returns null if end has been reached and starts from beginning on
    // next call. Should ensure prevKey contains prevKey
    private IndexInfo nextKey() {
        IndexInfo indexInfo = null;
        buildInfo.prevKey = buildInfo.key;
        IndexInfo[] keys = buildInfo.keys;
        if (keys != null) {
            if (buildInfo.keyInfoIndex < keys.length) {
                indexInfo = keys[buildInfo.keyInfoIndex];
                buildInfo.keyInfoIndex++;
            } else {
                buildInfo.keyInfoIndex = 0;
            }
        } else {
            try {
                if (buildInfo.keyInfoIndex == 0) {
                    buildInfo.keyIp = new ObjectInputStream(new BufferedInputStream(new FileInputStream(buildInfo.keyFile)));
                }
                if (buildInfo.keyInfoIndex < buildInfo.numKeys) {
                    indexInfo = (IndexInfo) buildInfo.keyIp.readObject();
                    buildInfo.keyInfoIndex++;
                } else {
                    buildInfo.keyInfoIndex = 0;
                    buildInfo.keyIp.close();
                }
            } catch (Exception e) {
                logger.error("Error reading from Temp File " + buildInfo.keyFile.getPath(), e);
            }
        }
        if (indexInfo != null) {
            buildInfo.key = indexInfo.key;
        } else {
            buildInfo.key = null;
        }

        return indexInfo;
    }

    private int getAndStoreStateDepth(int state) {
        int startIndex = stateOffsets[state];
        int endIndex = stateOffsets[state + 1];
        int depth = -1;
        while (startIndex < endIndex) {
            int nextState = nextStates[startIndex];
            if (nextState > 0) {
                int childDepth = getAndStoreStateDepth(nextState);
                if (childDepth > depth) {
                    depth = childDepth;
                }
            }
            startIndex++;
        }
        depth++;
        if (depth > 0xFE) {
            stateDepths[state] = -1;
        } else {
            stateDepths[state] = (byte) depth;
        }

        return depth;
    }

    private int getAndStoreNumKeys(int state) {
        int startIndex = stateOffsets[state];
        int endIndex = stateOffsets[state + 1];
        int numKeys = 0;
        while (startIndex < endIndex) {
            int nextState = nextStates[startIndex];
            if (nextState > 0) {
                numKeys += getAndStoreNumKeys(nextState);
            } else {
                int nextEntintyIndex = -nextState;
                numKeys += (nextEntintyIndex - buildInfo.currentEntityIndex);
                buildInfo.currentEntityIndex = nextEntintyIndex;
            }
            startIndex++;
        }
        if (numKeys > 0xFFFE) {
            stateNumKeys[state] = -1;
        } else {
            stateNumKeys[state] = (short) numKeys;
        }

        return numKeys;
    }

    private int getNextStateIndex(int currentState, char ch) {
        int nextStateIndex = -1;
        if (currentState + 1 < stateOffsets.length) {
            int startIndex = stateOffsets[currentState];
            int endIndex = stateOffsets[currentState + 1];
            int lookupIndex = -1;
            if (charToQuickLookupIndex != null && ch <= 127) {
                lookupIndex = charToQuickLookupIndex[ch];
                if (lookupIndex >= 0) {
                    lookupIndex += numQuickLookups * currentState;
                    int offsetFromStartIndex = quickSymbolLookups[lookupIndex];
                    if (offsetFromStartIndex >= 0) {
                        nextStateIndex = startIndex + offsetFromStartIndex;
                    }
                }
            }
            if (lookupIndex < 0) {
                nextStateIndex = Arrays.binarySearch(nextStateSymbols, startIndex, endIndex, ch);
            }
        }
        return nextStateIndex;
    }

    private void allocEntityArrayAndSaveKeys() {
        NavigableSet<IndexInfo> keyInfos = buildInfo.keyInfos;

        // This step is required only if add0 has not been called
        if (buildInfo.entityIndexArr == null) {
            int i, len = 0;
            for (IndexInfo indexInfo : keyInfos) {
                for (E entity : ((KeyInfo) indexInfo).entities) {
                    len++;
                }
            }
            // Transfer Entities to Entity Array
            entities = (E[]) Array.newInstance(entityType, len + 1); // One extra since 0 is not used
            i = 0;
            for (IndexInfo indexInfo : keyInfos) {
                KeyInfo keyInfo = (KeyInfo) indexInfo;
                for (E entity : keyInfo.entities) {
                    i++;
                    entities[i] = entity;
                }
                indexInfo.numEntities = keyInfo.entities.size();
                keyInfo.entities = null;
            }

            saveKeys();
            keyInfos = null;
        }
    }

    @Override
    public void build() {
        if (buildInfo == null) {
            throw new IllegalStateException("Cannot build after Build is complete");
        }
        if (!buildInfo.addCalled && buildInfo.keyInfos.size() > 0) {
            throw new IllegalStateException("Build called after calling add0 without calling add");
        }
        IndexInfo indexInfo;
        allocEntityArrayAndSaveKeys();
        buildInfo.keyArr = null;
        int i;

        // Do a dry run to compute Array sizes
        while ((indexInfo = nextKey()) != null) {
            processNextKeyPass1(indexInfo);
        }

        if (buildInfo.errorMsg != null) {
            throw new IllegalStateException(buildInfo.errorMsg);
        }
        buildInfo.entityIndexArr = null;

        if (buildInfo.numStates < 1) {
            buildInfo.numStates = 1;
            buildInfo.numNextStates = 1;
        }
        // Allocate Arrays and initialize
        stateOffsets = new int[buildInfo.numStates + 1]; // One Extra to get length of last state from next offset
        stateDepths = new byte[buildInfo.numStates];
        stateNumKeys = new short[buildInfo.numStates];
        quickSymbolLookups = new byte[numQuickLookups * buildInfo.numStates];
        for (i = 0; i < quickSymbolLookups.length; i++) {
            quickSymbolLookups[i] = -1;
        }
        nextStateSymbols = new char[buildInfo.numNextStates];
        nextStates = new int[buildInfo.numNextStates];
        buildInfo.stateForKeyPrefixes = new int[longestKeyLen];
        buildInfo.numStates = 1;
        buildInfo.prevKey = null;

        // Do a dry run to Fill in State lengths
        while ((indexInfo = nextKey()) != null) {
            processNextKeyPass2(indexInfo.key);
        }

        generateStateOffsetsFromLength();

        buildInfo.numStates = 1;
        buildInfo.prevKey = null;
        i = 1;
        // Fill in Trie
        while ((indexInfo = nextKey()) != null) {
            i += indexInfo.numEntities;
            processNextKeyPass3(i, indexInfo.key);
        }

        removeSavedKeys();

        fillQuickLookups(0);
        getAndStoreStateDepth(0);
        buildInfo.currentEntityIndex = 1;
        getAndStoreNumKeys(0);

        buildInfo = null;
    }

    @Override
    public E[] checkKey(String key) {
        E entityArr[] = null;
        int i, nextStateIndex = -1, prevStateIndex = -1;
        int keyLen = key.length();
        int state = 0;
        for (i = 0; i < keyLen;) {
            nextStateIndex = getNextStateIndex(state, key.charAt(i));
            if (nextStateIndex < 0) {
                break;
            } else {
                if (nextStateIndex > stateOffsets[state]) {
                    prevStateIndex = nextStateIndex - 1;
                }
            }
            state = nextStates[nextStateIndex];
            i++;
            if (state <= 0) {
                break;
            }
        }
        if (i >= keyLen && nextStates.length > 0) {
            int entityIndex = state;
            if (state >= 0) {
                nextStateIndex = stateOffsets[state];
                if (nextStateSymbols[nextStateIndex] == 0) {
                    entityIndex = nextStates[nextStateIndex];
                }
            }
            if (entityIndex < 0) {
                entityIndex = -entityIndex;
                int prevEntityIndex = 1;
                if (prevStateIndex >= 0) {
                    while ((prevEntityIndex = nextStates[prevStateIndex]) > 0) {
                        prevStateIndex = stateOffsets[prevEntityIndex + 1] - 1;
                    }
                    prevEntityIndex = -prevEntityIndex;
                }
                entityArr = (E[]) Array.newInstance(entityType, entityIndex - prevEntityIndex);
                System.arraycopy(entities, prevEntityIndex, entityArr, 0, entityArr.length);
            }
        }

        return entityArr;
    }

    @Override
    public int getNumEntitiesWithPrefixMatch(String key) {
        int i, nextStateIndex = -1, numKeys = 0;
        int keyLen = key.length();
        int prevStateIndex = -1, state = 0;
        for (i = 0; i < keyLen;) {
            nextStateIndex = getNextStateIndex(state, key.charAt(i));
            if (nextStateIndex < 0) {
                break;
            }
            if (nextStateIndex > stateOffsets[state]) {
                prevStateIndex = nextStateIndex - 1;
            }
            state = nextStates[nextStateIndex];
            i++;
            if (state <= 0) {
                break;
            }
        }
        if (i >= keyLen && nextStates.length > 0) {
            numKeys = -1;
            if (state >= 0) {
                numKeys = stateNumKeys[state];
                if (numKeys != -1) {
                    numKeys &= 0xFFFF;
                } else {
                    numKeys = 0xFFFF;
                }
                if (numKeys > 0 && nextStateSymbols[stateOffsets[state]] == 0) {
                    // Is also an exact prefix return -numKeys to indicate that
                    numKeys = -numKeys;
                }
            } else {
                // Since we track only the end of the end of the entityIndex some jazz to traverse and find
                // out prevEntityIndex to get numEntities
                int prevEntityIndex = 1;
                if (prevStateIndex >= 0) {
                    while ((prevEntityIndex = nextStates[prevStateIndex]) > 0) {
                        prevStateIndex = stateOffsets[prevEntityIndex + 1] - 1;
                    }
                    prevEntityIndex = -prevEntityIndex;
                }
                numKeys = -state - prevEntityIndex;
                if (numKeys > 0xFFFE) {
                    numKeys &= 0xFFFF; // Limit val to 2 byte or short as with other states
                }
                numKeys = -numKeys; // Make negative to indicate exact match
            }
        }

        return numKeys;
    }

    @Override
    public int getNumEntities(String prefix) {
        int numKeys = getNumEntitiesWithPrefixMatch(prefix);
        if (numKeys < 0) {
            numKeys = -numKeys;
        }

        return numKeys;
    }

    @Override
    public TrieIterator<E> iterator(String prefix) {
        return new TrieIteratorImpl(prefix);
    }

    public int getNumQuickLookups() {
        return numQuickLookups;
    }

    public int getNumStates() {
        return stateOffsets.length;
    }

    public int getNumNextStates() {
        return nextStates.length;
    }

    public int getNumEntities() {
        return entities.length;
    }

    public int getLongestKeyLen() {
        return longestKeyLen;
    }

}
