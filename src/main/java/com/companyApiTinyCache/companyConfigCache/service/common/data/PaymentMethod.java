package com.companyApiTinyCache.companyConfigCache.service.common.data;

/**
 *
 * @author amith
 *
 */
public enum PaymentMethod {
    CHEQUE("CHEQUE", "Cheque/DD"), CASH("CASH", "Cash"), ETRANSFER("ETRANSFER", "NEFT"), FUNDSTRANSFER("FUNDS_TFR", "Funds transfer"),
 CREDITNOTE("CREDITNOTE", "Credit note"), RTGS("RTGS", "RTGS"), WIRESTRANSFER(
            "WIRE_TFR", "Wire transfer"), CREDITCARD("CREDITCARD", "Credit Card"), VCC("VCC", "Virtual Credit Card"), PAH("PAH", "Pay At Hotel");

    private String code;

    private String label;

    private PaymentMethod(String pcode, String plabel) {
        code = pcode;
        label = plabel;
    }

    public final String getCode() {
        return code;
    }

    public final String getLabel() {
        return label;
    }

    public static final PaymentMethod fromChar(String value) {
        if (value == null) {
            return null;
        }
        PaymentMethod[] statuses = PaymentMethod.values();
        for (PaymentMethod status : statuses) {
            if (value.equalsIgnoreCase(status.getCode())) {
                return status;
            }
        }
        return null;
    }
}
