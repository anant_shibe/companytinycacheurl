package com.companyApiTinyCache.companyConfigCache.service.common.cache;

/**
 * Exception thrown if an item could not be stored. Note that this class is a subclass of CacheException; most client code can just catch CacheException for general cache error handling: this
 * exception is intended for clients that has to explicitly differentiate between different kinds of cache errors.
 * 
 * @author sdesai
 */
public class CacheStoreException extends CacheException {
    private static final long serialVersionUID = 2830621303124776598L;

    /**
     * Constructs a new exception with <code>null</code> as its detail message. The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
     */
    public CacheStoreException() {
    }

    /**
     * Constructs a new exception with the specified detail message. The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause}.
     * 
     * @param message
     *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
     */
    public CacheStoreException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     * <p>
     * Note that the detail message associated with <code>cause</code> is <i>not</i> automatically incorporated in this exception's detail message.
     * 
     * @param message
     *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     * @param cause
     *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.)
     * @since 1.4
     */
    public CacheStoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
