package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.util.Pool;

import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class JedisCacheFactory implements DisposableBean {

    private static final Log logger = LogFactory.getLog(JedisCacheFactory.class);

    private CachedProperties commonCachedProperties;

    private static Map<String, Pool<Jedis>> cachePoolMap = new ConcurrentHashMap<>();
    private JedisPoolConfig poolConfig;

    public JedisCacheFactory() {
        poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(40); // Arguably High // 20
        poolConfig.setMinIdle(5);// Arguably High // 5
        poolConfig.setMaxIdle(5); // 5
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
    }

    public JedisCache getCache(String serverProperty) {
        Pool<Jedis> pool = null;
        String serverPropertyVal = commonCachedProperties.getPropertyValue(serverProperty);
        pool = cachePoolMap.computeIfAbsent(serverPropertyVal, s -> createJedisPool(serverPropertyVal));
        return new JedisCacheImpl((Jedis) pool.getResource());
    }

    public JedisSentinelPool getSentinelPool(String serverProperty) throws Exception {
        Pool<Jedis> pool = null;
        String serverPropertyVal = commonCachedProperties.getPropertyValue(serverProperty);

        if (isSentinelConfig(serverPropertyVal)) {
            pool = cachePoolMap.computeIfAbsent(serverPropertyVal, s -> createJedisPool(serverPropertyVal));
        } else {
            throw new Exception("Requested Sentinel Pool from Non Sentinel Resource");
        }

        return (JedisSentinelPool) pool;
    }

    public JedisPool getJedisPool(String serverProperty){
        String value = commonCachedProperties.getPropertyValue(serverProperty);
        String redis[] = value.split(":");
        String host = redis[0];
        int port = Integer.parseInt(redis[1]);
        JedisPool jedisPool = new JedisPool(poolConfig, host, port);
        logger.info("Created Jedis Pool for Host: " + host);
        return jedisPool;
    }

    public boolean isSentinelConfig(String serverPropertyVal) {
        return serverPropertyVal.contains("@");
    }

    private Pool<Jedis> createJedisPool(String serverPropertyVal) {
        // String serverPropertyVal = commonCachedProperties.getPropertyValue(serverProperty);
        if (isSentinelConfig(serverPropertyVal)) {
            String[] connectionDetails = serverPropertyVal.split("@");
            String masterName = connectionDetails[0];
            String sentinelsString = connectionDetails[1];
            String[] vals = sentinelsString.split("\\|");
            HashSet<String> sentinels = new HashSet<String>();
            for (String sentinel : vals) {
                sentinels.add(sentinel);
            }
            return new JedisSentinelPool(masterName, sentinels, poolConfig);
        } else {
            String[] vals = serverPropertyVal.split(":");
            return new JedisPool(poolConfig, vals[0], Integer.parseInt(vals[1]));
        }

    }

    /**
     * Setter for commonCachedProperties.
     *
     * @param pcommonCachedProperties
     *            the commonCachedProperties to set
     */
    public void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
    }

    @Override
    public void destroy() throws Exception {
        for (String serversProperty : cachePoolMap.keySet()) {
            Pool<Jedis> jedis = cachePoolMap.get(serversProperty);
            jedis.destroy();
        }
    }

    public void releaseJedisPool(String serverProperty, JedisCache cache) {
        if (cache != null) {
            String serverPropertyVal = commonCachedProperties.getPropertyValue(serverProperty);
            if (cachePoolMap.containsKey(serverPropertyVal)) {
                Pool<Jedis> pool = cachePoolMap.get(serverPropertyVal);
                pool.returnResource(cache.getJedis());
            }
        }
    }
}
