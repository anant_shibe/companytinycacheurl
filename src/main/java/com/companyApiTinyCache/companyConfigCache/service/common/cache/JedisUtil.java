package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisDataException;

public class JedisUtil {

    public static Object evalScript(Jedis jedis, RedisScript script, int keyCount, String... params) {
        Object result = null;
        try {
            result = jedis.evalsha(script.getSha1sum(), keyCount, params);
        } catch (JedisDataException e) {
            String msg = e.getMessage();
            if (msg != null && msg.indexOf("NOSCRIPT") >= 0) {
                result = jedis.eval(script.getScript(), keyCount, params);
            } else {
                throw e;
            }
        }
        return result;
    }
}
