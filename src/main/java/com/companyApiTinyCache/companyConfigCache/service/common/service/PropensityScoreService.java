package com.companyApiTinyCache.companyConfigCache.service.common.service;


public interface PropensityScoreService {
	
	/**
	 * Returns user score depending on user activities. Data platform team analyzes past user 
	 * activity and returns the score accordingly
	 * @param uid
	 * @return User score
	 */
	int getScore(String uid);
	

}
