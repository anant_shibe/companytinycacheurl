/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * @author deepa
 */
public class Address implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private int sequenceNumber;

    private String addressType;

    private String streetAddress;

    private String city;

    private String state;

    private String country;

    private int cityId;

    private int stateId;

    private int countryId;

    private String pincode;

    @JsonProperty("pincode")
    public String getPincode() {
        return pincode;
    }

    @JsonProperty("pincode")
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @JsonProperty("category")
    public String getAddressType() {
        return addressType;
    }

    @JsonProperty("category")
    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    @JsonProperty("street_address")
    public String getStreetAddress() {
        return streetAddress;
    }

    @JsonProperty("street_address")
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("city_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getCityId() {
        return cityId;
    }

    @JsonProperty("city_id")
    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @JsonProperty("state_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getStateId() {
        return stateId;
    }

    @JsonProperty("state_id")
    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    @JsonProperty("country_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getCountryId() {
        return countryId;
    }

    @JsonProperty("country_id")
    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    @JsonProperty("seq_no")
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    @JsonProperty("seq_no")
    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Address == false)
            return false;
        Address add = (Address) obj;

        if ((addressType != null || add.getAddressType() != null) && !GenUtil.equals(addressType, add.getAddressType())) {
            return false;
        }
        if ((streetAddress != null || add.getStreetAddress() != null) && !GenUtil.equals(streetAddress, add.getStreetAddress())) {
            return false;
        }
        if ((city != null || add.getCity() != null) && !GenUtil.equals(city, add.getCity())) {
            return false;
        }
        if ((state != null || add.getState() != null) && !GenUtil.equals(state, add.getState())) {
            return false;
        }
        if ((country != null || add.getCountry() != null) && !GenUtil.equals(country, add.getCountry())) {
            return false;
            // }if(cityId != add.getCityId()){
            // return false;
            // }if(stateId != add.getStateId()){
            // return false;
            // }if(countryId != add.getCountryId()){
            // return false;
        }
        if ((pincode != null || add.getPincode() != null) && !GenUtil.equals(pincode, add.getPincode())) {
            return false;
        }

        return true;
    }

}
