package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

public class InspectBeanController extends AbstractController {

    private String defaultBeanName;

    public InspectBeanController() {
        setCacheSeconds(0);
    }

    private ApplicationContext getContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.mvc.AbstractController#handleRequestInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String beanName = request.getParameter("beanName");
        if (beanName == null) {
            beanName = defaultBeanName;
        }
        OutputStream op = new BufferedOutputStream(response.getOutputStream());
        if (beanName != null) {
            ApplicationContext ctx = getContext();
            Object bean = ctx.getBean(beanName);
            boolean isCompress = false;
            String encodings = request.getHeader("Accept-Encoding");
            if (encodings != null && encodings.indexOf("gzip") != -1) {
                isCompress = true;
            }
            if (isCompress) {
                // Go with GZIP
                response.setHeader("Content-Encoding", "gzip");
                op = new GZIPOutputStream(op);
            }
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            JsonFactory jsonFactory = new JsonFactory();
            JsonGenerator jgen = jsonFactory.createGenerator(op, JsonEncoding.UTF8);
            jgen.useDefaultPrettyPrinter();
            mapper.writeValue(jgen, bean);
        } else {
            op.write("Please Specify defaultBeanName in Controller or Pass beanName= in URL param to indicate the bean\n".getBytes());
        }
        op.close();
        return null;
    }

    public void setDefaultBeanName(String pdefaultBeanName) {
        defaultBeanName = pdefaultBeanName;
    }

}
