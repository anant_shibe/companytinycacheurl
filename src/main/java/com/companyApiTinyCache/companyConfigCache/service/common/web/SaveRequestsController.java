package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.zip.GZIPInputStream;

/**
 * Saves the generated Request Data to a file. The request data generated is actually present in the /common/requests
 * page as a javascript variable as a Base64 encoding of the Gzipped data. The data is posted to this controller with
 * the file to save to via an Ajax call and this controller simply extracts the data and saves it.
 *
 * @author suresh
 */
@ClassExcludeCoverage
public class SaveRequestsController extends AbstractController {

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
                                                 HttpServletResponse response) throws Exception {
        String msg = null;
        String file = StringUtils.trimToNull(request.getParameter("file"));
        String data = StringUtils.trimToNull(request.getParameter("data"));
        if (file == null) {
            msg = "Missing parameter file";
        } else if (data == null) {
            msg = "Missing parameter data";
        } else {
            OutputStream op = null;
            try {
                File f = new File(file);
                File parent = f.getParentFile();
                if (parent.exists() || parent.mkdirs()) {
                    op = new FileOutputStream(f);
                    byte [] buf = new byte[4096];
                    byte [] dataBytes = Base64.decodeBase64(data.getBytes());
                    GZIPInputStream gzipIp = new GZIPInputStream(new ByteArrayInputStream(dataBytes));
                    int len;
                    while ((len = gzipIp.read(buf)) > 0) {
                        op.write(buf, 0, len);
                    }
                } else {
                    msg = "Unable to Create Parent Directory " + parent;
                }
            } catch (Exception e) {
                msg = "Error Saving to " + file + ": " + e.getMessage();
            } finally {
                if (op != null) {
                    op.close();
                }
            }
        }
        boolean success = msg == null;
        Writer writer = response.getWriter();
        writer.write("{\"success\":\"" + String.valueOf(success) + '"');
        if (msg != null) {
            writer.write(", \"msg\":\"" + msg + '"');
        }
        writer.write('}');
        return null;
    }

}
