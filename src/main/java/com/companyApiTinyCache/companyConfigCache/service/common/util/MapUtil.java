package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

public class MapUtil<K, V> {
    private Collection<Map<K, V>> maps = new HashSet<Map<K, V>>();

    private Collection<K> commonKeys = new HashSet<K>();

    private Map<K, AtomicInteger> allKeys = new HashMap<K, AtomicInteger>();

    public void addMap(Map<K, V> map) {
        maps.add(map);
        if (maps.size() == 1) {
            commonKeys.addAll(map.keySet());
        } else {
            commonKeys.retainAll(map.keySet());
        }
        for (Entry<K, V> entry : map.entrySet()) {
            AtomicInteger integer = allKeys.get(entry.getKey());
            if (integer == null) {
                integer = new AtomicInteger(0);
                allKeys.put(entry.getKey(), integer);
            }
            integer.addAndGet(1);
        }
    }

    public Collection<K> getCommonKeys() {
        return commonKeys;
    }

    public Map<K, AtomicInteger> getAllKeys() {
        return allKeys;
    }

    public void retainCommonKeys() {
        for (Map<K, V> map : maps) {
            Collection<K> keys = new HashSet<K>(map.keySet());
            for (K key : keys) {
                if (!commonKeys.contains(key)) {
                    allKeys.remove(key);
                    map.remove(key);
                }
            }
        }
    }

    public void removeCommonKeys() {
        for (K key : commonKeys) {
            allKeys.remove(key);
            for (Map<K, V> map : maps) {
                map.remove(key);
            }
        }
        commonKeys.clear();
    }

    public void putToAll(K key, V value) {
        for (Map<K, V> map : maps) {
            map.put(key, value);
        }
        commonKeys.add(key);
        allKeys.put(key, new AtomicInteger(maps.size()));
    }
}
