package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;

public class InsuredPerson implements Serializable {
    private static final long serialVersionUID = 1L;

    private String title;
    private String firstName;
    private String lastName;
    private double premium;
    private double tax;
    private double totalPremium;

    /**
     * @return the premium
     */
    public double getPremium() {
        return premium;
    }

    /**
     * @param premium
     *            the premium to set
     */
    public void setPremium(double premium) {
        this.premium = premium;
    }

    /**
     * @return the tax
     */
    public double getTax() {
        return tax;
    }

    /**
     * @param tax
     *            the tax to set
     */
    public void setTax(double tax) {
        this.tax = tax;
    }

    /**
     * @return the totalPremium
     */
    public double getTotalPremium() {
        return totalPremium;
    }

    /**
     * @param totalPremium
     *            the totalPremium to set
     */
    public void setTotalPremium(double totalPremium) {
        this.totalPremium = totalPremium;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
