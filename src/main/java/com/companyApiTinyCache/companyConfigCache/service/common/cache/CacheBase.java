package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * ABC for operations common to all concrete Cache implementations.
 *
 * @author twilsher
 */
public abstract class CacheBase implements Cache {
    private static final double MICROSEC_PER_MS = 1000.0;
    private static final int MEMCACHED_MAX_KEY_LEN = 250;
    /** Class logger. */
    private static final Log logger = LogFactory.getLog(CacheBase.class);

    public static final String DEFAULT_SERVERS_PROPERTY = "ct.services.memcached.primaryservers";

    protected final String recordDelimiter = "<>";
    protected final String filedDimiter = "::";

    protected String connectionString;

    protected boolean encodeKeyRequired = true;

    protected static final String OK = "OK";
    /**
     * Constructor.
     */
    protected CacheBase() {
    }

    public String getConnectionString() {
        return connectionString;
    }

    /**
     * Get the canonical String representation of this key.
     *
     * @param key
     *            the key to Stringify
     * @return String representation of the key.
     */
    protected String getKeyAsString(Object key) {
        final String ret;
        try {
            String keyStr = String.valueOf(key).trim();

            if (this.encodeKeyRequired) {
                ret = URLEncoder.encode(keyStr, "UTF-8");
            } else {
                ret = keyStr;
            }
        } catch (UnsupportedEncodingException e) {
            // Shouldn't happen -- UTF-8 should always be available. Propagate
            // as RuntimeException
            throw new RuntimeException("URLEncoding failed", e);
        }

        if (ret.length() >= MEMCACHED_MAX_KEY_LEN) {
            throw new IllegalArgumentException("Key too long: " + ret);
        }
        return ret;
    }

    @Override
    public void returnNativeConnection(String key, Object con) {
    }

    /**
     * check integrity of passed-in arguments.
     *
     * @param map
     *            key-value map.
     */
    protected void checkPutParams(Map map) {
        if (map == null) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * check integrity of passed-in arguments.
     *
     * @param unusedId
     *            key.
     * @param obj
     *            value.
     */
    protected final void checkPutArgs(Object unusedId, Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("NULL object specified to put()");
        }
    }

    /**
     * check integrity of passed-in arguments.
     *
     * @param ids
     *            keys.
     * @param objs
     *            values.
     */
    final void checkPutArgs(String [] ids, List<Object> objs) {
        if (ids == null) {
            throw new IllegalArgumentException("NULL id array specified to put()");
        }
        if (objs == null) {
            throw new IllegalArgumentException("NULL object array specified to put()");
        }
        final int listLen = objs.size();
        if (ids.length != listLen) {
            throw new IllegalArgumentException("length of object ids and object list does not match, " + ids.length + " != " + listLen);
        }
    }

    /**
     * check integrity of passed-in arguments.
     *
     * @param ids
     *            keys.
     */
    protected final void checkGetArgs(String [] ids) {
        if (ids == null) {
            throw new IllegalArgumentException("NULL id array specified to get()");
        }
    }

    @Override
    public Object get(String key) throws CacheRetrieveException {
        return get(key, GenUtil.UTF_8_CHARSET);
    }

    @Override
    public Map<String, Object> getMulti(String[] ids) {
        return getMulti(ids, GenUtil.UTF_8_CHARSET);
    }

    @Override
    public List<Object> get(String [] ids, Charset charset) throws CacheRetrieveException {
        Map<String, Object> valueMap = getMulti((String[]) ids, charset);
        int len = 0;
        if (valueMap != null) {
            len = valueMap.size();
        }
        List<Object> values = new ArrayList<Object>(len);
        if (len > 0) {
            for (Object value : valueMap.values()) {
                values.add(value);
            }
        }

        return values;
    }

    @Override
    public List<Object> get(String [] ids) throws CacheRetrieveException {
        return get(ids, GenUtil.UTF_8_CHARSET);
    }

    @Override
    public boolean put(String id, Object obj) throws CacheStoreException {
        return put(id, obj, 0);
    }

    /**
     * Multi put API.
     *
     * {@inheritDoc}
     */
    public void put(String [] ids, List<Object> objs) throws CacheStoreException {
        checkPutArgs(ids, objs);
        for (int i = 0; i < ids.length; i++) {
            put(ids[i], objs.get(i));
        }
    }

    @Override
    public boolean put(String key, Object val, Date dateOfExpiry) throws CacheStoreException {
        int expirySeconds = 0;
        if (dateOfExpiry != null) {
            expirySeconds = (int) ((dateOfExpiry.getTime() - System.currentTimeMillis() + 500) / 1000);
        }
        boolean status = put(key, val, expirySeconds);

        return status;
    }

    @Override
    public boolean atomicUpdate(String id, int expirySeconds, ValueUpdater updater, int maxTries) {
        throw new UnsupportedOperationException("Atomic Update Not Supported");
    }

    @Override
    public boolean atomicUpdate(List<String> idList, int expirySeconds, List<ValueUpdater> updaterList, int maxTries) {
        throw new UnsupportedOperationException("Atomic Update Not Supported");
    }

    /**
     * Have to have two prototypes for the putAll method due to some type weirdness.
     *
     * @param map
     *            key-values to insert.
     * @throws CacheStoreException
     *             if kes could not be set
     */
    protected void doPutAll(Map<String, ? extends Object> map) throws CacheStoreException {
        checkPutParams(map);
        for (Map.Entry<String, ? extends Object> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * {@inheritDoc}
     *
     */
    public void putAll(Map<String, ? extends Object> map) throws CacheStoreException {
        doPutAll(map);
    }

    @Override
    public boolean syncPut(String key, Object val, int expirySeconds) throws CacheStoreException {
        return put(key, val, expirySeconds);
    }

    @Override
    public boolean syncRemove(String key) {
        return remove(key);
    }

    @Override
    public boolean syncPut(String key, Object val, Date dateOfExpiry) throws CacheStoreException {
        return put(key, val, dateOfExpiry);
    }

    @Override
    public boolean setExpiry(String key, int expirySeconds) {
        boolean success = false;
        Object val = get(key);
        if (val != null) {
            success = put(key, val, expirySeconds);
        }

        return success;
    }

    /**
     * check integrity of passed-in arguments.
     *
     * @param id
     *            key.
     */
    protected final void checkGetArgs(Object id) {
        // no-op for now
    }

    /**
     * {@inheritDoc}
     *
     * @see com.cleartrip.common.cache.Cache#destroy()
     */
    @Override
    public void destroy() throws Exception {
    }

    /**
     * {@inheritDoc}
     *
     * @see com.cleartrip.common.cache.Cache#lock(Object, int, boolean)
     */
    @Override
    public LockStatus lock(String key, int expirySeconds, boolean verifyFailedLock) {
        LockStatus status = LockStatus.UNKNOWN;
        try {
            if (add(key, "1", expirySeconds)) {
                status = LockStatus.SUCCESS;
            } else if (verifyFailedLock) {
                Object val = get(key);
                if (val != null) {
                    status = LockStatus.FAILED;
                }
            } else {
                status = LockStatus.FAILED;
            }
        } catch (Exception e) {
            status = LockStatus.UNKNOWN;
        }

        return status;
    }

    public boolean isEncodeKeyRequired() {
        return encodeKeyRequired;
    }

    public void setEncodeKeyRequired(boolean encodeKeyRequired) {
        this.encodeKeyRequired = encodeKeyRequired;
    }

    @Override
    public long getServerTime() {
        return 0;
    }

    @Override
    public boolean putMulti(Map<String, ? extends Object> map, int expirySeconds) throws CacheStoreException {
    	return putMulti(map, expirySeconds, GenUtil.UTF_8_CHARSET);
    }

    // /**
    // * Returns the number of key-value mappings in this map. If the
    // * map contains more than <tt>Integer.MAX_VALUE</tt> elements, returns
    // * <tt>Integer.MAX_VALUE</tt>.
    // *
    // * @return the number of key-value mappings in this map.
    // */
    // public int size() {
    // throw new UnsupportedOperationException("Map.size");
    // }
    //
    // /**
    // * Returns <tt>true</tt> if this map contains no key-value mappings.
    // *
    // * @return <tt>true</tt> if this map contains no key-value mappings.
    // */
    // public boolean isEmpty() {
    // throw new UnsupportedOperationException("Map.isEmpty");
    // }
    //
    // /**
    // * Returns <tt>true</tt> if this map contains a mapping for the specified
    // * key. More formally, returns <tt>true</tt> if and only if
    // * this map contains a mapping for a key <tt>k</tt> such that
    // * <tt>(key==null ? k==null : key.equals(k))</tt>. (There can be
    // * at most one such mapping.)
    // *
    // * @param key key whose presence in this map is to be tested.
    // * @return <tt>true</tt> if this map contains a mapping for the specified
    // * key.
    // * @throws ClassCastException if the key is of an inappropriate type for
    // * this map (optional).
    // * @throws NullPointerException if the key is <tt>null</tt> and this map
    // * does not permit <tt>null</tt> keys (optional).
    // */
    // public boolean containsKey(Object key) {
    // throw new UnsupportedOperationException("Map.containsKey");
    // }
    //
    // /**
    // * Returns <tt>true</tt> if this map maps one or more keys to the
    // * specified value. More formally, returns <tt>true</tt> if and only if
    // * this map contains at least one mapping to a value <tt>v</tt> such that
    // * <tt>(value==null ? v==null : value.equals(v))</tt>. This operation
    // * will probably require time linear in the map size for most
    // * implementations of the <tt>Map</tt> interface.
    // *
    // * @param value value whose presence in this map is to be tested.
    // * @return <tt>true</tt> if this map maps one or more keys to the
    // * specified value.
    // * @throws ClassCastException if the value is of an inappropriate type for
    // * this map (optional).
    // * @throws NullPointerException if the value is <tt>null</tt> and this map
    // * does not permit <tt>null</tt> values (optional).
    // */
    // public boolean containsValue(Object value) {
    // throw new UnsupportedOperationException("Map.containsValue");
    // }
    //
    //
    // /**
    // * Removes all mappings from this map (optional operation).
    // *
    // * @throws UnsupportedOperationException clear is not supported by this
    // * map.
    // */
    // public void clear() {
    // throw new UnsupportedOperationException("Map.clear");
    // }
    //
    // /**
    // * Returns a set view of the keys contained in this map. The set is
    // * backed by the map, so changes to the map are reflected in the set, and
    // * vice-versa. If the map is modified while an iteration over the set is
    // * in progress (except through the iterator's own <tt>remove</tt>
    // * operation), the results of the iteration are undefined. The set
    // * supports element removal, which removes the corresponding mapping from
    // * the map, via the <tt>Iterator.remove</tt>, <tt>Set.remove</tt>,
    // * <tt>removeAll</tt> <tt>retainAll</tt>, and <tt>clear</tt> operations.
    // * It does not support the add or <tt>addAll</tt> operations.
    // *
    // * @return a set view of the keys contained in this map.
    // */
    // public Set<K> keySet() {
    // throw new UnsupportedOperationException("Map.keySet");
    // }
    //
    // /**
    // * Returns a collection view of the values contained in this map. The
    // * collection is backed by the map, so changes to the map are reflected in
    // * the collection, and vice-versa. If the map is modified while an
    // * iteration over the collection is in progress (except through the
    // * iterator's own <tt>remove</tt> operation), the results of the
    // * iteration are undefined. The collection supports element removal,
    // * which removes the corresponding mapping from the map, via the
    // * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>,
    // * <tt>removeAll</tt>, <tt>retainAll</tt> and <tt>clear</tt> operations.
    // * It does not support the add or <tt>addAll</tt> operations.
    // *
    // * @return a collection view of the values contained in this map.
    // */
    // @SuppressWarnings("unchecked")
    // public Collection<V> values() {
    // throw new UnsupportedOperationException("Map.values");
    // }
    //
    // /**
    // * Returns a set view of the mappings contained in this map. Each element
    // * in the returned set is a {@link java.util.Map.Entry}. The set is backed by the
    // * map, so changes to the map are reflected in the set, and vice-versa.
    // * If the map is modified while an iteration over the set is in progress
    // * (except through the iterator's own <tt>remove</tt> operation, or through
    // * the <tt>setValue</tt> operation on a map entry returned by the iterator)
    // * the results of the iteration are undefined. The set supports element
    // * removal, which removes the corresponding mapping from the map, via the
    // * <tt>Iterator.remove</tt>, <tt>Set.remove</tt>, <tt>removeAll</tt>,
    // * <tt>retainAll</tt> and <tt>clear</tt> operations. It does not support
    // * the <tt>add</tt> or <tt>addAll</tt> operations.
    // *
    // * @return a set view of the mappings contained in this map.
    // */
    // @SuppressWarnings("unchecked")
    // public Set<Map.Entry<K, V>> entrySet() {
    // throw new UnsupportedOperationException("Map.entrySet");
    // }
}
