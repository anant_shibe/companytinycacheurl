package com.companyApiTinyCache.companyConfigCache.service.common.web.admin.cache;

import com.companyApiTinyCache.companyConfigCache.service.common.util.tinycache.TinyCache;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class FlushCacheController extends AbstractController {

    public FlushCacheController() {
        setCacheSeconds(0);
    }

    private ApplicationContext getContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String beanName = request.getParameter("beanName");
        ApplicationContext ctx = getContext();
        TinyCache tinyCache = (TinyCache) ctx.getBean(beanName);
        tinyCache.flush();

        PrintWriter op = response.getWriter();
        op.println("Cache Flushed Successfully.");
        op.close();
        return null;
    }

}
