package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.*;
import java.util.Map.Entry;

/**
 *
 */
public class CompanyDetail extends AffiliateInfo {

    @JsonIgnore
    private static final long serialVersionUID = 4420749216395138917L;

    private Integer id;

    private Integer companyId;

    private String companyName;

    private String companyWebsite;

    private String statusId;

    private int inviteId;

    private int importId;

    private String companyDesignation;

    /**
     * -999 is the department that indicates that a person does not belong to any company. To delete a person from a Department too, we will set it to -999 and then do a User-Update The -999 value is
     * decided by the Accounts API.
     */
    private int departmentId;

    private String department;

    private double marketingSubscription;

    private Date effectiveFrom;

    private Date effectiveTo;

    private Map<Integer, Role> roles;

    private String workEmailId;

    private String workPhoneNumber;

    @JsonIgnore
    private boolean accountOwner;

    private String employeeId;
    
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("company_id")
    public Integer getCompanyId() {
        return companyId;
    }

    @JsonProperty("company_id")
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @JsonProperty("status")
    public String getStatusId() {
        return statusId;
    }

    @JsonProperty("status")
    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    @JsonProperty("invite_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getInviteId() {
        return inviteId;
    }

    @JsonProperty("invite_id")
    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }

    @JsonProperty("import_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getImportId() {
        return importId;
    }

    @JsonProperty("import_id")
    public void setImportId(int importId) {
        this.importId = importId;
    }

    @JsonProperty("company_designation")
    public String getCompanyDesignation() {
        return companyDesignation;
    }

    @JsonProperty("company_designation")
    public void setCompanyDesignation(String companyDesignation) {
        this.companyDesignation = companyDesignation;
    }

    @JsonProperty("department_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getDepartmentId() {
        return departmentId;
    }

    @JsonProperty("department_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    @JsonProperty("marketing_subscription")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public double getMarketingSubscription() {
        return marketingSubscription;
    }

    @JsonProperty("marketing_subscription")
    public void setMarketingSubscription(double marketingSubscription) {
        this.marketingSubscription = marketingSubscription;
    }

    @JsonProperty("effective_from")
    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    @JsonProperty("effective_from")
    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    @JsonProperty("effective_to")
    public Date getEffectiveTo() {
        return effectiveTo;
    }

    @JsonProperty("effective_to")
    public void setEffectiveTo(Date effectiveTo) {
        this.effectiveTo = effectiveTo;
    }

    @JsonProperty("company_people_roles")
    public List<Role> getRolesList() {
        List<Role> rolesList = null;
        if (this.roles != null) {
            rolesList = new ArrayList<Role>();
            for (Entry<Integer, Role> entry : roles.entrySet()) {
                rolesList.add(entry.getValue());
            }
        }
        return rolesList;
    }

    @JsonProperty("company_people_roles")
    public void setRolesFromList(List<Role> roles) {
        this.roles = new LinkedHashMap<Integer, Role>();
        for (Role role : roles) {
            this.roles.put(role.getId(), role);
        }
    }

    @JsonIgnore
    public Map<Integer, Role> getRoles() {
        return roles;
    }

    @JsonIgnore
    public void setRoles(Map<Integer, Role> roles) {
        this.roles = roles;
    }

    @JsonIgnore
    public boolean isAccountOwner() {
        return accountOwner;
    }

    @JsonIgnore
    public void setAccountOwner(boolean accountOwner) {
        this.accountOwner = accountOwner;
    }

    @JsonProperty("company_name")
    public String getCompanyName() {
        return companyName;
    }

    @JsonProperty("company_name")
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @JsonProperty("company_website")
    public String getCompanyWebsite() {
        return companyWebsite;
    }

    @JsonProperty("company_website")
    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    @JsonProperty("department")
    public String getDepartment() {
        return department;
    }

    @JsonProperty("department")
    public void setDepartment(String department) {
        this.department = department;
    }

    @JsonProperty("employee_id")
    public String getEmployeeId() {
        return employeeId;
    }

    @JsonProperty("employee_id")
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @JsonProperty("work_email")
    public String getWorkEmailId() {
        return workEmailId;
    }

    @JsonProperty("work_email")
    public void setWorkEmailId(String workEmailId) {
        this.workEmailId = workEmailId;
    }

    @JsonProperty("work_phone_number")
    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    @JsonProperty("work_phone_number")
    public void setWorkPhoneNumber(String workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (accountOwner ? 1231 : 1237);
        result = prime * result + ((companyDesignation == null) ? 0 : companyDesignation.hashCode());
        result = prime * result + ((companyId == null) ? 0 : companyId.hashCode());
        result = prime * result + departmentId;
        result = prime * result + ((effectiveFrom == null) ? 0 : effectiveFrom.hashCode());
        result = prime * result + ((effectiveTo == null) ? 0 : effectiveTo.hashCode());
        result = prime * result + importId;
        result = prime * result + inviteId;
        long temp;
        temp = Double.doubleToLongBits(marketingSubscription);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((roles == null) ? 0 : roles.hashCode());
        result = prime * result + ((statusId == null) ? 0 : statusId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CompanyDetail other = (CompanyDetail) obj;
        if (accountOwner != other.accountOwner) {
            return false;
        }
        if (companyDesignation == null) {
            if (other.companyDesignation != null) {
                return false;
            }
        } else if (!companyDesignation.equals(other.companyDesignation)) {
            return false;
        }
        if (companyId == null) {
            if (other.companyId != null) {
                return false;
            }
        } else if (!companyId.equals(other.companyId)) {
            return false;
        }
        if (departmentId != other.departmentId) {
            return false;
        }
        if (effectiveFrom == null) {
            if (other.effectiveFrom != null) {
                return false;
            }
        } else if (!effectiveFrom.equals(other.effectiveFrom)) {
            return false;
        }
        if (effectiveTo == null) {
            if (other.effectiveTo != null) {
                return false;
            }
        } else if (!effectiveTo.equals(other.effectiveTo)) {
            return false;
        }
        if (importId != other.importId) {
            return false;
        }
        if (inviteId != other.inviteId) {
            return false;
        }
        if (Double.doubleToLongBits(marketingSubscription) != Double.doubleToLongBits(other.marketingSubscription)) {
            return false;
        }
        if (roles == null) {
            if (other.roles != null) {
                return false;
            }
        } else if (!roles.equals(other.roles)) {
            return false;
        }
        if (statusId == null) {
            if (other.statusId != null) {
                return false;
            }
        } else if (!statusId.equals(other.statusId)) {
            return false;
        }
        if ((companyName != null || other.getCompanyName() != null) && !GenUtil.equals(companyName, other.getCompanyName())) {
            return false;
        }
        if ((companyWebsite != null || other.getCompanyWebsite() != null) && !GenUtil.equals(companyWebsite, other.getCompanyWebsite())) {
            return false;
        }
        if ((companyDesignation != null || other.getCompanyDesignation() != null) && !GenUtil.equals(companyDesignation, other.getCompanyDesignation())) {
            return false;
        }
        if ((department != null || other.getDepartment() != null) && !GenUtil.equals(department, other.getDepartment())) {
            return false;
        }
        return true;
    }

}
