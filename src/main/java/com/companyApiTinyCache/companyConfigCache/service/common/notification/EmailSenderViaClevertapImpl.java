package com.companyApiTinyCache.companyConfigCache.service.common.notification;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.RestUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CleverTapEnumConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;

/**
 * @author parvatkumar
 *
 */
@ClassExcludeCoverage
public class EmailSenderViaClevertapImpl implements EmailSenderViaClevertap, CleverTapEnumConstants {
	
	private static final Log logger = LogFactory.getLog(EmailSenderViaClevertapImpl.class);

	private static final String CLEVER_TAP_SEND_MAIL_ENDPOINT = "ct.air.clevertap.send.mail.endpoint";

	private static final String CLEVER_TAP_ACCOUNT_ID_VAL = "ct.clevertap.acc.id";

	private static final String CLEVER_TAP_ACCOUNT_PASSCODE_VAL = "ct.common.clevertap.passcode";
	
	private static final String CLEVER_TAP_MAIL_SENDER_NAME = "ct.air.clevertap.mail.sender.name";
	
	private static final String TIMEOUT_FOR_CLEVER_TAP_MAIL = "ct.air.timeout.clevertap.mail";
	
	private static final String RESPECT_FREQUENCY_CAPS_CLEVERTAP = "ct.air.respect.frequency.caps.enable";
   
	private CachedProperties cachcedProperties;

	private final Map<String, String> requestHeaders = new HashMap<String, String>();

	public void setCachcedProperties(CachedProperties cachcedProperties) {
		this.cachcedProperties = cachcedProperties;
	}

	public void addClevertapCredentials() {
		requestHeaders.put(ACCOUNT_ID, cachcedProperties.getPropertyValue(CLEVER_TAP_ACCOUNT_ID_VAL));
		requestHeaders.put(PASS_CODE, cachcedProperties.getPropertyValue(CLEVER_TAP_ACCOUNT_PASSCODE_VAL));
		requestHeaders.put(CONTENT_TYPE, MEDIATYPE_JSON);
	}

	@Override
	public void sendEmail(Map<String, Object> payLoad) {
		String url;
		String senderName;
		int timeout;
		String payloadJsonForMail;
		if(MapUtils.isNotEmpty(payLoad)) {
			url = cachcedProperties.getPropertyValue(CLEVER_TAP_SEND_MAIL_ENDPOINT);
			senderName = cachcedProperties.getPropertyValue(CLEVER_TAP_MAIL_SENDER_NAME);
			timeout = cachcedProperties.getIntPropertyValue(TIMEOUT_FOR_CLEVER_TAP_MAIL, 1000);
			Map<String, Object> actualPayload = buildPayloadForMail(payLoad, senderName);
			addClevertapCredentials();
		    try {
				payloadJsonForMail = JsonUtil.toJson(actualPayload);
				logger.error(payloadJsonForMail);
				RestUtil.post(url, payloadJsonForMail, null, MEDIATYPE_JSON, requestHeaders, null, "POST", timeout);
			} catch (Exception e) {
			   logger.error("Exception occured while sending mail with payload ",e);
			}
			
		}

	}
	
	

	
	public Map<String, Object> buildPayloadForMail(Map<String, Object> payLoad, String senderName) {
		Map<String, Object> requestPayload = new LinkedHashMap<String, Object>();
	    Map<String, Object> senderDetails = null;
	    Map<String, Object> senderDetailsForRequestPayload = new HashMap<String, Object>();
	    Map<String, Object> contentDetails = new HashMap<String, Object>();
	    senderDetails = (Map<String, Object>) payLoad.get(TO);
	    //if u dont provide any value then its value will be true by default or it its not present in json then default value will be true
	    boolean respectFrequencyCaps = cachcedProperties.getBooleanPropertyValue(RESPECT_FREQUENCY_CAPS_CLEVERTAP, false);
	    if(MapUtils.isNotEmpty(senderDetails)) {
	    	List<String> fbIds = (List<String>) senderDetails.get(FBID);
	    	List<String> gpids = (List<String>) senderDetails.get(GPID);
	    	List<String> emails = (List<String>) senderDetails.get(EMAIL);
	    	List<String> identities = (List<String>) senderDetails.get(IDENTITY);
	    	List<String> objectIds = (List<String>) senderDetails.get(OBJECT_ID);
	    	if(CollectionUtils.isNotEmpty(fbIds)) {
	    		senderDetailsForRequestPayload.put(FBID, fbIds);
	    	}
	       if(CollectionUtils.isNotEmpty(gpids)) {
	    	   senderDetailsForRequestPayload.put(GPID, gpids);
	       }
	       if(CollectionUtils.isNotEmpty(emails)) {
	    	   senderDetailsForRequestPayload.put(EMAIL, emails);
	       }
	       if(CollectionUtils.isNotEmpty(identities)) {
	    	   senderDetailsForRequestPayload.put(IDENTITY, identities);
	       }
	       if(CollectionUtils.isNotEmpty(objectIds)) {
	    	   senderDetailsForRequestPayload.put(OBJECT_ID, objectIds);
	       }
	       requestPayload.put(TO, senderDetailsForRequestPayload);
	       if(StringUtils.isNotBlank((String)payLoad.get(TAG_GROUP))) {
	    	   requestPayload.put(TAG_GROUP, (String)payLoad.get(TAG_GROUP));
	       }
	       requestPayload.put(RESPECT_FREQUENCY_CAPS, respectFrequencyCaps);
	       if(StringUtils.isNotBlank(senderName)) {
	       contentDetails.put(SENDER_NAME,senderName);
	       }else {
	    	   logger.warn("Sender name is not defind which is mandatory field plz provide sender name for property "+CLEVER_TAP_MAIL_SENDER_NAME);
	       }
	       String subject = (String) payLoad.get(EMAIL_SUBJECT);
	       if(StringUtils.isNotBlank(subject)) {
	    	   contentDetails.put(EMAIL_SUBJECT, subject);
	       }
	       String body = (String) payLoad.get(EMAIL_BODY);
	       if(StringUtils.isNotBlank(body)) {
	    	   contentDetails.put(EMAIL_BODY, body);
	       }
	       requestPayload.put(CONTENT, contentDetails);
	       
	    }
	    return requestPayload;
	}

	/* (non-Javadoc)
	 * @see com.cleartrip.common.notification.EmailSenderViaClevertap#validatePayload(java.util.Map, java.util.List, java.util.Map)
	 */
	public boolean validatePayload(Map<String, Object> payLoad, List<String> errorMessages,
			Map<String, String> sendEmailResponseMap) {
		boolean isNotificationTypePresent = true;
		String notificationTypes = cachcedProperties.getPropertyValue(NOTIFICATION_TYPE_KEY);
		String[] notifications = null;
		List<String> notificationTypesList = null;
		if (StringUtils.isNotBlank(notificationTypes)) {
			notifications = notificationTypes.split(DELIMETER_FOR_NOTIFICATION_TYPE);
			if (notifications.length > 0) {
				notificationTypesList = Arrays.asList(notifications);
			}
		}
		if (MapUtils.isEmpty(payLoad)) {
			if (MapUtils.isNotEmpty(sendEmailResponseMap)) {
				errorMessages.add(sendEmailResponseMap.get(EMPTY_PAYLOAD));
			} else {
				errorMessages.add(" No data was sent at all empty payload.");
			}
			return false;
		}
		Set<String> requestPayloadKeys = payLoad.keySet();
		if (!requestPayloadKeys.contains(NOTIFICATION_TYPE)) {
			isNotificationTypePresent = false;
			if (MapUtils.isNotEmpty(sendEmailResponseMap)) {
				errorMessages.add(sendEmailResponseMap.get(EMP_NOTIFICATION_TYPE));
			} else {
				errorMessages.add("Notification type is not their as part of payload json");
			}
			return false;
		}

		if (isNotificationTypePresent) {
			String notificationType = (String) payLoad.get(NOTIFICATION_TYPE);
			if (StringUtils.isEmpty(notificationType)) {
				if (MapUtils.isNotEmpty(sendEmailResponseMap)) {
					errorMessages.add(sendEmailResponseMap.get(EMP_NOTIFICATION_TYPE));
				} else {
					errorMessages.add("Notification type is not their as part of payload json");
				}
				return false;
			} else {
				if (CollectionUtils.isNotEmpty(notificationTypesList)) {
					if (!notificationTypesList.contains(notificationType)) {
						if (MapUtils.isNotEmpty(sendEmailResponseMap)) {
							errorMessages.add(sendEmailResponseMap.get(INV_NOTIFICATION_TYPE)+ notificationTypesList);
						} else {
							errorMessages.add("Not a valid notification type it must be one from these" + notificationTypesList);
						}
						return false;
					}
				}
			}

		}
		Map<String, List<String>> to =  (Map<String, List<String>>) payLoad.get(TO);
		if (MapUtils.isEmpty(to)) {
			if (MapUtils.isNotEmpty(sendEmailResponseMap)) {
				errorMessages.add(sendEmailResponseMap.get(MANDATORY_FIELD_TO));
			} else {
				errorMessages.add("To is a mandatory field for payload json which can not be empty");
			}
			return false;
		}
		String tagGroup = (String) payLoad.get(TAG_GROUP);
		if (StringUtils.isEmpty(tagGroup)) {
			if (MapUtils.isNotEmpty(sendEmailResponseMap)) {
				errorMessages.add(sendEmailResponseMap.get(MANDATORY_TAG_GROUP));
			} else {
				errorMessages.add("Tag group is mandatory so plz spcify tag_group in payload json");
			}
			return false;
		}
		return true;

	}
}
