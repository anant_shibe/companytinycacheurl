package com.companyApiTinyCache.companyConfigCache.service.common.util.rest;

import java.io.IOException;

/*
 * This class is the new alternate for depreciated RestUtil.
 */
public interface RestClient {
    public RestResponse get(RestRequest request) throws IOException;

    public RestResponse post(RestRequest request) throws IOException;

    public RestResponse put(RestRequest request) throws IOException;

    public RestResponse delete(RestRequest request) throws IOException;

    public static enum CharEncoding {
        UTF8("UTF-8");

        private String stringValue;

        CharEncoding(String value) {
            this.stringValue = value;
        }

        public String stringValue() {
            return stringValue;
        }
    };
}
