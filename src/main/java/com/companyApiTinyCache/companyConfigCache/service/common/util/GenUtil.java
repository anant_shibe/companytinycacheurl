package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Flight;
import com.companyApiTinyCache.companyConfigCache.service.common.data.FlightSegment;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Tag;
import com.companyApiTinyCache.companyConfigCache.service.common.io.ReusableByteArrayOutputStream;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.TxnSourceType;

import com.csvreader.CsvReader;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rits.cloning.Cloner;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.web.util.HtmlUtils;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Method;
import java.net.*;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.*;
import java.util.List;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * General utility class.
 * @author Amlan Roy
 */
public final class GenUtil {

    public static final String PROP_LANG_PREF_COOKIE_NAME = "ct.services.language.preference.cookie.name";

	public static final String LP = "lp";

	public static final String YES = "y";

	public static final String R_IS_CT_EMP = "r_isctemp";

	public static final String IS_CT_EMP = "isctemp";

	public static final String R_COOKIE = "r_cookie";

	public static final String COOKIE = "cookie";

	public static final String CLEARTRIP_DOT_COM = "cleartrip.com";

	public static final String EQUALS_TO = "=";

	public static final String SEMICOLON = ";";

	public static final String USERID = "userid";

	private static volatile DatatypeFactory datatypeFactory;

    /**
     * Different Image Scaling Options.
     * @author suresh
     */
    public static enum ScaleOption {
        SCALE_INSIDE, SCALE_OUTSIDE, SCALE_FIT, SCALE_CROP
    }

    /**
     * Different Mobile Platform Options.
     * @author vipra
     */
    public enum MobilePlatforms {
        Android, iOS, BlackBerry, Windows, Firefox, other
    };

    /**
     * Different Desktop Platform Options.
     * @author vipra
     */
    public enum DesktopPlatforms {
        Ubuntu, Windows, Mac, Linux, ChromeOS, other, Mac_OS_X
    };

    public static final int OBJECT_STREAM_HEADER = 0xaced;

    public static final int GZIP_HEADER = 0x1f8b;

    public static final Charset UTF_8_CHARSET = Charset.forName("UTF-8");

    public static final Charset ISO8859_1_CHARSET = Charset.forName("ISO8859-1");

    private static final Log logger = LogFactory.getLog(GenUtil.class);

    private static final String AFFILIATE_LOG_FORMAT = "%1$s-%2$s::User-%3$s";

    private static final String CSV_FILE_EXTENSION = ".csv";

    public static final String CLEARTRIP_DOMAIN = ".cleartrip.com";

    public static final String NUMBER_FORMAT_PATTERN = "##,##,###.##";

    public static final long MILLIS_PER_DAY = 24 * 3600 * (long) 1000;

    public static final long MILLIS_PER_HOUR = 1 * 3600 * (long) 1000;

    public static final int HTTP_SUCCESS_RESPONSE = 200;

    public static final int HTTP_CREATED_SUCCESSFULLY = 201;

    private static final String CAMM_OPAQUE_TIMIGS = "ct.air.camm.domestic.opaque.timings";

    // For passing as bean to Velocity Templates. Mainly for using xmlGregToDate
    // Since on some environments calling the code in vm files gives error
    public static final GenUtil INSTANCE = new GenUtil();

    private static final String SECURITY_BEAN = "securityBean";

    private static volatile Cloner cloner;

    private static String[] units = {"", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine"};
    private static String[] teen = {" Ten", " Eleven", " Twelve", " Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen"};
    private static String[] tens = {" Twenty", " Thirty", " Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
    private static String[] maxs = {"", "", " Hundred", " Thousand", " Lakh", " Crore"};
    private static String input;
    
    private static String characters = "012WXYZ34abPQRSTUVcdefghijpq56789rstuvwxyzABCklmnoDEFGHIJKLMNO";
    
    private GenUtil() {

    }

    private static volatile CachedProperties cachedProperties;

    public static CachedProperties getProperties() {
        if (cachedProperties == null) {
            cachedProperties = (CachedProperties) ServicesApplicationContext.getBean("commonCachedProperties");
        }

        return cachedProperties;
    }
    
    public static long decodeBase62(String b62) {
        for (char character : b62.toCharArray()) {
            if (!characters.contains(String.valueOf(character))) {
                throw new IllegalArgumentException("Invalid character(s) in string: " + character);
            }
        }
        long ret = 0;
        b62 = new StringBuffer(b62).reverse().toString();
        long count = 1;
        for (char character : b62.toCharArray()) {
            ret += characters.indexOf(character) * count;
            count *= 62;
        }
        return ret;
    }

    public static String encodeBase10(long b10) {
        if (b10 < 0) {
            throw new IllegalArgumentException("b10 must be nonnegative");
        }
        String ret = "";
        while (b10 > 0) {
            ret = characters.charAt((int) (b10 % 62)) + ret;
            b10 /= 62;
        }
        return ret;

    }

    public static int getAge(int dobYear, int dobMonth, int dobDay) {
        int day = 1, month = 0, year = 1, ageYears;

        Calendar cd = Calendar.getInstance();
        try {
            year = dobYear;
            month = dobMonth;
            day = dobDay;
            if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11) {
                if (day > 31 || day < 1) {
                    return 0;
                }
            } else if (month == 3 || month == 5 || month == 8 || month == 10) {
                if (day > 30 || day < 1) {
                    return 0;
                }
            } else {
                if (new GregorianCalendar().isLeapYear(year)) {
                    if (day < 1 || day > 29) {
                        return 0;
                    }
                } else if (day < 1 || day > 28) {
                    return 0;
                }
            }
        } catch (NumberFormatException ne) {
            return 0;
        }

        Calendar bd = new GregorianCalendar(year, month, day);
        ageYears = cd.get(Calendar.YEAR) - bd.get(Calendar.YEAR);
        if (cd.before(new GregorianCalendar(cd.get(Calendar.YEAR), month, day))) {
            ageYears--;
        }
        return ageYears;
    }

    public static String removeCardNumberAndCVVCode(String postMessage, String paymentMode, String cardNbr, String cvv, String typeOfCall) {
        String logPostMessage = "";
        if (paymentMode.equals("CC") || (paymentMode.equalsIgnoreCase("DC"))) {
            String cardNumber = cardNbr;
            int lengthOfCardNumber = cardNumber.length();

            String cardNumberToReplace = "", cvvCodeToReplace = "";
            int lengthOfCVVCode = cvv.length();
            if (typeOfCall.equalsIgnoreCase("payCall")) {
                cardNumberToReplace = "<payment:card-number>" + cardNumber.substring(0, 6) + "XXXXX" + cardNumber.substring(lengthOfCardNumber - 4, lengthOfCardNumber) + "</payment:card-number>";
                logPostMessage = postMessage.replaceFirst("<payment:card-number>" + cardNumber + "</payment:card-number>", cardNumberToReplace);
                cvvCodeToReplace = "<payment:cvv>XX";
                for (int i = 2; i < lengthOfCVVCode; i++) {
                    cvvCodeToReplace = cvvCodeToReplace + "X";
                }
                cvvCodeToReplace = cvvCodeToReplace + "</payment:cvv>";
                logPostMessage = logPostMessage.replaceFirst("<payment:cvv>" + cvv + "</payment:cvv>", cvvCodeToReplace);
            } else {
                cardNumberToReplace = "<card-number>" + cardNumber.substring(0, 6) + "XXXXX" + cardNumber.substring(lengthOfCardNumber - 4, lengthOfCardNumber) + "</card-number>";
                logPostMessage = postMessage.replaceFirst("<card-number>" + cardNumber + "</card-number>", cardNumberToReplace);
                cvvCodeToReplace = "<card-cvv>XX";
                for (int i = 2; i < lengthOfCVVCode; i++) {
                    cvvCodeToReplace = cvvCodeToReplace + "X";
                }
                cvvCodeToReplace = cvvCodeToReplace + "</card-cvv>";
                logPostMessage = logPostMessage.replaceFirst("<card-cvv>" + cvv + "</card-cvv>", cvvCodeToReplace);
            }
        } else {
            logPostMessage = postMessage;
        }
        return logPostMessage;
    }

    public static String getCabinTypeCode(String cabinType) {
        if (cabinType.equals("Economy")) {
            return "E";
        } else if (cabinType.equals("Business")) {
            return "B";
        } else if (cabinType.equals("Premium Economy")) {
            return "P";
        } else if (cabinType.equals("First")) {
            return "F";
        }
        return "E";
    }

    public static String getSeatingClassCode(String cabinType) {
        if (cabinType.equals("Sleeper")) {
            return "SL";
        } else if (cabinType.equals("Second Sitting")) {
            return "2S";
        } else if (cabinType.equals("AC Chair Car")) {
            return "CC";
        } else if (cabinType.equals("First Class")) {
            return "FC";
        } else if (cabinType.equals("AC First Class")) {
            return "1A";
        } else if (cabinType.equals("AC 2 Tier")) {
            return "2A";
        } else if (cabinType.equals("AC 3 Tier")) {
            return "3A";
        }
        return "SL";
    }

    public static String getSeatingClassStr(String cabinType) {
        if (cabinType.equals("SL")) {
            return "Sleeper";
        } else if (cabinType.equals("2S")) {
            return "Second Sitting";
        } else if (cabinType.equals("CC")) {
            return "AC Chair Car";
        } else if (cabinType.equals("FC")) {
            return "First Class";
        } else if (cabinType.equals("1A")) {
            return "AC First Class";
        } else if (cabinType.equals("2A")) {
            return "AC 2 Tier";
        } else if (cabinType.equals("3A")) {
            return "AC 3 Tier";
        } else if (cabinType.equals("3E")) {
            return "AC 3 Economy";
        }
        return "Sleeper";
    }

    public static String getBerthStr(String berthCode) {
        if (berthCode.equals("LB")) {
            return "Lower";
        } else if (berthCode.equals("UB")) {
            return "Upper";
        } else if (berthCode.equals("SL")) {
            return "Side Lower";
        } else if (berthCode.equals("SU")) {
            return "Side Upper";
        } else if (berthCode.equals("MB")) {
            return "Middle";
        }
        return "Lower";
    }

    /**
     * Overloading method which takes the response code.
     * @param responseCode Response code
     * @return True If it is a 200 or 201 response
     */

    public static boolean hasSuccessCode(int responseCode) {
        return ((responseCode == HTTP_CREATED_SUCCESSFULLY) || (responseCode == HTTP_SUCCESS_RESPONSE));
    }

    /**
     * Checks if the RestResponse is a Valid and successful returning Response. The check is done by validating against response code for 200 and 201
     * @param restResponse Rest Response
     * @return boolean True If it is a 200 or 201 response
     */
    public static boolean hasSuccessCode(RestResponse restResponse) {
        if (restResponse != null) {
            int responseCode = restResponse.getCode();
            return hasSuccessCode(responseCode);
        } else {
            return false;
        }
    }

    public static int getFlightCountFromSearchResultJson(String searchResult) {
        int flightCount = 0;

        try {
            if (StringUtils.isNotEmpty(searchResult)) {
                if (searchResult.indexOf("flightData") != -1) { // international
                    int idIdx = searchResult.lastIndexOf("\"id\"", searchResult.length());
                    int commaIdx = searchResult.indexOf(",", idIdx + 5);
                    String flightCountStr = searchResult.substring(idIdx + 5, commaIdx);
                    flightCountStr = flightCountStr.replaceAll("\"", "");
                    flightCountStr = getFlightNumber(flightCountStr);
                    flightCount = Integer.parseInt(StringUtils.isNotEmpty(flightCountStr) ? flightCountStr : "0") + 1;
                } else {
                    int idIdx = searchResult.lastIndexOf("\"id\":\"", searchResult.length());
                    if (idIdx != -1) {
                        int commaIdx = searchResult.indexOf("\",", idIdx + 5);
                        String flightCountStr = searchResult.substring(idIdx + 5, commaIdx);
                        flightCountStr = flightCountStr.replaceAll("\"", "");
                        flightCountStr = getFlightNumber(flightCountStr);
                        flightCount = Integer.parseInt(StringUtils.isNotEmpty(flightCountStr) ? flightCountStr : "0");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("error GenUtil.getFlightCountFromSearchResultJson", e);
        }

        return flightCount;
    }

    private static String getFlightNumber(String flightCountStr) {
        String flightNumber = "0";
        Pattern pattern = Pattern.compile("[0-9]+");
        Matcher matcher = pattern.matcher(flightCountStr);
        if (matcher.find()) {
            flightNumber = matcher.group(0);
        }
        return flightNumber;
    }

    public static List<Tag> getTags(HttpServletRequest request) {
        List<Tag> tags = new ArrayList<Tag>();

        for (Iterator<?> iterator = request.getParameterMap().keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            if (key.contains("tag_")) {
                String tagName = ((String[]) request.getParameterMap().get(key))[0];
                String[] selectedTagIds = key.split("_");
                String tagId = selectedTagIds[1].toString();
                Tag tag = new Tag();
                tag.setTagId(StringUtils.isNotEmpty(tagId) ? Integer.parseInt(tagId) : 0);
                tag.setTagName(tagName);
                tags.add(tag);
            }
        }
        return tags;
    }

    /**
     * Gives the difference of two days.
     * @param startDate Date
     * @param endDate Date
     * @return difference of two days.
     */
    public static int getNumberOfNights(Date startDate, Date endDate) {
        return getNumberOfNights(startDate, endDate, false);
    }

    public static int getNumberOfNights(Date startDate, Date endDate, boolean setNullStartDateAsToday) {
        if (setNullStartDateAsToday && startDate == null) {
            startDate = new Date();
        }
        int nights = 0;
        try {
            if (startDate != null && endDate != null) {
                startDate = formatDate(startDate, "yyyy-MM-dd");
                endDate = formatDate(endDate, "yyyy-MM-dd");
                /*Calendar start = new GregorianCalendar();
                start.setTime(startDate);
                Calendar end = new GregorianCalendar();
                end.setTime(endDate);
                while (start.before(end)) {
                    nights++;
                    start.add(Calendar.DAY_OF_YEAR, 1);
                }*/
                // Changing implementation for same.
                if(startDate.before(endDate)) {
                    nights = getDifferenceBetweenTwoDatesInDays(startDate, endDate);
                }
            }
        } catch (Exception e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        return nights;
    }

    public static int getNumberOfDays(Date startDate, Date endDate) {
        return getNumberOfDays(startDate, endDate, false);
    }

    private static int getDifferenceBetweenTwoDatesInDays(Date startDate, Date endDate) {
        long diffInMillis = Math.abs(endDate.getTime() - startDate.getTime());
        return (int) TimeUnit.DAYS.convert(diffInMillis, TimeUnit.MILLISECONDS);
    }

    public static int getNumberOfDays(Date startDate, Date endDate, boolean setNullStartDateAsToday) {
        if (setNullStartDateAsToday && startDate == null) {
            startDate = new Date();
        }
        int days = 0;
        try {
            if (startDate != null && endDate != null) {
                startDate = formatDate(startDate, "yyyy-MM-dd");
                endDate = formatDate(endDate, "yyyy-MM-dd");
                /*Calendar start = new GregorianCalendar();
                start.setTime(startDate);
                Calendar end = new GregorianCalendar();
                end.setTime(endDate);
                while (start.before(end)) {
                    days++;
                    start.add(Calendar.DAY_OF_YEAR, 1);
                }*/
                // Changing implementation for same.
                if(startDate.before(endDate)) {
                    days = getDifferenceBetweenTwoDatesInDays(startDate, endDate);
                }
                days++;
            }
        } catch (Exception e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        return days;
    }

    // Assumes the dates to be truncated to midnight as per IST or other non-Daylight savings Time Zone
    public static int getNumberOfNightsLite(Date startDate, Date endDate) {
        return (int) ((endDate.getTime() - startDate.getTime()) / GenUtil.MILLIS_PER_DAY);
    }

    /**
     * Logic given in below url is used to calculate duration: http://openflights.org/help/time.html.
     * @param departureDateTime Departure Time
     * @param arrivalDateTime Arrival Time
     * @param departureTimeZone Departure TimeZone
     * @param arrivalTimeZone Arrival TimeZone
     * @return Duration in seconds
     */
    public static int getDuraion(Date departureDateTime, Date arrivalDateTime, double departureTimeZone, double arrivalTimeZone) {
        int durationInSeconds = -1;

        int numberOfNightsInMinutes = 0;

        if (!DateUtils.isSameDay(departureDateTime, arrivalDateTime)) {
            int numberOfNights = GenUtil.getNumberOfNights(departureDateTime, arrivalDateTime);
            numberOfNightsInMinutes = numberOfNights * 24 * 60;
        }

        Calendar departureDateTimeCalendar = Calendar.getInstance();
        Calendar arrivalDateTimeInstance = Calendar.getInstance();
        departureDateTimeCalendar.setTime(departureDateTime);
        arrivalDateTimeInstance.setTime(arrivalDateTime);

        int departureHours = departureDateTimeCalendar.get(Calendar.HOUR_OF_DAY);
        int departureMinutes = departureDateTimeCalendar.get(Calendar.MINUTE);

        int departureTimeInMinutes = departureHours * 60 + departureMinutes;

        int arrivalHours = arrivalDateTimeInstance.get(Calendar.HOUR_OF_DAY);
        int arrivalMinutes = arrivalDateTimeInstance.get(Calendar.MINUTE);
        int arrivalTimeInMinutes = arrivalHours * 60 + arrivalMinutes;

        int floorDepTimeZoneVal = (int) Math.floor(departureTimeZone);
        int floorArrivalTimeZoneVal = (int) Math.floor(arrivalTimeZone);

        int departureTimeZoneInMinutes = (int) ((int) floorDepTimeZoneVal * 60 + ((departureTimeZone - floorDepTimeZoneVal) * 60));
        int arrivalTimeZoneInMinutes = (int) ((int) floorArrivalTimeZoneVal * 60 + ((arrivalTimeZone - floorArrivalTimeZoneVal) * 60));

        double timeZoneDiff = arrivalTimeZoneInMinutes - departureTimeZoneInMinutes;

        arrivalTimeInMinutes = arrivalTimeInMinutes + numberOfNightsInMinutes;

        int duratinInMinutes = (int) ((int) (arrivalTimeInMinutes - departureTimeInMinutes) - timeZoneDiff);
        // UCBS-1372 CX-872 duration bug fix
        int minutesToBeReduced = 0;
        if (arrivalDateTime.before(departureDateTime)) {
            int noOfDays = getNumberOfDays(arrivalDateTime, departureDateTime) - 1;
            minutesToBeReduced = noOfDays * 1440;
        }
        duratinInMinutes -= minutesToBeReduced;
        durationInSeconds = Math.abs(duratinInMinutes * 60);
        return durationInSeconds;
    }

    /**
     * This method takes 2 dates as input and returns the ceil difference in Days.
     * @param startDate
     * @param endDate
     * @return
     */
    public static Double getDateDifferenceInDays(Date startDate, Date endDate) {
    	return (Double) Math.ceil((TimeUnit.MILLISECONDS.toHours(Math.abs((endDate.getTime() - startDate.getTime()))))/24);
    }

    /**
     * This method returns the hex format.
     * @param buf byte[]
     * @return String
     */
    public static String asHex(byte[] buf) {
        StringBuilder strbuf = new StringBuilder(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

    /**
     * Formats Indian currency with grouping separators, suitable for lakhs and crores. If format does not have comma(,) or value is less than 1 lakh then it just calls
     * {@link Formatter#format(String, Object...)} with passed format string. Otherwise it uses a DecimalFormat to format the portion above 1 lakh with commas every 2 digits and uses Formatter to
     * format the remaining.
     * @param format Format String. Should not be prefixed with non format text
     * @param value to format
     * @param op Output to append to e.g StringBuilder or Writer
     * @throws IOException on IO Exception
     */
    public static void formatIndianCurrency(String format, double value, Appendable op) throws IOException {
        Formatter formatter = new Formatter(op);
        if (value < 100000 || format.indexOf(',') < 0) {
            formatter.format(format, value);
        } else {
            DecimalFormat df = new DecimalFormat();
            df.applyPattern("##,#0");
            double val1 = (long) Math.floor(value / 1000);
            String leadingDigits = df.format(val1);
            op.append(leadingDigits).append(',');
            int remainingValueDigits = String.valueOf(Math.abs((int) Math.floor(value % 1000))).length();
            if (remainingValueDigits < 3) {
                if (remainingValueDigits == 1) {
                    op.append("00");
                } else if (remainingValueDigits == 2) {
                    op.append("0");
                }
            }
            formatter.format(format, value % 1000);
        }
    }

    /**
     * Formats Indian currency with grouping separators, suitable for lakhs and crores. If format does not have comma(,) or value is less than 1 lakh then it just calls
     * {@link Formatter#format(String, Object...)} with passed format string. Otherwise it uses a DecimalFormat to format the portion above 1 lakh with commas every 2 digits and uses Formatter to
     * format the remaining.
     * @param format Format String. Should not be prefixed with non format text
     * @param value to format return Formatted String
     * @return Formatted Currency as String
     */
    public static String formatIndianCurrency(String format, double value) {
        StringBuilder opBuf = BaseStats.tmpStringBuilder();
        try {
            formatIndianCurrency(format, value, opBuf);
        } catch (IOException e) {
            opBuf = opBuf;
        }
        String formatResult = opBuf.toString();
        opBuf = BaseStats.returnTmpStringBuilder(opBuf);
        return formatResult;
    }

    /**
     * Creates a deep copy of original object.
     * @param original Original object
     * @return Returns the clone
     */
    public static Serializable deepCopy(Serializable original) {

        /**
         * The following code ensures that the new cloning logic (which uses Reflection) is called only for the classes which fall under "air" package. If the switch is off or if the package does not
         * contain air in it, the old Serialization flow will be followed.
         */
        boolean iscloner = false;
        boolean isAircloner = false;
        boolean isHotelcloner = false;
        boolean isClonerLoggingEnabled = false;
        try {
            getProperties();
            CachedProperties cachedProperties = (CachedProperties) ServicesApplicationContext.getBean("commonCachedProperties");
            if (cachedProperties != null) {
                iscloner = cachedProperties.getBooleanPropertyValue("ct.common.deep.copy.cloner", false);
                isClonerLoggingEnabled = cachedProperties.getBooleanPropertyValue("ct.air.cloner.time.logger.enabled", false);
                isAircloner = cachedProperties.getBooleanPropertyValue("ct.common.deep.copy.cloner.air", false);
            }

            if (iscloner && original != null) {
                if (isAircloner) {
                    String name = original.getClass().getName();
                    if (name != null) {
                        if ((isAircloner && name.contains(".air."))) {
                            if (isClonerLoggingEnabled) {
                                logger.error("Using Cloner Copy for :" + name);
                            }
                            return clonerCopy(original);
                        }
                    }
                }
            }
        } catch (Exception e) {
            if (isClonerLoggingEnabled) {
                String className = "";
                if (original != null && original.getClass() != null) {
                    String name = original.getClass().getName();
                    className = name;
                }
                logger.error("Exception while trying to cloner copy for : " + className, e);
            }

            logger.info("Exception while trying to cloner copy", e);
        }

        Serializable copy = null;

        ReusableByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;

        try {
            baos = BaseStats.tmpReusableByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(original);
            oos.flush();

            ByteArrayInputStream bais = baos.getInputStream();
            ois = new ObjectInputStream(bais);
            copy = (Serializable) ois.readObject();
        } catch (Exception e) {
            logger.error("Exception occured during deep copy", e);
        } finally {
            try {
                baos = BaseStats.returnTmpReusableByteArrayOutputStream(baos);
            } catch (Exception e) {
                copy = copy;
            }
        }

        return copy;
    }

    public static Serializable clonerCopy(Serializable original) {
        Serializable copy = null;
        boolean iscloner = false;

        try {
            getProperties();
            if (cachedProperties != null) {
                iscloner = cachedProperties.getBooleanPropertyValue("ct.common.deep.copy.cloner", false);
            }
        } catch (Exception e) {
            logger.info("Exception while fetching cloner property" + e);
            // If this is logged as an Error , it will flood the logs. Hence, logging it as info.
        }

        if (iscloner) {
            try {
                if (cloner == null) {
                    cloner = new Cloner();
                }

                if (cloner != null) {
                    copy = cloner.deepClone(original);
                    if (copy != null) {
                        return copy;
                    }
                }
            } catch (Exception e) {
                logger.error("Error while Deep Copying using Cloner : If this error persists, please switch this property off : " + "ct.common.deep.copy.cloner");
            }
        }

        return copy;
    }

    private static final char[] ranChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678901234567890123456789".toCharArray();

    /**
     * Generates 16 byte randon alpha numeric String.
     * @param r Random generator to use
     * @return Random alpha numeric
     */
    public static String generateRandomAlphaNumeric(Random r) {

        StringBuilder ranAlpha = new StringBuilder();

        for (int i = 0; i < 16; i++) {
            ranAlpha.append(ranChars[r.nextInt(ranChars.length)]);
        }
        return ranAlpha.toString();
    }

    /**
     * Used to merge Velocity templates.
     * @param template Velocity template
     * @param context Velocity context
     * @param writer StringBuilder Writer to use
     * @return Merged output
     */
    public static String mergeTemplateIntoString(Template template, VelocityContext context, StringBuilderWriter writer) {
        String str = null;

        if (template != null) {
            StringBuilderWriter sbw = null;
            if (writer == null) {
                sbw = BaseStats.tmpWriter();
                writer = sbw;
            }

            try {
                template.merge(context, writer);
                str = writer.toString();
            } catch (IOException e) {
                logger.error("IOException while merging velocity template", e);
            } catch (Exception ex) {
                logger.error("Exception while merging velocity template", ex);
            }
            sbw = BaseStats.returnTmpWriter(sbw);
        }

        return str;
    }

    /**
     * Used to merge Velocity templates.
     * @param template Velocity template
     * @param context Velocity context
     * @return Merged output
     */
    public static String mergeTemplateIntoString(Template template, VelocityContext context) {
        return mergeTemplateIntoString(template, context, null);
    }

    /**
     * parses the date string for the given formatter.
     * @param ongoingDepartDate Date to Parse
     * @param dateFormatter Date Format to use
     * @return Parsed Date
     */
    public static Date createParsedDate(String ongoingDepartDate, String dateFormatter) {
        SimpleDateFormat sdfInput = new SimpleDateFormat(dateFormatter);
        String textDate = ongoingDepartDate;
        try {
            return sdfInput.parse(textDate);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * parses the date string for the given formatter.
     * @param date Date to format to String
     * @param dateFormatter Date Formatter to use
     * @return Formatted Date String
     */
    public static String createParsedDate(Date date, String dateFormatter) {
        SimpleDateFormat sdfInput = new SimpleDateFormat(dateFormatter);
        return sdfInput.format(date);
    }

    /**
     * parses the date string for the given formatter.
     * @param date Date to parse
     * @param dateFormatter Date Formatter to use
     * @return Formatted Date
     * @exception Exception on any exception
     */
    public static Date formatDate(Date date, String dateFormatter) throws Exception {
        SimpleDateFormat sdfInput = new SimpleDateFormat(dateFormatter);
        String formattedDate = sdfInput.format(date);
        date = sdfInput.parse(formattedDate);
        return date;
    }

    public static String formateDoubleValue(double value) {
        NumberFormat format = NumberFormat.getInstance();
        DecimalFormat df = (DecimalFormat) format;
        String pattern = GenUtil.NUMBER_FORMAT_PATTERN;
        df.applyPattern(pattern);
        df.setMinimumFractionDigits(2);
        return format.format(value);
    }

    public static String escapeHtmlAndNl(String s) {
        String escapedS = "";
        if (s != null) {
            String nlStr = System.getProperty("line.separator");
            if (nlStr == null) {
                nlStr = "\n";
            }
            escapedS = s.trim().replace(nlStr, "\\n");
            escapedS = escapedS.replace("\n", "\\n");
            escapedS = HtmlUtils.htmlEscape(escapedS);
        }
        return escapedS;
    }

    /**
     * Gets the todays date in dd/MM/yyyy string format.
     * @return String
     */
    public static String getMinDate() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String mindate = df.format(date);
        return mindate;
    }

    public static void writeXMLResponse(HttpServletResponse response, String xml) throws IOException {
        byte[] contents = xml.getBytes();

        // Set to expire far in the past.
        response.setHeader("Expires", "Mon, 3 Jul 1967 12:00:00 GMT");

        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache,must-revalidate");

        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");

        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");

        response.setContentType("text/xml");

        response.setContentLength(contents.length);

        OutputStream os = response.getOutputStream();
        os.write(contents);
        os.flush();
    }

    public static void writeCSVResponse(byte[] data, HttpServletResponse response, String reportType) throws IOException {
        response.setHeader("Expires", "Mon, 3 Jul 1967 12:00:00 GMT");
        response.setHeader("Cache-Control", "no-store, no-cache,must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("text/csv");
        String fileName = reportType + CSV_FILE_EXTENSION;
        response.setHeader("Content-disposition", "attachment; filename=" + fileName + "; size=" + data.length);
        response.setContentLength(data.length);
        OutputStream os = response.getOutputStream();
        os.write(data);
        os.flush();
        os.close();

    }

    public static void writePlainTextResponse(HttpServletResponse response, String text) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.print(text);
        writer.flush();
    }

    public static byte[] converToByteArray(String[] arr) {
        StringBuilder builder = BaseStats.tmpStringBuilder();
        for (String string : arr) {
            builder.append(string);
            String newline = System.getProperty("line.separator");
            builder.append(newline);
        }
        byte[] bytes = builder.toString().getBytes();
        builder = BaseStats.returnTmpStringBuilder(builder);
        return bytes;
    }

    public static Flight getFlight(HttpServletRequest request, String type) {
        Flight flight = new Flight();
        flight.setBase_price(
                Double.parseDouble((request.getParameter(type + "_base_price") != null && !request.getParameter(type + "_base_price").equals("")) ? request.getParameter(type + "_base_price") : "0"));
        flight.setAdult_base(
                Double.parseDouble((request.getParameter(type + "_adult_base") != null && !request.getParameter(type + "_adult_base").equals("")) ? request.getParameter(type + "_adult_base") : "0"));
        flight.setTaxes(Double.parseDouble((request.getParameter(type + "_taxes") != null && !request.getParameter(type + "_taxes").equals("")) ? request.getParameter(type + "_taxes") : "0"));
        flight.setDiscount(Double.parseDouble((request.getParameter(type + "_disc") != null && !request.getParameter(type + "_disc").equals("")) ? request.getParameter(type + "_disc") : "0"));
        flight.setBookingFee(Double.parseDouble((request.getParameter(type + "_fee") != null && !request.getParameter(type + "_fee").equals("")) ? request.getParameter(type + "_fee") : "0"));
        flight.setE_ticket(request.getParameter(type + "_e_ticket"));

        Collection<FlightSegment> segments = new ArrayList<FlightSegment>();
        int legs = Integer.parseInt((request.getParameter(type + "_no_legs") != null && !request.getParameter(type + "_no_legs").equals("")) ? request.getParameter(type + "_no_legs") : "0");
        for (int i = 1; i <= legs; i++) {
            FlightSegment flightSegment = new FlightSegment();
            flightSegment.setAirlineName(request.getParameter(type + "_leg_airline_" + i));
            flightSegment.setAirlineCode(request.getParameter(type + "_leg_aircode_" + i));
            flightSegment.setFrom(request.getParameter(type + "_leg_from_" + i));
            flightSegment.setFromCityName(request.getParameter(type + "_leg_fromCityName_" + i));
            flightSegment.setFromAirportName(request.getParameter(type + "_leg_fromAirportName_" + i));

            flightSegment.setTo(request.getParameter(type + "_leg_to_" + i));
            flightSegment.setToCityName(request.getParameter(type + "_leg_toCityName_" + i));
            flightSegment.setToAirportName(request.getParameter(type + "_leg_toAirportName_" + i));

            flightSegment.setFinalDest(request.getParameter(type + "_leg_finalDest_" + i));

            flightSegment.setFlightNumber(request.getParameter(type + "_leg_flt_num_" + i));
            flightSegment.setEquipment(request.getParameter(type + "_leg_equipment_" + i));
            flightSegment.setDeparts(request.getParameter(type + "_leg_departs_" + i));
            flightSegment.setDepartDateStr(request.getParameter(type + "_leg_departDate_" + i));

            flightSegment.setArrives(request.getParameter(type + "_leg_arrives_" + i));
            flightSegment.setArriveDateStr(request.getParameter(type + "_leg_arriveDate_" + i));
            flightSegment.setDuration(Integer.parseInt((request.getParameter(type + "_leg_duration_" + i) != null && !request.getParameter(type + "_leg_duration_" + i).equals(""))
                    ? request.getParameter(type + "_leg_duration_" + i) : "0"));
            flightSegment.setLayover(Integer.parseInt((request.getParameter(type + "_leg_layover_" + i) != null && !request.getParameter(type + "_leg_layover_" + i).equals(""))
                    ? request.getParameter(type + "_leg_layover_" + i) : "0"));
            flightSegment.setFareBasisClass(request.getParameter(type + "_leg_fare_class_" + i));
            flightSegment.setFareBasis(request.getParameter(type + "_leg_fare_basis_" + i));
            flightSegment.setFareKey(request.getParameter(type + "_leg_fare_key_" + i));

            flightSegment.setCabinType(request.getParameter(type + "_leg_cabin_type_" + i));
            flightSegment.setTicketType(request.getParameter(type + "_leg_tkt_type_" + i));
            flightSegment.setAdultBase(Double.parseDouble((request.getParameter(type + "_leg_adult_base_" + i) != null && !request.getParameter(type + "_leg_adult_base_" + i).equals(""))
                    ? request.getParameter(type + "_leg_adult_base_" + i) : "0"));
            flightSegment.setChildBase(Double.parseDouble((request.getParameter(type + "_leg_child_base_" + i) != null && !request.getParameter(type + "_leg_child_base_" + i).equals(""))
                    ? request.getParameter(type + "_leg_child_base_" + i) : "0"));
            flightSegment.setInfantBase(Double.parseDouble((request.getParameter(type + "_leg_infant_base_" + i) != null && !request.getParameter(type + "_leg_infant_base_" + i).equals(""))
                    ? request.getParameter(type + "_leg_infant_base_" + i) : "0"));
            flightSegment.setVia(request.getParameter(type + "_leg_via_" + i));
            flightSegment.setStops(Integer.parseInt(
                    (request.getParameter(type + "_leg_stops_" + i) != null && !request.getParameter(type + "_leg_stops_" + i).equals("")) ? request.getParameter(type + "_leg_stops_" + i) : "0"));
            flightSegment.setHasStops((request.getParameter(type + "_leg_hasStops_" + i) != null && request.getParameter(type + "_leg_hasStops_" + i).equals(YES)) ? true : false);
            flightSegment
                    .setStopOverSegment((request.getParameter(type + "_leg_isStopOverSegment_" + i) != null && request.getParameter(type + "_leg_isStopOverSegment_" + i).equals(YES)) ? true : false);

            segments.add(flightSegment);
        }

        flight.setSegments(segments);

        return flight;
    }

    public static String paramMapToQueryString(Map<String, String[]> paramMap, StringBuilder sb) throws UnsupportedEncodingException {
        String queryString = null;
        if (paramMap != null) {
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.setLength(0);
            }
            int i;
            boolean first = true;
            for (String paramName : paramMap.keySet()) {
                String[] paramVals = paramMap.get(paramName);
                for (i = 0; i < paramVals.length; i++) {
                    if (!first) {
                        sb.append('&');
                    }
                    sb.append(paramName).append('=').append(URLEncoder.encode(paramVals[i], "UTF-8"));
                    first = false;
                }
            }
            queryString = sb.toString();
        }

        return queryString;
    }

    public static String constructPostMsgFromRequest(HttpServletRequest request, String prod) {
        String postMsg = "";
        for (Iterator iterator = request.getParameterMap().keySet().iterator(); iterator.hasNext();) {
            String paramName = (String) iterator.next();
            try {
                // if (prod.equals("train")) { // train search goes to the train
                // app and gets redirected with the params already encoded
                // postMsg+=(StringUtils.isNotEmpty(postMsg)?"&":"")+ // so no
                // need to do an URLEncode
                // paramName+"="+request.getParameter(paramName);
                // } else {
                if (prod.equals("hotels")) {
                    String regex = "(ca\\d+)";
                    Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
                    Matcher matcher = pattern.matcher(paramName);
                    if (matcher.find() && !StringUtils.containsIgnoreCase(postMsg, paramName)) {
                        String[] childAges = request.getParameterValues(paramName);
                        for (String childAge : childAges) {
                            postMsg += (StringUtils.isNotEmpty(postMsg) ? "&" : "") + URLEncoder.encode(paramName, "UTF-8") + EQUALS_TO + URLEncoder.encode(childAge, "UTF-8");
                        }
                        continue;
                    }
                }
                postMsg += (StringUtils.isNotEmpty(postMsg) ? "&" : "") + URLEncoder.encode(paramName, "UTF-8") + EQUALS_TO + URLEncoder.encode(request.getParameter(paramName), "UTF-8");
                // }
            } catch (Exception ex) {
                logger.error("Exception is:" + ex.getMessage(), ex);
            }
        }
        if (prod.equals("air")) {
            if (!postMsg.contains("childs")) {
                postMsg += "&childs=0";
            }
            if (!postMsg.contains("infants")) {
                postMsg += "&infants=0";
            }
        }
        if (request.getAttribute(CommonConstants.COUNTRY) != null && request.getAttribute(CommonConstants.SELL_CURRENCY) != null && !(prod.equals("train"))) {
            postMsg += "&sct=" + request.getAttribute(CommonConstants.COUNTRY).toString();
            postMsg += "&scr=" + request.getAttribute(CommonConstants.SELL_CURRENCY).toString();
        }
        return postMsg;
    }

    public static String xmlToString(Node node) {
        try {
            Source source = new DOMSource(node);
            StringWriter stringWriter = new StringWriter();
            Result result = new StreamResult(stringWriter);
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.transform(source, result);
            return stringWriter.getBuffer().toString();
        } catch (TransformerConfigurationException e) {
            logger.error("Exception is:" + e.getMessage(), e);
        } catch (TransformerException e) {
            logger.error("Exception is:" + e.getMessage(), e);
        }
        return null;
    }

    private static final Map<String, String> bookingStatus;

    public static String getBookingStatus(String bookingStatusCode) {
        return bookingStatus.get(bookingStatusCode);
    }

    private static final Map<String, String> quota;

    static {
        quota = new HashMap<String, String>();
        quota.put("GN", "General");
        quota.put("CK", "Tatkal");

        bookingStatus = new HashMap<String, String>();
        bookingStatus.put("P", "Confirmed");
        bookingStatus.put("C", "Confirmed"); // C-Coupon. Since this a confirmed booking, putting this under confirmed.
        bookingStatus.put("Q", "Cancelled");
        bookingStatus.put("K", "Refunded");
        bookingStatus.put("H", "Failed");
        bookingStatus.put("R", "Hold");
        bookingStatus.put("W", "Approval Pending"); // Status added for Pre Trip Approval Feature in Corporate
        bookingStatus.put("B", "Approved"); // Status added for Pre Trip Approval Feature in Corporate
        bookingStatus.put("J", "Approval Rejected"); // Status added for Pre Trip Approval Feature in Corporate
        bookingStatus.put("T", "PayAtHotel Confirmed"); // Status added for PAH feature in Corporate
        bookingStatus.put("I", "PayAtHotel Confirmed"); // Status added for PAH feature in Corporate
        bookingStatus.put("N", "NoShow"); // Status added for PAH feature in Corporate
    }

    public static String getQuotaName(String quotaCode) {
        return quota.get(quotaCode);
    }

    public static String getRidOfXmlTimeZone(String flightStartDate) {
        int colonLastIndex = flightStartDate.lastIndexOf(':');
        if (StringUtils.isNotEmpty(flightStartDate)) {
            flightStartDate = flightStartDate.substring(0, colonLastIndex) + flightStartDate.substring(colonLastIndex + 1);
        }
        return flightStartDate;
    }

    /**
     * Invokes the inner methods. e.g. Class Structure: Employee -> Department -> departmentName To get the department name from employee object, pass employee object as first parameter and
     * department.departmentName as second parameter. If the method is not available
     * @param obj Base object on which the method invocation to start.
     * @param attribute Dot seaprated attributes.
     * @return Value for the last attribute .
     * @exception Exception If there is any exception during invocation
     */
    public static Object invokeMethod(Object obj, String attribute) throws Exception {

        Object value = null;

        if (obj != null) {
            String[] attributes = attribute.split("\\.", -1);
            try {
                for (int i = 0; i < attributes.length; i++) {
                    String field = attributes[i];
                    String methodName = "get" + StringUtils.capitalize(field);
                    Method m = obj.getClass().getDeclaredMethod(methodName, (Class[]) null);
                    value = m.invoke(obj, (Object[]) null);
                    if (value == null) {
                        break;
                    }
                    obj = value;
                }
            } catch (NullPointerException npe) {
                value = null;
            }

        }
        return value;
    }

    /**
     * Reads an image from an input stream and closes it at the end.
     * @param ip Image input
     * @return Buffered Image read from input
     * @throws IOException If there is an IO Exception
     */
    public static BufferedImage readImage(InputStream ip) throws IOException {
        ImageReader reader;
        ImageInputStream iis = ImageIO.createImageInputStream(ip);
        reader = (ImageReader) ImageIO.getImageReaders(iis).next();
        reader.setInput(iis);
        BufferedImage inputImage = reader.read(0);
        iis.close();
        ip.close();

        return inputImage;
    }

    /**
     * Scales an image according to spec and writes in requested format format to the output. If the image already fits in the supplied rectangle then no scaling is done. An image is always written to
     * op in any case and op is closed after that.
     * @param inputImage Input Image to be scaled
     * @param op Output to write the scaled image
     * @param maxWidth Maximum width of scaled image
     * @param maxHeight Maximum height of scaled image
     * @param scaleOption One of: SCALE_INSIDE: Aspect Ratio maintained and target image scaled to fit within passed dimensions SCALE_OUTSIDE: Aspect Ration maintained and target image scaled so that
     *            dimensions fit in scaled image SCALE_FIT: Aspect Ratio discarded and image scaled to fit SCALE_CROP: After SCALE_OUTSIDE, the excess portion of scaled image is cropped from both ends
     *            of longer side so that the scaled image matches the passed dimensions.
     * @param compressionQuality A quality between 0.0 and 1.0 and -1 for default quality
     * @param outFileSuffix Specifies Target Image File format
     * @exception IOException if there is an IO Exception during processing
     * @return true if the input image was scaled, false otherwise
     */
    public static boolean scaleImage(BufferedImage inputImage, OutputStream op, int maxWidth, int maxHeight, ScaleOption scaleOption, float compressionQuality, String outFileSuffix)
            throws IOException {
        if (scaleOption == null) {
            scaleOption = ScaleOption.SCALE_INSIDE;
        }

        // 1. Compute scaling factor so that image fits within maxWidth,
        // maxHeight
        int width = inputImage.getWidth();
        int height = inputImage.getHeight();
        double scale = maxWidth / (double) width;
        double heightScale = maxHeight / (double) height;
        if (scaleOption == ScaleOption.SCALE_INSIDE) {
            if (heightScale < scale) {
                scale = heightScale;
            }
        } else if (scaleOption != ScaleOption.SCALE_FIT) {
            if (heightScale > scale) {
                scale = heightScale;
            }
        }

        boolean requiresScaling = false;

        if (scale < 1 || scaleOption == ScaleOption.SCALE_FIT && heightScale < 1) {
            requiresScaling = true;
        }

        // We are only going to shrink the input image and not expand it
        if (scaleOption != ScaleOption.SCALE_FIT && scale > 1) {
            scale = 1;
        }

        BufferedImage scaledImage = inputImage;
        int scaledWidth = width;
        int scaledHeight = height;
        if (requiresScaling) {
            // 2. Scale down the input image
            scaledWidth = maxWidth;
            scaledHeight = maxHeight;
            if (scaleOption != ScaleOption.SCALE_FIT) {
                scaledWidth = (int) Math.round(scale * width);
                scaledHeight = (int) Math.round(scale * height);
            }
            int imageType = inputImage.getType();
            if (imageType <= 0) {
                imageType = BufferedImage.TYPE_INT_RGB;
            }
            scaledImage = new BufferedImage(scaledWidth, scaledHeight, imageType);
            Graphics g = scaledImage.getGraphics();
            Image scaledImageInstance = inputImage.getScaledInstance(scaledWidth, scaledHeight, Image.SCALE_SMOOTH);
            g.drawImage(scaledImageInstance, 0, 0, null);
            scaledImageInstance = null; // Help GC
            inputImage = null; // Help GC
        }

        if (scaleOption == ScaleOption.SCALE_CROP && (scaledWidth > maxWidth || scaledHeight > maxHeight)) {
            int x = 0;
            int y = 0;
            int cropWidth = scaledWidth;
            int cropHeight = scaledHeight;
            if (scaledWidth > maxWidth) {
                x = (scaledWidth - maxWidth) / 2;
                cropWidth = maxWidth;
            }
            if (scaledHeight > maxHeight) {
                y = (scaledHeight - maxHeight) / 2;
                cropHeight = maxHeight;
            }
            scaledImage = scaledImage.getSubimage(x, y, cropWidth, cropHeight);
        }

        // 3. Save the scaled image
        ImageWriter writer = ImageIO.getImageWritersBySuffix(outFileSuffix).next();
        ImageWriteParam iwp = null;
        if (compressionQuality > 0) {
            iwp = writer.getDefaultWriteParam();
            iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            iwp.setCompressionQuality(compressionQuality);
        }
        ImageOutputStream ios = ImageIO.createImageOutputStream(op);
        writer.setOutput(ios);
        if (iwp != null) {
            IIOImage image = new IIOImage(scaledImage, null, null);
            writer.write(null, image, iwp);
        } else {
            writer.write(scaledImage);
        }
        ios.close();
        op.close();
        scaledImage = null; // Help GC

        return requiresScaling;
    }

    /**
     * Reads an image, scales it to fit in the given dimensions, preserving the aspect ratio and writes it in JPEG format to the output. If the image already fits in the supplied rectangle then no
     * scaling is done.
     * @param ip Input containing photo
     * @param op Output to write the scaled image
     * @param maxWidth Maximum width of scaled image
     * @param maxHeight Maximum height of scaled image
     * @param outFileSuffix Output File suffix indicating format of output file
     * @return True if image was scaled. False source image was copied as it is to target.
     * @exception IOException on IOException
     */
    public static boolean scaleImage(InputStream ip, OutputStream op, int maxWidth, int maxHeight, String outFileSuffix) throws IOException {
        BufferedImage inputImage = readImage(ip);
        return scaleImage(inputImage, op, maxWidth, maxHeight, ScaleOption.SCALE_INSIDE, (float) 0.75, outFileSuffix);
    }

    public static String getAffiliateString(HttpServletRequest request) {
        return String.format(AFFILIATE_LOG_FORMAT, getAffiliateId(request), getCompanyName(request), getPeopleId(request));
    }

    public static String getAffiliateString(int affiliateId, int userId, String companyName) {
        return String.format(AFFILIATE_LOG_FORMAT, affiliateId, companyName, userId);
    }

    private static int getAffiliateId(HttpServletRequest request) {
        int affiliateId = 0;
        SecurityBean secBean = (SecurityBean) request.getAttribute(SECURITY_BEAN);
        if (secBean != null) {
            // affiliateId = secBean.getAffiliateId();
            if (affiliateId == 0) {
                affiliateId = secBean.getCompanyId();
            }
        }

        return affiliateId;
    }

    private static int getPeopleId(HttpServletRequest request) {
        int pplId = 0;
        SecurityBean secBean = (SecurityBean) request.getAttribute(SECURITY_BEAN);
        if (secBean != null) {
            pplId = secBean.getPeopleId();
        }
        return pplId;
    }

    public static String getApiKey(HttpServletRequest request) {
        String apiKey = "";
        SecurityBean secBean = (SecurityBean) request.getAttribute(SECURITY_BEAN);
        if (secBean != null) {
            apiKey = secBean.getApiKey();
        }
        return apiKey;
    }

    private static String getCompanyName(HttpServletRequest request) {
        String companyName = null;
        SecurityBean secBean = (SecurityBean) request.getAttribute(SECURITY_BEAN);
        if (secBean != null) {
            companyName = secBean.getCompanyName();
        }
        return companyName;
    }

    public static Date xmlGregToDate(XMLGregorianCalendar xmlGregorianCalendar) {
        Date date = null;
        if (xmlGregorianCalendar != null) {
            date = xmlGregorianCalendar.toGregorianCalendar().getTime();
        }

        return date;
    }

    /*
     * following method return list of all the dates falling between startdate & enddate, including only the startdate.
     * @param date startDate starting point.
     * @param date endDate end point doesn't included in the list.
     */
    public static List<Date> dateList(Date startDate, Date endDate) {
        List<Date> list = new ArrayList<Date>();
        Calendar start = new GregorianCalendar();
        start.setTime(startDate);
        Calendar end = new GregorianCalendar();
        end.setTime(endDate);
        int index = 0;
        while (start.before(end)) {
            list.add(index, start.getTime());
            start.add(Calendar.DAY_OF_YEAR, 1);
            index++;
        }
        return list;
    }

    public static DatatypeFactory getDatatypeFactory() throws DatatypeConfigurationException {
        DatatypeFactory dtf = datatypeFactory;
        if (dtf == null) {
            synchronized (GenUtil.class) {
                dtf = datatypeFactory;
                if (dtf == null) {
                    dtf = DatatypeFactory.newInstance();
                    datatypeFactory = dtf;
                }
            }
        }
        return dtf;
    }

    /**
     * Converts date to XmlGregorianCalendar.
     * @param date Date to convert. If null the date in c will be used
     * @param c Calendar object to use to call setTime. If null a new one will be created.
     * @return Converted XMLGregorianCalendar Obj
     */
    public static XMLGregorianCalendar dateToXmlGreg(Date date, GregorianCalendar c) {
        if (c == null) {
            c = new GregorianCalendar();
        }
        if (date != null) {
            c.setTime(date);
        }
        XMLGregorianCalendar xmlGregorianCalendar = null;
        try {
            xmlGregorianCalendar = getDatatypeFactory().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException e) {
            logger.error("Error Converting date to XmlGreg", e);
        }

        return xmlGregorianCalendar;
    }

    public static XMLGregorianCalendar dateToXmlGreg(Date date) {
        return dateToXmlGreg(date, null);
    }

    public static void setUiVariables(Map<String, String> properties, HttpServletRequest httpReq) {
        for (String key : properties.keySet()) {
            if (key.indexOf('.') < 0 || key.startsWith("ct_") || Character.isUpperCase(key.charAt(0))) {
                String value = properties.get(key);
                if (key.equals("ct_ui_webhost") && StringUtils.isEmpty(value)) {
                    try {
                        URL requestUrl = new URL(httpReq.getRequestURL().toString());
                        int port = requestUrl.getPort();
                        value = requestUrl.getHost();
                        if (port > 0) {
                            value = value + ':' + port;
                        }
                    } catch (Exception e) {
                        value = "www.cleartrip.com";
                        logger.error("Error in getting ct_hotel_cthost from Request URL, defaulting to www.cleartrip.com.", e);
                    }
                }
                httpReq.setAttribute(key, value);
            }
        }
    }

    /**
     * Rounds to given precision or returns same number if precision < 0.
     * @param val value to round
     * @param precision precision
     * @return rounded value
     */
    public static double roundToPrecision(double val, int precision) {
        double roundedVal = val;
        if (precision >= 0) {
            int i;
            double factor = 1;
            for (i = precision; i > 0; i--) {
                factor *= 10;
            }
            roundedVal = Math.round(val * factor) / factor;
        }
        return roundedVal;
    }

    public static String nullifyBlankPhone(String phone) {
        String retPhone = null;
        if (phone != null) {
            phone = phone.trim();
            int len = phone.length();
            if (len >= 3) {
                // Validate atleast 3 digits in phone
                int i = 0, numDigits = 0;
                do {
                    if (Character.isDigit(phone.charAt(i))) {
                        numDigits++;
                        if (numDigits >= 3) {
                            retPhone = phone;
                            break;
                        }
                    }
                    i++;
                } while (i < len);
            }
        }

        return retPhone;
    }

    public static String getRemoteAddr(HttpServletRequest request) {
        // If Akamai Header present use that
        String remoteIpAddress = request.getHeader("true-client-ip");

        if (remoteIpAddress == null) {

            String remoteAddresses = request.getHeader("ns-remote-addr");
            if (remoteAddresses != null) {
                String[] remoteIpAddresses = remoteAddresses.split(",");
                remoteIpAddress = remoteIpAddresses[0];
            }

            String forwardedIpAddress = request.getHeader("x-forwarded-for");
            int pos = 0;
            if (forwardedIpAddress != null) {
                pos = forwardedIpAddress.indexOf(',');
            }
            if (pos > 0) {
                forwardedIpAddress = forwardedIpAddress.substring(0, pos);
            }
            if (remoteIpAddress == null || pos > 0) {
                // If a comma is present in forwardedIpAddress that is given
                // preference since it
                // would be forwarded by a proxy server
                remoteIpAddress = forwardedIpAddress;
            }
            if (remoteIpAddress == null) {
                remoteIpAddress = request.getRemoteAddr();
            }
        }

        if (remoteIpAddress != null) {
            remoteIpAddress = remoteIpAddress.trim();
        }

        return remoteIpAddress;
    }

    public static String nullifyLiteralNull(String s) {
        if (s != null) {
            s = s.trim();
            if (s.length() == 0 || "null".equalsIgnoreCase(s) || "(null)".equalsIgnoreCase(s)) {
                s = null;
            }
        }

        return s;
    }

    /**
     * Validates the XML passed as <code>source</code> against the <code>schema</code>.
     * @param schema Schema
     * @param source Xml
     * @throws SAXException If the document is not valid
     */
    public static void validateXml(Schema schema, Source source) throws SAXException {
        Validator validator = schema.newValidator();

        try {
            validator.validate(source);
        } catch (IOException e) {
            logger.error("Exception occured while validating XML", e);
        }
    }

    private static final Pattern cardPattern = Pattern.compile("([^0-9.]\\d{6})\\d{6}(\\d{3,4})", Pattern.CASE_INSENSITIVE);

    private static final Pattern xmlCardPattern = Pattern.compile("([<:]card-detail.*card-number.*>\\s*\\d{6})\\d{6}(\\d{3,4})", Pattern.DOTALL);

    private static final Pattern updatedXMLCardPattern = Pattern.compile("([<:]card-detail.*card-number.*>)(\\s*\\n*)(\\d)(\\d{6})(\\d*)(\\s*\\n*)(</.*card-number.*card-detail)", Pattern.DOTALL);
    private static final Pattern cvvPattern = Pattern.compile("((cvv|cardId)[^0-9,]*)\\d{3,4}\\b", Pattern.CASE_INSENSITIVE);

    private static final Pattern cvvOnlyPattern = Pattern.compile(">\\s*\\d{3,4}\\s*<", Pattern.CASE_INSENSITIVE);

    private static final Pattern multipleCardPattern = Pattern.compile("([<:]\\s*\\n*card-number\\s*\\n*>)(\\s*\\n*)(\\d)(\\d{6})(\\d*)(\\s*\\n*)(\\s*\\n*<\\s*\\n*/\\s*\\n*card-number>)",
            Pattern.DOTALL);

    private static final int DEFAULT_NONCE_LENGTH = 8;

    public static String hideCardInfo(String s) {
        return hideCardInfo(null, s);
    }

    public static String hideXmlCardInfo(String xml) {
        // return hideCardInfo("xml", xml);
        Pattern cardPatternToUse = updatedXMLCardPattern;
        String replacedStr = cardPatternToUse.matcher(xml).replaceAll("$1$2$3XXXXXX$5$6$7");
        // This line is not required since only CC/DC should be masked. GC should not be masked!! So commenting the following line
        /*
         * replacedStr = multipleCardPattern.matcher(replacedStr).replaceAll( "$1$2$3XXXXXX$5$6$7");
         */
        replacedStr = cvvPattern.matcher(replacedStr).replaceAll("$1XXX");
        return replacedStr;
    }

    public static String hideCardInfo(String paramName, String s) {
        String replacedStr = s;
        boolean isValue = false;
        // IndexOf("payment") is added as a precheck to reduce calls to
        // replaceAll
        // Which use regular expressions
        if (s != null && s.contains("ns1:bookreservation")) {
            return s;
        }
        if (s != null && (paramName == null || "xml".equals(paramName) || paramName.indexOf("payment") >= 0 || paramName.indexOf("card") >= 0 || paramName.indexOf("Card") >= 0
                || paramName.indexOf("CARD") >= 0 || paramName.indexOf("cvv") >= 0 || paramName.indexOf("Cvv") >= 0 || paramName.indexOf("CVV") >= 0)) {
            Pattern cardPatternToUse;
            if ("xml".equals(paramName)) {
                cardPatternToUse = xmlCardPattern;
            } else {
                if (paramName != null) {
                    isValue = true;
                    // If it is a value and not xml will fake it by prefixing the param
                    // So that s matches the pattern and remove the param prefix later
                    s = paramName + s;
                }
                cardPatternToUse = cardPattern;
            }
            replacedStr = cardPatternToUse.matcher(s).replaceAll("$1XXXXXX$2");
            replacedStr = cvvPattern.matcher(replacedStr).replaceAll("$1XXX");
        }
        if (isValue) {
            // Remove Added prefix
            replacedStr = replacedStr.substring(paramName.length());
        }

        return replacedStr;
    }

    /**
     * Reads the contents of the file and return it as string.
     * @param path file path
     * @return contents of the file
     */
    public static String getFileContents(String path) {
        StringBuilder sb = BaseStats.tmpStringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(path));
            String str;
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
            in.close();
        } catch (IOException e) {
            logger.error("Error while reading file contents", e);
        }
        String contents = sb.toString();
        sb = BaseStats.returnTmpStringBuilder(sb);
        return contents;
    }

    public static void putFileContents(byte[] contents, String path) {
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            fos = new FileOutputStream(path);
            bos = new BufferedOutputStream(fos);
            bos.write(contents);
        } catch (FileNotFoundException fnfe) {
            logger.error("Exception writing file to location : " + path, fnfe);
        } catch (IOException ioe) {
            logger.error("Exception writing file to location : " + path, ioe);
        } finally {
            if (bos != null) {
                try {
                    bos.flush();
                    bos.close();
                } catch (Exception e) {
                    fos = fos;
                }
            }
        }
    }

    public static byte[] getBytes(Object object) throws IOException {
        ReusableByteArrayOutputStream bStream = BaseStats.tmpReusableByteArrayOutputStream();
        byte[] bytes = null;
        try {
            ObjectOutputStream oStream = new ObjectOutputStream(bStream);
            oStream.writeObject(object);
            bytes = bStream.toByteArray();
        } finally {
            BaseStats.returnTmpReusableByteArrayOutputStream(bStream);
        }
        return bytes;
    }

    public static Object getObject(byte[] bytes) throws Exception {
        ByteArrayInputStream b = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream(b);
        Object object = ois.readObject();
        return object;
    }

    public static String getFullHttpURL(String relativeUrl, HttpServletRequest request) throws MalformedURLException {
        String host = SecurityUtil.getHost(request);

        String url = "http://" + host + relativeUrl;

        return url;
    }

    public static String getFullURL(String relativeUrl, CachedProperties cachedProperties, HttpServletRequest request, boolean removeContextPath, String useProtocol) throws MalformedURLException {

        return getFullURL(relativeUrl, cachedProperties, request, removeContextPath, useProtocol, null);
    }

    public static String getFullURL(String relativeUrl, CachedProperties cachedProperties, HttpServletRequest request, boolean removeContextPath, String useProtocol, String actualUrl)
            throws MalformedURLException {
        String protocol = useProtocol;
        if (protocol == null) {
            protocol = cachedProperties.getPropertyValue("ct_ui_https", "https");
        }

        StringBuilder sb = BaseStats.tmpStringBuilder();
        sb.append("http".equals(protocol) ? "http://" : "https://");
        URL requestUrl = (actualUrl == null) ? new URL(request.getRequestURL().toString()) : new URL(actualUrl);
        int port = requestUrl.getPort();
        String host = requestUrl.getHost();

        sb.append(host);
        if (port > 0) {
            sb.append(':').append(port);
        }

        String path = relativeUrl;
        if (relativeUrl != null && relativeUrl.charAt(0) != '/') {
            path = requestUrl.getPath();
            int pos = path.lastIndexOf('/');
            path = path.substring(0, pos + 1);
        }

        if (removeContextPath) {
            String contextPrefix = request.getContextPath() + '/';
            path = path.replace(contextPrefix, "/");

            if (path.contains("/mobile")) {
                path = path.replace("/mobile", "/m");
            }
            if (path.contains("/preferred-suppliers")) {
                path = path.replace("/preferred-suppliers", "/hotels");
            }
        }
        if (path.contains("/flights")) {
            String langPrefCookieName = cachedProperties.getPropertyValue(PROP_LANG_PREF_COOKIE_NAME, LP);
            Cookie langPrefCookie = getCookieFromRequest(request, langPrefCookieName);
            if (langPrefCookie != null && null != langPrefCookie.getValue() && !langPrefCookie.getValue().equalsIgnoreCase("en")) {
                sb.append('/').append(langPrefCookie.getValue());
            }
        }

        sb.append(path);
        if (!path.equals(relativeUrl)) {
            sb.append(relativeUrl);
        }

        String url = sb.toString();
        sb = BaseStats.returnTmpStringBuilder(sb);

        return url;
    }

    public static String getFullURL(String relativeUrl, CachedProperties cachedProperties, HttpServletRequest request, boolean removeContextPath) throws MalformedURLException {
        return getFullURL(relativeUrl, cachedProperties, request, removeContextPath, null, null);
    }

    /**
     * Gets the the requested cookie from request.
     * @param httpReq HttpServletRequest
     * @param cookieName String
     * @return Cookie
     */
    public static Cookie getCookieFromRequest(HttpServletRequest httpReq, String cookieName) {

        Cookie[] cookies = httpReq.getCookies();
        Cookie authCookie = null;
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equalsIgnoreCase(cookieName)) {
                    authCookie = cookies[i];
                    break;
                }
            }
        }
        return authCookie;
    }

    /**
     * Gets truncated date without time fields similar to Oracle's trunc(date). Works with time in millis to keep it more flexible and avoid unnecessary creation of Date Objects. To truncate a Date
     * Object Date truncDate = new Date(getTruncatedDateMillis(orgDate.getTime(), null)) To truncate current date: Date todaysDate = new Date(getTruncatedDateMillis(System.getCurrentTimeMillis(),
     * null))
     * @param timeInMillis Milliseconds of date to truncate
     * @param tmpCal Tmp Calendar Object to use. Pass null if not available.
     * @return Milliseconds time corresponding to truncated date
     */
    public static long getTruncatedDateMillis(long timeInMillis, Calendar tmpCal) {
        if (tmpCal == null) {
            tmpCal = Calendar.getInstance();
        }
        tmpCal.setTimeInMillis(timeInMillis);
        int year = tmpCal.get(Calendar.YEAR);
        int month = tmpCal.get(Calendar.MONTH);
        int date = tmpCal.get(Calendar.DATE);
        tmpCal.clear();
        tmpCal.set(year, month, date);

        return tmpCal.getTimeInMillis();
    }

    public static String readAsStringString(InputStream ip) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(ip));
        StringBuilder sb = BaseStats.tmpStringBuilder();
        String line = null;

        while ((line = reader.readLine()) != null) {
            sb.append(line).append('\n');
        }

        String s = sb.toString();
        sb = BaseStats.returnTmpStringBuilder(sb);
        return s;
    }

    public static boolean equals(Object o1, Object o2) {
        boolean isEqual = (o1 == o2);
        if (!isEqual && o1 != null && o2 != null) {
            isEqual = o1.equals(o2);
        }

        return isEqual;
    }

    /**
     * Finds the time difference between two date object in minutes.
     * @param depDate Date
     * @param now Date
     * @return Time diff in minutes
     */
    public static long getTimeDiffInMinutes(Date depDate, Date now) {
        long timeDiffInMinutes = 0;
        // e.g. departure flight is today at 6:00 AM and you r searching at
        // 10.00 AM today,
        // then 6:00 AM time - 10:00 AM time will return minus value.
        timeDiffInMinutes = (depDate.getTime() - now.getTime());
        timeDiffInMinutes = (timeDiffInMinutes / (60 * 1000));
        return timeDiffInMinutes;
    }

    /**
     * Finds the time difference between two date object in hours.
     * @param depDate Date
     * @param now Date
     * @return Time diff in hours
     */
    public static long getTimeDiffInHrs(Date depDate, Date now) {
        long timeDiffInMinutes = getTimeDiffInMinutes(depDate, now);
        long timeDiffInHrs = timeDiffInMinutes / 60;
        return timeDiffInHrs;
    }

    /**
     * Find the time difference between two date object in years.
     * @param toDate From Date
     * @param fromDate To Date
     * @return No of years between two dates
     */
    public static long getTimeDiffInYears(Date toDate, Date fromDate) {
        long hours = getTimeDiffInHrs(toDate, fromDate);
        hours = hours / (24 * 365);
        return hours;
    }

    /**
     * Creates TimeZone for values like 6.5/-6.
     * @param offset TimeZone offset
     * @return TimeZone
     */
    public static TimeZone getTimeZone(double offset) {
        String id = "GMT";

        if (offset != 0) {
            String integerStr = null;
            String fractionStr = null;
            double integer = 0;
            double fraction = 0;

            if (offset > 0) {
                integer = Math.floor(offset);
                fraction = offset - integer;
            } else {
                integer = Math.ceil(offset);
                fraction = offset - integer;

                integer = Math.abs(integer);
                fraction = Math.abs(fraction);
            }

            fraction = fraction * 60;

            integerStr = String.valueOf((int) integer);

            if (integerStr.length() == 1) {
                integerStr = "0" + integerStr;
            }

            fractionStr = String.valueOf((int) fraction);

            if (fractionStr.length() == 1) {
                fractionStr = "0" + fractionStr;
            }

            String sign = (offset > 0) ? "+" : "-";

            StringBuilder sb = BaseStats.tmpStringBuilder();
            id = sb.append(id).append(sign).append(integerStr).append(":").append(fractionStr).toString();

            sb = BaseStats.returnTmpStringBuilder(sb);
        }

        TimeZone timeZone = TimeZone.getTimeZone(id);
        return timeZone;
    }

    public static String formatTimeZone(TimeZone t) {
        String s = null;
        if (t != null) {
            long offset = t.getRawOffset();
            // SHOULD NOT use BaseStats.tmpStringBuilder as this will be called
            // from within megeTemplateToString which already uses it.
            StringBuilder sb = new StringBuilder(8);
            if (offset < 0) {
                sb.append('-');
                offset = -offset;
            } else {
                sb.append('+');
            }
            offset /= 60000; // Convert to minutes
            int hours = (int) offset / 60;
            if (hours < 10) {
                sb.append('0');
            }
            sb.append(hours).append(':');
            int mins = (int) offset % 60;
            if (mins < 10) {
                sb.append('0');
            }
            sb.append(mins);
            s = sb.toString();
        }

        return s;
    }

    public static Date[] getDateAndTime(Date originalDate) {
        Date date = extractDateFromDateTime(originalDate);
        Date time = extractTimeFromDateTime(originalDate);
        return new Date[] {date, time};
    }

    public static Date extractTimeFromDateTime(Date originalDate) {
        Date time = null;
        try {
            Calendar calendarForTime = Calendar.getInstance();
            calendarForTime.setTime(originalDate);
            calendarForTime.set(Calendar.DATE, 1);
            calendarForTime.set(Calendar.MONTH, 0);
            calendarForTime.set(Calendar.YEAR, 1970);
            time = calendarForTime.getTime();
        } catch (Exception e) {
            logger.error("Error occurred while extracting time from dateTime", e);
        }
        return time;
    }

    public static Date extractDateFromDateTime(Date originalDate) {
        Date date = null;
        try {
            Calendar calendarForDate = Calendar.getInstance();
            calendarForDate.setTime(originalDate);
            calendarForDate.set(Calendar.HOUR_OF_DAY, 0);
            calendarForDate.set(Calendar.MINUTE, 0);
            calendarForDate.set(Calendar.SECOND, 0);
            calendarForDate.set(Calendar.MILLISECOND, 0);
            date = calendarForDate.getTime();
        } catch (Exception e) {
            logger.error("Error occurred while extracting date from dateTime", e);
        }
        return date;
    }

    public static boolean getBooleanParameter(HttpServletRequest request, String name, boolean defaultValue) {
        boolean value = defaultValue;
        if (request != null && name != null) {
            String param = request.getParameter(name);
            if (param != null) {
                value = "1".equals(param) || "true".equalsIgnoreCase(param);
            }
        }

        return value;
    }

    public static <K extends Enum<K>> K getEnumParameter(HttpServletRequest request, String name, CachedProperties cachedProperties, String propertyName, K defaultValue, Class<K> enumClass) {
        K value = null;
        if (request != null && name != null) {
            String param = request.getParameter(name);
            if (param != null) {
                try {
                    value = K.valueOf(enumClass, param);
                } catch (Exception e) {
                    logger.error(
                            "Error Converting Request ParamName " + name + " value " + param + " to enum " + enumClass + ". Will use default value from property or passed default " + defaultValue, e);
                }
            }
            if (value == null) {
                if (cachedProperties != null) {
                    value = cachedProperties.getEnumPropertyValue(propertyName, defaultValue, enumClass);
                } else {
                    value = defaultValue;
                }
            }
        }

        return value;
    }

    public static <K extends Enum<K>> K getEnumParameter(HttpServletRequest request, String name, K defaultValue, Class<K> enumClass) {
        return getEnumParameter(request, name, null, null, defaultValue, enumClass);
    }

    /**
     * Returns the stack trace as string.
     * @param e Exception
     * @return String
     */
    public static String getStackTrace(Exception e) {
        String stackTrace = null;
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        logger.error("Exception is:" + e.getMessage(), e);
        stackTrace = sw.toString();
        return stackTrace;
    }

    public static byte[] digestBytes(byte[] b, int offset, int len) throws NoSuchAlgorithmException {
        byte[] hashedArr = new byte[32];
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(b, offset, len);
        hashedArr = md.digest();
        return hashedArr;
    }

    public static String digestBytes(byte[] b, int offset, int len, boolean isHex) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String digestedString = null;

        byte[] hashedArr = digestBytes(b, offset, len);

        if (isHex) {
            digestedString = new String(Hex.encodeHex(hashedArr));
        } else {
            byte[] encodedBytes = Base64.encodeBase64(hashedArr);
            digestedString = new String(encodedBytes);
        }

        return digestedString;
    }

    public static String sha1sum(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] b = input.getBytes(UTF_8_CHARSET);
        return digestBytes(b, 0, b.length, true);
    }

    public static String digestBytes(byte[] b) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return digestBytes(b, 0, b.length, false);
    }

    public static String digestString(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return digestBytes(input.getBytes(UTF_8_CHARSET));
    }

    /*
     * Returns the hex representation of hashed input string.
     */
    public static String getSha256Of(String input) {
        if (StringUtils.isBlank(input)) {
            return input;
        }
        try {
            MessageDigest encryptionAlgorithm = MessageDigest.getInstance("SHA-256");
            byte[] inputAsBytes = input.getBytes(UTF_8_CHARSET);
            for (int i = 0; i < 200; i++) {
                inputAsBytes = encryptionAlgorithm.digest(inputAsBytes);
            }
            return asHex(inputAsBytes);
        } catch (Exception e) {
            throw new UnsupportedOperationException();
        }
    }

    public static boolean isTomcatRequest(HttpServletRequest request) {
        boolean isTomcatRequest = false;
        String hostHeader = request.getHeader("host");
        if (hostHeader != null && hostHeader.indexOf(':') > 0) {
            isTomcatRequest = true;
        }

        return isTomcatRequest;
    }

    /**
     * Gets Request Headers as Map. Note that Header Names as case insensitive and java converts them to lowercase.
     * @param request Servlet Request Object
     * @param convertToUpperCase Whether to convert header Name keys to upper case. Otherwise keys are left as it is which means they are ususally in lowerCase
     * @return Request Headers
     */
    public static Map<String, String> getRequestHeaders(HttpServletRequest request, boolean convertToUpperCase) {
        Map<String, String> headers = new HashMap<String, String>();
        for (Enumeration<String> headerNames = request.getHeaderNames(); headerNames.hasMoreElements();) {
            String headerName = headerNames.nextElement();
            if (convertToUpperCase) {
                headerName = headerName.toUpperCase();
            }
            String header = request.getHeader(headerName);
            if (header != null) {
                headers.put(headerName, header);
            }
        }

        return headers;
    }

    public static Map<String, String> getRequestParameters(HttpServletRequest request) {
        Map<String, String> params = new HashMap<String, String>();
        for (Enumeration<String> paramNames = request.getParameterNames(); paramNames.hasMoreElements();) {
            String paramName = paramNames.nextElement();
            params.put(paramName, request.getParameter(paramName));
        }

        return params;
    }

    public static String toTitleCase(String inStr) {
        String outStr = new String();
        if (StringUtils.isNotBlank(inStr)) {
            if (inStr.contains(" ")) {
                String[] sb = inStr.split(" ");
                for (int i = 0; i < sb.length; i++) {
                    if (i > 0) {
                        outStr = outStr + " ";
                    }
                    if (sb[i].length() > 1) {
                        outStr = outStr + sb[i].substring(0, 1).toUpperCase() + sb[i].substring(1).toLowerCase();
                    } else {
                        outStr = outStr + sb[i];
                    }
                }
            } else {
                if (inStr.length() > 1) {
                    outStr = inStr.substring(0, 1).toUpperCase() + inStr.substring(1).toLowerCase();
                } else {
                    outStr = inStr;
                }
            }
        }
        return outStr;
    }

    // Utility to trim a list to its actual size, useful for jvm caching. Java 7 has a ArrayList.trimToSize method
    public static <E> List<E> trimListToSize(List<E> l) {
        if (l != null) {
            l = new ArrayList<E>(l);
        }

        return l;
    }

    // Returns a sorted copy of c
    public static <T extends Comparable<? super T>> List<T> sort(Collection<T> c) {
        List<T> l = null;
        if (c != null) {
            l = new ArrayList<T>(c);
            Collections.sort(l);
        }

        return l;
    }

    /**
     * Returns the country supported along with their respective domain addresses as the values of the country key in the Map.
     * @param cachedProperties Cached Properties
     * @return Map<String,String> Supported Countries Map
     */
    public static Map<String, Map<String, String>> getSupportedCountryListMap(CachedProperties cachedProperties) {
        Map<String, Map<String, String>> countryList = new LinkedHashMap<String, Map<String, String>>();
        String json = cachedProperties.getPropertyValue("ct.services.supported.countries.list");
        if (json != null) {
            ObjectMapper objMapper = JsonUtil.getObjectMapper();
            try {
                countryList = (Map<String, Map<String, String>>) objMapper.readValue(json, Map.class);
            } catch (Exception e) {
                logger.error("Error retrieving the supported countries property.", e);
                countryList = null;
            }
        }

        return countryList;
    }

    /**
     * Converts the amount from one currency to other currency.
     * @param fromCurrency From Currency
     * @param toCurrency To Currency
     * @param amount Amount to convert
     * @param cachedCurrencyConversionJson Currency Conversion table as cached resource
     * @return Converted currency
     */
    public static double convertCurrency(String fromCurrency, String toCurrency, double amount, CachedCurrencyConversionJson cachedCurrencyConversionJson) {
        double conversionRate = 1;

        if (!StringUtils.isEmpty(fromCurrency) && !StringUtils.isEmpty(toCurrency) && !fromCurrency.equalsIgnoreCase(toCurrency)) {
            Map<String, Double> currencyConversion = cachedCurrencyConversionJson.getResourceValue();

            if ("INR".equalsIgnoreCase(toCurrency)) {
                double rate = currencyConversion.get(fromCurrency);
                conversionRate = rate;
            } else if ("INR".equalsIgnoreCase(fromCurrency)) {
                double rate = currencyConversion.get(toCurrency);
                conversionRate = 1 / rate;
            } else {
                double rate1 = currencyConversion.get(fromCurrency);
                double rate2 = currencyConversion.get(toCurrency);
                conversionRate = rate1 / rate2;
            }
        }

        return amount * conversionRate;
    }

    public static double getCurrencyConversionRate(String fromCurrency, String toCurrency, CachedCurrencyConversionJson cachedCurrencyConversionJson) {
        double conversionRate = 1;

        if (!StringUtils.isEmpty(fromCurrency) && !StringUtils.isEmpty(toCurrency) && !fromCurrency.equalsIgnoreCase(toCurrency)) {
            Map<String, Double> currencyConversion = cachedCurrencyConversionJson.getResourceValue();

            if ("INR".equalsIgnoreCase(toCurrency)) {
                double rate = currencyConversion.get(fromCurrency);
                conversionRate = rate;
            } else if ("INR".equalsIgnoreCase(fromCurrency)) {
                double rate = currencyConversion.get(toCurrency);
                conversionRate = 1 / rate;
            } else {
                double rate1 = currencyConversion.get(fromCurrency);
                double rate2 = currencyConversion.get(toCurrency);
                conversionRate = rate1 / rate2;
            }
        }

        return conversionRate;
    }

    public static Double formatDoubleToTwoDecimal(Double doubleValue) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        String szDouble = twoDForm.format(doubleValue);
        doubleValue = Double.valueOf(szDouble);
        return doubleValue;
    }

    public static int compare(Comparable com1, Comparable com2) {
        if (com1 == null && com2 == null) {
            return 0;
        }
        if (com1 == null) {
            return -1;
        }
        if (com2 == null) {
            return 1;
        }
        return com1.compareTo(com2);
    }

    public static boolean sameCollections(Collection<? extends Object> col1, Collection<? extends Object> col2, boolean compareNullAndEmpty) {
        if (compareNullAndEmpty) {
            if (col1 != null && col2 == null) {
                if (col1.size() == 0) {
                    return true;
                } else {
                    return sameCollections(col1, col2);
                }
            } else if (col1 == null && col2 != null) {
                if (col2.size() == 0) {
                    return true;
                } else {
                    return sameCollections(col1, col2);
                }
            } else {
                return sameCollections(col1, col2);
            }
        } else {
            return sameCollections(col1, col2);
        }

    }

    public static boolean sameCollections(Collection<? extends Object> col1, Collection<? extends Object> col2) {
        if (col1 == col2) {
            return true;
        }
        if (col1 == null || col2 == null) {
            return false;
        }
        if (col1.size() != col2.size()) {
            return false;
        }
        if (!col1.containsAll(col2)) {
            return false;
        }
        if (!col2.containsAll(col1)) {
            return false;
        }
        return true;
    }

    public static boolean sameMaps(Map map1, Map map2) {
        if (map1 == map2) {
            return true;
        }
        if (map1 == null || map2 == null) {
            return false;
        }
        if (map1.size() != map2.size()) {
            return false;
        }
        Set keys = map1.keySet();
        try {
            for (Object key : keys) {
                Object val1 = map1.get(key);
                Object val2 = map2.get(key);
                if (val1 == null && val2 == null) {
                    continue;
                }
                if (val1 == null || val2 == null) {
                    return false;
                }
                if (val1 instanceof Comparable && val2 instanceof Comparable) {
                    if (compare((Comparable) val1, (Comparable) val2) != 0) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } catch (ClassCastException e) {
            return false;
        }
        return true;
    }

    /**
     * This method returns a map containing all the headers and cookies starting with "r_". All the headers will be prepended with "r_"
     * @param request Request
     * @return Map containing r_ headers and cookies
     */
    public static Map<String, String> getRequestHeaders(HttpServletRequest request) {
        Enumeration headerNames = request.getHeaderNames();
        Map<String, String> requestHeaderMap = new HashMap<String, String>();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            String headervalue = request.getHeader(headerName);
            String requestHeaderName = headerName;
            boolean remoteHeader = true;
            if (!requestHeaderName.startsWith("r_")) {
                requestHeaderName = "r_" + requestHeaderName;
                remoteHeader = false;
            }
            // In case of conflicts give preference to remote header where the headerName itself starts with r_
            // This fix is added because r_user-agent gets replaced with the Web Service's java user-agent in case
            // of call to createHotelItinerary Web Service.
            if (remoteHeader || !requestHeaderMap.containsKey(requestHeaderName)) {
                requestHeaderMap.put(requestHeaderName, headervalue);
            }
        }

        Cookie[] cookies = request.getCookies();
        String cookieValue = null;
        try {
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    String cookieName = cookies[i].getName();
					if (cookieName.startsWith("r_") || StringUtils.equalsIgnoreCase(cookieName, LP)) {
                        cookieValue = URLDecoder.decode(cookies[i].getValue(), "UTF-8");
                        logger.info("COOKIE ==> " + cookieName + ":" + cookieValue);
                        if (cookieName.toLowerCase().contains("params")) {
                            List<String> paramList = Arrays.asList(cookieValue.split("&", -1));
                            for (String value : paramList) {
                                if (StringUtils.isNotEmpty(value) && value.indexOf(EQUALS_TO) > 0) {
                                    requestHeaderMap.put("r_" + value.substring(0, value.indexOf(EQUALS_TO)), value.substring(value.indexOf(EQUALS_TO) + 1));
                                }
                            }
                        } else {
                            requestHeaderMap.put(cookieName, cookieValue);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error:", e);
        }
        return requestHeaderMap;
    }

    public static Map<String, String> getBrowserRelatedRequesrHeaders(HttpServletRequest request) {
        Map<String, String> requestHeaderMap = new HashMap<String, String>();
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            String headerValue = request.getHeader(headerName);
            if (headerName.startsWith("r_") || StringUtils.equalsIgnoreCase(headerName, LP)) {
                requestHeaderMap.put(headerName, headerValue);
            }
            try {
            	Cookie[] cookies = request.getCookies();
            	String cookieValue = null;
            	if (cookies != null) {
            		for (int i = 0; i < cookies.length; i++) {
            			String cookieName = cookies[i].getName();
            			if (StringUtils.equalsIgnoreCase(cookieName, LP)) {
            				cookieValue = URLDecoder.decode(cookies[i].getValue(), "UTF-8");
            				requestHeaderMap.put(cookieName, cookieValue);
            			}
            		}
            	}
            } catch (Exception e) {
                logger.error("Error:", e);
            }
        }
        return requestHeaderMap;
    }

//    @SuppressWarnings("unchecked")
//    public static byte[] getUplodadedFile(MultipartHttpServletRequest multipartRequest) {
//        Collection<CommonsMultipartFile> uploadedFiles = multipartRequest.getFileMap().values();
//        CommonsMultipartFile file = null;
//        for (CommonsMultipartFile commonsMultipartFile : uploadedFiles) {
//            file = (CommonsMultipartFile) commonsMultipartFile;
//        }
//        byte[] vCardFile = file.getBytes();
//        return vCardFile;
//    }

    public static Map<String, String> getBrowserRelatedHeadersFromRequestHeaders(Map<String, String> requestHeaders) {
        Map<String, String> requestHeaderMap = new HashMap<String, String>();
        Set<String> keySet = requestHeaders.keySet();
        if (keySet != null) {
            for (String key : keySet) {
                if (key.startsWith("r_")) {
                    String value = requestHeaders.get(key);
                    requestHeaderMap.put(key, value);
                }
            }
        }
        return requestHeaderMap;
    }

    /**
     * Extracts the server-hostname from the request. Eliminates port numbers too.
     * @param request Servlet Request
     * @return Value of HTTP header HOST. Empty String if valid host header is not found.
     */
    public static String resolveHostname(HttpServletRequest request) {

        // Can never be null as per w3c.org, HTTP 1.1 specifications RFC 2616.
        // (http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)
        String hostname = request.getHeader("HOST");

        // ... but bad things do happen.
        if (hostname == null) {
            // HTTP HOST header can be null for crafted calls
            // such as sever-to-server crafted java calls.
            // In such a case, return empty string, but never return null.
            hostname = "";

            logger.warn("HOST is null for : " + request.getRequestURI() + ", below are the headers");

            try {
                Enumeration eNames = request.getHeaderNames();

                while (eNames.hasMoreElements()) {
                    String name = (String) eNames.nextElement();
                    String value = (String) request.getHeader(name);
                    logger.warn(name + ":" + value);
                }
            } catch (Exception e) {
                hostname = hostname;
            }

        }

        int portIndex = hostname.indexOf(":");
        if (portIndex > -1) {
            hostname = hostname.substring(0, portIndex);
        }

        return hostname;
    }

    /**
     * Set cookie domain to complete hostname for non cleartrip sites.
     * @param request Request
     * @return True if Cookie has to be set at .cleartrip.com domain level
     */
    public static boolean isSetCookieDomain(HttpServletRequest request) {

        String hostname = GenUtil.resolveHostname(request);

        // if not a cleartrip domain
        if (hostname.contains(".cleartrip.")) {
            return true;
        }

        return false;
    }

    /**
     * @author Gururaj Given a country (gets from HttpReq) returns the currency.
     * @param country Country
     * @return Selling Currency for Country
     */
    public static String getDomainSellingCurrency(String country) {
        Map<String, Map<String, String>> domainToCountryMap = SecurityUtil.getDomainToCountryMap();
        String sellingCurency = "INR";
        if (domainToCountryMap != null) {
            try {
                for (Entry<String, Map<String, String>> entry : domainToCountryMap.entrySet()) {
                    Map<String, String> domainCountryMap = entry.getValue();
                    String countryName = domainCountryMap.get("country");
                    if (countryName.equalsIgnoreCase(country)) {
                        sellingCurency = domainCountryMap.get("sellingCurrency");
                        break;
                    }
                }
            } catch (Exception e) {
                logger.error("Error while getting country currency information from rquest: ", e);
                logger.error("Domain Selling Currency is: " + sellingCurency);
                sellingCurency = "INR";
            }
        }
        return sellingCurency;
    }

    public static Object formatCsvField(Object field) {
        if (field instanceof String) {
            String str = (String) field;
            int index = str.indexOf(",");

            if (index == -1) {
                index = str.indexOf("\"");
            }

            field = str.replaceAll("\"", "\"\"");

            if (index != -1) {
                str = "\"" + str + "\"";
            }

            field = str;
        }

        return field;
    }

    public static Date getEOD(Date date) {
        if (date == null) {
            return date;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    public static Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> headerMap = new HashMap<String, String>();

        Enumeration<String> e = request.getHeaderNames();

        if (e != null) {
            while (e.hasMoreElements()) {
                String requestHeaderName = e.nextElement();
                headerMap.put(requestHeaderName, request.getHeader(requestHeaderName));
            }
        }

        return headerMap;

    }

    public static Map<String, String> getParameters(HttpServletRequest request) {
        Map<String, String> parameterMap = new HashMap<String, String>();

        Enumeration<String> e = request.getParameterNames();

        if (e != null) {

            while (e.hasMoreElements()) {
                String paramName = e.nextElement();
                parameterMap.put(paramName, request.getParameter(paramName));
            }
        }

        return parameterMap;
    }

    public static String getDomainNameFromCountryName(String country) {
        CachedProperties props = SecurityUtil.getProperties();
        String domainName = "cleartrip.in";
        Map<String, String> countryToDomainMap = null;
        String countryToDomainMapString = props.getPropertyValue("ct.country.to.domain.map.for.tripxml");
        ObjectMapper mapper = JsonUtil.getObjectMapper();

        try {
            countryToDomainMap = (Map<String, String>) mapper.readValue(countryToDomainMapString, Map.class);
            Map<String, String> domainCountryMap = countryToDomainMap;
            domainName = domainCountryMap.get(country.toUpperCase());
        } catch (Exception e) {
            logger.error("Exception while reading", e);
            return domainName;
        }

        return domainName;
    }
    @MethodExcludeCoverage
    public static String getDomainName(String sellingCountry, TxnSourceType txnSourceType, SourceType sourceType, String domain) {
    	String domainName = null;
    	if(txnSourceType == null && sourceType != null) {
    		switch(sourceType.name()) {
    		case "API":
    			txnSourceType = TxnSourceType.API;
    			break;
    		case "MOBILE":
    			txnSourceType = TxnSourceType.MOBILE;
    			break;
    		case "CORP":
    			txnSourceType = TxnSourceType.CORP;
    			break;
    		case "AGENCY":
    			txnSourceType = TxnSourceType.AGENCY;
    			break;
    		case "WL":
    			txnSourceType = TxnSourceType.WL;
    			break;
    		case "ACCOUNT":
    			txnSourceType = TxnSourceType.ACCOUNT;
    			break;
    		case "HQ":
    			txnSourceType = TxnSourceType.HQ;
    			break;
    		case "SYNC":
    			txnSourceType = TxnSourceType.SYNC;
    			break;
    		case "DEPOSIT_ACCOUNT":
    			txnSourceType = TxnSourceType.DEPOSIT_ACCOUNT;
    			break;
    		case "MOBILE_WL":
    			txnSourceType = TxnSourceType.MOBILE_WL;
    			break;
    		case "API_MOBILE":
    			txnSourceType = TxnSourceType.API_MOBILE;
    			break;
    	    }
    	}
    	if(StringUtils.isNotBlank(sellingCountry)) {
    		 if (txnSourceType == TxnSourceType.CORP || txnSourceType == TxnSourceType.AGENCY || txnSourceType == TxnSourceType.WL || txnSourceType == TxnSourceType.MOBILE
    	                || txnSourceType == TxnSourceType.MOBILE_WL) {
    	            String ctHloc = cachedProperties.getPropertyValue("ct.hloc", "www");
    	            if ((txnSourceType == TxnSourceType.MOBILE) && domain != null && domain.startsWith(ctHloc + ".")) {
    	                domainName = domain.substring(ctHloc.length() + 1);
    	            } else {
    	            	if(domain == null && txnSourceType == TxnSourceType.MOBILE) {
    	            		 if (StringUtils.isNotBlank(sellingCountry)) {
    	    	                    domainName = GenUtil.getDomainNameFromCountryName(sellingCountry);
    	    	                }
    	            	}
    	                if (domain == null && txnSourceType == TxnSourceType.CORP) {
    	                	domainName = "cleartripforbusiness." + ((sellingCountry.equals("IN")) ? "com" : sellingCountry.toLowerCase());
    	                } else {
    	                	domainName = domain;
    	                }
    	            }
    	        } else if (txnSourceType == TxnSourceType.API) {
    	            domainName = "api.cleartrip." + ((sellingCountry.equals("IN")) ? "com" : sellingCountry.toLowerCase());
    	        } else if (StringUtils.isNotBlank(sellingCountry)) {
    	            domainName = GenUtil.getDomainNameFromCountryName(sellingCountry);
    	        }
    	}
    	return domainName;
    }

    /**
     * To Force use Object's version of toString() for a big Map.
     * @param o Object
     * @return toString result
     */
    public static String toSring(Object o) {
        String s = "null";
        if (o != null) {
            s = o.getClass().getName() + "@" + Integer.toHexString(o.hashCode());
        }
        return s;
    }

    public static String getLocalIpAddress() {
        String ipAddress = null;
        try {
            InetAddress localMachine = InetAddress.getLocalHost();
            ipAddress = localMachine.getHostAddress();
            if (ipAddress != null && ipAddress.startsWith("127.0.0")) {
                // Go through all Interfaces and get
                for (Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
                    NetworkInterface iface = ifaces.nextElement();
                    for (Enumeration<InetAddress> ips = iface.getInetAddresses(); ips.hasMoreElements();) {
                        InetAddress ia = ips.nextElement();
                        if (!ia.isLoopbackAddress() && !ia.getHostAddress().contains(":")) {
                            ipAddress = ia.getHostAddress();
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error Getting IP Address", e);
        }

        return ipAddress;
    }

    public static String getEth0Address() {
        String ip = null;
        Enumeration<NetworkInterface> networkInterfaces;
        try {
            networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = (NetworkInterface) networkInterfaces.nextElement();
                if (networkInterface.getName().equals("eth0")) {
                    Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                    while (inetAddresses.hasMoreElements()) {
                        InetAddress inetAddress = (InetAddress) inetAddresses.nextElement();
                        if (!inetAddress.getHostAddress().contains(":")) {
                            ip = inetAddress.getHostAddress();
                            break;
                        }
                    }
                }
            }
        } catch (SocketException e) {
            logger.error("unable to get eth 0 address");
        }
        return ip;
    }

    /**
     * This is an adaptation of the Levenshtein Distance algorithm in http://en.wikipedia.org/wiki/Levenshtein_distance for Hotel Auto Complete. The modifications are: 1. We restrict distance
     * computation to maxLd. So if the distance is greater than maxLd the return value will be maxLd + 1 and not the exact distance. 2. We stop the match once the shorter string is matched, if so
     * required. So if s1 is a prefix of s2, the distance is 0. Please refer to the ".. less space" and "..threshold k" points in Possible Improvements section.
     * @param prefix prefix String if matchPrefix is true, just one of the strings otherwise
     * @param mainStr Main String
     * @param maxLd Max Distince interested in. Should be <= 126 since we use byte array to store distances.
     * @param offsetForPrefixMatch Offset to be added for prefix string matches. If the value after adding this offset is lesser than total distance this is returned. Pass 0 for full prefix matching
     *            and maxLd + 1 for no prefix matching or something in between to give lesser preference to prefix matches. For example MYIL and MYLADUDURAI will return 1 + offsetForPrefixMatch
     *            instead of 9 or maxLd
     * @param d Temp Distance Array of size atleast min(2 * maxLd, prefix.length(), mainStr.length()) + 1. If null is passed an array will be created internally.
     * @return Levenshtein Distance between prefix and mainStr if <= maxLd or maxLd + 1.
     */
    public static int getLevenshteinDistance(String prefix, String mainStr, int maxLd, int offsetForPrefixMatch, byte[] d) {
        if (prefix == null) {
            prefix = "";
        }
        if (mainStr == null) {
            mainStr = "";
        }
        int exceededLd = maxLd + 1;
        boolean matchPrefix = false;
        if (offsetForPrefixMatch <= maxLd) {
            matchPrefix = true;
        }
        boolean matchShorter = matchPrefix;
        boolean matchLonger = false;
        // Move s1, s2 to ls1 and ls2 so that ls2 is the smaller of the two if both are not equal.
        String ls1 = mainStr;
        String ls2 = prefix;
        int len1 = ls1.length();
        int len2 = ls2.length();
        int i, j;
        if (len1 < len2) {
            ls1 = prefix;
            ls2 = mainStr;
            i = len1;
            len1 = len2;
            len2 = i;
            matchShorter = false;
            matchLonger = matchPrefix;
        }
        int ld = exceededLd;
        int prefixLd = exceededLd;
        if (matchShorter || len1 - len2 <= maxLd) {
            // Max Size of Distance Array ld that would be required
            int dLen = 2 * maxLd;
            if (dLen > len2) {
                dLen = len2;
            }
            dLen++;
            if (d == null) {
                d = new byte[dLen];
            }
            for (i = 0; i < dLen; i++) {
                // distance from 0 length substring of ls1 to i length substrings of ls2.
                d[i] = (byte) i;
            }
            int lenToChkInMainStr = len2 + maxLd;
            if (lenToChkInMainStr > len1) {
                lenToChkInMainStr = len1;
            }
            int ldOfs = 0, prevLdOfs;
            int nextLd = lenToChkInMainStr;
            for (i = 0; i < lenToChkInMainStr;) {
                char mainCh = Character.toLowerCase(ls1.charAt(i));
                i++;
                prevLdOfs = ldOfs;
                ldOfs = i - maxLd;
                if (ldOfs < 0) {
                    ldOfs = 0;
                }
                int dIMinus1J = d[0];
                int dIJMinus1 = i;
                j = 0;
                if (ldOfs > 0) {
                    j = ldOfs - 1;
                    dIJMinus1 = exceededLd;
                } else {
                    d[0] = (byte) i;
                }
                int lenToChkInMatchStr = i + maxLd;
                if (lenToChkInMatchStr > len2) {
                    lenToChkInMatchStr = len2;
                }
                nextLd = lenToChkInMainStr;
                int minNextLd = exceededLd;
                while (j < lenToChkInMatchStr) {
                    char matchCh = Character.toLowerCase(ls2.charAt(j));
                    j++;
                    int dIMinus1JMinus1 = dIMinus1J;
                    int jInPrevRow = j - prevLdOfs;
                    dIMinus1J = exceededLd;
                    if (jInPrevRow < dLen) {
                        dIMinus1J = d[jInPrevRow];
                    }
                    if (mainCh == matchCh) {
                        nextLd = dIMinus1JMinus1;
                    } else {
                        nextLd = Math.min(
                                Math.min(
                                        // a deletion
                                        dIMinus1J,
                                        // an insertion
                                        dIJMinus1),
                                // a substitution
                                dIMinus1JMinus1);
                        if (nextLd < exceededLd) {
                            nextLd++;
                        }
                    }
                    if (nextLd < minNextLd) {
                        minNextLd = nextLd;
                    }
                    d[j - ldOfs] = (byte) nextLd;
                    dIJMinus1 = nextLd;
                    if (i == len1 && matchLonger && nextLd < prefixLd) {
                        prefixLd = nextLd;
                    }
                }
                if (minNextLd > maxLd) {
                    break;
                }
                // i >= j was added to avoid matches to 2-3 char prefixes in main string like
                // udipi -> upper .. (up matched) and to uchisar (uchi matched).
                if (j == len2 && matchShorter && i >= j && nextLd < prefixLd) {
                    prefixLd = nextLd;
                }
            }
            ld = nextLd;
            prefixLd += offsetForPrefixMatch;
            if (prefixLd < ld) {
                ld = prefixLd;
            }
            if (ld > exceededLd) {
                ld = exceededLd;
            }
        }

        return ld;
    }

    public static String setOpaqueTimigs(Date flightTime, CachedProperties cachedProperties, String itineraryId, boolean dateRequired) {
        Map<String, Object> cammOpaqueTimingsMap = null;
        SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfDisplayDate = new SimpleDateFormat("EEE, dd MMM, yyyy");

        String opaqueTiming = "";

        if (cachedProperties != null) {
            String cammOpaqueTimingsJson = cachedProperties.getPropertyValue(CAMM_OPAQUE_TIMIGS);
            try {
                ObjectMapper mapper = JsonUtil.getObjectMapper();
                cammOpaqueTimingsMap = (Map<String, Object>) mapper.readValue(cammOpaqueTimingsJson, Map.class);
            } catch (Exception ex) {
                if (StringUtils.isNotEmpty(itineraryId)) {
                    logger.info("Exception while getting CAMM opaque timings from property file for itinerary " + itineraryId);
                } else {
                    logger.info("Exception while getting CAMM opaque timings from property file");
                }
                opaqueTiming = "";
            }
        }

        try {
            for (Iterator iterator = cammOpaqueTimingsMap.keySet().iterator(); iterator.hasNext();) {
                Map<String, String> cammOpaqueTiming = (Map<String, String>) cammOpaqueTimingsMap.get(iterator.next());
                Date cammOpaqueStartTiming = sdfDateTime.parse(sdfDate.format(flightTime) + " " + cammOpaqueTiming.get("s"));
                Date cammOpaqueEndTiming = sdfDateTime.parse(sdfDate.format(flightTime) + " " + cammOpaqueTiming.get("e"));

                if (flightTime != null) {
                    if (flightTime.compareTo(cammOpaqueStartTiming) > 0 && flightTime.compareTo(cammOpaqueEndTiming) <= 0) {
                        if (dateRequired) {
                            opaqueTiming = sdfDisplayDate.format(flightTime) + "<" + cammOpaqueTiming.get("s") + " and " + cammOpaqueTiming.get("e") + ">";
                        } else {
                            opaqueTiming = "<" + cammOpaqueTiming.get("s") + " and " + cammOpaqueTiming.get("e") + ">";
                        }
                    }
                }
            }
        } catch (Exception ex) {
            opaqueTiming = "";
        }

        return opaqueTiming;
    }

    /**
     * Format input based on the below parameters.
     * @param input - input string
     * @param splitSize - size of a delimiter-separated split of 'input'; for e.g., if input.length() == 16 and splitSize == 4, there will be 4 uniform splits of 4 characters, separated by the
     *            delimiter; if (splitSize <= 0 || splitSize > input.length()), then splitSize is equated to input.length(). However, if (input == null || input.length() == 0), then formatted input is
     *            set same as input;
     * @param delimiter - string to show the separation between card number splits
     * @return formatted input
     */
    public static String formatString(String input, int splitSize, String delimiter) {
        String formattedInput = input;

        if (input != null && input.length() != 0) {
            int inputLength = input.length();

            if (splitSize <= 0 || splitSize > inputLength) {
                splitSize = inputLength;
            }

            int numberOfSplits = (int) Math.ceil(1.0 * inputLength / splitSize);
            StringBuilder sb = new StringBuilder();
            int index = 0;

            for (int i = 1; i < numberOfSplits; i++) {
                sb.append(input.substring(index, index + splitSize)).append(delimiter);

                index += splitSize;
            }

            sb.append(input.substring(index, input.length()));
            formattedInput = sb.toString();
        }

        return formattedInput;
    }

    public static boolean isIE6(HttpServletRequest request) {
        String userAgent = StringUtils.trimToNull(request.getHeader("User-Agent"));

        return (userAgent != null && userAgent.contains("MSIE 6.0"));
    }

    public static String getUIVersion(CachedProperties cachedProperties, SecurityBean securityBean, HttpServletRequest request, String itineraryId) {
        boolean enableAdditionalChecks = cachedProperties.getBooleanPropertyValue("ct.bookapp.newbookflow.additionalchecks.enable", false);

        String uiVersion = getUIVersion(cachedProperties, securityBean, request);

        if ("v2".equalsIgnoreCase(uiVersion) && enableAdditionalChecks) {
            String productCheckJSONString = cachedProperties.getPropertyValue("ct.bookapp.oldbookflow.products", "[]");

            ObjectMapper mapper = JsonUtil.getObjectMapper();
            try {
                List<String> productMap = (List<String>) mapper.readValue(productCheckJSONString, List.class); // If a product is defined there they will be on old Book flow

                if (StringUtils.isNotEmpty(itineraryId)) {
                    String product = "";

                    if (itineraryId.startsWith("75")) {
                        product = "HOTEL";
                    } else if (itineraryId.startsWith("73")) {
                        product = "INTL-AIR";
                    } else if (itineraryId.startsWith("68")) {
                        product = "DOM-AIR";
                    }

                    if (productMap != null) {
                        if (productMap.contains(product)) {
                            uiVersion = "v1";
                        }
                    }
                }

                String userAgentCheckJSONString = cachedProperties.getPropertyValue("ct.bookapp.oldbookflow.useragents", "[]");
                List<String> userAgents = (List<String>) mapper.readValue(userAgentCheckJSONString, List.class);
                String requestUserAgent = StringUtils.trimToNull(request.getHeader("User-Agent"));

                if (userAgents != null && requestUserAgent != null) {
                    for (String userAgent : userAgents) {
                        if (requestUserAgent.contains(userAgent)) {
                            uiVersion = "v1";
                            break;
                        }
                    }
                }
            } catch (JsonParseException e) {
                logger.error("Exception is:" + e.getMessage(), e);
            } catch (JsonMappingException e) {
                logger.error("Exception is:" + e.getMessage(), e);
            } catch (IOException e) {
                logger.error("Exception is:" + e.getMessage(), e);
            }
        }

        return uiVersion;
    }

    public static String getUIVersion(CachedProperties cachedProperties, SecurityBean securityBean, HttpServletRequest request) {
        String uiVersion = "v1";

        if (isIE6(request)) {
            uiVersion = "v1";
        } else {
            boolean bookFlowV2 = false;
            if (securityBean != null) {
                String bookFlowRole = cachedProperties.getPropertyValue("ct.bookapp.bookflow.v2.role", "1164");

                if (securityBean.getAllRoles() != null && (securityBean.getAllRoles().contains("," + bookFlowRole) || securityBean.getAllRoles().contains(bookFlowRole + ","))) {
                    bookFlowV2 = true;
                }
            }

            boolean forceBookFlowV2 = cachedProperties.getBooleanPropertyValue("ct.bookapp.bookflow.v2.override", false);

            if (forceBookFlowV2) {
                bookFlowV2 = true;
            }

            if (bookFlowV2) {
                uiVersion = "v2";
            } else {
                uiVersion = "v1";
            }
        }

        return uiVersion;
    }

    public static double getRadialDistanceBetweenTwoLatLng(double loc1lat, double loc1long, double loc2lat, double loc2long) {
        double earthRadius = 6371; // Radius of the earth in km
        double dLat = Math.toRadians(loc2lat - loc1lat);
        double dLon = Math.toRadians(loc2long - loc1long);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(loc1lat)) * Math.cos(Math.toRadians(loc2lat)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return Math.abs(earthRadius * c); // Distance in km
    }

    public static String convertToSeparatorString(List<? extends Object> list, String separator) {
        StringBuilder csvBuff = BaseStats.tmpStringBuilder();
        for (Object tmp : list) {
            csvBuff.append(tmp.toString());
            csvBuff.append(separator);
        }
        int lastIndexOfSeparator = csvBuff.lastIndexOf(separator);
        String csv = csvBuff.toString().substring(0, lastIndexOfSeparator);
        csvBuff = BaseStats.returnTmpStringBuilder(csvBuff);
        return csv;
    }

    public static long getTimeDiffInHrs(long fromMillies, long toMillies) {
        long timeDiffInMinutes = ((toMillies - fromMillies) / (60 * 1000));
        long timeDiffInHours = timeDiffInMinutes / 60;
        return timeDiffInHours;
    }

    public static long getTimeDiffInMinutes(long fromMillies, long toMillies) {
        long timeDiffInMinutes = ((toMillies - fromMillies) / (60 * 1000));
        return timeDiffInMinutes;
    }

    public static Date convertISTtoUTC(Date date, String format, String timeZone) {

        if (date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        if (timeZone == null || timeZone.isEmpty()) {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        return GenUtil.createParsedDate(sdf.format(date), format);
    }

    public static Object getCountCookieCounts(String value) {
        String[] cookies = value.split(SEMICOLON);
        for (String cookie : cookies) {
            cookie = cookie.trim();
            if (cookie.startsWith("ct_ct=") && cookie.length() > 6) {
                ObjectMapper mapper = JsonUtil.getObjectMapper();
                try {
                    String encryptedCookie = cookie.substring(6);
                    String countJson = SecurityUtil.aesDecrypt(URLDecoder.decode(encryptedCookie, "UTF-8"), SecurityUtil.getAcubeKey(null)).split("\\|")[0];
                    return mapper.readValue(countJson, Map.class);
                } catch (IOException e) {
                    logger.warn("Count cookie extraction failed.", e);
                }
            }
        }
        return null;
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); // minus number would decrement the days
        return cal.getTime();
    }

    /**
     * Gives the next 20 years list to be displayed for the card expiry years.
     * @param commonCachedProperties CachedProperties
     * @return next 20 years as a list
     * @author Ulhas
     */
    public static ArrayList<String> getNext50Years(CachedProperties commonCachedProperties) {
        int expiryYears = commonCachedProperties.getIntPropertyValue("ct.book.payment.cardexpiry.years", 20);
        ArrayList<String> a = new ArrayList<String>();
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        for (int i = year; i < year + expiryYears; i++) {
            a.add(Integer.toString(i));
        }
        return a;
    }

    public static Date addSeconds(Date date, int seconds) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.SECOND, seconds); // minus number would decrement the seconds
        return cal.getTime();
    }

    /**
     * Method returns Cookie value for A/B Test, cookie name is ct-ab.
     * @param request httpRequest
     * @return String value of cookie post Decoding using UTF-8, this can also return null value in case of missing cookie.
     */
    public static String getCTABCookieFromRequest(HttpServletRequest request) {
        String value = null;
        Cookie ctAB = GenUtil.getCookieFromRequest(request, "ct-ab");
        if (ctAB != null && StringUtils.trimToNull(ctAB.getValue()) != null) {
            try {
                value = URLDecoder.decode(ctAB.getValue(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                logger.warn("unable to encode ct-ab Cookie. " + e);
            }
        }
        return value;
    }

    /**
     * User group for a particular test case. Here it is always assumed that default group is A, further there can be multiple user group as configured in test case. If cookieVal is passed as null
     * Default User group will be returned.
     * @param cookieVal URL-Decoded value of ct-ab cookie.
     * @param testName Name of the Test case for which user gropu is required.
     * @return String ('A' OR 'B' ..)
     */
    public static String getUserGroupFromCTABCookie(String cookieVal, String testName) {
        String userGroup = "A";
        try {
            if (StringUtils.trimToNull(cookieVal) != null) {
                ObjectMapper objectMapper = JsonUtil.getObjectMapper();
                @SuppressWarnings("rawtypes")
                Map userGroupMap = objectMapper.readValue(cookieVal, Map.class);
                if (userGroupMap.containsKey(testName)) {
                    userGroup = userGroupMap.get(testName).toString();
                }
            }
        } catch (IOException e) {
            userGroup = "A";
        }
        return userGroup;
    }

    /**
     * This Method can be use to get CTAB Cookie value in internal API calls(book and pay). it will try to extract value from r_cookie in header map and then key ct-ab.
     * @param requestHeaderMap map of request header created with referer request header.
     * @return url decode cookie value of cookie ct-ab.
     */
    public static String getCTABCookieFromRequestHeader(Map<String, String> requestHeaderMap) {
        String ctab = null;
        String cookieKey = R_COOKIE;
        if (requestHeaderMap != null && requestHeaderMap.containsKey(cookieKey)) {
            String cookieStr = requestHeaderMap.get(cookieKey);
            String[] cookies = cookieStr.split(SEMICOLON);
            for (String cookie : cookies) {
                String[] keyVal = cookie.split(EQUALS_TO);
                if (keyVal[0].trim().equals("ct-ab")) {
                    try {
                        ctab = URLDecoder.decode(keyVal[1], "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        ctab = keyVal[1].trim();
                    }
                }

            }
        }
        return ctab;
    }

    public static int getSumOfAList(List<Integer> list) {
        int sum = 0;
        if (list != null) {
            for (Integer item : list) {
                sum += item.intValue();
            }
        }
        return sum;
    }

    public static Double getTimeZoneFromRegion(Date date, String region) {

        TimeZone tz = TimeZone.getTimeZone(region);

        Calendar cal = GregorianCalendar.getInstance(TimeZone.getDefault());
        cal.setTime(date);
        int offsetInMillis = tz.getOffset(cal.getTimeInMillis());
        int hours = offsetInMillis / 3600000;
        double minutes = Math.abs(((double) (offsetInMillis / 60000) % 60) / 60);

        double offset = hours + minutes;

        return offset;

    }

    public static String getClassPathResource(String resourcePath) throws IOException {
        InputStream ip = GenUtil.class.getClassLoader().getResourceAsStream(resourcePath);
        String s = readAsStringString(ip);
        ip.close();
        return s;
    }

    public static String intern(String s) {
        if (s != null) {
            s = s.intern();
        }

        return s;
    }

    public static List<String> internStrings(List<String> l) {
        List<String> newList = null;
        int i;
        if (l != null) {
            String[] items = l.toArray(new String[l.size()]);
            for (i = 0; i < items.length; i++) {
                String s = items[i];
                if (s != null) {
                    items[i] = s.intern();
                }
            }
            newList = new ArrayWrappingList<String>(items);
        }

        return newList;
    }

    @SuppressWarnings("rawtypes")
    public static Map getCTABCookieMapFromRequest(HttpServletRequest request) {
        String ctabCookieFromRequest = getCTABCookieFromRequest(request);
        Map ctAbCookieMap = null;
        try {
            if (StringUtils.isNotBlank(ctabCookieFromRequest)) {
                ctAbCookieMap = JsonUtil.getObjectMapper().readValue(ctabCookieFromRequest, Map.class);
            }
        } catch (Exception e) {
            logger.debug("Error in parsing ctabCookieFromRequest; exception: " + e.getMessage());
        }
        return ctAbCookieMap;
    }

    public static boolean equals(CharSequence cs1, CharSequence cs2) {
        boolean result = true;
        if (cs1 != cs2) {
            result = false;
            if (cs1 != null && cs2 != null) {
                int i, len;
                if (cs1 instanceof String && cs2 instanceof String) {
                    result = cs1.equals(cs2);
                } else if ((len = cs1.length()) == cs2.length()) {
                    result = true;
                    for (i = 0; i < len; i++) {
                        if (cs1.charAt(i) != cs2.charAt(i)) {
                            result = false;
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    public static MobilePlatforms getMobilePlatform(String userAgent) {
        userAgent = userAgent.toLowerCase();
        Pattern pattern = Pattern.compile("^mozilla/([\\d].[\\d])? \\((mobile;|tablet;)");
        Matcher matcher = pattern.matcher(userAgent);
        if (matcher.find()) {
            return MobilePlatforms.Firefox;
        }
        if (userAgent.contains("android")) {
            return MobilePlatforms.Android;
        } else if (userAgent.contains("iphone")) {
            return MobilePlatforms.iOS;
        } else if (userAgent.contains("blackberry") || userAgent.contains("bb10")) {
            return MobilePlatforms.BlackBerry;
        } else if (userAgent.contains("windows mobile") || userAgent.contains("windows phone")) {
            return MobilePlatforms.Windows;
        }
        return MobilePlatforms.other;
    }

    public static DesktopPlatforms getDesktopPlatform(String userAgent) {
        userAgent = userAgent.toLowerCase();
        if (userAgent.contains("ubuntu")) {
            return DesktopPlatforms.Ubuntu;
        } else if (userAgent.contains("linux")) {
            return DesktopPlatforms.Linux;
        } else if (userAgent.contains("mac os x")) {
            return DesktopPlatforms.Mac_OS_X;
        } else if (userAgent.contains("mac_powerpc") || userAgent.contains("macintosh")) {
            return DesktopPlatforms.Mac;
        } else if (userAgent.contains("windows nt")) {
            return DesktopPlatforms.Windows;
        } else if (userAgent.contains("cros")) {
            return DesktopPlatforms.ChromeOS;
        }
        return DesktopPlatforms.other;
    }

    public static String getMobileOSVersion(String userAgent) {
        Pattern pattern = null;
        String osVersion = "";
        userAgent = userAgent.toLowerCase();
        if (getMobilePlatform(userAgent) == MobilePlatforms.Android) {
            pattern = Pattern.compile("android [\\d]\\.[\\d](\\.\\d)?");
            osVersion = getVersionFormUserAgent(pattern, userAgent, "android");
        } else if (getMobilePlatform(userAgent) == MobilePlatforms.iOS) {
            pattern = Pattern.compile("os [\\d]_[\\d](_[\\d])?");
            osVersion = getVersionFormUserAgent(pattern, userAgent, "os");
        } else if (getMobilePlatform(userAgent) == MobilePlatforms.Windows) {
            pattern = Pattern.compile("windows phone [\\d]*\\.[\\d]*(\\.\\d)?");
            osVersion = getVersionFormUserAgent(pattern, userAgent, "windows phone");
            if (osVersion == null) {
                pattern = Pattern.compile("windows phone os [\\d]*\\.[\\d]*(\\.\\d)?");
                osVersion = getVersionFormUserAgent(pattern, userAgent, "windows phone os");
                if (osVersion == null) {
                    osVersion = "8.0";
                }
            }
        } else if (getMobilePlatform(userAgent) == MobilePlatforms.Firefox) {
            pattern = Pattern.compile("(rv:)(\\d)+(.\\d)?");
            osVersion = getVersionFormUserAgent(pattern, userAgent, "rv:");
        }
        return osVersion;
    }

    public static String getDesktopOSVersion(String userAgent) {
        Pattern pattern = null;
        String osVersion = "";
        userAgent = userAgent.toLowerCase();
        DesktopPlatforms deskPlatForm = getDesktopPlatform(userAgent);
        if (deskPlatForm == DesktopPlatforms.Ubuntu) {
            osVersion = "";
        } else if (deskPlatForm == DesktopPlatforms.Linux) {
            osVersion = "";
        } else if (deskPlatForm == DesktopPlatforms.Windows) {
            pattern = Pattern.compile("windows nt [\\d]*.[\\d*]?");
            osVersion = getVersionFormUserAgent(pattern, userAgent, "windows nt ");
        } else if (deskPlatForm == DesktopPlatforms.Mac) {
            osVersion = "";
        } else if (deskPlatForm == DesktopPlatforms.ChromeOS) {
            pattern = Pattern.compile("cros [\\d]* [\\d]*\\.[\\d]*\\.[\\d*]?");
            osVersion = getVersionFormUserAgent(pattern, userAgent, "CrOS ");
        } else if (deskPlatForm == DesktopPlatforms.Mac_OS_X) {
            pattern = Pattern.compile("mac os x [\\d]*_[\\d]*_[\\d*]?");
            osVersion = getVersionFormUserAgent(pattern, userAgent, "mac os x ");
        }
        return osVersion;
    }

    private static String getVersionFormUserAgent(Pattern pattern, String userAgent, String replace) {
        String osVersion = null;
        Matcher matcher = pattern.matcher(userAgent);
        if (matcher.find()) {
            String matched = userAgent.substring(matcher.start(), matcher.end());
            osVersion = matched.replace(replace, "").trim();
            if (osVersion.indexOf("_") > 0) {
                osVersion = osVersion.replaceAll("_", "\\.");
            }
            if (osVersion.contains(".")) {
                String[] nums = osVersion.split("\\.");
                osVersion = nums[0] + "." + nums[1];
            }
        }
        return osVersion;
    }

    public static String getMobileBrowserName(String userAgent) {
        String browser = "Unknown";
        userAgent = userAgent.toLowerCase();
        if (userAgent.contains("windows")) {
            browser = "IEMobile";
        } else {
            Pattern pattern = Pattern.compile("\\(khtml, like gecko\\) [a-zA-Z0-9]*");
            Matcher matcher = pattern.matcher(userAgent);
            if (matcher.find()) {
                String browserStr = userAgent.substring(matcher.start(), matcher.end());
                browserStr = browserStr.replace("(khtml, like gecko) ", "");
                if (browserStr.equalsIgnoreCase("crios") || browserStr.equalsIgnoreCase("chrome")) {
                    browser = "Chrome";
                } else if (browserStr.equalsIgnoreCase("version")) {
                    browser = "Safari";
                }
            }
        }
        return browser;
    }

    public static String getMobileBrowserVersion(String userAgent) {
        String browserVersion = "";
        userAgent = userAgent.toLowerCase();
        Pattern patternBrowser = Pattern.compile("\\(khtml, like gecko\\) [a-zA-Z0-9]*");
        Matcher matcherBrowser = patternBrowser.matcher(userAgent);
        try {
            if (matcherBrowser.find()) {
                String browser = "";
                browser = userAgent.substring(matcherBrowser.start(), matcherBrowser.end());
                browser = browser.replace("(khtml, like gecko) ", "");
                if (browser.equalsIgnoreCase("crios")) {
                    Pattern patternBrowserVersion = Pattern.compile("crios/[\\d]*\\.[\\d]*");
                    Matcher matcherBrowserVersion = patternBrowserVersion.matcher(userAgent);
                    if (matcherBrowserVersion.find()) {
                        browserVersion = userAgent.substring(matcherBrowserVersion.start(), matcherBrowserVersion.end());
                        browserVersion = browserVersion.replace("crios/", "");
                    }
                } else if (browser.equalsIgnoreCase("chrome")) {
                    Pattern patternBrowserVersion = Pattern.compile("chrome/[\\d]*\\.[\\d]*");
                    Matcher matcherBrowserVersion = patternBrowserVersion.matcher(userAgent);
                    if (matcherBrowserVersion.find()) {
                        browserVersion = userAgent.substring(matcherBrowserVersion.start(), matcherBrowserVersion.end());
                        browserVersion = browserVersion.replace("chrome/", "");
                    }
                } else if (browser.equalsIgnoreCase("version")) {
                    Pattern patternBrowserVersion = Pattern.compile("version/[\\d]*\\.[\\d]*");
                    Matcher matcherBrowserVersion = patternBrowserVersion.matcher(userAgent);
                    if (matcherBrowserVersion.find()) {
                        browserVersion = userAgent.substring(matcherBrowserVersion.start(), matcherBrowserVersion.end());
                        browserVersion = browserVersion.replace("version/", "");
                    }
                }
            } else {
                Pattern patternWindowsBrowser = Pattern.compile("iemobile/[\\d]*\\.[\\d]*");
                Matcher matcherWindowsBrowser = patternWindowsBrowser.matcher(userAgent);
                if (matcherWindowsBrowser.find()) {
                    browserVersion = userAgent.substring(matcherWindowsBrowser.start(), matcherWindowsBrowser.end());
                    browserVersion = browserVersion.replace("iemobile/", "");
                } else {
                    browserVersion = "9.0";
                }
            }
        } catch (Exception e) {
            logger.error("Error", e);
        }
        return browserVersion;
    }

    public static String getDesktopBrowserName(String userAgent) {
        String browser = "Chrome/48.0"; // default
        userAgent = userAgent.toLowerCase();
        Pattern patternIE = Pattern.compile("msie [\\d]*\\.[\\d]*");
        Matcher matcherIE = patternIE.matcher(userAgent);

        Pattern patternChrome = Pattern.compile("chrome/[\\d]*\\.[\\d]*");
        Matcher matcherChrome = patternChrome.matcher(userAgent);

        Pattern patternFirefox = Pattern.compile("firefox/[\\d]*\\.[\\d]*");
        Matcher matcherFirefox = patternFirefox.matcher(userAgent);

        Pattern patternBrowserVersion = Pattern.compile("version/[\\d]*\\.[\\d]*");
        Matcher matcherBrowserVersion = patternBrowserVersion.matcher(userAgent);

        if (matcherIE.find()) {
            String tempBrowser = userAgent.substring(matcherIE.start(), matcherIE.end());
            tempBrowser = tempBrowser.replace("msie ", "");
            browser = "Internet Explorer" + "/" + tempBrowser;
        } else if (matcherChrome.find()) {
            String tempBrowser = userAgent.substring(matcherChrome.start(), matcherChrome.end());
            tempBrowser = tempBrowser.replace("chrome/", "");
            browser = "Chrome" + "/" + tempBrowser;
        } else if (matcherFirefox.find()) {
            String tempBrowser = userAgent.substring(matcherFirefox.start(), matcherFirefox.end());
            tempBrowser = tempBrowser.replace("firefox/", "");
            browser = "Firefox" + "/" + tempBrowser;
        } else if (matcherBrowserVersion.find()) {
            String tempBrowser = userAgent.substring(matcherBrowserVersion.start(), matcherBrowserVersion.end());
            tempBrowser = tempBrowser.replace("version/", "");
            browser = "Safari" + "/" + tempBrowser;
        }
        return browser;
    }

    // Changes of snapdeal white label
    @MethodExcludeCoverage
    public static String isSDEnabled(HttpServletRequest request, CachedProperties cachedProperties) {
        String sdWhiteLabel = "false";
        if (cachedProperties.getBooleanPropertyValue("ct.mobile.snapdeal.whitelabel.enabled", true)) {
            String appAgent = request.getHeader("app-agent");
            String userAgent = request.getHeader("user-agent");
            if (hasSnapdealAppAgent(appAgent) || hasSnapdealUserAgent(userAgent)) {
                sdWhiteLabel = "true";
            } else {
                sdWhiteLabel = "false";
            }
        } else {
            sdWhiteLabel = "false";
        }
        return sdWhiteLabel;
    }

    private static boolean hasSnapdealUserAgent(String userAgent) {
        return StringUtils.isNotBlank(userAgent) && userAgent.contains("snapdeal_app/1.0");
    }

    private static boolean hasSnapdealAppAgent(String appAgent) {
        return StringUtils.isNotBlank(appAgent) && (appAgent.equalsIgnoreCase("SnapdealAndroidAppWebView Mobile") || appAgent.equalsIgnoreCase("SnapdealiOSAppWebView Mobile"));
    }

    public static Double getLatLongDistanceInMeters(Double lat1, Double lng1, Double lat2, Double lng2) {
        Double dist = null;
        try {
            if (isValidLatLng(lat1, lng1, lat2, lng2)) {
                double earthRadius = 6371000; // meters
                double dLat = Math.toRadians(lat2 - lat1);
                double dLng = Math.toRadians(lng2 - lng1);
                double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
                double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                dist = earthRadius * c;
            }
        } catch (Exception e) {
            dist = null;
        }
        return dist;
    }

    private static boolean isValidLatLng(Double lat1, Double lng1, Double lat2, Double lng2) {
        return (lat1 != null && lat1 > 0) && (lat2 != null && lat2 > 0) && (lng1 != null && lng1 > 0) && (lng2 != null && lng2 > 0);
    }
    
    public static String convertNumberToWords(int number) {
        input = numToString(number);
        String converted = "";
        int pos = 1;
        boolean hun = false;
        while (input.length() > 0) {
            if (pos == 1) {
                if (input.length() >= 2) {

                    String temp = input.substring(input.length() - 2, input.length());
                    input = input.substring(0, input.length() - 2);
                    converted += digits(temp);
                } else if (input.length() == 1) {
                    converted += digits(input);

                    input = "";
                }
                pos++;
            } else if (pos == 2) {

                String temp = input.substring(input.length() - 1, input.length());
                input = input.substring(0, input.length() - 1);
                if (converted.length() > 0 && digits(temp) != "") {
                    converted = (digits(temp) + maxs[pos] + " and") + converted;

                    hun = true;
                } else {
                    if (digits(temp) == "")
                        ;
                    else
                        converted = (digits(temp) + maxs[pos]) + converted;
                    hun = true;
                }
                pos++;
            } else if (pos > 2) {
                if (input.length() >= 2) {

                    String temp = input.substring(input.length() - 2, input.length());
                    input = input.substring(0, input.length() - 2);
                    if (!hun && converted.length() > 0)
                        converted = digits(temp) + maxs[pos] + " and" + converted;
                    else {
                        if (digits(temp) == "")
                            ;
                        else
                            converted = digits(temp) + maxs[pos] + converted;
                    }
                } else if (input.length() == 1) {

                    if (!hun && converted.length() > 0)
                        converted = digits(input) + maxs[pos] + " and" + converted;
                    else {
                        if (digits(input) == "")
                            ;
                        else
                            converted = digits(input) + maxs[pos] + converted;
                        input = "";
                    }
                }
                pos++;
            }
        }
        return converted;
    }

    private static String digits(String temp) {
        String converted = "";
        for (int i = temp.length() - 1; i >= 0; i--) {
            int ch = temp.charAt(i) - 48;
            if (i == 0 && ch > 1 && temp.length() > 1)
                converted = tens[ch - 2] + converted;
            else if (i == 0 && ch == 1 && temp.length() == 2) {
                int sum = 0;
                for (int j = 0; j < 2; j++)
                    sum = (sum * 10) + (temp.charAt(j) - 48);
                return teen[sum - 10];
            } else {
                if (ch > 0)
                    converted = units[ch] + converted;
            }
        }
        return converted;
    }

    private static String numToString(int number) {
        String num = "";
        while (number != 0) {
            num = ((char) ((number % 10) + 48)) + num;
            number /= 10;
        }
        return num;
    }

	public static String getRemoteAddrFromRequestHeaders(Map<String, String[]> requestHeaders) {
	    // If Akamai Header present use that
	    String remoteIpAddress = null;
	    if (requestHeaders.get("r_true-client-ip") != null) {
	        remoteIpAddress = requestHeaders.get("r_true-client-ip")[0];
	    } else if (requestHeaders.get("true-client-ip") != null) {
	        remoteIpAddress = requestHeaders.get("true-client-ip")[0];
	    }
	
	    if (remoteIpAddress == null) {
	        if (requestHeaders.get("r_ns-remote-addr") != null) {
	            remoteIpAddress = requestHeaders.get("r_ns-remote-addr")[0];
	        } else if (requestHeaders.get("ns-remote-addr") != null) {
	            remoteIpAddress = requestHeaders.get("ns-remote-addr")[0];
	        }
	
	        String forwardedIpAddress = null;
	        if (requestHeaders.get("r_x-forwarded-for") != null) {
	            forwardedIpAddress = requestHeaders.get("r_x-forwarded-for")[0];
	        } else if (requestHeaders.get("x-forwarded-for") != null) {
	            forwardedIpAddress = requestHeaders.get("x-forwarded-for")[0];
	        }
	
	        int pos = 0;
	        if (forwardedIpAddress != null) {
	            pos = forwardedIpAddress.indexOf(',');
	        }
	
	        if (pos > 0) {
	            forwardedIpAddress = forwardedIpAddress.substring(0, pos);
	        }
	
	        if (remoteIpAddress == null || pos > 0) {
	            // If a comma is present in forwardedIpAddress that is given
	            // preference since it would be forwarded by a proxy server
	            remoteIpAddress = forwardedIpAddress;
	        }
	    }
	
	    if (remoteIpAddress != null) {
	        remoteIpAddress = remoteIpAddress.trim();
	    }

	    return remoteIpAddress;
	}

	public static  List<String[]> readCsv(File dataFile, char delimiter) throws Exception {
        List<String[]> lines = new ArrayList<String[]>();
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(new BufferedInputStream(new FileInputStream(dataFile)));
            CsvReader reader = new CsvReader(inputStreamReader);
            reader.setDelimiter(delimiter);
            reader.readHeaders();

            while (reader.readRecord()) {
                String[] data = reader.getValues();
                lines.add(data);
            }

        } catch (Exception e) {
            logger.error("Error processing csv", e);
            throw new Exception(e.getMessage());
        } finally {
            if (inputStreamReader != null) {
                inputStreamReader.close();                
            }
        }
        return lines;
    }

    public static boolean isABEnabled(HttpServletRequest request, CachedProperties cachedProperties, String cookieNameConfig, String cookieValueConfig, String defaultCookieName, String defaultCookieValue) {
        Map<String, String> requestHeaders = getRequestHeaders(request);
        String ctABCookieValue = GenUtil.getCTABCookieFromRequestHeader(requestHeaders);

        return isABEnabled(ctABCookieValue, cachedProperties, cookieNameConfig, cookieValueConfig, defaultCookieName, defaultCookieValue);
    }


	@SuppressWarnings("unchecked")
	public static boolean isABEnabled(String ctABCookieValue, CachedProperties cachedProperties,
			String cookieNameConfig, String cookieValueConfig, String defaultCookieName, String defaultCookieValue) {
		boolean isABEnabled = false;
		if (StringUtils.isNotBlank(ctABCookieValue)) {
            try {
                Map<String, String> ctABCookieMap = APIUtil.deserializeJsonToObject(ctABCookieValue, Map.class);
                String abCookieKey = cachedProperties.getPropertyValue(cookieNameConfig, defaultCookieName);
                String abCookieVal = ctABCookieMap.get(abCookieKey);
                if (StringUtils.isNotBlank(abCookieVal)) {
                    String abValueFromProperty = cachedProperties.getPropertyValue(cookieValueConfig, defaultCookieValue);
                    if (StringUtils.isNotBlank(abValueFromProperty) && abCookieVal.equalsIgnoreCase(abValueFromProperty)) {
                    	isABEnabled = true;
                    }
                }
            } catch (Exception e) {
                logger.error("Error while parsing AB cookie Value: ", e);
            }
        }
		return isABEnabled;
	}

	public static boolean isCtEmployee(Map<String, String> requestHeaders) {
		boolean isCtEmployee = false;
		if (MapUtils.isNotEmpty(requestHeaders)) {
			String isCtEmp = requestHeaders.get(IS_CT_EMP);
			if (StringUtils.isNotBlank(isCtEmp) && YES.equalsIgnoreCase(isCtEmp)) {
				isCtEmployee = true;
			}
			isCtEmp = requestHeaders.get(R_IS_CT_EMP);
			if (!isCtEmployee && StringUtils.isNotBlank(isCtEmp) && YES.equalsIgnoreCase(isCtEmp)) {
				isCtEmployee = true;
			}
			if (!isCtEmployee) {
				isCtEmployee = isCtEmployee(requestHeaders.get(COOKIE));
			}
			if (!isCtEmployee) {
				isCtEmployee = isCtEmployee(requestHeaders.get(R_COOKIE));
			}
		}
		return isCtEmployee;
	}

	private static boolean isCtEmployee(String cookieValue) {
		boolean isCtEmployee = false;
		if (StringUtils.isNotBlank(cookieValue)) {
			int index = cookieValue.indexOf(USERID);
			if (index > -1) {
				String userId = cookieValue.substring(index);
				if (userId.indexOf(SEMICOLON) > userId.indexOf(EQUALS_TO)) {
					userId = userId.substring(userId.indexOf(EQUALS_TO) + 1, userId.indexOf(SEMICOLON));
					if (userId.contains(CLEARTRIP_DOT_COM)) {
						isCtEmployee = true;
					}
				}
			}
		}
		return isCtEmployee;
	}

	public static String getCryptNonce() throws UnsupportedEncodingException {
        return getCryptNonce(DEFAULT_NONCE_LENGTH);
    }

    public static String getCryptNonce(int nonceLength) throws UnsupportedEncodingException {
        String nonce = null;
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < nonceLength; index++) {
            sb.append(characters.charAt((int)(Math.floor(Math.random() * characters.length()))));
        }
        nonce = sb.toString();
        nonce = new String(Base64.encodeBase64(nonce.getBytes(ISO8859_1_CHARSET.toString())), ISO8859_1_CHARSET.toString());
        return nonce;
    }
    
    public static BufferedImage getImageOverHTTP(String location) throws IOException { 
    	BufferedImage img = null;
    	URL url = new URL(location);
    	img = ImageIO.read(url);
    	return img;
    }
    
    public static void writeImageToDisk(BufferedImage img, String location, String filename, String filetype) throws IOException {
    	File outputfile = new File(location + filename);
    	ImageIO.write(img, filetype, outputfile);
    }

    public static byte[] compress(String data) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length());
        GZIPOutputStream gzip = new GZIPOutputStream(bos);
        gzip.write(data.getBytes());
        gzip.close();
        byte[] compressed = bos.toByteArray();
        bos.close();
        return compressed;
    }

    public static String decompress(byte[] compressed) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
        GZIPInputStream gis = new GZIPInputStream(bis);
        BufferedReader br = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        gis.close();
        bis.close();
        return sb.toString();
    }

}
