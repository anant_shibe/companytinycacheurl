package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.FaultDetail;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author amith
 *
 */
public class FaultDetailsHolder {

    private FaultDetail faultDetail;

    public FaultDetailsHolder() {
    }

    @JsonProperty("fault")
    public FaultDetail getFaultDetail() {
        return faultDetail;
    }

    @JsonProperty("fault")
    public void setFaultDetail(FaultDetail faultDetail) {
        this.faultDetail = faultDetail;
    }

}
