/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PhoneNumber implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private String type;

    private String number;

    private int sequenceNumber;

    @JsonProperty("category")
    public String getType() {
        return type;
    }

    @JsonProperty("category")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("phone_number_value")
    public String getNumber() {
        return number;
    }

    @JsonProperty("phone_number_value")
    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("seq_no")
    public void setSequenceNumber(int seqNumber) {
        this.sequenceNumber = seqNumber;
    }

    @JsonProperty("seq_no")
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PhoneNumber == false)
            return false;
        PhoneNumber ph = (PhoneNumber) obj;
        if ((type != null && ph.getType() != null) && !GenUtil.equals(type, ph.getType())) {
            return false;
        }
        if ((number != null && ph.getNumber() != null) && !GenUtil.equals(number, ph.getNumber())) {
            return false;
        }
        return true;
    }

}
