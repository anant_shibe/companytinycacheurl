package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;

public class VersionReader {

    public static Map<String, String> localResources;

    public static final String CT_SERVICES_AIR_MEMCACHED_SERVERS = "ct.services.air.memcached.servers";

    private static CacheFactory cacheFactory = (CacheFactory) ServicesApplicationContext.getBean("cacheFactory");

    private static Cache cache;

    static {
        try {
            String fileName= System.getProperty("spring.profiles.active").contains("prod") ?
                    "classpath:/static/version_file.json":
                    "classpath:/static/version_file_qa.json";

            localResources = JsonUtil.getObjectMapper()
                    .readValue(new URL(fileName), Map.class);
            cache = cacheFactory.getCacheForServer(CT_SERVICES_AIR_MEMCACHED_SERVERS);
        } catch (IOException e) {
            localResources = Collections.emptyMap();
        }
    }

    public static void cacheVersionForResource(String fileName, String resourceName, int ttl) {
        cache.put(fileName, resourceName, ttl);
    }

    public static String getResourceVersion(String fileName) {
        return (String) cache.get(fileName);
    }

    public static boolean remove(String fileName) {
        return cache.remove(fileName);
    }


}
