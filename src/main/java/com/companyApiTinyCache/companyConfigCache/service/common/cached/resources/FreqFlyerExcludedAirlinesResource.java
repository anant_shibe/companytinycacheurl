package com.companyApiTinyCache.companyConfigCache.service.common.cached.resources;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedUrlResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.StreamResourceLoader;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class FreqFlyerExcludedAirlinesResource extends CachedUrlResource<Map<String, Set<String>>> {

    private static Set<String> ffExcludedDomAirlineCodes;
    private static Set<String> ffExcludedIntlAirlineCodes;

    public static Set<String> getExcludedDomAirlineCodes() {
        return ffExcludedDomAirlineCodes;
    }

    /**
     * useful for testing purposes
     */
    public static void setExcludedDomAirlineCodes(Set<String> excludedDomAirlineCodes) {
        ffExcludedDomAirlineCodes = excludedDomAirlineCodes;
    }

    public static Set<String> getExcludedIntlAirlineCodes() {
        return ffExcludedIntlAirlineCodes;
    }

    /**
     * useful for testing purposes
     */
    public static void setExcludedIntlAirlineCodes(Set<String> excludedIntlAirlineCodes) {
        ffExcludedIntlAirlineCodes = excludedIntlAirlineCodes;
    }

    public FreqFlyerExcludedAirlinesResource(CachedProperties cachedProperties) throws MalformedURLException {
        super(cachedProperties, "ct.services.air.freq-flyer-excluded-airlines.url.ctconfig", null, false);
        setLoader(new FreqFlyerExcludedAirlinesResourceLoader());
        refreshResource();
    }

    private class FreqFlyerExcludedAirlinesResourceLoader implements StreamResourceLoader<Map<String, Set<String>>> {

        @Override
        public Map<String, Set<String>> loadStreamResource(InputStream is) throws Exception {
            String line = null;
            StringBuilder sb = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
            String excludedAirlineCodesJson = sb.toString();

            ObjectMapper mapper = JsonUtil.getObjectMapper();
            Map<String, Set<String>> excludedAirlineCodesMap = mapper.readValue(excludedAirlineCodesJson, new TypeReference<Map<String, LinkedHashSet<String>>>() {
            });

            ffExcludedDomAirlineCodes = excludedAirlineCodesMap.get("dom");
            ffExcludedIntlAirlineCodes = excludedAirlineCodesMap.get("intl");

            return excludedAirlineCodesMap;
        }
    }

}
