package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.JedisCache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.JedisCacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.util.*;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.BookingStatusCode;
import com.companyApiTinyCache.companyConfigCache.service.common.web.PersonalizationGetOpBean;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jedis.lock.JedisLock;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;

@ClassExcludeCoverage
public class PersonalizationServiceImpl implements PersonalizationService {

    private static final String PRS_FALLBACK_V1_0 = "PRS_FALLBACK:V1.0";

    private static final String CT_COMMON_PERSONALIZATION_TRIPS_VERSION_ID = "ct.common.personalization.trips.version.id";

    private static final String TOTAL_COUNT = "tc";

    private static final String MULTICITY = "mc";

    private static final String FLIGHT_NUMBER = "fn";

    private static final String SEGMENT = "sg";

    private static final String FLIGHTS = "fl";

    private static final String AIRLINE__DATA = "ad";

    private static final String FULL_CITY_NAME = "cn";

    private static final String HOTEL_DATA = "hd";

    private static final String FLIGHT = "fl";

    private static final String AREA = "a";

    private static final String CHECK_IN_DATE = "in";

    private static final String PAX = "px";

    private static final String TO = "to";

    private static final String FROM = "fr";

    private static final String NS_TH = "ns_th";

    private static final String CT_COMMON_PERSONALIZATION_HOTEL_FILTER_COUNTER_THRESHOLD = "ct.common.personalization.hotel.filter.counter.threshold";

    private static final String CT_COMMON_PERSONALIZATION_AIRLINE_FILTER_COUNTER_THRESHOLD = "ct.common.personalization.airline.filter.counter.threshold";

    private static final String CT_COMMON_PERSONALISATION_PUT_RETRY_COUNT = "ct.common.personalisation.put.retry.count";

    private static final String BOOKING_PREFERENCE = "s_bp";

    private static final String HOTEL_ID = "id";

    private static final String HOTEL_NAME = "hn";

    private static final String THRESHOLD = "th";

    private static final String AIRLINE = "al";

    private static final String CT_COMMON_PERSONALISATION_BOOKED_HOTEL_THRESHOLD = "ct.common.personalisation.booked.hotel.threshold";

    private static final String CT_COMMON_PERSONALISATION_BOOKED_AIRLINE_THRESHOLD = "ct.common.personalisation.booked.airline.threshold";

    private static final String CT_COMMON_PERSONALISATION_PUT_EXPIRY_SECONDS = "ct.common.personalisation.put.expiry.seconds";

    private static final String BOOKING_DATE = "bd";

    private static final String TRIP_REF = "tr";

    private static final String TRAVEL_DATE = "td";

    private static final String BOOK_STATUS = "bs";

    private static final String SESSION_DATA_PREFIX = "s_";

    private static final String CT_SERVICES_TM_API_PERSONALIZATION_URL = "ct.services.tm.api.personalization.url";

    private static final String UPCOMING_TRIP = "s_uct";

    private static final String LAST_COMPLETED_TRIP = "s_lct";

    private static final String LAST_BOOKED = "s_lb";

    private static final String PRODUCT_TYPE_HOTEL = "HOTEL";

    private static final String PRODUCT_TYPE_AIR = "AIR";

    private static final String ROR_PRODUCT_TYPE_HOTEL = "h";

    private static final String ROR_PRODUCT_TYPE_AIR = AREA;

    private static final String CT_COMMON_PERSONALIZATION_SERVICE_PRECHECKS_JSON = "ct.common.personalization.service.prechecks.json";

    private static final String CT_COMMON_PERSONALIZATION_DEFAULT_ELEMENT_SIZES = "ct.common.personalization.default.element.sizes";

    public static final String DATE_FORMAT_NOW = "dd-MM-yyyy HH:mm:ss";

    private static final Log logger = LogFactory.getLog(PersonalizationServiceImpl.class);

    private CachedProperties commonCachedProperties;
    private JedisCacheFactory jedisCacheFactory;
    private String serverProperty = "ct.service.air.personalization.redis.server";

    // private String rorServerProperty = "ct.service.air.tm.redis.server";

    public PersonalizationServiceImpl(CachedProperties pCachedProperties, JedisCacheFactory jedisCacheFactory) {
        this.commonCachedProperties = pCachedProperties;
        this.jedisCacheFactory = jedisCacheFactory;
    }

    @Override
    public String put(String userId, String jsonData, String source) throws JsonParseException, JsonMappingException, IOException {

        logger.info("entered put service");

        String jsonToReturn = null;

        JedisCache cache = jedisCacheFactory.getCache(serverProperty);

        if (cache == null) {
            logger.error("Unable to get cache. Put failed. jedisCacheFactory " + jedisCacheFactory);
            return "{\"fail\":\"Unable to get cache\"}";
        }

        // acquire on a key
        Jedis jedis = cache.getJedis();
        String lockKey = userId + "_id";
        JedisLock jedisLock = new JedisLock(lockKey);
        int numberOfRetries = commonCachedProperties.getIntPropertyValue(CT_COMMON_PERSONALISATION_PUT_RETRY_COUNT, 3);
        boolean persisted = false;
        for (int retry = 0; retry < numberOfRetries; retry++) {
            try {
                boolean lock = jedisLock.acquire(jedis);

                if (lock) {
                    logger.info("lock acquired during put service on key " + lockKey);

                    String retrievedJsonString = cache.get(userId);

                    Map<String, Map<String, List<Object>>> retrievedJsonObject = null;
                    if (retrievedJsonString != null) {
                        retrievedJsonObject = getObjectFromJsonString(retrievedJsonString);
                    }

                    Map<String, Map<String, List<Object>>> personalisationServiceObject = null;
                    if (jsonData != null) {
                        personalisationServiceObject = getObjectFromJsonString(jsonData);

                        boolean preCheck = preCheckForInputJson(personalisationServiceObject);
                        if (!preCheck) {
                            logger.error("Json validation failed during pre check");
                            return "{\"fail\":\"Json validation failed during pre check\"}";
                        }

                    } else {
                        logger.error("Json data posted for updation is null");
                        return "{\"fail\":\"Json data posted for updation is null\"}";
                    }

                    retrievedJsonObject = updateRetirevedJsonObject(retrievedJsonObject, personalisationServiceObject);

                    // cache retrievedJsonObject
                    String objectToSetInJedis = APIUtil.serializeObjectToJson(retrievedJsonObject);
                    if (logger.isInfoEnabled()) {
                        logger.info("objectToSetInJedis--" + objectToSetInJedis);
                        logger.info("key--" + userId);
                    }

                    int expirySeconds = commonCachedProperties.getIntPropertyValue(CT_COMMON_PERSONALISATION_PUT_EXPIRY_SECONDS, 3600);
                    cache.put(userId, objectToSetInJedis, expirySeconds);

                    persisted = true;
                    // end of put logic

                    if (source != null && source.equalsIgnoreCase("ui")) {
                        // do a get and return the json
                        logger.info("Since source is UI doing a get");
                        PersonalizationGetOpBean personalizationGetOps = new PersonalizationGetOpBean();
                        personalizationGetOps.setUserId(userId);

                        jsonToReturn = cache.get(userId);

                    } else {
                        jsonToReturn = "{\"success\":\"Data updated.\"}";
                    }

                } else {
                    logger.error("Failed to acquire lock to put data for lockKey--> " + lockKey);
                }

            } catch (Exception e) {
                logger.error("Exception " + e + " occured during put data for user id " + userId, e);

            } finally {
                jedisLock.release(jedis);
                jedisCacheFactory.releaseJedisPool(serverProperty, cache);
            }

            if (persisted) {
                break;
            }
        }

        logger.info("exiting put service. Return json--" + jsonToReturn);
        return jsonToReturn;
    }

    /**
     * preCheckForInputJson does json(passed for updation) validation for product type and element type.
     * 
     * @param personalisationServiceObject
     * @throws IOException
     * @throws JsonParseException
     * @throws JsonMappingException
     */
    @SuppressWarnings("unchecked")
    public boolean preCheckForInputJson(Map<String, Map<String, List<Object>>> personalisationServiceObject) throws IOException, JsonParseException, JsonMappingException {

        String preCheckJson = commonCachedProperties.getPropertyValue(CT_COMMON_PERSONALIZATION_SERVICE_PRECHECKS_JSON);
        Map<String, List<String>> preCheckObjectMap = new HashMap<String, List<String>>();
        if (StringUtils.isNotBlank(preCheckJson)) {
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            preCheckObjectMap = mapper.readValue(preCheckJson, Map.class);
        } else {
            logger.error("preCheckJson is null or empty. preCheckJson " + preCheckJson);
            return false;
        }

        Set<String> preCheckProductTypeSet = new HashSet<String>();

        if (preCheckObjectMap != null) {
            preCheckProductTypeSet = preCheckObjectMap.keySet();
        } else {
            logger.error("preCheckObjectMap is null");
            return false;
        }

        Set<String> productTypeSet = new HashSet<String>();

        if (personalisationServiceObject != null) {
            productTypeSet = personalisationServiceObject.keySet();
        } else {
            logger.error("personalisationServiceObject is null");
            return false;
        }

        for (String productType : productTypeSet) {
            boolean productMatch = false;
            for (String preCheckProductType : preCheckProductTypeSet) {

                if (productType.equalsIgnoreCase(preCheckProductType)) {
                    // valid product

                    logger.info("valid product entry in input json. Product type " + productType);
                    Map<String, List<Object>> elementMap = personalisationServiceObject.get(productType);
                    Set<String> elementTypeSet = elementMap.keySet();
                    for (String elementType : elementTypeSet) {
                        boolean elementMatch = false;
                        for (String preCheckElementType : preCheckObjectMap.get(preCheckProductType)) {
                            if (elementType.equalsIgnoreCase(preCheckElementType)) {
                                // valid element found
                                logger.info("valid element entry in input json. Element type " + elementType);
                                elementMatch = true;
                                break;
                            }
                        }

                        if (!elementMatch) {
                            logger.error("Invalid element type passed in input json. Fix- Put element type " + elementType + " in property for precheck");
                            return false;
                        }

                    }
                    productMatch = true;
                    break;
                }

            }

            if (!productMatch) {
                logger.error("Invalid product type passed in input json. Fix- Put product type " + productType + " in property for precheck");
                return false;
            }
        }
        return true;
    }

    /**
     * updateRetirevedJsonObject updates the json retrieved from cache with json sent for updation.
     * 
     * @param retrievedJsonObject
     * @param personalisationServiceObject
     * @return updated retrieved json object
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    private Map<String, Map<String, List<Object>>> updateRetirevedJsonObject(Map<String, Map<String, List<Object>>> retrievedJsonObject,
            Map<String, Map<String, List<Object>>> personalisationServiceObject) throws JsonParseException, JsonMappingException, IOException {

        Set<String> productTypes = new HashSet<String>();
        if (personalisationServiceObject != null) {
            productTypes = personalisationServiceObject.keySet();
        }

        Set<String> productTypesFromRedis = new HashSet<String>();
        if (retrievedJsonObject != null && !retrievedJsonObject.isEmpty()) {
            productTypesFromRedis = retrievedJsonObject.keySet();
        } else {
            // no data available in redis. First put
            // blind put of passed json data to redis map
            retrievedJsonObject = new HashMap<String, Map<String, List<Object>>>();
            // here update ns_at object with threshold data to be used by ui
            // during filter airline counter
            updateFilterThreshold(personalisationServiceObject);
            retrievedJsonObject.putAll(personalisationServiceObject);
            return retrievedJsonObject;
        }

        Map<String, List<Object>> productDataMap = null;
        // boolean productMatch = false;
        for (String productType : productTypes) {
            boolean productMatch = false;
            logger.info("productType---" + productType);
            productDataMap = personalisationServiceObject.get(productType);
            Set<String> elementTypes = new HashSet<String>();
            if (productDataMap != null) {
                elementTypes = productDataMap.keySet();
            }

            Map<String, Map<String, Integer>> configurableDataSizeMap = new HashMap<String, Map<String, Integer>>();

            String configurableDataSizeJson = commonCachedProperties.getPropertyValue(CT_COMMON_PERSONALIZATION_DEFAULT_ELEMENT_SIZES);
            Map<String, Integer> configurableDataSizeData = null;
            if (StringUtils.isNotBlank(configurableDataSizeJson)) {
                ObjectMapper mapper = JsonUtil.getObjectMapper();

                configurableDataSizeMap = mapper.readValue(configurableDataSizeJson, Map.class);
            }
            if (configurableDataSizeMap != null) {
                configurableDataSizeData = configurableDataSizeMap.get(productType);
            }

            Map<String, List<Object>> productDataMapFromRedis = null;
            for (String productTypeFromRedis : productTypesFromRedis) {
                productDataMapFromRedis = retrievedJsonObject.get(productTypeFromRedis);// update
                // this
                // directly
                Set<String> elementTypesFromRedis = new HashSet<String>();
                if (productDataMapFromRedis != null) {
                    elementTypesFromRedis = productDataMapFromRedis.keySet();
                }

                if (productType.equalsIgnoreCase(productTypeFromRedis)) {
                    // product match.
                    // boolean elementMatch = false;
                    for (String elementType : elementTypes) {
                        boolean elementMatch = false;

                        int configurableDataSize = 0;

                        if (configurableDataSizeData != null && configurableDataSizeData.containsKey(elementType)) {
                            configurableDataSize = configurableDataSizeData.get(elementType);
                        } else {
                            logger.error("No default config size defined for productType " + productType + " in property ct.common.personalization.default.element.sizes");
                            configurableDataSize = 1;
                        }

                        List<Object> dataList = productDataMap.get(elementType);
                        for (String elementTypeFromRedis : elementTypesFromRedis) {

                            if (elementType.equalsIgnoreCase(elementTypeFromRedis)) {
                                // elementMatch.. updata dataListFromRedis
                                productDataMapFromRedis = updateElementData(dataList, configurableDataSize, productDataMapFromRedis, elementTypeFromRedis);

                                elementMatch = true;
                                break;
                            }
                        }

                        if (!elementMatch) {
                            // this element type entry not available in redis.
                            // make new entry in map
                            productDataMapFromRedis.put(elementType, dataList);
                        }
                    }
                    productMatch = true;
                    break;
                }
            }
            if (!productMatch) {
                // this product not available in redis.
                // put the product entry in redis product map.
                retrievedJsonObject.put(productType, productDataMap);
            }
        }
        return retrievedJsonObject;

    }

    /**
     * @param personalisationServiceObject
     */
    private void updateFilterThreshold(Map<String, Map<String, List<Object>>> personalisationServiceObject) {
        Set<String> productSet = personalisationServiceObject.keySet();
        for (String product : productSet) {
            Map<String, List<Object>> productDataMap = personalisationServiceObject.get(product);
            List<Object> tempList = new ArrayList<Object>();
            Map<String, Integer> tempMap = new HashMap<String, Integer>();
            int threshold = 0;
            if (product.equalsIgnoreCase(PRODUCT_TYPE_AIR)) {
                // add s_th with value picked from air cached property
                threshold = commonCachedProperties.getIntPropertyValue(CT_COMMON_PERSONALIZATION_AIRLINE_FILTER_COUNTER_THRESHOLD, 3);
            } else if (product.equalsIgnoreCase(PRODUCT_TYPE_HOTEL)) {
                // add s_th with value picked from hotel cached property
                threshold = commonCachedProperties.getIntPropertyValue(CT_COMMON_PERSONALIZATION_HOTEL_FILTER_COUNTER_THRESHOLD, 3);
            }
            tempMap.put("th", threshold);// for all airlines it is the common
            // threshold value.
            tempList.add(tempMap);
            productDataMap.put(NS_TH, tempList);
        }
    }

    /**
     * @param dataList
     * @param dataFromRedis
     * @param newDataList
     * @return
     */
    private Map<String, List<Object>> updateElementData(List<Object> dataList, int configurableDataSize, Map<String, List<Object>> productDataMapFromRedis, String elementTypeFromRedis) {
        // make size configurable..pick from property json.
        List<Object> dataFromRedis = productDataMapFromRedis.get(elementTypeFromRedis);
        productDataMapFromRedis.remove(elementTypeFromRedis);
        int dataSize = 0;
        if (dataFromRedis == null) {
            dataFromRedis = new ArrayList<Object>();
        }

        dataFromRedis.addAll(dataList);
        dataSize = dataFromRedis.size();

        if (dataSize > configurableDataSize) {
            dataFromRedis = dataFromRedis.subList(dataSize - configurableDataSize, dataSize);
        }

        productDataMapFromRedis.put(elementTypeFromRedis, dataFromRedis);
        return productDataMapFromRedis;

    }

    /**
     * @param jsonData
     */
    @SuppressWarnings("unchecked")
    private static Map<String, Map<String, List<Object>>> getObjectFromJsonString(String jsonData) {
        Map<String, Map<String, List<Object>>> personalisationServiceJson = new HashMap<String, Map<String, List<Object>>>();
        try {
            personalisationServiceJson = (Map<String, Map<String, List<Object>>>) APIUtil.deserializeJsonToObject(jsonData, personalisationServiceJson);
        } catch (Exception e) {
            logger.error("Conversion to json structure failed: " + e.getMessage());
        }

        return personalisationServiceJson;
    }

    @Override
    @SuppressWarnings("unchecked")
    public String get(PersonalizationGetOpBean personalizationGetOps, boolean isConfirmed) throws JsonParseException, JsonMappingException, IOException {
        logger.info("entered get service");

        String userId = personalizationGetOps.getUserId();

        String retrievedJson = get(userId);
        Map<String, Map<String, List<Object>>> retrievedJsonObject = null;

        String rorJson = null;
        JedisCache cache = null;

        List<String> rorRetrievedDataList = null;

        String tripsVersion = commonCachedProperties.getPropertyValue(CT_COMMON_PERSONALIZATION_TRIPS_VERSION_ID, "V1.0");

        try {
            // cache = jedisCacheFactory.getCache(rorServerProperty);
            // ror data will also be picked from same server.
            cache = jedisCacheFactory.getCache(serverProperty);
            Jedis jedis = null;

            if (cache != null) {
                jedis = cache.getJedis();
                logger.info("fetching ror details from redis directly");

                if (jedis != null) {
                    Set<String> keys = jedis.keys("TRIPS:" + tripsVersion + ":" + userId + ":TRIPDATA:*");
                    String[] keyArray = new String[keys.size()];

                    int j = 0;
                    for (String key : keys) {
                        keyArray[j] = key;
                        j++;
                    }

                    if (keyArray != null && keyArray.length > 0) {
                        rorRetrievedDataList = jedis.mget(keyArray);
                    }
                } else {
                    logger.info("Jedis is null while trying to fetch pers json from redis. Will hit fall back service for data.");
                }

                logger.info("redis response from ror service redis directly " + rorRetrievedDataList);

            } else {
                logger.info("cache obtained is during during fetch for ror json. Will resort to fall back service.");
            }

        } catch (Exception e) {
            logger.info("Exception " + e.getMessage() + " occured while trying to fetch pers json from redis. Will pick data from fall back service.");
        } finally {
            logger.info("Releasing cache to pool post get");
            jedisCacheFactory.releaseJedisPool(serverProperty, cache);
        }

        if (rorRetrievedDataList == null || rorRetrievedDataList.isEmpty()) { // hit fall back
            logger.info("fetching ror details from fall back service");
            rorJson = getJsonFromFallBackService(userId, tripsVersion);
            logger.info("fall back service response " + rorJson);

        }

        List<Map<String, Object>> parsedRorJsonObject = new ArrayList<Map<String, Object>>();

        if (rorJson != null) {
            parsedRorJsonObject = (List<Map<String, Object>>) APIUtil.deserializeJsonToObject(rorJson, parsedRorJsonObject);
        } else if (rorRetrievedDataList != null) {
            for (String rorRetrievedData : rorRetrievedDataList) {
                Map<String, Object> intermediate = new HashMap<String, Object>();
                intermediate = (Map<String, Object>) APIUtil.deserializeJsonToObject(rorRetrievedData, intermediate);
                parsedRorJsonObject.add(intermediate);
            }
        }

        if (retrievedJson != null) {
            retrievedJsonObject = getObjectFromJsonString(retrievedJson);
        }

        if (parsedRorJsonObject != null && isConfirmed) {
            // do this as ror has only session data.
            retrievedJsonObject = updateWithRoRJsonObject(retrievedJsonObject, parsedRorJsonObject, userId);
        }

        // here add filter code.
        // confirmed user send complete data
        // else send only non session data
        if (!isConfirmed && retrievedJsonObject != null) {
            // call filter
            filter(retrievedJsonObject);
        }

        if (retrievedJsonObject != null) {
            retrievedJson = APIUtil.serializeObjectToJson(retrievedJsonObject);
        }

        if (retrievedJson == null) {
            logger.error("Data not found in redis for user id " + userId);
            String errorJson = "{\"fail\":\"Data not found in redis\"}";
            return errorJson;
        }

        retrievedJson = doGet(personalizationGetOps, userId, retrievedJson);

        logger.info("exiting get service.Retrieved json ---" + retrievedJson);
        return retrievedJson;
    }

    /**
     * call tm personalization fall back service and get the json.
     * 
     * @param userId
     * @return
     */
    private String getJsonFromFallBackService(String userId, String tripsVersion) {

        RestResponse response = null;
        String rorJson = null;
        String url = commonCachedProperties.getPropertyValue(CT_SERVICES_TM_API_PERSONALIZATION_URL);
        // we need to send source too.
        String source = PRS_FALLBACK_V1_0;
        url = url + userId + "&version=" + tripsVersion + "&source=" + source;
        logger.info("Entered fall back block. Hitting url " + url);
        try {
            response = RestUtil.get(url, null);
            rorJson = response.getMessage();
            logger.info("Fallback service response " + rorJson);
        } catch (MalformedURLException e) {
            logger.error("get call to ror failed for user id " + userId + " url " + url);
            logger.error("Exception " + e.getMessage());
            // e.printStackTrace();
        } catch (IOException e) {
            logger.error("get call to ror failed for user id " + userId + " url " + url);
            logger.error("Exception " + e.getMessage());
            // e.printStackTrace();
        } catch (Exception e) {
            logger.error("get call to ror failed for user id " + userId + " url " + url);
            logger.error("Exception " + e.getMessage());
        }
        return rorJson;
    }

    /**
     * filter incoming json to retains only session data
     * 
     * @param jsonObject
     */
    public void filter(Map<String, Map<String, List<Object>>> jsonObject) {
        Set<String> productSet = jsonObject.keySet();
        List<String> removeIndex = new ArrayList<String>();
        for (String productKey : productSet) {
            Map<String, List<Object>> productMap = jsonObject.get(productKey);
            Set<String> elementSet = productMap.keySet();
            for (String elementKey : elementSet) {
                if (elementKey.startsWith(SESSION_DATA_PREFIX)) {
                    removeIndex.add(elementKey);
                }
            }
            for (String index : removeIndex) {
                productMap.remove(index);
            }
            jsonObject.put(productKey, productMap);// overwrite
        }
    }

    /**
     * @param personalizationGetOps
     * @param userId
     * @param retrievedJson
     * @return
     */
    private String doGet(PersonalizationGetOpBean personalizationGetOps, String userId, String retrievedJson) {
        String productType = personalizationGetOps.getProductType();

        if (productType != null) {

            Map<String, List<Object>> productData = null;

            productData = getProductData(retrievedJson, userId, productType);

            String elementType = personalizationGetOps.getElementType();

            if (elementType != null) {

                List<Object> elementData = null;
                if (productData != null) {
                    elementData = productData.get(elementType);
                } else {
                    logger.error("Data not found in redis for user id " + userId + " product type " + productType);
                    retrievedJson = "{\"fail\":\"Data not found in redis\"}";
                }

                if (elementData != null) {
                    retrievedJson = APIUtil.serializeObjectToJson(elementData);
                } else {
                    logger.error("Data not found in redis for user id " + userId + " product type " + productType + " element type " + elementType);
                    retrievedJson = "{\"fail\":\"Data not found in redis\"}";
                }
            } else {
                retrievedJson = APIUtil.serializeObjectToJson(productData);
            }
        }
        return retrievedJson;
    }

    /**
     * @param retrievedJsonObject
     * @param rorJsonObject
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public Map<String, Map<String, List<Object>>> updateWithRoRJsonObject(Map<String, Map<String, List<Object>>> retrievedJsonObject, List<Map<String, Object>> rorJsonObjects, String userId)
            throws JsonParseException, JsonMappingException, IOException {

        SortedMap<Date, List<PersonalizationTripDetailBean>> lastBookedMap = new TreeMap<Date, List<PersonalizationTripDetailBean>>();
        SortedMap<Date, PersonalizationTripDetailBean> lastTravelledTripMap = new TreeMap<Date, PersonalizationTripDetailBean>();
        SortedMap<Date, PersonalizationTripDetailBean> upComingTripMap = new TreeMap<Date, PersonalizationTripDetailBean>();

        SortedMap<Date, List<PersonalizationTripDetailBean>> lastBookedHotelMap = new TreeMap<Date, List<PersonalizationTripDetailBean>>();
        SortedMap<Date, PersonalizationTripDetailBean> lastStayedHotelMap = new TreeMap<Date, PersonalizationTripDetailBean>();
        SortedMap<Date, PersonalizationTripDetailBean> upComingHotelMap = new TreeMap<Date, PersonalizationTripDetailBean>();

        populateEachMapForAir(rorJsonObjects, lastBookedMap, lastTravelledTripMap, upComingTripMap);

        populateEachMapForHotel(rorJsonObjects, lastBookedHotelMap, lastStayedHotelMap, upComingHotelMap);

        retrievedJsonObject = updateJsonWithLastBookedFlightMapData(retrievedJsonObject, lastBookedMap, LAST_BOOKED);

        retrievedJsonObject = updateJsonWithLastBookedHotelMapData(retrievedJsonObject, lastBookedHotelMap, LAST_BOOKED);

        if (retrievedJsonObject != null) {
            populateBookedProductPreference(retrievedJsonObject, userId);
        }

        retrievedJsonObject = updateJsonWithMapData(retrievedJsonObject, lastTravelledTripMap, LAST_COMPLETED_TRIP);

        retrievedJsonObject = updateJsonWithMapData(retrievedJsonObject, lastStayedHotelMap, LAST_COMPLETED_TRIP);

        retrievedJsonObject = updateJsonWithMapData(retrievedJsonObject, upComingTripMap, UPCOMING_TRIP);

        retrievedJsonObject = updateJsonWithMapData(retrievedJsonObject, upComingHotelMap, UPCOMING_TRIP);

        return retrievedJsonObject;

    }

    /**
     * updateJsonWithLastBookedFlightMapData updates the retrieved json with last booked flight details.
     * 
     * @param retrievedJsonObject
     * @param lastBookedMap
     * @param lastBooked
     * @return
     */
    private Map<String, Map<String, List<Object>>> updateJsonWithLastBookedFlightMapData(Map<String, Map<String, List<Object>>> retrievedJsonObject,
            SortedMap<Date, List<PersonalizationTripDetailBean>> lastBookedMap, String lastBooked) {

        Map<String, Object> tmpObject = new HashMap<String, Object>();
        String rorProductType = PRODUCT_TYPE_AIR;
        String mapKey = null;

        if (lastBookedMap == null || lastBookedMap.isEmpty()) {
            logger.info("no last booked air/hotel");
            return retrievedJsonObject;
        }

        List<PersonalizationTripDetailBean> tripDetailsList = lastBookedMap.get(lastBookedMap.lastKey());
        // instead of below pick last key from map.-this wud be last booked
        for (PersonalizationTripDetailBean personalizationTripDetailBean : tripDetailsList) {
            Map<String, Object> innerTempMap = populateRorData(personalizationTripDetailBean, lastBooked);
            tmpObject.put(MULTICITY, personalizationTripDetailBean.isMulticity());
            tmpObject.put(TRIP_REF, personalizationTripDetailBean.getTripRef());

            mapKey = personalizationTripDetailBean.getFrom() + "_" + personalizationTripDetailBean.getTo();

            tmpObject.put(mapKey, innerTempMap);// this will be unique entry
        }

        retrievedJsonObject = updateRorDataInJson(retrievedJsonObject, tmpObject, rorProductType, lastBooked, mapKey);

        return retrievedJsonObject;
    }

    /**
     * updateJsonWithLastBookedHotelMapData updates the retrieved json with last booked hotel details.
     * 
     * @param retrievedJsonObject
     * @param lastBookedMap
     * @param lastBooked
     * @return
     */
    private Map<String, Map<String, List<Object>>> updateJsonWithLastBookedHotelMapData(Map<String, Map<String, List<Object>>> retrievedJsonObject,
            SortedMap<Date, List<PersonalizationTripDetailBean>> lastBookedMap, String lastBooked) {

        Map<String, Object> tmpObject = new HashMap<String, Object>();
        String rorProductType = PRODUCT_TYPE_HOTEL;
        String mapKey = null;

        if (lastBookedMap == null || lastBookedMap.isEmpty()) {
            logger.info("no last booked air/hotel");
            return retrievedJsonObject;
        }

        List<PersonalizationTripDetailBean> tripDetailsList = lastBookedMap.get(lastBookedMap.lastKey());
        // instead of below pick last key from map.-this wud be last booked
        for (PersonalizationTripDetailBean personalizationTripDetailBean : tripDetailsList) {
            mapKey = personalizationTripDetailBean.getCity();
            tmpObject = populateRorData(personalizationTripDetailBean, lastBooked);
        }

        retrievedJsonObject = updateRorDataInJson(retrievedJsonObject, tmpObject, rorProductType, lastBooked, mapKey);

        return retrievedJsonObject;
    }

    /**
     * @param retrievedJsonObject
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    private void populateBookedProductPreference(Map<String, Map<String, List<Object>>> retrievedJsonObject, String userId) throws JsonParseException, JsonMappingException, IOException {

        Set<String> productSet = retrievedJsonObject.keySet();
        Map<String, Map<String, List<Object>>> cachingMap = new HashMap<String, Map<String, List<Object>>>();
        for (String product : productSet) {
            Map<String, List<Object>> productMap = retrievedJsonObject.get(product);
            Map<String, List<Object>> dublicateProductMap = new HashMap<String, List<Object>>();
            dublicateProductMap.putAll(productMap);

            List<Object> elementDataList = new ArrayList<Object>();
            if (product.equalsIgnoreCase(PRODUCT_TYPE_AIR)) {
                getElementDataList(productMap, elementDataList, PRODUCT_TYPE_AIR);
                if (elementDataList != null && !elementDataList.isEmpty()) {
                    dublicateProductMap.put(BOOKING_PREFERENCE, elementDataList);
                    if (dublicateProductMap.containsKey(LAST_BOOKED)) {
                        dublicateProductMap.remove(LAST_BOOKED);
                    }
                    cachingMap.put(PRODUCT_TYPE_AIR, dublicateProductMap);
                }
                // cache json for this user id only for booked preference
            } else if (product.equalsIgnoreCase(PRODUCT_TYPE_HOTEL)) {
                getElementDataList(productMap, elementDataList, PRODUCT_TYPE_HOTEL);
                if (elementDataList != null && !elementDataList.isEmpty()) {
                    dublicateProductMap.put(BOOKING_PREFERENCE, elementDataList);
                    if (dublicateProductMap.containsKey(LAST_BOOKED)) {
                        dublicateProductMap.remove(LAST_BOOKED);
                    }
                    cachingMap.put(PRODUCT_TYPE_HOTEL, dublicateProductMap);
                }
            }
            // here new element list ready.
            // update in primary map
            if (elementDataList != null && !elementDataList.isEmpty()) {
                productMap.put(BOOKING_PREFERENCE, elementDataList);
            }

        }

        if (cachingMap != null && !cachingMap.isEmpty()) {
            String cachingJson = APIUtil.serializeObjectToJson(cachingMap);
            put(userId, cachingJson, null);
        }

    }

    /**
     * getElementDataList updates element data list
     * 
     * @param productMap
     * @param elementDataList
     * @param key
     * @param product
     */
    @SuppressWarnings("unchecked")
    private void getElementDataList(Map<String, List<Object>> productMap, List<Object> elementDataList, String product) {

        int productDisplayThreshold = 0;

        Set<String> elementSet = productMap.keySet();
        List<Object> existingBookedPreferenceDL = productMap.get(BOOKING_PREFERENCE);
        for (String element : elementSet) {
            if (element.equalsIgnoreCase(LAST_BOOKED)) {
                List<Object> lastBookedFlightList = productMap.get(element);
                for (Object lastBookedFlight : lastBookedFlightList) {
                    Map<String, Object> lastBookedFlightMap = (Map<String, Object>) lastBookedFlight;

                    String mapKey = null;

                    if (product != null && product.equalsIgnoreCase(PRODUCT_TYPE_AIR)) {
                        String tripRef = (String) lastBookedFlightMap.get(TRIP_REF);
                        for (String key : lastBookedFlightMap.keySet()) {
                            if (!(key.equalsIgnoreCase(MULTICITY) || key.equalsIgnoreCase(TRIP_REF))) {
                                logger.info("key " + key);
                                Map<String, String> airObjectMap = (Map<String, String>) lastBookedFlightMap.get(key);

                                mapKey = airObjectMap.get(AIRLINE);
                                productDisplayThreshold = commonCachedProperties.getIntPropertyValue(CT_COMMON_PERSONALISATION_BOOKED_AIRLINE_THRESHOLD, 3);

                                updateElementDataList(existingBookedPreferenceDL, mapKey, tripRef, productDisplayThreshold, elementDataList);
                            }

                        }
                    } else if (product != null && product.equalsIgnoreCase(PRODUCT_TYPE_HOTEL)) {
                        // hotel
                        for (String key : lastBookedFlightMap.keySet()) {
                            Map<String, Object> hotelObjectMap = (Map<String, Object>) lastBookedFlightMap.get(key);
                            String tripRef = (String) hotelObjectMap.get(TRIP_REF);
                            String hotelName = (String) hotelObjectMap.get(HOTEL_NAME);
                            String cityName = key;
                            String hotelId = (String) hotelObjectMap.get(HOTEL_ID);

                            mapKey = hotelName + "_" + cityName + "_" + hotelId;

                            productDisplayThreshold = commonCachedProperties.getIntPropertyValue(CT_COMMON_PERSONALISATION_BOOKED_HOTEL_THRESHOLD, 3);
                            updateElementDataList(existingBookedPreferenceDL, mapKey, tripRef, productDisplayThreshold, elementDataList);
                        }

                    }

                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void updateElementDataList(List<Object> existingBookedPreferenceDL, String mapKey, String tripRef, int productDisplayThreshold, List<Object> elementDataList) {
        boolean dataMatch = false;
        if (existingBookedPreferenceDL != null) {
            // S_BP already exists in json
            // match data -if matches increment counter
            // if not then add data with fresh counter
            // by design we know single object always
            // present in booked preference
            Map<String, String> existingMap = (Map<String, String>) existingBookedPreferenceDL.get(0);
            Set<String> existingDataSet = existingMap.keySet();// a set of
            // data(airline/hotel) might
            // be present
            for (String existingData : existingDataSet) {
                String existingTripRef = existingMap.get(TRIP_REF);
                if (existingData.equalsIgnoreCase(mapKey)) {
                    dataMatch = true;
                    if (!(existingTripRef.equalsIgnoreCase(tripRef))) {
                        // data matched. increment only if incoming data is from different trip ref
                        // else this is old data
                        // get counter value and increment it
                        String existingCounter = existingMap.get(existingData);
                        int counter = Integer.parseInt(existingCounter);
                        counter = counter + 1;
                        existingMap.put(mapKey, String.valueOf(counter));
                        existingMap.put(TRIP_REF, tripRef);

                        elementDataList.add(existingMap);
                        break;
                    }
                }
            }

        }

        if (!dataMatch) {
            // new data
            // RT or multi seg case might be there
            Map<String, String> bookedProductMap = new HashMap<String, String>();
            if (elementDataList != null && !elementDataList.isEmpty()) {
                Object elementData = elementDataList.get(0);
                elementDataList.clear();
                Map<String, String> existingMap = (Map<String, String>) elementData;
                Set<String> existingDataSet = existingMap.keySet();

                for (String existingData : existingDataSet) {
                    String existingTripRef = existingMap.get(TRIP_REF);
                    if (existingData.equalsIgnoreCase(mapKey) && (existingTripRef.equalsIgnoreCase(tripRef))) {
                        // same trip Id.. handling RT and multi seg case here
                        // data matched
                        // get counter value and increment it
                        String existingCounter = existingMap.get(existingData);
                        int counter = Integer.parseInt(existingCounter);
                        counter = counter + 1;
                        existingMap.put(mapKey, String.valueOf(counter));
                        existingMap.put(TRIP_REF, tripRef);

                        elementDataList.add(existingMap);
                        break;
                    }
                }
            } else {
                int counter = 1;
                bookedProductMap.put(mapKey, String.valueOf(counter));
                bookedProductMap.put(TRIP_REF, tripRef);

                bookedProductMap.put(THRESHOLD, String.valueOf(productDisplayThreshold));
                elementDataList.add(bookedProductMap);
            }
        }
    }

    private Map<String, Map<String, List<Object>>> updateJsonWithMapData(Map<String, Map<String, List<Object>>> retrievedJsonObject, SortedMap<Date, PersonalizationTripDetailBean> inputMap, String key) {

        if (inputMap == null || inputMap.isEmpty()) {
            logger.info("upcoming/last completed trip map is null or empty");
            return retrievedJsonObject;
        }

        Date inputKey = getInputKey(inputMap, key);

        Map<String, Object> tmpObject = populateRorData(inputMap.get(inputKey), key);

        if (key != null && key.equalsIgnoreCase(UPCOMING_TRIP)) {
            int totalUpcomingTrips = inputMap.size();
            tmpObject.put(TOTAL_COUNT, String.valueOf(totalUpcomingTrips));
        }

        String rorProductType = getProductType(inputMap, inputKey);

        String mapKey = null;
        if (rorProductType != null && rorProductType.equalsIgnoreCase(PRODUCT_TYPE_AIR)) {
            mapKey = inputMap.get(inputKey).getFrom() + "_" + inputMap.get(inputKey).getTo();
        } else if (rorProductType != null && rorProductType.equalsIgnoreCase(PRODUCT_TYPE_HOTEL)) {
            mapKey = inputMap.get(inputKey).getCity();
        }

        retrievedJsonObject = updateRorDataInJson(retrievedJsonObject, tmpObject, rorProductType, key, mapKey);

        return retrievedJsonObject;

    }

    /**
     * @param inputMap
     * @param key
     * @return
     */
    private Date getInputKey(SortedMap<Date, PersonalizationTripDetailBean> inputMap, String key) {
        Date inputKey = null;
        if (inputMap != null && key != null) {
            if (key.equalsIgnoreCase(LAST_COMPLETED_TRIP)) {
                inputKey = inputMap.lastKey();
            } else if (key.equalsIgnoreCase(UPCOMING_TRIP)) {
                inputKey = inputMap.firstKey();
            }

        }
        return inputKey;
    }

    /**
     * @param rorJsonObjects
     * @param lastBookedFlightMap
     * @param lastTravelledTripMap
     * @param upComingTripMap
     */
    @SuppressWarnings("unchecked")
    private void populateEachMapForAir(List<Map<String, Object>> rorJsonObjects, SortedMap<Date, List<PersonalizationTripDetailBean>> lastBookedFlightMap,
            SortedMap<Date, PersonalizationTripDetailBean> lastTravelledTripMap, SortedMap<Date, PersonalizationTripDetailBean> upComingTripMap) {

        // rorJsonObjects is list of trips
        // before looping take current date which is constant for all trips.
        // take 7th day outside
        // last booked list will also go outside
        Calendar cal = Calendar.getInstance();
        Date currentDate = cal.getTime();
        Calendar seventhDay = Calendar.getInstance();
        seventhDay.add(Calendar.DAY_OF_MONTH, 7);
        Date nextSeventhDate = seventhDay.getTime();
        Date formattedSeventhDay = null;
        Date formattedDate = null;
        formattedDate = formatDate(currentDate);
        formattedSeventhDay = formatDate(nextSeventhDate);
        for (Map<String, Object> rorJsonObject : rorJsonObjects) {
            Date bookDate = null;
            List<PersonalizationTripDetailBean> tripDetailBeanList = new ArrayList<PersonalizationTripDetailBean>();
            // each trip will have single book data with list of trip detail
            // based on seg details.
            if (rorJsonObject.get(BOOK_STATUS) != null && ((String) rorJsonObject.get(BOOK_STATUS)).equalsIgnoreCase(BookingStatusCode.P.name())) {
                // doing only for confirmed trip. avoiding canceled(Q)
                // refunded(K) and failed (H) trips
                List<String> paxListString = (List<String>) rorJsonObject.get(PAX);

                String bookDateString = (String) rorJsonObject.get(BOOKING_DATE);
                bookDate = GenUtil.createParsedDate(bookDateString, DATE_FORMAT_NOW);
                Set<String> rorJsonObjectKeySet = rorJsonObject.keySet();
                for (String key : rorJsonObjectKeySet) {
                    if (key.equalsIgnoreCase(AIRLINE__DATA)) {
                        populateAirMaps(lastTravelledTripMap, upComingTripMap, formattedSeventhDay, formattedDate, rorJsonObject, bookDate, tripDetailBeanList, paxListString, bookDateString, key);
                        break;
                    }
                }
            }
            if (bookDate != null) {
                lastBookedFlightMap.put(bookDate, tripDetailBeanList);
            }
        }
    }

    /**
     * populateEachMapForHotel populates the hotel maps with last booked, last completed and up coming trips.
     * 
     * @param rorJsonObjects
     * @param lastBookedFlightMap
     * @param lastTravelledTripMap
     * @param upComingTripMap
     */
    @SuppressWarnings("unchecked")
    private void populateEachMapForHotel(List<Map<String, Object>> rorJsonObjects, SortedMap<Date, List<PersonalizationTripDetailBean>> lastBookedHotelMap,
            SortedMap<Date, PersonalizationTripDetailBean> lastTravelledTripMap, SortedMap<Date, PersonalizationTripDetailBean> upComingTripMap) {

        // rorJsonObjects is list of trips
        // before looping take current date which is constant for all trips.
        // take 7th day outside
        // last booked list will also go outside
        Calendar cal = Calendar.getInstance();
        Date currentDate = cal.getTime();
        Calendar seventhDay = Calendar.getInstance();
        seventhDay.add(Calendar.DAY_OF_MONTH, 7);
        Date nextSeventhDate = seventhDay.getTime();
        Date formattedSeventhDay = null;
        Date formattedDate = null;
        formattedDate = formatDate(currentDate);
        formattedSeventhDay = formatDate(nextSeventhDate);
        for (Map<String, Object> rorJsonObject : rorJsonObjects) {
            Date bookDate = null;
            List<PersonalizationTripDetailBean> tripDetailBeanList = new ArrayList<PersonalizationTripDetailBean>();
            // each trip will have single book data with list of trip detail
            // based on seg details.
            if (rorJsonObject.get(BOOK_STATUS) != null && ((String) rorJsonObject.get(BOOK_STATUS)).equalsIgnoreCase(BookingStatusCode.P.name())) {
                // doing only for confirmed trip. avoiding canceled(Q)
                Set<String> rorJsonObjectKeySet = rorJsonObject.keySet();
                for (String key : rorJsonObjectKeySet) {
                    if (key.equalsIgnoreCase(HOTEL_DATA)) {
                        List<String> paxListString = (List<String>) rorJsonObject.get(PAX);

                        String bookDateString = (String) rorJsonObject.get(BOOKING_DATE);
                        bookDate = GenUtil.createParsedDate(bookDateString, DATE_FORMAT_NOW);
                        populateHotelMap(lastTravelledTripMap, upComingTripMap, formattedSeventhDay, formattedDate, rorJsonObject, bookDate, tripDetailBeanList, paxListString, bookDateString, key);
                        break;
                    }

                }
                if (bookDate != null) {
                    lastBookedHotelMap.put(bookDate, tripDetailBeanList);
                }
            }
        }
    }

    /**
     * @param lastTravelledTripMap
     * @param upComingTripMap
     * @param formattedSeventhDay
     * @param formattedDate
     * @param rorJsonObject
     * @param bookDate
     * @param tripDetailBeanList
     * @param paxListString
     * @param bookDateString
     * @param key
     */
    @SuppressWarnings("unchecked")
    private void populateHotelMap(SortedMap<Date, PersonalizationTripDetailBean> lastTravelledTripMap, SortedMap<Date, PersonalizationTripDetailBean> upComingTripMap, Date formattedSeventhDay,
            Date formattedDate, Map<String, Object> rorJsonObject, Date bookDate, List<PersonalizationTripDetailBean> tripDetailBeanList, List<String> paxListString, String bookDateString, String key) {
        // hotel
        Map<String, String> hotelData = new HashMap<String, String>();
        hotelData = (Map<String, String>) rorJsonObject.get(key);
        PersonalizationTripDetailBean tripDetailBean = new PersonalizationTripDetailBean();

        if (hotelData.get(HOTEL_ID) != null) {
            tripDetailBean.setHotelId(String.valueOf(hotelData.get(HOTEL_ID)));
        }
        tripDetailBean.setHotelName(hotelData.get(HOTEL_NAME));
        tripDetailBean.setArea(hotelData.get(AREA));

        String checkInDateString = hotelData.get(CHECK_IN_DATE);
        tripDetailBean.setCheckInDate(checkInDateString);

        String[] fullCityName = hotelData.get(FULL_CITY_NAME).split(",");

        tripDetailBean.setCity(fullCityName[0]);

        tripDetailBean.setProductType(PRODUCT_TYPE_HOTEL);
        tripDetailBean.setBookingDate(bookDateString);
        tripDetailBean.setPaxName(paxListString);
        tripDetailBean.setTripRef((String) rorJsonObject.get(TRIP_REF));

        Date checkInDate = GenUtil.createParsedDate(checkInDateString, DATE_FORMAT_NOW);

        if (checkInDate.before(formattedDate)) { // less
            lastTravelledTripMap.put(checkInDate, tripDetailBean);
        } else {
            // greater than
            if (checkInDate.before(formattedSeventhDay) || checkInDate.equals(formattedSeventhDay)) {
                upComingTripMap.put(checkInDate, tripDetailBean);
            }
        }
        // last booked.. store all trips..highest
        // bookdate is (should be less than current date of course
        if (bookDate.before(formattedDate) || bookDate.equals(formattedDate)) {
            tripDetailBeanList.add(tripDetailBean);
        }
    }

    /**
     * @param lastTravelledTripMap
     * @param upComingTripMap
     * @param formattedSeventhDay
     * @param formattedDate
     * @param rorJsonObject
     * @param bookDate
     * @param tripDetailBeanList
     * @param paxListString
     * @param bookDateString
     * @param key
     */
    @SuppressWarnings("unchecked")
    private void populateAirMaps(SortedMap<Date, PersonalizationTripDetailBean> lastTravelledTripMap, SortedMap<Date, PersonalizationTripDetailBean> upComingTripMap, Date formattedSeventhDay,
            Date formattedDate, Map<String, Object> rorJsonObject, Date bookDate, List<PersonalizationTripDetailBean> tripDetailBeanList, List<String> paxListString, String bookDateString, String key) {
        Map<String, Object> airlineData = new HashMap<String, Object>();
        airlineData = (Map<String, Object>) rorJsonObject.get(key);
        List<Map<String, List<Map<String, String>>>> flightData = new ArrayList<Map<String, List<Map<String, String>>>>();
        flightData = (List<Map<String, List<Map<String, String>>>>) airlineData.get(FLIGHTS);
        for (Map<String, List<Map<String, String>>> eachFlightMap : flightData) {
            List<Map<String, String>> segmentList = eachFlightMap.get(SEGMENT);
            for (Map<String, String> eachSegmentMap : segmentList) {
                PersonalizationTripDetailBean lBTripDetailBean = new PersonalizationTripDetailBean();
                PersonalizationTripDetailBean tripDetailBean = new PersonalizationTripDetailBean();

                tripDetailBean.setTripRef((String) rorJsonObject.get(TRIP_REF));
                tripDetailBean.setPaxName(paxListString);
                tripDetailBean.setBookingDate(bookDateString);
                tripDetailBean.setAirline(eachSegmentMap.get(AIRLINE));
                tripDetailBean.setFrom(eachSegmentMap.get(FROM));
                tripDetailBean.setTo(eachSegmentMap.get(TO));
                tripDetailBean.setFlightNumber(eachSegmentMap.get(FLIGHT_NUMBER));
                tripDetailBean.setProductType(PRODUCT_TYPE_AIR);

                lBTripDetailBean.setAirline(eachSegmentMap.get(AIRLINE));
                lBTripDetailBean.setFrom(eachSegmentMap.get(FROM));
                lBTripDetailBean.setTo(eachSegmentMap.get(TO));
                lBTripDetailBean.setFlightNumber(eachSegmentMap.get(FLIGHT_NUMBER));
                lBTripDetailBean.setMulticity(((Boolean) airlineData.get(MULTICITY)).booleanValue());
                lBTripDetailBean.setTripRef((String) rorJsonObject.get(TRIP_REF));
                lBTripDetailBean.setProductType(PRODUCT_TYPE_AIR);

                String travelDateString = eachSegmentMap.get(TRAVEL_DATE);
                Date travelDate = GenUtil.createParsedDate(travelDateString, DATE_FORMAT_NOW);
                lBTripDetailBean.setTravelDate(travelDateString);
                tripDetailBean.setTravelDate(travelDateString);
                if (travelDate.before(formattedDate)) { // less
                    lastTravelledTripMap.put(travelDate, tripDetailBean);
                } else {
                    // greater than
                    if (travelDate.before(formattedSeventhDay) || travelDate.equals(formattedSeventhDay)) {
                        upComingTripMap.put(travelDate, tripDetailBean);
                    }
                }
                // last booked.. store all trips..highest
                // bookdate is (sud be less than current date of
                // course
                if (bookDate.before(formattedDate) || bookDate.equals(formattedDate)) {
                    tripDetailBeanList.add(lBTripDetailBean);
                }

            }
        }
    }

    /**
     * @param currentDate
     * @param formattedDate
     * @return
     */
    private Date formatDate(Date currentDate) {
        Date formattedDate = null;
        try {
            formattedDate = GenUtil.formatDate(currentDate, DATE_FORMAT_NOW);
        } catch (Exception e) {
            logger.error("Error occured while parsing current date " + e.getMessage(), e);

        }
        return formattedDate;
    }

    /**
     * @param inputMap
     * @param inputKey
     * @return
     */
    private String getProductType(SortedMap<Date, PersonalizationTripDetailBean> inputMap, Date inputKey) {

        String rorProductType = null;
        if (inputMap != null && inputMap.get(inputKey) != null) {
            rorProductType = inputMap.get(inputKey).getProductType();
        }

        if (rorProductType != null) {
            if (rorProductType.equalsIgnoreCase(ROR_PRODUCT_TYPE_AIR)) {
                rorProductType = PRODUCT_TYPE_AIR;
            } else if (rorProductType.equalsIgnoreCase(ROR_PRODUCT_TYPE_HOTEL)) {
                rorProductType = PRODUCT_TYPE_HOTEL;
            }
        }
        return rorProductType;
    }

    /**
     * 
     * @param tripDetailBean
     * @param type
     * @return
     */
    private Map<String, Object> populateRorData(PersonalizationTripDetailBean tripDetailBean, String type) {
        Map<String, Object> tmpObject = new HashMap<String, Object>();

        String productType = tripDetailBean.getProductType();
        if (productType != null) {
            if (productType.equalsIgnoreCase(PRODUCT_TYPE_AIR)) {
                if (type != null && !type.equalsIgnoreCase(LAST_BOOKED)) {
                    tmpObject.put(BOOKING_DATE, tripDetailBean.getBookingDate());
                    List<String> paxList = tripDetailBean.getPaxName();
                    tmpObject.put(PAX, paxList);
                    tmpObject.put(TRIP_REF, tripDetailBean.getTripRef());
                }
                tmpObject.put(TRAVEL_DATE, tripDetailBean.getTravelDate());
                tmpObject.put(FLIGHT, tripDetailBean.getFlightNumber());
                tmpObject.put(AIRLINE, tripDetailBean.getAirline());
            } else if (productType.equalsIgnoreCase(PRODUCT_TYPE_HOTEL)) {
                tmpObject.put(HOTEL_NAME, tripDetailBean.getHotelName());
                tmpObject.put(HOTEL_ID, tripDetailBean.getHotelId());
                tmpObject.put(CHECK_IN_DATE, tripDetailBean.getCheckInDate());
                tmpObject.put(AREA, tripDetailBean.getArea());
                tmpObject.put(TRIP_REF, tripDetailBean.getTripRef());
                tmpObject.put(BOOKING_DATE, tripDetailBean.getBookingDate());
                List<String> paxList = tripDetailBean.getPaxName();
                tmpObject.put(PAX, paxList);
            }
        }

        return tmpObject;
    }

    /**
     * @param retrievedJsonObject
     * @param tmpObject
     * @param rorProductType
     */
    private Map<String, Map<String, List<Object>>> updateRorDataInJson(Map<String, Map<String, List<Object>>> retrievedJsonObject, Map<String, Object> tmpObject, String rorProductType, String key,
            String mapKey) {

        if (retrievedJsonObject != null) {
            Set<String> productSet = new HashSet<String>();
            productSet = retrievedJsonObject.keySet();

            boolean productMatch = false;
            for (String product : productSet) {
                if (product.equalsIgnoreCase(rorProductType)) {
                    Map<String, List<Object>> retrievedJsonObjectToUpdate = retrievedJsonObject.get(product);
                    List<Object> tmpList = new ArrayList<Object>();
                    Map<String, Map<String, Object>> tmpMapObject = new HashMap<String, Map<String, Object>>();

                    if (product.equalsIgnoreCase(PRODUCT_TYPE_HOTEL) || !key.equalsIgnoreCase(LAST_BOOKED)) {
                        tmpMapObject.put(mapKey, tmpObject);
                        tmpList.add(tmpMapObject);
                    } else {
                        tmpList.add(tmpObject);
                    }

                    if (retrievedJsonObjectToUpdate != null) {
                        // we do a blind update of the element type.
                        // if element type was already in json it gets
                        // overwritten and current data becomes latest.
                        retrievedJsonObjectToUpdate.put(key, tmpList);
                    } else {
                        // directly add
                        // this case should never happen as product type matched
                        // rorProductType.
                        // so for this product type element data must be present
                        // however for safety doing update.
                        updateJsonObject(retrievedJsonObject, key, product, tmpList);
                    }
                    productMatch = true;
                    break;

                }

            }

            if (!productMatch) {
                List<Object> tmpList = new ArrayList<Object>();

                Map<String, Map<String, Object>> tmpMapObject = new HashMap<String, Map<String, Object>>();

                if (rorProductType.equalsIgnoreCase(PRODUCT_TYPE_HOTEL) || !key.equalsIgnoreCase(LAST_BOOKED)) {
                    tmpMapObject.put(mapKey, tmpObject);
                    tmpList.add(tmpMapObject);
                } else {
                    tmpList.add(tmpObject);
                }

                updateJsonObject(retrievedJsonObject, key, rorProductType, tmpList);
            }

        } else {
            // input from pers cache is null
            // however ror cache has data
            // make this pers cache data and return
            retrievedJsonObject = new HashMap<String, Map<String, List<Object>>>();
            List<Object> tmpList = new ArrayList<Object>();

            Map<String, Map<String, Object>> tmpMapObject = new HashMap<String, Map<String, Object>>();

            if (rorProductType.equalsIgnoreCase(PRODUCT_TYPE_HOTEL) || !key.equalsIgnoreCase(LAST_BOOKED)) {
                tmpMapObject.put(mapKey, tmpObject);
                tmpList.add(tmpMapObject);
            } else {
                tmpList.add(tmpObject);
            }
            updateJsonObject(retrievedJsonObject, key, rorProductType, tmpList);
        }

        return retrievedJsonObject;
    }

    /**
     * @param retrievedJsonObject
     * @param key
     * @param product
     * @param tmpList
     */
    private void updateJsonObject(Map<String, Map<String, List<Object>>> retrievedJsonObject, String key, String product, List<Object> tmpList) {
        Map<String, List<Object>> tmpObjectToUpdate = new HashMap<String, List<Object>>();
        tmpObjectToUpdate.put(key, tmpList);
        retrievedJsonObject.put(product, tmpObjectToUpdate);
    }

    /**
     * @param retrievedJson
     * @param userId
     * @param productType
     * @param returnJsonString
     * @return
     */
    private Map<String, List<Object>> getProductData(String personalisationServiceJson, String userId, String productType) {

        Map<String, Map<String, List<Object>>> retrievedJsonObject = getObjectFromJsonString(personalisationServiceJson);
        // then fish data based in product type and return only that
        Map<String, List<Object>> productData = null;
        if (retrievedJsonObject != null) {
            productData = retrievedJsonObject.get(productType);
        }

        return productData;
    }

    /**
     * @param userId
     * @return
     */
    private String get(String userId) {
        String retrievedJson = null;
        JedisCache cache = null;
        try {
            cache = jedisCacheFactory.getCache(serverProperty);

            retrievedJson = cache.get(userId);

        } catch (Exception e) {
            logger.error("Exception " + e.getMessage() + " occurred while trying to get from cache for userid " + userId);
        } finally {
            logger.info("Releasing cache to pool post get");
            jedisCacheFactory.releaseJedisPool(serverProperty, cache);
        }

        logger.info("Exiting get service. Retrieved json---" + retrievedJson);
        return retrievedJson;
    }

}
