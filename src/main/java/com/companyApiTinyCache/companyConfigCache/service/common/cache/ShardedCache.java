package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Sharding implementation which distributes keys across multiple cache implementations depeding on the hash of the key.
 * This implementation is used for jedis rather than ShardedJedis it provides as that API does not have methods like mget and evalsha.
 *
 * @author suresh
 */
public class ShardedCache extends CacheBase {

    private KeyHasher keyHasher;

    private Cache[] caches;

    private Map<String, String> cachedPropertiesResource;

    private boolean propagateErrors;

    public ShardedCache(Map<String, String> pcachedPropertiesResource, String pconnectionString, Cache[] pcaches, boolean ppropagateErrors) {
        cachedPropertiesResource = pcachedPropertiesResource;
        connectionString = pconnectionString;
        caches = pcaches;
        keyHasher = new ConsistentKeyHasher(pcaches);
        propagateErrors = ppropagateErrors;
    }

    @Override
    public Object getNativeConnection(String key) {
        Cache cache = keyHasher.getCacheForKey(key);
        return cache.getNativeConnection(key);
    }

    @Override
    public void returnNativeConnection(String key, Object con) {
        Cache cache = keyHasher.getCacheForKey(key);
        cache.returnNativeConnection(key, con);
    }

    @Override
    public Object get(String key, Charset charset) throws CacheRetrieveException {
        Cache cache = keyHasher.getCacheForKey(key);
        Object val = null;
        try {
            val = cache.get(key, charset);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in get", e);
            }
        }
        return val;
    }

    @Override
    public Map<String, Object> getMulti(String[] ids, Charset charset)
            throws CacheRetrieveException {
        Map<String, Object> valueMap = null;
        try {
            if (caches.length > 1) {
                List<String>[] serverKeys = new List[caches.length];
                List<String> keyList;
                Cache cache;
                int i, cacheIndex;
                for (i = 0; i < ids.length; i++) {
                    String key = ids[i];
                    cache = keyHasher.getCacheForKey(key);
                    for (cacheIndex = 0; cacheIndex < caches.length; cacheIndex++) {
                        if (caches[cacheIndex] == cache) {
                            keyList = serverKeys[cacheIndex];
                            if (keyList == null) {
                                keyList = new ArrayList<String>();
                                serverKeys[cacheIndex] = keyList;
                            }
                            keyList.add(key);
                            break;
                        }
                    }
                }
                for (i = 0; i < caches.length; i++) {
                    keyList = serverKeys[i];
                    if (keyList != null) {
                        String[] keys = keyList.toArray(new String[keyList.size()]);
                        cache = caches[i];
                        Map<String, Object> nextVals = cache.getMulti(keys, charset);
                        if (nextVals != null) {
                            if (valueMap == null) {
                                valueMap = nextVals;
                            } else {
                                valueMap.putAll(nextVals);
                            }
                        }
                    }
                }
            } else if (caches.length == 1) {
                valueMap = caches[0].getMulti(ids);
            }
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in getMulti", e);
            }
        }

        return valueMap;
    }

    @Override
    public boolean put(String key, Object val, int expirySeconds) throws CacheStoreException {
        Cache cache = keyHasher.getCacheForKey(key);
        boolean success = false;
        try {
            success = cache.put(key, val, expirySeconds);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in put", e);
            }
        }
        return success;
    }

    @Override
    public boolean putMulti(Map<String, ? extends Object> map, int expirySeconds, Charset charset) throws CacheStoreException {
        boolean success = false;
        try {
        	if (caches.length > 1) {
        		Map<Cache, Map<String, Object>> shardedCacheMap = new HashMap<>();
        		map.forEach((key, value) -> {
        			Cache cache = keyHasher.getCacheForKey(key);
        			shardedCacheMap.computeIfAbsent(cache, k -> new HashMap<>()).put(key, (Object)value);
        		});
        		shardedCacheMap.forEach((cache, object) -> {
        			caches[0].putMulti(object, expirySeconds, charset);
        		});
        	} else if (caches.length == 1) {
        		success = caches[0].putMulti(map, expirySeconds, charset);
        	}
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in put", e);
            }
        }
        return success;
    }

    @Override
    public Long incrBy(String key, long integer) throws CacheStoreException {
        Cache cache = keyHasher.getCacheForKey(key);
        Long result = 0L;
        try {
            result = cache.incrBy(key, integer);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in put", e);
            }
        }
        return result;
    }

    @Override
    public Long incr(String key) throws CacheStoreException {
        Cache cache = keyHasher.getCacheForKey(key);
        Long result = 0L;
        try {
            result = cache.incr(key);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in put", e);
            }
        }
        return result;    }

    @Override
    public Long decr(String key) throws CacheStoreException {
        Cache cache = keyHasher.getCacheForKey(key);
        Long result = 0L;
        try {
            result = cache.decr(key);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in put", e);
            }
        }
        return result;    }

    @Override
    public Long decrBy(String key, long integer) throws CacheStoreException {
        Cache cache = keyHasher.getCacheForKey(key);
        Long result = 0L;
        try {
            result = cache.decrBy(key, integer);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in put", e);
            }
        }
        return result;
    }

    @Override
    public boolean remove(String key) {
        Cache cache = keyHasher.getCacheForKey(key);
        boolean success = false;
        try {
            success = cache.remove(key);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in remove", e);
            }
        }
        return success;
    }

    @Override
    public void clear() throws CacheDeleteException {
        try {
            for (Cache cache : caches) {
                cache.clear();
            }
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in clear", e);
            }
        }
    }

    @Override
    public boolean add(String key, Object val, int expirySeconds)
            throws CacheStoreException {
        Cache cache = keyHasher.getCacheForKey(key);
        boolean success = false;
        try {
            success = cache.add(key, val, expirySeconds);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in add", e);
            }
        }
        return success;
    }

    @Override
    public LockStatus lock(String key, int expirySeconds, boolean verifyFailedLock) {
        Cache cache = keyHasher.getCacheForKey(key);
        LockStatus lockStatus = LockStatus.UNKNOWN;
        try {
            lockStatus = cache.lock(key, expirySeconds, verifyFailedLock);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in lock", e);
            }
        }
        return lockStatus;
    }

    @Override
    public boolean atomicUpdate(String id, int expirySeconds, ValueUpdater updater, int maxTries) {
        Cache cache = keyHasher.getCacheForKey(id);
        boolean success = false;
        try {
            success = cache.atomicUpdate(id, expirySeconds, updater, maxTries);
        } catch (Exception e) {
            if (propagateErrors) {
                throw new CacheRetrieveException("Error in atomicUpdate", e);
            }
        }
        return success;
    }

    @Override
    public boolean atomicUpdate(List<String> idList, int expirySeconds, List<ValueUpdater> updaterList, int maxTries) {
        boolean success = true;
        Map<Cache, Integer> cacheToIndexMap = new HashMap<>();
        List<Object[]> cacheList = new ArrayList<>();
        for (int i = 0; i < idList.size(); i++) {
            String id = idList.get(i);
            Cache cache = keyHasher.getCacheForKey(id);
            Integer index = cacheToIndexMap.get(cache);
            Object[] lists;
            if (index == null) {
                index = cacheList.size();
                cacheToIndexMap.put(cache, index);
                lists = new Object[2];
                lists[0] = new ArrayList<String>();
                lists[1] = new ArrayList<ValueUpdater>();
                cacheList.add(lists);
            } else {
                lists = cacheList.get(index);
            }
            ((ArrayList<String>)lists[0]).add(id);
            ((ArrayList<ValueUpdater>)lists[1]).add(updaterList.get(i));
        }
        for (Cache cache : cacheToIndexMap.keySet()) {
            Object[] lists = cacheList.get(cacheToIndexMap.get(cache));
            try {
                success &= cache.atomicUpdate((ArrayList<String>)lists[0], expirySeconds, (ArrayList<ValueUpdater>)lists[1], maxTries);
            } catch (Exception e) {
                success = false;
                if (propagateErrors) {
                    throw new CacheRetrieveException("Error in atomicUpdate", e);
                }
            }
        }
        return success;
    }

    @Override
    public long getServerTime() {
        long st = 0;
        if (caches.length == 1) {
            try {
                st = caches[0].getServerTime();
            } catch (Exception e) {
                if (propagateErrors) {
                    throw new CacheRetrieveException("Error in getServerTime", e);
                }
            }
        }
        return st;
    }

    @Override
    public void destroy() throws Exception {
        for (Cache cache : caches) {
            cache.destroy();
        }
    }

    Map<String, String> getCachedPropertiesResource() {
        return cachedPropertiesResource;
    }
}
