package com.companyApiTinyCache.companyConfigCache.service.common.webservice;

import com.companyApiTinyCache.companyConfigCache.service.common.data.Affiliate;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.CtBaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CompanyConfigurationFilter;
import com.companyApiTinyCache.companyConfigCache.service.common.web.ViewController;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Controller will return the path of the resource requested by a user. The controller expects companyid and resourcetype and an optional view name. Based on the request controller will return the
 * ModelAndView object.
 * 
 * @author Saurabh Narain
 * @author Amit Kumar Sharma
 */
public class CommonUIPageElementsService extends ViewController {

    public CommonUIPageElementsService() {
        doStats = false;
    }

    @Override
    protected ModelAndView processRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String companyIdParam = StringUtils.trimToEmpty(request.getParameter(CompanyConfigurationFilter.COMPANY_ID_PARAM));
        String viewName = StringUtils.trimToEmpty(request.getParameter("view"));
        String resourceType = StringUtils.trimToEmpty(request.getParameter("resourceType"));
        // XXX: TODO - add support for domain also

        if (resourceType.isEmpty()) {
            // request was syntactically incorrect
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }

        // 1. Attempt to extract pageElement value from Affiliate object
        // This is set by companyConfig Filter. Should directly write value
        // to response stream
        String pageElementValue = extractFromCompanyConfig(request, resourceType);

        if (pageElementValue != null) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().write(pageElementValue);
            return null;
        }

        // 2. Attempt to extract pageElement value from whitelabelViewResource
        // In this case, send across ModelAndView.
        try {
            pageElementValue = extractFromWhiteLabelViewResource(companyIdParam, resourceType, viewName);
        } catch (NoSuchCompanyException ex) {
            logger.error("Could not find companyResourceMap for companyId = " + companyIdParam);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

        ModelAndView modelAndView = null;
        if (pageElementValue != null) {
            modelAndView = new ModelAndView(pageElementValue);
        }

        // 3. If no validd value can be found, log an error and send HTTP 404
        else {
            logger.error("Page element value undefined for [companyId = " + companyIdParam + ", resourceType = " + resourceType + "]");
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }

        return modelAndView;

    }

    private String extractFromWhiteLabelViewResource(String companyIdParam, String resourceType, String viewName) throws NoSuchCompanyException {

        resourceType = resourceType.toUpperCase();
        Map<String, Map<String, String>> companyResourceMap = getPageElementsMap(companyIdParam);
        if (companyResourceMap == null) {
            throw new NoSuchCompanyException("Map not found in whitelabelViewResource for companyId =" + companyIdParam);
        }

        // view will not be null at this point.
        Map<String, String> viewResourceMap = companyResourceMap.get(viewName);

        // Extract the default value for the page element
        String pageElementValue = companyResourceMap.get("default").get(resourceType);

        // Use page-level-override if defined
        if (viewResourceMap != null) {
            pageElementValue = viewResourceMap.get(resourceType);
        }

        return pageElementValue;
    }

    private String extractFromCompanyConfig(HttpServletRequest request, String resourceType) {

        // 1. Extract Affiliate object from request. Company-id to affiliate resolution is done
        // by CompanyConfigFilter. The same filter attached Affiliate object as parameter
        // to the request.
        Object affiliateAttrib = request.getAttribute(CompanyConfigurationFilter.COMPANY_ATTRIB);
        Affiliate company = null;

        // 2. Return null if Affiliate object is not found
        if (affiliateAttrib == null) {
            return null;
        }

        // 3. Obtain CompanyConfig map from Affiliate
        company = (Affiliate) affiliateAttrib;
        Map<String, String> companyConfig = company.getAffiliateConfigsMap();

        // 4. Obtain the requested resource value. This will always be
        // path to a JSP.
        String resourceValue = companyConfig.get(resourceType);

        return resourceValue;
    }

    // ///////////////////////////////////////////////////////////////
    // /////////////////// Redundant Overrides ///////////////////////
    // ///////////////////////////////////////////////////////////////

    @Override
    protected CtBaseStats initStats(HttpServletRequest request) {
        return null;
    }

    @Override
    protected CtBaseStats commitStats(CtBaseStats statsObj, HttpServletRequest request, int responseStatusCode) {
        return null;
    }

    // ///////////////////////////////////////////////////////////////
    // ///////////////// Private Exception Class ////////////////////
    // ///////////////////////////////////////////////////////////////
    private class NoSuchCompanyException extends Exception {

        private static final long serialVersionUID = 1721126422159067343L;

        public NoSuchCompanyException(String message) {
            super(message);
        }
    }

}
