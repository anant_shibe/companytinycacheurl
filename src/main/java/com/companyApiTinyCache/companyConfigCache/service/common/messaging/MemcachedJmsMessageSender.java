package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.CacheFactory;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import java.io.Serializable;
import java.util.UUID;

/**
 * MemcachedJmsMessageSender.
 */
public class MemcachedJmsMessageSender implements MessageSender {

    public static final String LOGGING_MEMCACHED_SERVER_PROPERTY_NAME = "ct.services.logging.memcached.servers";

    private static final Log log = LogFactory.getLog(MemcachedJmsMessageSender.class);

    private static final String DEFAULT_CACHE_EXPIRY_PROPERTY = "ct.common.memcachedq.cacheseconds";

    private static final int DEFAULT_CACHE_EXPIRY_TIME = 7200;

    private Cache cache;

    private JmsTemplate jmsTemplate;

    private String cacheExpiryProperty;

    private String formatProperty;

    private CachedProperties commonCachedProperties;

    // Whole Serializer thing introduced just because payment code is in war file.
    // Memcached client serialization does not work because it is present in jar
    // due to which Deserialization gives ClassNotFoundException.
    private MessageSerializer serializer;

    public MemcachedJmsMessageSender(CacheFactory pcacheFactory, ConnectionFactory pconnectionFactory,
            Destination pdestination, CachedProperties pcommonCachedProperties, String pcacheExpiryProperty) {
        cache = pcacheFactory.getCacheForServer(LOGGING_MEMCACHED_SERVER_PROPERTY_NAME);
        jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(pconnectionFactory);
        jmsTemplate.setDefaultDestination(pdestination);
        jmsTemplate.afterPropertiesSet();
        commonCachedProperties = pcommonCachedProperties;
        cacheExpiryProperty = pcacheExpiryProperty;
        // The second check is to optimize memory by String interning
        if (cacheExpiryProperty == null || DEFAULT_CACHE_EXPIRY_PROPERTY.equals(cacheExpiryProperty)) {
            cacheExpiryProperty = DEFAULT_CACHE_EXPIRY_PROPERTY;
        }
        // Derive formatProperty from cacheExpiryProperty. This is presently an
        // unlisted Property defaulting to json
        int pos = cacheExpiryProperty.lastIndexOf('.') + 1;
        if (pos <= 0) {
            pos = 0;
        }
        formatProperty = cacheExpiryProperty.substring(0, pos) + "format";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(Object message) throws Exception {
        boolean isDebug = log.isDebugEnabled();
        boolean isInfo = log.isInfoEnabled();
        String instanceId = null;
        if (message instanceof MessageWithId) {
            instanceId = ((MessageWithId) message).getMessageId();
        }
        if (StringUtils.isBlank(instanceId)) {
            instanceId = UUID.randomUUID().toString();
        }
        int expiryTime = DEFAULT_CACHE_EXPIRY_TIME;
        if (commonCachedProperties != null) {
            expiryTime = commonCachedProperties.getIntPropertyValue(cacheExpiryProperty, -1);
            if (expiryTime == -1) {
                if (!DEFAULT_CACHE_EXPIRY_PROPERTY.equals(cacheExpiryProperty)) {
                    expiryTime = commonCachedProperties.getIntPropertyValue(DEFAULT_CACHE_EXPIRY_PROPERTY, DEFAULT_CACHE_EXPIRY_TIME);
                } else {
                    expiryTime = DEFAULT_CACHE_EXPIRY_TIME;
                }
            }
        }
        Object storedMessage = message;
        String format = commonCachedProperties.getPropertyValue(formatProperty, "json");
        long start = System.currentTimeMillis();
        long end = 0;
        if ("json".equalsIgnoreCase(format)) {
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            StringBuilderWriter sw = BaseStats.tmpWriter();
            mapper.writeValue(sw, message);
            storedMessage = sw.toString();
            sw = BaseStats.returnTmpWriter(sw);
            storedMessage = GenUtil.hideCardInfo(null, (String) storedMessage);
            if (isInfo) {
                end = System.currentTimeMillis();
                log.info("Time taken to serialize message = " + (end - start));
                start = end;
            }
        } else if (serializer != null) {
            storedMessage = serializer.serialize(message);
        }
        if (isDebug) {
            log.debug("Sending instanceId: " + instanceId);
            log.debug("Stored Message = " + storedMessage);
        }
        cache.syncPut(instanceId, (Serializable) storedMessage, expiryTime);
        if (isInfo) {
            end = System.currentTimeMillis();
            log.info("Time taken to store message = " + (end - start));
            start = end;
        }
        jmsTemplate.convertAndSend(instanceId);
        if (isInfo) {
            end = System.currentTimeMillis();
            log.info("Time taken to send message Id " + instanceId + " = " + (end - start));
            start = end;
        }
    }

    /**
     * Getter for formatProperty.
     *
     * @return the formatProperty
     */
    public String getFormatProperty() {
        return formatProperty;
    }

    /**
     * Setter for serializer.
     *
     * @param pserializer
     *            the serializer to set
     */
    public void setSerializer(MessageSerializer pserializer) {
        serializer = pserializer;
    }

    /**
     * Return the jms template instance.
     *
     * @return the jms template instance
     */
    public JmsTemplate getJmsTemplate() {
        return jmsTemplate;
    }

}
