/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.notification;

import javax.mail.MessagingException;
import java.util.Collection;
import java.util.List;

/**
 * @author sanjeev
 */
public interface EmailSender {

    void sendEmail(String to, String from, String subjectMessage, String bodyMessage, String fromName, String contentType) throws MessagingException;

    void sendEmail(String email, String from, String subjectMessage, String bodyMessage) throws MessagingException;

    void sendEmail(Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String from, String subjectMessage, String bodyMessage) throws MessagingException;

    void sendEmail(Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String from, String subjectMessage, String bodyMessage, String contentType)
            throws MessagingException;

    void sendEmail(Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String from, String subjectMessage, String bodyMessage, byte[] file, String fileName,
            String contentType) throws MessagingException;

    void sendEmail(Collection<String> toList, Collection<String> ccList, Collection<String> bccList, String from, String subjectMessage, String bodyMessage, byte[][] files, List<String> fileNames,
            List<String> contentTypes) throws MessagingException;
}
