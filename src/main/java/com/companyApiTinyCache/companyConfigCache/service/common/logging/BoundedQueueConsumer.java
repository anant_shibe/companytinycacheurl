package com.companyApiTinyCache.companyConfigCache.service.common.logging;

/**
 * Interface specifying a Message processing consumer class for messages from BoundedQueue.
 *
 * @author suresh
 *
 * @param <T> Type of Message
 */
public interface BoundedQueueConsumer<T> {
    void process(T message) throws Exception;
}
