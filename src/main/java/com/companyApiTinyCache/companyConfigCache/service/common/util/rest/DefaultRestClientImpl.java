package com.companyApiTinyCache.companyConfigCache.service.common.util.rest;

import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.LogData;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.LogThreadLocal;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.Cookie;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Utility class to make HTTP calls.
 */
public final class DefaultRestClientImpl implements RestClient {

    private static final String RESPONSE_COOKIE_HEADER = "Set-Cookie";
    private static final String REQUEST_COOKIE_HEADER = "Cookie";
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String CONTENT_LENGTH_HEADER = "Content-Length";
    private static final String CONTENT_ENCODING_HEADER = "Content-Encoding";
    private static final String ACCEPT_ENCODING_HEADER = "Accept-Encoding";
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String CONTENT_ENCODING_GZIP = "gzip";
    private static final String POST_METHOD = "POST";
    private static final String PUT_METHOD = "PUT";
    private static final String DELETE_METHOD = "DELETE";

    private static class IgnoreHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    private static final Log LOGGER = LogFactory.getLog(DefaultRestClientImpl.class);
    private static final IgnoreHostnameVerifier ignoreHostnameVerifier = new IgnoreHostnameVerifier();
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private CachedProperties cachedProperties;

    // Singleton
    private DefaultRestClientImpl() {
    }

    private static DefaultRestClientImpl instance = new DefaultRestClientImpl();

    public static DefaultRestClientImpl getInstance() {
        return instance;
    }

    public void setCachedProperties(CachedProperties cachedProperties) {
        this.cachedProperties = cachedProperties;
    }

    @Override
    public RestResponse get(RestRequest request) throws IOException {
        RestResponse response = new RestResponse();
        StringBuilder sb = BaseStats.tmpStringBuilder();
        String targetUrl = request.getTargetUrl();
        URLConnection connection = null;
        BufferedReader reader = null;

        try {
            String queryString = request.getQueryString();
            if (!StringUtils.isEmpty(request.getQueryString())) {
                targetUrl = String.format("%s?%s", targetUrl, queryString);
            }

            URL server = new URL(targetUrl);
            connection = server.openConnection();
            connection.setConnectTimeout(request.getTimeout());
            connection.setReadTimeout(request.getTimeout());

            Map<String, String> requestHeaders = request.getHeaders();
            LogData logData = LogThreadLocal.getLogData();
            String requestId = logData == null ? null : logData.getRequestId();
            if (StringUtils.isNotBlank(requestId)) {
                if (requestHeaders == null) {
                    requestHeaders = new HashMap<String, String>();
                }
                requestHeaders.put(LogData.REQUEST_ID, requestId);
            }
            if (requestHeaders != null) {
                for (String key : requestHeaders.keySet()) {
                    connection.setRequestProperty(key, requestHeaders.get(key));
                }
            }
            connection.setRequestProperty(ACCEPT_ENCODING_HEADER, CONTENT_ENCODING_GZIP);

            Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>(connection.getHeaderFields());
            responseHeaders.put(RESPONSE_COOKIE_HEADER, extractCookieHeaders(connection));
            response.setHeaders(responseHeaders);

            InputStream responseStream = connection.getInputStream();
            if (CONTENT_ENCODING_GZIP.equalsIgnoreCase(connection.getHeaderField(CONTENT_ENCODING_HEADER)) && request.isGunzipResponse()) {
                responseStream = new GZIPInputStream(responseStream);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Got Gzipped Response for URL: " + targetUrl);
                }
            }
            if (request.isByteResponse()) {
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[4096];
                while ((nRead = responseStream.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                byte[] bytes = buffer.toByteArray();
                response.setBinaryContent(bytes);
            } else {
                CharEncoding charEncoding = request.getCharEncoding();
                if (charEncoding != null) {
                    reader = new BufferedReader(new InputStreamReader(responseStream, charEncoding.stringValue()));
                } else {
                    reader = new BufferedReader(new InputStreamReader(responseStream));
                }
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append(LINE_SEPARATOR);
                }
                response.setContent(sb.toString());
            }
            if (connection instanceof HttpURLConnection) {
                response.setStatus(((HttpURLConnection) connection).getResponseCode());
            } else {
                response.setStatus(RestResponse.STATUS_OK);
            }
            response.setContent(sb.toString());
        } catch (MalformedURLException e) {
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info("MalformedURLException occured for URL: " + targetUrl, e);
            }
            if (connection instanceof HttpURLConnection) {
                getErrorMessage((HttpURLConnection) connection, sb);
                response.setStatus(((HttpURLConnection) connection).getResponseCode());
            }
        } catch (FileNotFoundException e) {
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info("FileNotFoundException occured for URL: " + targetUrl);
            }
            if (connection instanceof HttpURLConnection) {
                getErrorMessage((HttpURLConnection) connection, sb);
                response.setStatus(((HttpURLConnection) connection).getResponseCode());
            }
            response.setContent(sb.toString());
        } catch (IOException e) {
            if (LOGGER.isInfoEnabled() && cachedProperties != null && cachedProperties.getBooleanPropertyValue("ct.restclient.print.ioexception", false)) {
                LOGGER.info("IOException occured for URL: " + targetUrl, e);
            }
            if (connection instanceof HttpURLConnection) {
                getErrorMessage((HttpURLConnection) connection, sb);
                response.setStatus(((HttpURLConnection) connection).getResponseCode());
            }
            response.setContent(sb.toString());
        } finally {
            sb = BaseStats.returnTmpStringBuilder(sb);
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    LOGGER.error("Error closing response stream reader", e);
                }
            }
        }

        return response;
    }

    private RestResponse postOrPut(RestRequest request, String requestMethod) throws IOException {
        RestResponse response = new RestResponse();
        String targetUrl = request.getTargetUrl();
        String payload = request.getPayload();

        if (targetUrl != null && payload != null) {
            BufferedReader reader = null;
            OutputStreamWriter writer = null;
            URLConnection connection;
            StringBuilder sb = BaseStats.tmpStringBuilder();

            try {
                URL url = new URL(targetUrl);
                connection = url.openConnection();
                connection.setConnectTimeout(request.getTimeout());
                connection.setReadTimeout(request.getTimeout());
                connection.setDoOutput(true);
                connection.setRequestProperty(CONTENT_LENGTH_HEADER, String.valueOf(payload.length()));
                connection.setRequestProperty(ACCEPT_ENCODING_HEADER, CONTENT_ENCODING_GZIP);

                StringBuilder cookieStr;
                List<Cookie> cookies = request.getCookies();
                if (cookies != null && cookies.size() > 0) {
                    cookieStr = new StringBuilder();
                    for (Cookie cookie : cookies) {
                        cookieStr.append(cookie.getName()).append("=").append(cookie.getValue()).append("; ");
                    }
                    cookieStr.setLength(cookieStr.length() - 2); // this is to remove the last 2 characters - "; "
                    connection.setRequestProperty(REQUEST_COOKIE_HEADER, cookieStr.toString());
                }

                String userName = request.getUserName();
                String password = request.getPassword();
                if (userName != null && password != null) {
                    Base64 encoder = new Base64();
                    String userPassword = userName + ":" + password;
                    String encodedUserPassword = new String(encoder.encode(userPassword.getBytes()));

                    connection.setRequestProperty(AUTHORIZATION_HEADER, "Basic " + encodedUserPassword);
                }

                String requestContentType = request.getContentType();
                if (requestContentType != null) {
                    connection.setRequestProperty(CONTENT_TYPE_HEADER, requestContentType);
                }

                Map<String, String> requestHeaders = request.getHeaders();
                LogData logData = LogThreadLocal.getLogData();
                String requestId = logData == null ? null : logData.getRequestId();
                if (StringUtils.isNotBlank(requestId)) {
                    if (requestHeaders == null) {
                        requestHeaders = new HashMap<String, String>();
                    }
                    requestHeaders.put(LogData.REQUEST_ID, requestId);
                }
                if (requestHeaders != null) {
                    for (String key : requestHeaders.keySet()) {
                        connection.setRequestProperty(key, requestHeaders.get(key));
                    }
                }

                if (connection instanceof HttpURLConnection) {
                    HttpURLConnection httpCon = (HttpURLConnection) connection;
                    httpCon.setRequestMethod(requestMethod);

                    boolean ignoreHostVerification = request.isIgnoreHostVerification();
                    if (ignoreHostVerification && httpCon instanceof HttpsURLConnection) {
                        HttpsURLConnection httpsCon = (HttpsURLConnection) httpCon;
                        httpsCon.setHostnameVerifier(ignoreHostnameVerifier);
                    }

                    String paramPrefix = request.getParamPrefix();
                    if (paramPrefix != null) {
                        payload = paramPrefix + URLEncoder.encode(payload, CharEncoding.UTF8.stringValue());
                    }

                    writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(payload);
                    writer.flush();
                }

                response.setHeaders(connection.getHeaderFields());

                String line;
                try {
                    InputStream responseStream = connection.getInputStream();
                    if (CONTENT_ENCODING_GZIP.equalsIgnoreCase(connection.getHeaderField(CONTENT_ENCODING_HEADER)) && request.isGunzipResponse()) {
                        responseStream = new GZIPInputStream(responseStream);
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("Got gzipped response from URL: " + targetUrl);
                        }
                    }
                    if (request.isByteResponse()) {
                        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                        int nRead;
                        byte[] data = new byte[4096];
                        while ((nRead = responseStream.read(data, 0, data.length)) != -1) {
                            buffer.write(data, 0, nRead);
                        }
                        buffer.flush();
                        byte[] bytes = buffer.toByteArray();
                        response.setBinaryContent(bytes);
                    } else {
                        reader = new BufferedReader(new InputStreamReader(responseStream));
                        while ((line = reader.readLine()) != null) {
                            sb.append(line).append(LINE_SEPARATOR);
                        }
                        // Removes all special characters.
                        response.setContent(new String(sb.toString().getBytes()));
                    }
                } catch (IOException e) {
                    if (connection instanceof HttpURLConnection) {
                        HttpURLConnection httpCon = (HttpURLConnection) connection;
                        getErrorMessage(httpCon, sb);

                        if (httpCon.getResponseCode() == 400 || httpCon.getResponseCode() == 401) {
                            LOGGER.warn("Got error response: " + sb.toString(), e);
                        } else if (connection != null && httpCon.getResponseCode() == 500) {
                            LOGGER.error("Got error response: " + sb.toString(), e);
                            if (targetUrl.contains("/book") || targetUrl.contains("/register")) {
                                LOGGER.error(String.format("Got error from: url-> %s, parameters->%s, postmessage is: %n %s", url, request.getParamPrefix(), payload));
                            }
                        }
                    }
                }
                if (connection instanceof HttpURLConnection) {
                    response.setStatus(((HttpURLConnection) connection).getResponseCode());
                } else {
                    response.setStatus(RestResponse.STATUS_OK);
                }
            } finally {
                BaseStats.returnTmpStringBuilder(sb);
                try {
                    if (writer != null) {
                        writer.close();
                    }
                    if (reader != null) {
                        reader.close();
                    }
                } catch (Exception e) {
                    LOGGER.error("Error closing response stream reader", e);
                }
            }
        } else {
            LOGGER.error(String.format("Skipped executing incomplete POST request with URL: %s, Payload: %s", targetUrl, payload));
        }

        return response;
    }

    @Override
    public RestResponse post(RestRequest request) throws IOException {
        return postOrPut(request, POST_METHOD);
    }

    @Override
    public RestResponse put(RestRequest request) throws IOException {
        return postOrPut(request, PUT_METHOD);
    }

    @Override
    public RestResponse delete(RestRequest request) throws IOException {
        return postOrPut(request, DELETE_METHOD);
    }

    /*
     * Function to extract the Http cookie header from HttpUrlConnection. The exact header name is 'Set-Cookie'.
     * @param connection
     * @return
     */
    private List<String> extractCookieHeaders(URLConnection connection) {
        // Extract all incoming cookies.
        // We need to explicitly extract the value of HTTP Set-Cookie header
        // as it is not returned when UrlConnection.getHeaderFields() is invoked
        // as of Java 1.6.0_20.
        String setCookieHeaderValue = connection.getHeaderField(RESPONSE_COOKIE_HEADER);
        List<String> cookiesHeaderValue = new ArrayList<String>();
        cookiesHeaderValue.add(setCookieHeaderValue);
        LOGGER.info("Extracted cookies from request = " + cookiesHeaderValue);
        return cookiesHeaderValue;
    }

    private void getErrorMessage(HttpURLConnection connection, StringBuilder sb) throws IOException {
        sb.setLength(0);
        BufferedReader reader;
        String line;

        InputStream errorStream = connection.getErrorStream();
        if (errorStream != null) {
            if (CONTENT_ENCODING_GZIP.equalsIgnoreCase(connection.getHeaderField(CONTENT_ENCODING_HEADER))) {
                errorStream = new GZIPInputStream(errorStream);
            }
            reader = new BufferedReader(new InputStreamReader(errorStream));
            while ((line = reader.readLine()) != null) {
                sb.append(line).append(LINE_SEPARATOR);
            }
        }
    }
}
