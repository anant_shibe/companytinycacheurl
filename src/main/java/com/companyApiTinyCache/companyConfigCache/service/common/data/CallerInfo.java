package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;

import java.util.Comparator;

public class CallerInfo {

    private String host;

    private int port;

    private String app;

    private String end_url;

    private volatile String source_url;

    private volatile long lct;

    private static class HostComparator implements Comparator<CallerInfo> {

        @Override
        public int compare(CallerInfo s1, CallerInfo s2) {
            int result = 0;
            if (s1 != null) {
                if (s2 != null) {
                    result = GenUtil.compare(s1.host, s2.host);
                    if (result == 0) {
                        result = s1.getPort() - s2.getPort();
                        if (result == 0) {
                            result = GenUtil.compare(s1.end_url, s2.end_url);
                            if (result == 0) {
                                result = GenUtil.compare(s1.app, s2.app);
                            }
                        }
                    }
                } else {
                    result = 1;
                }
            } else if (s2 != null) {
                result = -1;
            }

            return result;
        }

    }

    public static final Comparator<CallerInfo> hostComparator = new HostComparator();

    public final String getHost() {
        return host;
    }

    public final void setHost(String phost) {
        host = phost;
    }

    public final int getPort() {
        return port;
    }

    public final void setPort(int pport) {
        port = pport;
    }

    public final String getApp() {
        return app;
    }

    public final void setApp(String papp) {
        app = papp;
    }

    public final String getEnd_url() {
        return end_url;
    }

    public final void setEnd_url(String pendUrl) {
        end_url = pendUrl;
    }

    public final String getSource_url() {
        return source_url;
    }

    public final void setSource_url(String psourceUrl) {
        source_url = psourceUrl;
    }

    public final long getLct() {
        return lct;
    }

    public final void setLct(long plct) {
        lct = plct;
    }

}
