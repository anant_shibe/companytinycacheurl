package com.companyApiTinyCache.companyConfigCache.service.common.util.config;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Properties;

public class DefaultProperties extends HashMap<String, String> {

    private static final Log logger = LogFactory.getLog(DefaultProperties.class);

    /**
     *
     */
    private static final long serialVersionUID = 6334673531386913227L;

    public DefaultProperties() {
        String home = System.getProperty("user.home").replace('\\', '/');
        put("user.home", home);
        put("ct.common.ip", GenUtil.getLocalIpAddress());
        String hostName = getHostName();
        int i, len;
        if (hostName != null) {
            i = hostName.indexOf('.');
            if (i > 0) {
                hostName = hostName.substring(0, i);
            }
            put("ct.common.hostname", hostName);
            // Begin Get the last 1/2 characters of hostName if digits
            len = hostName.length();
            int maxIndex = len > 2 ? len - 2 : 0;
            for (i = len; i > maxIndex;) {
                i--;
                if (!Character.isDigit(hostName.charAt(i))) {
                    i++;
                    break;
                }
            }
            String hostId = "0";
            String serverType = hostName;
            if (i < len) {
                hostId = hostName.substring(i);
                serverType = hostName.substring(0, i);
            }
            put("ct.common.servertype", serverType);
            // End Get the last 1/2 characters of hostName if digits
            put("ct.common.hostid", hostId);
        }
        Properties systemProperties = System.getProperties();
        for (Object propertyNameObj : systemProperties.keySet()) {
            String propertyName = (String) propertyNameObj;
            if (propertyName.startsWith("ct.")) {
                put(propertyName, systemProperties.getProperty(propertyName));
            } else if (propertyName.startsWith("ct")) {
                put("ct.common." + propertyName.substring(2), systemProperties.getProperty(propertyName));
            }
        }
        if (get("ct.common.local.serverid") == null) {
            put("ct.common.local.serverid", "1");
        }
    }

    private static String getHostName() {
        String hostName = null;
        try {
            InetAddress localMachine = InetAddress.getLocalHost();
            hostName = localMachine.getHostName();
        } catch (UnknownHostException e) {
            logger.error("Error Getting Host", e);
        }

        return hostName;
    }
}
