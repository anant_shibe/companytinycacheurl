package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

import java.util.EnumMap;

/**
 * Enum Resources Holder for CachedEnumUrlResource.
 *
 * @author suresh
 *
 * @param <K> Key Enum Type of resource
 * @param <V> Value Type of resource
 */
@ClassExcludeCoverage
public class UrlResources<K extends Enum<K>, V> {

    private EnumMap<K, V> urlData;

    private long createTime;

    private Object custom; // Custom field for sub-classes

    public UrlResources(EnumMap<K, V> purlData) {
        urlData = purlData;
        createTime = System.currentTimeMillis();
    }

    /**
     * Getter for urlData.
     *
     * @return the urlData
     */
    public EnumMap<K, V> getUrlData() {
        return urlData;
    }

    /**
     * Getter for createTime.
     *
     * @return the createTime
     */
    public long getCreateTime() {
        return createTime;
    }

    /**
     * Getter for custom.
     *
     * @return the custom
     */
    public Object getCustom() {
        return custom;
    }

    /**
     * Setter for custom.
     *
     * @param pcustom
     *            the custom to set
     */
    public void setCustom(Object pcustom) {
        custom = pcustom;
    }

}
