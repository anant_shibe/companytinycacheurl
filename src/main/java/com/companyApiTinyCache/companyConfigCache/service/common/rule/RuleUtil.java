package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.jackson.DefaultDeserializer;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.template.JedisExecutor;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisSentinelPool;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Common RuleUtil.
 */
public abstract class RuleUtil {

    /**
     * Holds names of rule's misc attributes as String constants.
     */
    public static final class RuleMisc {
        public static final String CAMPAIGN = "campaign";
    }

    private static final Log LOGGER = LogFactory.getLog(RuleUtil.class);

    private static final ObjectMapper ruleObjectMapper = formRuleObjectMapper();

    private static final Set<String> searchRuleReplaceOps = new HashSet<String>(Arrays.asList(new String[] {"EXT"}));

    private static final Set<String> displayPromoRuleReplaceOps = new HashSet<String>(Arrays.asList(new String[] {"EXT", "EXT COM"}));

    private static final String[] PAYMENT_ATTRS = new String[] {"$paymentType", "$paymentSubtype", "$creditCardType", "$creditCardBankId", "$creditCardNumber", "$noOfEmis", "$debitCardBankId",
            "$debitCardNumber", "$netBankingBankId", "$expressCheckout", "$ccBinNumber", "$dcBinNumber"};

    public static final String SEARCH_TIME = "search-time";

    private static List<String> paymentAttributes = Arrays.asList(PAYMENT_ATTRS);

    public static List<String> getPaymentAttributes() {
        return paymentAttributes;
    }

    public static void addAttributeValues(Map<String, Object> condition, Map<String, Set<String>> attributeValues) {
        Object lhs = condition.get("lhs");
        Object rhs = condition.get("rhs");

        if (lhs instanceof String) {
            String lhsStr = (String) lhs;

            if (lhsStr.startsWith("$") && lhsStr.length() > 1) {
                lhsStr = lhsStr.substring(1);
                Set<String> values = attributeValues.get(lhsStr);

                if (values == null) {
                    values = new HashSet<String>();
                    attributeValues.put(lhsStr, values);
                }

                if (rhs instanceof String) {
                    String rhsStr = (String) rhs;

                    if (!rhsStr.startsWith("$")) {
                        values.add(rhsStr);
                    }
                } else if (rhs instanceof Collection) {
                    values.addAll((Collection<String>) rhs);
                }
            }
        }

        if (rhs instanceof String) {
            String rhsStr = (String) rhs;

            if (rhsStr.startsWith("$") && rhsStr.length() > 1) {
                rhsStr = rhsStr.substring(1);
                Set<String> values = attributeValues.get(rhsStr);

                if (values == null) {
                    values = new HashSet<String>();
                    attributeValues.put(rhsStr, values);
                }

                if (lhs instanceof String) {
                    String lhsStr = (String) lhs;

                    if (!lhsStr.startsWith("$")) {
                        values.add(lhsStr);
                    }
                } else if (lhs instanceof Collection) {
                    values.addAll((Collection<String>) lhs);
                }
            }
        }

        if (lhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) lhs;
            addAttributeValues(condition1, attributeValues);
        }

        if (rhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) rhs;
            addAttributeValues(condition1, attributeValues);
        }
    }

    public static void addAttributeValuesWithOperators(Map<String, Object> condition, Map<String, Set<Map<String, String>>> attributeValues) {
        Object lhs = condition.get("lhs");
        Object rhs = condition.get("rhs");
        String op = (String) condition.get("op");

        if (lhs instanceof String) {
            String lhsStr = (String) lhs;

            if (lhsStr.startsWith("$") && lhsStr.length() > 1) {
                lhsStr = lhsStr.substring(1);
                Set<Map<String, String>> values = attributeValues.get(lhsStr);

                if (values == null) {
                    values = new HashSet<Map<String, String>>();
                    attributeValues.put(lhsStr, values);
                }

                if (rhs instanceof String) {
                    String rhsStr = (String) rhs;

                    if (!rhsStr.startsWith("$")) {
                        Map<String, String> opVal = new HashMap<String, String>();
                        opVal.put(op, rhsStr);
                        values.add(opVal);
                    }
                } else if (rhs instanceof Collection) {
                    for (String rhsStr : (Collection<String>) rhs) {
                        Map<String, String> opVal = new HashMap<String, String>();
                        opVal.put(op, rhsStr);
                        values.add(opVal);
                    }
                }
            }
        }

        if (rhs instanceof String) {
            String rhsStr = (String) rhs;

            if (rhsStr.startsWith("$") && rhsStr.length() > 1) {
                rhsStr = rhsStr.substring(1);
                Set<Map<String, String>> values = attributeValues.get(rhsStr);

                if (values == null) {
                    values = new HashSet<Map<String, String>>();
                    attributeValues.put(rhsStr, values);
                }

                if (lhs instanceof String) {
                    String lhsStr = (String) lhs;

                    if (!lhsStr.startsWith("$")) {
                        Map<String, String> opVal = new HashMap<String, String>();
                        opVal.put(op, lhsStr);
                        values.add(opVal);
                    }
                } else if (lhs instanceof Collection) {
                    for (String lhsStr : (Collection<String>) lhs) {
                        Map<String, String> opVal = new HashMap<String, String>();
                        opVal.put(op, lhsStr);
                        values.add(opVal);
                    }
                }
            }
        }

        if (lhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) lhs;
            addAttributeValuesWithOperators(condition1, attributeValues);
        }

        if (rhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) rhs;
            addAttributeValuesWithOperators(condition1, attributeValues);
        }
    }

    public static Map<String, Object> createPaymentRuleForSearch(Map<String, Object> rateRule) {
        Map<String, Object> newRule = (Map<String, Object>) GenUtil.deepCopy((Serializable) rateRule);

        Map<String, Object> condition = (Map<String, Object>) newRule.get("condition");
        boolean conditionModified = createConditionForSearch(condition);

        if (conditionModified) {
            Map<String, Object> miscAttributes = (Map<String, Object>) newRule.get("misc");
            miscAttributes.put("payment_related", "true");
        }

        return newRule;
    }

    /*
     * createPGFeeRuleForSearch is only to populate condition_string during server boot-up as condition_string of a raterule will remain constant for all cases where in, consequence_string creation is
     * done during search as it depends on no_of_pax and will change for different searches
     */
    public static Map<String, Object> createPGFeeRuleForSearch(Map<String, Object> rateRule) {
        StringBuilder sbCondition = BaseStats.tmpStringBuilder();
        sbCondition.setLength(0);

        Map<String, Object> newRule = (Map<String, Object>) GenUtil.deepCopy((Serializable) rateRule);
        Map<String, Object> condition = (Map<String, Object>) newRule.get("condition");

        Map<String, Object> paymentConditions = new LinkedHashMap<String, Object>();
        addPaymentConditions(condition, paymentConditions);

        Set<String> paymentTypeList = (Set<String>) paymentConditions.get("paymentType");

        if (paymentTypeList != null) {
            for (String paymentType : paymentTypeList) {
                if ("CC".equalsIgnoreCase(paymentType)) {
                    if (sbCondition.length() > 0) {
                        sbCondition.append(";");
                    }
                    sbCondition.append(paymentType).append(":");
                    List<String> creditCardTypes = (List<String>) paymentConditions.get("creditCardType");
                    if (creditCardTypes != null) {
                        for (int i = 0; i < creditCardTypes.size(); i++) {
                            if (i == creditCardTypes.size() - 1) {
                                sbCondition.append(creditCardTypes.get(i));
                            } else {
                                sbCondition.append(creditCardTypes.get(i)).append(",");
                            }
                        }
                    }
                } else if ("DC".equalsIgnoreCase(paymentType)) {
                    if (sbCondition.length() > 0) {
                        sbCondition.append(";");
                    }
                    sbCondition.append(paymentType).append(":");
                    List<String> debitCardTypes = (List<String>) paymentConditions.get("debitCardBankId");
                    if (debitCardTypes != null) {
                        for (int i = 0; i < debitCardTypes.size(); i++) {
                            if (i == debitCardTypes.size() - 1) {
                                sbCondition.append(debitCardTypes.get(i));
                            } else {
                                sbCondition.append(debitCardTypes.get(i)).append(",");
                            }
                        }
                    }
                } else if ("NB".equalsIgnoreCase(paymentType)) {
                    if (sbCondition.length() > 0) {
                        sbCondition.append(";");
                    }
                    sbCondition.append(paymentType).append(":");
                    List<String> netBankTypes = (List<String>) paymentConditions.get("netBankingBankId");
                    if (netBankTypes != null) {
                        for (int i = 0; i < netBankTypes.size(); i++) {
                            if (i == netBankTypes.size() - 1) {
                                sbCondition.append(netBankTypes.get(i));
                            } else {
                                sbCondition.append(netBankTypes.get(i)).append(",");
                            }
                        }
                    }
                } else {
                    if (sbCondition.length() > 0) {
                        sbCondition.append(";");
                    }
                    sbCondition.append(paymentType).append(":");
                }
            }
        }

        boolean conditionModified = createConditionForSearch(condition);

        Map<String, Object> miscAttributes = (Map<String, Object>) newRule.get("misc");
        miscAttributes.put("condition_string", sbCondition.toString());

        if (conditionModified) {
            miscAttributes.put("payment_related", "true");
        }

        sbCondition = BaseStats.returnTmpStringBuilder(sbCondition);

        return newRule;
    }

    public static Map<String, Object> createPGChargeRuleForSearch(Map<String, Object> rateRule) {
        StringBuilder sbCondition = BaseStats.tmpStringBuilder();
        sbCondition.setLength(0);

        Map<String, Object> newRule = (Map<String, Object>) GenUtil.deepCopy((Serializable) rateRule);
        Map<String, Object> condition = (Map<String, Object>) newRule.get("condition");
        Map<String, Object> consequence = (Map<String, Object>) newRule.get("consequence");

        Map<String, Object> paymentConditions = new LinkedHashMap<String, Object>();
        addPaymentConditions(condition, paymentConditions);
        String consequenceString = createConsequenceString(consequence);

        Set<String> paymentTypeList = (Set<String>) paymentConditions.get("paymentType");

        if (paymentTypeList != null) {
            for (String paymentType : paymentTypeList) {
                if ("CC".equalsIgnoreCase(paymentType)) {
                    if (sbCondition.length() > 0) {
                        sbCondition.append(";");
                    }

                    sbCondition.append(paymentType).append(":{\"type\":[");
                    List<String> creditCardTypes = (List<String>) paymentConditions.get("creditCardType");

                    if (creditCardTypes != null) {
                        for (int i = 0; i < creditCardTypes.size(); i++) {
                            if (i == creditCardTypes.size() - 1) {
                                sbCondition.append(creditCardTypes.get(i));
                            } else {
                                sbCondition.append(creditCardTypes.get(i)).append(",");
                            }
                        }
                    }

                    sbCondition.append("],\"bank\":[");
                    List<String> creditCardBankIds = (List<String>) paymentConditions.get("creditCardBankId");

                    if (creditCardBankIds != null) {
                        for (int i = 0; i < creditCardBankIds.size(); i++) {
                            if (i == creditCardBankIds.size() - 1) {
                                sbCondition.append(creditCardBankIds.get(i));
                            } else {
                                sbCondition.append(creditCardBankIds.get(i)).append(",");
                            }
                        }
                    }

                    sbCondition.append("],\"emi\":[");
                    List<String> noOfEmis = (List<String>) paymentConditions.get("noOfEmis");

                    if (noOfEmis != null) {
                        for (int i = 0; i < noOfEmis.size(); i++) {
                            if (i == noOfEmis.size() - 1) {
                                sbCondition.append(noOfEmis.get(i));
                            } else {
                                sbCondition.append(noOfEmis.get(i)).append(",");
                            }
                        }
                    }

                    sbCondition.append("]}");
                } else if ("DC".equalsIgnoreCase(paymentType)) {
                    if (sbCondition.length() > 0) {
                        sbCondition.append(";");
                    }

                    sbCondition.append(paymentType).append(":{\"bank\":[");
                    List<String> debitCardBankIds = (List<String>) paymentConditions.get("debitCardBankId");

                    if (debitCardBankIds != null) {
                        for (int i = 0; i < debitCardBankIds.size(); i++) {
                            if (i == debitCardBankIds.size() - 1) {
                                sbCondition.append(debitCardBankIds.get(i));
                            } else {
                                sbCondition.append(debitCardBankIds.get(i)).append(",");
                            }
                        }
                    }

                    sbCondition.append("]}");
                } else if ("NB".equalsIgnoreCase(paymentType)) {
                    if (sbCondition.length() > 0) {
                        sbCondition.append(";");
                    }

                    sbCondition.append(paymentType).append(":{\"bank\":[");
                    List<String> netBankingBankIds = (List<String>) paymentConditions.get("netBankingBankId");

                    if (netBankingBankIds != null) {
                        for (int i = 0; i < netBankingBankIds.size(); i++) {
                            if (i == netBankingBankIds.size() - 1) {
                                sbCondition.append(netBankingBankIds.get(i));
                            } else {
                                sbCondition.append(netBankingBankIds.get(i)).append(",");
                            }
                        }
                    }

                    sbCondition.append("]}");
                } else if ("CA".equalsIgnoreCase(paymentType)) {
                    if (sbCondition.length() > 0) {
                        sbCondition.append(";");
                    }

                    sbCondition.append(paymentType).append(":");
                }
            }
        }

        boolean conditionModified = createConditionForSearch(condition);

        Map<String, Object> miscAttributes = (Map<String, Object>) newRule.get("misc");
        miscAttributes.put("condition_string", sbCondition.toString());
        miscAttributes.put("consequence_string", consequenceString);

        if (conditionModified) {
            miscAttributes.put("payment_related", "true");
        }

        sbCondition = BaseStats.returnTmpStringBuilder(sbCondition);

        return newRule;
    }

    private static void addPaymentConditions(Map<String, Object> condition, Map<String, Object> paymentConditions) {
        Object lhs = condition.get("lhs");
        Object rhs = condition.get("rhs");

        if (lhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) lhs;
            addPaymentConditions(condition1, paymentConditions);
        } else if (lhs instanceof String) {
            String lhsStr = (String) lhs;

            if ("$paymentType".equalsIgnoreCase(lhsStr)) {
                Set<String> paymentTypes = (Set<String>) paymentConditions.get("paymentType");

                if (paymentTypes == null) {
                    paymentTypes = new HashSet<String>();
                    paymentConditions.put("paymentType", paymentTypes);
                }

                if (rhs instanceof String) {
                    paymentTypes.add((String) rhs);
                } else if (rhs instanceof Collection) {
                    paymentTypes.addAll((Collection) rhs);
                }
            } else if ("$expressCheckout".equalsIgnoreCase(lhsStr)) {
                // $expressCheckout is not a payment-type
                // this is a non-ideal implementation to allow business to set different PGFee messages for expressCheckout
                Set<String> paymentTypes = (Set<String>) paymentConditions.get("paymentType");
                if (paymentTypes == null) {
                    paymentTypes = new HashSet<String>();
                    paymentConditions.put("paymentType", paymentTypes);
                }
                paymentTypes.add("EC");
            } else if ("$creditCardType".equalsIgnoreCase(lhsStr)) {
                List<String> creditCardTypes = (List<String>) paymentConditions.get("creditCardType");

                if (creditCardTypes == null) {
                    creditCardTypes = new ArrayList<String>();
                    paymentConditions.put("creditCardType", creditCardTypes);
                }

                if (rhs instanceof String) {
                    creditCardTypes.add((String) rhs);
                } else if (rhs instanceof Collection) {
                    creditCardTypes.addAll((Collection) rhs);
                }
            } else if ("$creditCardBankId".equalsIgnoreCase(lhsStr)) {
                List<String> creditCardBankIds = (List<String>) paymentConditions.get("creditCardBankId");

                if (creditCardBankIds == null) {
                    creditCardBankIds = new ArrayList<String>();
                    paymentConditions.put("creditCardBankId", creditCardBankIds);
                }

                if (rhs instanceof String) {
                    creditCardBankIds.add((String) rhs);
                } else if (rhs instanceof Collection) {
                    creditCardBankIds.addAll((Collection) rhs);
                }
            } else if ("$noOfEmis".equalsIgnoreCase(lhsStr)) {
                List<String> noOfEmis = (List<String>) paymentConditions.get("noOfEmis");

                if (noOfEmis == null) {
                    noOfEmis = new ArrayList<String>();
                    paymentConditions.put("noOfEmis", noOfEmis);
                }

                if (rhs instanceof String) {
                    noOfEmis.add((String) rhs);
                } else if (rhs instanceof Collection) {
                    noOfEmis.addAll((Collection) rhs);
                }

                // EMI is not a payment-type
                // this is a non-ideal implementation to allow business to set different PGFee messages for EMI
                Set<String> paymentTypes = (Set<String>) paymentConditions.get("paymentType");
                if (paymentTypes == null) {
                    paymentTypes = new HashSet<String>();
                    paymentConditions.put("paymentType", paymentTypes);
                }
                paymentTypes.add("EM");
            } else if ("$debitCardBankId".equalsIgnoreCase(lhsStr)) {
                List<String> debitCardBankIds = (List<String>) paymentConditions.get("debitCardBankId");

                if (debitCardBankIds == null) {
                    debitCardBankIds = new ArrayList<String>();
                    paymentConditions.put("debitCardBankId", debitCardBankIds);
                }

                if (rhs instanceof String) {
                    debitCardBankIds.add((String) rhs);
                } else if (rhs instanceof Collection) {
                    debitCardBankIds.addAll((Collection) rhs);
                }
            } else if ("$netBankingBankId".equalsIgnoreCase(lhsStr)) {
                List<String> netBankingBankIds = (List<String>) paymentConditions.get("netBankingBankId");

                if (netBankingBankIds == null) {
                    netBankingBankIds = new ArrayList<String>();
                    paymentConditions.put("netBankingBankId", netBankingBankIds);
                }

                if (rhs instanceof String) {
                    netBankingBankIds.add((String) rhs);
                } else if (rhs instanceof Collection) {
                    netBankingBankIds.addAll((Collection) rhs);
                }
            }
        }

        if (rhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) rhs;
            addPaymentConditions(condition1, paymentConditions);
        }
    }

    public static String createConsequenceString(Map<String, Object> consequence) {
        return createPerPaxConsequenceString(consequence, null);
    }

    public static String createPerPaxConsequenceString(Map<String, Object> consequence, Integer noOfPax) {
        if (noOfPax == null || noOfPax == 0) {
            noOfPax = 1;
        }
        String consequenceString = null;
        String value = null;

        Object lhs = consequence.get("lhs");
        Object rhs = consequence.get("rhs");
        Object op = consequence.get("op");

        if (lhs instanceof String && !((String) lhs).startsWith("$")) {
            value = (String) lhs;
        } else if (rhs instanceof String && !((String) rhs).startsWith("$")) {
            value = (String) rhs;
        }

        if (StringUtils.isNotBlank(value)) {
            if ("%".equalsIgnoreCase((String) op)) {
                consequenceString = "{\"pg\":{\"p\":" + value + "}}";
            } else {
                consequenceString = "{\"pg\":{\"f\":" + (Double.parseDouble(value) * noOfPax) + "}}";
            }
        }

        if (StringUtils.isEmpty(consequenceString) && (lhs instanceof Map)) {
            Map<String, Object> condition1 = (Map<String, Object>) lhs;
            consequenceString = createPerPaxConsequenceString(condition1, noOfPax);
        }

        if (StringUtils.isEmpty(consequenceString) && (rhs instanceof Map)) {
            Map<String, Object> condition1 = (Map<String, Object>) rhs;
            consequenceString = createPerPaxConsequenceString(condition1, noOfPax);
        }

        return consequenceString;
    }

    private static boolean createConditionForSearch(Map<String, Object> condition) {
        return createConditionForSearch(condition, searchRuleReplaceOps);
    }

    private static boolean createConditionForSearch(Map<String, Object> condition, Collection<String> replaceOps) {
        Object lhs = condition.get("lhs");
        Object rhs = condition.get("rhs");

        boolean conditionModified = false;
        boolean lhsModified = false;
        boolean rhsModified = false;

        if (lhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) lhs;
            String operator1 = (String) condition1.get("op");

            if (replaceOps.contains(operator1)) {
                conditionModified = true;
            } else {
                lhsModified = createConditionForSearch(condition1, replaceOps);
            }
        } else if (lhs instanceof String) {
            String lhsStr = (String) lhs;

            if (paymentAttributes.contains(lhsStr)) {
                conditionModified = true;
            }
        }

        if (rhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) rhs;
            String operator1 = (String) condition1.get("op");

            if (replaceOps.contains(operator1)) {
                conditionModified = true;
            } else {
                rhsModified = createConditionForSearch(condition1, replaceOps);
            }
        } else if (rhs instanceof String) {
            String rhsStr = (String) rhs;

            if (paymentAttributes.contains(rhsStr)) {
                conditionModified = true;
            }
        }

        if (conditionModified) {
            condition.put("lhs", "true");
            condition.put("rhs", null);
            condition.put("op", null);
        }

        return conditionModified || lhsModified || rhsModified;
    }

    public static double getCurrencyConversionRate(Map<String, Double> currencyConversion, String fromCurrency, String toCurrency) {
        double conversionRate = 1;

        if (!StringUtils.isEmpty(fromCurrency) && !StringUtils.isEmpty(toCurrency) && !fromCurrency.equalsIgnoreCase(toCurrency)) {

            if ("INR".equalsIgnoreCase(toCurrency)) {
                double rate = currencyConversion.get(fromCurrency);
                conversionRate = rate;
            } else if ("INR".equalsIgnoreCase(fromCurrency)) {
                double rate = currencyConversion.get(toCurrency);
                conversionRate = 1 / rate;
            } else {
                double rate1 = currencyConversion.get(fromCurrency);
                double rate2 = currencyConversion.get(toCurrency);
                conversionRate = rate1 / rate2;
            }
        }

        return conversionRate;
    }

    public static void populateCacheAttributes(Map<String, Object> attributes, Integer ruleId, String cacheLookup) {
        StringBuilder sb = BaseStats.tmpStringBuilder();

        if ("true".equals(cacheLookup)) {
            String userCacheKey = null;
            String companyCacheKey = null;
            String cookieCacheKey = null;

            String userId = (String) attributes.get("userId");
            String companyId = attributes.get("companyId") == null ? "" : (attributes.get("companyId")).toString();
            String cookie = (String) attributes.get("cookie");

            if (!StringUtils.isEmpty(userId)) {
                sb.setLength(0);
                sb.append(ruleId).append("u_").append(userId);
                userCacheKey = sb.toString();
            }

            if (!StringUtils.isEmpty(companyId)) {
                sb.setLength(0);
                sb.append(ruleId).append("c_").append(companyId);
                companyCacheKey = sb.toString();
            }

            if (!StringUtils.isEmpty(cookie)) {
                sb.setLength(0);
                sb.append(ruleId).append("a_").append(cookie);
                cookieCacheKey = sb.toString();
            }

            attributes.put("userCacheKey", userCacheKey);
            attributes.put("companyCacheKey", companyCacheKey);
            attributes.put("cookieCacheKey", cookieCacheKey);
        } else {
            attributes.put("userCacheKey", null);
            attributes.put("companyCacheKey", null);
            attributes.put("cookieCacheKey", null);
        }

        sb = BaseStats.returnTmpStringBuilder(sb);
    }

    public static boolean ruleHasntExpiredAlready(Map<String, Object> rateRule, List<String> expiryVariables) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Map<String, Object> condition = (Map<String, Object>) rateRule.get("condition");
        Map<String, Set<Map<String, String>>> attributeValues = new HashMap<String, Set<Map<String, String>>>();
        addAttributeValuesWithOperators(condition, attributeValues);
        for (String variable : expiryVariables) {
            if (attributeValues.get(variable) != null && !attributeValues.get(variable).isEmpty()) {
                Date today = sdf.parse(sdf.format(new Date()));
                for (Map<String, String> opToDate : attributeValues.get(variable)) {
                    if (opToDate.containsKey("<=")) {
                        Date ruleDate = sdf.parse(opToDate.get("<="));
                        if (ruleDate.before(today)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public static void getRegexMatchers(Map<String, Object> condition, HashMap<String, Pattern> matchers) {
        String operator = (String) condition.get("op");
        Object lhs = condition.get("lhs");
        Object rhs = condition.get("rhs");
        if ("=~".equals(operator)) {
            String regex = (String) rhs;
            matchers.put(regex, Pattern.compile(regex));
        } else {
            if (lhs instanceof Map) {
                getRegexMatchers((Map<String, Object>) lhs, matchers);
            }
            if (rhs instanceof Map) {
                getRegexMatchers((Map<String, Object>) rhs, matchers);
            }
        }
    }

    public static void populateCountCookieAttributes(Map<String, Object> attributes, Map<String, Object> miscAttributes) {
        if (miscAttributes == null) {
            return;
        }
        Set<String> countCookieAttrs = new HashSet<String>();
        attributes.put("countCookieAttrs", countCookieAttrs);
        Map<String, Object> countCookie = (Map<String, Object>) miscAttributes.get("ct_ct");
        if (countCookie == null) {
            return;
        }
        for (Map.Entry entry : countCookie.entrySet()) {
            String productKey = (String) entry.getKey();
            Map<String, Number> pageCounts = (Map<String, Number>) entry.getValue();
            for (Map.Entry pageCount : pageCounts.entrySet()) {
                String pageName = (String) pageCount.getKey();
                Number visitCount = (Number) pageCount.getValue();
                String countCookieAttribute = productKey + "_" + pageName;
                countCookieAttrs.add(countCookieAttribute);
                attributes.put(countCookieAttribute, visitCount);
            }
        }
    }

    /**
     * @param rateRule
     *            - rule from which display-promo rule should be obtained
     * @param displayPromoRuleTypeStr
     *            - "rule_type" value of the display-promo rule.
     * @return displayPromoRule
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> createDisplayPromoRule(Map<String, Object> rateRule, String displayPromoRuleTypeStr) {
        Map<String, Object> displayPromoRule = (Map<String, Object>) GenUtil.deepCopy((Serializable) rateRule);

        Map<String, Object> condition = (Map<String, Object>) displayPromoRule.get("condition");
        createConditionForSearch(condition, displayPromoRuleReplaceOps);

        displayPromoRule.put("consequence", null);
        displayPromoRule.put("rule_type", displayPromoRuleTypeStr);

        Map<String, Object> miscAttributes = (Map<String, Object>) displayPromoRule.get("misc");
        miscAttributes.put("auto_generated", "true");

        return displayPromoRule;
    }

    public static boolean shouldDisplayPromoForAnyCoupon(CachedProperties cachedProperties) {
        return cachedProperties != null && cachedProperties.getBooleanPropertyValue("ct.common.raterule.display.promo.for.any.coupon", false);
    }

    @MethodExcludeCoverage
    public static Set<String> getEmpCouponTravellerIds(Long userId, JedisSentinelPool jedisSentinelPool,JedisPool jedisPool) {
        try {
            String travellerIdsStr;
            if(jedisSentinelPool == null) {
                travellerIdsStr = (userId == null ? null : JedisExecutor.hget().execute(jedisPool, "EMP_COUPON_TRAVELLER_IDS", userId.toString()));
            }
            else{
                travellerIdsStr = (userId == null ? null : JedisExecutor.hget().execute(jedisSentinelPool, "EMP_COUPON_TRAVELLER_IDS", userId.toString()));

            }
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("userId:" + userId + " , travellerIds:" + travellerIdsStr);
            }

            if (StringUtils.isBlank(travellerIdsStr)) {
                return null;
            }

            Set<String> travellerIds = new HashSet<String>();

            for (String travellerId : travellerIdsStr.split(",")) {
                travellerIds.add(travellerId.trim());
            }

            return travellerIds;

        } catch (Exception e) {
            LOGGER.error("Exception during getEmpCouponTravellerIds for userId:" + userId, e);
            return null;
        }
    }

    public static String createCouponErrorMessageJson(Set<String> rejectedCoupons, String message, String promoLink, Set<String> reasons, String genericMsg, String header, String promo) {
        Map<String, Object> errorMessageMap = createErrorMap(rejectedCoupons,message,promoLink);
        List<Map<String, Object>> details = (List)errorMessageMap.get("details");
        Map<String, Object> detail = details.get(0);

        if(reasons!=null && !reasons.isEmpty()){
            detail.put("reason", reasons);
        }
        if(genericMsg!=null){
            detail.put("genericMsg", genericMsg);
        }

        if(header!=null){
            detail.put("headerMsg", header);
        }

        if(promo!=null){
            detail.put("promoMsg", promo);
        }

        return APIUtil.serializeObjectToJson(errorMessageMap);
    }

    public static String createCouponErrorMessageJson(Set<String> rejectedCoupons, String message, String promoLink) {
        Map<String, Object> errorMessageMap = createErrorMap(rejectedCoupons,message,promoLink);
        return APIUtil.serializeObjectToJson(errorMessageMap);
    }

    private static Map<String, Object> createErrorMap(Set<String> rejectedCoupons, String message, String promoLink){
        Map<String, Object> errorMessageMap = new HashMap<String, Object>();
        List<Map<String, Object>> details = new ArrayList<Map<String, Object>>();
        Map<String, Object> detail = new LinkedHashMap<String, Object>();

        errorMessageMap.put("details", details);
        details.add(detail);
        detail.put("message", message);

        if (StringUtils.isNotBlank(promoLink)) {
            detail.put("promo_link", promoLink);
        }
        detail.put("coupons", rejectedCoupons);
        detail.put("coupon_type", "error");
        return errorMessageMap;
    }

    /**
     * @return an ObjectMapper that maps json arrays to set using the DefaultDeserializer.
     */
    private static ObjectMapper formRuleObjectMapper() {
        ObjectMapper mapper = JsonUtil.getNewObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addDeserializer(Object.class, new DefaultDeserializer());
        mapper.registerModule(sm);

        /*CustomDeserializerFactory sf = new CustomDeserializerFactory();
        mapper.setDeserializerProvider(new StdDeserializerProvider(sf));
        sf.addSpecificMapping(Object.class, new DefaultDeserializer());*/
        return mapper;
    }

    public static ObjectMapper getRuleObjectMapper() {
        return ruleObjectMapper;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> createPromoRuleForSearch(Map<String, Object> rateRule, String displayPromoRuleTypeStr, List<String> replaceAttributes) {
        Map<String, Object> displayPromoRule = (Map<String, Object>) GenUtil.deepCopy((Serializable) rateRule);

        Map<String, Object> condition = (Map<String, Object>) displayPromoRule.get("condition");
        createConditionForSearch(condition, displayPromoRuleReplaceOps, replaceAttributes);

        displayPromoRule.put("consequence", null);
        displayPromoRule.put("rule_type", displayPromoRuleTypeStr);

        Map<String, Object> miscAttributes = (Map<String, Object>) displayPromoRule.get("misc");
        miscAttributes.put("auto_generated", "true");

        return displayPromoRule;
    }

    private static boolean createConditionForSearch(Map<String, Object> condition, Collection<String> replaceOps, List<String> replaceAttributes) {
        Object lhs = condition.get("lhs");
        Object rhs = condition.get("rhs");

        boolean conditionModified = false;
        boolean lhsModified = false;
        boolean rhsModified = false;

        if (lhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) lhs;
            String operator1 = (String) condition1.get("op");

            if (replaceOps.contains(operator1)) {
                conditionModified = true;
            } else {
                lhsModified = createConditionForSearch(condition1, replaceOps, replaceAttributes);
            }
        } else if (lhs instanceof String) {
            String lhsStr = (String) lhs;

            if (replaceAttributes.contains(lhsStr)) {
                conditionModified = true;
            }
        }

        if (rhs instanceof Map) {
            Map<String, Object> condition1 = (Map<String, Object>) rhs;
            String operator1 = (String) condition1.get("op");

            if (replaceOps.contains(operator1)) {
                conditionModified = true;
            } else {
                rhsModified = createConditionForSearch(condition1, replaceOps, replaceAttributes);
            }
        } else if (rhs instanceof String) {
            String rhsStr = (String) rhs;

            if (replaceAttributes.contains(rhsStr)) {
                conditionModified = true;
            }
        }

        if (conditionModified) {
            condition.put("lhs", "true");
            condition.put("rhs", null);
            condition.put("op", null);
        }

        return conditionModified || lhsModified || rhsModified;
    }
}
