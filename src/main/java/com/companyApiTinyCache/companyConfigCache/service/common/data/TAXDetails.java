/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author sumanth
 *
 */
@ClassExcludeCoverage
public class TAXDetails implements Serializable {


	private static final long serialVersionUID = 7264046852109081733L;

	private String registrationNumber;
	private String holderName;
	private String holderAddress;
	private String type;
	
	@JsonProperty("reg_number")
	public String getRegistrationNumber() {
		return registrationNumber;
	}

	@JsonProperty("reg_number")
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	@JsonProperty("holder_name")
	public String getHolderName() {
		return holderName;
	}

	@JsonProperty("holder_name")
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	@JsonProperty("holder_address")
	public String getHolderAddress() {
		return holderAddress;
	}

	@JsonProperty("holder_address")
	public void setHolderAddress(String holderAddress) {
		this.holderAddress = holderAddress;
	}

	@JsonProperty("tax_type")
	public String getType() {
		return type;
	}

	@JsonProperty("tax_type")
	public void setType(String type) {
		this.type = type;
	}

}
