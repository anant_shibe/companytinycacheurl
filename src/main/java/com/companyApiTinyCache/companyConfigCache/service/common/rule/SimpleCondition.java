package com.companyApiTinyCache.companyConfigCache.service.common.rule;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

import java.util.Map;

import static com.companyApiTinyCache.companyConfigCache.service.common.rule.RuleEvaluator.*;

/**
 * SimpleCondition.
 */
@ClassExcludeCoverage
public final class SimpleCondition {

    private SimpleCondition() { }

    public static boolean formatSatisfiedBy(Map<String, Object> expression) {
        if (expression == null) {
            return false;
        }

        if (!EXP_MAP_KEYS.equals(expression.keySet())) {
            return false;
        }

        String op = (String) expression.get("op");

        if (op == null) {
            return true;
        }

        if (REL_OPERATORS.contains(op)) {
            return true;
        }

        if (UNCAT_BOOL_VALUE_OPERATORS.contains(op)) {
            return true;
        }

        return false;
    }
}
