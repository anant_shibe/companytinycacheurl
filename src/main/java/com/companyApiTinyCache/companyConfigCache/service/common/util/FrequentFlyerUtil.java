package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.data.FrequentFlyerNumber;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FrequentFlyerUtil {

    /**
     * @param airlineCode
     *            is expected to be in upper-case and have surrounding whitespace trimmed.. this would avoid overhead of ignore-case comparison and trimming within this method
     */
    public static String getFreqFlyerMktgAirlineCode(String airlineCode) {
        if ("IT-RED".equals(airlineCode)) {
            return "IT";
        }

        if ("9W-K".equals(airlineCode)) {
            return "9W";
        }

        return airlineCode;
    }

    public static FrequentFlyerNumber getApplicableFFN(Collection<FrequentFlyerNumber> ffnCollection, String airlineCode) {
        if (ffnCollection != null) {
            List<FrequentFlyerNumber> applicableFFNlist = new ArrayList<FrequentFlyerNumber>();

            for (FrequentFlyerNumber ffn : ffnCollection) {
                if (ffn.isIssuedBy(airlineCode)) {
                    return ffn;
                }

                if (ffn.isApplicableTo(airlineCode)) {
                    applicableFFNlist.add(ffn);
                }
            }

            return applicableFFNlist.isEmpty() ? null : applicableFFNlist.get(0);
        }

        return null;
    }

}
