package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedResource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ServerInfoConfig extends ServerInfoAbstract {

    private static final long serialVersionUID = 6630555656536106469L;

    private static final Log log = LogFactory.getLog(ServerInfoConfig.class);

    List<CachedResourceInfo> cachedResources;

    public ServerInfoConfig() {
        super("CONFIG");
    }

    public void updateConfig(ApplicationContext ctx) {
        boolean isDebug = log.isDebugEnabled();
        log.debug("UPDATE CONFIG CALLED");
        if (ctx != null) {
            log.debug("ENTERING IF BLOCK");
            Map<String, CachedResource> cachedResourceBeans = ctx.getBeansOfType(CachedResource.class, false, false);
            log.debug("BEAN CHECK: " + cachedResourceBeans);
            if (cachedResourceBeans != null && cachedResourceBeans.size() > 0) {
                log.debug("GOT BEANS");
                if (cachedResources == null) {
                    cachedResources = new ArrayList<CachedResourceInfo>();
                }
                for (String beanName : cachedResourceBeans.keySet()) {
                    if (isDebug) {
                        log.debug("Adding BEAN " + beanName);
                    }
                    CachedResourceInfo cachedResource = new CachedResourceInfo();
                    CachedResource resource = cachedResourceBeans.get(beanName);
                    cachedResource.setBeanName(beanName);
                    cachedResource.setClassName(resource.getClass().getName());
                    cachedResource.setName(resource.getName());
                    cachedResources.add(cachedResource);
                }
            }
            log.debug("CALLING PARENT");
            updateConfig(ctx.getParent());
            setLastUpdated(System.currentTimeMillis());
        }
    }

    public List<CachedResourceInfo> getCachedResources() {
        return cachedResources;
    }

    public void setCachedResources(List<CachedResourceInfo> pcachedResources) {
        cachedResources = pcachedResources;
    }
}
