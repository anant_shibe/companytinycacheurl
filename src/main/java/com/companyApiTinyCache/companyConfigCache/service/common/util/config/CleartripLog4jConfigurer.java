package com.companyApiTinyCache.companyConfigCache.service.common.util.config;//package com.cleartrip.common.util.config;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//
//import org.springframework.beans.factory.BeanCreationException;
//import org.springframework.core.Ordered;
//import org.springframework.util.Log4jConfigurer;
//
//public class CleartripLog4jConfigurer implements Ordered {
//    private static final long DEFAULT_RELOAD_INTERVAL = 1000;
//
//    private long log4jReloadInterval = DEFAULT_RELOAD_INTERVAL;
//
//    /**
//     * Initializes the Log4jConfigurer by retrieving the property files in the correct order.
//     *
//     * @throws BeanCreationException
//     *             In the event that no file is found or one cannot be opened
//     */
//    public void init() throws BeanCreationException {
//        try {
//            String propertiesDirectory = DefaultProperties.getPropertiesDirectory();
//            initializeLog4JConfigurer(propertiesDirectory);
//        } catch (FileNotFoundException fnfe) {
//            throw new BeanCreationException("Failed configuring log4j", fnfe);
//        }
//    }
//
//    @Override
//    public int getOrder() {
//        return 0;
//    }
//
//    /**
//     * Sets the poll interval on the log4j configuration.
//     *
//     * @param theInterval
//     *            The new poll interval
//     */
//    public void setLog4jReloadInterval(long theInterval) {
//        log4jReloadInterval = theInterval;
//    }
//
//    /**
//     * Initialized log4j.
//     *
//     * @param propertiesDirectory
//     *            The directory with the foreign resources
//     * @throws FileNotFoundException
//     *             In no config can be found
//     */
//    private void initializeLog4JConfigurer(String propertiesDirectory) throws FileNotFoundException {
//        String defaultLog4jPropertiesPath = propertiesDirectory + "log4j.properties";
//        System.err.println("Loading log4j config from: " + defaultLog4jPropertiesPath);
//        File file = new File(defaultLog4jPropertiesPath);
//        if (file.exists()) {
//            Log4jConfigurer.initLogging(defaultLog4jPropertiesPath, log4jReloadInterval);
//        } else {
//            System.err.println("Log4j config file log4j.properties file does not exists in ${user.home}/cleartrip directory");
//        }
//    }
//}
