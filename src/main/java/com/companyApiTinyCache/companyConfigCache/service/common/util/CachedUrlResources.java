package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.EnumMap;

/**
 * Sub class of CachedEnumUrlResources mainly for caching htmls.
 *
 * @author suresh
 *
 * @param <K> Enumeration type enumerating the resources with path
 */
public class CachedUrlResources<K extends Enum<K> & EnumResourceName> extends CachedEnumUrlResources<K, String> {

    private static final Log log = LogFactory.getLog(CachedUrlResources.class);

    public CachedUrlResources(Class<K> c, CachedProperties pcommonCachedProperties, boolean loadResource) {
        super(c, pcommonCachedProperties, loadResource);
    }

    public CachedUrlResources(Class<K> c, CachedProperties pcommonCachedProperties) {
        this(c, pcommonCachedProperties, true);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.cleartrip.common.util.CachedEnumUrlResources#loadStreamResource(InputStream, Enum)
     */
    protected String loadStreamResource(InputStream is, K resourceKey) throws Exception {
        boolean isDebug = log.isDebugEnabled();
        BufferedReader in = null;
        in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        String inputLine;
        StringBuilder opBuf = new StringBuilder();
        PrintWriter opWriter = new PrintWriter(new StringBuilderWriter(opBuf));
        while ((inputLine = in.readLine()) != null) {
            opWriter.println(inputLine);
        }
        opWriter.flush();
        String resourceVal = opBuf.toString().trim();
        if (isDebug) {
            log.debug("LoadResource: new value = '" + resourceVal + '\'');
        }
        int notFoundIndex = resourceVal.indexOf("404");
        if (notFoundIndex > 0 && !Character.isDigit(resourceVal.charAt(notFoundIndex - 1)) && resourceVal.length() > notFoundIndex + 3 && !Character.isDigit(resourceVal.charAt(notFoundIndex + 3))) {
            resourceVal = null;
            if (isDebug) {
                log.debug("LoadResource: Ignoring above as it looks like 404 page");
            }
        }
        EnumMap<K, String> urlData = null;
        if (resource != null) {
            urlData = resource.getUrlData();
        }
        if (StringUtils.isBlank(resourceVal) || (urlData != null && resourceVal.equals(urlData.get(resourceKey)))) {
            if (isDebug) {
                log.debug("Ignoring above as it is blank or same as old value");
            }
            resourceVal = null;
        }
        if (isDebug && resourceVal != null) {
            log.debug("CONSIDERING above");
        }

        return resourceVal;
    }

    public EnumMap<K, String> updateUiAttributes(HttpServletRequest request) {
        int i;
        EnumMap<K, String> urlData = null;
        UrlResources<K, String> uiResources = getResource();
        if (uiResources != null) {
            urlData = uiResources.getUrlData();
            for (i = 0; i < enumVals.length; i++) {
                K resourceKey = enumVals[i];
                String urlContents = urlData.get(resourceKey);
                String resourceName = resourceKey.getResourceName();
                if (!StringUtils.isEmpty(urlContents) && (resourceName == null || resourceName.charAt(0) != '$')) {
                    request.setAttribute(resourceKey.name(), urlContents);
                }
            }
        }

        return urlData;
    }

    public EnumMap<K, String> updateVelocityContext(VelocityContext vc) {
        int i;
        UrlResources<K, String> uiResources = getResource();
        EnumMap<K, String> urlData = uiResources.getUrlData();
        for (i = 0; i < enumVals.length; i++) {
            K resourceKey = enumVals[i];
            String urlContents = urlData.get(resourceKey);
            String resourceName = resourceKey.getResourceName();
            if (!StringUtils.isEmpty(urlContents) && (resourceName == null || resourceName.charAt(0) != '$')) {
                vc.put(resourceKey.name(), urlContents);
            }
        }

        return urlData;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.cleartrip.common.util.CachedEnumUrlResources#getUrl()
     */
    @Override
    public String getUrl() {
        String defaultUrl = commonCachedProperties.getPropertyValue("ct.services.urlresources.base");
        if (defaultUrl != null && defaultUrl.charAt(defaultUrl.length() - 1) != '/') {
            defaultUrl += '/';
        }
        return defaultUrl;
    }
}
