package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.ApiType;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.DirectionType;

import java.io.Serializable;

/**
 *
 *
 */
public class ApiCallStats implements Serializable {

    private static final long serialVersionUID = -9063430127230238804L;

    private ApiType apiType;

    private DirectionType directionType;

    private String apiName;

    private String supplier;

    private String cacheKey;

    private long callIn;

    private long callOut;

    private String url;

    private String request;

    private String response;

    private int responseSize;

    private int responseCode;

    private boolean isSuccess;

    private String credentialInfo;

    private String hostIp;

    public String getConnectorSearchType() {
        return connectorSearchType;
    }

    public void setConnectorSearchType(String connectorSearchType) {
        this.connectorSearchType = connectorSearchType;
    }

    private String connectorSearchType;

    private CtStep ctStep;

    private String errorMessage;

    private ErrorCategory errorCategory;

    private ErrorSeverity severity;

    private int retryCount = 0;

    private String itineraryId;


    public ApiCallStats() {
        callIn = System.currentTimeMillis();
        isSuccess = true;
    }

    public ApiCallStats(long pcallIn, long pcallOut) {
        callIn = pcallIn;
        callOut = pcallOut;
        url = "CACHED";
        isSuccess = true;
    }

    /**
     * Getter for apiType.
     *
     * @return the apiType
     */
    public ApiType getApiType() {
        return apiType;
    }

    /**
     * Setter for apiType.
     *
     * @param papiType
     *            the apiType to set
     */
    public void setApiType(ApiType papiType) {
        apiType = papiType;
    }

    /**
     * Getter for apiName.
     *
     * @return the apiName
     */
    public String getApiName() {
        return apiName;
    }

    /**
     * Setter for apiName.
     *
     * @param papiName
     *            the apiName to set
     */
    public void setApiName(String papiName) {
        apiName = papiName;
    }

    /**
     * Getter for supplier.
     *
     * @return the supplier
     */
    public String getSupplier() {
        return supplier;
    }

    /**
     * Setter for supplier.
     *
     * @param psupplier
     *            the supplier to set
     */
    public void setSupplier(String psupplier) {
        supplier = psupplier;
    }

    /**
     * Getter for cacheKey.
     *
     * @return the cacheKey
     */
    public String getCacheKey() {
        return cacheKey;
    }

    /**
     * Setter for cacheKey.
     *
     * @param pcacheKey
     *            the cacheKey to set
     */
    public void setCacheKey(String pcacheKey) {
        cacheKey = pcacheKey;
    }

    /**
     * Getter for callIn.
     *
     * @return the callIn
     */
    public long getCallIn() {
        return callIn;
    }

    /**
     * Setter for callIn.
     *
     * @param pcallIn
     *            the callIn to set
     */
    public void setCallIn(long pcallIn) {
        callIn = pcallIn;
    }

    /**
     * Getter for callOut.
     *
     * @return the callOut
     */
    public long getCallOut() {
        return callOut;
    }

    /**
     * Setter for callOut.
     *
     * @param pcallOut
     *            the callOut to set
     */
    public void setCallOut(long pcallOut) {
        callOut = pcallOut;
    }

    /**
     * Getter for url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Setter for url.
     *
     * @param purl
     *            the url to set
     */
    public void setUrl(String purl) {
        url = purl;
    }

    /**
     * Getter for request.
     *
     * @return the request
     */
    public String getRequest() {
        return request;
    }

    /**
     * Setter for request.
     *
     * @param prequest
     *            the request to set
     */
    public void setRequest(String prequest) {
        request = prequest;
    }

    /**
     * Getter for response.
     *
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * Setter for response.
     *
     * @param presponse
     *            the response to set
     */
    public void setResponse(String presponse) {
        response = presponse;
    }

    /**
     * Getter for responseSize.
     *
     * @return the responseSize
     */
    public int getResponseSize() {
        return responseSize;
    }

    /**
     * Setter for responseSize.
     *
     * @param presponseSize
     *            the responseSize to set
     */
    public void setResponseSize(int presponseSize) {
        responseSize = presponseSize;
    }

    /**
     * Getter for responseCode.
     *
     * @return the responseCode
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * Setter for responseCode.
     *
     * @param presponseCode
     *            the responseCode to set
     */
    public void setResponseCode(int presponseCode) {
        responseCode = presponseCode;
    }

    /**
     * Getter for isSuccess.
     *
     * @return the isSuccess
     */
    public boolean isSuccess() {
        return isSuccess;
    }

    /**
     * Setter for isSuccess.
     *
     * @param pisSuccess
     *            the isSuccess to set
     */
    public void setSuccess(boolean pisSuccess) {
        isSuccess = pisSuccess;
    }

    /**
     * Getter for directionType.
     *
     * @return the directionType
     */
    public DirectionType getDirectionType() {
        return directionType;
    }

    /**
     * Setter for directionType.
     *
     * @param pdirectionType
     *            the directionType to set
     */
    public void setDirectionType(DirectionType pdirectionType) {
        directionType = pdirectionType;
    }

    public String getCredentialInfo() {
        return credentialInfo;
    }

    public void setCredentialInfo(String credentialInfo) {
        this.credentialInfo = credentialInfo;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public CtStep getCtStep() {
        return ctStep;
    }

    public void setCtStep(CtStep ctStep) {
        this.ctStep = ctStep;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorCategory getErrorCategory() {
        return errorCategory;
    }

    public void setErrorCategory(ErrorCategory errorCategory) {
        this.errorCategory = errorCategory;
    }

    public ErrorSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(ErrorSeverity severity) {
        this.severity = severity;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public String getItineraryId() {
        return itineraryId;
    }

    public void setItineraryId(String itineraryId) {
        this.itineraryId = itineraryId;
    }

    @Override
    public String toString() {
        return "ApiCallStats{" +
                "apiType=" + apiType +
                ", directionType=" + directionType +
                ", apiName='" + apiName + '\'' +
                ", supplier='" + supplier + '\'' +
                ", cacheKey='" + cacheKey + '\'' +
                ", callIn=" + callIn +
                ", callOut=" + callOut +
                ", url='" + url + '\'' +
                ", request='" + request + '\'' +
                ", response='" + response + '\'' +
                ", responseSize=" + responseSize +
                ", responseCode=" + responseCode +
                ", isSuccess=" + isSuccess +
                ", credentialInfo='" + credentialInfo + '\'' +
                ", hostIp='" + hostIp + '\'' +
                ", connectorSearchType='" + connectorSearchType + '\'' +
                ", ctStep=" + ctStep +
                ", errorMessage='" + errorMessage + '\'' +
                ", errorCategory=" + errorCategory +
                ", severity=" + severity +
                ", retryCount=" + retryCount +
                ", itineraryId='" + itineraryId + '\'' +
                '}';
    }
}
