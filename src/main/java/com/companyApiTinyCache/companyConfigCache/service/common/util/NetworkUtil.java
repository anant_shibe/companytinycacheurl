package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class NetworkUtil {

    private static final Log logger = LogFactory.getLog(NetworkUtil.class);

    private static String hostName;

    static {
        try {
            hostName =  InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            logger.error("Exception while getting hostname. Making it as Unknown", e);
            hostName = "unknown";
        }
    }

    public static String getHostName() {
        return hostName;
    }
}
