package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.codec.binary.Base64;

import java.io.Serializable;

public class AffiliateLogo implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = -8446138552833538164L;

    @JsonIgnore
    private int id;

    @JsonIgnore
    private int affiliateId;

    private String imageName;

    @JsonIgnore
    private byte[] image;

    private String base64EncodedImage;

    @JsonIgnore
    public int getId() {
        return id;
    }

    @JsonIgnore
    public void setId(int id) {
        this.id = id;
    }

    @JsonIgnore
    public int getAffiliateId() {
        return this.affiliateId;
    }

    @JsonIgnore
    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    @JsonProperty("resource_name")
    public String getImageName() {
        return this.imageName;
    }

    @JsonProperty("resource_name")
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @JsonIgnore
    public byte[] getImage() {
        return this.image;
    }

    @JsonIgnore
    public void setImage(byte[] img) {
        image = img.clone();
        base64EncodedImage = new String(Base64.encodeBase64(image));
    }

    @JsonProperty("resource_blob")
    public String getBase64EncodedImage() {
        return this.base64EncodedImage;
    }

    @JsonProperty("resource_blob")
    public void setBase64EncodedImage(String imgAscii) {
        if (imgAscii != null) {
            base64EncodedImage = new String(imgAscii);
            image = Base64.decodeBase64(base64EncodedImage.getBytes());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof AffiliateLogo == false)
            return false;
        AffiliateLogo affLogo = (AffiliateLogo) o;
        if ((imageName != null || affLogo.getImageName() != null) && !imageName.equals(affLogo.getImageName())) {
            return false;
        }
        if ((image != null || affLogo.getImage() != null) && !image.equals(affLogo.getImage())) {
            return false;
        }
        return true;
    }

}
