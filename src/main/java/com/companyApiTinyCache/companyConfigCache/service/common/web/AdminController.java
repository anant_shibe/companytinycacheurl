package com.companyApiTinyCache.companyConfigCache.service.common.web;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AdminController extends AbstractController {

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        ModelAndView modelAndView = new ModelAndView("admin");
        ArrayList<Memory> memories = AdminBean.getMemories();
        MemoryStat memoryStat = MemoryStat.getMemoryStat(memories);
        StringBuilder used = new StringBuilder();
        StringBuilder free = new StringBuilder();
        StringBuilder time = new StringBuilder();
        int count = 0;
        for (Memory memory : memories) {
            if (count > 0) {
                used.append(",");
                free.append(",");
                time.append(",");
            }
            used.append(memory.getUsedMB());
            free.append(memory.getFreeMB());
            time.append(memoryStat.getMaxTime() - memory.getTimeMin());
            count++;
        }

        String imgUrl = "http://chart.apis.google.com/chart?";
        String title = "&chtt=Memory Graph(MB/Min) at " + sdf.format(new Date());
        // String axisLabel = "&chxl=0:|Time|1:|Memory";
        // String axisLabelPos = "&chxp=0,30|1,30";
        long memInterval = ((memoryStat.getMaxMemory() - memoryStat.getMinMemory()) / 5) / 5 * 5;
        String axisMinMax = "&chxr=0," + ((memoryStat.getMaxTime() - memoryStat.getMinTime() + 9) / 10 * 10) + ",0,-10|1," + memoryStat.getMinMemory() / 5 * 5 + "," + memoryStat.getMaxMemory() + ","
                + memInterval;
        String chartXY = "&chxt=x,y";
        String chartSize = "&chs=1000x300";
        String unknown = "&cht=lxy&chdlp=b&chma=5,5,5,25|20";
        String lineStyle = "&chls=1|1";
        String chartColor = "&chco=3072F3,FF0000";
        String minMaxVal = "&chds=" + (memoryStat.getMaxTime() - memoryStat.getMinTime()) + ",0," + memoryStat.getMinMemory() + "," + memoryStat.getMaxMemory() + ","
                + (memoryStat.getMaxTime() - memoryStat.getMinTime()) + ",0," + memoryStat.getMinMemory() + "," + memoryStat.getMaxMemory();

        String data = "&chd=t:" + time + "|" + used + "|" + time + "|" + free;
        String dataLabel = "&chdl=Used|Free";

        imgUrl = imgUrl + title + axisMinMax + chartXY + chartSize + unknown + lineStyle + chartColor + minMaxVal + data + dataLabel;
        // imgUrl = imgUrl + axisLabel + axisLabelPos;
        modelAndView.addObject("IMG_SRC", imgUrl);
        return modelAndView;
    }

}
