package com.companyApiTinyCache.companyConfigCache.service.common.data;

public interface JSONSizeCheckerConstants {
	public static final String JSON_ENDPOINTS = "ct.common.jsonsizechecker.endpoints";
	public static final String JSON_MAX_SIZE = "ct.common.jsonsizechecker.maxSize";
	public static final String JSON_RETRY_TIMES = "ct.common.jsonsizechecker.retry";
	public static final String JSON_MAIL_RECIPIENTS = "ct.common.jsonsizechecker.mailRecipient";

}
