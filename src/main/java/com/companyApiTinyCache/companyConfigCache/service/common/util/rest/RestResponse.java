package com.companyApiTinyCache.companyConfigCache.service.common.util.rest;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;

import java.util.List;
import java.util.Map;

/**
 * Holds REST API call response details.
 */
public class RestResponse {

    public static final int STATUS_OK = 200;

    public static final int STATUS_REDIRECT = 302;

    private int status;

    private String content;

    private byte[] binaryContent;

    private Map<String, List<String>> headers;

    private static final RestResponse INTERNAL_ERROR = new RestResponse();

    static {
        INTERNAL_ERROR.setStatus(500);
    }

    /*
     * Used to indicate Rest call errors e.g UnknownHost, Malformed URL etc.
     */
    public static RestResponse internalError() {
        return INTERNAL_ERROR;
    }

    public RestResponse() {
    }

    /**
     * Convenience constructor for 200 response.
     * @param pcontent Content
     */
    public RestResponse(String pcontent) {
        status = STATUS_OK;
        content = pcontent;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, List<String>> headers) {
        this.headers = headers;
    }

    public boolean isOk() {
        return GenUtil.hasSuccessCode(status);
    }

    public byte[] getBinaryContent() {
        return binaryContent;
    }

    public void setBinaryContent(byte[] binaryContent) {
        this.binaryContent = binaryContent;
    }

    @Override
    public String toString() {
        String output = String.format("RestResponse {%n Status: %s %n Headers: %s %n Content: %s %n}", this.status, this.headers, this.content);
        return output;
    }
}
