package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.data.Address;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants.CommonTemplate;
import com.companyApiTinyCache.companyConfigCache.service.common.data.VisaDetails;
import com.companyApiTinyCache.companyConfigCache.service.common.notification.NotificationMediator;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedVms;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.web.servlet.ModelAndView;
import com.companyApiTinyCache.companyConfigCache.service.common.org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;

public class RiyaVisaServiceController extends SimpleFormController {

    private NotificationMediator notificationMediator;
    private CachedProperties commonCachedProperties;
    private CachedVms<CommonTemplate> cachedVms;

    public RiyaVisaServiceController(NotificationMediator pnotificationMediator, CachedProperties pcommonCachedProperties, CachedVms<CommonTemplate> pcachedVms) {
        this.notificationMediator = pnotificationMediator;
        this.commonCachedProperties = pcommonCachedProperties;
        this.cachedVms = pcachedVms;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView model = new ModelAndView("riyaVisaForm");
        boolean showSuccessMessage = false;
        if (isFormSubmit(request)) {
            VisaDetails visaDetails = getVisaDetails(request);
            EnumMap<CommonTemplate, Template> templates = cachedVms.getResource();
            Template t = templates.get(CommonTemplate.RIYA_MAIL_CONTENT);
            VelocityContext context = new VelocityContext();
            context.put("visaDetails", visaDetails);
            String emailBody = GenUtil.mergeTemplateIntoString(t, context);

            String fromAddress = commonCachedProperties.getPropertyValue("ct.services.riya.visa.email.from");
            String toListStr = commonCachedProperties.getPropertyValue("ct.services.riya.visa.email.to.list");
            String ccListStr = commonCachedProperties.getPropertyValue("ct.services.riya.visa.email.cc.list");
            String bccListStr = commonCachedProperties.getPropertyValue("ct.services.riya.visa.email.bcc.list");
            String subject = commonCachedProperties.getPropertyValue("ct.services.riya.visa.email.subject");

            Collection<String> toList = new ArrayList<String>();
            if (StringUtils.isNotBlank(toListStr)) {
                if (StringUtils.contains(toListStr, ",")) {
                    String[] toArray = toListStr.split(",");
                    for (int i = 0; i < toArray.length; i++) {
                        toList.add(toArray[i]);
                    }
                } else
                    toList.add(toListStr);
            }

            Collection<String> ccList = new ArrayList<String>();
            if (StringUtils.isNotBlank(ccListStr)) {
                if (StringUtils.contains(ccListStr, ",")) {
                    String[] ccArray = ccListStr.split(",");
                    for (int i = 0; i < ccArray.length; i++) {
                        ccList.add(ccArray[i]);
                    }
                } else
                    ccList.add(ccListStr);
            }

            Collection<String> bccList = new ArrayList<String>();
            if (StringUtils.isNotBlank(bccListStr)) {
                if (StringUtils.contains(bccListStr, ",")) {
                    String[] bccArray = bccListStr.split(",");
                    for (int i = 0; i < bccArray.length; i++) {
                        bccList.add(bccArray[i]);
                    }
                } else
                    bccList.add(bccListStr);
            }

            try {
                notificationMediator.transmitNotification(subject, emailBody, toList, ccList, bccList, fromAddress, null, null);
                showSuccessMessage = true;
            } catch (Exception e) {
                logger.error("Exception is:" + e.getMessage(), e);
            }

        }

        if (StringUtils.isNotBlank(request.getParameter("tocountry"))) {
            String tocountry = request.getParameter("tocountry");
            model.addObject("tocountry", tocountry);
        }
        model.addObject("showSuccessMessage", showSuccessMessage);
        return model;
    }

    private boolean isFormSubmit(HttpServletRequest request) {
        String contactPerson = request.getParameter("contact_name");
        if (StringUtils.isNotBlank(contactPerson))
            return true;
        return false;
    }

    private VisaDetails getVisaDetails(HttpServletRequest request) {
        VisaDetails visaDetails = new VisaDetails();
        String companyName = request.getParameter("company_name");
        String contactPerson = request.getParameter("contact_name");
        String email = request.getParameter("email");
        String mobile = request.getParameter("contact_number");
        String streetAddress = request.getParameter("address");
        String city = request.getParameter("city_name");
        String state = StringUtils.isNotBlank(request.getParameter("state_name")) ? request.getParameter("state_name") : "";
        String postalCode = request.getParameter("pincode");
        String countryName = request.getParameter("country_name");
        String visaCountryName = request.getParameter("visaCountryName");
        String visaType = request.getParameter("visa_type");
        String urgencyType = request.getParameter("urgency_type");
        String entries = request.getParameter("visa_entry_type");
        String adultStr = request.getParameter("adult");
        String childStr = request.getParameter("child");
        int adult = Integer.parseInt(adultStr);
        int child = Integer.parseInt(childStr);
        visaDetails.setCompanyName(companyName);
        visaDetails.setContactPerson(contactPerson);
        visaDetails.setEmail(email);
        visaDetails.setMobile(mobile);
        Address address = new Address();
        address.setStreetAddress(streetAddress);
        address.setCity(city);
        address.setState(state);
        address.setPincode(postalCode);
        address.setCountry(countryName);
        visaDetails.setAddress(address);
        visaDetails.setVisaCountryName(visaCountryName);
        visaDetails.setVisaType(visaType);
        visaDetails.setUrgencyType(urgencyType);
        visaDetails.setEntries(entries);
        visaDetails.setAdult(adult);
        visaDetails.setChild(child);
        return visaDetails;
    }

}
