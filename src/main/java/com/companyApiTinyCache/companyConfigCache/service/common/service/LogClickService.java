package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent.CtThreadPoolExecutorWrapper;
import com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent.SingleConsumerQueue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import redis.clients.jedis.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A service which fetches Clicks stored in a Redis server and stores them via a StoreClicks service.
 *
 * @author suresh
 *
 */
public class LogClickService implements DisposableBean {

    private static final String KEY_PREFIX = "clk_";

    private static final int MAX_PIPELINE_SIZE = 1000;

    private static final Log logger = LogFactory.getLog(LogClickService.class);

    private class StoreClicksTask implements Runnable {
        private boolean isRunning;

        private String pageCode;

        private StoreClicksHandler handler;

        @Override
        public void run() {
            boolean alreadyRunning = setRunning(true);
            if (alreadyRunning) {
                return;
            }
            Jedis jedis = null;
            try {
                long minAgeSeconds = commonCachedProperties.getIntPropertyValue("ct.common.clicklog.store.delay-minutes", 5) * 60;
                jedis = jedisPool.getResource();
                long startTime, endTime, lastProcessedTime = 0;
                do {
                    long nowSeconds = System.currentTimeMillis() / 1000;
                    startTime = lastProcessedTime;
                    endTime = nowSeconds - minAgeSeconds;
                    String idSetKey = getIdSetKey(pageCode);
                    Set<Tuple> idSet = jedis.zrangeByScoreWithScores(idSetKey, startTime, endTime, 0, 10);
                    if (idSet != null && idSet.size() > 0) {
                        for (Tuple tuple : idSet) {
                            String id = tuple.getElement();
                            lastProcessedTime = (long) tuple.getScore();
                            String idKey = getIdKey(pageCode, id);
                            String lockKey = idKey + "_lock";
                            if (lock(jedis, lockKey, 600)) {
                                List<String> clicks = jedis.lrange(idKey, 0, -1);
                                int len = 0;
                                boolean handlerSuccess = clicks != null;
                                if (clicks != null && (len = clicks.size()) > 0) {
                                    try {
                                        handlerSuccess = handler.saveClickData(pageCode, id, clicks);
                                    } catch (Exception e) {
                                        logger.error("Error Handling Click Log: " + clicks, e);
                                    }
                                    if (handlerSuccess) {
                                        jedis.ltrim(idKey, len, -1);
                                    }
                                }
                                if (handlerSuccess) {
                                    jedis.del(lockKey);
                                    jedis.watch(idKey);
                                    Long numClicks = jedis.llen(idKey);
                                    if (numClicks != null && numClicks == 0) {
                                        Transaction t = jedis.multi();
                                        t.zrem(idSetKey.getBytes(), id.getBytes());
                                        t.del(idKey.getBytes());
                                        t.exec();
                                    } else {
                                        jedis.unwatch();
                                    }
                                }
                            }
                        }
                    } else {
                        break;
                    }
                } while (true);
            } catch (Exception e) {
                logger.error("Error During Processing Clicks from Redis", e);
            } finally {
                setRunning(false);
                if (jedis != null) {
                    try {
                        jedisPool.returnResource(jedis);
                    } catch (Exception e) {
                        logger.error("LogClickService.StoreClicksTask: Error Returning jedis Connection: " + e.getMessage());
                    }
                }
            }
        }

        public void setPageCode(String ppageCode) {
            pageCode = ppageCode;
        }

        public void setHandler(StoreClicksHandler phandler) {
            handler = phandler;
        }

        public synchronized boolean isRunning() {
            return isRunning;
        }

        public synchronized boolean setRunning(boolean pisRunning) {
            boolean currentlyRunning = isRunning;
            isRunning = pisRunning;
            return currentlyRunning;
        }

    }

    private class LogClickTask implements Runnable {
        @Override
        public void run() {
            int i;
            boolean idle = false;
            String [] ids = new String[2];
            do {
                Jedis jedis = null;
                String clickParam = null;
                try {
                    // Wait till 5 seconds before idling current thread
                    clickParam = q.remove(5000000, true);
                    if (clickParam != null) {
                        i = 0;
                        // Create a pipeline with all items in Q
                        jedis = jedisPool.getResource();
                        Pipeline p = jedis.pipelined();
                        long nowSeconds = System.currentTimeMillis() / 1000;
                        do {
                            getPageCodeNInstanceId(clickParam, ids);
                            String pageCode = ids[0];
                            String instanceId = ids[1];
                            String idKey = getIdKey(pageCode, instanceId);
                            if (pageCode != null && instanceId != null) {
                                p.rpush(idKey, clickParam);
                                p.zadd(getIdSetKey(pageCode), nowSeconds, instanceId);
                            }
                            clickParam = q.remove(0, false);
                            i++;
                            if (clickParam != null && i % MAX_PIPELINE_SIZE == 0) {
                                p.sync();
                                p = jedis.pipelined();
                            }
                        } while (clickParam != null);
                        p.sync();
                    } else {
                        idle = true;
                    }
                } catch (Exception e) {
                    logger.error("Error Logging Click " + clickParam, e);
                } finally {
                    if (jedis != null) {
                        try {
                            jedisPool.returnResource(jedis);
                        } catch (Exception e) {
                            logger.error("LogClickService.LogClickTask: Error Returning jedis Connection: " + e.getMessage());
                        }
                    }
                }
            } while (!idle);
        }
    }

    private CachedProperties commonCachedProperties;

    private CtThreadPoolExecutorWrapper executor;

    private JedisPool jedisPool;

    private SingleConsumerQueue<String> q;

    private LogClickTask logClickTask;

    private Map<String, List<StoreClicksTask>> storeTasksMap;

    public LogClickService(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        // 2 = One for sending and one for receiving.
        poolConfig.setMaxTotal(10);
        String host = commonCachedProperties.getPropertyValue("ct.services.common.clicklog.redis.server");
        int pos = host.indexOf(':');
        int port = 6379;
        if (pos > 0) {
            port = Integer.parseInt(host.substring(pos + 1));
            host = host.substring(0, pos);
        }
        jedisPool = new JedisPool(poolConfig, host, port);
        q = new SingleConsumerQueue<String>();
        logClickTask = new LogClickTask();
        storeTasksMap = new ConcurrentHashMap<String, List<StoreClicksTask>>();
    }

    public static String getIdSetKey(String pageCode) {
        return KEY_PREFIX + pageCode;
    }

    public static String getIdKey(String pageCode, String instanceId) {
        return KEY_PREFIX + pageCode + '_' + instanceId;
    }

    public void getPageCodeNInstanceId(String clickParam, String [] ids) {
        ids[0] = null;
        ids[1] = null;
        // Get 2nd and 3rd fields
        int beginIndex = clickParam.indexOf('|');
        int endIndex = -1;
        if (beginIndex > 0) {
            beginIndex++;
            endIndex = clickParam.indexOf('|', beginIndex);
        }
        if (endIndex > 0) {
            ids[0] = clickParam.substring(beginIndex, endIndex);
            beginIndex = endIndex + 1;
            endIndex = clickParam.indexOf('|', beginIndex);
            if (endIndex > 0) {
                ids[1] = clickParam.substring(beginIndex, endIndex);
            }
        }
    }

    public static boolean isLogClickEnabled(String pageCode, CachedProperties commonCachedProperties) {
        boolean logClickEnabled = commonCachedProperties.getBooleanPropertyValue("ct.common.clicklog.enabled", false);
        if (pageCode != null) {
            logClickEnabled = commonCachedProperties.getBooleanPropertyValue("ct.common.clicklog." + pageCode.toLowerCase() + ".enabled", logClickEnabled);
        }

        return logClickEnabled;
    }

    public void logClick(String clickParam) {
        boolean logClickEnabled = false;
        int pos = clickParam.indexOf('|');
        if (pos > 0) {
            String pageCode = clickParam.substring(0, pos);
            logClickEnabled = isLogClickEnabled(pageCode, commonCachedProperties);
        }
        if (logClickEnabled) {
            // Add Local Server Time
            clickParam = System.currentTimeMillis() + "|" + clickParam;
            boolean acquired = q.add(clickParam, true);
            if (acquired) {
                executor.getThreadPoolExecutor().execute(logClickTask);
            }
        }
    }

    public void setStoreHandlers(String pageCode, StoreClicksHandler handler, StoreClicksHandler... moreHandlers) {
        List<StoreClicksTask> storeClicksTasks = new ArrayList<StoreClicksTask>(moreHandlers.length + 1);
        StoreClicksTask storeClicksTask = new StoreClicksTask();
        storeClicksTask.setPageCode(pageCode);
        storeClicksTask.setHandler(handler);
        storeClicksTasks.add(storeClicksTask);
        int i;
        for (i = 0; i < moreHandlers.length; i++) {
            storeClicksTasks.add(storeClicksTask);
            storeClicksTask = new StoreClicksTask();
            storeClicksTask.setPageCode(pageCode);
            storeClicksTask.setHandler(moreHandlers[i]);
        }
        storeTasksMap.put(pageCode, storeClicksTasks);
    }

    public void storeClicks() {
        boolean defaultStoreEnabled = commonCachedProperties.getBooleanPropertyValue("ct.common.clicklog.store.enabled", false);
        for (List<StoreClicksTask> tasks : storeTasksMap.values()) {
            boolean storeEnabled = false;
            if (tasks.size() > 0) {
                String storeProperty = "ct.common.clicklog.store." + tasks.get(0).pageCode.toLowerCase() + ".enabled";
                storeEnabled = commonCachedProperties.getBooleanPropertyValue(storeProperty, defaultStoreEnabled);
            }
            if (storeEnabled) {
                for (StoreClicksTask task : tasks) {
                    if (!task.isRunning()) {
                        executor.getThreadPoolExecutor().execute(task);
                    }
                }
            }
        }
    }

    private static boolean lock(Jedis jedis, String key, int seconds) {
        boolean locked = false;
        try {
            String val = jedis.get(key);
            if (val == null) {
                Transaction t = jedis.multi();
                Response<Long> setRes = t.setnx(key, "1");
                t.expire(key, seconds);
                t.exec();
                Long setSuccess = setRes.get();
                locked = setSuccess != null && setSuccess > 0;
            }
        } catch (Exception e) {
            logger.error("Exception when try to lock on " + key, e);
        }

        return locked;
    }

    // Cache Format: serverTime|pg|id|ts|eid|[p0,p1...
    // 1st 3 params server Time, pageCode and id are not added to modifiedParam
    public String modifyClickParam(String clickParam, long [] retMinTimeDiff) {
        long serverTime = 0, browserTime;
        int i, beginIndex = 0, endIndex, fourthFieldPos = 0;
        for (i = 0; i < 4 && (endIndex = clickParam.indexOf('|', beginIndex)) > 0; i++) {
            String token = clickParam.substring(beginIndex, endIndex);
            switch (i) {
            case 0:
                if (retMinTimeDiff != null) {
                    serverTime = Long.parseLong(token);
                }
                break;
            case 3:
                fourthFieldPos = beginIndex;
                if (retMinTimeDiff != null) {
                    browserTime = Long.parseLong(token);
                    long diff = serverTime - browserTime;
                    if (diff < retMinTimeDiff[0]) {
                        retMinTimeDiff[0] = diff;
                    }
                }
                break;
            default:
                break;
            }
            beginIndex = endIndex + 1;
        }

        return clickParam.substring(fourthFieldPos);
    }

    public List<String> getClicks(String pageCode, String id) {
        String idKey = getIdKey(pageCode, id);
        List<String> clicks = null;
        try {
            Jedis jedis = jedisPool.getResource();
            if (jedis != null) {
                try {
                    clicks = jedis.lrange(idKey, 0, -1);
                } finally {
                    try {
                        jedisPool.returnResource(jedis);
                    } catch (Exception e) {
                        logger.error("LogClickService.getClicks(): Error Returning jedis Connection: " + e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error Getting Clicks. Is redis server up?", e);
        }

        return clicks;
    }

    public boolean[] hasClicks(String pageCode, Collection<String> ids) {
        Jedis jedis = null;
        int i, len = 0;
        boolean [] statuses = new boolean[len];
        try {
            if (ids != null && (len = ids.size()) > 0) {
                jedis = jedisPool.getResource();
            }
            if (jedis != null) {
                try {
                    Pipeline p = jedis.pipelined();
                    for (String id : ids) {
                        if (id != null) {
                            String idKey = getIdKey(CommonConstants.HOTEL_SEARCH_RESULTS_PAGE_CODE, id);
                            p.llen(idKey);
                        }
                    }
                    List<Object> responses = p.syncAndReturnAll();
                    i = 0;
                    int len1 = responses.size();
                    for (String id : ids) {
                        if (id != null && i < len1) {
                            Long response = (Long) responses.get(i);
                            if (response != null && response > 0) {
                                statuses[i] = true;
                            }
                            i++;
                        }
                    }
                } finally {
                    try {
                        jedisPool.returnResource(jedis);
                    } catch (Exception e) {
                        logger.error("LogClickService.getClicks(): Error Returning jedis Connection: " + e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error Checking Clicks. Is redis server up?", e);
        }

        return statuses;
    }

    @Override
    public void destroy() throws Exception {
        jedisPool.destroy();
    }

    public void setExecutor(CtThreadPoolExecutorWrapper pExecutor) {
        executor = pExecutor;
    }
}
