package com.companyApiTinyCache.companyConfigCache.service.common.web;

import org.springframework.core.Ordered;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.AbstractCachingViewResolver;

import java.util.Locale;
import java.util.Set;

public class CtCommonViewResolver extends AbstractCachingViewResolver implements Ordered {

    private static final String COMMON_CONTEXT = "/common";

    private String prefix;
    private String suffix;
    private int order;

    @Override
    protected View loadView(String viewName, Locale locale) throws Exception {

        @SuppressWarnings("unchecked")
        Set<String> commonViewJSPs = getServletContext().getResourcePaths(prefix);
        String requestViewPath = prefix + viewName + suffix;

        if (commonViewJSPs.contains(requestViewPath)) {
            return new CtView(COMMON_CONTEXT, requestViewPath);
        }

        // Returning null eanble more ViewResolvers to be chained.
        return null;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

}
