package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.exceptions.JWTException;
import com.companyApiTinyCache.companyConfigCache.service.common.service.JWTService;
import com.companyApiTinyCache.companyConfigCache.service.common.service.SourceIdentifierService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * . This filter checks jwt token based on the source type and restrict further
 * access on failure
 *
 * @author sudhanshu
 */
@ClassExcludeCoverage
public class JWTFilter implements Filter {

	private static final Log logger = LogFactory.getLog(JWTFilter.class);
	private JWTService jwtService;
	private SourceIdentifierService sourceIdentifierService;
	private CachedProperties commonCachedProperties;

	private static final String JWT_SERVICE_ENABLED = "ct.common.services.jwtservice.enabled";

	public CachedProperties getCommonCachedProperties() {
		return commonCachedProperties;
	}

	public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
		this.commonCachedProperties = commonCachedProperties;
	}

	@Override
	public void destroy() {

	}

	public boolean isMulticity(ServletRequest request) {
		return StringUtils.isNotBlank(request.getParameter("from1"));
		// return StringUtils.isNotBlank(request.getParameter("multicity")) &&
		// request.getParameter("multicity").equalsIgnoreCase("true");
	}

	public boolean shouldFilter(ServletRequest request) {
		if (isMulticity(request)){
			return true;
		}
		//put more search type or call whole search criterion
		return false;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		if (commonCachedProperties.getBooleanPropertyValue(JWT_SERVICE_ENABLED, false) && shouldFilter(request)) {
			if (!(request instanceof HttpServletRequest) || !(response instanceof HttpServletResponse)) {
				throw new ServletException("Can only process HttpServletResponse");
			}
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			String source = sourceIdentifierService.getSource(httpRequest);
			switch (source) {
			case SourceIdentifierService.DESKTOP:
				try {
					jwtService.validateToken(httpRequest);
				} catch (JWTException ex) {
					logger.warn("Exception: " + ex.getMessage() + " RA: " + request.getRemoteAddr());
					httpResponse.setStatus(HttpServletResponse.SC_OK);
					// should be SC_FORBIDDEN
					return;
				}
			case SourceIdentifierService.MOBILE_APP:
				break;
			case SourceIdentifierService.MOBILE_WEBSITE:
				break;
			case SourceIdentifierService.THIRD_PARTY:// need to implement in
														// sourceIdentifierService
				break;
			case SourceIdentifierService.UNKNOWN:
			default:
				logger.warn("Unidentified Source. RA: " + request.getRemoteAddr());
				httpResponse.setStatus(HttpServletResponse.SC_OK);
				// should be SC_FORBIDDEN
				return;
			}
		}
		chain.doFilter(request, response);
	}

	protected ApplicationContext getContext(FilterConfig filterConfig) {
		return WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		ApplicationContext ctx = getContext(config);
		jwtService = (JWTService) ctx.getBean("jwtService");
		sourceIdentifierService = (SourceIdentifierService) ctx.getBean("sourceIdentifierService");
		commonCachedProperties = (CachedProperties) ctx.getBean("commonCachedProperties");
	}
}
