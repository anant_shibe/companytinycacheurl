package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.newrelic.api.agent.Insights;
//import com.newrelic.api.agent.NewRelic;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A class for handling a Reloadable Cached resource. The assumption is that the resource is central and is read only. When the resource is reloaded it is assumed that a new copy is created and the
 * existing copy is replaced.
 *
 * The class should be Thread-safe if the above conditions are met.
 *
 * The class is required to be extended by implementing loadResource method for loading the resource. Subclasses do not require synchronized blocks or locks for multi-threading.
 *
 * Code is derived from {@link ReloadableCachingAttributes}
 *
 * @param <R>
 *            Type of the Object to cache.
 *
 * @author Suresh
 *
 */
public abstract class CachedResource<R> implements InitializingBean {

    private static final Log logger = LogFactory.getLog(CachedResource.class);
    public static final String RESOURCE_FAILURE = "Resource_failure";

    private String name;

    /* -1 indicates cache forever. */
    private long cacheMillis;

    private long backgroundCacheMillis;

    /* Used to determine when to refresh */
    private volatile long lastCheckTimestamp;

    /**
     * Differs from lastCheckTimestamp if requiresRefresh returns false. This is not used internaly.
     */
    private long lastRefreshTimestamp;

    private long forceRefreshMillis;

    private long initializeMillis;

    /** Cache to hold already loaded properties. */
    protected volatile R resource;

    protected String digest;

    /**
     * Keeps track of if there was an error on last load to suppress repeated error logs due to basic configuration issues like missing property value. Sub classes should set this to false once it is
     * sure a new copy of the resource is going to be loaded to avoid missing out on errors, after fileModified time has changed for a file resource for example.
     */
    protected boolean isErrorOnLastLoad;

    protected String resourceContentType;

    protected boolean raiseRefreshError;

    /* This flag is set to true after complete initialization */
    private boolean initialized = false;

    private Lock lock;

    /* This flag is set to true for Test Cases to refresh resources lazily (time saving) */
    private static boolean lazyRefresh = false;

    protected static final String vertical = "common";

    protected static final String FETCH_RESOURCE = "/fetch";

    protected static final String STARTUP = "STARTUP";

    protected static final String VERSION = "version";

    public static final String AIR_SERVICE = "AirService";

    private static final String REQ_SOURCE = "source";

    private static Map<String, String> resourcesVersionMap = new ConcurrentHashMap<>();

    protected static final String CT_CONFIG_DNS = "http://ct-config.cltp.com:9001";

    private static final ExecutorService executorService = Executors.newFixedThreadPool(5);

  //  public static final Insights INSIGHTS = NewRelic.getAgent().getInsights();

    /**
     *
     */
    public CachedResource() {
        cacheMillis = -1;
        backgroundCacheMillis = -1;
        forceRefreshMillis = -1;
        initializeMillis = 120000; // 2 minutes
        lastCheckTimestamp = 0;
        lastRefreshTimestamp = 0;
        resourceContentType = "application/json";
        raiseRefreshError = true;
        lock = new ReentrantLock();
    }

    /**
     * Sub class should load the resource and return a new copy of the Refreshed resource. The existing resource got by getCachedResource should not be modified.
     *
     * @param cacheMillis
     *            The cacheMillis parameter that was passed to the refresh call
     * @param force
     *            Indicates that the resource should be refreshed even if it does not seem to be modified because more than forceRefresh millis have elapsed since last refresh.
     * @return A new copy of the resource or null if refresh is not required
     */
    protected abstract R loadResource(long cacheMillis, boolean force) throws Exception;

    public enum Vertical {
        //AIR("air"), HOTEL("hotel"), LOCAL("local"), PLATFORM("platform"), COMMON("common"), SHARED("shared");

        COMMON("common");

        private String name;

        Vertical(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    protected static String computeDigest(String s) {
        String digest = "0";
        try {
            if (s != null) {
                digest = GenUtil.digestString(s);
            }
        } catch (Exception e) {
            logger.error("Error computing digest", e);
        }
        return digest;
    }

    protected String computeDigest() {
        String description = getResourceDescription();
        return computeDigest(description);
    }

    /**
     * Refreshes the resource if older than specified time. Does the following 1. if lastCheckTime is not older than pcacheMillis just returns 2. Updates lastCheckTimestamp to current time 3. Calls
     * loadResource and if it returns non null, updates the resource and sets lastRefreshTimestamp to current time if there are no errors. While the loadResource is in progress other calls to
     * getResource return the old resource and calls to refresh return.
     *
     * @param pcacheMillis
     *            Refresh is performed if the last call is older than this
     * @param force
     *            True indicates resource is to be loaded even if not out of date, If false forceRefresh is determined by forceRefreshMillis if it is > 0.
     * @return Returns true if loadResource was called and it returned a non null value
     */
    public boolean refreshResource(long pcacheMillis, boolean force) {
        boolean isDebug = logger.isDebugEnabled();
        boolean locked = false;
        boolean refreshed = false;
        boolean forceRefresh = false;
        long now = System.currentTimeMillis();
        R newResource = null;
        if (isDebug) {
            logger.debug("Refresh called with cacheMillis : " + pcacheMillis + " force = " + force + ". forceRefreshMillis = " + forceRefreshMillis);
        }
        R currentResource = resource;
        if (currentResource == null) {
            locked = true;
            // If current resource is null wait for the lock so we are sure it is refreshed.
            lock.lock();
        } else {
            // If current resource is not null then continue with old resource if refresh is in progress
            locked = lock.tryLock();
        }
        if (locked) {
            try {
                if (now - lastCheckTimestamp >= pcacheMillis) {
                    forceRefresh = force || (forceRefreshMillis >= 0 && now - lastRefreshTimestamp > forceRefreshMillis);
                    if (isDebug) {
                        if (!force && forceRefreshMillis >= 0) {
                            logger.debug("Evaluated forceRefresh = " + forceRefresh + " as currentTime = " + new Date(now) + " (" + now + ')' + " lastRefreshTime = " + new Date(lastRefreshTimestamp)
                                    + " (" + lastRefreshTimestamp + ')' + " and diff = " + (now - lastRefreshTimestamp) + " > forceRefreshMillis");
                        }
                    }
                    lastCheckTimestamp = now;
                    long start = System.currentTimeMillis();
                    Exception ex = null;
                    String resourceName = name == null ? getClass().toString() : name;

                    // Load the resource
                    try {
                        logger.info("(INFO ONLY): GOING to load Resource: " + resourceName);
                        newResource = loadResource(pcacheMillis, forceRefresh);
                        if (isDebug) {
                            logger.debug("Got Refreshed Resource: " + GenUtil.toSring(newResource));
                        }
                        isErrorOnLastLoad = false;
                        refreshed = newResource != null;
                    } catch (Exception e) {
                        ex = e;
                        try {
                            Map <String, Object> attributes = new HashMap<>();
                            attributes.put("Resource_url", name);
                            attributes.put("response message", e);
                            //INSIGHTS.recordCustomEvent(RESOURCE_FAILURE, attributes);
                            logger.error("Unable to refresh or load resource: "+ this.getClass().getName() + " on "+ InetAddress.getLocalHost());
                        } catch (UnknownHostException e1) {
                            logger.error("Unable to get host information: "+ e1);
                        }
                        if (!isErrorOnLastLoad) {
                            isErrorOnLastLoad = true;
                            logger.info("Error Refreshing Resource " + name + ". Will retain current resource: ", e);
                        } else if (isDebug) {
                            logger.debug("Suppressing Exception from log4j", ex);
                        }
                    }

                    now = System.currentTimeMillis();
                    if (refreshed) {
                        resource = newResource;
                        lastRefreshTimestamp = now;
                        digest = computeDigest();
                        logger.info("(INFO ONLY): Successfully loaded Resource: " + resourceName + (forceRefreshMillis >= 0 ? " forceRefresh = " + forceRefresh : ", Load Time = " + (now - start)));
                    }
                    lastCheckTimestamp = now;
                    if (ex != null) {
                        if (raiseRefreshError) {
                            throw new RuntimeException("Error Refreshing Resource " + name, ex);
                        } else if (pcacheMillis == 0) {
                            logger.error("Error Refreshing Resource", ex);
                        }
                    }
                } else if (isDebug) {
                    logger.debug("Ignoring Refresh as currentTime = " + new Date(now) + " (" + now + ')' + " lastCheckTime = " + new Date(lastCheckTimestamp) + " (" + lastCheckTimestamp + ')'
                            + " and diff = " + (now - lastCheckTimestamp) + " < cacheMillis ");
                }
            } finally {
                lock.unlock();
            }
        }

        return refreshed;
    }

    /**
     * Refreshes the resource if older than specified time. Does the following 1. if lastCheckTime is not older than pcacheMillis just returns 2. Updates lastCheckTimestamp to current time 3. Calls
     * loadResource and if it returns non null, updates the resource and sets lastRefreshTimestamp to current time if there are no errors. While the loadResource is in progress other calls to
     * getResource return the old resource and calls to refresh return.
     *
     * @param pcacheMillis
     *            Refresh is performed if the last call is older than this
     * @return Returns true if loadResource was called and it returned a non null value
     */
    public boolean refreshResource(long pcacheMillis) {
        return refreshResource(pcacheMillis, false);
    }

    /**
     * calls refreshResource with backgroundCacheMillis or cacheMillis if backgroundCacheMillis is -1.
     *
     * @return true if successfully refreshed resource
     */
    public boolean refreshResource() {
        if (lazyRefresh && !initialized) {
            return false;
        }
        long checkMillis = backgroundCacheMillis;
        if (checkMillis <= 0) {
            checkMillis = cacheMillis;
        }

        boolean isRefreshed = refreshResource(checkMillis);
        return isRefreshed;
    }

    protected static String getJson(Object r) {
        String description = "<null>";
        if (r != null) {
            Writer sw = new StringBuilderWriter(new StringBuilder(1024));
            try {
                ObjectMapper mapper = JsonUtil.getObjectMapper();
                JsonFactory jsonFactory = new JsonFactory();
                JsonGenerator jgen = jsonFactory.createGenerator(sw);
                jgen.useDefaultPrettyPrinter();
                mapper.writeValue(jgen, r);
            } catch (Exception e) {
                PrintWriter pw = new PrintWriter(sw);
                pw.println();
                pw.println("** Below Errors were encountered when converting the resource to JSON. Make your resource bean JACKSON compatible or override CachedResource.getResourceDescription()::");
                logger.error("Exception is:" + e.getMessage(), e);
            }
            description = sw.toString();
        }
        return description;
    }

    public String getResourceDescription() {
        return getJson(resource);
    }

    /**
     * Getter for resourceContentType.
     *
     * @return the resourceContentType
     */
    public String getResourceContentType() {
        return resourceContentType;
    }

    /**
     * Getter for name. Just a name to identify in logs.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for name.
     *
     * @param pname
     *            the name to set
     */
    public void setName(String pname) {
        name = pname;
    }

    /**
     * Getter for resource. Calls refreshResource async if last check is before cacheMillis.
     *
     * @return the resource
     */
    public R getResource() {
        long refreshInterval = resource == null ? initializeMillis : cacheMillis;
        if (refreshInterval >= 0 && System.currentTimeMillis() - lastCheckTimestamp > refreshInterval) {
            CompletableFuture.runAsync(()->{
                try {
                            refreshResource(refreshInterval);
                        } catch (Exception e) {
                            logger.error("Error during periodic refresh of resource " + this, e);
                        }
                }, executorService);
        }
            /*
            if (backgroundCacheMillis >= 0) {
                log.error("Unexpected Refresh during getter for CachedResource " + this + " for resource " + res + " with Name: " + getName());
            }
            */

        return resource;
    }

    public String getDigest() {
        return digest;
    }

    /**
     * Setter for resource.
     *
     * @param presource
     *            the resource to set
     */
    public void setResource(R presource) {
        resource = presource;
    }

    /**
     * Getter for cacheMillis.
     *
     * @return the cacheMillis
     */
    public long getCacheMillis() {
        return cacheMillis;
    }

    /**
     * Setter for cacheMillis.
     *
     * @param pcacheMillis
     *            the cacheMillis to set
     */
    public void setCacheMillis(long pcacheMillis) {
        cacheMillis = pcacheMillis;
    }

    /**
     * Getter for lastCheckTimestamp.
     *
     * @return the lastCheckTimestamp
     */
    public long getLastCheckTimestamp() {
        return lastCheckTimestamp;
    }

    /**
     * Getter for lastRefreshTimestamp.
     *
     * @return the lastRefreshTimestamp
     */
    public synchronized long getLastRefreshTimestamp() {
        return lastRefreshTimestamp;
    }

    /**
     * Setter for forceRefreshMillis. If this is set >=0, The refreshResource method calls loadResource after forceRefreshMillis since last refresh irrespective of the return value it gets from
     * requiresRefresh.
     *
     * @param pforceRefreshMillis
     *            the forceRefreshMillis to set
     */
    public void setForceRefreshMillis(long pforceRefreshMillis) {
        forceRefreshMillis = pforceRefreshMillis;
    }

    public void setInitializeMillis(long pinitializeMillis) {
        initializeMillis = pinitializeMillis;
    }

    /**
     * Getter for backgroundCacheMillis.
     *
     * @return the backgroundCacheMillis
     */
    public long getBackgroundCacheMillis() {
        return backgroundCacheMillis;
    }

    /**
     * Setter for backgroundCacheMillis. Refresh Milliseconds to use when invoking from a background job. The default is -1 which falls back to cacheMillis. If this is set then there is a warning log
     * message if refresh happens on a getResource call.
     *
     * @param pbackgroundCacheMillis
     *            the backgroundCacheMillis to set
     */
    public void setBackgroundCacheMillis(long pbackgroundCacheMillis) {
        backgroundCacheMillis = pbackgroundCacheMillis;
    }

    @Override
    public final void afterPropertiesSet() throws Exception {
        onPropertiesSet();
        initialized = true;
    }

    public void onPropertiesSet() throws Exception {
    }

    /* This method should be called from TestCases */
    public static void setLazyRefresh(boolean lazyRefresh) {
        CachedResource.lazyRefresh = lazyRefresh;
    }

    /**
     *
     * @param currentUrl
     * @return
     * @throws IOException
     *   In case of v1 fetch api, version will be STARTUP for the first time which will always return the resource and its version.
     *   Version fetched needs to passed for further fetches, if there is no difference it will return 304 response code
     *   and we will retain previous version from memory
     *
     */
    protected RestResponse getUrlResponse(String currentUrl, int ttl, int timeout) throws IOException {

        RestResponse response;

        if (!isCtConfigV1Fetch(currentUrl)) {
            if(timeout!=0){
                response = RestUtil.get(currentUrl, null,null,null,timeout);
            }else{
                response = RestUtil.get(currentUrl, null);
            }
        } else {
            String resourceName = currentUrl.substring(currentUrl.lastIndexOf("/") + 1);
            String resourceVersionKey = this.getClass().getName() + ":" + resourceName;
            String version = resourcesVersionMap.get(resourceVersionKey) == null ? STARTUP : resourcesVersionMap.get(resourceVersionKey);

            String queryParam = VERSION + "=" + version;
            Map<String, List<String>> responseHeaders = new HashMap<>();
            Map<String, String> requestMap = new HashMap<>();
            requestMap.put(REQ_SOURCE, AIR_SERVICE);
            if(timeout!=0){
                response = RestUtil.get(currentUrl, queryParam, requestMap, responseHeaders,timeout);
            }else{
                response = RestUtil.get(currentUrl, queryParam, requestMap, responseHeaders);
            }
            if (response.getCode() == 200) {
                resourcesVersionMap.put(resourceVersionKey, responseHeaders.get(VERSION).get(0));
                VersionReader.cacheVersionForResource(resourceName, responseHeaders.get(VERSION).get(0), ttl);
            }
        }

        return response;
    }

    protected RestResponse getUrlResponse(String currentUrl, int ttl) throws IOException {
        return getUrlResponse(currentUrl,ttl,0);
    }

    private boolean isCtConfigV1Fetch(String url) {
        return url.contains(FETCH_RESOURCE) && url.contains("v1");
    }

}
