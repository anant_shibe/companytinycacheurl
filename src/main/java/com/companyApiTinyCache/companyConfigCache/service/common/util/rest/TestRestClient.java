package com.companyApiTinyCache.companyConfigCache.service.common.util.rest;

public class TestRestClient {

    public static void main(String[] args) {
        RestClient restClient = DefaultRestClientImpl.getInstance();
        RestRequest request = new RestRequest("http://www.google.com/");
        // request.setPayload("test data");
        try {
            System.out.println("Rest request: " + request);

            RestResponse response = restClient.post(request);

            System.out.println("Rest response: " + response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
