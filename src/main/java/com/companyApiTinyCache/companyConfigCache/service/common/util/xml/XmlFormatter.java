package com.companyApiTinyCache.companyConfigCache.service.common.util.xml;

import com.companyApiTinyCache.companyConfigCache.service.common.exceptions.CtRuntimeException;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * The Class XmlFormatter.
 */
@SuppressWarnings("restriction")
public final class XmlFormatter {
    private XmlFormatter() {
    }

    private static final int LINE_WIDTH = 160;

    /**
     * Format.
     * 
     * @param unformattedXml
     *            the unformatted xml
     * @return the string
     */
    public static String format(String unformattedXml) {
        try {
            final Document document = parseXmlFile(unformattedXml);

            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(LINE_WIDTH);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);

            return out.toString();
        } catch (IOException e) {
            throw new CtRuntimeException(e);
        }
    }

    private static Document parseXmlFile(String in) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(in));
            return db.parse(is);
        } catch (Exception e) {
            throw new CtRuntimeException(e);
        }
    }
}
