package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Calendar;

/**
 * UserCardDetail.
 */
public class UserCardDetail implements Serializable {

    private static final long serialVersionUID = -479832004525345148L;

    protected String id;
    protected String number;

    protected String expirationMonth;
    protected String expirationYear;

    protected String name;
    private String bank;

    protected String type;
    private String creditDebit;

    private String billFname;
    private String billLname;
    private String billAddress;
    private String billCity;
    private String billState;
    private String billCountry;
    private String billPcode;

    private boolean expired;
    private String formattedCardNumber;

    private String cardNumberHash;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    @JsonIgnore
    public String getFormattedCardNumber() {
        return formattedCardNumber;
    }

    @JsonProperty("number")
    public void setNumber(String number) {
        this.number = number;

        int splitSize = 4;
        String delimiter = "-";
        formattedCardNumber = GenUtil.formatString(number, splitSize, delimiter);
    }

    @JsonProperty("exp_month")
    public String getExpirationMonth() {
        return expirationMonth;
    }

    @JsonProperty("exp_month")
    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    @JsonProperty("exp_year")
    public String getExpirationYear() {
        return expirationYear;
    }

    @JsonProperty("exp_year")
    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("credit_debit")
    public String getCreditDebit() {
        return creditDebit;
    }

    @JsonProperty("credit_debit")
    public void setCreditDebit(String creditDebit) {
        this.creditDebit = creditDebit;
    }

    @JsonProperty("bill_fname")
    public String getBillFname() {
        return billFname;
    }

    @JsonProperty("bill_fname")
    public void setBillFname(String billFname) {
        this.billFname = billFname;
    }

    @JsonProperty("bill_lname")
    public String getBillLname() {
        return billLname;
    }

    @JsonProperty("bill_lname")
    public void setBillLname(String billLname) {
        this.billLname = billLname;
    }

    @JsonProperty("bill_address")
    public String getBillAddress() {
        return billAddress;
    }

    @JsonProperty("bill_address")
    public void setBillAddress(String billAddress) {
        this.billAddress = billAddress;
    }

    @JsonProperty("bill_city")
    public String getBillCity() {
        return billCity;
    }

    @JsonProperty("bill_city")
    public void setBillCity(String billCity) {
        this.billCity = billCity;
    }

    @JsonProperty("bill_state")
    public String getBillState() {
        return billState;
    }

    @JsonProperty("bill_state")
    public void setBillState(String billState) {
        this.billState = billState;
    }

    @JsonProperty("bill_country")
    public String getBillCountry() {
        return billCountry;
    }

    @JsonProperty("bill_country")
    public void setBillCountry(String billCountry) {
        this.billCountry = billCountry;
    }

    @JsonProperty("bill_pcode")
    public String getBillPcode() {
        return billPcode;
    }

    @JsonProperty("bill_pcode")
    public void setBillPcode(String billPcode) {
        this.billPcode = billPcode;
    }

    @JsonProperty("expired")
    public boolean isExpired() {

        Calendar day = Calendar.getInstance();
        int currMonth = day.get(Calendar.MONTH) + 1;
        int currYear = day.get(Calendar.YEAR);

        try {
            int expiryMonth = Integer.parseInt(expirationMonth);
            int expiryYear = Integer.parseInt(expirationYear);

            if (currYear > expiryYear) {
                expired = true;
            } else if (currYear == expiryYear && currMonth > expiryMonth) {
                expired = true;
            }
        } catch (NumberFormatException nfe) {
            // if error getting expiry date, set expired to false by default
            return false;
        }

        return expired;
    }

    @JsonProperty("expired")
    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    @JsonProperty("bank")
    public String getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getCardNumberHash() {
        return cardNumberHash;
    }

    @JsonProperty("cardNumberHash")
    public void setCardNumberHash(String cardNumberHash) {
        this.cardNumberHash = cardNumberHash;
    }
}
