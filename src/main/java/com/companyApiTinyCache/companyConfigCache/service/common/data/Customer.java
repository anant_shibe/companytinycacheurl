/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;

public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;
    // Customer Details
    private String userId; // affiliate user id of the contact person
    private String peopleId; // peopleId of the contact person
    private String title;
    private String firstName;
    private String lastName;
    private String userName;
    private String landline;
    private String mobile;
    private String email;

    private String shipAddress1;
    private String shipAddress2;
    private String shipCity;
    private String shipState;
    private String shipPinCode;
    private String shipCountry;

    /**
     * @return the userId
     */
    public String getPeopleId() {
        return peopleId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setPeopleId(String peopleId) {
        this.peopleId = peopleId;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the landline
     */
    public String getLandline() {
        return landline;
    }

    /**
     * @param landline
     *            the landline to set
     */
    public void setLandline(String landline) {
        this.landline = landline;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the shipAddress1
     */
    public String getShipAddress1() {
        return shipAddress1;
    }

    /**
     * @param shipAddress1
     *            the shipAddress1 to set
     */
    public void setShipAddress1(String shipAddress1) {
        this.shipAddress1 = shipAddress1;
    }

    /**
     * @return the shipAddress2
     */
    public String getShipAddress2() {
        return shipAddress2;
    }

    /**
     * @param shipAddress2
     *            the shipAddress2 to set
     */
    public void setShipAddress2(String shipAddress2) {
        this.shipAddress2 = shipAddress2;
    }

    /**
     * @return the shipCity
     */
    public String getShipCity() {
        return shipCity;
    }

    /**
     * @param shipCity
     *            the shipCity to set
     */
    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    /**
     * @return the shipState
     */
    public String getShipState() {
        return shipState;
    }

    /**
     * @param shipState
     *            the shipState to set
     */
    public void setShipState(String shipState) {
        this.shipState = shipState;
    }

    /**
     * @return the shipPinCode
     */
    public String getShipPinCode() {
        return shipPinCode;
    }

    /**
     * @param shipPinCode
     *            the shipPinCode to set
     */
    public void setShipPinCode(String shipPinCode) {
        this.shipPinCode = shipPinCode;
    }

    /**
     * @return the shipCountry
     */
    public String getShipCountry() {
        return shipCountry;
    }

    /**
     * @param shipCountry
     *            the shipCountry to set
     */
    public void setShipCountry(String shipCountry) {
        this.shipCountry = shipCountry;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
