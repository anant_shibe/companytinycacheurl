package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;

public class AffiliateInfo implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 744594340924978827L;

    private String address1;

    private String address2;

    private String city;

    private String state;

    private String zip;

    private String country;

    private String companyWebsite;

    private Integer channelType;

    private SourceType channelName;

    private String site_name;

    private String site_description;

    private String cust_service_phone;

    private String cust_service_email;

    private String domainName;

    @JsonProperty("address1")
    public String getAddress1() {
        return address1;
    }

    @JsonProperty("address1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @JsonProperty("address2")
    public String getAddress2() {
        return address2;
    }

    @JsonProperty("address2")
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("zip")
    public String getZip() {
        return zip;
    }

    @JsonProperty("zip")
    public void setZip(String zip) {
        this.zip = zip;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("company_website")
    public String getCompanyWebsite() {
        return companyWebsite;
    }

    @JsonProperty("company_website")
    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    /**
     * Refer getChannelType()
     * 
     * @return int
     * @author Ulhas
     */
    @JsonIgnore
    @Deprecated
    public int getAffiliateType() {
        return channelType;
    }

    /**
     * Refer setChannelType(int channelType)
     * 
     * @param affiliateType
     * @author Ulhas
     */
    @JsonIgnore
    @Deprecated
    public void setAffiliateType(int affiliateType) {
        this.channelType = affiliateType;
    }

    @JsonProperty("channel_type")
    public Integer getChannelType() {
        return channelType;
    }

    @JsonProperty("channel_type")
    public void setChannelType(int channelType) {
        this.channelType = new Integer(channelType);
    }

    /**
     * This parameter should not be updated. That is why its json binding is removed from the getter method
     * 
     * @return SourceType
     */
    @JsonIgnore
    public SourceType getChannelName() {
        return channelName;
    }

    @JsonProperty("channel_type_name")
    public void setChannelName(SourceType channelName) {
        this.channelName = channelName;
    }

    @JsonIgnore
    public String getSite_url() {
        return domainName;
    }

    @JsonIgnore
    public void setSite_url(String site_url) {
        /*
         * This line of code of converting it to lower case was written when a domain with camel case is created, but the browser url does not distinguish this difference and converts it to lower
         * case. hence when the query to db always validates this and if the site url is found with Camel casing it returns null...
         * 
         * NOTE: the other way of doing it might be to ignore case while querying..but when the browser url acts this way just store it that way.
         */
        if (site_url != null) {
            site_url = site_url.toLowerCase();
            this.domainName = site_url;
        }
    }

    @JsonProperty("site_name")
    public String getSite_name() {
        return site_name;
    }

    @JsonProperty("site_name")
    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    @JsonProperty("site_description")
    public String getSite_description() {
        return site_description;
    }

    @JsonProperty("site_description")
    public void setSite_description(String site_description) {
        this.site_description = site_description;
    }

    @JsonProperty("cust_service_phone")
    public String getCust_service_phone() {
        return cust_service_phone;
    }

    @JsonProperty("cust_service_phone")
    public void setCust_service_phone(String cust_service_phone) {
        this.cust_service_phone = cust_service_phone;
    }

    @JsonProperty("cust_service_email")
    public String getCust_service_email() {
        return cust_service_email;
    }

    @JsonProperty("cust_service_email")
    public void setCust_service_email(String cust_service_email) {
        this.cust_service_email = cust_service_email;
    }

    @JsonProperty("domain_name")
    public String getDomainName() {
        return domainName;
    }

    @JsonProperty("domain_name")
    public void setDomainName(String domainName) {
        if (domainName != null) {
            domainName = domainName.toLowerCase();
            this.domainName = domainName;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AffiliateInfo == false)
            return false;
        AffiliateInfo ai = (AffiliateInfo) obj;

        if ((address1 != null || ai.getAddress1() != null) && !GenUtil.equals(address1, ai.getAddress1())) {
            return false;
        }
        if ((address2 != null || ai.getAddress2() != null) && !GenUtil.equals(address2, ai.getAddress2())) {
            return false;
        }
        if ((city != null || ai.getCity() != null) && !GenUtil.equals(city, ai.getCity())) {
            return false;
        }
        if ((state != null || ai.getState() != null) && !GenUtil.equals(state, ai.getState())) {
            return false;
        }
        if ((country != null || ai.getCountry() != null) && !GenUtil.equals(country, ai.getCountry())) {
            return false;
        }
        if ((zip != null || ai.getZip() != null) && !GenUtil.equals(zip, ai.getZip())) {
            return false;
        }
        if ((companyWebsite != null || ai.getCompanyWebsite() != null) && !GenUtil.equals(companyWebsite, ai.getCompanyWebsite())) {
            return false;
        }
        if (channelType != ai.getChannelType()) {
            return false;
        }
        if ((site_name != null || ai.getSite_name() != null) && !GenUtil.equals(site_name, ai.getSite_name())) {
            return false;
        }
        if ((site_description != null || ai.getSite_description() != null) && !GenUtil.equals(site_description, ai.getSite_description())) {
            return false;
        }
        if ((cust_service_phone != null || ai.getCust_service_phone() != null) && !GenUtil.equals(cust_service_phone, ai.getCust_service_phone())) {
            return false;
        }
        if ((cust_service_email != null || ai.getCust_service_email() != null) && !GenUtil.equals(cust_service_email, ai.getCust_service_email())) {
            return false;
        }
        if ((domainName != null || ai.getDomainName() != null) && !GenUtil.equals(domainName, ai.getDomainName())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
