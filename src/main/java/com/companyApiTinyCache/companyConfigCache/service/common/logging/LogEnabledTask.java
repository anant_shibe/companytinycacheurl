package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.util.TimeKeeper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.MDC;


import java.util.concurrent.Callable;

/**
 *
 * log Enabled task.
 *
 * @param <C>
 */
public abstract class LogEnabledTask<C> implements Callable<C> {

    private BaseStats stats;
    private String traceID;
    private String callerID;

    // Assumed to be created in the main thread
    public LogEnabledTask(BaseStats pstats) {
        traceID = (String) MDC.get("traceID");
        callerID = (String) MDC.get("callerID");
        setStats(pstats);
    }

    public LogEnabledTask() {
        this(new CtBaseStats());
        traceID = (String) MDC.get("traceID");
        callerID = (String) MDC.get("callerID");
    }

    public abstract C callInternal() throws Exception;

    /*
     * (non-Javadoc)
     *
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public final C call() throws Exception {
        try {
            if(StringUtils.isNotEmpty(traceID)) {
                MDC.put("traceID", traceID);
            }
            if(StringUtils.isNotEmpty(callerID)) {
                MDC.put("callerID",callerID);
            }
            BaseStats.THREADLOCAL.set(stats);
            return callInternal();
        } finally {
            MDC.remove("traceID");
            MDC.remove("callerID");
            BaseStats.THREADLOCAL.remove();
        }
    }

    /**
     * Getter for stats.
     *
     * @return the stats
     */
    public BaseStats getStats() {
        return stats;
    }

    /**
     * Setter for stats.
     *
     * @param pstats
     *            the stats to set
     */
    public void setStats(BaseStats pstats) {
        stats = pstats;
        if (pstats != null) {
            BaseStats mainStats = BaseStats.threadLocal();
            if (mainStats != null) {
                stats.setLogLevel(mainStats.getLogLevel());
                stats.setLogRequestResponse(mainStats.isLogRequestResponse());
                if (mainStats.getTimeKeeper() != null) {
                    stats.setTimeKeeper(new TimeKeeper());
                }
            }
        }
    }

}
