package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filters that the connector search request with src=connector will be fulfilled or not.
 *
 * @author vaibhavmaniar
 */
public class ConnectorFilter implements Filter {

    private static final String QUERY_PARAM_SRC = "src";
    private static final String SRC_CONNECTOR = "connector";
    private static final String CONNECTOR_CALL_ERROR = "Connector search request is not supported.";

    private static final Log LOGGER = LogFactory.getLog(ConnectorFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
        LOGGER.debug("In Connector Filter init.");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (this.isConnectorSearchRequest(request)) {
            LOGGER.error(CONNECTOR_CALL_ERROR);
            ((HttpServletResponse) response).sendError(HttpStatus.FORBIDDEN.value(), CONNECTOR_CALL_ERROR);
        }
        chain.doFilter(request, response);
    }

    private boolean isConnectorSearchRequest(ServletRequest request) {
        String queryParamSrcVal = request.getParameter(QUERY_PARAM_SRC);
        return StringUtils.equals(queryParamSrcVal, SRC_CONNECTOR);
    }

    @Override
    public void destroy() {
        LOGGER.debug("In Connector Filter destroy.");
    }
}
