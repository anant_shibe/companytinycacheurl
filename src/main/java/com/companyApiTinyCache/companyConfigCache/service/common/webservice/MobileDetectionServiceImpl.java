package com.companyApiTinyCache.companyConfigCache.service.common.webservice;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedMobileHeadersResource;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import com.companyApiTinyCache.companyConfigCache.service.common.org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Implementation of restful webservice which tells if a useragent is from mobile source or not.
 * 
 * @author Amit Kumar Sharma
 * @version 1.0
 */
public class MobileDetectionServiceImpl extends SimpleFormController {

    public static final String USERAGENT_PARAM = "user_agent";
    protected CachedMobileHeadersResource cachedMobileHeadersResource;

    public MobileDetectionServiceImpl(CachedMobileHeadersResource cachedMobileHeadersResource) {

        this.cachedMobileHeadersResource = cachedMobileHeadersResource;
    }

    /*
     * (non-Javadoc) To handle bot get and post requests
     */
    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.mvc.AbstractFormController#formBackingObject(javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        return new Object();
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {

        // 1. Extract param 'user_agent' from request
        String userAgent = StringUtils.trimToEmpty(request.getParameter(USERAGENT_PARAM));

        // 2. Check if it is mobile
        Boolean isMobileDevice = cachedMobileHeadersResource.isMobile(userAgent);

        // 3. Write true/false to output stream
        byte[] bytes = isMobileDevice.toString().getBytes("UTF-8");
        response.getOutputStream().write(bytes);

        // 4. Boo!
        return null;
    }

}
