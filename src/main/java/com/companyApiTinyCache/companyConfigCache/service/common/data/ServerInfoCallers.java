package com.companyApiTinyCache.companyConfigCache.service.common.data;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class ServerInfoCallers extends ServerInfoAbstract {

    private static final Log log = LogFactory.getLog(ServerInfoCallers.class);

    private static final long serialVersionUID = -2750029343213004034L;

    private volatile List<CallerInfo> callers;

    public ServerInfoCallers() {
        super("CALLERS");
    }

    private CallerInfo findCaller(String callingHost, int callingPort, String endUrl, String callingApp) {
        int i, len = 0;
        List<CallerInfo> callersList = callers;
        CallerInfo caller = null;
        if (callersList == null) {
            return caller;
        }
        len = callersList.size();
        for (i = 0; i < len; i++) {
            CallerInfo testCaller = callersList.get(i);
            if (callingHost.equals(testCaller.getHost()) && callingPort == testCaller.getPort() && endUrl.equals(testCaller.getEnd_url()) && StringUtils.equals(callingApp, testCaller.getApp())) {
                caller = testCaller;
                break;
            }
        }

        return caller;
    }

    public void addCaller(String callerInfoHeader, String endUrl) {
        int i, len;
        if (callerInfoHeader != null) {
            try {
                String callingHost = null, callingApp = "none", sourceUrl = null;
                int callingPort = 0;
                StringTokenizer tokens = new StringTokenizer(callerInfoHeader, "|");
                i = 0;
                while (tokens.hasMoreTokens()) {
                    String field = tokens.nextToken();
                    switch (i) {
                    case 0:
                        callingHost = field;
                        break;
                    case 1:
                        callingPort = Integer.parseInt(field);
                        break;
                    case 2:
                        sourceUrl = field;
                        break;
                    case 3:
                        callingApp = field;
                        break;
                    default:
                        break;
                    }
                    i++;
                }
                if (endUrl != null && callingHost != null && callingPort > 0 && sourceUrl != null) {
                    CallerInfo caller = findCaller(callingHost, callingPort, endUrl, callingApp);
                    if (caller == null) {
                        synchronized (this) {
                            // 2nd Check for DCL
                            caller = findCaller(callingHost, callingPort, endUrl, callingApp);
                            if (caller == null) {
                                len = 0;
                                if (callers != null) {
                                    len = callers.size();
                                }
                                if (len < 32) {
                                    List<CallerInfo> newCallers = new ArrayList<CallerInfo>(len + 1);
                                    if (len > 0) {
                                        newCallers.addAll(callers);
                                    }
                                    caller = new CallerInfo();
                                    caller.setHost(callingHost);
                                    caller.setPort(callingPort);
                                    caller.setApp(callingApp);
                                    caller.setEnd_url(endUrl);
                                    newCallers.add(caller);
                                    Collections.sort(newCallers, CallerInfo.hostComparator);
                                    callers = Collections.unmodifiableList(newCallers);
                                    setLastUpdated(System.currentTimeMillis());
                                }
                            }
                        }
                    }
                    if (caller != null) {
                        caller.setSource_url(sourceUrl);
                        caller.setLct(System.currentTimeMillis() / 1000);
                    }
                }
            } catch (Exception e) {
                log.error("Error", e);
            }
        }
    }

    public final List<CallerInfo> getCallers() {
        return callers;
    }

    public synchronized final void setCallers(List<CallerInfo> pcallers) {
        List<CallerInfo> newCallers = pcallers;
        if (pcallers != null) {
            newCallers = new ArrayList<CallerInfo>(pcallers);
            Collections.sort(newCallers, CallerInfo.hostComparator);
            newCallers = Collections.unmodifiableList(newCallers);
        }
        callers = newCallers;
        setLastUpdated(System.currentTimeMillis());
    }

}
