package com.companyApiTinyCache.companyConfigCache.service.common.cache;

public interface ValueUpdater {

    public static final String SKIP_UPDATE = "__SKIP_UPDATE__";

    Object update(Object value, int tryIndex);
}
