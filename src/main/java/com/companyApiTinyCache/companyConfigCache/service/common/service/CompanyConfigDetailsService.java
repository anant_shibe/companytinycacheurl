package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Affiliate;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CompanyConfigDetailsBean;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CompanyConfigurationFilter;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@ClassExcludeCoverage
public class CompanyConfigDetailsService {

	private CachedProperties commonCachedProperties;

	public CachedProperties getCommonCachedProperties() {
		return commonCachedProperties;
	}

	public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
		this.commonCachedProperties = commonCachedProperties;
	}

	public CompanyConfigDetailsBean getCompanyDetails(HttpServletRequest request) {

		Object affiliateAttrib = request.getAttribute(CompanyConfigurationFilter.COMPANY_ATTRIB);
		Affiliate company = null;

		if (affiliateAttrib != null) {
			CompanyConfigDetailsBean companyConfigDetailsBean = new CompanyConfigDetailsBean();

			company = (Affiliate) affiliateAttrib;

			Map<String, String> companyConfig = company.getAffiliateConfigsMap();

			companyConfigDetailsBean.setCompanyId(company.getAffiliateId());
			companyConfigDetailsBean.setName(company.getCompanyName());
			companyConfigDetailsBean.setScreenName(companyConfig.get("COMPANY_SCREEN_NAME"));
			companyConfigDetailsBean.setCompanyCode(companyConfig.get("COMPANY_CODE"));
			companyConfigDetailsBean.setEnabledProducts(companyConfig.get("ENABLED_PRODUCTS"));
			companyConfigDetailsBean.setSupportNumber(companyConfig.get("SUPPORT_NUMBER"));
			companyConfigDetailsBean.setResourcepath(commonCachedProperties.getPropertyValue(""));
			companyConfigDetailsBean.setCouponCodeEnable(companyConfig.get("SHOW_COUPON_CODE"));
			companyConfigDetailsBean.setPrivacyLink(companyConfig.get("PRIVACY_LINK"));

			return companyConfigDetailsBean;

		}

		return null;
	}
}
