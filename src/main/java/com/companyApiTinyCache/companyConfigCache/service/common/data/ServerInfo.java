package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;

/**
 * Holds Information about the current App Server (Tomcat) used to update App Registry info held in memcache.
 * 
 * @author suresh
 * 
 */
public class ServerInfo implements Serializable, Cloneable {

    private static final long serialVersionUID = 9147253686467235332L;

    private long lastCleanupTime;

    private long lastServerListTouchTime;

    private long lastValidateTime;

    private int cleanupTries;

    private final ServerInfoBasic basicInfo = new ServerInfoBasic(this, true);

    private final ServerInfoConfig configInfo = new ServerInfoConfig();

    private final ServerInfoCallers callerInfo = new ServerInfoCallers();

    private final ServerInfoAbstract[] serverInfos = {basicInfo, configInfo, callerInfo};

    public ServerInfo() {
        lastCleanupTime = System.currentTimeMillis();
    }

    public ServerInfoAbstract findServerInfo(String name) {
        ServerInfoAbstract serverInfo = null;
        int i;
        for (i = 0; i < serverInfos.length; i++) {
            if (serverInfos[i].getName().equals(name)) {
                serverInfo = serverInfos[i];
                break;
            }
        }

        return serverInfo;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public long getLastCleanupTime() {
        return lastCleanupTime;
    }

    public void setLastCleanupTime(long plastCleanupTime) {
        lastCleanupTime = plastCleanupTime;
    }

    public long getLastServerListTouchTime() {
        return lastServerListTouchTime;
    }

    public void setLastServerListTouchTime(long plastServerListTouchTime) {
        lastServerListTouchTime = plastServerListTouchTime;
    }

    public long getLastValidateTime() {
        return lastValidateTime;
    }

    public void setLastValidateTime(long plastValidateTime) {
        lastValidateTime = plastValidateTime;
    }

    public int getCleanupTries() {
        return cleanupTries;
    }

    public void setCleanupTries(int pcleanupTries) {
        cleanupTries = pcleanupTries;
    }

    public ServerInfoBasic getBasicInfo() {
        return basicInfo;
    }

    public ServerInfoConfig getConfigInfo() {
        return configInfo;
    }

    public ServerInfoCallers getCallerInfo() {
        return callerInfo;
    }

    public ServerInfoAbstract[] getServerInfos() {
        return serverInfos;
    }
}
