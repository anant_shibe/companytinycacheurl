package com.companyApiTinyCache.companyConfigCache.service.common.cached.resources;

import com.companyApiTinyCache.companyConfigCache.service.common.cached.resources.datatypes.ZeroSectorAltRouteInfo;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedUrlResource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.StreamResourceLoader;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Map;

public class ZeroResultsSectorsResource extends CachedUrlResource<Map<String, ZeroSectorAltRouteInfo>> {

    public ZeroResultsSectorsResource(CachedProperties pcommonCachedProperties) throws MalformedURLException {

        super(pcommonCachedProperties, "ct.services.air.zero.results.json.url.ctconfig", null, false);
        setLoader(new ZeroResultsSectorsResourceLoader());
        refreshResource();
    }

    private class ZeroResultsSectorsResourceLoader implements StreamResourceLoader<Map<String, ZeroSectorAltRouteInfo>> {

        /**
         * Loads zero-flights.json
         */
        @Override
        public Map<String, ZeroSectorAltRouteInfo> loadStreamResource(InputStream is) throws Exception {

            Map<String, ZeroSectorAltRouteInfo> zeroSectorInfoLookup = JsonUtil.parseMap(is, String.class, ZeroSectorAltRouteInfo.class);

            return zeroSectorInfoLookup;
        }
    }
}
