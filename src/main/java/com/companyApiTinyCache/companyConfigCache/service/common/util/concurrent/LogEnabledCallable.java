package com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.LogData;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.LogThreadLocal;
import com.companyApiTinyCache.companyConfigCache.service.common.web.RequestTrackingListener.RequestInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.MDC;

import java.util.Vector;
import java.util.concurrent.Callable;

/**
 * User: Naveen Date: 8/1/13
 */

@ClassExcludeCoverage
public class LogEnabledCallable<T> implements Callable<T> {
    private LogData logData;
    private RequestInfo requestInfo;
    private Callable<T> taskToCall;
    String traceID = null;
    String callerID = null;

    public LogEnabledCallable(Callable<T> taskToCall) {
        traceID = (String) MDC.get("traceID");
        callerID = (String) MDC.get("callerID");
        this.taskToCall = taskToCall;
        logData = LogThreadLocal.getLogData();
        requestInfo = RequestInfo.threadLocal();
    }

    @Override
    public T call() throws Exception {
        if(StringUtils.isNotEmpty(traceID)) {
            MDC.put("traceID", traceID);
        }
        if(StringUtils.isNotEmpty(callerID)) {
            MDC.put("callerID",callerID);
        }
        LogThreadLocal.setLogData(logData);
        Vector<Thread> trackedChildRequests = null;
        RequestInfo ri = requestInfo;
        if (ri != null) {
            trackedChildRequests = requestInfo.getActiveChildThreads();
        }
        try {
            if (trackedChildRequests != null) {
                trackedChildRequests.add(Thread.currentThread());
            }
            return taskToCall.call();
        } finally {
            MDC.remove("traceID");
            MDC.remove("callerID");
            LogThreadLocal.getThreadLocal().remove();
            if (trackedChildRequests != null) {
                trackedChildRequests.remove(Thread.currentThread());
            }
        }
    }
}
