package com.companyApiTinyCache.companyConfigCache.service.common.util;

/**
 * Packs an array of positive integers into a smaller array by using only the number of bits required by the
 * maximum value. So if you have an array of numbers where the max value is 7 (Inventory array by day) the
 * resultant array will use only 3 bits per number and so contain round(375 * 3 / 32) = 36 integers = 144 bytes
 * for 375 inventories.
 *
 * @author suresh
 *
 */
public final class NumberArrayPacker {

    private static final int BITS_PER_INT = 32;

    private NumberArrayPacker() {
    }

    private static int countBits(int n) {
        int bits = 0;
        do {
            bits++;
            n >>>= 1;
        } while (n != 0);

        return bits;
    }

    public static int [] pack(int [] orgArray, int ofs, int len, int bits, int [] bitsPerNumberHolder) {
        int i, n = 0, bitCount = 0, endOfs = ofs + len;
        for (i = ofs; i < endOfs; i++) {
            n = orgArray[i];
            if (n < 0) {
                throw new IllegalArgumentException("Cannot pack negative number " + n + " at index " + i);
            }
            int nextBits = countBits(n);
            if (nextBits > bitCount) {
                bitCount = nextBits;
            }
        }
        if (bits <= 0) {
            bits = bitCount;
        }
        int packedSize = (bits * len + BITS_PER_INT - 1) / BITS_PER_INT;
        int [] packed = new int[packedSize];
        int currentPackedInt = 0;
        int currentPackedIndex = 0;
        int intraIntOffset = 0;
        for (i = ofs; i < endOfs; i++) {
            n = orgArray[i];
            currentPackedInt |= n << intraIntOffset;
            intraIntOffset += bits;
            if (intraIntOffset >= BITS_PER_INT) {
                packed[currentPackedIndex] = currentPackedInt;
                intraIntOffset -= BITS_PER_INT;
                currentPackedInt = n >> (bits - intraIntOffset);
                currentPackedIndex++;
            }
        }
        if (intraIntOffset > 0) {
            packed[currentPackedIndex] = currentPackedInt;
        }
        if (bitsPerNumberHolder != null) {
            bitsPerNumberHolder[0] = bits;
        }

        return packed;
    }

    public static int unPack(int [] packed, int from, int count, int bits, int [] unPacked, int ofs) {
        int curOfs = ofs;
        int mask = (1 << bits) - 1;
        int actualCount = (packed.length << 5) / bits - from; // << 5 is same as * 32
        if (count < actualCount) {
            actualCount = count;
        }
        if (actualCount > 0) {
            int bitIndex = from * bits;
            int packedIndex = bitIndex >> 5; // >> 5 Faster than / 32
            int intraIntOfs = bitIndex & 0x1F; // & 0x1F Faster than % 32
            int packedInt = packed[packedIndex] >>> intraIntOfs;
            int endOfs = ofs + actualCount;
            intraIntOfs += bits;
            outer:
            do {
                while (intraIntOfs <= BITS_PER_INT) {
                    unPacked[curOfs] = packedInt & mask;
                    curOfs++;
                    if (curOfs >= endOfs) {
                        break outer;
                    }
                    packedInt >>>= bits;
                    intraIntOfs += bits;
                }
                packedIndex++;
                int nextPackedInt = packed[packedIndex];
                intraIntOfs -= BITS_PER_INT;
                unPacked[curOfs] = (packedInt | (nextPackedInt << (bits - intraIntOfs))) & mask;
                curOfs++;
                packedInt = nextPackedInt >>> intraIntOfs;
                intraIntOfs += bits;
            } while (curOfs < endOfs);
        }

        return curOfs - ofs;
    }

    // Created to pass coverage check during build
    static void testConstructor() {
        new NumberArrayPacker();
    }
}
