package com.companyApiTinyCache.companyConfigCache.service.common.cache;

/**
 * A runtime property DTO.
 * 
 * @author sdesai
 */
public class MemcachedCacheProperties {

    private static final int INIT_CONN = 5;
    private static final int MAINT_SLEEP = 300;
    private static final int MAX_CONN = 250;
    private static final int COMPRESS_THRESHOLD = 1 * 1024;
    private static final int SOCKET_TIMEOUT_MS = 10000;

    // 1000 * 60 is seconds i.e. 1 minute
    // 1000 * 60 * 60 is one hours because 1 min * 60 is equal to 1 hr.
    // 5 * 24 * 60 * 60 * 1000
    private static final int MAX_IDLE = 5 * 24 * 60 * 60 * 1000;

    private int initConn = INIT_CONN;
    private int minConn = INIT_CONN;
    private int maxConn = MAX_CONN;
    private int maxIdle = MAX_IDLE;
    private int maintSleep = MAINT_SLEEP;
    private int socketTO = SOCKET_TIMEOUT_MS;
    private int compressThreshold = COMPRESS_THRESHOLD;

    private boolean compressEnable = true;
    private boolean nagle = false;

    /**
     * setter.
     * 
     * @param i
     *            -.
     */
    public void setInitConn(int i) {
        this.initConn = i;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public int getInitConn() {
        return initConn;
    }

    // "ct.search.memcached.minConnections"
    /**
     * setter.
     * 
     * @param m
     *            -.
     */
    public void setMinConn(int m) {
        this.minConn = m;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public int getMinConn() {
        return minConn;
    }

    // "ct.search.memcached.maxConnections"
    /**
     * setter.
     * 
     * @param m
     *            -.
     */
    public void setMaxConn(int m) {
        this.maxConn = m;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public int getMaxConn() {
        return maxConn;
    }

    // "ct.search.memcached.maxIdle"
    /**
     * setter.
     * 
     * @param m
     *            -.
     */
    public void setMaxIdle(int m) {
        this.maxIdle = m;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public int getMaxIdle() {
        return maxIdle;
    }

    // "ct.search.memcached.maintSleep"
    /**
     * setter.
     * 
     * @param m
     *            -.
     */
    public void setMaintSleep(int m) {
        this.maintSleep = m;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public int getMaintSleep() {
        return maintSleep;
    }

    // "ct.search.memcached.nagle"
    /**
     * setter.
     * 
     * @param m
     *            -.
     */
    public void setNagle(boolean m) {
        this.nagle = m;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public boolean getNagle() {
        return nagle;
    }

    // "ct.search.memcached.SocketTO"
    /**
     * setter.
     * 
     * @param m
     *            -.
     */
    public void setSocketTO(int m) {
        this.socketTO = m;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public int getSocketTO() {
        return socketTO;
    }

    // "ct.search.memcached.CompressEnable"
    /**
     * setter.
     * 
     * @param m
     *            -.
     */
    public void setCompressEnable(boolean m) {
        this.compressEnable = m;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public boolean getCompressEnable() {
        return compressEnable;
    }

    // "ct.search.memcached.CompressThreshold"
    /**
     * setter.
     * 
     * @param m
     *            -.
     */
    public void setCompressThreshold(int m) {
        this.compressThreshold = m;
    }

    /**
     * getter.
     * 
     * @return -.
     */
    public int getCompressThreshold() {
        return compressThreshold;
    }
}
