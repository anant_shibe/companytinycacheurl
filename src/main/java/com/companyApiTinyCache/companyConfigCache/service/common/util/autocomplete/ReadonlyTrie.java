package com.companyApiTinyCache.companyConfigCache.service.common.util.autocomplete;

/**
 * Represents a Readonly Trie through which one can search for prefixes
 * 
 * @author suresh
 * 
 * @param <E>
 *            Type of the Entity attached to each key. This is a User type to identify the Entities indexed for each key for example Hotel or Area or State.
 */
public interface ReadonlyTrie<E> {

    /**
     * Checks if the searched key is exactly matched
     * 
     * @param key
     *            Key to search
     * @return List of Entities stored against the key
     */
    E[] checkKey(String key);

    /**
     * Gets number of entities beginning with a prefix. Essentially abs(getNumEntitiesWithPrefixMatch(prefix))
     * 
     * @param prefix
     *            Prefix to check
     * @return number of keys begninning with the prefix. If "" is passed, returns the total number of keys
     */
    int getNumEntities(String prefix);

    /**
     * Gets number of keys beginning with a prefix. Returns -numKeys if prefix is also an exact match.
     * 
     * @param prefix
     *            Prefix to check
     * @return number of keys begninning with the prefix. If "" is passed, returns the total number of keys
     */
    int getNumEntitiesWithPrefixMatch(String prefix);

    /**
     * Returns an iterator for Keys with prefix as passed prefix
     * 
     * @param prefix
     *            prefix to match
     * @return Matching keys
     */
    TrieIterator<E> iterator(String prefix);
}
