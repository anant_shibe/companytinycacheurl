package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.data.ServerInfo;
import com.companyApiTinyCache.companyConfigCache.service.common.data.ThisServer;
import com.companyApiTinyCache.companyConfigCache.service.common.io.StringBuilderWriter;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.BaseStats;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.LogData;
import com.companyApiTinyCache.companyConfigCache.service.common.logging.LogThreadLocal;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CtAppRegistryFilter;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

/**
 * This class is depreciated. Use RestClient interface for making REST calls.
 * Using RestClient allows mocking REST calls from with unit tests.
 */
public final class RestUtil {

    /**
     * Constructor.
     */
    private RestUtil() {
    }

    public static final String HTTP_COOKIE_HEADER_NAME = "Set-Cookie";

    /**
     * Ignore Hostname Verifier.
     * @author abhishek
     */
    public static class IgnoreHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    public static final String ACCEPT_ENCODING = "Accept-Encoding";

    public static final String CONTENT_ENCODING = "Content-Encoding";

    public static final String GZIP = "gzip";

    private static final Log log = LogFactory.getLog(RestUtil.class);

    /**
     * @author cleartrip
     */
    public static enum Char_Encoding_Type_Enum {
        UTF
    };

    private static IgnoreHostnameVerifier ignoreHostnameVerifier = new IgnoreHostnameVerifier();

    private static volatile boolean callTraceEnabled;

    private static volatile ThisServer thisServer;

    public static void setCallTraceEnabled(boolean pcallTraceEnabled) {
        callTraceEnabled = pcallTraceEnabled;
    }

    public static void setThisServer(ThisServer pthisServer) {
        thisServer = pthisServer;
    }

    public static void setAuthenticator(String fuserName, String fpassWord) {
        final String userName = fuserName;
        final String pwd = fpassWord;
        try {
            Authenticator auth = new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userName, pwd.toCharArray());
                }
            };
            Authenticator.setDefault(auth);
        } catch (Exception e) {
            log.error(e);
        }
    }

    private static volatile Authenticator auth = null;

    public static void setResetAuthentication(CachedProperties commonCachedProperties) {
        String userName = null;
        String password = null;
        boolean isAuth = commonCachedProperties.getBooleanPropertyValue("ct.services.authentication.auth.enable", true);
        if (isAuth) {
            userName = commonCachedProperties.getPropertyValue("ct.services.authentication.proxy.username");
            password = commonCachedProperties.getPropertyValue("ct.services.authentication.proxy.password");
            if (StringUtils.isBlank(userName) || StringUtils.isBlank(password)) {
                isAuth = false;
            }
        }
        boolean isPrevAuth = (auth != null);
        if (isAuth != isPrevAuth) {
            if (isAuth) {
                final String fuserName = userName;
                final String fpassword = password;
                auth = new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(fuserName, fpassword.toCharArray());
                    }
                };
            } else {
                auth = null;
            }
        }
        Authenticator.setDefault(auth);
    }

    public static void setProxy(String proxyAddr, String proxyPort, String proxyAuthUsername, String proxyAuthPassword) {
        try {
            Authenticator.setDefault(new SimpleAuthenticator(proxyAuthUsername, proxyAuthPassword));

            Properties systemSettings = System.getProperties();
            systemSettings.put("proxySet", "true");
            systemSettings.put("http.proxyHost", proxyAddr);
            systemSettings.put("http.proxyPort", proxyPort);
            System.setProperties(systemSettings);
        } catch (Exception e) {
            log.error(e);
        }
    }

    /**
     * Remove the proxy setting.
     * (This is mainly for accessing local hosts during development life cycle)
     */
    public static void removeProxy() {
        try {
            Authenticator.setDefault(null);
            Properties systemSettings = System.getProperties();
            systemSettings.put("proxySet", "false");
            systemSettings.remove("http.proxyHost");
            systemSettings.remove("http.proxyPort");
            systemSettings.remove("proxySet");
            System.setProperties(systemSettings);
        } catch (Exception e) {
            log.error(e);
        }
    }

    private static void addCallerInfoHeader(URL url, URLConnection connection) {
        String host = null;
        int port = 0;
        boolean addHeader = callTraceEnabled;
        String calledHost = null;
        if (addHeader) {
            if (thisServer != null) {
                ServerInfo serverInfo = thisServer.getServerInfo();
                if (serverInfo != null) {
                    host = serverInfo.getBasicInfo().getHost();
                    port = serverInfo.getBasicInfo().getPort();
                }
            }
            if (host == null || port <= 0) {
                addHeader = false;
            }
        }
        if (addHeader) {
            if (url != null) {
                calledHost = url.getHost();
            }
            if (calledHost == null || !calledHost.endsWith(".cleartrip.com")) {
                addHeader = false;
            }
        }
        if (addHeader) {
            if (connection == null || !(connection instanceof HttpURLConnection)) {
                addHeader = false;
            }
        }
        if (addHeader) {
            StringBuilder sb = BaseStats.tmpStringBuilder();
            sb.append(host).append('|').append(port).append('|').append(calledHost);
            int calledPort = url.getPort();
            if (calledPort > 0) {
                sb.append(':').append(calledPort);
            }
            sb.append(url.getPath());
            String app = CtAppRegistryFilter.getCurrentApp();
            if (app != null) {
                sb.append('|').append(app);
            }
            String callerInfoStr = sb.toString();
            connection.setRequestProperty(CtAppRegistryFilter.CALL_TRACE_HEADER, callerInfoStr);
            BaseStats.returnTmpStringBuilder(sb);
        }
    }

    /**
     * Sends a GET request to the given url and returns the response as a String.
     * @param urlLocation String
     * @param queryString String
     * @return String
     * @throws IOException IOException
     */
    public static RestResponse get(String urlLocation, String queryString) throws IOException {
        return get(urlLocation, queryString, null, null);
    }

    /**
     * Sends a GET request to the given url and returns the response as a String.
     * @param urlLocation String
     * @param queryString String
     * @param requestHeaders Map
     * @param responseHeaders Map
     * @param dontLog List
     * @param encodingType Char_Encoding_Type_Enum
     * @return String
     * @throws IOException IOException
     */
    public static RestResponse get(String urlLocation, String queryString, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders, List<String> dontLog,
            Char_Encoding_Type_Enum encodingType) throws IOException {
        return get(urlLocation, queryString, requestHeaders, responseHeaders, dontLog, encodingType, 120000);
    }

    /**
     * Sends a GET request to the given url and returns the response as a String.
     * @param urlLocation String
     * @param queryString String
     * @param requestHeaders Map
     * @param responseHeaders Map
     * @return String
     * @throws IOException IOException
     */
    public static RestResponse get(String urlLocation, String queryString, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders) throws IOException {
        return get(urlLocation, queryString, requestHeaders, responseHeaders, null, Char_Encoding_Type_Enum.UTF, 120000);
    }

    /**
     * Sends a GET request to the given url and returns the response as a String.
     * @param urlLocation String
     * @param queryString String
     * @param requestHeaders Map
     * @param responseHeaders Map
     * @param timeout int
     * @return String
     * @throws IOException IOException
     */
    public static RestResponse get(String urlLocation, String queryString, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders, int timeout) throws IOException {
        return get(urlLocation, queryString, requestHeaders, responseHeaders, null, Char_Encoding_Type_Enum.UTF, timeout);
    }

    /**
     * Sends a GET request to the given url and returns the response as a String.
     * @param urlLocation String
     * @param queryString String
     * @param requestHeaders Map
     * @param responseHeaders Map
     * @param dontLog List
     * @param encodingType Char_Encoding_Type_Enum
     * @param timeout int
     * @return String
     * @throws IOException IOException
     */
    public static RestResponse get(String urlLocation, String queryString, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders, List<String> dontLog,
            Char_Encoding_Type_Enum encodingType, int timeout) throws IOException {
        boolean isDebug = log.isDebugEnabled();
        RestResponse response = new RestResponse();
        URLConnection connection = null;
        BufferedReader reader = null;

        if (queryString != null && !queryString.isEmpty()) {
            urlLocation = urlLocation + "?" + queryString;
        }

        // log.info("making get call to : \n" + urlLocation);

        StringBuilder sb = BaseStats.tmpStringBuilder();
        try {
            URL server = new URL(urlLocation);
            connection = server.openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            LogData logData = LogThreadLocal.getLogData();
            String requestId = logData == null ? null : logData.getRequestId();
            if (StringUtils.isNotBlank(requestId)) {
                if (requestHeaders == null) {
                    requestHeaders = new HashMap<String, String>();
                }
                requestHeaders.put(LogThreadLocal.getLogData().REQUEST_ID, requestId);
            }
            if (requestHeaders != null) {
                for (String key : requestHeaders.keySet()) {
                    connection.setRequestProperty(key, requestHeaders.get(key));
                }
            }
            addCallerInfoHeader(server, connection);
            connection.setRequestProperty(ACCEPT_ENCODING, GZIP);

            if (responseHeaders != null) {
                responseHeaders.putAll(connection.getHeaderFields());
                responseHeaders.put(HTTP_COOKIE_HEADER_NAME, extractCookieHeaders(connection));
            }

            InputStream responseStream = connection.getInputStream();
            if (GZIP.equalsIgnoreCase(connection.getHeaderField(CONTENT_ENCODING))) {
                responseStream = new GZIPInputStream(responseStream);
                if (isDebug) {
                    log.debug("Got Gzipped Response for url: " + urlLocation);
                }
            }
            if (encodingType == Char_Encoding_Type_Enum.UTF) {
                reader = new BufferedReader(new InputStreamReader(responseStream, "UTF-8"));
            } else {
                reader = new BufferedReader(new InputStreamReader(responseStream));
            }

            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
            if (connection instanceof HttpURLConnection) {
                response.setCode(String.valueOf(((HttpURLConnection) connection).getResponseCode()));
            } else {
                response.setCode("200");
            }
            response.setMessage(sb.toString());
        } catch (MalformedURLException malEx) {
            log.error(malEx);
        } catch (FileNotFoundException fnfEx) {
            if (dontLog == null || dontLog.indexOf("404") == -1) {
                log.error("FileNotFoundException exception for URL-" + urlLocation, fnfEx);
            }
            if (connection instanceof HttpURLConnection) {
                getErrorMessage((HttpURLConnection) connection, sb);
                response.setCode(String.valueOf(((HttpURLConnection) connection).getResponseCode()));
            }
            response.setMessage(sb.toString());
        } catch (IOException ioEx) {
            if (connection instanceof HttpURLConnection) {
                getErrorMessage((HttpURLConnection) connection, sb);
                response.setCode(String.valueOf(((HttpURLConnection) connection).getResponseCode()));
            }
            if (dontLog == null || !dontLog.contains(Integer.toString(response.getCode()))) {
                log.error("I/O exception for URL-" + urlLocation, ioEx);
            } else {
                log.error("I/O exception for URL-" + urlLocation + sb.toString());
            }
            response.setMessage(sb.toString());
        } finally {
            sb = BaseStats.returnTmpStringBuilder(sb);
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    log.info(e);
                }
            }
        }

        return response;
    }

    /**
     * Function to extract the Http cookie header from HttpUrlConnection. The exact header name is 'Set-Cookie'.
     * @param connection
     * @return
     */
    private static List<String> extractCookieHeaders(URLConnection connection) {

        // Extract all incoming cookies.
        // We need to explicitly extract the value of HTTP Set-Cookie header
        // as it is not returned when UrlConnection.getHeaderFields() is invoked
        // as of Java 1.6.0_20.
        String setCookieHeaderValue = connection.getHeaderField("Set-Cookie");
        List<String> cookiesHeaderValue = new ArrayList<String>();
        cookiesHeaderValue.add(setCookieHeaderValue);
        log.info("extracted cookies from request = " + cookiesHeaderValue);
        return cookiesHeaderValue;

    }

    public static RestResponse delete(String urlLocation, String queryString) throws IOException {
        return delete(urlLocation, queryString, null, null);
    }

    public static RestResponse delete(String urlLocation, String queryString, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders) throws IOException {
        boolean isDebug = log.isDebugEnabled();
        RestResponse response = new RestResponse();
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        if (queryString != null && !queryString.isEmpty()) {
            urlLocation = urlLocation + "?" + queryString;
        }

        StringBuilder sb = BaseStats.tmpStringBuilder();
        try {
            URL server = new URL(urlLocation);
            connection = (HttpURLConnection) server.openConnection();
            connection.setConnectTimeout(120000);
            connection.setReadTimeout(120000);
            connection.setRequestMethod("DELETE");

            LogData logData = LogThreadLocal.getLogData();
            String requestId = logData == null ? null : logData.getRequestId();
            if (StringUtils.isNotBlank(requestId)) {
                if (requestHeaders == null) {
                    requestHeaders = new HashMap<String, String>();
                }
                requestHeaders.put(LogThreadLocal.getLogData().REQUEST_ID, requestId);
            }
            if (requestHeaders != null) {
                for (String key : requestHeaders.keySet()) {
                    connection.setRequestProperty(key, requestHeaders.get(key));
                }
            }
            addCallerInfoHeader(server, connection);
            connection.setRequestProperty(ACCEPT_ENCODING, GZIP);

            if (responseHeaders != null) {
                responseHeaders.putAll(connection.getHeaderFields());
            }

            InputStream responseStream = connection.getInputStream();
            if (GZIP.equalsIgnoreCase(connection.getHeaderField(CONTENT_ENCODING))) {
                responseStream = new GZIPInputStream(responseStream);
                if (isDebug) {
                    log.debug("Got Gzipped Response for url: " + urlLocation);
                }
            }
            reader = new BufferedReader(new InputStreamReader(responseStream, "UTF-8"));

            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }

            response.setCode(String.valueOf(connection.getResponseCode()));
            response.setMessage(sb.toString());
        } catch (MalformedURLException malEx) {
            log.error(malEx);
        } catch (IOException ioEx) {
            getErrorMessage(connection, sb);
            if (connection != null) {
                response.setCode(String.valueOf(connection.getResponseCode()));
            }
            response.setMessage(sb.toString());
        } finally {
            sb = BaseStats.returnTmpStringBuilder(sb);
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    log.info(e);
                }
            }
        }

        return response;
    }

    /**
     * Send a POST request to the URL. equivalent to post(urlLocation, postMessage, null, "text/xml", null, null, "POST").
     * @param urlLocation
     *            URL location
     * @param postMessage
     *            Message to be posted
     * @return Response
     * @throws IOException IOException
     */
    public static RestResponse post(String urlLocation, String postMessage) throws IOException {
        return post(urlLocation, postMessage, null, "text/xml", null, null, "POST", null);
    }

    /**
     * Send a POST request to the URL. equivalent to post(urlLocation, postMessage, null, "text/xml", requestHeaders, responseHeaders, "POST").
     * @param urlLocation
     *            URL location
     * @param postMessage
     *            Message to be posted
     * @param requestHeaders
     *            Request headers
     * @param responseHeaders
     *            Output parameter. Will be populated with response headers if not null.
     * @return Response
     * @throws IOException IOException
     */
    public static RestResponse post(String urlLocation, String postMessage, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders) throws IOException {
        return post(urlLocation, postMessage, null, "text/xml", requestHeaders, responseHeaders, "POST", null);
    }

    /**
     * 
     * @param urlLocation
     * @param postMessage
     * @param requestHeaders
     * @param responseHeaders
     * @param timeout
     * @return
     * @throws IOException
     */
    public static RestResponse post(String urlLocation, String postMessage, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders, Integer timeout) throws IOException {
        return post(urlLocation, postMessage, null, "text/xml", requestHeaders, responseHeaders, "POST", timeout);
    }

    /**
     * Send a POST request to the URL. equivalent to post(urlLocation, postMessage, paramPrefix, requestContentType, null, null, "POST").
     * @param urlLocation String
     *            URL location
     * @param postMessage String
     *            Message to be posted
     * @param requestContentType String
     *            Output parameter. Will be populated with response headers if not null.
     * @param paramPrefix String
     * @return Response
     * @throws IOException IOException
     */
    public static RestResponse post(String urlLocation, String postMessage, String paramPrefix, String requestContentType) throws IOException {
        return post(urlLocation, postMessage, paramPrefix, requestContentType, null, null, "POST", null);
    }

    /**
     * Send a PUT request to the URL. equivalent to post(urlLocation, postMessage, null, "text/xml", null, null, "PUT").
     * @param urlLocation
     *            URL location
     * @param postMessage
     *            Message to be posted
     * @return Response
     * @throws IOException IOException
     */
    public static RestResponse put(String urlLocation, String postMessage) throws IOException {
        return post(urlLocation, postMessage, null, "text/xml", null, null, "PUT", null);
    }

    /**
     * Send a PUT request to the URL. equivalent to post(urlLocation, postMessage, null, "text/xml", null, null, "PUT").
     * @param urlLocation
     *            URL location
     * @param postMessage
     *            Message to be posted
     * @param contentType String
     * @return Response
     * @throws IOException IOException
     */
    public static RestResponse put(String urlLocation, String postMessage, String contentType) throws IOException {
        return post(urlLocation, postMessage, null, contentType, null, null, "PUT", null);
    }

    /**
     * Send a POST request to the URL. equivalent to post(urlLocation, postMessage, null, "text/xml", requestHeaders, responseHeaders, "PUT").
     * @param urlLocation
     *            URL location
     * @param postMessage
     *            Message to be posted
     * @param requestHeaders Map
     * @param responseHeaders Map
     *            Output parameter. Will be populated with response headers if not null.
     * @return String
     * @throws IOException IOException
     */
    public static RestResponse put(String urlLocation, String postMessage, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders) throws IOException {
        return post(urlLocation, postMessage, null, "text/xml", requestHeaders, responseHeaders, "PUT", null);
    }

    /**
     * Put method that takes a timeout
     * @param urlLocation
     * @param postMessage
     * @param requestHeaders
     * @param responseHeaders
     * @param timeout
     * @return
     * @throws IOException
     */
    public static RestResponse put(String urlLocation, String postMessage, Map<String, String> requestHeaders, Map<String, List<String>> responseHeaders, Integer timeout) throws IOException {
        return post(urlLocation, postMessage, null, "text/xml", requestHeaders, responseHeaders, "PUT", timeout);
    }

    /**
     * Send a POST request to the URL.
     * @param urlLocation
     *            URL location
     * @param postMessage
     *            Message to be posted
     * @param paramPrefix
     *            This is the parameter prefix, type=hotel&amp;req= for example. If this is null then postMessage is posted as is. Otherwise paramPrefix is prefixed to the URL encoded postMessage and
     *            sent as from a Web Browser.
     * @param requestContentType
     *            Request Content Type to use. If null then it is not set and the default application/x-www-form-urlencoded.
     * @param requestHeaders
     *            Request headers
     * @param responseHeaders
     *            Output parameter. Will be populated with response headers if not null.
     * @param requestMethod
     *            Request method type, POST/PUT
     * @param timeout
     *            Connection timeout
     * @return Response from server
     * @throws IOException
     *             on IO error
     */
    public static RestResponse post(String urlLocation, String postMessage, String paramPrefix, String requestContentType, Map<String, String> requestHeaders,
            Map<String, List<String>> responseHeaders, String requestMethod, Integer timeout) throws IOException {
        return post(urlLocation, postMessage, paramPrefix, requestContentType, requestHeaders, responseHeaders, requestMethod, timeout, false);
    }

    public static RestResponse post(String urlLocation, String postMessage, String paramPrefix, String requestContentType, Map<String, String> requestHeaders,
            Map<String, List<String>> responseHeaders, String requestMethod, Integer timeout, boolean ignoreHostVerification) throws IOException {
        return post(urlLocation, postMessage, paramPrefix, requestContentType, requestHeaders, responseHeaders, requestMethod, timeout, null, null, null, ignoreHostVerification);
    }

    /**
     * Send a POST request to the URL.
     * @param urlLocation
     *            URL location
     * @param postMessage
     *            Message to be posted
     * @param paramPrefix
     *            This is the parameter prefix, type=hotel&amp;req= for example. If this is null then postMessage is posted as is. Otherwise paramPrefix is prefixed to the URL encoded postMessage and
     *            sent as from a Web Browser.
     * @param requestContentType
     *            Request Content Type to use. If null then it is not set and the default application/x-www-form-urlencoded.
     * @param requestHeaders
     *            Request headers
     * @param responseHeaders
     *            Output parameter. Will be populated with response headers if not null.
     * @param requestMethod
     *            Request method type, POST/PUT
     * @param timeout
     *            Connection timeout
     * @param dontLog
     *            List of exceptions that we don't want in the logs
     * @return Response from server
     * @throws IOException
     *             on IO error
     */
    public static RestResponse post(String urlLocation, String postMessage, String paramPrefix, String requestContentType, Map<String, String> requestHeaders,
            Map<String, List<String>> responseHeaders, String requestMethod, Integer timeout, List<String> dontLog) throws IOException {
        return post(urlLocation, postMessage, paramPrefix, requestContentType, requestHeaders, responseHeaders, requestMethod, timeout, null, null, dontLog, false);
    }

    public static RestResponse post(String urlLocation, String postMessage, String paramPrefix, String requestContentType, Map<String, String> requestHeaders,
            Map<String, List<String>> responseHeaders, String requestMethod, Integer timeout, String userName, String password, List<String> dontLog) throws IOException {
        return post(urlLocation, postMessage, paramPrefix, requestContentType, requestHeaders, responseHeaders, requestMethod, timeout, userName, password, dontLog, false);
    }

    public static RestResponse post(String urlLocation, String postMessage, String paramPrefix, String requestContentType, Map<String, String> requestHeaders,
            Map<String, List<String>> responseHeaders, String requestMethod, Integer timeout, String userName, String password, List<String> dontLog, boolean ignoreHostVerification)
            throws IOException {
        return post(urlLocation, postMessage, paramPrefix, requestContentType, requestHeaders, responseHeaders, requestMethod, timeout, userName, password, dontLog, false, null);
    }

    public static RestResponse post(String urlLocation, String postMessage, String paramPrefix, String requestContentType, Map<String, String> requestHeaders,
            Map<String, List<String>> responseHeaders, String requestMethod, Integer timeout, String userName, String password, List<String> dontLog, boolean ignoreHostVerification,
            List<Cookie> cookies) throws IOException {
        boolean isDebug = log.isDebugEnabled();
        RestResponse response = new RestResponse();

        if (urlLocation != null && postMessage != null) {
            if (requestMethod == null) {
                requestMethod = "POST";
            }

            if (timeout == null) {
                timeout = 120000;
            }

            BufferedReader reader = null;
            OutputStreamWriter writer = null;
            URLConnection connection = null;
            StringBuilder sb = BaseStats.tmpStringBuilder();

            try {
                URL url = new URL(urlLocation);
                connection = (URLConnection) url.openConnection();
                connection.setConnectTimeout(timeout);
                connection.setReadTimeout(timeout);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Length", "" + postMessage.length());
                connection.setRequestProperty(ACCEPT_ENCODING, GZIP);

                StringBuilder cookieStr = null;
                if (cookies != null && cookies.size() > 0) {
                    cookieStr = new StringBuilder();
                    for (int i = 0; i < cookies.size(); i++) {
                        Cookie cookie = cookies.get(i);
                        cookieStr.append(cookie.getName()).append("=").append(cookie.getValue()).append("; ");
                    }
                    cookieStr.setLength(cookieStr.length() - 2); // this is to remove the last 2 characters - "; "
                    connection.setRequestProperty("Cookie", cookieStr.toString());
                }

                if (userName != null && password != null) {
                    Base64 encoder = new Base64();
                    String userPassword = userName + ":" + password;
                    String encodedUserPassword = new String(encoder.encode(userPassword.getBytes()));

                    connection.setRequestProperty("Authorization", "Basic " + encodedUserPassword);
                }

                if (requestContentType != null) {
                    connection.setRequestProperty("Content-Type", requestContentType);
                }

                LogData logData = LogThreadLocal.getLogData();
                String requestId = logData == null ? null : logData.getRequestId();
                if (StringUtils.isNotBlank(requestId)) {
                    if (requestHeaders == null) {
                        requestHeaders = new HashMap<String, String>();
                    }
                    requestHeaders.put(LogThreadLocal.getLogData().REQUEST_ID, requestId);
                }
                if (requestHeaders != null) {
                    for (String key : requestHeaders.keySet()) {
                        connection.setRequestProperty(key, requestHeaders.get(key));
                    }
                }

                addCallerInfoHeader(url, connection);

                if (connection instanceof HttpURLConnection) {
                    HttpURLConnection httpCon = (HttpURLConnection) connection;
                    httpCon.setRequestMethod(requestMethod);

                    if (ignoreHostVerification && httpCon instanceof HttpsURLConnection) {
                        HttpsURLConnection httpsCon = (HttpsURLConnection) httpCon;
                        httpsCon.setHostnameVerifier(ignoreHostnameVerifier);
                    }

                    if (paramPrefix != null) {
                        postMessage = paramPrefix + URLEncoder.encode(postMessage, "UTF-8");
                    }

                    writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(postMessage);
                    writer.flush();
                }

                if (responseHeaders != null) {
                    responseHeaders.putAll(connection.getHeaderFields());
                }

                String line = null;

                try {
                    InputStream responseStream = connection.getInputStream();
                    if (GZIP.equalsIgnoreCase(connection.getHeaderField(CONTENT_ENCODING))) {
                        responseStream = new GZIPInputStream(responseStream);
                        if (isDebug) {
                            log.debug("Got Gzipped Response for url: " + urlLocation);
                        }
                    }
                    reader = new BufferedReader(new InputStreamReader(responseStream));
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append('\n');
                    }
                } catch (IOException e) {
                    if (connection instanceof HttpURLConnection) {
                        HttpURLConnection httpCon = (HttpURLConnection) connection;
                        getErrorMessage(httpCon, sb);

                        if (connection != null && (httpCon.getResponseCode() == 400 || httpCon.getResponseCode() == 401)) {
                            log.warn("Got Error Response: " + sb.toString(), e);
                        } else if (connection != null && httpCon.getResponseCode() == 500) {
                            log.error("Got Error Response: " + sb.toString(), e);
                            if (urlLocation.contains("/book") || urlLocation.contains("/register")) {
                                log.error("Got error for: url->" + url + ", parameters->" + paramPrefix + ", postmessage is: \n" + postMessage);
                            }
                        }
                    }
                }

                // Removes all special characters.
                response.setMessage(new String(sb.toString().getBytes()));

                if (connection instanceof HttpURLConnection) {
                    response.setCode(String.valueOf(((HttpURLConnection) connection).getResponseCode()));
                } else {
                    response.setCode("200");
                }
            } finally {
                BaseStats.returnTmpStringBuilder(sb);
                try {
                    if (writer != null) {
                        writer.close();
                    }
                    if (reader != null) {
                        reader.close();
                    }
                } catch (Exception e) {
                    log.info(e);
                }
            }
        }

        return response;
    }

    private static void getErrorMessage(HttpURLConnection connection, StringBuilder sb) throws IOException {
        sb.setLength(0);
        BufferedReader reader = null;
        String line = null;

        InputStream errorStream = connection.getErrorStream();
        if (errorStream != null) {
            if (GZIP.equalsIgnoreCase(connection.getHeaderField(CONTENT_ENCODING))) {
                errorStream = new GZIPInputStream(errorStream);
            }
            reader = new BufferedReader(new InputStreamReader(errorStream));
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        }
    }

    /**
     * WARNING: For debug use only (AFTER checking log.isDebugEnabled). Creating a JAXBContext is a costly process and should be done in constructors for actual use. Marshals a jaxbElement into xml
     * String.
     * @param packages Object
     * @param jaxbElement Object
     * @return String
     */
    public static String marshalJaxb(Object jaxbElement, String packages) {
        String xml = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(packages);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty("jaxb.formatted.output", true);
            StringBuilderWriter writer = new StringBuilderWriter();
            marshaller.marshal(jaxbElement, writer);
            xml = writer.toString();
        } catch (Exception e) {
            log.info(e);
        }
        return xml;
    }

    /**
     * WARNING: For debug use only (AFTER checking log.isDebugEnabled). Creating a JAXBContext is a costly process and should be done in constructors for actual use. Marshals a jaxbElement into xml
     * String.
     * @param marshaller Marshaller
     * @param jaxbElement Object
     * @return String
     */
    public static String marshalJaxb(Object jaxbElement, Marshaller marshaller) {
        String xml = null;
        try {
            marshaller.setProperty("jaxb.formatted.output", true);
            StringWriter writer = new StringWriter();
            marshaller.marshal(jaxbElement, writer);
            xml = writer.toString();
        } catch (Exception e) {
            log.error("Exception in Marhall: ", e);
        }
        return xml;
    }

    /**
     * WARNING: For debug use only (AFTER checking log.isDebugEnabled). Creating a JAXBContext is a costly process and should be done in constructors for actual use. UnMarshals an xml String into
     * jaxbElement.
     * @param xml String
     * @param packages String
     * @return Object
     */
    public static Object unMarshal(String xml, String packages) {
        Object obj = null;
        StringReader stringReader = new StringReader(xml);
        try {
            JAXBContext context = JAXBContext.newInstance(packages);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            obj = ((JAXBElement) unmarshaller.unmarshal(stringReader)).getValue();
        } catch (Exception e) {
            obj = null;
            log.error("Exception while unmarshalling", e);
        }
        return obj;
    }

    public static Object unMarshal(Unmarshaller unmarshaller, String xml) {
        Object obj = null;
        StringReader stringReader = new StringReader(xml);
        try {
            obj = ((JAXBElement) unmarshaller.unmarshal(stringReader)).getValue();
        } catch (Exception e) {
            obj = null;
            log.error("Exception while unmarshalling xml: " + xml, e);
        }
        return obj;
    }

    public static DefaultHandler parse(String xmlString, DefaultHandler handler) throws ParserConfigurationException, SAXException, IOException {
        return parse(xmlString, handler, false);
    }

    public static DefaultHandler parse(String xmlString, DefaultHandler handler, boolean isNameSpaceAware) throws ParserConfigurationException, SAXException, IOException {
        if (xmlString != null && handler != null) {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            saxParserFactory.setNamespaceAware(isNameSpaceAware);

            SAXParser saxParser = saxParserFactory.newSAXParser();
            InputSource is = new InputSource(new StringReader(xmlString));
            saxParser.parse(is, handler);
        }
        return handler;
    }

    public static String getBaseUrlFromRequest(HttpServletRequest request) {
        String baseURL = "";
        URL urlObj = null;
        try {
            urlObj = new URL(request.getRequestURL().toString());
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
        int port = urlObj.getPort();

        // If port is -1, then there is no port specified.
        if (port != -1) {
            baseURL = "http" + "://" + urlObj.getHost() + ":" + port;
        } else {
            baseURL = "http" + "://" + urlObj.getHost();
        }
        return baseURL;
    }

    /**
     * A method to get the hostname out of the url.
     * @param request
     *            the HttpServletRequest object
     * @param addPort boolean
     * @return String.
     */
    public static String getIncomingUrlHost(HttpServletRequest request, boolean addPort) {
        String url = request.getRequestURL().toString();
        URL urlObject;
        try {
            urlObject = new URL(url);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
        String hostname = urlObject.getHost();

        if (addPort) {
            int port = urlObject.getPort();
            if (port > 0) {
                hostname = hostname + ':' + port;
            }
        }

        return hostname;
    }

    /**
     * This method gets the action from the request url.
     * @param request HttpServletRequest
     * @return String
     */
    public static String getActionFromIncomingUrl(HttpServletRequest request) {

        String action = null;
        String url = request.getRequestURL().toString();
        try {
            String[] splitVals = url.split("/");
            String actionUri = splitVals[splitVals.length - 1].toString();
            if (actionUri.indexOf('?') != -1) {
                splitVals = actionUri.split("\\?");
                action = splitVals[0];
            } else {
                action = actionUri;
            }
        } catch (Exception e) {
            log.error(e);
        }

        return action;
    }

    /**
     * Utility method to add query parameters to the query string after properly encoding it.
     * @param queryString
     *            the queryString to which the parameter needs to be added
     * @param paramName
     *            the name of the query parameter
     * @param paramValue
     *            the value of the query parameter
     */
    public static void addQueryParameter(StringBuilder queryString, String paramName, String paramValue) {
        String queryParam = "";
        try {
            if ((paramValue != null) && (!paramValue.isEmpty())) {
                queryParam = paramName + "=" + URLEncoder.encode(paramValue, "UTF-8");
            }
        } catch (UnsupportedEncodingException ex) {
            log.info(ex);
        }

        if (!queryParam.isEmpty()) {
            if (queryString.length() > 0) {
                queryString.append("&").append(queryParam);
            } else {
                queryString.append(queryParam);
            }
        }
    }

    /**
     * Gets the document object for the given string xml.
     * @param xml
     *            String
     * @return Document object for the given xml.
     * @throws ParserConfigurationException ParserConfigurationException
     * @throws SAXException SAXException
     * @throws IOException IOException
     */
    public static Document getDocument(String xml) throws ParserConfigurationException, SAXException, IOException {
        StringReader reader = new StringReader(xml);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(new InputSource(reader));
        return document;
    }

    public static Optional<String> getQueryParamValue(final String url1, String queryParamName) {
        String url = url1;
        url = url.substring(url.indexOf("?")+1);
        if(StringUtils.isBlank(url)) {
            return Optional.empty();
        }
        int index = url.indexOf(queryParamName + "=");
        if(index == -1) {
            return Optional.empty();
        }
        url = url.substring(url.indexOf(queryParamName)+queryParamName.length() + 1);
        int endIndex = url.indexOf("&");
        if(endIndex == -1) {
            endIndex = url.length();
        }
        String key = url.substring(0,endIndex);
        return Optional.of(key);
    }
}
