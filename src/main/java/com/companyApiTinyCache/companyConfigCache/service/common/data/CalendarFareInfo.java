package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author ilaya
 *
 */
public class CalendarFareInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String price;

    private String airline;
    private String airlineName;
    private String departureDate; // NOT USED as departure date is a part of the key. But we can add it as a part of the json and pass it on.
    private String departureTime;
    private String arrivalDate;
    private String arrivalTime;

    private List<String> allFlightDetails;
    private List<String> via;

    private String onwardAirline;
    private String onwardAirlineName;
    private String onwardDepartureDate;
    private String onwardDepartureTime;
    private String onwardArrivalDate;
    private String onwardArrivalTime;

    private List<String> onwardAllFlightDetails;
    private List<String> onwardVia;

    private String returnAirline;
    private String returnAirlineName;
    private String returnDepartureDate;
    private String returnDepartureTime;
    private String returnArrivalDate;
    private String returnArrivalTime;

    private List<String> returnAllFlightDetails;
    private List<String> returnVia;

	private long cachedTime;

	//is cheapest for the group of dates. true if this fare is cheapest among all the fares from different dates.
    @JsonProperty("cp")
	private Boolean cheapest = null;

    /**
     * @return the price
     */
    @JsonProperty("pr")
    public String getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    @JsonProperty("pr")
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the airline
     */
    @JsonProperty("al")
    public String getAirline() {
        return airline;
    }

    /**
     * @param airline
     *            the airline to set
     */
    @JsonProperty("al")
    public void setAirline(String airline) {
        this.airline = airline;
    }

    /**
     * @return the airlineName
     */
    @JsonProperty("aln")
    public String getAirlineName() {
        return airlineName;
    }

    /**
     * @param airlineName
     *            the airlineName to set
     */
    @JsonProperty("aln")
    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    /**
     * @return the departureDate
     */
    @JsonIgnore
    public String getDepartureDate() {
        return departureDate;
    }

    /**
     * @param departureDate
     *            the departureDate to set
     */
    @JsonIgnore
    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    /**
     * @return the departureTime
     */
    @JsonProperty("dt")
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * @param departureTime
     *            the departureTime to set
     */
    @JsonProperty("dt")
    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    /**
     * @return the arrivalDate
     */
    @JsonProperty("ad")
    public String getArrivalDate() {
        return arrivalDate;
    }

    /**
     * @param arrivalDate
     *            the arrivalDate to set
     */
    @JsonProperty("ad")
    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    /**
     * @return the arrivalTime
     */
    @JsonProperty("at")
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * @param arrivalTime
     *            the arrivalTime to set
     */
    @JsonProperty("at")
    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * @return the allFlightDetails
     */
    @JsonProperty("afd")
    public List<String> getAllFlightDetails() {
        return allFlightDetails;
    }

    /**
     * @param allFlightDetails
     *            the allFlightDetails to set
     */
    @JsonProperty("afd")
    public void setAllFlightDetails(List<String> allFlightDetails) {
        this.allFlightDetails = allFlightDetails;
    }

    /**
     * @return the via
     */
    @JsonProperty("via")
    public List<String> getVia() {
        return via;
    }

    /**
     * @param via
     *            the via to set
     */
    @JsonProperty("via")
    public void setVia(List<String> via) {
        this.via = via;
    }

    /**
     * @return the onwardAirline
     */
    @JsonProperty("oal")
    public String getOnwardAirline() {
        return onwardAirline;
    }

    /**
     * @param onwardAirline
     *            the onwardAirline to set
     */
    @JsonProperty("oal")
    public void setOnwardAirline(String onwardAirline) {
        this.onwardAirline = onwardAirline;
    }

    /**
     * @return the onwardAirlineName
     */
    @JsonProperty("oaln")
    public String getOnwardAirlineName() {
        return onwardAirlineName;
    }

    /**
     * @param onwardAirlineName
     *            the onwardAirlineName to set
     */
    @JsonProperty("oaln")
    public void setOnwardAirlineName(String onwardAirlineName) {
        this.onwardAirlineName = onwardAirlineName;
    }

    /**
     * @return the onwardDepartureDate
     */
    @JsonProperty("odd")
    public String getOnwardDepartureDate() {
        return onwardDepartureDate;
    }

    /**
     * @param onwardDepartureDate
     *            the onwardDepartureDate to set
     */
    @JsonProperty("odd")
    public void setOnwardDepartureDate(String onwardDepartureDate) {
        this.onwardDepartureDate = onwardDepartureDate;
    }

    /**
     * @return the onwardDepartureTime
     */
    @JsonProperty("odt")
    public String getOnwardDepartureTime() {
        return onwardDepartureTime;
    }

    /**
     * @param onwardDepartureTime
     *            the onwardDepartureTime to set
     */
    @JsonProperty("odt")
    public void setOnwardDepartureTime(String onwardDepartureTime) {
        this.onwardDepartureTime = onwardDepartureTime;
    }

    /**
     * @return the onwardArrivalDate
     */
    @JsonProperty("oad")
    public String getOnwardArrivalDate() {
        return onwardArrivalDate;
    }

    /**
     * @param onwardArrivalDate
     *            the onwardArrivalDate to set
     */
    @JsonProperty("oad")
    public void setOnwardArrivalDate(String onwardArrivalDate) {
        this.onwardArrivalDate = onwardArrivalDate;
    }

    /**
     * @return the onwardArrivalTime
     */
    @JsonProperty("oat")
    public String getOnwardArrivalTime() {
        return onwardArrivalTime;
    }

    /**
     * @param onwardArrivalTime
     *            the onwardArrivalTime to set
     */
    @JsonProperty("oat")
    public void setOnwardArrivalTime(String onwardArrivalTime) {
        this.onwardArrivalTime = onwardArrivalTime;
    }

    /**
     * @return the onwardAllFlightDetails
     */
    @JsonProperty("oafd")
    public List<String> getOnwardAllFlightDetails() {
        return onwardAllFlightDetails;
    }

    /**
     * @param onwardAllFlightDetails
     *            the onwardAllFlightDetails to set
     */
    @JsonProperty("oafd")
    public void setOnwardAllFlightDetails(List<String> onwardAllFlightDetails) {
        this.onwardAllFlightDetails = onwardAllFlightDetails;
    }

    /**
     * @return the onwardVia
     */
    @JsonProperty("ovia")
    public List<String> getOnwardVia() {
        return onwardVia;
    }

    /**
     * @param onwardVia
     *            the onwardVia to set
     */
    @JsonProperty("ovia")
    public void setOnwardVia(List<String> onwardVia) {
        this.onwardVia = onwardVia;
    }

    /**
     * @return the returnAirline
     */
    @JsonProperty("ral")
    public String getReturnAirline() {
        return returnAirline;
    }

    /**
     * @param returnAirline
     *            the returnAirline to set
     */
    @JsonProperty("ral")
    public void setReturnAirline(String returnAirline) {
        this.returnAirline = returnAirline;
    }

    /**
     * @return the returnAirlineName
     */
    @JsonProperty("raln")
    public String getReturnAirlineName() {
        return returnAirlineName;
    }

    /**
     * @param returnAirlineName
     *            the returnAirlineName to set
     */
    @JsonProperty("raln")
    public void setReturnAirlineName(String returnAirlineName) {
        this.returnAirlineName = returnAirlineName;
    }

    /**
     * @return the returnDepartureDate
     */
    @JsonProperty("rdd")
    public String getReturnDepartureDate() {
        return returnDepartureDate;
    }

    /**
     * @param returnDepartureDate
     *            the returnDepartureDate to set
     */
    @JsonProperty("rdd")
    public void setReturnDepartureDate(String returnDepartureDate) {
        this.returnDepartureDate = returnDepartureDate;
    }

    /**
     * @return the returnDepartureTime
     */
    @JsonProperty("rdt")
    public String getReturnDepartureTime() {
        return returnDepartureTime;
    }

    /**
     * @param returnDepartureTime
     *            the returnDepartureTime to set
     */
    @JsonProperty("rdt")
    public void setReturnDepartureTime(String returnDepartureTime) {
        this.returnDepartureTime = returnDepartureTime;
    }

    /**
     * @return the returnArrivalDate
     */
    @JsonProperty("rad")
    public String getReturnArrivalDate() {
        return returnArrivalDate;
    }

    /**
     * @param returnArrivalDate
     *            the returnArrivalDate to set
     */
    @JsonProperty("rad")
    public void setReturnArrivalDate(String returnArrivalDate) {
        this.returnArrivalDate = returnArrivalDate;
    }

    /**
     * @return the returnArrivalTime
     */
    @JsonProperty("rat")
    public String getReturnArrivalTime() {
        return returnArrivalTime;
    }

    /**
     * @param returnArrivalTime
     *            the returnArrivalTime to set
     */
    @JsonProperty("rat")
    public void setReturnArrivalTime(String returnArrivalTime) {
        this.returnArrivalTime = returnArrivalTime;
    }

    /**
     * @return the returnAllFlightDetails
     */
    @JsonProperty("rafd")
    public List<String> getReturnAllFlightDetails() {
        return returnAllFlightDetails;
    }

    /**
     * @param returnAllFlightDetails
     *            the returnAllFlightDetails to set
     */
    @JsonProperty("rafd")
    public void setReturnAllFlightDetails(List<String> returnAllFlightDetails) {
        this.returnAllFlightDetails = returnAllFlightDetails;
    }

    /**
     * @return the returnVia
     */
    @JsonProperty("rvia")
    public List<String> getReturnVia() {
        return returnVia;
    }

    /**
     * @param returnVia
     *            the returnVia to set
     */
    @JsonProperty("rvia")
    public void setReturnVia(List<String> returnVia) {
        this.returnVia = returnVia;
    }

    /**
     * @param cachedTime
     *            the cached time to set
     */
    @JsonProperty("ct")
	public void setCachedTime(long cachedTime) {
		this.cachedTime = cachedTime;
	}

    /**
     * @return the cached time
     */
    @JsonProperty("ct")
	public long getCachedTime() {
		return this.cachedTime;
	}
    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((airline == null) ? 0 : airline.hashCode());
        result = prime * result + ((airlineName == null) ? 0 : airlineName.hashCode());
        result = prime * result + ((allFlightDetails == null) ? 0 : allFlightDetails.hashCode());
        result = prime * result + ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
        result = prime * result + ((arrivalTime == null) ? 0 : arrivalTime.hashCode());
        result = prime * result + ((departureDate == null) ? 0 : departureDate.hashCode());
        result = prime * result + ((departureTime == null) ? 0 : departureTime.hashCode());
        result = prime * result + ((onwardAirline == null) ? 0 : onwardAirline.hashCode());
        result = prime * result + ((onwardAirlineName == null) ? 0 : onwardAirlineName.hashCode());
        result = prime * result + ((onwardAllFlightDetails == null) ? 0 : onwardAllFlightDetails.hashCode());
        result = prime * result + ((onwardArrivalDate == null) ? 0 : onwardArrivalDate.hashCode());
        result = prime * result + ((onwardArrivalTime == null) ? 0 : onwardArrivalTime.hashCode());
        result = prime * result + ((onwardDepartureDate == null) ? 0 : onwardDepartureDate.hashCode());
        result = prime * result + ((onwardDepartureTime == null) ? 0 : onwardDepartureTime.hashCode());
        result = prime * result + ((onwardVia == null) ? 0 : onwardVia.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((returnAirline == null) ? 0 : returnAirline.hashCode());
        result = prime * result + ((returnAirlineName == null) ? 0 : returnAirlineName.hashCode());
        result = prime * result + ((returnAllFlightDetails == null) ? 0 : returnAllFlightDetails.hashCode());
        result = prime * result + ((returnArrivalDate == null) ? 0 : returnArrivalDate.hashCode());
        result = prime * result + ((returnArrivalTime == null) ? 0 : returnArrivalTime.hashCode());
        result = prime * result + ((returnDepartureDate == null) ? 0 : returnDepartureDate.hashCode());
        result = prime * result + ((returnDepartureTime == null) ? 0 : returnDepartureTime.hashCode());
        result = prime * result + ((returnVia == null) ? 0 : returnVia.hashCode());
        result = prime * result + ((via == null) ? 0 : via.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CalendarFareInfo other = (CalendarFareInfo) obj;
        if (airline == null) {
            if (other.airline != null) {
                return false;
            }
        } else if (!airline.equals(other.airline)) {
            return false;
        }
        if (airlineName == null) {
            if (other.airlineName != null) {
                return false;
            }
        } else if (!airlineName.equals(other.airlineName)) {
            return false;
        }
        if (allFlightDetails == null) {
            if (other.allFlightDetails != null) {
                return false;
            }
        } else if (!allFlightDetails.equals(other.allFlightDetails)) {
            return false;
        }
        if (arrivalDate == null) {
            if (other.arrivalDate != null) {
                return false;
            }
        } else if (!arrivalDate.equals(other.arrivalDate)) {
            return false;
        }
        if (arrivalTime == null) {
            if (other.arrivalTime != null) {
                return false;
            }
        } else if (!arrivalTime.equals(other.arrivalTime)) {
            return false;
        }
        if (departureDate == null) {
            if (other.departureDate != null) {
                return false;
            }
        } else if (!departureDate.equals(other.departureDate)) {
            return false;
        }
        if (departureTime == null) {
            if (other.departureTime != null) {
                return false;
            }
        } else if (!departureTime.equals(other.departureTime)) {
            return false;
        }
        if (onwardAirline == null) {
            if (other.onwardAirline != null) {
                return false;
            }
        } else if (!onwardAirline.equals(other.onwardAirline)) {
            return false;
        }
        if (onwardAirlineName == null) {
            if (other.onwardAirlineName != null) {
                return false;
            }
        } else if (!onwardAirlineName.equals(other.onwardAirlineName)) {
            return false;
        }
        if (onwardAllFlightDetails == null) {
            if (other.onwardAllFlightDetails != null) {
                return false;
            }
        } else if (!onwardAllFlightDetails.equals(other.onwardAllFlightDetails)) {
            return false;
        }
        if (onwardArrivalDate == null) {
            if (other.onwardArrivalDate != null) {
                return false;
            }
        } else if (!onwardArrivalDate.equals(other.onwardArrivalDate)) {
            return false;
        }
        if (onwardArrivalTime == null) {
            if (other.onwardArrivalTime != null) {
                return false;
            }
        } else if (!onwardArrivalTime.equals(other.onwardArrivalTime)) {
            return false;
        }
        if (onwardDepartureDate == null) {
            if (other.onwardDepartureDate != null) {
                return false;
            }
        } else if (!onwardDepartureDate.equals(other.onwardDepartureDate)) {
            return false;
        }
        if (onwardDepartureTime == null) {
            if (other.onwardDepartureTime != null) {
                return false;
            }
        } else if (!onwardDepartureTime.equals(other.onwardDepartureTime)) {
            return false;
        }
        if (onwardVia == null) {
            if (other.onwardVia != null) {
                return false;
            }
        } else if (!onwardVia.equals(other.onwardVia)) {
            return false;
        }
        if (price == null) {
            if (other.price != null) {
                return false;
            }
        } else if (!price.equals(other.price)) {
            return false;
        }
        if (returnAirline == null) {
            if (other.returnAirline != null) {
                return false;
            }
        } else if (!returnAirline.equals(other.returnAirline)) {
            return false;
        }
        if (returnAirlineName == null) {
            if (other.returnAirlineName != null) {
                return false;
            }
        } else if (!returnAirlineName.equals(other.returnAirlineName)) {
            return false;
        }
        if (returnAllFlightDetails == null) {
            if (other.returnAllFlightDetails != null) {
                return false;
            }
        } else if (!returnAllFlightDetails.equals(other.returnAllFlightDetails)) {
            return false;
        }
        if (returnArrivalDate == null) {
            if (other.returnArrivalDate != null) {
                return false;
            }
        } else if (!returnArrivalDate.equals(other.returnArrivalDate)) {
            return false;
        }
        if (returnArrivalTime == null) {
            if (other.returnArrivalTime != null) {
                return false;
            }
        } else if (!returnArrivalTime.equals(other.returnArrivalTime)) {
            return false;
        }
        if (returnDepartureDate == null) {
            if (other.returnDepartureDate != null) {
                return false;
            }
        } else if (!returnDepartureDate.equals(other.returnDepartureDate)) {
            return false;
        }
        if (returnDepartureTime == null) {
            if (other.returnDepartureTime != null) {
                return false;
            }
        } else if (!returnDepartureTime.equals(other.returnDepartureTime)) {
            return false;
        }
        if (returnVia == null) {
            if (other.returnVia != null) {
                return false;
            }
        } else if (!returnVia.equals(other.returnVia)) {
            return false;
        }
        if (via == null) {
            if (other.via != null) {
                return false;
            }
        } else if (!via.equals(other.via)) {
            return false;
        }
        return true;
    }
    @JsonProperty("cp")
    public Boolean getCheapest() {
        return cheapest;
    }
    @JsonProperty("cp")
    public void setCheapest(Boolean cheapest) {
        cheapest = cheapest;
    }

}
