package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Abstract cache interface, will have concrete implementations for various cache providers.
 *
 * @author sdesai
 *
 */
public interface Cache {

    /**
     * Enumerates the statuses of a lock call on a cache.
     *
     * @author suresh
     *
     */
    enum LockStatus {
        FAILED, SUCCESS, UNKNOWN
    }

    String getConnectionString();

    Object getNativeConnection(String key);

    void returnNativeConnection(String key, Object con);

    /**
     * Get the object of type V for a given object id.
     *
     * Following are the rules for the get method: <br>
     * <br>
     *
     * When not to process the value object: <br>
     * ===================================== <br>
     * 1. If valueClass of the MemcachedImpl object is null. 2. If obj is a not a instace of CachedObjectConverter <br>
     * <br>
     * When to process the value object: <br>
     * ================================= <br>
     * 1. If valueClass is defined and value object is of type CachedObjectConverter. 2. If collClass is defined then, process each of the strings, call convertToObject method on each value object and
     * add it to collection. 3. If no collClass defined, then just process the single string stored for object and return the corresponding object by calling convertToObject method. (This logic is
     * handled in getValueObject() api)
     *
     * @param key
     *            key to retrieve.
     * @return cached object, NULL if cache miss
     * @throws CacheRetrieveException
     *             if an error occured during key retrieval
     */
    Object get(String key) throws CacheRetrieveException;

    /**
     * Get a list of objects of type V for the given list of object ids.
     *
     * @param ids
     *            array of object ids
     * @return cached object, NULL if cache miss
     * @throws CacheRetrieveException
     *             if an error occured during key retrieval
     */
    List<Object> get(String [] ids) throws CacheRetrieveException;

    /**
     * Get a map of key value objects.
     *
     * @param ids
     *            array of object ids
     *
     * @return cached object, NULL if cache miss
     *
     * @throws CacheRetrieveException
     *             if an error occured during key retrieval
     */
    Map<String, Object> getMulti(String[] ids) throws CacheRetrieveException;

    /**
     * Following are the rules for the put method.<br>
     * <br>
     *
     * When not to process the value object:<br>
     * ==================================== <br>
     * 1. If value object is not of type CachedObjectConverter then add the value object to memcache as it is. 2. If value object is a collection, and elements within the collection are not of type
     * CachedObjectConverter then just put the collection in memcache. <br>
     * When to process the value object: <br>
     * ================================ <br>
     * 1. If value object is of type CachedObjectConverter, call convertToString() method on the value object and store the corresponding string inside memcache. 2. If value object is a collection and
     * elemetns within the collection are of type CachedObjectConverter then convert the entire collection in to a single string by calling the convertToString() method of each element and append it
     * to each of the objects. Currently we are using "^^" as the delimiter for record separation.
     *
     * @param id
     *            id of the object to store
     * @param obj
     *            object to store
     * @throws CacheStoreException
     *             if an error occured when setting key/value
     * @return true on success false otherwise
     */
    boolean put(String id, Object obj) throws CacheStoreException;

    /**
     *
     * This method will be used if date of expiry needs to be mentioned.
     *
     * Following are the rules for the put method.<br>
     * <br>
     *
     * When not to process the value object:<br>
     * ==================================== <br>
     * 1. If value object is not of type CachedObjectConverter then add the value object to memcache as it is. 2. If value object is a collection, and elements within the collection are not of type
     * CachedObjectConverter then just put the collection in memcache. <br>
     * When to process the value object: <br>
     * ================================ <br>
     * 1. If value object is of type CachedObjectConverter, call convertToString() method on the value object and store the corresponding string inside memcache. 2. If value object is a collection and
     * elemetns within the collection are of type CachedObjectConverter then convert the entire collection in to a single string by calling the convertToString() method of each element and append it
     * to each of the objects. Currently we are using "^^" as the delimiter for record separation.
     *
     * {@inheritDoc}
     */
    boolean put(String key, Object val, Date dateOfExpiry) throws CacheStoreException;

    /**
     *
     * This method will be used if date of expiry needs to be mentioned.
     *
     * Following are the rules for the put method.<br>
     * <br>
     *
     * When not to process the value object:<br>
     * ==================================== <br>
     * 1. If value object is not of type CachedObjectConverter then add the value object to memcache as it is. 2. If value object is a collection, and elements within the collection are not of type
     * CachedObjectConverter then just put the collection in memcache. <br>
     * When to process the value object: <br>
     * ================================ <br>
     * 1. If value object is of type CachedObjectConverter, call convertToString() method on the value object and store the corresponding string inside memcache. 2. If value object is a collection and
     * elemetns within the collection are of type CachedObjectConverter then convert the entire collection in to a single string by calling the convertToString() method of each element and append it
     * to each of the objects. Currently we are using "^^" as the delimiter for record separation.
     *
     * {@inheritDoc}
     */
    boolean put(String key, Object val, int expirySeconds) throws CacheStoreException;

    boolean setExpiry(String key, int expirySeconds) throws CacheStoreException;

    boolean atomicUpdate(String id, int expirySeconds, ValueUpdater updater, int maxTries);

    boolean atomicUpdate(List<String> idList, int expirySeconds, List<ValueUpdater> updaterList, int maxTries);

    boolean syncPut(String key, Object val, int expirySeconds) throws CacheStoreException;

    boolean syncPut(String key, Object val, Date dateOfExpiry) throws CacheStoreException;

    /**
     * Put a list of objects of type V for the given list of object ids.
     *
     * @param ids
     *            array of ids of the objects to store
     * @param objs
     *            list of objects
     * @throws CacheStoreException
     *             if an error occured when setting key/values
     */
    void put(String [] ids, List<Object> objs) throws CacheStoreException;

    /**
     * Put a map of objects of type &lt;K,V&gt;. Inherited from map interface
     *
     * @param map
     *            map of objects
     * @throws CacheStoreException
     *             if an error occured when setting key/value
     */
    void putAll(Map<String, ? extends Object> map) throws CacheStoreException;

    /**
     * Removes the mapping for this key from this map if it is present (optional operation). More formally, if this map contains a mapping from key <tt>k</tt> to value <tt>v</tt> such that
     * <code>(key==null ?  k==null : key.equals(k))</code>, that mapping is removed. (The map can contain at most one such mapping.)
     * <p/>
     * <p>
     * Returns the value to which the map previously associated the key, or <tt>null</tt> if the map contained no mapping for this key. (A <tt>null</tt> return can also indicate that the map
     * previously associated <tt>null</tt> with the specified key if the implementation supports <tt>null</tt> values.) The map will not contain a mapping for the specified key once the call returns.
     *
     * Note that unlike its Map equivalent, this method does not return the value of the key being removed.
     *
     * @param key
     *            key whose mapping is to be removed from the map.
     *
     * @return boolean true - if either of the servers successfully deleted the key. false - if both the servers failed to remove or if the key doesn't exist in the cache.
     */
    boolean remove(String key);

    boolean syncRemove(String key);

    /**
     * Removes all mappings from this map (optional operation).
     *
     * @throws CacheDeleteException
     *             if underlying cache(s) could not be fully cleared
     */
    void clear() throws CacheDeleteException;

    void destroy() throws Exception;

    /**
     * Adds a Key to Cache if not already present. This is an atomic operation and can be used for a timed lock as below.
     *
     * @param key
     *            Key
     * @param val
     *            Value
     * @param expirySeconds
     *            Expiry Seconds
     * @return True if added successfully, false if add failed, usually because there is already a value against the key.
     * @throws CacheStoreException Exception on Cache Storage
     */
    boolean add(String key, Object val, int expirySeconds) throws CacheStoreException;

    /**
     * A simple lock API based on the atomic nature of add call.
     *
     * @param key
     *            Key to lock ON. Only one caller with this key will get SUCCESS as return value
     * @param expirySeconds
     *            Seconds after which key (i.e lock) will expire
     * @param verifyFailedLock
     *            If this is false this API is equivalent to add(key, "1", expirySeconds). If true and add returns a false a get call is made to make sure there is a value returned since add could
     *            have failed due to connectivity problems. If get call returns null after add fails UNKNOWN status is returned.
     * @return FAILED to indicate that key is already locaked, SUCCESS to indicate success, and UNKNOWN if not certain
     * @throws CacheStoreException
     *             On Exception
     */
    LockStatus lock(String key, int expirySeconds, boolean verifyFailedLock) throws CacheStoreException;

    Object get(String key, Charset charset) throws CacheRetrieveException;

    Map<String, Object> getMulti(String[] ids, Charset charset)
            throws CacheRetrieveException;

    List<Object> get(String [] ids, Charset charset) throws CacheRetrieveException;

    long getServerTime();

	boolean putMulti(Map<String, ? extends Object> map, int expirySeconds) throws CacheStoreException;

	default boolean putMulti(Map<String, ? extends Object> map, int expirySeconds, Charset charset) throws CacheStoreException {
		return false;
	};

    Long incrBy(String key, long integer) throws CacheStoreException;
    Long incr(String key)  throws CacheStoreException;
    Long decr(String key)  throws CacheStoreException;
    Long decrBy(String key, long integer)  throws CacheStoreException;

}
