package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.Tag;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
*
* @author amith
*
*/
public class TagsHolder implements Serializable {

    private static final long serialVersionUID = -3435530290709870876L;

    @JsonProperty("company_tags")
    private List<Tag> tags;

    public TagsHolder() {
        tags = new ArrayList<Tag>(1);
    }

    @JsonProperty("company_tags")
    public List<Tag> getTags() {
        return tags;
    }

    @JsonProperty("company_tags")
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}
