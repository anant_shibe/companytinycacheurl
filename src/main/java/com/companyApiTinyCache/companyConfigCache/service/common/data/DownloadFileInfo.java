package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.File;

public class DownloadFileInfo {

    private File file;

    private String contentType;

    private String fileName;

    private boolean isGzip;

    public File getFile() {
        return file;
    }

    public void setFile(File pfile) {
        file = pfile;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String pcontentType) {
        contentType = pcontentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String pfileName) {
        fileName = pfileName;
    }

    public boolean isGzip() {
        return isGzip;
    }

    public void setGzip(boolean pisGzip) {
        isGzip = pisGzip;
    }

}
