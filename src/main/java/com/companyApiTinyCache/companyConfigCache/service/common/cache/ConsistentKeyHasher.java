package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.CRC32;

/**
 * Consistent Hashing Algorithm. Essentially a number of hashcodes (Default of 160)
 * are generated for each server by adding numeric suffixes to each server's connection string and generating the hashcode when this Key Hasher is created.
 * When a key is to be mapped to a server, the nearest server hashcode greater than or equal to the hashcode of the key is used.
 * (The server hashcodes are treated as points on a circle. So if the there is no greater hashcode the smallest server hashcode is used. For implementation
 * all server hashcodes are sorted in an array and a parallel array contains the corresponding cache. Binary search on the hashcode array is used to find the
 * closest server hashcode)
 *
 * This algorithm re-destributes keys uniformly when a new server is added or removed to the Sharded Set of servers.
 * Assigning different weights to different servers is not supported.
 *
 * @author suresh
 */
public class ConsistentKeyHasher implements KeyHasher {

    public static final int HASHES_PER_CACHE = 160;

    private int[] sortedConnectionHashes;

    // Parallel array of same length as sortedConnectionHashes
    private Cache[] cachesForHashes;

    private static final Charset UTF_8_CHARSET = Charset.forName("UTF-8");

    private static class TempHashInfo implements Comparable<TempHashInfo> {

        private int connectionHash;

        private int connectionIndex;

        @Override
        public int compareTo(TempHashInfo otherHashInfo) {
            int result;
            if (connectionHash < otherHashInfo.connectionHash) {
                result = -1;
            } else if (connectionHash > otherHashInfo.connectionHash) {
                result = 1;
            } else {
                result = connectionIndex - otherHashInfo.connectionIndex;
            }

            return result;
        }

    }

    // Variation of jdk version which does not match but only finds insertion point whose val >= keyHash
    private static int binarySearch(int[] a, int keyHash) {
        int low = 0;
        int high = a.length - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal < keyHash) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return low;
    }

    private String getKeyToHash(Cache cache, int repetitionIndex) {
        // When testing with redis-airapi1.cltp.com:6379,redis-airapi2.cltp.com:6379,redis-airapi3.cltp.com:6379
        // all keys got mapped to the 1st server because the connection Strings differ by 1 digit around the middle.
        // Because of this the 3 hashes for a given repetitionIndex are very close making the hash of a key land
        // in between two groups of hashes with adjacent repetition index thus always mapping to one server.
        //
        // serverNameRandomizer digits are added to provide more variation in the connectionStrings itself by
        // including last 2 digits of java hashCode as prefix and suffix
        int javaHashCode = cache.getConnectionString().hashCode();
        String serverNameRandomizer1 = String.valueOf(javaHashCode % 10);
        String serverNameRandomizer2 = String.valueOf((javaHashCode / 10) % 10);
        String connectionKeyToHash = "" + serverNameRandomizer1 + serverNameRandomizer2 + '-' + repetitionIndex + '-' + cache.getConnectionString()
                                     + '-' + repetitionIndex + '-' + serverNameRandomizer2 + serverNameRandomizer1;
        return connectionKeyToHash;
    }

    // Using CRC32 hashing since it seems to give more even distribution based on 2 days of airapi redis keys
    private int hashCode(String keyToHash) {
        CRC32 crc32 = new CRC32();
        crc32.update(keyToHash.getBytes(UTF_8_CHARSET));
        long checkSum = crc32.getValue();
        int result = (int) (checkSum & 0xffffffffL); /* Truncate to 32-bits */
        return result;
    }

    /**
     * Creates a Consistent Key Hasher.
     * @param caches caches to pick one from
     */
    public ConsistentKeyHasher(Cache[] caches) {
        if (caches.length > 1) {
            TempHashInfo tempHashInfo;
            List<TempHashInfo> tempHashInfos = new ArrayList<TempHashInfo>(HASHES_PER_CACHE * caches.length);
            int cacheIndex;
            for (cacheIndex = 0; cacheIndex < caches.length; cacheIndex++) {
                Cache cache = caches[cacheIndex];
                for (int i = 0; i < HASHES_PER_CACHE; i++) {
                    tempHashInfo = new TempHashInfo();
                    tempHashInfo.connectionHash = hashCode(getKeyToHash(cache, i));
                    tempHashInfo.connectionIndex = cacheIndex;
                    tempHashInfos.add(tempHashInfo);
                }
            }
            Collections.sort(tempHashInfos);
            int len = tempHashInfos.size();
            sortedConnectionHashes = new int[len];
            cachesForHashes = new Cache[len];
            len = 0;
            for (TempHashInfo hi : tempHashInfos) {
                sortedConnectionHashes[len] = hi.connectionHash;
                cachesForHashes[len] = caches[hi.connectionIndex];
                len++;
            }
        } else {
            cachesForHashes = Arrays.copyOf(caches, caches.length);
        }
    }

    @Override
    public Cache getCacheForKey(String key) {
        Cache cache = null;
        if (cachesForHashes.length > 1) {
            int hash = hashCode(key);
            int pos = binarySearch(sortedConnectionHashes, hash);
            if (pos >= sortedConnectionHashes.length) {
                pos = 0;
            }
            cache = cachesForHashes[pos];
        } else if (cachesForHashes.length > 0) {
            cache = cachesForHashes[0];
        }

        return cache;
    }

}
