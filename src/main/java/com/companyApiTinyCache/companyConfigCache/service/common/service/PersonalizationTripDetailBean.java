package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;

import java.util.List;

public class PersonalizationTripDetailBean {

    private String tripRef;
    private String tripName;
    private String travelDate;
    private String productType;
    private String airline;
    private String from;
    private List<String> paxName;
    private String hotelName;
    private String city;
    private String hotelId;
    private String checkInTime;
    private String bookingDate;
    private String area;
    private String checkInDate;
    private String flightNumber;
    private boolean multicity;

    /**
     * @return the checkInTime
     */
    public String getCheckInTime() {
        return checkInTime;
    }

    /**
     * @param checkInTime
     *            the checkInTime to set
     */
    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    /**
     * @return the bookingDate
     */
    public String getBookingDate() {
        return bookingDate;
    }

    /**
     * @param bookingDate
     *            the bookingDate to set
     */
    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    /**
     * @return the area
     */
    public String getArea() {
        return area;
    }

    /**
     * @param area
     *            the area to set
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * @return the paxName
     */
    public List<String> getPaxName() {
        return paxName;
    }

    /**
     * @param paxName
     *            the paxName to set
     */
    public void setPaxName(List<String> paxName) {
        this.paxName = paxName;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from
     *            the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to
     *            the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the checkInData
     */
    public String getCheckInDate() {
        return checkInDate;
    }

    /**
     * @param checkInData
     *            the checkInData to set
     */
    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    private String to;
    private String checkInData;

    /**
     * @return the airline
     */
    public String getAirline() {
        return airline;
    }

    /**
     * @param airline
     *            the airline to set
     */
    public void setAirline(String airline) {
        this.airline = airline;
    }

    /**
     * @return the hotelName
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * @param hotelName
     *            the hotelName to set
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the hotelId
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * @param hotelId
     *            the hotelId to set
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * @return the tripRef
     */
    public String getTripRef() {
        return tripRef;
    }

    /**
     * @param tripRef
     *            the tripRef to set
     */
    public void setTripRef(String tripRef) {
        this.tripRef = tripRef;
    }

    /**
     * @return the tripName
     */
    public String getTripName() {
        return tripName;
    }

    /**
     * @param tripName
     *            the tripName to set
     */
    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    /**
     * @return the travelDate
     */
    public String getTravelDate() {
        return travelDate;
    }

    /**
     * @param travelDate
     *            the travelDate to set
     */
    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    /**
     * @param productType
     *            the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param flightNumber
     *            the flightNumber to set
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * @return the flightNumber
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * @param mc
     *            the multicity to set
     */
    public void setMulticity(boolean mc) {
        this.multicity = mc;
    }

    /**
     * @return the multicity
     */
    @MethodExcludeCoverage
    public boolean isMulticity() {
        return multicity;
    }
}
