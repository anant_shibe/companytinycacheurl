package com.companyApiTinyCache.companyConfigCache.service.common.data;

public class AccessRoleDetail {

    private long id;

    private String access_role;

    private String descripttion;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccess_role() {
        return access_role;
    }

    public void setAccess_role(String access_role) {
        this.access_role = access_role;
    }

    public String getDescripttion() {
        return descripttion;
    }

    public void setDescripttion(String descripttion) {
        this.descripttion = descripttion;
    }

}
