package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Amit Kumar Sharma
 */
public class CachedMobileHeadersResource extends CachedUrlResource<List<String>> implements StreamResourceLoader<List<String>>{

    private static final Log logger = LogFactory.getLog(CachedMobileHeadersResource.class);
    private static final String COMMENT_MARKER = "#";
    private static final String USER_AGENT_HEADER = "user-agent";
    private static final String APP_AGENT_HEADER = "app-agent";
    private static final String MOBILE_OVERRIDE_COOKIE_NAME = "ct.mobile.override";

    private boolean mobileLoggingEnabled;

    CachedMobileHeadersResource(CachedProperties pCommonCachedProperties, String url) throws Exception {
        super(pCommonCachedProperties, url, null, false);
        setLoader(this);
        refreshResource();
        mobileLoggingEnabled = pCommonCachedProperties.getBooleanPropertyValue("ct.mobile.log.enabled", false);
    }

    private boolean isCommented(String line) {
        boolean result = false;
        line = StringUtils.trimToEmpty(line);
        if (line.startsWith(COMMENT_MARKER) || StringUtils.isEmpty(line)) {
            result = true;
        }
        return result;
    }

    public boolean isMobileApp(HttpServletRequest request) {
        // If mobile override cookie is set it means that
        // we should treat this device as non-mobile, else
        // controllers will start rendering mobile views
        if (isMobileOverrideCookieSet(request)) {
            return false;
        }
        String appAgentValue = request.getHeader(APP_AGENT_HEADER);
        return (appAgentValue != null && !appAgentValue.isEmpty());
    }

    public boolean isMobile(HttpServletRequest request) {
        String userAgent = request.getHeader(USER_AGENT_HEADER);
        // If mobile override cookie is set it means that
        // we should treat this device as non-mobile, else
        // controllers will start rendering mobile views
        return !isMobileOverrideCookieSet(request) && isMobile(userAgent);
    }

    public boolean isMobile(String userAgent) {

        boolean isMobileDevice = false;

        if (userAgent == null) { // crafted calls are not mobile calls
            return false;
        }
        Pattern pattern = Pattern.compile("^mozilla/([\\d].[\\d])? \\((mobile;)");
        Matcher matcher = pattern.matcher(userAgent.toLowerCase());
        if (matcher.find()) {
            return true;
        }

        List<String> mobileClues = getResource();
        if (mobileClues != null) {
            for (String mobileClue : mobileClues) {

                // case insensitivity for better matches
                mobileClue = mobileClue.toLowerCase();

                if (userAgent.toLowerCase().contains(mobileClue)) {
                    isMobileDevice = true;
                    if (mobileLoggingEnabled) {
                        logger.info("Mobile agent detected | " + mobileClue + " | " + userAgent);
                    }
                    break;
                }
            }
        }

        return isMobileDevice;

    }

    private static boolean isMobileOverrideCookieSet(HttpServletRequest request) {

        boolean result = false;

        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return false;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equalsIgnoreCase(MOBILE_OVERRIDE_COOKIE_NAME)) {
                result = true;
                break;
            }
        }

        return result;
    }

    public GenUtil.MobilePlatforms getMobilePlatform(HttpServletRequest request) {
        String userAgent = request.getHeader(USER_AGENT_HEADER).toLowerCase();
        return GenUtil.getMobilePlatform(userAgent);
    }

    public String getOSVersion(HttpServletRequest request) {
        String userAgent = request.getHeader(USER_AGENT_HEADER).toLowerCase();
        return GenUtil.getMobileOSVersion(userAgent);
    }

    public String getMobileBrowserName(HttpServletRequest request) {
        String userAgent = request.getHeader(CachedMobileHeadersResource.USER_AGENT_HEADER).toLowerCase();
        return GenUtil.getMobileBrowserName(userAgent);
    }

    @Override
    public List<String> loadStreamResource(InputStream is) throws Exception {
        List<String> mobileHeaders = new ArrayList<>();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            line = line.trim();
            if (isCommented(line)) {
                continue;
            }
            mobileHeaders.add(line);
        }
        br.close();
        return mobileHeaders;
    }
}
