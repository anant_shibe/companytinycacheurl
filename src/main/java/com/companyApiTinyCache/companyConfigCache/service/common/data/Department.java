package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class Department implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 6091584885895051992L;

    private int departmentId;

    private String departmentName;

    @JsonIgnore
    private Collection<Person> people;

    private int companyId;

    private int status;

    private Date createdAt;

    private Date upatedAt;

    @JsonProperty("id")
    public int getDepartmentId() {
        return this.departmentId;
    }

    @JsonProperty("id")
    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    @JsonProperty("name")
    public String getDepartmentName() {
        return this.departmentName;
    }

    @JsonProperty("name")
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @JsonIgnore
    public Collection<Person> getPeople() {
        return this.people;
    }

    @JsonIgnore
    public void setPeople(Collection<Person> people) {
        this.people = people;
    }

    @JsonProperty("company_id")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getCompanyId() {
        return companyId;
    }

    @JsonProperty("company_id")
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @JsonProperty("status")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    public int getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(int status) {
        this.status = status;
    }

    @JsonProperty("created_at")
    public Date getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public Date getUpatedAt() {
        return upatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpatedAt(Date upatedAt) {
        this.upatedAt = upatedAt;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Department == false)
            return false;
        Department dept = (Department) obj;
        if (departmentId != dept.getDepartmentId()) {
            return false;
        }
        if ((departmentName != null || dept.getDepartmentName() != null) && !GenUtil.equals(departmentName, dept.getDepartmentName())) {
            return false;
        }
        if (companyId != dept.getCompanyId()) {
            return false;
        }
        if (status != dept.getStatus()) {
            return false;
        }

        return true;
    }

}
