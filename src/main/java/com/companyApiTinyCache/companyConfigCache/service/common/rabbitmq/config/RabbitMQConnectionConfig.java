package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.config;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

@ClassExcludeCoverage
public class RabbitMQConnectionConfig {
	//requireed parameters
	private String hostName;
	private String vhostName;
	private String userName;
	private String password;
	private String port;
	private boolean isAutoRecoverable;
	private RabbitMQConnectionConfig(ConnectionBuilder builder) {
		this.hostName = builder.hostName;
		this.vhostName = builder.vhostName;
		this.userName = builder.userName;
		this.password = builder.password;
		this.port = builder.port;
		this.isAutoRecoverable = builder.isAutorecoverable;
	}
	public String getHostName() {
		return hostName;
	}
	public boolean isAutoRecoverable() {
		return isAutoRecoverable;
	}
	public void setAutoRecoverable(boolean isAutoRecoverable) {
		this.isAutoRecoverable = isAutoRecoverable;
	}
	public String getVhostName() {
		return vhostName;
	}
	public String getUserName() {
		return userName;
	}
	public String getPassword() {
		return password;
	}
	public String getPort() {
		return port;
	}
	
	public static class ConnectionBuilder {
		private String hostName;
		private String vhostName;
		private String userName;
		private String password;
		private String port;
		private boolean isAutorecoverable;
		
		
		public boolean isAutorecoverable() {
			return isAutorecoverable;
		}
		public ConnectionBuilder setAutorecoverable(boolean isAutorecoverable) {
			this.isAutorecoverable = isAutorecoverable;
			return this;
		}
		public ConnectionBuilder setHostName(String hostName) {
			this.hostName = hostName;
			return this;
		}
		public ConnectionBuilder setVhostName(String vhostName) {
			this.vhostName = vhostName;
			return this;
		}
		public ConnectionBuilder setUserName(String userName) {
			this.userName = userName;
			return this;
		}
		public ConnectionBuilder setPassword(String password) {
			this.password = password;
			return this;
		}
		public ConnectionBuilder setPort(String port) {
			this.port = port;
			return this;
		}
		public RabbitMQConnectionConfig build() {
			return new RabbitMQConnectionConfig(this);
		}
	}
}