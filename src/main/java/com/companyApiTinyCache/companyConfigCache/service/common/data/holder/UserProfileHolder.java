package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.FaultDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.data.UserProfile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author amith
 *
 */
public class UserProfileHolder implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = -78418568260218209L;

    @JsonProperty("user")
    private UserProfile userProfile;

    @JsonProperty("person")
    private UserProfile travelerProfile;

    @JsonProperty("faults")
    private List<FaultDetail> faultDetails;

    /**
     * @return the userProfile
     */
    @JsonProperty("user")
    public UserProfile getUserProfile() {
        return userProfile;
    }

    /**
     * @param userProfile
     *            the userProfile to set
     */
    @JsonProperty("user")
    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    /**
     * @return the travelerProfile
     */
    @JsonProperty("person")
    public UserProfile getTravelerProfile() {
        return travelerProfile;
    }

    /**
     * @param travelerProfile
     *            the travelerProfile to set
     */
    @JsonProperty("person")
    public void setTravelerProfile(UserProfile travelerProfile) {
        this.travelerProfile = travelerProfile;
    }

    /**
     * @return the faultDetail
     */
    @JsonProperty("faults")
    public List<FaultDetail> getFaultDetails() {
        return faultDetails;
    }

    @JsonProperty("faults")
    public void setFaultDetails(List<FaultDetail> faultDetails) {
        this.faultDetails = faultDetails;
    }

    @JsonIgnore
    public FaultDetail getFaultDetail() {
        if (this.faultDetails != null) {
            return faultDetails.get(0);
        }
        return null;
    }

}
