package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent.SimpleThreadFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * A simple timer which is essentially a ScheduledThreadPoolExecutor with poolSize 1. Since it is single threaded It is assumed that the actual work of the scheduled tasks is taken up by a suitable
 * executor. So the scheduled tasks submitted to this should be extremely light weight and should use a Thread pool for anything beyond checking the time or some status, even memcached/redis
 * operations should be in a separate task.
 * 
 * We already used quartz configured via Spring for many jobs. This is created mainly to submit a delayed task dynamically.
 * 
 * @author Suresh
 * 
 */
public class CtTimer extends ScheduledThreadPoolExecutor implements DisposableBean {

    public CtTimer() {
        super(1, SimpleThreadFactory.daemonThreadFactory);
        setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
    }

    @Override
    public void destroy() throws Exception {
        shutdown();
    }

}
