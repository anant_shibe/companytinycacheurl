package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

public interface MessageSender {
    void send(Object message) throws Exception;
}
