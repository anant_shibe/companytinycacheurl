package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Collections;

/**
 * APIUtil.
 */
public final class APIUtil {

	private APIUtil() {
    }

    private static final Log logger = LogFactory.getLog(APIUtil.class);
    private static final ObjectMapper mapper = formObjectMapper();

    public static final String EMPTY_MAP_JSON = serializeObjectToJson(Collections.EMPTY_MAP);

    public static ObjectMapper formObjectMapper() {
        ObjectMapper mapper = JsonUtil.getNewObjectMapper();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
/*        mapper.getSerializationConfig().setDateFormat(sdf);
        mapper.getDeserializationConfig().setDateFormat(sdf);*/

        return mapper;
    }

    public static String serializeObjectToJson(Object obj) {
        StringWriter sw = new StringWriter();

        try {
            mapper.writeValue(sw, obj);
        } catch (JsonGenerationException e) {
            logger.error("Json Generation Exception : ", e);
        } catch (JsonMappingException e) {
            logger.error("Json Mapping Exception : ", e);
        } catch (IOException e) {
            logger.error("IO Exception : ", e);
        }

        return sw.toString();
    }

    public static Object deserializeJsonToObject(String json, Object obj) {
        Object obj2 = deserializeJsonToObject(json, obj.getClass());
        return obj2 == null ? obj : obj2;
    }

    public static <T> T deserializeJsonToObject(String json, Class<T> cls) {
        return deserializeJson(mapper, json, cls);
    }

    public static <T> T deserializeJsonToObject(ObjectMapper objectMapper, String json, Class<T> cls) {
    	if (objectMapper == null) {
    		objectMapper = mapper;
    	}
        return deserializeJson(objectMapper, json, cls);
    }

	public static <T> T deserializeJson(ObjectMapper objectMapper, String json, Class<T> cls) {
		long start = 0;
        if (logger.isDebugEnabled()) {
            logger.debug("input json = " + json);
            start = System.currentTimeMillis();
        }

        T obj = null;
        try {
            obj = objectMapper.readValue(json, cls);
        } catch (JsonParseException e) {
            logger.error("Json Parsing Exception : ", e);
        } catch (JsonMappingException e) {
            logger.error("Json Mapping Exception : ", e);
        } catch (IOException e) {
            logger.error("IO Exception : ", e);
        }

        if (logger.isDebugEnabled() && obj != null) {
            logger.debug("Time taken to bind Json to Object(" + cls + ") : " + (System.currentTimeMillis() - start) + " milliseconds");
        }
        return obj;
	}

    public static String getSourceString(String module, String feature, String action) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotBlank(module)) {
            stringBuilder.append(module);
            if (StringUtils.isNotBlank(feature)) {
                stringBuilder.append("_").append(feature);
            }
            if (StringUtils.isNotBlank(action)) {
                stringBuilder.append("_").append(action);
            }
        } else {
            stringBuilder.append("UNKNOWN_REQUEST");
        }
        return stringBuilder.toString();
    }

}
