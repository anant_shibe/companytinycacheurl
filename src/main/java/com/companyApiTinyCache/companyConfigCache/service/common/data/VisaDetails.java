package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.util.Date;

public class VisaDetails {

    private String companyName;
    private String contactPerson;
    private String email;
    private String mobile;
    private String altContact;
    private Address address;
    private String visaCountryName;
    private Date departDate;
    private String visaType;
    private String urgencyType;
    private String entries;
    private int adult;
    private int child;

    public int getAdult() {
        return adult;
    }

    public void setAdult(int adult) {
        this.adult = adult;
    }

    public int getChild() {
        return child;
    }

    public void setChild(int child) {
        this.child = child;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAltContact() {
        return altContact;
    }

    public void setAltContact(String altContact) {
        this.altContact = altContact;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getVisaCountryName() {
        return visaCountryName;
    }

    public void setVisaCountryName(String visaCountryName) {
        this.visaCountryName = visaCountryName;
    }

    public Date getDepartDate() {
        return departDate;
    }

    public void setDepartDate(Date departDate) {
        this.departDate = departDate;
    }

    public String getVisaType() {
        return visaType;
    }

    public void setVisaType(String visaType) {
        this.visaType = visaType;
    }

    public String getUrgencyType() {
        return urgencyType;
    }

    public void setUrgencyType(String type) {
        this.urgencyType = type;
    }

    public String getEntries() {
        return entries;
    }

    public void setEntries(String entries) {
        this.entries = entries;
    }

}
