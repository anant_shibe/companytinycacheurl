package com.companyApiTinyCache.companyConfigCache.service.common.service;

import java.util.List;

public interface StoreClicksHandler {

    boolean saveClickData(String pageCode, String instanceId, List<String> clicks);
}
