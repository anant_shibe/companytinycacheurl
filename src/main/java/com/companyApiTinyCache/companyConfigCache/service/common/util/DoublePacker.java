package com.companyApiTinyCache.companyConfigCache.service.common.util;

/**
 * A class which is used to store positive double values like currency which are usually whole numbers into a
 * short variable. Positive whole numbers from 0 - 32767 can be store directly as a short. Small double values
 * upto around 320 which are not whole numbers but are positive numbers with utmost 2 decimals (like margin percents
 * and tax percents) are also represented using negative short numbers.
 *
 * Those double values which cannot be directly encoded as a short as mentioned above are stored into a doubles
 * array passed by the user after checking if it already exists and the index into the array is encoded as a
 * negative short value. Utmost MAX_ACTUAL_DOUBLES double values can be handled this way.
 *
 * This is used for memory optimization in Chmm JVM Cache since a short takes 2 bytes whereas a double takes
 * 8 bytes.
 *
 * @author suresh
 *
 */
public final class DoublePacker {

    public static final int MAX_ACTUAL_DOUBLES = 1024;

    private static final int INDEXED_VALUES_START = -Short.MIN_VALUE - MAX_ACTUAL_DOUBLES;

    // Some random number treated as Double.Nan since Double.NaN == Double.NaN is false
    public static final double INVALID_NUMBER = -13782.191414;

    private DoublePacker() {
    }

    public static double getDouble(int shortVal, double [] doubles, int baseIndex) {
        double d = DoublePacker.INVALID_NUMBER;
        if (shortVal >= 0) {
            d = shortVal;
        } else if (shortVal != -1) {
            shortVal = -1 - shortVal;
            if (shortVal < INDEXED_VALUES_START) {
                d = shortVal / 100.0;
            } else {
                d = doubles[shortVal + baseIndex - INDEXED_VALUES_START];
            }
        }
        return d;
    }

    public static int convertToShort(double d, double [] doubles, int baseIndex, int [] lenHolder) {
        int shortVal = -1;
        if (d != DoublePacker.INVALID_NUMBER) {
            if (d >= 0 && d <= Short.MAX_VALUE) {
                long longVal = Math.round(d * 100000);
                if (longVal % 100000 == 0) {
                    // Perfect positive integer so use directly
                    shortVal = (int) Math.round(d);
                } else if (longVal % 1000 == 0) {
                    // Has only 2 decimals try to pack as a negative short value
                    int valInto100 = (int) (longVal / 1000);
                    if (valInto100 < INDEXED_VALUES_START) {
                        shortVal = -1 - valInto100;
                    }
                }
            }
            if (shortVal == -1) {
                // Packing failed add the double to array if not already present and encode the index into
                // a negative short value
                int i, len = lenHolder[0];
                for (i = baseIndex; i < len; i++) {
                    if (Math.abs(doubles[i] - d) < 0.000001) {
                        break;
                    }
                }
                if (i == len) {
                    doubles[len] = d;
                    lenHolder[0]++;
                }
                if (i - baseIndex >= MAX_ACTUAL_DOUBLES) {
                    throw new IllegalStateException("Number of unpackable doubles " + (i - baseIndex + 1) + " exceeds max allowed " + MAX_ACTUAL_DOUBLES);
                }
                i = i - baseIndex + INDEXED_VALUES_START;
                shortVal = -1 - i;
            }
        }

        return shortVal;
    }

    // Created to pass coverage check during build
    static void testConstructor() {
        new DoublePacker();
    }
}
