package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;
import java.util.List;

/**
 * Stores the Insurance Information for a booking.
 *
 * @author suresh
 */
public class Insurance implements Serializable {

    private static final long serialVersionUID = -869338480856933777L;

    private String policyNumber;
    private List<InsuredPerson> insuredPeople;
    private double totalPremium;

    public String getPolicyNumber() {
        return this.policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public List<InsuredPerson> getInsuredPeople() {
        return this.insuredPeople;
    }

    public void setInsuredPeople(List<InsuredPerson> insuredPeople) {
        this.insuredPeople = insuredPeople;
    }

    public int getNumberOfInsuredPeople() {
        int numberOfInsuredPeople = this.insuredPeople.size();
        return numberOfInsuredPeople;
    }

    public double getTotalPremium() {
        double total = 0;
        for (InsuredPerson person : insuredPeople) {
            total += person.getTotalPremium();
        }
        return total;
    }

    public void setTotalPremium(double totalPremium) {
        this.totalPremium = totalPremium;
    }
}
