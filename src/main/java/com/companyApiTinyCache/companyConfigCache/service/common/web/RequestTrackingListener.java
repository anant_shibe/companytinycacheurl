package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedPropertiesManager;
import com.companyApiTinyCache.companyConfigCache.service.common.util.ConfigLoader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * RequestListener which keeps track of active and slow requests in a Tomcat. When a request is created it is timestamped
 * which helps in identifying the age. Also the request Object and the calling thread is saved to produce logs. This info
 * is removed in the requestDestroyed method.
 *
 * @author suresh
 */
@ClassExcludeCoverage
public class RequestTrackingListener implements ServletRequestListener, ConfigLoader<RequestTrackingListener.Config> {

    public RequestTrackingListener(ApplicationContext applicationContext){
        init(applicationContext);
    }

    /**
     * Class containing information regarding an active request.
     *
     * @author suresh
     */
    public static class RequestInfo implements Comparable<RequestInfo> {

        private static final ThreadLocal<RequestInfo> RequestInfoTL = new ThreadLocal<RequestInfo>();

        private int id;

        private long startTime;

        private volatile long endTime;

        private ServletRequest request;

        private Thread requestThread;

        private Vector<Thread> activeChildThreads;

        public RequestInfo(int pid, ServletRequest prequest, boolean trackChildRequests) {
            id = pid;
            request = prequest;
            requestThread = Thread.currentThread();
            long now = System.currentTimeMillis();
            startTime = now;
            if (trackChildRequests) {
                // Tracking of child threads is a little expensive since you have to create a Vector and each child thread has to add to it
                // So tracking of child threads is activated for a max of 5 minutes since last visit to /common/requests
                activeChildThreads = new Vector<Thread>();
                RequestInfoTL.set(this);
            } else {
                RequestInfoTL.remove();
            }
        }

        public static RequestInfo threadLocal() {
            return RequestInfoTL.get();
        }

        public int getId() {
            return id;
        }

        public long getStartTime() {
            return startTime;
        }

        public long getEndTime() {
            return endTime;
        }

        public ServletRequest getRequest() {
            return request;
        }

        public Thread getRequestThread() {
            return requestThread;
        }

        public Vector<Thread> getActiveChildThreads() {
            return activeChildThreads;
        }

        @Override
        public int compareTo(RequestInfo o) {
            int result = 1;
            if (o != null) {
                long x = startTime;
                long y = o.startTime;
                result = (x < y) ? -1 : ((x == y) ? 0 : 1);
            }
            return result;
        }
    }

    public static enum ChildRequestTrackingOption {
        NEVER, AUTO, ALWAYS
    }

    // The below holder class is required because java 6 does not have an array of volatiles
    private static class RequestInfoHolder {
        private volatile RequestInfo requestInfo;
    }

    static class Config {
        private boolean isRequestTrackingEnabled = true;

        private ChildRequestTrackingOption childRequestTrackingOption = ChildRequestTrackingOption.AUTO;

        public Config(Map<String, String> cachedPropertiesResource) {
            if (cachedPropertiesResource != null) {
                isRequestTrackingEnabled = CachedProperties.getBooleanPropertyValue(cachedPropertiesResource, ENABLE_PROPERTY, true);
                childRequestTrackingOption = CachedProperties.getEnumPropertyValue(cachedPropertiesResource, CHILD_REQUEST_OPTION_PROPERTY, ChildRequestTrackingOption.AUTO, ChildRequestTrackingOption.class);
            }
        }
    }

    private static final Log log = LogFactory.getLog(RequestTrackingListener.class);

    private static final String CT_REQUEST_INFO_HOLDER = "CT_REQUEST_INFO_HOLDER";

    private static final RequestInfoHolder [] requestInfos = new RequestInfoHolder[512];

    private static AtomicInteger requestIdHolder = new AtomicInteger();

    public static final String ENABLE_PROPERTY = "ct.common.request-tracking.enabled";

    public static final String CHILD_REQUEST_OPTION_PROPERTY = "ct.common.request-tracking.child-request.option";

    public static volatile long LAST_INSPECT_TIME_MILLIS;

    private volatile boolean initialized;

    private CachedPropertiesManager<Config> configManager;

    static {
        for (int i = 0; i < requestInfos.length; i++) {
            requestInfos[i] = new RequestInfoHolder();
        }
    }

    public static List<RequestInfo> getActiveRequests() {
        List<RequestInfo> activeRequests = new ArrayList<RequestInfo>();
        for (RequestInfoHolder requestInfoHolder : requestInfos) {
            RequestInfo nextRequest = requestInfoHolder.requestInfo;
            if (nextRequest != null) {
                activeRequests.add(nextRequest);
            }
        }
        Collections.sort(activeRequests);

        return activeRequests;
    }

    @Override
    public void requestInitialized(ServletRequestEvent requestEvent) {
        try {
            Config c = configManager.getConfig();
            if (c.isRequestTrackingEnabled) {
                int index, requestId;
                int i = 0, maxTries = 2 * requestInfos.length;
                RequestInfoHolder requestInfoHolder;
                RequestInfo requestInfo;
                boolean found = false;
                long now = System.currentTimeMillis();
                do {
                    i++;
                    requestId = requestIdHolder.incrementAndGet();
                    index = requestId % requestInfos.length;
                    requestInfoHolder = requestInfos[index];
                    requestInfo = requestInfoHolder.requestInfo;
                    if (requestInfo == null || now - requestInfo.startTime > 1800000) {
                        found = true;
                    }
                } while (i < maxTries && !found);
                if (found) {
                    ServletRequest request = requestEvent.getServletRequest();
                    boolean trackChildRequests = false;
                    switch (c.childRequestTrackingOption) {
                      case NEVER:
                        break;
                      case AUTO:
                          // Tracking of child threads is a little expensive since you have to create a Vector and each child thread has to add to it
                          // So tracking of child threads is activated for a max of 5 minutes since last visit to /common/requests
                        if (System.currentTimeMillis() - LAST_INSPECT_TIME_MILLIS < 300000) {
                            trackChildRequests = true;
                        }
                        break;
                      case ALWAYS:
                        trackChildRequests = true;
                        break;
                      default:
                        break;
                    }
                    requestInfo = new RequestInfo(requestId, requestEvent.getServletRequest(), trackChildRequests);
                    requestInfoHolder.requestInfo = requestInfo;
                    request.setAttribute(CT_REQUEST_INFO_HOLDER, requestInfoHolder);
                }
            }
        } catch (Exception e) {
            log.error("Exception in RequestTracker initialization", e);
        }
    }

    @Override
    public void requestDestroyed(ServletRequestEvent requestEvent) {
        try {
            RequestInfo.RequestInfoTL.remove();
            ServletRequest request = requestEvent.getServletRequest();
            RequestInfoHolder requestInfoHolder = (RequestInfoHolder) request.getAttribute(CT_REQUEST_INFO_HOLDER);
            if (requestInfoHolder != null) {
                RequestInfo requestInfo = requestInfoHolder.requestInfo;
                if (requestInfo != null) {
                    requestInfoHolder.requestInfo = null;
                    requestInfo.endTime = System.currentTimeMillis();
                }
            }
        } catch (Exception e) {
            log.error("Exception in RequestTracker cleanup", e);
        }
    }

    private void init(ApplicationContext applicationContext) {
        CachedProperties commonCachedProperties = (CachedProperties) applicationContext.getBean("commonCachedProperties");
        configManager = new CachedPropertiesManager<Config>(commonCachedProperties, this);
    }

    @Override
    public Config loadConfig(Map<String, String> cachedPropertiesResource) {
        return new Config(cachedPropertiesResource);
    }
}
