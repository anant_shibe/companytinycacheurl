package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Optional;

/**
 * Cached Resource which gets cache data from a URL.
 *
 * @author suresh
 *
 * @param <R> Resource Type
 */
public class CachedUrlResource<R> extends CachedResource<R> {

    private static final Log logger = LogFactory.getLog(CachedUrlResource.class);

    protected CachedProperties commonCachedProperties;

    protected StreamResourceLoader<R> loader;

    private boolean isUrlProperty;

    protected String prevUrl;

    private String url;

    private String urlDigest;

    private static final ImmutableSet<String> EXTENTIONS = ImmutableSet.of(".json", ".properties", ".txt", ".csv", ".js");

    private static final String PATH_STATIC_PROD = "classpath:/static/prod/";

    private static final String PATH_STATIC_QA = "classpath:static/qa/";

    public CachedUrlResource(CachedProperties pcommonCachedProperties, String purl, StreamResourceLoader<R> ploader, boolean loadResource) throws MalformedURLException {
        if (purl != null && purl.indexOf('/') < 0) {
            isUrlProperty = true;
        }
        commonCachedProperties = pcommonCachedProperties;
        setUrl(purl);
        String url = getUrl();
        setName(url);
        if (loadResource) {
            refreshResource();
        }
        // Setting all cached url resources to 30 mins
        super.setCacheMillis(commonCachedProperties.getLongPropertyValue("ct.air.cached.url.resources.default.cached.time", 1800000));
    }

    public CachedUrlResource(CachedProperties pcommonCachedProperties, String purl) throws MalformedURLException {
        this(pcommonCachedProperties, purl, null, true);
    }

    /**
     * A convenience method over getResource() to getResource from holder nad map from singleValue.
     * This method has been deprecated as it is just a synonym for getResource().
     *
     * @return Resource Value
     */
    @Deprecated
    public R getResourceValue() {
        return getResource();
    }

    /**
     * Calls loader.loadStreamResource.
     *
     * @param is InputStream
     * @return Resource
     * @throws Exception
     */
    protected R loadStreamResource(InputStream is) throws Exception {
        return loader.loadStreamResource(is);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.cleartrip.common.util.CachedEnumUrlResources#loadResource(long, boolean)
     */
    @Override
    protected R loadResource(long cacheMillis, boolean force) throws Exception {
        boolean isloadEnabled = commonCachedProperties.getBooleanPropertyValue("ct.common.urlresources.enabled", true);
        boolean isDebug = logger.isDebugEnabled();
        if (!isloadEnabled) {
            logger.debug("LOAD NOT ENABLED");
            return null;
        }
        urlDigest = computeDigest(null);
        RestUtil.setResetAuthentication(commonCachedProperties);
        R newResource = null;
        String currentUrl = getUrl();
        if (isDebug) {
            logger.debug("BASE URL = " + currentUrl + " resouce = " + resource + " bean = " + this);
        }
        if (getName() == null) {
            setName(currentUrl);
        }
        if (!currentUrl.equals(prevUrl)) {
            if (isDebug) {
                logger.debug("BASE URL CHANGED FROM PREV URL = " + prevUrl);
            }
            if (getName() == prevUrl) {
                setName(currentUrl);
            }
            prevUrl = currentUrl;
        }

        currentUrl = decideStaticOrDynamicResource(currentUrl);
        setName(currentUrl);

        newResource = getResource(currentUrl);

        if (isDebug) {
            logger.debug(" RETURNING " + newResource);
        }
        return newResource;
    }

    private String decideStaticOrDynamicResource(String currentUrl) throws IOException {
        String fileName = currentUrl.substring(currentUrl.lastIndexOf("/") + 1);
        Optional<String> ctConfigVersion = getCtConfigVersion(fileName);
        Optional<String> localVersion = getLocalVersion(fileName);
        logVersions(fileName, ctConfigVersion, localVersion);
        if(!isLocalResourceInSync(ctConfigVersion, localVersion)) {
            return currentUrl;
        }
        boolean isProdEnv = System.getProperty("spring.profiles.active").contains("prod");
        return (isProdEnv ? PATH_STATIC_PROD : PATH_STATIC_QA) + fileName;
    }

    private void logVersions(String fileName, Optional<String> ctConfigVersion, Optional<String> localVersion) {
        logger.error("FileName : " + fileName);
        if(!ctConfigVersion.isPresent()) {
            logger.error("CtConfigVersion version is not present");
        } else {
            logger.error("CtConfig version :"+ ctConfigVersion.get());
        }
        if(!localVersion.isPresent()) {
            logger.error("Local version is not present");
        } else {
            logger.error("Local version:" + localVersion.get());
        }
    }

    private boolean isLocalResourceInSync(Optional<String> ctConfigVersion, Optional<String> localVersion) throws IOException {
        return ctConfigVersion.isPresent() && localVersion.isPresent()
                && ctConfigVersion.get().equals(localVersion.get());
    }

    /**
     * @param fileName
     * @return
     */
    private Optional<String> getLocalVersion(String fileName) throws IOException {
       return Optional.ofNullable(VersionReader.localResources.get(fileName));
    }

    /**
     *  fetch the version of resource in Ct-config
     * @param fileName
     * @return
     */
    private Optional<String> getCtConfigVersion(String fileName) {
       return Optional.ofNullable(VersionReader.getResourceVersion(fileName));
    }

    private R getResource(String currentUrl) throws Exception {
        int ttl = commonCachedProperties.getIntPropertyValue("ct.ctconfig.resources.expiry.time",86400);
        int resourceUrlTimeout = commonCachedProperties.getIntPropertyValue("ct.ctconfig.resources.url.timeout",2000);
        RestResponse response = getUrlResponse(currentUrl,ttl,resourceUrlTimeout);

        int responseCode = response.getCode();
        if (responseCode == 304) {
            logger.warn("No change in resource while polling :"+ currentUrl);
            return this.resource;
        }
        if (responseCode != -1 && responseCode != 200) {
            throw new IOException("Got error code: " + response.getCode() + ", message: " + response.getMessage());
        }
        return getResourceFromResponse(response);
    }

   private R getResourceFromResponse(RestResponse response) throws Exception {
        R newResource;
        byte[] b = response.getMessage().getBytes();
        try (InputStream in = new ByteArrayInputStream(b)) {
            newResource = loadStreamResource(in);
            urlDigest = computeDigest(response.getMessage());
            return newResource;
        }
    }

    public String getUrl() {
        String urlToUse = url;
        String ctConfigDNS = commonCachedProperties.getPropertyValue("ct.air.ct.config.hostname",CT_CONFIG_DNS);
        if (isUrlProperty) {
            urlToUse = commonCachedProperties.getPropertyValue(urlToUse);
            urlToUse = urlToUse.contains(FETCH_RESOURCE) ? ctConfigDNS + urlToUse : urlToUse;
        }

        return urlToUse;
    }

    @Override
    protected String computeDigest() {
        return urlDigest;
    }

    /**
     * Setter for url.
     *
     * @param purl
     *            the url to set
     */
    public void setUrl(String purl) {
        url = purl;
    }

    /**
     * Setter for loader.
     *
     * @param ploader
     *            the loader to set
     */
    public void setLoader(StreamResourceLoader<R> ploader) {
        loader = ploader;
        registerConsulListener(getUrl());
    }

    private void registerConsulListener(String url) {

        final Optional<String> fileType = RestUtil.getQueryParamValue(url, "fileType");

        if (fileType.isPresent()) {
            logger.error("Registering consul event listener for fileType " + fileType.get());
            commonCachedProperties.registerConsulEventListener(fileType.get(), () -> {
                logger.error("Received CachedUrlResource Refresh Event for key = " + fileType.get() + ". Go to refresh Now.");
                refreshResource(0, false);
                logger.error("CachedUrlResource refresh completed for " + fileType.get());
            });
        } else if (isCtConfigExtension(url)) {
            String resourceName = url.substring(url.lastIndexOf("/") + 1);
            commonCachedProperties.registerConsulEventListener(resourceName, vertical, (key, value) -> {
                logger.error("CT-CONFIG refresh event triggered for Key =" + key);
                VersionReader.remove(resourceName);
                refreshResource(0, false);
                logger.error("CT-CONFIG refreshResource completed for Key =" + key);
            });
        }  else {
            logger.error("Not registering consul event listener as not a ct-admin url => " + url);
        }
    }

    private boolean isCtConfigExtension(String url) {
        return url != null && url.contains(FETCH_RESOURCE) ? EXTENTIONS.stream().filter(ext -> url.endsWith(ext)).findAny().isPresent() : false;
    }
}
