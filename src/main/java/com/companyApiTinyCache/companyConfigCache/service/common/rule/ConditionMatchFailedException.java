package com.companyApiTinyCache.companyConfigCache.service.common.rule;

public class ConditionMatchFailedException extends Exception {

    private static final long serialVersionUID = 1L;

    public ConditionMatchFailedException(String message) {
        super(message);
    }

}
