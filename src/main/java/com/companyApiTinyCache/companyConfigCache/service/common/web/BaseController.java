package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedMobileHeadersResource;
import com.companyApiTinyCache.companyConfigCache.service.common.org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;

/**
 * All application controllers will extend from this BaseController. Common logic across all controllers will go here. <br/>
 * All sub-classes must override handleRequestInternal(HttpServletRequest request, HttpServletResponse response).
 * 
 * @author Sanjeev Desai
 * 
 */
public abstract class BaseController extends SimpleFormController {

    protected CachedMobileHeadersResource cachedMobileHeadersResource;

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        return new Object();
    }

    public void setCachedMobileHeadersResource(CachedMobileHeadersResource cachedMobileHeadersResource) {
        this.cachedMobileHeadersResource = cachedMobileHeadersResource;
    }
}
