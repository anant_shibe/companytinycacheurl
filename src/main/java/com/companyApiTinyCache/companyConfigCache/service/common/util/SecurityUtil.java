package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.cache.Cache;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CommonUrlResources;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.UserDataKey;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tiemens.secretshare.engine.SecretShare;
import com.tiemens.secretshare.engine.SecretShare.CombineOutput;
import com.tiemens.secretshare.engine.SecretShare.PublicInfo;
import com.tiemens.secretshare.engine.SecretShare.ShareInfo;
import com.tiemens.secretshare.engine.SecretShare.SplitSecretOutput;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Security Related Utility methods.
 *
 * @author suresh
 */
public final class SecurityUtil {

    private static final Log logger = LogFactory.getLog(SecurityUtil.class);

    private static final String CT_IPLIGENCE_URL = "ct.services.places.ipligence.url";

    private static final String CT_IPLIGENCE_CACHE = "ct.places.ipligence.cache";

    private static volatile CachedProperties cachedProperties;

    private static final String NPS_SECURE_AUTH_PRIVATE_KEY = "HKfjFH23DK45FGJFCK";

    private static final byte[] defaultAuthKey = {65, (byte) 128, 127, (byte) 230, 30, (byte) 252, (byte) 136, (byte) 224, (byte) 135, (byte) 187, (byte) 142, (byte) 208, 104, (byte) 183, (byte) 183,
            23, (byte) 184, 11, 104, 102, 84, 107, 21, (byte) 240, (byte) 182, (byte) 242, (byte) 146, 107, 73, 11, 11, 3};

    private SecurityUtil() {
    }

    public static CachedProperties getProperties() {
        if (cachedProperties == null) {
            cachedProperties = (CachedProperties) ServicesApplicationContext.getBean("commonCachedProperties");
        }
        return cachedProperties;
    }

    public static byte[] generateRandomKey(int size, Random random) {
        BigInteger bigInteger = new BigInteger((size * 8), 1, random);
        byte[] bytes = bigInteger.toByteArray();
        return bytes;
    }

    public static List<byte[]> splitSecret(byte[] secret, int size) {
        List<byte[]> splits = new ArrayList<byte[]>();

        PublicInfo publicInfo = new SecretShare.PublicInfo(size, size, null, null);
        SecretShare secretShare = new SecretShare(publicInfo);
        SplitSecretOutput generate = secretShare.split(new BigInteger(secret));
        List<ShareInfo> shareInfos = generate.getShareInfos();

        for (ShareInfo shareInfo : shareInfos) {
            splits.add(shareInfo.getShare().toByteArray());
        }

        return splits;
    }

    public static byte[] combineSecret(List<byte[]> splits) {
        List<ShareInfo> shareInfos = new ArrayList<ShareInfo>();
        int size = splits.size();

        PublicInfo publicInfo = new SecretShare.PublicInfo(size, size, null, null);
        SecretShare secretShare = new SecretShare(publicInfo);

        for (int i = 0; i < size; i++) {
            shareInfos.add(new ShareInfo(i + 1, new BigInteger(splits.get(i)), publicInfo));
        }

        CombineOutput output = secretShare.combine(shareInfos);
        return output.getSecret().toByteArray();
    }

    public static String aesEncrypt(String originalString, byte[] authKey) {
        String encryptedString = "";

        if (originalString != null) {
            byte[] encoded = aesEncrypt(originalString.getBytes(), authKey);

            if (encoded != null) {
                encoded = Base64.encodeBase64(encoded);
                encryptedString = new String(encoded);
            }
        }

        return encryptedString;
    }

    public static byte[] aesEncrypt(byte[] input, byte[] authKey) {
        byte[] encoded = null;

        try {
            int rem = input.length % 16;

            if (rem > 0) {
                byte[] paddedArray = new byte[input.length + 16 - rem];
                System.arraycopy(input, 0, paddedArray, 0, input.length);
                input = paddedArray;
            }

            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            SecretKeySpec keySpec = new SecretKeySpec(authKey, "AES");

            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
            encoded = cipher.doFinal(input);
        } catch (Exception e) {
            String encodedInput = "undefined";
            String encodedKey = "undefined";

            try {
                encodedInput = GenUtil.asHex(input);
                encodedKey = GenUtil.asHex(authKey);
            } catch (Exception e1) {
                encodedKey = encodedKey;
            }

            logger.warn("Error encrypting '" + encodedInput + "' using aesEncrypt with key: " + encodedKey, e);
        }

        return encoded;
    }

    public static String aesDecrypt(String encryptedString, byte[] authKey) {
        String originalString = "";

        if (encryptedString != null) {
            byte[] byteArrToDecode = encryptedString.getBytes();
            byte[] decodedString = Base64.decodeBase64(byteArrToDecode);
            byte[] original = aesDecrypt(decodedString, authKey);

            if (original != null) {
                originalString = new String(original);
            }
        }

        return originalString;
    }

    public static byte[] aesDecrypt(byte[] encrypted, byte[] authKey) {
        byte[] original = null;

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            SecretKeySpec keySpec = new SecretKeySpec(authKey, "AES");

            cipher.init(Cipher.DECRYPT_MODE, keySpec);
            byte[] decypted = cipher.doFinal(encrypted);

            // Ignore 0 padded bytes.
            int i = decypted.length;

            do {
                i--;
            } while (i >= 0 && decypted[i] == 0);

            i++;

            original = new byte[i];
            System.arraycopy(decypted, 0, original, 0, i);
        } catch (Exception e) {
            String encodedInput = "undefined";
            String encodedKey = "undefined";

            try {
                encodedInput = GenUtil.asHex(encrypted);
                encodedKey = GenUtil.asHex(authKey);
            } catch (Exception e1) {
                encodedKey = encodedKey;
            }

            logger.warn("Error decrypting '" + encodedInput + "' using aesEncrypt with key: " + encodedKey, e);
        }

        return original;
    }

    public static byte[] getAcubeKey(CustomCachedUrlResources cachedUrlResources) throws IOException {
        int i;
        long start = System.currentTimeMillis();

        byte[] authKeyArr = new byte[32];

        String key = null;
        UrlResources<CommonUrlResources, String> urlResources = null;

        if (cachedUrlResources != null) {
            urlResources = cachedUrlResources.getResource();
        }

        if (urlResources != null) {
            EnumMap<CommonUrlResources, String> cachedUrlData = urlResources.getUrlData();
            key = cachedUrlData.get(CommonUrlResources.ACUBE_SERVER_KEY);
        }

        if (!StringUtils.isBlank(key)) {
            String newKey = removeChar(key, '|').trim();
            String[] tokens = null;
            tokens = newKey.split(" ", -1);
            for (i = 0; i < tokens.length; i++) {
                authKeyArr[i] = (byte) Integer.parseInt(tokens[i]);
            }
        } else {
            for (i = 0; i < defaultAuthKey.length; i++) {
                authKeyArr[i] = defaultAuthKey[i];
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Get Auth Key Time = " + (System.currentTimeMillis() - start));
        }
        return authKeyArr;
    }

    public static String removeChar(String s, char c) {
        String r = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c) {
                r += s.charAt(i);
            }
        }
        return r;
    }

    /**
     * Checks if the given roleId is present in the role map from security filter or request attribute.
     *
     * @param cookieInfoMap Cookies
     * @param roleId Role Id
     * @return boolean True if role present
     */
    public static boolean hasRole(Map<String, String> cookieInfoMap, String roleId) {
        boolean hasRole = false;
        String userRoles = null;
        if (!StringUtils.isBlank(roleId) && cookieInfoMap != null) {
            userRoles = cookieInfoMap.get(CommonEnumConstants.UserDetails.USER_ROLES.toString());
        }
        if (!StringUtils.isBlank(userRoles)) {
            String[] loggedInUserRoles = userRoles.split(",");
            for (int i = 0; i < loggedInUserRoles.length; i++) {
                if (loggedInUserRoles[i].equals(roleId)) {
                    hasRole = true;
                    break;
                }
            }
        }

        return hasRole;
    }

    /**
     * Checks if the given roleId is present in the role map attributefrom the passed request.
     *
     * @param request Request
     * @param roleId roleId
     * @return boolean True if Role Id is present
     */
    public static boolean hasRole(HttpServletRequest request, String roleId) {
        boolean hasRole = false;
        if (!StringUtils.isBlank(roleId)) {
            Map<String, String> cookieInfoMap = (Map<String, String>) request.getAttribute(UserDataKey.USER_DATA_KEY.name());
            hasRole = hasRole(cookieInfoMap, roleId);
        }
        return hasRole;
    }

    /**
     * Returns null if not duplicate "" if duplicate but original page not found in Memcached, returns the cached page otherwise.
     *
     * @param request Request
     * @param cache Cache Object
     * @param key Key
     * @return String Cached Page if duplicate submission or null otherwise
     */
    public static String checkDuplicateSubmission(HttpServletRequest request, Cache cache, String key) {
        String orgHtml = null;
        try {
            if (cache != null) {
                Object obj = cache.get(key);
                if (obj != null && obj instanceof String) {
                    orgHtml = (String) obj;
                }
            }
        } catch (Exception e) {
            logger.error("Error getting org Form: ", e);
        }
        if (orgHtml == null) {
            try {
                String sessionStatus = (String) request.getSession().getAttribute(key);
                if ("submitted".equals(sessionStatus)) {
                    orgHtml = "";
                }
            } catch (Exception e) {
                logger.error("Error getting Submitted status from session:", e);
            }
        }
        return orgHtml;
    }

    public static String getHost(HttpServletRequest httpReq) throws MalformedURLException {
        URL requestUrl = new URL(httpReq.getRequestURL().toString());
        int port = requestUrl.getPort();
        String host = requestUrl.getHost();
        if (port > 0) {
            host = host + ':' + port;
        }
        return host;
    }

    public static String getHostWithoutPort(HttpServletRequest httpReq) throws MalformedURLException {
        URL requestUrl = new URL(httpReq.getRequestURL().toString());
        return requestUrl.getHost();
    }

    /**
     * Method will return the domain to country map.
     *
     * e.g. cleartrip.com - IN cleartrip.ae - AE
     *
     * @param props Properties
     * @return Map<String, String> Domain To country map
     */
    public static Map<String, Map<String, String>> getDomainToCountryMap(CachedProperties props) {
        Map<String, Map<String, String>> domainToCountryMap = Collections.EMPTY_MAP;
        String domainToCountryMapString = props.getPropertyValue("ct.domain.to.country.map");
        ObjectMapper mapper = JsonUtil.getObjectMapper();

        if (StringUtils.isNotBlank(domainToCountryMapString)) {
            try {
                domainToCountryMap = mapper.readValue(domainToCountryMapString, Map.class);

            } catch (Exception e) {
                logger.error("Exception while reading", e);
            }
        }
        return domainToCountryMap;
    }

    /**
     * Method will return the domain to country map.
     *
     * e.g. cleartrip.com - IN cleartrip.ae - AE
     *
     * @return Map<String, String>
     */
    public static Map<String, Map<String, String>> getDomainToCountryMap() {
        CachedProperties props = getProperties();
        Map<String, Map<String, String>> domainToCountryMap = getDomainToCountryMap(props);
        return domainToCountryMap;
    }

    /**
     * Returns the host based on referer.
     *
     * @param request
     * @return String
     * @throws MalformedURLException
     */
    private static String getHostBasedOnRefer(HttpServletRequest request) throws MalformedURLException {

        String referer = getReferer(request);

        String host = referer;
        // added this cond.CBS-23465 - getting mobile application deep link calls. like  android-app://com.cleartrip.android/cleartrip/www.cleartrip.com/
        // Raising java.net.MalformedURLException
        if (StringUtils.isNotBlank(host) && host.startsWith("android-app")) {
            host = "http://www.cleartrip.com/";
        }
        // added this condition to fix CBS-19023 issue - while checking the host it was comparing the referer URL
        if (StringUtils.isNotBlank(host)) {
            URL requestUrl = new URL(host);

            host = requestUrl.getHost();
        }

        if (StringUtils.isBlank(host) || !host.toLowerCase().contains("cleartrip")) {
            host = getHost(request);
        }

        return host;
    }

    /**
     * getDomainSettings for given domain.
     * @param domain
     * @return settings like country, currency, cookieDomain
     */
    private static Map<String, String> getDomainSettings(String domain) {
    	if (StringUtils.isNotBlank(domain)) {
    		Map<String, Map<String, String>> domainToCountryMap = getDomainToCountryMap();

            for (Entry<String, Map<String, String>> entry : domainToCountryMap.entrySet()) {

                Map<String, String> domainSettings = entry.getValue();

                String domainPattern = domainSettings.get("pattern");

                if (domain.toLowerCase().matches(domainPattern)) {
                    return domainSettings;
                }
            }
    	}
        return Collections.EMPTY_MAP;
    }

    /**
     * Returns the country information from request URL.
     *
     * e.g. If request url is www.cleartrip.ae, then country returned will be AE i.e. Dubai. If request url is www.cleartrip.com, then country returned will be IN
     *
     * @param request Request
     * @return String
     * @throws MalformedURLException When there is malformed URL
     */
    public static String getCountryFromDomain(HttpServletRequest request) throws MalformedURLException {
        String host = getHostBasedOnRefer(request);
        return getCountryFromDomain(host);
    }

    @MethodExcludeCoverage
    public static String getCurrencyFromDomain(HttpServletRequest request) throws MalformedURLException {
        String host = getHostBasedOnRefer(request);
        Map<String, String> domainSettings = getDomainSettings(host);
        return domainSettings.isEmpty() ? "INR" : domainSettings.get("sellingCurrency");
    }

    /**
     * Returns the country information for a given domain.
     *
     * e.g. If given domain is www.cleartrip.ae, then country returned will be AE i.e. Dubai. If given domain is www.cleartrip.com, then country returned will be IN
     *
     * @param domain URL domain
     * @return String
     */
    public static String getCountryFromDomain(String domain) {
        try {
            Map<String, String> domainSettings = getDomainSettings(domain);
            return domainSettings.isEmpty() ? "IN" : domainSettings.get("country");
        } catch (Exception e) {
            logger.error("Error while getting country information from domain: " + domain, e);
            return "IN";
        }
    }

    public static String getCookieDomainForRequestDomain(HttpServletRequest request) throws MalformedURLException {
        String host = getHostBasedOnRefer(request);
        Map<String, String> domainSettings = getDomainSettings(host);
        return domainSettings.isEmpty() ? ".cleartrip.com" : domainSettings.get("cookieDomain");
    }

    /**
     * This will return the domain from the request. for e.g. www.cleartrip.com - it will return .cleartrip.com www.cleartrip.ae it will return .cleartrip.ae
     *
     * @param request
     *            The http request
     * @return String
     * @throws MalformedURLException
     *             if the URL is not correct
     */
    public static String getDomainFromRequest(HttpServletRequest request) throws MalformedURLException {

        String domain = ".cleartrip.com";

        String host = getHostBasedOnRefer(request);

        Map<String, Map<String, String>> domainToCountryMap = getDomainToCountryMap();

        if (domainToCountryMap != null) {

            for (Entry<String, Map<String, String>> entry : domainToCountryMap.entrySet()) {

                Map<String, String> domainCountryMap = entry.getValue();

                String domainPattern = domainCountryMap.get("pattern");

                if (host.toLowerCase().matches(domainPattern)) {

                    domain = entry.getKey();
                    break;
                }

            }
        }
        return domain;
    }

    public static String getReferer(HttpServletRequest request) {
        String referer = request.getHeader("referer");
        return referer;
    }

    public static String replaceHost(String url, HttpServletRequest httpReq) throws MalformedURLException {
        if (url != null) {
            String host = getHost(httpReq);
            url = url.replaceAll("\\$\\{webhost}", host);
        }
        return url;
    }

    public static String getCtHloc(HttpServletRequest httpReq) {
        String ctHloc = httpReq.getHeader("x-ct-domain");
        if (ctHloc == null) {
            ctHloc = "www";
            String host = null;
            try {
                host = getHost(httpReq);
            } catch (MalformedURLException e) {
                host = "www.cleartrip.com";
            }
            if (host != null && host.startsWith("beta.")) {
                ctHloc = "beta";
            }
        }

        return ctHloc;
    }

    public static String replaceEnvPrefix(String url, HttpServletRequest httpReq) {
        if (url != null) {
            String ctHloc = getCtHloc(httpReq);
            url = url.replace("${ct.env.prefix}", ctHloc);
        }
        return url;
    }

    /**
     * Gets the user id for the logged in user.
     *
     * @param request Request
     *
     * @return user id.
     */
    public static long getUserId(HttpServletRequest request) {
        long userId = 0;
        SecurityBean securityBean = (SecurityBean) request.getAttribute(CommonEnumConstants.SESSION_SECURITY_BEAN_ID);
        if (securityBean != null) {
            if (securityBean.isUserSignedIn()) {
                userId = securityBean.getPeopleId();
            }
        }
		if (userId < 1) {
			String userIdFromRequest = request.getParameter("userId");
			String concurUserId = "";
			if (StringUtils.isBlank(userIdFromRequest)) {
				Cookie[] ck = request.getCookies();
				if (ck != null) {
					for (Cookie cookie : ck) {
						if (CommonEnumConstants.CONCUR_USER_ID.equals(cookie.getName())) {
							concurUserId = cookie.getValue();
							break;
						}
					}
				}
			} else {
				concurUserId = userIdFromRequest;
			}
			if (StringUtils.isNotBlank(concurUserId) && (StringUtils.isBlank(userIdFromRequest) || StringUtils.equals(userIdFromRequest, concurUserId))) {
				userId = Long.parseLong(concurUserId);
			}
        }
        return userId;
    }

/*
    * Sample Response :
    *
    * <ipligence> <city-name>DUBAI</city-name> <continent-code>ME</continent-code> <continent-name>MIDDLE EAST</continent-name> <country-code>AE</country-code> <country-name>UNITED ARAB
    * EMIRATES</country-name> <county-name/> <ip-from>3576304896</ip-from> <ip-to>3576308735</ip-to> <latitude>25.25</latitude> <longitude>55.28</longitude> <owner>EMIRATES TELECOMMUNICATIONS
    * CORPORATION</owner> <region-code/> <region-name/> <time-zone>GMT+4</time-zone> <ip-address>213.42.21.58</ip-address> <airport>Dubai Intl Arpt(DXB)</airport> </ipligence>
    *
*/    /**
     * Given the request the method will return user related information like the country of the user etc.
     *
     * @param request Servlet Request
     * @param cachedProperties Cached Properties
     * @return String
     */
    public static String getCountryFromIpaddress(HttpServletRequest request, CachedProperties cachedProperties) {
        String country = null;
        String regExpressionForIp = "\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b";
        try {
            String ipAddress = request.getParameter("fromIp");

            if (ipAddress == null) {
                ipAddress = GenUtil.getRemoteAddr(request);
            }

            // HARD CODING FOR THE 'AE' SITE
            // ipAddress = "213.42.21.58";

            String urlLocation = cachedProperties.getPropertyValue(CT_IPLIGENCE_URL);
            if (StringUtils.isNotBlank(urlLocation) && StringUtils.isNotBlank(ipAddress) && ipAddress.matches(regExpressionForIp)) {
                String queryString = "ip=" + ipAddress + "&cache=" + cachedProperties.getBooleanPropertyValue(CT_IPLIGENCE_CACHE, true);
                RestResponse restResponse = RestUtil.get(urlLocation, queryString);
                if (restResponse.getCode() == 200 && StringUtils.isNotBlank(restResponse.getMessage())) {
                    country = StringUtils.trimToNull(restResponse.getMessage());
                }
            }

        } catch (Exception e) {
            logger.error("Error retrieving the ipligence information for the request.", e);
        }

        if (country == null) {
            country = "IN";
        }

        return country.toUpperCase();
    }

    private static Collection<String> getUserRoles(HttpServletRequest request) {
        Map<String, String> cookieInfoMap = (Map<String, String>) request.getAttribute(UserDataKey.USER_DATA_KEY.name());
        String userRoles = null;
        if (cookieInfoMap != null) {
            userRoles = cookieInfoMap.get(CommonEnumConstants.UserDetails.USER_ROLES.toString());
        }
        if (!StringUtils.isBlank(userRoles)) {
            return Arrays.asList(userRoles.split(","));
        }
        return new ArrayList<String>();
    }

    public static boolean isAuthorized(HttpServletRequest request, CachedProperties commonCachedProperties, String appName, String servletPath) {
        if (!servletPath.startsWith("/")) {
            servletPath = "/" + servletPath;
        }

        Collection<String> userRoles = getUserRoles(request);
        String allowedRoles = commonCachedProperties.getPropertyValue(appName + servletPath, null);
        if (allowedRoles != null) {
            if (allowedRoles.equals("*")) {
                return true;
            }
            String[] roles = allowedRoles.split(",");
            for (String role : roles) {
                if (userRoles.contains(role)) {
                    return true;
                }
            }
            return false;
        }
        // Try All Prefixes
        int pos = servletPath.length();
        if (servletPath.charAt(pos - 1) == '/') {
            pos--;
        }
        Boolean allowed = null;
        do {
            String prefix = servletPath.substring(0, pos);
            String tmp = commonCachedProperties.getPropertyValue(appName + prefix + "/*", null);
            if (pos > 0) {
                pos = servletPath.lastIndexOf('/', pos - 1);
            } else {
                pos = -1;
            }
            if (tmp == null) {
                continue;
            }
            allowed = false;
            if (tmp.equals("*")) {
                allowed = true;
            } else {
                Collection<String> masterRoles = Arrays.asList(tmp.split(","));
                Collection<String> disallowedRoles = null;
                tmp = commonCachedProperties.getPropertyValue(appName + prefix + "/disallowed", null);
                if (tmp == null) {
                    disallowedRoles = new ArrayList<String>();
                } else {
                    disallowedRoles = Arrays.asList(tmp.split(","));
                }
                for (String userRole : userRoles) {
                    if (masterRoles.contains(userRole) && !disallowedRoles.contains(userRole)) {
                        allowed = true;
                    }
                }
            }
        } while (allowed == null && pos >= 0);
        return allowed == null ? false : allowed;
    }

    public static boolean isConcurUser(HttpServletResponse response, long userId, CachedProperties commonCachedProperties) {
		boolean isConcurUser = isConcurUser(commonCachedProperties, userId);
		if (isConcurUser) {
		    Cookie concurUserCookie = new Cookie(CommonEnumConstants.CONCUR_USER_ID, String.valueOf(userId));
			response.addCookie(concurUserCookie);
		}
		return isConcurUser;
	}

    public static boolean isConcurUser(CachedProperties commonCachedProperties, long loggedInUser) {
    	String concurAdminUsers = commonCachedProperties.getPropertyValue("ct.ct-admin.concur.admin-user-id.list", "");
    	List<String> concurAdminUsersList = Arrays.asList(concurAdminUsers.split(","));
    	return concurAdminUsersList.contains(String.valueOf(loggedInUser)) ? true : false;
    }

    /**
     * Method to fetch the r_referer site domain.
     *
     * @param requestHeaderMap Request Headers
     * @return String
     */
    public static Map<String, String> getRefererDomain(Map<String, String> requestHeaderMap) {
        Map<String, String> refererParams = new HashMap<String, String>();
        String refererDomain = "";
        try {
            if (requestHeaderMap != null && requestHeaderMap.containsKey("r_referer")) {
                URL url = new URL(requestHeaderMap.get("r_referer"));
                refererDomain = StringUtils.trimToEmpty(url.getHost());
                refererParams.put("r_referer", requestHeaderMap.get("r_referer"));
                refererParams.put("referer_domain", refererDomain);
                String refererQueryString = url.getQuery();
                if (refererQueryString != null) {
                    String[] refParams = refererQueryString.split("&");
                    if (refParams != null) {
                        for (String param : refParams) {
                            if (param.startsWith("utm_source")) {
                                String[] utm = param.split("=");
                                refererParams.put(utm[0], utm[1]);
                            }
                            if (param.startsWith("utm_medium")) {
                                String[] utm = param.split("=");
                                refererParams.put(utm[0], utm[1]);
                            }
                            if (param.startsWith("utm_campaign")) {
                                String[] utm = param.split("=");
                                refererParams.put(utm[0], utm[1]);
                            }
                            if (param.startsWith("utm_content")) {
                                String[] utm = param.split("=");
                                refererParams.put(utm[0], utm[1]);
                            }
                            if (param.startsWith("utm_term")) {
                                String[] utm = param.split("=");
                                refererParams.put(utm[0], utm[1]);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in SecurityUtil while fetching r_referer params from request header : " + e);
            refererDomain = "";
        }

        return refererParams;
    }

    @MethodExcludeCoverage
    public static String getAuthCookieName(CachedProperties cachedProperties) {
        return cachedProperties.getPropertyValue("ct.services.authentication.auth-cookiename");
    }

    @MethodExcludeCoverage
    public static String getNpsSecureHashKey(String tripRef, String txnId) {
        String message = null;
        if (StringUtils.isNotBlank(tripRef) && StringUtils.isNotBlank(txnId)) {
            message = tripRef + txnId + NPS_SECURE_AUTH_PRIVATE_KEY;
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                byte[] hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
                hash = Base64.encodeBase64(hash);
                message = new String(hash);
            } catch (NoSuchAlgorithmException ex) {
                logger.error("SecurityUtil | NPS Hashing", ex);
            }
        } else {
            logger.error("SecurityUtil | NPS Hashing | Either tripRef or txnId is blank");
        }
        return message;
    }
    
    @MethodExcludeCoverage
    public static String getMd5SecureHash(String message) {

        if (message != null) {
            try {
                MessageDigest m = MessageDigest.getInstance("MD5");

        		System.out.println("MD5: " + new BigInteger(1, m.digest()).toString(16));
                if(m != null) {
                	m.update(message.getBytes(), 0, message.length());
                    message = new BigInteger(1, m.digest()).toString(16);
                }
            } catch (NoSuchAlgorithmException ex) {
                logger.error("SecurityUtil | MD5 Hashing", ex);
            }
        } else {
            logger.error("SecurityUtil | MD5 Hashing ");
        }
        return message;
    }

    /**
     * Return cookie domain i.e.(append '.' at start of hostname) for specific non cleartrip sites.
     * 
     * @param request
     *            Request
     * @param cachedProperties
     *            commonCachedProperties
     * @return cookie domain i.e. (append '.' at start of hostname) if hostname contains the pattern in ct.services.wl.host.pattern.allow.set.cookie.domain property
     */
    public static String setCookieDomainWL(HttpServletRequest request, CachedProperties cachedProperties) {

        String hostname = GenUtil.resolveHostname(request);
        String[] host = cachedProperties.getPropertyValue("ct.services.wl.host.pattern.allow.set.cookie.domain", ".triplocal.").split(",");
        StringBuffer cookieDomain = new StringBuffer();
        for (String h : host) {
            if (hostname.contains(h)) {
                cookieDomain.append(".").append(hostname);
                if (logger.isDebugEnabled()) {
                    logger.debug("cookieDomain for local wl sites: " + cookieDomain);
                }
                return cookieDomain.toString();
            }
        }

        return null;
    }
}
