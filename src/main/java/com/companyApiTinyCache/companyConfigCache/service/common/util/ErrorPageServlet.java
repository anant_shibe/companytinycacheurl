package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Common servlet which is mapped to /error_pages/* in conf/web.xml. It forwards any *.html or *.html to the root
 * webapp. For example to serve a 404 from /not_found.html not_found.html is added under webapps/ROOT. Then 404
 * error code is mapped to /error_pages/not_found.html in conf/web.xml. So any webapp on getting a 404 gets a
 * request to /<app>/error_pages/not_found.html which hits this servlet which forwards it to /not_found.html
 * which serves the custom error page.
 *
 * @author suresh
 *
 */
@ClassExcludeCoverage
public class ErrorPageServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 5900138557489126002L;

    /**
     * {@inheritDoc}
     *
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String basePath = getInitParameter("basePath");
        if (basePath == null) {
            basePath = "";
        }
        String path = request.getRequestURI();
        int startPos = request.getContextPath().length() + 1;
        int pos = path.indexOf('/', startPos);
        // Below if is a kind of security check to not blindly forward all requests
        if (pos >= 0 && path.indexOf('/', pos + 1) < 0 && (path.endsWith(".html") || path.endsWith(".htm") || path.endsWith(".jsp"))) {
            ServletContext context = getServletContext();
            String baseContextPath = "/";
            String basePathPrefix = "";
            if (basePath.length() > 0) {
                baseContextPath = basePath;
                int pos1 = basePath.indexOf('/', 1);
                if (pos1 > 0) {
                    baseContextPath = basePath.substring(0, pos1);
                    basePathPrefix = basePath.substring(pos1);
                }
            }
            ServletContext baseContext = context.getContext(baseContextPath);
            RequestDispatcher dispatcher = baseContext.getRequestDispatcher(basePathPrefix + path.substring(pos));
            dispatcher.forward(request, response);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
