/**
 *
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;

/**
 * @author Ramesh
 *
 */
public class BookStats implements Serializable {

    private static final long serialVersionUID = 1L;
    private int userId;
    private String tripRef;
    private String txnId;
    private String voucherNo;
    private double minFare;
    private double maxFare;
    private double avgFare;
    private int resultCount;

    private double minSlotFare;
    private double maxSlotFare;
    private double avgSlotFare;

    private String minFareAirlineName;
    private String maxFareAirlineName;

    private String minFareFlightDeptTime;
    private String minSlotFareFlightDeptTime;

    private String minFareOnEighthDayBeforeDepart;

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the tripRef
     */
    public String getTripRef() {
        return tripRef;
    }

    /**
     * @param tripRef
     *            the tripRef to set
     */
    public void setTripRef(String tripRef) {
        this.tripRef = tripRef;
    }

    /**
     * @return the txnId
     */
    public String getTxnId() {
        return txnId;
    }

    /**
     * @param txnId
     *            the txnId to set
     */
    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    /**
     * @return the minFare
     */
    public double getMinFare() {
        return minFare;
    }

    /**
     * @param minFare
     *            the minFare to set
     */
    public void setMinFare(double minFare) {
        this.minFare = minFare;
    }

    /**
     * @return the maxFare
     */
    public double getMaxFare() {
        return maxFare;
    }

    /**
     * @param maxFare
     *            the maxFare to set
     */
    public void setMaxFare(double maxFare) {
        this.maxFare = maxFare;
    }

    /**
     * @return the avgFare
     */
    public double getAvgFare() {
        return avgFare;
    }

    /**
     * @param avgFare
     *            the avgFare to set
     */
    public void setAvgFare(double avgFare) {
        this.avgFare = avgFare;
    }

    /**
     * @return the resultCount
     */
    public int getResultCount() {
        return resultCount;
    }

    /**
     * @param resultCount
     *            the resultCount to set
     */
    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    /**
     * @return the voucherNo
     */
    public String getVoucherNo() {
        return voucherNo;
    }

    /**
     * @param voucherNo
     *            the voucherNo to set
     */
    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    /**
     * @return the minSlotFare
     */
    public double getMinSlotFare() {
        return minSlotFare;
    }

    /**
     * @param minSlotFare
     *            the minSlotFare to set
     */
    public void setMinSlotFare(double minSlotFare) {
        this.minSlotFare = minSlotFare;
    }

    /**
     * @return the maxSlotFare
     */
    public double getMaxSlotFare() {
        return maxSlotFare;
    }

    /**
     * @param maxSlotFare
     *            the maxSlotFare to set
     */
    public void setMaxSlotFare(double maxSlotFare) {
        this.maxSlotFare = maxSlotFare;
    }

    /**
     * @return the avgSlotFare
     */
    public double getAvgSlotFare() {
        return avgSlotFare;
    }

    /**
     * @param avgSlotFare
     *            the avgSlotFare to set
     */
    public void setAvgSlotFare(double avgSlotFare) {
        this.avgSlotFare = avgSlotFare;
    }

    public void setMinFareAirlineName(String minFlightName) {
        this.minFareAirlineName = minFlightName;
    }

    public String getMinFareAirlineName() {
        return minFareAirlineName;
    }

    public void setMaxFareAirlineName(String maxFlightName) {
        this.maxFareAirlineName = maxFlightName;
    }

    public String getMaxFareAirlineName() {
        return maxFareAirlineName;
    }

    public void setMinFareFlightDeptTime(String minFareFlightDeptTime) {
        this.minFareFlightDeptTime = minFareFlightDeptTime;
    }

    public String getMinFareFlightDeptTime() {
        return minFareFlightDeptTime;
    }

    public void setMinSlotFareFlightDeptTime(String minSlotFareFlightDeptTime) {
        this.minSlotFareFlightDeptTime = minSlotFareFlightDeptTime;
    }

    public String getMinSlotFareFlightDeptTime() {
        return minSlotFareFlightDeptTime;
    }

    public String getMinFareOnEighthDayBeforeDepart() {
        return minFareOnEighthDayBeforeDepart;
    }

    public void setMinFareOnEighthDayBeforeDepart(String minFareOnEighthDayBeforeDepart) {
        this.minFareOnEighthDayBeforeDepart = minFareOnEighthDayBeforeDepart;
    }

}
