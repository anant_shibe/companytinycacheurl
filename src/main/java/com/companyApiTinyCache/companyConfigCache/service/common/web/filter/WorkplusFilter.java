package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import com.cleartrip.clearhttp.api.HttpApi;
import com.cleartrip.clearhttp.api.HttpClient;
import com.cleartrip.clearhttp.api.HttpResponse;
import com.cleartrip.clearhttp.core.exception.HasHttpResponse;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CommonConstants;
import com.companyApiTinyCache.companyConfigCache.service.common.service.AuthenticationService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.WebApplicationContextUtils;
import scala.util.Try;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class WorkplusFilter implements Filter {

    private static final String CT_AIR_WP_SERVICE_CONCURRENCY = "ct.air.wp.service.concurrency";
    private static final String CT_AIR_WP_SERVICE_TIMEOUT = "ct.air.wp.service.timeout";
    private static final String WORKPLUS_USER_END_POINT = "ct.air.wp.service.user-state.endpoint";
    private static final String CT_AIR_WP_SERVICE_BASE_URL = "ct.air.wp.service.base-url";//workplus.cltp.com:9001/user/state
    private static final String MOBILE_APP = "mobileApp";
    private static final String CLEARTRIP = "cleartrip.com";
    private static final String ME_DOMAINS = "bh.cleartrip.com, kw.cleartrip.com, qa.cleartrip.com, om.cleartrip.com, me.cleartrip.com";
    private static final String AUTH_COOKIE_NAME = "ct-auth";
    private static final String SIGNED_IN_NON_GST = "SIGNED_IN_NON_GST";
    private static final String SIGNED_IN = "SIGNED_IN";
    private static final String UNSIGNED_NON_GST = "UNSIGNED_NON_GST";
    private static final String WORKPLUS_DOMAINS = "ct.air.workplus.service.enabled.domains";
    private static final String CT_FREQ_FLYER_VIRTUAL_COMPANY_ID = "ct.freq-flyer.virtual.company-id";
    private static final String DEMO_CLTP_COMPANY_ID = "101";
    private static final String WORKPLUS_ENABLED = "ct.air.services.workplus.enabled";
    private static final String SELLING_COUNTRY = "sct";
    private static final String CT_AIR_WP_USERS = "ct.air.wp.users";
    private static final String UTM_SOURCE = "utm_source";
    private static final String WORKPLUS = "WORKPLUS";
    private static final String TRIAL = "TRIAL";
    private static final String MOBILE_WP_ENABLED_PARAM = "mobileWPEnabled";


    private static final Log logger = LogFactory.getLog(WorkplusFilter.class);
    private FilterConfig filterConfig;
    private CachedProperties cachedProperties;
    private AuthenticationService authenticationService;
    private HttpClient httpClient;
    private HttpApi httpWorkPlusUserApi;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        ApplicationContext ctx = this.getContext(filterConfig);

        Object beanObj;
        beanObj = ctx.getBean("commonCachedProperties");
        if (beanObj instanceof CachedProperties) {
            this.cachedProperties = (CachedProperties) beanObj;
        }

        beanObj = ctx.getBean("authenticationServiceBean");
        if (beanObj instanceof AuthenticationService) {
            this.authenticationService = (AuthenticationService) beanObj;
        }

        beanObj = ctx.getBean("clearHttpClient");
        if (beanObj instanceof HttpClient) {
            this.httpClient = (HttpClient) beanObj;
            try {
                this.httpWorkPlusUserApi = getHttpWorkPlusUserApi();
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            boolean isWorkplusEnabled = cachedProperties.getBooleanPropertyValue(WORKPLUS_ENABLED, false);
            if (isWorkplusEnabled) {
                setWorkPlusParams(request, response);
            }
        } catch (Exception e) {
            logger.error("Exception raised when filtering for workplus");
        } finally {
            chain.doFilter(request, response);
        }
    }


    private void setWorkPlusParams(ServletRequest request, ServletResponse response) {
        boolean isIntlSearch = isIntlSearchRequest(request);
        HttpServletRequest req = (HttpServletRequest) request;
        boolean isMobileApp = isRequestFromMobileApp(request);
        boolean isDomainEnabled = isWorkplusEnabled(req,isMobileApp);
        Optional<Cookie> authCookie = Optional.ofNullable(GenUtil.getCookieFromRequest(req, "ct-auth"));
        String wpUsers = cachedProperties.getPropertyValue(CT_AIR_WP_USERS, "@cleartrip.com");
        boolean isUserEligibleForWP = wpUsers.equalsIgnoreCase("*");

        if (isMobileApp) {
            authCookie = getAuthCookieForMobile(authCookie, request);
        }

        if(req.getParameter(UTM_SOURCE)!=null && WORKPLUS.equals(req.getParameter(UTM_SOURCE))){
            request.setAttribute("wpStatus", TRIAL);
            setCorpFareParams(req);
            return;
        }

        Optional<String> loggedInMailId = getUserEmailIDFromAuthCookie(authCookie) ;
        if(loggedInMailId.isPresent()) {
            for (String wpUser : wpUsers.split(",")) {
                if ((loggedInMailId.get().contains(wpUser))) {
                    isUserEligibleForWP = true;
                    logger.error("User "+loggedInMailId.get()+" allowed to access Cleartrip for Work");
                }
            }
        }
        if(!isUserEligibleForWP){
            return;
        }
        if (isIntlSearch || !isDomainEnabled) {
            return;
        }

        if (!authCookie.isPresent()) {
            request.setAttribute("wpStatus", UNSIGNED_NON_GST);
            return;
        }
        Optional<Integer> loggedInUserId = getUserIdFromAuthCookie(authCookie.get());
        if (!loggedInUserId.isPresent()) {
            request.setAttribute("wpStatus", UNSIGNED_NON_GST);
            return;
        }

        try {
            Try<HttpResponse> httpResponseTry = httpWorkPlusUserApi.execute(ImmutableMap.of("userId", loggedInUserId.get()));
            if (httpResponseTry.isSuccess()) {
                String resp = new String(httpResponseTry.get().content());
                if (StringUtils.isEmpty(resp)) {
                    request.setAttribute("wpStatus", SIGNED_IN_NON_GST);
                } else {
                    request.setAttribute("wpStatus", resp);
                    logger.error("Workplus state fetched from app for userId " +loggedInUserId.get() + " is " +resp);
                    if (!resp.equalsIgnoreCase(SIGNED_IN)) {
                        setCorpFareParams(req);
                    }
                }
            } else if (httpResponseTry.isFailure()) {
                logger.error("Workplus state fetched from app is a failue setting to SIGNED_IN_NON_GST for userId "+loggedInUserId.get());
                request.setAttribute("wpStatus", SIGNED_IN_NON_GST);
                handleRestCallException(httpResponseTry.failed().get());
            }
        }
        catch(Exception ex){
             logger.error("Logging: Failed to fetch user state from workplus "+ex);
        }

    }

    private void handleRestCallException(Throwable throwable) {
        try {
            if (HasHttpResponse.class.isInstance(throwable)) {
                HasHttpResponse hasHttpResponse = HasHttpResponse.class.cast(throwable);
                HttpResponse response = hasHttpResponse.response();
                logger.error("Error while getting workplus state with Response code: " + response.status());
            } else {
                logger.error("Error while getting workplus state" + throwable);
            }
        } catch (Exception e) {
            logger.error("Error while getting workplus state" + e);
        }
    }

    private void setCorpFareParams(ServletRequest request) {
        request.setAttribute("showCorpFareForUser", "true");
        String companyId = cachedProperties.getPropertyValue(CT_FREQ_FLYER_VIRTUAL_COMPANY_ID, DEMO_CLTP_COMPANY_ID);
        request.setAttribute("companyid", companyId);
        request.setAttribute("enableCorpFare", "true");
    }

    private boolean isIntlSearchRequest(ServletRequest request) {
        String inltSearch = org.apache.commons.lang.StringUtils.trimToEmpty(request.getParameter("intl"));
        return inltSearch.equalsIgnoreCase("true") || inltSearch.equalsIgnoreCase("y");
    }

    private Optional<Cookie> getAuthCookieForMobile(Optional<Cookie> authCookie, ServletRequest request) {
        String r_cookie = org.apache.commons.lang.StringUtils.trimToEmpty(((HttpServletRequest) request).getHeader("r_cookie"));
        if (r_cookie.contains("ct-auth=")) {
            String cookieValue = r_cookie.substring(r_cookie.indexOf("ct-auth=") + 8);
            cookieValue = cookieValue.substring(0, cookieValue.indexOf(";"));
            authCookie = Optional.of(new Cookie(AUTH_COOKIE_NAME, cookieValue));
            authCookie.get().setDomain(CLEARTRIP);
        }
        return authCookie;
    }

    private boolean isWorkplusEnabled(HttpServletRequest req, boolean isMobileRequest) {
        try{
            String sellingCountry = req.getParameter(SELLING_COUNTRY);
            if (org.apache.commons.lang.StringUtils.isBlank(sellingCountry)) {
                sellingCountry = (String) req.getAttribute(CommonConstants.COUNTRY);

                if (org.apache.commons.lang.StringUtils.isBlank(sellingCountry)) {
                    sellingCountry = "IN";
                }
            }

            String enabledDomains= cachedProperties.getPropertyValue(WORKPLUS_DOMAINS,"");
            ObjectMapper mapper = JsonUtil.getObjectMapper();
            Map<String,Object> enabledMap = (Map<String,Object>)mapper.readValue(enabledDomains,Map.class);
            if(enabledMap.containsKey(sellingCountry)){
                Map<String, Boolean> enabledChannels = (Map<String,Boolean>)enabledMap.get(sellingCountry);
                if(isMobileRequest){
                    boolean isMobileWPEnabled = false;
                    if(req.getParameter(MOBILE_WP_ENABLED_PARAM)!=null){
                        isMobileWPEnabled = Boolean.valueOf(req.getParameter(MOBILE_WP_ENABLED_PARAM));
                    }
                    return isMobileWPEnabled && enabledChannels.get("MOBILE");
                }else{
                    return enabledChannels.get("B2C");
                }
            }
        }catch (Exception e){
            logger.error("Error while checking for domain on workplus filter");
        }

        return false;
    }


    private boolean isRequestFromMobileApp(ServletRequest request) {
        String source = org.apache.commons.lang.StringUtils.trimToEmpty(request.getParameter("source"));
        String host = org.apache.commons.lang.StringUtils.trimToEmpty(((HttpServletRequest) request).getHeader("r_host"));
        if (host.contains(CLEARTRIP) && !isMEDomain(host) && host.equalsIgnoreCase("www.cleartrip.com")
                && source.equalsIgnoreCase(CommonEnumConstants.SourceType.MOBILE.name())
                && Boolean.valueOf(request.getParameter(MOBILE_APP))) {
            return true;
        }
        return false;
    }

    private boolean isMEDomain(String domain) {
        return domain != null && !domain.isEmpty() &&
                ME_DOMAINS.contains(domain);
    }

    @Override
    public void destroy() {

    }

    /**
     * Allows test cases to override where application context obtained from.
     *
     * @param filterConfig which can be used to find the <code>ServletContext</code>
     * @return the Spring application context
     */
    protected ApplicationContext getContext(FilterConfig filterConfig) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
    }

    private HttpApi getHttpApi(URL httpURL, String methodType, String endPoint) {
        int concurrency = cachedProperties.getIntPropertyValue(CT_AIR_WP_SERVICE_CONCURRENCY, 20);
        int timeout = cachedProperties.getIntPropertyValue(CT_AIR_WP_SERVICE_TIMEOUT, 500);
        return httpClient.apiSpecBuilder(httpURL.getHost(), httpURL.getHost(),
                httpURL.getPort(), concurrency, timeout).build().apiBuilder(methodType, endPoint)
                .withHeader("content-type", "application/json").build();
    }

    private HttpApi getHttpWorkPlusUserApi() throws MalformedURLException {
        String endpoint = cachedProperties.getPropertyValue(WORKPLUS_USER_END_POINT, "/workplus/user/state");
        return getHttpApi(getWorkplusServiceBaseUrl(), HttpMethod.GET.name(), endpoint);
    }

    private URL getWorkplusServiceBaseUrl() throws MalformedURLException {
        return new URL(cachedProperties.getPropertyValue(CT_AIR_WP_SERVICE_BASE_URL, "http://workplus.cltp.com:9001"));
    }

    private Optional<Integer> getUserIdFromAuthCookie(Cookie authCookie) {
        try {
            HashMap<String, String> map = authenticationService.getPersonFromCookie(authCookie);
            return Optional.of(Integer.parseInt(String.valueOf(map.get(CommonEnumConstants.UserDetails.USER_ID.toString()))));
        } catch (Exception e) {
//            logger.info("Could-not get user-id from auth cookie");
        }
        return Optional.empty();
    }

    private Optional<String> getUserEmailIDFromAuthCookie(Optional<Cookie> authCookie) {
        try {
            if (authCookie.isPresent()) {
                HashMap<String, String> map = authenticationService.getPersonFromCookie(authCookie.get());
                return Optional.of(map.get(CommonEnumConstants.UserDetails.USER_NAME.toString()));
            }
        } catch (Exception e) {
            logger.info("Could not get email id from cookie");
        }
        return Optional.empty();
    }
}
