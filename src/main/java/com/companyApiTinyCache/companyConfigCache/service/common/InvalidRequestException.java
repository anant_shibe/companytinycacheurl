package com.companyApiTinyCache.companyConfigCache.service.common;

/**
 * Exception thrown to indicate an invalid request.
 * 
 * @author Amlan Roy
 */
public class InvalidRequestException extends Exception {

    private static final long serialVersionUID = -7045980965029611323L;

    public InvalidRequestException(String message) {
        super(message);
    }
}
