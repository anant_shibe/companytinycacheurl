package com.companyApiTinyCache.companyConfigCache.service.common.webservice;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.service.JSONSizeCheckerService;
import com.companyApiTinyCache.companyConfigCache.service.common.util.APIUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

@ClassExcludeCoverage
public class JSONSizeCheckerWebService extends AbstractController {
	private JSONSizeCheckerService jsonSizeCheckerService;

	private static final Logger LOGGER = Logger.getLogger(JSONSizeCheckerWebService.class.getName());

	public JSONSizeCheckerWebService(JSONSizeCheckerService jsonSizeCheckerService) {
		this.jsonSizeCheckerService = jsonSizeCheckerService;
	}

	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) {
		boolean debug = StringUtils.isNotBlank(request.getParameter("debug"));

		response.setContentType("text/plain");
		PrintWriter op = null;

		try {
			op = response.getWriter();
		} catch (IOException e1) {
			LOGGER.error("Printwriter object error");
		}

		Map<String, Map<String, Double>> completeList = jsonSizeCheckerService.createCompleteList();

		String respon = "";

		if (debug) {
			if (completeList == null)
				respon = "The list is null, please check the value of maxSize (should be greater than/ equal to 0).";
			else {
				if (completeList.isEmpty())
					respon = "There are no endpoints which have a response greater than the specified size";
				else
					respon = APIUtil.serializeObjectToJson(completeList);
			}
		}
		op.println(respon);
		return null;
	}
}
