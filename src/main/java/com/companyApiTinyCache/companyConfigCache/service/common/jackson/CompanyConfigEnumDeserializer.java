package com.companyApiTinyCache.companyConfigCache.service.common.jackson;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.*;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CompanyConfig;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * Custom deserializer added for
 * 
 * @author sanjeev
 * 
 */
public class CompanyConfigEnumDeserializer extends JsonDeserializer<Enum<?>> {

    /**
     * If you don't find matching CompanyConfig enum, just assign itto UNKNOWN.
     * 
     */
    @MethodExcludeCoverage
    public Enum<?> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonToken curr = jp.getCurrentToken();
        CompanyConfig c1 = CompanyConfig.UNKNOWN;
        if (curr == JsonToken.VALUE_STRING) {
            String name = jp.getText();
            try {
                c1 = CompanyConfig.valueOf(name);
            } catch (Exception e) {
                ;
            }
            if (c1 == null) {
                return CompanyConfig.UNKNOWN;
            }
        }
        return c1;
    }
}
