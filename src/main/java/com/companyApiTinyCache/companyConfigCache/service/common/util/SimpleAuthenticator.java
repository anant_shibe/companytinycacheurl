/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * Utility class for setting proxy authentication
 * 
 * @author Ramesh Krishnamoorthy
 * 
 */
public class SimpleAuthenticator extends Authenticator {
    private String username, password;

    public SimpleAuthenticator(String username, String password) {
        this.username = username;
        this.password = password;
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password.toCharArray());
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
