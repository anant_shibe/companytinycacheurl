package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.ContactDetail;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
*
* @author amith
*
*/
public class ContactDetailHolder {

    @JsonProperty("contact_data")
    private ContactDetail contactDetail;

    /**
     * @return the contactDetail
     */
    @JsonProperty("contact_data")
    public ContactDetail getContactDetail() {
        return contactDetail;
    }

    /**
     * @param contactDetail
     *            the contactDetail to set
     */
    @JsonProperty("contact_data")
    public void setContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
    }
}
