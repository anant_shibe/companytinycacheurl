package com.companyApiTinyCache.companyConfigCache.service.common.util;

import java.util.*;

/**
 * Wrapper Map over a list of maps. For get operations the first match from index 0 is returned. Put
 * and remove operations always go to index 0 which means maps and index > 0 are not modified. The number
 * of Maps gets fixed during constructor time but individual maps can be changed. This was created for Hotel
 * Rate Rules for generating cityAttributes and Hotel Attributes separately without having to put all attributes
 * in one big attribute map. The map at index 0 is assumed to be not null.
 * @author suresh
 *
 * @param <K> Key Type
 * @param <V> Value Type
 */
public class MapList<K, V> implements Map<K, V> {

    private Map<K, V> [] maps;

    public MapList(Map<K, V> map, Map<K, V>...moreMaps) {
        maps = new Map[moreMaps.length + 1];
        maps[0] = map;
        for (int i = 1; i < maps.length; i++) {
            maps[i] = moreMaps[i - 1];
        }
    }

    public int getNumMaps() {
        return maps.length;
    }

    public void setMapAt(int index, Map<K, V> map) {
        maps[index] = map;
    }

    public Map<K, V> getMapAt(int index) {
        return maps[index];
    }

    @Override
    public int size() {
        int i, len = maps[0].size();
        for (i = 1; i < maps.length; i++) {
            Map<K, V> m = maps[i];
            if (m != null) {
                len += m.size();
            }
        }

        return len;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        boolean contains = maps[0].containsKey(key);
        int i = 1;
        while (!contains && i < maps.length) {
            Map<K, V> m = maps[i];
            if (m != null) {
                contains = m.containsKey(key);
            }
            i++;
        }
        return contains;
    }

    @Override
    public boolean containsValue(Object value) {
        boolean contains = maps[0].containsValue(value);
        int i = 1;
        while (!contains && i < maps.length) {
            Map<K, V> m = maps[i];
            if (m != null) {
                contains = m.containsValue(value);
            }
            i++;
        }
        return contains;
    }

    @Override
    public V get(Object key) {
        V value = null;
        boolean gotVal = false;
        int i = 0;
        do {
            Map<K, V> m = maps[i];
            if (i == 0 || m != null) {
                value = m.get(key);
                gotVal = true;
                if (value == null) {
                    gotVal = m.containsKey(key);
                }
            }
            i++;
        } while (!gotVal && i < maps.length);
        return value;
    }

    // Put implementation is not perfect since it does not return the previous value present in other maps
    @Override
    public V put(K key, V value) {
        return maps[0].put(key, value);
    }

    // Remove just removes the key from 1st Map. So it will not remove values in other maps or return it if
    // 1st map does not have the key
    @Override
    public V remove(Object key) {
        return maps[0].remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        maps[0].putAll(m);
    }

    // Only 1st map is cleared
    @Override
    public void clear() {
        maps[0].clear();
    }

    // The returned set is a new set and not backed by Collection entries according to API contract
    @Override
    public Set<K> keySet() {
        Set<K> s = new HashSet<K>();
        s.addAll(maps[0].keySet());
        for (int i = 1; i < maps.length; i++) {
            Map<K, V> m = maps[i];
            if (m != null) {
                s.addAll(m.keySet());
            }
        }
        return s;
    }

    // Suitable only for Read only use
    @Override
    public Collection<V> values() {
        List<V> vals = new ArrayList<V>();
        int i = 0;
        do {
            Map<K, V> m = maps[i];
            if (i == 0 || m != null) {
                Set<Entry<K, V>> entries = m.entrySet();
                for (Entry<K, V> entry : entries) {
                    V val = entry.getValue();
                    if (val == get(entry.getKey())) {
                        vals.add(val);
                    }
                }
            }
            i++;
        } while (i < maps.length);
        return vals;
    }

    // Suitable only for Read only use
    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> s = new HashSet<Entry<K, V>>();
        int i = 0;
        do {
            Map<K, V> m = maps[i];
            if (i == 0 || m != null) {
                Set<Entry<K, V>> entries = m.entrySet();
                for (Entry<K, V> entry : entries) {
                    if (entry.getValue() == get(entry.getKey())) {
                        s.add(entry);
                    }
                }
            }
            i++;
        } while (i < maps.length);
        return s;
    }
}
