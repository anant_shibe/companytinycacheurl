package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.connector.UserAPIConnector;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CompanyDetail;
import com.companyApiTinyCache.companyConfigCache.service.common.data.UserProfile;
import com.companyApiTinyCache.companyConfigCache.service.common.security.CTSecurityHeaders;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.SourceType;

import javax.servlet.http.HttpServletRequest;

/**
 * Introduced the class to add some service level methods for common package code.
 * @author Sanjeev
 * @version 1.0
 */
public class CommonMiscServiceImpl implements CommonMiscService {

    private UserAPIConnector userAPIConnector;

    private CachedProperties cachedProperties;

    @Override
    public void subscribeToNewsLetters(UserProfile userProfile, boolean enableSubscription) {

        int companyId = cachedProperties.getIntPropertyValue("ct.cleartrip.company.id", 110340);

        CompanyDetail companyDetails = userProfile.getCompanyDetails().get(companyId);

        if (companyDetails != null) {
            if (enableSubscription) {
                companyDetails.setMarketingSubscription(1);
            } else {
                companyDetails.setMarketingSubscription(0);
            }

            userAPIConnector.updateUserProfile(userProfile, SourceType.B2C.toString(), "B2C_UPDATEUSERPROFILE");
        }

    }

    /**
     * Method to enable or disable the news letter subscription for given people id.
     */
    /* (non-Javadoc)
     * @see com.cleartrip.common.service.CommonMiscService#subscribeToNewsLetters(int, boolean)
     */
    @Override
    public void subscribeToNewsLetters(int peopleId, boolean enableSubscription) throws Exception {

        CTSecurityHeaders ctSecurityHeaders = new CTSecurityHeaders(null, null);

        UserProfile userProfile = userAPIConnector.getUserProfile(peopleId, SourceType.B2C.toString(), "B2C_GETUSERPROFILE", ctSecurityHeaders);

        if (userProfile != null) {

            subscribeToNewsLetters(userProfile, enableSubscription);

        } else {

            throw new Exception("User profile null for people id: " + peopleId);
        }

    }

    /**
     * Method to enable or disable the news letter subscription for given people id.
     */
    /* (non-Javadoc)
     * @see com.cleartrip.common.service.CommonMiscService#subscribeToNewsLetters(java.lang.String, boolean)
     */
    @Override
    public void subscribeToNewsLetters(String emailAddress, boolean enableSubscription, HttpServletRequest request) throws Exception {

        CTSecurityHeaders ctSecurityHeader = new CTSecurityHeaders(request, SourceType.B2C);
        UserProfile userProfile = userAPIConnector.getUserProfileFromUserName(emailAddress, SourceType.B2C.toString(), "B2C_GETUSERPROFILE", ctSecurityHeader);

        if (userProfile != null) {

            subscribeToNewsLetters(userProfile, enableSubscription);

        } else {

            throw new Exception("User profile null for user name: " + emailAddress);
        }

    }

    public void setUserAPIConnector(UserAPIConnector userAPIConnector) {
        this.userAPIConnector = userAPIConnector;
    }

    public void setCachedProperties(CachedProperties cachedProperties) {
        this.cachedProperties = cachedProperties;
    }

}
