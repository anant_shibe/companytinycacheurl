package com.companyApiTinyCache.companyConfigCache.service.common.util.autocomplete;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.MethodExcludeCoverage;

import java.lang.reflect.Array;
import java.util.NoSuchElementException;

/**
 * Utility to merge multiple Tries and iterate over them giving a sorted result across all Tries. Assumes that each Trie gives sorted results. Uses standard algo to merge sorted lists.
 * 
 * @author suresh
 * 
 * @param <E>
 */
public class MergedTrieIterator<E> implements TrieIterator<E> {

    private TrieIterator<E> its[];

    private E entities[][];

    private int ofs;

    private int currentItIndex;

    private int numActiveIts;

    public MergedTrieIterator(String prefix, ReadonlyTrie<E> tries[], int pofs, int len) {
        currentItIndex = -1;
        int i;
        its = new TrieIterator[len];
        ofs = pofs;
        for (i = 0; i < len; i++) {
            TrieIterator<E> it = tries[i + pofs].iterator(prefix);
            if (it.hasNext()) {
                numActiveIts++;
                E[] entityArr = it.next();
                if (entities == null) {
                    entities = (E[][]) Array.newInstance(entityArr.getClass(), len);
                }
                entities[i] = entityArr;
                its[i] = it;
            }
        }
    }

    public MergedTrieIterator(String prefix, ReadonlyTrie<E>... tries) {
        this(prefix, tries, 0, tries.length);
    }

    @Override
    public boolean hasNext() {
        return numActiveIts > 0;
    }

    @Override
    public E[] next() {
        if (numActiveIts <= 0) {
            throw new NoSuchElementException("No Next Element");
        }
        TrieIterator<E> it;
        int itIndex = currentItIndex, i;
        if (itIndex >= 0) {
            it = its[itIndex];
            if (it.hasNext()) {
                entities[itIndex] = it.next();
            } else {
                its[itIndex] = null;
                entities[itIndex] = null;
            }
        }
        String minKey = null;
        itIndex = -1;
        for (i = 0; i < its.length; i++) {
            it = its[i];
            if (it != null) {
                String checkKey = it.getMatchedKey();
                if (minKey == null || checkKey.compareTo(minKey) < 0) {
                    minKey = checkKey;
                    itIndex = i;
                }
            }
        }
        if (!its[itIndex].hasNext()) {
            numActiveIts--;
        }
        currentItIndex = itIndex;

        return entities[itIndex];
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Iterator is Read only");
    }

    @Override
    public boolean isMatchesPrefix() {
        boolean matches = false;
        if (currentItIndex >= 0) {
            matches = its[currentItIndex].isMatchesPrefix();
        }
        return matches;
    }

    @Override
    public String getMatchedKey() {
        if (currentItIndex < 0) {
            throw new IllegalStateException("Cannot call getMatchedKey without calling next()");
        }

        return its[currentItIndex].getMatchedKey();
    }

    public int getCurrentItIndex() {
        return currentItIndex + ofs;
    }

    @Override
    @MethodExcludeCoverage
    public int getPrevCommonPrefixLen() {
        return 0;
    }

    @Override
    @MethodExcludeCoverage
    public int getNextCommonPrefixLen() {
        return 0;
    }

    @Override
    @MethodExcludeCoverage
    public int getNumEntities(int prefixLen) {
        return 0;
    }

    @Override
    public int getNumEntities() {
        if (currentItIndex < 0) {
            throw new IllegalStateException("Cannot call getNumEntities without calling next()");
        }

        return its[currentItIndex].getNumEntities();
    }

}
