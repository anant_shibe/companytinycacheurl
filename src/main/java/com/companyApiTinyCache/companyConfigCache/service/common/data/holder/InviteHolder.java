package com.companyApiTinyCache.companyConfigCache.service.common.data.holder;

import com.companyApiTinyCache.companyConfigCache.service.common.data.Invite;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
*
* @author amith
*
*/
public class InviteHolder extends AffiliateImportHolder {

    @JsonProperty("company_people_invite")
    private Invite invite;

    @JsonProperty("company_people_invite")
    public Invite getInvite() {
        return invite;
    }

    @JsonProperty("company_people_invite")
    public void setInvite(Invite invite) {
        this.invite = invite;
    }

}
