package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

public class OtherDetail implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private int sequenceNumber;

    private Integer id;

    private String category;

    private String name;

    private String value;

    @JsonIgnore
    private boolean profiledUpdated;

    @JsonIgnore
    public boolean isProfiledUpdated() {
        return profiledUpdated;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        if (StringUtils.isNotEmpty(name))
            profiledUpdated = true;
        this.name = name;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        if (StringUtils.isNotEmpty(value))
            profiledUpdated = true;
        this.value = value;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        if (StringUtils.isNotEmpty(category))
            profiledUpdated = true;
        this.category = category;
    }

    @JsonProperty("seq_no")
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    @JsonProperty("seq_no")
    public void setSequenceNumber(int sequenceNumber) {
        if (sequenceNumber > 0)
            profiledUpdated = true;
        this.sequenceNumber = sequenceNumber;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        if (sequenceNumber > 0)
            profiledUpdated = true;
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof OtherDetail == false)
            return false;
        OtherDetail od = (OtherDetail) obj;
        if (id != od.getId()) {
            return false;
        }
        if ((category != null || od.getCategory() != null) && !GenUtil.equals(category, od.getCategory())) {
            return false;
        }
        if ((name != null || od.getName() != null) && !GenUtil.equals(name, od.getName())) {
            return false;
        }
        if ((value != null || od.getValue() != null) && !GenUtil.equals(value, od.getValue())) {
            return false;
        }

        return true;
    }

}
