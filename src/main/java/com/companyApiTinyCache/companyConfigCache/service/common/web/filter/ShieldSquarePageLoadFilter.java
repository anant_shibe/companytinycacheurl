package com.companyApiTinyCache.companyConfigCache.service.common.web.filter;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;

import javax.servlet.ServletRequest;

/**
 * ShieldSquarePageLoadFilter.
 * @author amith
 */
@ClassExcludeCoverage
public class ShieldSquarePageLoadFilter extends ShieldSquareAbstractFilter {

    @Override
    public SSCALLTYPE getCallType() {
        return SSCALLTYPE.PAGE_LOAD;
    }

    @Override
    public boolean callShieldSquare(ServletRequest request, CachedProperties commonCachedProperties) {
        // TODO Auto-generated method stub
        return commonCachedProperties.getBooleanPropertyValue("ct.hotel.shieldsquare.enabled", false);
    }
}
