package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class WebSite implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private int sequenceNumber;

    private String category;

    private String url;

    @JsonProperty("seq_no")
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    @JsonProperty("seq_no")
    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof WebSite == false)
            return false;
        WebSite ws = (WebSite) obj;
        if ((category != null || ws.getCategory() != null) && !GenUtil.equals(category, ws.getCategory())) {
            return false;
        }
        if ((url != null || ws.getUrl() != null) && !GenUtil.equals(url, ws.getUrl())) {
            return false;
        }

        return true;
    }

}
