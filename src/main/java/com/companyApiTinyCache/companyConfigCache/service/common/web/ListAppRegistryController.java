package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.data.*;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.*;

/**
 * Controller for app_registry URL/json service to list tomcat servers in an environment.
 *
 * @author suresh
 */
public class ListAppRegistryController extends AbstractController {

    private static final Log logger = LogFactory.getLog(ListAppRegistryController.class);

    private ThisServer thisServer;

    private CachedProperties commonCachedProperties;

    private boolean isJson;

    public ListAppRegistryController() {
        setCacheSeconds(0);
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<ServerInfoBasic> basicInfos = null;
        List<ServerInfoCallers> callerInfos = null;
        List<ServerInfoConfig> configInfos = null;
        String beanName = StringUtils.trimToNull(request.getParameter("beanName"));
        boolean listActive = GenUtil.getBooleanParameter(request, "active", true);
        boolean showCallers = GenUtil.getBooleanParameter(request, "sc", false);
        ServerInfo serverInfo = thisServer.getServerInfo();
        List<Object>[] serverDetails = null;
        if (beanName != null) {
            serverDetails = thisServer.getServerInfos(listActive, serverInfo.getBasicInfo().getName(), serverInfo.getCallerInfo().getName(), serverInfo.getConfigInfo().getName());
        } else {
            serverDetails = thisServer.getServerInfos(listActive, serverInfo.getBasicInfo().getName(), serverInfo.getCallerInfo().getName());
        }
        if (serverDetails != null) {
            basicInfos = (List) serverDetails[0];
            callerInfos = (List) serverDetails[1];
            if (serverDetails.length > 2) {
                configInfos = (List) serverDetails[2];
            }
        }

        if (basicInfos == null) {
            basicInfos = new ArrayList<ServerInfoBasic>(0);
        }

        if (callerInfos == null) {
            callerInfos = new ArrayList<ServerInfoCallers>(0);
        }

        int numCallerInfos = callerInfos.size();

        int numConfigInfos = 0;

        if (configInfos != null) {
            numConfigInfos = configInfos.size();
        }

        String appsStr = StringUtils.trimToNull(request.getParameter("apps"));
        Set<String> apps = null;
        if (appsStr != null) {
            apps = new HashSet<String>();
            StringTokenizer tokens = new StringTokenizer(appsStr, ",");
            while (tokens.hasMoreTokens()) {
                String app = StringUtils.trimToNull(tokens.nextToken());
                apps.add(app);
            }
        }
        int i, pos, len = basicInfos.size();
        long now = System.currentTimeMillis();
        long shortExpiryTime = thisServer.getShortExpiryTime(now);
        for (i = len; i > 0;) {
            i--;
            boolean remove = false;
            ServerInfoBasic currentServerInfo = basicInfos.get(i);
            if (apps != null) {
                remove = true;
                List<String> currentApps = currentServerInfo.getApps();
                if (currentApps != null) {
                    for (String app : currentApps) {
                        if (apps.contains(app)) {
                            remove = false;
                            break;
                        }
                    }
                }
            }
            if (beanName != null && i < numConfigInfos) {
                ServerInfoConfig configInfo = configInfos.get(i);
                if (configInfo != null) {
                    List<CachedResourceInfo> cachedResources = configInfo.getCachedResources();
                    if (cachedResources != null) {
                        remove = true;
                        String hostName = currentServerInfo.getHost();
                        if (hostName == null || hostName.indexOf("ct-admin") < 0) {
                            for (CachedResourceInfo cachedResource : cachedResources) {
                                if (beanName.equals(cachedResource.getBeanName())) {
                                    remove = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (remove) {
                basicInfos.remove(i);
                len--;
            }
        }
        ModelAndView modelAndView = null;
        if (!isJson) {
            modelAndView = new ModelAndView("listAppRegistry");
            modelAndView.addObject("basicInfos", basicInfos);
            String env = commonCachedProperties.getPropertyValue("ct.common.runtime.env", "(unknown env)");
            modelAndView.addObject("env", env);
            long inactiveTime = now - shortExpiryTime;
            modelAndView.addObject("inactiveTime", inactiveTime);
            modelAndView.addObject("active", listActive);
            modelAndView.addObject("sc", showCallers);
            List<Map<String, List<String>>> callerSummaries = new ArrayList<Map<String, List<String>>>();
            if (basicInfos != null) {
                StringBuilder sb = new StringBuilder();
                len = basicInfos.size();
                for (i = 0; i < len; i++) {
                    ServerInfoBasic basicInfo = basicInfos.get(i);
                    sb.setLength(0);
                    String host1 = sb.append(basicInfo.getHost()).append(':').append(basicInfo.getPort()).toString();
                    sb.setLength(0);
                    String host2 = sb.append(basicInfo.getHost()).append(".cleartrip.com").append(':').append(basicInfo.getPort()).toString();
                    Map<String, List<String>> callerSummary = null;
                    List<CallerInfo> callers = null;
                    if (i < numCallerInfos) {
                        callers = callerInfos.get(i).getCallers();
                    }
                    if (callers != null && callers.size() > 0) {
                        callerSummary = new HashMap<String, List<String>>();
                        for (CallerInfo callerInfo : callers) {
                            String endUrl = callerInfo.getEnd_url();
                            String sourceUrl = callerInfo.getSource_url();
                            String sourceHost = null;
                            String sourcePath = null;
                            if (sourceUrl != null) {
                                pos = sourceUrl.indexOf('/');
                                if (pos < 0) {
                                    sourceUrl = sourceUrl + "/NO_PATH";
                                }
                                pos = sourceUrl.indexOf('/');
                                sourceHost = sourceUrl.substring(0, pos);
                                sourcePath = sourceUrl.substring(pos);
                            }
                            String key = endUrl;
                            if (sourceUrl != null && !host1.equals(sourceHost) && !host2.equals(sourceHost)) {
                                sb.setLength(0);
                                sb.append(endUrl);
                                String via = sourcePath.equals(endUrl) ? sourceHost : sourceUrl;
                                sb.append(" (Via: ").append(via).append(')');
                                key = sb.toString();
                            }
                            sb.setLength(0);
                            String invokerInfo = sb.append(callerInfo.getHost()).append(':').append(callerInfo.getPort()).append('/').append(callerInfo.getApp()).toString();
                            List<String> invokerInfos = callerSummary.get(key);
                            if (invokerInfos == null) {
                                invokerInfos = new ArrayList<String>();
                                callerSummary.put(key, invokerInfos);
                            }
                            invokerInfos.add(invokerInfo);
                        }
                    }
                    callerSummaries.add(callerSummary);
                }
                modelAndView.addObject("callers", callerSummaries);
            }
        } else {
            response.setContentType("text/json");
            PrintWriter writer = response.getWriter();
            try {
                ObjectMapper mapper = JsonUtil.getObjectMapper();
                JsonFactory jsonFactory = new JsonFactory();
                JsonGenerator jgen = jsonFactory.createGenerator(writer);
                jgen.useDefaultPrettyPrinter();
                mapper.writeValue(jgen, basicInfos);
            } catch (Exception e) {
                logger.error("Error Converting to Json for Server Registry", e);
            }
            writer.close();
        }

        return modelAndView;
    }

    public final void setThisServer(ThisServer pthisServer) {
        thisServer = pthisServer;
    }

    public final void setCommonCachedProperties(CachedProperties pcommonCachedProperties) {
        commonCachedProperties = pcommonCachedProperties;
    }

    public final void setJson(boolean pisJson) {
        isJson = pisJson;
    }

}
