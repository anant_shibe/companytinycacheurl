package com.companyApiTinyCache.companyConfigCache.service.common.cache;

import com.github.jedis.lock.JedisLock;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.Collection;
import java.util.List;

public interface JedisCache {

    String get(String key) throws CacheRetrieveException;

    void put(String id, String obj) throws CacheStoreException;

    void put(String id, String obj, int sec) throws CacheStoreException;

    String hget(String hash, String key);

    void hset(String hashName, String flightInfo, String dummy);

    List<String> hvals(String key);

    List<String> sort(String listKey);

    List<String> lrange(String key, long start, long end);

    void rpushx(String key, String value);

    Jedis getJedis();

    JedisLock getLock(String lockKey) throws InterruptedException;

    void release(JedisLock jedisLock) throws InterruptedException;

    Transaction multi();

    public void zadd(String key, double score, String member);

    Collection<String> zrangeByScore(String key, double min, double max);

    void zremrangeByScore(String key, double start, double end);

}
