package com.companyApiTinyCache.companyConfigCache.service.common.cache.impl;

import com.companyApiTinyCache.companyConfigCache.service.common.cache.*;
import com.companyApiTinyCache.companyConfigCache.service.common.io.ReusableByteArrayInputStream;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedPropertiesManager;
import com.companyApiTinyCache.companyConfigCache.service.common.util.ConfigLoader;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.*;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.util.Pool;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * An implementation of Cleartrip's Cache API using Jedis Redis client.
 *
 * Since Jedis does not have built-in compression and Object serialization we store binary data and handle
 * the encoding and decoding in the client.
 *
 * @author suresh
 */
public class JedisRedisCacheImpl extends CacheBase implements ConfigLoader<JedisRedisCacheImpl.Config> {

	class Config {
        private int connectAttempts;

        public Config(Map<String, String> cachedPropertiesResource) {
            connectAttempts = CachedProperties.getIntPropertyValue(cachedPropertiesResource, "ct.common.jedis.connect.attempts", 3);
        }
    }

    private static enum StreamType { STRING, GZIP, OBJECT };

    public static final int ESCAPE_MARKER = 0x8080;

    public static final int COMPRESSION_THRESHOLD = 1024;

    private static volatile byte [] addScriptBytes;

    private static final Log log = LogFactory.getLog(JedisRedisCacheImpl.class);

    private CachedProperties commonCachedProperties;

    private CachedPropertiesManager<Config> configManager;

    private Pool<Jedis> pool;
    
    public JedisRedisCacheImpl(String server, CachedProperties pcommonCachedProperties) {
        connectionString = server;
        commonCachedProperties = pcommonCachedProperties;
        configManager = new CachedPropertiesManager<Config>(commonCachedProperties, this);
        boolean isSentinel = false;
        String masterName = "";
        if (!"mock".equals(server)) {
            encodeKeyRequired = false;
            JedisPoolConfig poolConfig = new JedisPoolConfig();
            poolConfig.setMaxTotal(64);
            poolConfig.setMinIdle(5);
            poolConfig.setMaxIdle(5);
            poolConfig.setTestOnBorrow(false);
            poolConfig.setTestOnReturn(false);

            String host = server;
            int port = 6379;
            if (connectionString.contains("@")) {
            	isSentinel = true;
            	port = 26379;
            	String[] connectionDetails = server.split("@"); 
            	server = connectionDetails[1];
            	//connectionString = server;
            	masterName = connectionDetails[0];
            } 
            if (!isSentinel) {
            	int pos = server.indexOf(':');
                if (pos > 0) {
                    host = server.substring(0, pos);
                    port = Integer.parseInt(server.substring(pos + 1));
                } else {
                    connectionString = connectionString + ':' + port;
                }
            }

            int timeout = -1;
            if (commonCachedProperties != null) {
                timeout = commonCachedProperties.getIntPropertyValue("ct.jedis.timeout.millis", -1);
            }

            if (!isSentinel) {
            	if (timeout >= 0) {
            		pool = new JedisPool(poolConfig, host, port, timeout);
            		log.info("Starting JedisPool for " + host + ':' + port + " with timeout " + timeout);
            	} else {
            		pool = new JedisPool(poolConfig, host, port);
            		log.info("Starting JedisPool for " + host + ':' + port + " with Jedis default timeout (Should be 2000 ms)");
            	}
            } else {
            	String[] vals = server.split("\\|");
                HashSet<String> sentinels = new HashSet<String>();
                for (String sentinel : vals) {
                	sentinels.add(sentinel);
                }
            	if (timeout >= 0) {
            		pool = new JedisSentinelPool(masterName, sentinels, poolConfig, timeout);
            		log.info("Starting JedisSentinelPool for " + host + ':' + port + " with timeout " + timeout);
            	} else {
            		pool = new JedisSentinelPool(masterName, sentinels, poolConfig);
            		log.info("Starting JedisSentinelPool for " + host + ':' + port + " with Jedis default timeout (Should be 2000 ms)");
            	}
            }

        }
    }

    public JedisRedisCacheImpl(String server) {
        this(server, null);
    }

    public static byte [] serialize(Object data) throws IOException {
        byte [] b;
        if (data instanceof String) {
            String s = data.toString();
            int len;
            boolean writeEscapeMarker = false;
            byte [] strBytes = s.getBytes(GenUtil.UTF_8_CHARSET);
            len = strBytes.length;
            if (len > 1) {
                int firstTwoBytes = ((((int) strBytes[0]) << 8) | (strBytes[1] & 0xFF)) & 0xffff;
                if (firstTwoBytes == ESCAPE_MARKER) {
                    writeEscapeMarker = true;
                } else if (firstTwoBytes == GenUtil.GZIP_HEADER && len < COMPRESSION_THRESHOLD) {
                    writeEscapeMarker = true;
                }
            }
            if (len < COMPRESSION_THRESHOLD) {
                if (writeEscapeMarker) {
                    b = new byte[len + 2];
                    b[0] = (byte) (ESCAPE_MARKER >> 8);
                    b[1] = (byte) ESCAPE_MARKER;
                    System.arraycopy(strBytes, 0, b, 2, len);
                } else {
                    b = strBytes;
                }
            } else {
                ByteArrayOutputStream bop = new ByteArrayOutputStream(len / 4);
                GZIPOutputStream gzipOp = new GZIPOutputStream(bop);
                if (writeEscapeMarker) {
                    gzipOp.write(ESCAPE_MARKER >> 8);
                    gzipOp.write(ESCAPE_MARKER & 0xFF);
                }
                gzipOp.write(strBytes);
                gzipOp.close();
                b = bop.toByteArray();
            }
        } else {
            ByteArrayOutputStream bop = new ByteArrayOutputStream();
            // Write ESCAPE_MARKER, 00 to indicate Object Stream
            bop.write(ESCAPE_MARKER >> 8);
            bop.write(ESCAPE_MARKER & 0xFF);
            bop.write(0);
            bop.write(0);
            // Write Object
            ObjectOutputStream oop = new ObjectOutputStream(bop);
            oop.writeObject(data);
            oop.close();
            int len = bop.size();
            if (data instanceof byte[] || len < COMPRESSION_THRESHOLD) {
                b = bop.toByteArray();
            } else {
                ByteArrayOutputStream bop1 = new ByteArrayOutputStream(len / 4);
                GZIPOutputStream gzipOp = new GZIPOutputStream(bop1);
                bop.writeTo(gzipOp);
                gzipOp.close();
                if (bop1.size() * 4 <= len * 3) {
                    b = bop1.toByteArray();
                } else {
                    b = bop.toByteArray();
                    if (log.isInfoEnabled()) {
                        log.info("Abandoning Gzip as compressed size " + bop1.size() + " > 75% of original size " + len);
                    }
                }
            }
        }

        return b;
    }

    // Positions stream at a point ready to read Object or String after discarding ESCAPE_MARKER/Object Marker
    private static StreamType checkStreamType(InputStream ip) throws IOException {
        ip.mark(2);
        int firstTwoBytes = ip.read();
        firstTwoBytes = (firstTwoBytes << 8) | ip.read();
        StreamType status = StreamType.STRING;
        switch (firstTwoBytes) {
          case GenUtil.GZIP_HEADER:
            status = StreamType.GZIP;
            ip.reset();
            break;
          case ESCAPE_MARKER:
            ip.mark(2);
            int marker = ip.read();
            marker = (marker << 8) | ip.read();
            if (marker == 0) {
                status = StreamType.OBJECT;
            } else {
                ip.reset();
            }
            break;
          default:
            ip.reset();
            break;
        }

        return status;
    }

    public static Object deSerialize(byte [] b, Charset charset) throws IOException, ClassNotFoundException {
        ReusableByteArrayInputStream rip = new ReusableByteArrayInputStream(b);
        InputStream ip = rip;
        Object o = null;
        StreamType streamType = checkStreamType(ip);
        if (streamType == StreamType.STRING) {
            int strStart = rip.getPos();
            int strLen = rip.getCount() - strStart;
            o = new String(b, strStart, strLen, charset);
        } else if (streamType == StreamType.GZIP) {
            ip = new BufferedInputStream(new GZIPInputStream(ip), 1024);
            streamType = checkStreamType(ip);
            if (streamType == StreamType.GZIP) {
                streamType = StreamType.STRING;
            }
        }
        if (o == null) {
            switch(streamType) {
              case OBJECT:
                ObjectInputStream oip = new ObjectInputStream(ip);
                o = oip.readObject();
                break;
              default:
                Reader r = new InputStreamReader(ip, charset);
                int charsRead;
                char [] cbuf = new char[128];
                StringBuilder sb = new StringBuilder(b.length * 4); // Estimating 4x compression
                while ((charsRead = r.read(cbuf)) > 0) {
                    sb.append(cbuf, 0, charsRead);
                }
                o = sb.toString();
                break;
            }
        }

        return o;
    }

    @Override
    public Object getNativeConnection(String key) {
        return pool.getResource();
    }

    @Override
    public void returnNativeConnection(String key, Object jedis) {
        pool.returnResource((Jedis) jedis);
    }

    @Override
    public Object get(String key, Charset charset) throws CacheRetrieveException {
        Object value = null;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        byte [] b = null;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                jedis = pool.getResource();
                b = jedis.get(key.getBytes(GenUtil.UTF_8_CHARSET));
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("JedisConnection Error in Jedis get, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis get, redis server: " + connectionString + ", Key: " + key + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);
        if (b != null) {
            try {
                value = deSerialize(b, charset);
            } catch (Exception e) {
                log.error("Error deSerializing Jedis get, redis server: " + connectionString + ", Key: " + key, e);
                throw new CacheRetrieveException(e.getMessage());
            }
        }
        return value;
    }

    @Override
    public Map<String, Object> getMulti(String[] ids, Charset charset)
            throws CacheRetrieveException {
        Jedis jedis = null;
        Map<String, Object> valueMap = null;
        if (ids != null) {
            valueMap = new HashMap<String, Object>();
        }
        if (ids != null && ids.length > 0) {
            boolean isConnectionBroken;
            Config c = configManager.getConfig();
            int i, tryCount = 0, connectAttempts = c.connectAttempts;
            List<byte[]> binaryVals = null;
            do {
                isConnectionBroken = false;
                tryCount++;
                try {
                    jedis = pool.getResource();
                    byte [][] keys = new byte[ids.length][];
                    for (i = 0; i < ids.length; i++) {
                        keys[i] = ids[i].getBytes(GenUtil.UTF_8_CHARSET);
                    }
                    if (commonCachedProperties.getBooleanPropertyValue("ct.common.jedis.pipeline.for.mget", false)) {
                    	binaryVals = new ArrayList<>();
                    	Pipeline pipeline = jedis.pipelined();
                    	List<Response<byte[]>> responses = new ArrayList<>();
                    	for (int j = 0; j < ids.length; j++) {
							responses.add(pipeline.get(keys[j]));
						}
                    	pipeline.sync();
                    	binaryVals.addAll(responses.stream().map(Response::get).collect(Collectors.toList()));
                    } else {
                    	binaryVals = jedis.mget(keys);
                    }
                } catch (JedisConnectionException e) {
                    isConnectionBroken = true;
                    log.error("JedisConnection Error in Jedis mget, redis server: " + connectionString + ", Try No: " + tryCount, e);
                    if (tryCount >= connectAttempts) {
                        throw new CacheRetrieveException(e.getMessage());
                    }
                } catch (Exception e) {
                    log.error("Error in Jedis mget, redis server: " + connectionString + ", Try No: " + tryCount, e);
                    throw new CacheRetrieveException(e.getMessage());
                } finally {
                    if (jedis != null) {
                        if (isConnectionBroken) {
                            pool.returnBrokenResource(jedis);
                        } else {
                            pool.returnResource(jedis);
                        }
                    }
                }
            } while (isConnectionBroken && tryCount < connectAttempts);
            if (binaryVals != null && binaryVals.size() == ids.length) {
                try {
                    for (i = 0; i < ids.length; i++) {
                        String key = ids[i];
                        byte [] b = binaryVals.get(i);
                        if (b != null) {
                            Object nextVal = deSerialize(b, charset);
                            valueMap.put(key, nextVal);
                        }
                    }
                } catch (Exception e) {
                    log.error("Error deSerializing Jedis mget, redis server: " + connectionString, e);
                    throw new CacheRetrieveException(e.getMessage());
                }

            }
        }
        return valueMap;
    }

    @Override
    public Long incrBy(String key, long val) throws CacheStoreException {
        Long jedisResult = 0L;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        byte [] keyBytes = null;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                if (tryCount == 1) {
                    keyBytes = key.getBytes(GenUtil.UTF_8_CHARSET);
                }
                jedis = pool.getResource();
                jedisResult = jedis.incrBy(keyBytes, val);
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("JedisConnection Error in Jedis put, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis put, redis server: " + connectionString + ", Key: " + key + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);
        return jedisResult;
    }

    @Override
    public Long incr(String key) throws CacheStoreException {
        Long jedisResult = 0L;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        byte [] keyBytes = null;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                if (tryCount == 1) {
                    keyBytes = key.getBytes(GenUtil.UTF_8_CHARSET);
                }
                jedis = pool.getResource();
                jedisResult = jedis.incr(keyBytes);
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("JedisConnection Error in Jedis put, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis put, redis server: " + connectionString + ", Key: " + key + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);
        return jedisResult;
    }

    @Override
    public Long decr(String key) throws CacheStoreException {
        Long jedisResult = 0L;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        byte [] keyBytes = null;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                if (tryCount == 1) {
                    keyBytes = key.getBytes(GenUtil.UTF_8_CHARSET);
                }
                jedis = pool.getResource();
                jedisResult = jedis.decr(keyBytes);
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("JedisConnection Error in Jedis put, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis put, redis server: " + connectionString + ", Key: " + key + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);
        return jedisResult;
    }

    @Override
    public Long decrBy(String key, long val) throws CacheStoreException {
        Long jedisResult = 0L;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        byte [] keyBytes = null;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                if (tryCount == 1) {
                    keyBytes = key.getBytes(GenUtil.UTF_8_CHARSET);
                }
                jedis = pool.getResource();
                jedisResult = jedis.decrBy(keyBytes, val);
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("JedisConnection Error in Jedis put, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis put, redis server: " + connectionString + ", Key: " + key + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);
        return jedisResult;
    }


    @Override
    public boolean put(String key, Object val, int expirySeconds) throws CacheStoreException {
        boolean status = false;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        byte [] keyBytes = null;
        byte [] data = null;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                if (tryCount == 1) {
                    keyBytes = key.getBytes(GenUtil.UTF_8_CHARSET);
                    data = serialize(val);
                }
                jedis = pool.getResource();
                String jedisResult = null;
                if (expirySeconds > 0) {
                    jedisResult = jedis.setex(keyBytes, expirySeconds, data);
                } else if (expirySeconds == 0) {
                    jedisResult = jedis.set(keyBytes, data);
                } else {
                    status = jedis.del(keyBytes) != null;
                }
                if (expirySeconds >= 0) {
                    status = OK.equals(jedisResult);
                }
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("JedisConnection Error in Jedis put, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis put, redis server: " + connectionString + ", Key: " + key + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);

        return status;
    }

    @Override
    public boolean putMulti(Map<String, ? extends Object> map, int expirySeconds, Charset charset) throws CacheStoreException {
        boolean status = false;
        if (map != null && map.size() > 0) {
			Jedis jedis = null;
			boolean isConnectionBroken;
			Config c = configManager.getConfig();
			int tryCount = 0, connectAttempts = c.connectAttempts;
			byte[][] keyBytes = new byte[map.size()][];
			byte[][] data = new byte[map.size()][];
			do {
				isConnectionBroken = false;
				tryCount++;
				try {
					if (tryCount == 1) {
						int i = 0;
						for (Entry<String, ? extends Object> entry : map.entrySet()) {
							keyBytes[i] = entry.getKey().getBytes(GenUtil.UTF_8_CHARSET);
							data[i] = serialize(entry.getValue());
							i++;
						}
					}
					jedis = pool.getResource();
					Pipeline pipeline = jedis.pipelined();
					List<Response<String>> response = new ArrayList<>();
					if (expirySeconds > 0) {
						for (int i = 0; i < map.size(); i++) {
							response.add(pipeline.setex(keyBytes[i], expirySeconds, data[i]));
						}
					} else if (expirySeconds == 0) {
						for (int i = 0; i < map.size(); i++) {
							response.add(pipeline.set(keyBytes[i], data[i]));
						}
					} else {
						for (int i = 0; i < map.size(); i++) {
							pipeline.del(keyBytes[i]);
						}
					}
					pipeline.sync();
					if (expirySeconds >= 0 && response != null) {
						Optional<Response<String>> failedResponse = response.stream().filter(res -> !res.get().equals(OK)).findAny();
						status = !failedResponse.isPresent();
					}
				} catch (JedisConnectionException e) {
					isConnectionBroken = true;
					log.error("JedisConnection Error in Jedis multi put, redis server: " + connectionString + ", Try No: " + tryCount, e);
					if (tryCount >= connectAttempts) {
						throw new CacheRetrieveException(e.getMessage());
					}
				} catch (Exception e) {
					log.error("Error in Jedis multi put, redis server: " + connectionString + ", Try No: " + tryCount, e);
					throw new CacheRetrieveException(e.getMessage());
				} finally {
					if (jedis != null) {
						if (isConnectionBroken) {
							pool.returnBrokenResource(jedis);
						} else {
							pool.returnResource(jedis);
						}
					}
				}
			} while (isConnectionBroken && tryCount < connectAttempts);
		}
		return status;
    }

    @Override
    public boolean remove(String key) {
        boolean status = false;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                jedis = pool.getResource();
                Long longResult = jedis.del(key.getBytes(GenUtil.UTF_8_CHARSET));
                status = longResult != null && longResult == 1;
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("Error in Jedis remove, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis remove, redis server: " + connectionString + ", Key: " + key + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);

        return status;
    }

    @Override
    public void clear() throws CacheDeleteException {
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                jedis = pool.getResource();
                jedis.flushDB();
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("Error in Jedis clear, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis clear, redis server: " + connectionString + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);
    }

    public Long addInternal(String key, Object val, int expirySeconds)
            throws CacheStoreException {
        Long longResult = null;
        if (expirySeconds >= 0) {
            Jedis jedis = null;
            boolean isConnectionBroken;
            Config c = configManager.getConfig();
            int tryCount = 0, connectAttempts = c.connectAttempts;
            byte [] keyBytes = null;
            byte [] data = null;
            do {
                isConnectionBroken = false;
                tryCount++;
                try {
                    String s = null;
                    if (expirySeconds > 0 && val instanceof String) {
                        s = (String) val;
                    }
                    if (expirySeconds == 0 || s == null || s.length() >= COMPRESSION_THRESHOLD) {
                        if (tryCount == 1) {
                            keyBytes = key.getBytes(GenUtil.UTF_8_CHARSET);
                            data = serialize(val);
                        }
                    }
                    jedis = pool.getResource();
                    if (expirySeconds == 0) {
                        longResult = jedis.setnx(keyBytes, data);
                    } else {
                        String expirySecondsStr = String.valueOf(expirySeconds);
                        if (data == null) {
                            longResult = (Long) JedisUtil.evalScript(jedis, RedisScript.ADD_SCRIPT, 1, key, expirySecondsStr, s);
                        } else {
                            // Evalsha not available in jedis for binary data, so use eval
                            byte [] scriptBytes = addScriptBytes;
                            if (scriptBytes == null) {
                                scriptBytes = RedisScript.ADD_SCRIPT.getScript().getBytes(GenUtil.UTF_8_CHARSET);
                                addScriptBytes = scriptBytes;
                            }
                            byte [][] scriptArgs = new byte[3][];
                            scriptArgs[0] = keyBytes;
                            scriptArgs[1] = expirySecondsStr.getBytes(GenUtil.UTF_8_CHARSET);
                            scriptArgs[2] = data;
                            longResult = (Long) jedis.eval(scriptBytes, "1".getBytes(), scriptArgs);
                        }
                    }
                } catch (JedisConnectionException e) {
                    isConnectionBroken = true;
                    log.error("Error in Jedis add, redis server: " + connectionString + ", Try No: " + tryCount, e);
                    if (tryCount >= connectAttempts) {
                        throw new CacheRetrieveException(e.getMessage());
                    }
                } catch (Exception e) {
                    log.error("Error in Jedis add, redis server: " + connectionString + ", Try No: " + tryCount, e);
                    throw new CacheRetrieveException(e.getMessage());
                } finally {
                    if (jedis != null) {
                        if (isConnectionBroken) {
                            pool.returnBrokenResource(jedis);
                        } else {
                            pool.returnResource(jedis);
                        }
                    }
                }
            } while (isConnectionBroken && tryCount < connectAttempts);
        }

        return longResult;
    }

    @Override
    public boolean add(String key, Object val, int expirySeconds)
            throws CacheStoreException {
        Long longResult = addInternal(key, val, expirySeconds);
        boolean status = longResult != null && longResult == 1;
        return status;
    }

    /**
     * {@inheritDoc}
     * verifyFailedLock parameter is ignored as it is not required for redis. UNKNOWN might still be returned
     * in case of an execution error.
     * @see com.cleartrip.common.cache.Cache#lock(Object, int, boolean)
     */
    @Override
    public LockStatus lock(String key, int expirySeconds, boolean verifyFailedLock) {
        long now = System.currentTimeMillis(), currentExpiryTime, expiryMillis = expirySeconds * 1000;
        LockStatus status = LockStatus.UNKNOWN;
        String currentStr = (String) get(key);
        long expiryTime = now + expiryMillis;
        if (currentStr != null) {
            currentExpiryTime = expiryTime;
            try {
                currentExpiryTime = Long.parseLong(currentStr);
                if (currentExpiryTime < 1000) {
                    currentExpiryTime = expiryTime; // To account for existing locks which store 1
                }
                if (now < currentExpiryTime && currentExpiryTime - now <= expiryMillis + 5000) {
                    status = LockStatus.FAILED;
                }
            } catch (Exception e) {
                log.error("Error parsing expiryTime: " + currentStr + " for lock key " + key);
                throw new CacheRetrieveException(e.getMessage());
            }
            // Somehow an expired key is present possibly because this server is a slave or a redis bug which
            // is quite unlikely. Delete the key if the ttl is -1 or greater than current expiry seconds
            if (status == LockStatus.UNKNOWN) {
                Jedis jedis = null;
                boolean isConnectionBroken = false;
                try {
                    String k = (String) key;
                    jedis = pool.getResource();
                    Long ttl = jedis.ttl(k);
                    if (ttl != null) {
                        if (ttl == -1 || ttl > (currentExpiryTime - now + 30000) / 1000 || ttl > 3600) {
                            jedis.del(k);
                        }
                    }
                } catch (JedisConnectionException e) {
                    isConnectionBroken = true;
                    log.error("Error in Jedis ttl, redis server: " + connectionString, e);
                    throw new CacheRetrieveException(e.getMessage());
                } catch (Exception e) {
                    log.error("Error in Jedis ttl, redis server: " + connectionString, e);
                    throw new CacheRetrieveException(e.getMessage());
                } finally {
                    if (jedis != null) {
                        if (isConnectionBroken) {
                            pool.returnBrokenResource(jedis);
                        } else {
                            pool.returnResource(jedis);
                        }
                    }
                }
            }
        }
        if (status == LockStatus.UNKNOWN) {
            Long longResult = addInternal(key, String.valueOf(expiryTime), expirySeconds);
            if (longResult != null) {
                status = LockStatus.FAILED;
                if (longResult == 1) {
                    status = LockStatus.SUCCESS;
                }
            }
        }

        return status;
    }

    private boolean tryUpdate(Jedis jedis, byte [] keyBytes, int expirySeconds,
            ValueUpdater updater, int i) throws ClassNotFoundException, IOException {
        boolean success = false;
        jedis.watch(keyBytes);
        boolean unwatchPending = true;
        Transaction t = null;
        try {
            byte [] data = jedis.get(keyBytes);
            Object oldVal = null;
            if (data != null) {
                oldVal = deSerialize(data, GenUtil.UTF_8_CHARSET);
            }
            Object newVal = updater.update(oldVal, i);
            if (newVal == ValueUpdater.SKIP_UPDATE) {
                jedis.unwatch();
                unwatchPending = false;
                success = true;
            } else {
                data = null;
                if (newVal != null) {
                    data = serialize(newVal);
                }
                t = jedis.multi();
                if (data != null && expirySeconds >= 0) {
                    if (expirySeconds == 0) {
                        t.set(keyBytes, data);
                    } else {
                        t.setex(keyBytes, expirySeconds, data);
                    }
                } else {
                    t.del(keyBytes);
                }
                List<Object> result = t.exec();
                t = null;
                unwatchPending = false;

                success = result != null;
            }
        } finally {
            // Attempting to leave jedis in a consistent state in case of Exceptions
            try {
                if (t != null) {
                    t.discard();
                } else if (unwatchPending) {
                    jedis.unwatch();
                }
            } catch (Exception e) {
                t = null;
            }
        }

        return success;
    }

    @Override
    public boolean atomicUpdate(List<String> idList, int expirySeconds, List<ValueUpdater> updaterList, int maxTries) {
        if (updaterList == null || updaterList.size() == 0) {
            throw new NullPointerException("Null Updater");
        }
        boolean success = false;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                jedis = pool.getResource();
                for (int i = 0; i < maxTries; i++) {
                    success = tryUpdate(jedis, idList, expirySeconds, updaterList, i);
                    if (success) {
                        break;
                    }
                }
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("Error in Jedis atomicUpdate, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis atomicUpdate, redis server: " + connectionString + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);

        return success;
    }

    private boolean tryUpdate(Jedis jedis, List<String> keyList, int expirySeconds,
            List<ValueUpdater> updaterList, int i) throws ClassNotFoundException, IOException {
        boolean success = false;
        for (String key : keyList) {
            jedis.watch(key);
        }
        boolean unwatchPending = true;
        Transaction t = null;
        try {
            boolean notSkipped = false;
            List dataList = new ArrayList();
            for (int index = 0; index < keyList.size(); index++) {
                byte [] data = jedis.get(keyList.get(index).getBytes(GenUtil.UTF_8_CHARSET));
                Object oldVal = null;
                if (data != null) {
                    oldVal = deSerialize(data, GenUtil.UTF_8_CHARSET);
                }
                Object newVal = updaterList.get(index).update(oldVal, i);
                dataList.add(newVal);
                if (newVal != ValueUpdater.SKIP_UPDATE) {
                    notSkipped = true;
                }
            }
            if (!notSkipped) {
                jedis.unwatch();
                unwatchPending = false;
                success = true;
            } else {
                byte[] data = null;
                t = jedis.multi();
                for (int index = 0; index < keyList.size(); index++) {
                    Object newVal = dataList.get(index);
                    byte[] keyBytes = keyList.get(index).getBytes(GenUtil.UTF_8_CHARSET);
                    if (newVal == ValueUpdater.SKIP_UPDATE) {
                        continue;
                    }
                    if (newVal != null) {
                        data = serialize(newVal);
                    }
                    if (data != null && expirySeconds >= 0) {
                        if (expirySeconds == 0) {
                            t.set(keyBytes, data);
                        } else {
                            t.setex(keyBytes, expirySeconds, data);
                        }
                    } else {
                        t.del(keyBytes);
                    }
                }
                List<Object> result = t.exec();
                t = null;
                unwatchPending = false;
    
                success = result != null;
            }
        } finally {
            // Attempting to leave jedis in a consistent state in case of Exceptions
            try {
                if (t != null) {
                    t.discard();
                } else if (unwatchPending) {
                    jedis.unwatch();
                }
            } catch (Exception e) {
                t = null;
            }
        }

        return success;
    }

    @Override
    public boolean atomicUpdate(String id, int expirySeconds, ValueUpdater updater, int maxTries) {
        if (updater == null) {
            throw new NullPointerException("Null Updater");
        }
        boolean success = false;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                jedis = pool.getResource();
                byte [] keyBytes = id.getBytes(GenUtil.UTF_8_CHARSET);
                for (int i = 0; i < maxTries; i++) {
                    success = tryUpdate(jedis, keyBytes, expirySeconds, updater, i);
                    if (success) {
                        break;
                    }
                }
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("Error in Jedis atomicUpdate, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis atomicUpdate, redis server: " + connectionString + ", Key: " + id + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);

        return success;
    }

    @Override
    public boolean setExpiry(String key, int expirySeconds) {
        boolean status = false;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                byte [] keyBytes = key.getBytes(GenUtil.UTF_8_CHARSET);
                jedis = pool.getResource();
                Long jedisResult = null;
                if (expirySeconds > 0) {
                    jedisResult = jedis.expire(keyBytes, expirySeconds);
                } else if (expirySeconds == 0) {
                    jedisResult = jedis.persist(keyBytes);
                } else {
                    jedisResult = jedis.del(keyBytes);
                }
                if (jedisResult != null && jedisResult > 0) {
                    status = true;
                }
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("Error in Jedis put, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis put, redis server: " + connectionString + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);

        return status;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.cleartrip.common.cache.Cache#destroy()
     */
    @Override
    public void destroy() throws Exception {
        pool.destroy();
    }

    // Added to inject a mock JedisPool
    public void setPool(JedisPool ppool) {
        pool = ppool;
    }

    @Override
    public Config loadConfig(Map<String, String> cachedPropertiesResource) {
        return new Config(cachedPropertiesResource);
    }

    @Override
    public long getServerTime() {
        long now = 0;
        Jedis jedis = null;
        boolean isConnectionBroken;
        Config c = configManager.getConfig();
        int tryCount = 0, connectAttempts = c.connectAttempts;
        do {
            isConnectionBroken = false;
            tryCount++;
            try {
                jedis = pool.getResource();
                List<String> timeList = jedis.time();
                if (timeList != null && timeList.size() == 2) {
                    now = Long.parseLong(timeList.get(0)) * 1000 + Long.parseLong(timeList.get(1)) / 1000;
                }
            } catch (JedisConnectionException e) {
                isConnectionBroken = true;
                log.error("Error in Jedis get, redis server: " + connectionString + ", Try No: " + tryCount, e);
                if (tryCount >= connectAttempts) {
                    throw new CacheRetrieveException(e.getMessage());
                }
            } catch (Exception e) {
                log.error("Error in Jedis get, redis server: " + connectionString + ", Try No: " + tryCount, e);
                throw new CacheRetrieveException(e.getMessage());
            } finally {
                if (jedis != null) {
                    if (isConnectionBroken) {
                        pool.returnBrokenResource(jedis);
                    } else {
                        pool.returnResource(jedis);
                    }
                }
            }
        } while (isConnectionBroken && tryCount < connectAttempts);
        return now;
    }

    /*
    public static void main(String [] args) throws Exception {
        Jedis jedis = new Jedis("172.16.53.176");
        try {
            List<String> vals = jedis.mget(new String [] {"hello", "foo"});
            System.out.println("s = " + vals);
        } finally {
            jedis.disconnect();
        }
    }
    */
}
