package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.util.CustomCachedUrlResources;
import com.companyApiTinyCache.companyConfigCache.service.common.util.SecurityUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class CryptController extends AbstractController {

    protected static final Log logger = LogFactory.getLog(CryptController.class);

    private CustomCachedUrlResources cachedUrlResources;

    public CryptController(CustomCachedUrlResources pcachedUrlResources) {
        cachedUrlResources = pcachedUrlResources;
        setCacheSeconds(0);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.mvc.AbstractFormController#handleRequestInternal(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String message = request.getParameter("s");
        String type = request.getParameter("t");
        String decode = request.getParameter("d");
        byte[] authKey = SecurityUtil.getAcubeKey(cachedUrlResources);
        String outStr;
        logger.info("Type :" + type);
        logger.info("message :" + message);
        logger.info("decode :" + decode);

        if (StringUtils.isEmpty(decode))
            decode = "y";

        if (StringUtils.isNotEmpty(type) && type.equals("e")) {
            outStr = URLEncoder.encode(SecurityUtil.aesEncrypt(message, authKey), "UTF-8");
        } else {
            if (decode.equals("y"))
                outStr = SecurityUtil.aesDecrypt(URLDecoder.decode(message, "UTF-8"), authKey);
            else
                // no need to decode, just decrypt directly
                outStr = SecurityUtil.aesDecrypt(message, authKey);
        }

        PrintWriter responseWriter = response.getWriter();
        responseWriter.print(outStr);
        responseWriter.close();
        return null;
    }
}
