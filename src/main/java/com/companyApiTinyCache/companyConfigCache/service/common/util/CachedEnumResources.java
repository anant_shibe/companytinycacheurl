package com.companyApiTinyCache.companyConfigCache.service.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.EnumMap;

public abstract class CachedEnumResources<K extends Enum<K> & EnumResourceName, V> extends CachedResource<EnumMap<K, V>> {

    private static final Log logger = LogFactory.getLog(CachedEnumResources.class);

    protected Class<K> enumClass;

    // Array of Enum Vals
    protected K[] enumVals;

    // Pre-computed file for filePrefix + resourceName corresponding to enumvals
    protected File[] files;

    private long[] modifiedTimes;

    /**
     * Method to load a resource. The impementation can merge the result with the classpath resource if desired.
     * 
     * @param resourceKey
     *            The Enum value of the resource to load
     * @param isFileResource
     *            True if fileResource is to be loaded, false to load classPath Resource
     * @exception Exception
     *                If there is an error loading resource in which case the old resource is retained and other resources are loaded.
     * @return
     */
    protected abstract V loadResource(K resourceKey, boolean isFileResource) throws Exception;

    /**
     * Sets the path for file resources. This should be called only during initialization time before the first refresh and must be called if the filePath is passed as null in the constructor.
     * 
     * @param filePath
     */
    protected void setPath(String filePath) {
        String fileSeparator = System.getProperty("file.separator");
        if (!filePath.endsWith(fileSeparator) && !filePath.endsWith("/")) {
            filePath += '/';
        }
        int i;
        files = new File[enumVals.length];
        modifiedTimes = new long[enumVals.length];
        for (i = 0; i < enumVals.length; i++) {
            String resourceName = enumVals[i].getResourceName();
            String fileName = filePath + resourceName;
            files[i] = new File(fileName);
            modifiedTimes[i] = 1; // So that initial load works on all files
        }
    }

    /**
     * Constructor
     * 
     * @param c
     *            Class of the Enum Resource Key
     * @param filePath
     *            File Path under which resources are located. This can be null in which case the setPath method must be called before the first refresh call.
     */
    public CachedEnumResources(Class<K> c, String filePath) {
        enumClass = c;
        enumVals = c.getEnumConstants();
        if (filePath != null) {
            setPath(filePath);
        }
        setName(c.getName());
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.cleartrip.common.util.CachedResource#loadResource(boolean)
     */
    @Override
    protected EnumMap<K, V> loadResource(long cacheMillis, boolean force) throws Exception {
        EnumMap<K, V> newResourceMap = null;
        int i;
        for (i = 0; i < enumVals.length; i++) {
            long currentFileModifiedTime = files[i].lastModified();
            long fileModifiedTime = modifiedTimes[i];
            boolean requiresRefresh = currentFileModifiedTime > fileModifiedTime || currentFileModifiedTime <= 0 && fileModifiedTime > 0;
            if (requiresRefresh || force && currentFileModifiedTime > 0) {
                K resourceKey = enumVals[i];
                V newResource;
                try {
                    isErrorOnLastLoad = false;
                    newResource = loadResource(resourceKey, currentFileModifiedTime > 0);
                    modifiedTimes[i] = currentFileModifiedTime;
                } catch (Exception e) {
                    logger.info("Error loading Resource index: " + i + " name: " + resourceKey.name() + ", Ignoring it.", e);
                    continue;
                }
                if (newResourceMap == null) {
                    EnumMap<K, V> oldResourceMap = resource;
                    if (oldResourceMap != null) {
                        newResourceMap = new EnumMap<K, V>(oldResourceMap);
                    } else {
                        newResourceMap = new EnumMap<K, V>(enumClass);
                    }
                }
                newResourceMap.put(resourceKey, newResource);
            }
        }
        return newResourceMap;
    }
}
