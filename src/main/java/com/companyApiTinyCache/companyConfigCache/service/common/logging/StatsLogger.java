package com.companyApiTinyCache.companyConfigCache.service.common.logging;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

@ClassExcludeCoverage
public class StatsLogger {

    private CachedProperties commonCachedProperties;

    private static final Log logger = LogFactory.getLog(StatsLogger.class);

    public StatsLogger() {
    }

    public CachedProperties getCommonCachedProperties() {
        return commonCachedProperties;
    }

    public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }

    public void sendCounterStats(String counterName) {
        sendUDPPacket(counterName + ":1|c");
    }

    public void sendTimingStats(String counterName, long timeInMillsecs) {
        sendUDPPacket(counterName + ":" + timeInMillsecs + "|ms");
    }

    private void sendUDPPacket(String message) {
        boolean loggingEnabled = commonCachedProperties.getBooleanPropertyValue("ct.appdata.logging.statsd", false);

        if (loggingEnabled) {
            String statsDServerIP = commonCachedProperties.getPropertyValue("ct.appdata.logging.statsd.udp.server.ip", "statsd.cleartrip.com");
            int statsDServerPort = commonCachedProperties.getIntPropertyValue("ct.appdata.logging.statsd.udp.server.port", 4445);

            byte[] buf = message.getBytes();
            InetAddress address;
            try {
                address = InetAddress.getByName(statsDServerIP);
                DatagramPacket packet = new DatagramPacket(buf, buf.length, address, statsDServerPort);
                DatagramSocket socket = new DatagramSocket();
                socket.send(packet);
            } catch (UnknownHostException e) {
                logger.error("Exception is:" + e.getMessage(), e);
            } catch (IOException e) {
                logger.error("Exception is:" + e.getMessage(), e);
            }
        }
    }
}
