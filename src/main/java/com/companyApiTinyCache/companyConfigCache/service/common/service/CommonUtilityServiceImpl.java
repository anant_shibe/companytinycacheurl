package com.companyApiTinyCache.companyConfigCache.service.common.service;

import com.companyApiTinyCache.companyConfigCache.service.common.connector.CompanyAPIConnector;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Affiliate;
import com.companyApiTinyCache.companyConfigCache.service.common.data.AffiliateConfig;
import com.companyApiTinyCache.companyConfigCache.service.common.data.Resource;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CompanyConfig;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Collection;

public class CommonUtilityServiceImpl implements CommonUtilityService {

    private static final Log logger = LogFactory.getLog(CommonUtilityServiceImpl.class);

    private CachedProperties commonCachedProperties;

    private CompanyAPIConnector companyAPIConnector;

    @Override
    public void updateCompanyResourcesInFileSystem(int companyId, String resourceTypeTobeUpdated) throws Exception {

        Affiliate affiliate = companyAPIConnector.getCompanyByCompanyIdWithoutPeople(companyId);

        if (affiliate == null) {
            throw new Exception("Company " + companyId + " not found or company API has some problem");
        }
        AffiliateConfig affiliateConfig = affiliate.getAffiliateConfigs().get(CompanyConfig.URL_PATH_PREFIX);

        if (affiliateConfig == null) {
            throw new Exception("URL_PATH_PREFIX config is missing for company " + affiliate.getCompanyName());
        }
        String companyURLPrefix = affiliateConfig.getConfigValue();

        String baseDir = commonCachedProperties.getPropertyValue("ct.common.jsp.resource.dir");

        if (StringUtils.isBlank(baseDir)) {
            throw new Exception("ct.common.jsp.resource.dir is missing from property files.");
        }

        logger.info("Updating resources for company: " + companyId + ", company name: " + affiliate.getCompanyName());

        Collection<Resource> resources = affiliate.getResources();

        if (resources != null) {
            for (Resource resource : resources) {
                try {
                    String resourceType = resource.getResourceType();
                    if (StringUtils.isNotBlank(resourceTypeTobeUpdated) && !resourceTypeTobeUpdated.equalsIgnoreCase(resourceType)) {
                        continue;
                    }
                    String resourceValue = resource.getResourceValue();
                    String resourceFile = null;

                    if (resourceType.equalsIgnoreCase("LOGO") || resourceType.equalsIgnoreCase("CUSTOM_CSS")) {

                    } else {
                        resourceFile = baseDir + "/commonui/resources/" + resourceType.toLowerCase() + "/" + companyURLPrefix + "/" + resourceType.toLowerCase() + ".jsp";
                    }

                    if (StringUtils.isNotBlank(resourceFile)) {
                        FileUtils.writeStringToFile(new File(resourceFile), resourceValue);
                    }

                } catch (Exception e) {
                    logger.error(e);
                }
            }
        }

    }

    public void setCommonCachedProperties(CachedProperties commonCachedProperties) {
        this.commonCachedProperties = commonCachedProperties;
    }

    public void setCompanyAPIConnector(CompanyAPIConnector companyAPIConnector) {
        this.companyAPIConnector = companyAPIConnector;
    }

}
