package com.companyApiTinyCache.companyConfigCache.service.common.util.soap;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.*;
import org.apache.axis.message.SOAPEnvelope;

@ClassExcludeCoverage
public class SoapResponse {

    private SOAPEnvelope responseContents;

    public SoapResponse() {
        // Added for unit testing purpose
    }

    public SoapResponse(SOAPEnvelope responseContents) {
        this.responseContents = responseContents;
    }

    public SOAPEnvelope getResponseContents() {
        return responseContents;
    }

    public void setResponseContents(SOAPEnvelope responseContents) {
        this.responseContents = responseContents;
    }

}
