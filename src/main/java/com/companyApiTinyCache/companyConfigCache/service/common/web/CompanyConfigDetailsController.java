package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.data.CompanyConfigDetailsBean;
import com.companyApiTinyCache.companyConfigCache.service.common.service.CompanyConfigDetailsService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ClassExcludeCoverage
public class CompanyConfigDetailsController extends AbstractController {

	private CompanyConfigDetailsService companyConfigDetailsService;

	public CompanyConfigDetailsService getCompanyConfigDetailsService() {
		return companyConfigDetailsService;
	}

	public void setCompanyConfigDetailsService(CompanyConfigDetailsService companyConfigDetailsService) {
		this.companyConfigDetailsService = companyConfigDetailsService;
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		//Caching is done at companyapitinycache level in companyconfiguration filter
		
		ModelAndView model = new ModelAndView("companyConfigHeader");
		StringBuilder sb = new StringBuilder();
		CompanyConfigDetailsBean companyConfigDetailsBean = companyConfigDetailsService.getCompanyDetails(request);

		
		model.addObject("company_id", companyConfigDetailsBean.getCompanyId());
        model.addObject("name",companyConfigDetailsBean.getName());
        model.addObject("screen_name", companyConfigDetailsBean.getScreenName());
        model.addObject("company_code", companyConfigDetailsBean.getCompanyCode());
        model.addObject("support_number", companyConfigDetailsBean.getSupportNumber());
        model.addObject("enabled_products", companyConfigDetailsBean.getEnabledProducts());
        model.addObject("resourcepath", companyConfigDetailsBean.getResourcepath());
        model.addObject("coupon_code_enabled", companyConfigDetailsBean.getCouponCodeEnable());
        return model;
		}
		
}
