/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.CompanyConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;

public class Affiliate extends AffiliateInfo implements Serializable {

	@JsonIgnore
	private static final long serialVersionUID = 1L;

    private int affiliateId;

    private String companyName;

    private String companyTin;

    private String status;

    private Date effectiveFrom;

    private Date effectiveTo;

    private String promo_code;

    private Map<CompanyConfig, AffiliateConfig> affiliateConfigs;

    private AffiliateLogo affiliateLogo;

    private List<AffiliateTourCode> affiliateTourCodes;

    private ContactDetail contactDetail;

    private List<DepositAccount> depositAccounts;

    private List<UserProfile> affiliateUserProfiles;

    private String companyPan;

    private String companyServiceTaxNumber;

    private List<Tag> tags;

    private List<Department> departments;

    private Collection<Resource> resources;

    private Date createdAt;

    private Date updatedAt;

    private Double annualTravelBudget;

    @JsonIgnore
    private FaultDetail faultDetail;

    private String domainType;

    private List<GSTDetails> gstDetails;
    
    /**
     * @return the domainType
     */
    public String getDomainType() {
        return domainType;
    }

    /**
     * @param domainType
     *            the domainType to set
     */
    public void setDomainType(String domainType) {
        this.domainType = domainType;
    }

    public Affiliate() {
        // Removing all the instantiation of all objects like contactDetail, primaryContact
        // To accomodate for JSON binding
        affiliateUserProfiles = new ArrayList<UserProfile>();
        contactDetail = new ContactDetail();
    }

    @JsonProperty("service_no")
    public String getCompanyServiceTaxNumber() {
        return companyServiceTaxNumber;
    }

    @JsonProperty("service_no")
    public void setCompanyServiceTaxNumber(String companyServiceTaxNumber) {
        this.companyServiceTaxNumber = companyServiceTaxNumber;
    }

    @JsonProperty("pan_card_details")
    public String getCompanyPan() {
        return companyPan;
    }

    @JsonProperty("pan_card_details")
    public void setCompanyPan(String companyPan) {
        this.companyPan = companyPan;
    }

    @JsonIgnore
    public List<UserProfile> getAffiliateUserProfiles() {
        return affiliateUserProfiles;
    }

    @JsonIgnore
    public void setAffiliateUserProfiles(List<UserProfile> affiliateUserProfiles) {
        this.affiliateUserProfiles = affiliateUserProfiles;
    }

    /**
     * This method is only for JSON Binding
     */
    @JsonProperty("company_users")
    public void addAffiliateUsers(List<UserProfile> affiliateUsers) {
        if (this.affiliateUserProfiles == null) {
            this.affiliateUserProfiles = new ArrayList<UserProfile>();
        }
        this.affiliateUserProfiles.addAll(affiliateUsers);
    }

    /**
     * This method is only for JSON Binding
     */
    @JsonProperty("company_travellers")
    public void addAffiliateTravellers(List<UserProfile> affiliateTravellers) {
        if (this.affiliateUserProfiles == null) {
            this.affiliateUserProfiles = new ArrayList<UserProfile>();
        }
        this.affiliateUserProfiles.addAll(affiliateTravellers);
    }

    @JsonProperty("contact_data")
    public ContactDetail getContactDetail() {
        return contactDetail;
    }

    @JsonProperty("contact_data")
    public void setContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
    }

    @JsonProperty("id")
    public int getAffiliateId() {
        return affiliateId;
    }

    @JsonProperty("id")
    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    @JsonProperty("name")
    public String getCompanyName() {
        return companyName;
    }

    @JsonProperty("name")
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * TODO: suman: please check this should be lookups table sub_id value e.g 0,1,2,3,4 decoding of what they mean will be done at the application layer
     * 
     * @return
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("effective_from")
    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    @JsonProperty("effective_from")
    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    @JsonProperty("effective_to")
    public Date getEffectiveTo() {
        return effectiveTo;
    }

    @JsonProperty("effective_to")
    public void setEffectiveTo(Date effectiveTo) {
        this.effectiveTo = effectiveTo;
    }

    /**
     * Refer affiliateId
     * 
     * @return long
     * @author ulhas
     */
    @JsonIgnore
    public int getCompanyId() {
        return affiliateId;
    }

    /**
     * Refer affiliateId
     * 
     * @param companyId
     * @author ulhas
     */
    @JsonIgnore
    public void setCompanyId(int companyId) {
        this.affiliateId = companyId;
    }

    @JsonProperty("deposit_accounts")
    public List<DepositAccount> getDepositAccounts() {
        return depositAccounts;
    }

    @JsonProperty("deposit_accounts")
    public void setDepositAccounts(List<DepositAccount> depositAccountId) {
        this.depositAccounts = depositAccountId;
    }

    @JsonIgnore
    public String getPromo_code() {
        return promo_code;
    }

    @JsonIgnore
    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    @JsonProperty("tin_number")
    public String getCompanyTin() {
        return companyTin;
    }

    @JsonProperty("tin_number")
    public void setCompanyTin(String companyTin) {
        this.companyTin = companyTin;
    }

    @JsonProperty("company_tourcodes")
    public List<AffiliateTourCode> getAffiliateTourCodes() {
        return affiliateTourCodes;
    }

    @JsonProperty("company_tourcodes")
    public void setAffiliateTourCodes(List<AffiliateTourCode> tourCodesMap) {
        this.affiliateTourCodes = tourCodesMap;
    }

    @JsonProperty("company_configs")
    public List<AffiliateConfig> getAffiliateConfigsAsList() {
        List<AffiliateConfig> configList = null;
        if (this.affiliateConfigs != null) {
            configList = new ArrayList<AffiliateConfig>();
            for (Entry<CompanyConfig, AffiliateConfig> entry : this.affiliateConfigs.entrySet()) {
                configList.add(entry.getValue());
            }
        }
        return configList;
    }

    @JsonProperty("company_configs")
    public void setAffiliateConfigsFromList(List<AffiliateConfig> affiliateConfigsList) {
        this.affiliateConfigs = new LinkedHashMap<CompanyConfig, AffiliateConfig>();
        for (AffiliateConfig affConfig : affiliateConfigsList) {
            this.affiliateConfigs.put(affConfig.getConfigName(), affConfig);
        }
    }

    @JsonIgnore
    public Map<String, String> getAffiliateConfigsMap() {
        Map<String, String> configsMap = new LinkedHashMap<String, String>();
        for (Entry<CompanyConfig, AffiliateConfig> entry : this.affiliateConfigs.entrySet()) {
            configsMap.put(entry.getKey().name(), entry.getValue().getConfigValue());
        }
        return configsMap;
    }

    @JsonIgnore
    public Map<CompanyConfig, AffiliateConfig> getAffiliateConfigs() {
        return affiliateConfigs;
    }

    @JsonIgnore
    public void setAffiliateConfigs(Map<CompanyConfig, AffiliateConfig> affiliateConfigs) {
        this.affiliateConfigs = affiliateConfigs;
    }

    @JsonProperty("company_logo")
    public AffiliateLogo getAffiliateLogo() {
        return affiliateLogo;
    }

    @JsonProperty("company_logo")
    public void setAffiliateLogo(AffiliateLogo affiliateLogo) {
        this.affiliateLogo = affiliateLogo;
    }

    @JsonProperty("company_tags")
    public List<Tag> getTags() {
        return tags;
    }

    @JsonProperty("company_tags")
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @JsonProperty("departments")
    public List<Department> getDepartments() {
        return departments;
    }

    @JsonProperty("departments")
    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    @JsonProperty("resources")
    public Collection<Resource> getResources() {
        return resources;
    }

    @JsonProperty("resources")
    public void setResources(Collection<Resource> resources) {
        this.resources = resources;
    }

    @JsonProperty("created_at")
    public Date getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("annual_travel_budget")
    public Double getAnnualTravelBudget() {
        return annualTravelBudget;
    }

    @JsonProperty("annual_travel_budget")
    public void setAnnualTravelBudget(double annualTravelBudget) {
        this.annualTravelBudget = new Double(annualTravelBudget);
    }

    @JsonIgnore
    public FaultDetail getFaultDetail() {
        return faultDetail;
    }

    @JsonIgnore
    public void setFaultDetail(FaultDetail faultDetail) {
        this.faultDetail = faultDetail;
    }

    @JsonProperty("gst_details")
    public List<GSTDetails> getGstDetails() {
        return gstDetails;
    }

    @JsonProperty("gst_details")
    public void setGstDetails(List<GSTDetails> gstDetails) {
        this.gstDetails = gstDetails;
    }

	@Override
    public boolean equals(Object obj) {
        if (obj instanceof Affiliate == false)
            return false;
        Affiliate aff = (Affiliate) obj;
        // if(affiliateId != aff.getAffiliateId()){
        // return false;
        // }
        if ((companyName != null || aff.getCompanyName() != null) && !GenUtil.equals(companyName, aff.getCompanyName())) {
            return false;
        }
        if ((companyTin != null || aff.getCompanyTin() != null) && !GenUtil.equals(companyTin, aff.getCompanyTin())) {
            return false;
        }
        if ((status != null || aff.getStatus() != null) && !GenUtil.equals(status, aff.getStatus())) {
            return false;
        }
        if ((effectiveFrom != null || aff.getEffectiveFrom() != null) && !GenUtil.equals(effectiveFrom, aff.getEffectiveFrom())) {
            return false;
        }
        if ((effectiveTo != null || aff.getEffectiveTo() != null) && !GenUtil.equals(effectiveTo, aff.getEffectiveTo())) {
            return false;
        }
        if ((promo_code != null || aff.getPromo_code() != null) && !GenUtil.equals(promo_code, aff.getPromo_code())) {
            return false;
        }
        if ((companyPan != null || aff.getCompanyPan() != null) && !GenUtil.equals(companyPan, aff.getCompanyPan())) {
            return false;
        }
        if ((companyServiceTaxNumber != null || aff.getCompanyServiceTaxNumber() != null) && !GenUtil.equals(companyServiceTaxNumber, aff.getCompanyServiceTaxNumber())) {
            return false;
        }
        if (annualTravelBudget.doubleValue() != aff.getAnnualTravelBudget().doubleValue()) {
            return false;
        }
        if ((tags != null || aff.getTags() != null) && !GenUtil.sameCollections(tags, aff.getTags())) {
            return false;
        }
        if ((affiliateConfigs != null || aff.getAffiliateConfigs() != null) && !GenUtil.sameMaps(affiliateConfigs, aff.getAffiliateConfigs())) {
            return false;
        }
        if ((affiliateLogo != null || aff.getAffiliateLogo() != null) && !GenUtil.equals(affiliateLogo, aff.getAffiliateLogo())) {
            return false;
        }
        if ((contactDetail != null || aff.getContactDetail() != null) && !GenUtil.equals(contactDetail, aff.getContactDetail())) {
            return false;
        }
        if ((depositAccounts != null || aff.getDepositAccounts() != null) && !GenUtil.sameCollections(depositAccounts, aff.getDepositAccounts())) {
            return false;
        }
        if ((departments != null || aff.getDepartments() != null) && !GenUtil.equals(departments, aff.getDepartments())) {
            return false;
        }
        if ((resources != null || aff.getResources() != null) && !GenUtil.sameCollections(resources, aff.getResources())) {
            return false;
        }
        if ((affiliateUserProfiles != null || aff.getAffiliateUserProfiles() != null) && !GenUtil.sameCollections(affiliateUserProfiles, aff.getAffiliateUserProfiles())) {
            return false;
        }
        if ((affiliateTourCodes != null || aff.getAffiliateTourCodes() != null) && !GenUtil.sameCollections(affiliateTourCodes, aff.getAffiliateTourCodes())) {
            return false;
        }

        return true;

    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
