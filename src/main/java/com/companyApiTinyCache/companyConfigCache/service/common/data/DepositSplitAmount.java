package com.companyApiTinyCache.companyConfigCache.service.common.data;

import java.io.Serializable;

public class DepositSplitAmount implements Serializable {

    public DepositSplitAmount(Double initSplitAmt, Double availabelSplitAmt) {
        this.availabelSplitAmt = availabelSplitAmt;
        this.initSplitAmt = initSplitAmt;
    }

    private static final long serialVersionUID = 1L;

    Double availabelSplitAmt;

    Double initSplitAmt;

    public Double getAvailabelSplitAmt() {
        return availabelSplitAmt;
    }

    public void setAvailabelSplitAmt(Double availabelSplitAmt) {
        this.availabelSplitAmt = availabelSplitAmt;
    }

    public Double getInitSplitAmt() {
        return initSplitAmt;
    }

    public void setInitSplitAmt(Double initSplitAmt) {
        this.initSplitAmt = initSplitAmt;
    }

}
