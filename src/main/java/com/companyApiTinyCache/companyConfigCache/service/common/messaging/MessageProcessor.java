package com.companyApiTinyCache.companyConfigCache.service.common.messaging;

public interface MessageProcessor {
    void process(Object message);
}
