package com.companyApiTinyCache.companyConfigCache.service.common.web.filter.ss2;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

/**
 * ConnConstants.
 * @author ss2
 */
@ClassExcludeCoverage
public final class ConnConstants {

    private ConnConstants() {

    }

    public static final String IS_REQD_STATIC_FILTER = "_isreqd_staticfilter";

    public static final String FILTER_REQ_TYPE = "_reqfilter";

    public static final String JS_DATA_URL = "_js_url";

    public static final String HTTP_API_URL = "_ss2_http_api_url";

    public static final String MODE = "_mode";

    public static final String IP_ADDRESS = "_ipaddress";

    public static final String SID = "_sid";

    public static final String SESS_ID = "_sessid";

    public static final String JS_API_URL = "_ss2_js_api_url";

    public static final String REQ_TIME_OUT = "_timeout_value";
}
