package com.companyApiTinyCache.companyConfigCache.service.common.util.concurrent;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * A utility class which cannot be instantiated but which provides two final instances of ThreadFactorys one for creating daemon threads and one for creating user threads.
 * 
 * @author Suresh
 * 
 */
public class SimpleThreadFactory implements ThreadFactory {

    /**
     * ThreadFactory which creates daemon threads.
     */
    public static final SimpleThreadFactory daemonThreadFactory = new SimpleThreadFactory(true);

    /**
     * ThreadFactory which creates user threads.
     */
    public static final SimpleThreadFactory threadFactory = new SimpleThreadFactory(false);

    private ThreadFactory def = Executors.defaultThreadFactory();

    private boolean isDaemon;

    public Thread newThread(Runnable r) {
        Thread t = def.newThread(r);
        t.setDaemon(isDaemon);
        return t;
    }

    private SimpleThreadFactory(boolean pisDaemon) {
        isDaemon = pisDaemon;
    }

}
