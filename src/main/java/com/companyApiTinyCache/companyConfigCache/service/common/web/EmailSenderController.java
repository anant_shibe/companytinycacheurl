package com.companyApiTinyCache.companyConfigCache.service.common.web;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;
import com.companyApiTinyCache.companyConfigCache.service.common.notification.EmailSenderViaClevertap;
import com.companyApiTinyCache.companyConfigCache.service.common.util.CachedProperties;
import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.util.JsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author parvatkumar
 *
 */
@ClassExcludeCoverage
public class EmailSenderController extends AbstractController {

	private static final Log logger = LogFactory.getLog(EmailSenderController.class);

	private static final String SEND_EMAIL_VIA_CLEVERTAP = "ct.air.send.mail.clevertap.enable";

	private static final String PAYLOAD_VALIDATE_RESPONSE_JSON = "ct.air.clevertap.email.response.json";

	private CachedProperties cachedProperties;

	private EmailSenderViaClevertap emailSenderViaClevertapImpl;

	public CachedProperties getCachedProperties() {
		return cachedProperties;
	}

	public void setCachedProperties(CachedProperties cachedProperties) {
		this.cachedProperties = cachedProperties;
	}

	public EmailSenderViaClevertap getEmailSenderViaClevertapImpl() {
		return emailSenderViaClevertapImpl;
	}

	public void setEmailSenderViaClevertapImpl(EmailSenderViaClevertap emailSenderViaClevertapImpl) {
		this.emailSenderViaClevertapImpl = emailSenderViaClevertapImpl;
	}

	@SuppressWarnings({ "null", "unused", "unchecked" })
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		logger.info("Request for sending mail through clevertap");
		ObjectMapper mapper = null;
		PrintWriter pw = null;
		response.setContentType("text/json");

		try {
			boolean isMailEnabled = cachedProperties.getBooleanPropertyValue(SEND_EMAIL_VIA_CLEVERTAP, false);
			String emailResponseAsString = cachedProperties.getPropertyValue(PAYLOAD_VALIDATE_RESPONSE_JSON);
			Map<String, String> emailResponseMap = null;
			Map<String, Object> payLoadMap;
			List<String> errorMessages = null;
			boolean isValidPayload;
			InputStream is;
			mapper = JsonUtil.getObjectMapper();
			pw = response.getWriter();
			if (isMailEnabled) {
				if (request.getMethod().equalsIgnoreCase("POST")) {
					if (StringUtils.isNotBlank(emailResponseAsString)) {
						emailResponseMap = mapper.readValue(emailResponseAsString, Map.class);
					}
					is = request.getInputStream();
					String requestPayloadAsString = GenUtil.readAsStringString(is);
					is.close();
					if(StringUtils.isNotBlank(requestPayloadAsString)) {
						errorMessages = new ArrayList<String>();
						payLoadMap = mapper.readValue(requestPayloadAsString, Map.class);
						isValidPayload = emailSenderViaClevertapImpl.validatePayload(payLoadMap, errorMessages, emailResponseMap);
						if(isValidPayload) {
							emailSenderViaClevertapImpl.sendEmail(payLoadMap);
						}else {
							response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
							if(CollectionUtils.isNotEmpty(errorMessages)) {
								pw.println("{\"statusCode\":400,\"message\":\"" + errorMessages.get(0) + "\"}");
							}else {
								pw.println("{\"statusCode\":400,\"message\":\" Payload is not valid. \"}");
							}
						}
						
					}else {
						response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
						if(MapUtils.isNotEmpty(emailResponseMap)) {
							pw.println("{\"statusCode\":400,\"message\":\"" + emailResponseMap.get("emptyPayload") + "\"}");
						}else {
							pw.println("{\"statusCode\":400,\"message\":\"No data was sent at all empty payload.\"}");
						}
					}

				} else {
					response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
					if (MapUtils.isNotEmpty(emailResponseMap)) {
						pw.println("{\"statusCode\":405,\"message\":\"" + emailResponseMap.get("invMethod") + "\"}");
					} else {
						pw.println("{\"statusCode\":405,\"message\":\"This method is not allowed.\"}");
					}
				}
			} else {
				response.setStatus(HttpServletResponse.SC_OK);
				if (MapUtils.isNotEmpty(emailResponseMap)) {
					pw.println("{\"statusCode\":202,\"message\":\"" + emailResponseMap.get("disabled") + "\"}");
				} else {
					pw.println(
							"{\"statusCode\":202,\"message\":\"We are sorry. This feature is currently disabled.\"}");
				}

			}
		} catch (Exception e) {
			logger.error("Exception occured while sending mail through clevertap", e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			if (pw == null) {
				pw = response.getWriter();
			}
			pw.println("{\"statusCode\":500,\"message\":\"Push Event Failed with exception: " + e.getClass().getName()
					+ "}");
		} finally {
			if (pw != null) {
				pw.close();
			}
		}

		return null;
	}

}
