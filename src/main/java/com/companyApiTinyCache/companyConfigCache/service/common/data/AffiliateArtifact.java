package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class AffiliateArtifact implements Serializable {

    public AffiliateArtifact() {
    }

    public AffiliateArtifact(int id) {
        this.id = id;
    }

    private int id = -1;

    private String type;

    private String description;

    private String name;

    private String viewName;

    private String actionName;

    private String isProtected;

    private Date createDate;

    private Date updateDate;

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        if (this.id != -1)
            throw new RuntimeException("artifact id is immutable property");
        this.id = id;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("view_name")
    public String getViewName() {
        return viewName;
    }

    @JsonProperty("view_name")
    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    @JsonProperty("action_name")
    public String getActionName() {
        return actionName;
    }

    @JsonProperty("action_name")
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    @JsonProperty("protected")
    public String isProtected() {
        return isProtected;
    }

    @JsonProperty("protected")
    public void setProtected(String isProtected) {
        this.isProtected = isProtected;
    }

    @JsonProperty("create_date")
    public Date getCreateDate() {
        return createDate;
    }

    @JsonProperty("create_date")
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @JsonProperty("update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    @JsonProperty("update_date")
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AffiliateArtifact other = (AffiliateArtifact) obj;
        if (id != other.id)
            return false;
        return true;
    }

}
