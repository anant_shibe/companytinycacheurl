package com.companyApiTinyCache.companyConfigCache.service.common.rabbitmq.exceptions;

import com.companyApiTinyCache.companyConfigCache.service.common.annotation.coverage.ClassExcludeCoverage;

@ClassExcludeCoverage
public class RabbitMQPublishMesaageException extends Exception {

	private static final long serialVersionUID = 1L;
    public RabbitMQPublishMesaageException(Exception exception) {
    	super(exception);
    	
    }
    public RabbitMQPublishMesaageException(String message) {
    	super(message);
    }
    public RabbitMQPublishMesaageException(String message, Exception exception) {
    	super(message, exception);
    }
}
