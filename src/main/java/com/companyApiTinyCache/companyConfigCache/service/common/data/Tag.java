/**
 * 
 */
package com.companyApiTinyCache.companyConfigCache.service.common.data;

import com.companyApiTinyCache.companyConfigCache.service.common.util.GenUtil;
import com.companyApiTinyCache.companyConfigCache.service.common.web.CommonEnumConstants.TagStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Cleartrip
 * 
 */
public class Tag implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;

    private int tagId;

    @JsonIgnore
    private int affiliateId;

    private TagStatus status;

    private String tagName;

    private String tagDesc;

    @JsonIgnore
    private String createdBy;

    private Date created_date;

    @JsonIgnore
    private String updatedBy;

    private String sourceType;

    @JsonProperty("id")
    public int getTagId() {
        return tagId;
    }

    @JsonProperty("id")
    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    @JsonProperty("tag_name")
    public String getTagName() {
        return tagName;
    }

    @JsonProperty("tag_name")
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @JsonProperty("tag_description")
    public String getTagDesc() {
        return tagDesc;
    }

    @JsonProperty("tag_description")
    public void setTagDesc(String tagDesc) {
        this.tagDesc = tagDesc;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonProperty("created_at")
    public Date getCreated_date() {
        return created_date;
    }

    @JsonProperty("created_at")
    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    @JsonIgnore
    public int getAffiliateId() {
        return affiliateId;
    }

    @JsonIgnore
    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    @JsonProperty("status")
    public TagStatus getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(TagStatus status) {
        this.status = status;
    }

    @JsonProperty("updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updated_by")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @JsonProperty("source_type")
    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    @JsonProperty("source_type")
    public String getSourceType() {
        return sourceType;
    }

    @Override
    @JsonIgnore
    public boolean equals(Object obj) {
        if (obj instanceof Tag == false)
            return false;
        Tag tag = (Tag) obj;
        // if(tagId != tag.getTagId()){
        // return false;
        // }
        if ((tagName != null || tag.getTagName() != null) && !GenUtil.equals(tagName, tag.getTagName())) {
            return false;
        }
        if ((tagDesc != null || tag.getTagDesc() != null) && !GenUtil.equals(tagDesc, tag.getTagDesc())) {
            return false;
        }

        return true;
    }
}
