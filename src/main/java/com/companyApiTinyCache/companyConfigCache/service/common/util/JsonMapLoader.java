package com.companyApiTinyCache.companyConfigCache.service.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.util.Map;

public class JsonMapLoader implements StringResourceLoader<Map<String, Object>>, StreamResourceLoader<Map<String, Object>> {

    private static final Log logger = LogFactory.getLog(JsonMapLoader.class);

    @Override
    public Map<String, Object> loadResource(String s) {
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        Map<String, Object> map = null;
        try {
            if (s != null) {
                map = (Map<String, Object>) mapper.readValue(s, Map.class);
            }
        } catch (Exception e) {
            logger.error("Error Reading JSON", e);
        }

        return map;
    }

    @Override
    public Map<String, Object> loadStreamResource(InputStream is) throws Exception {
        ObjectMapper mapper = JsonUtil.getObjectMapper();
        Map<String, Object> map = null;
        if (is != null) {
            map = (Map<String, Object>) mapper.readValue(is, Map.class);
        }

        return map;
    }

}
